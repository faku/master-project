\cleardoublepage
\section{Introduction}

Modern computational problems require a growing amount of computational power, thus increasing the electrical power
consumption. Nowadays, \acrfull{ict} represent about 8\% of global electricity usage and could consume up to 20\% of
global electricity by 2025 \cite{electricity-share}.

A famous modern computing application includes \acrfullpl{ann}. These computing systems are vaguely inspired by the
biological \acrlongpl{nn} that constitute animal brains. An \acrlong{ann} is based on a collection of connected units or
nodes called artificial neurons, which loosely model the neurons in a biological brain.
Neurons can communicate with each other through connections that deliver signals, similarly to the synapses of a
biological brain.
An artificial neuron receiving a signal can then process it and further signal the neurons connected to it. \\
Globally, those networks can therefore serve to model complex patterns and prediction problems, given specific input
information.

Recent research has showed \acrlongpl{ann} capability to perform successfully well in several domains among which
biology \cite{biology-nn}, image processing \cite{deep-learning-lecun}, self-driving cars \cite{self-driving-cars} and
many others. Most of modern \acrshort{nn} architectures include convolutional layers and thus are called
\acrfullpl{cnn}. These \acrshortpl{cnn} are specialized in analyzing visual imagery. Furthermore, there is an important
need of deploying \acrshortpl{cnn} on embedded systems and mobile devices for applications such as autonomous cars
\cite{self-driving-cars} and medical devices \cite{healthcare-deep-learning}, \cite{surgery-deep-learning}, which demand
real-time and high-accuracy object recognition.

\acrlongpl{cnn}, and more generally \acrlongpl{ann}, consume a lot of power and require high computational loads.
However, embedded systems and mobile devices usually are limited in terms of resources. They often use a battery rather
than being plugged into a power source. Moreover, they are equipped with a low-power, low-frequency processor. Finally,
they commonly embeds small memory amount. These limitations make the use of embedded systems for \acrlongpl{ann}
implementation quite difficult.

In order to reduce power consumption and accelerate computational loads, an approach could be to use specific hardware.
Two possible accelerators are \acrfullpl{gpu} and \acrfullpl{fpga}, which can often achieve better performance than
\acrfullpl{cpu} on certain workloads \cite{bing-fpga}, \cite{accelerated-convnn}, \cite{perf-energy-comp-fpga-cpu-gpu},
\cite{Wal_2015_CVPR_Workshops}. \acrshortpl{gpu} are designed to perform floating-point operations and run software
while \acrshortpl{fpga} are integrated circuits that can be customized for a specific application. Since hardware is
faster and more power-efficient than software for specific applications, \acrshortpl{fpga} are generally a better choice
than \acrshortpl{gpu} \cite{bing-fpga}, \cite{energy-comparison}. Additionally, \acrshortpl{fpga} offer true parallel
processing, unlike \acrshortpl{cpu} and \acrshortpl{gpu}, and high-speed data processing.

However, hardware programming, and more specifically \acrshort{fpga} programming, is less user-friendly and more
unwieldy than software programming. It necessitates advanced knowledge in \acrfullpl{hdl}, such as VHDL or
SystemVerilog, as well as knowing precisely the target \acrshort{fpga} development board architecture. Moreover,
hardware programming requires specialized tools (\eg Xilinx's Vivado Design Suite \cite{vivado}, Intel's Quartus Prime
\cite{quartus}). Finally, developing hardware accelerators requires the developer to know perfectly the target of the
accelerator and its application. \\
These limitations can be discouraging thus limiting the development of hardware accelerators for modern computing
problems, like \acrlongpl{ann}.


\subsection{Aim and objectives}
\label{sec:aim-objectives}

In this work, our aim is to study the available technologies and toolchains for generating hardware accelerators
automatically, ideally without the developer's intervention. We are looking for a solution that seeks to reduce or even
-- ideally -- eliminate the hardware knowledge required to produce such an accelerator under normal circumstances.

The main purpose of this project is to critically analyze the use of a framework (or toolchain) for generating hardware
accelerators automatically, or almost. This critic will discuss the ease of use and the performances in terms of
resources utilization and time. \newpage
These performances will be compared among:
\begin{itemize}
    \item   A solution running on a \acrshort{cpu}
    \item   A solution developed by a -- human -- programmer, with a \acrshort{hdl}
    \item   A solution generated with the framework
    \item   The same solution as above, but optimized with code transformations manually applied after generation
\end{itemize}

As a testbench, the idea was to create an \acrshort{fpga}-based system which consists of acquiring image data from a
camera connected to an \acrshort{fpga} board and streaming out the data through an \acrfull{hdmi} port, also present on
the board. \\
The data passes through a \acrlong{ml} module which performs hand-written digits recognition. This module was
generated by a solution chosen among different frameworks. These several frameworks are presented later in the state of
the art section, at the end of which we discuss which framework is the best candidate for this work and why. The system
is described in details in \autoref{sec:system-desc}.

Once the hardware system had been set up, we were able create and include the \acrlong{ml} hardware accelerator that
performs hand-written digits recognition. \\
In order to find the framework that best met our needs and to then integrate the results, we followed the subsequent
workflow:
\begin{enumerate}[label=\textbf{\arabic*.}]
    \item   \textbf{Review existing frameworks} \\
            We first reviewed frameworks that allow to create an hardware accelerator from an high-level programming
            language (\eg Python) to an \acrlong{hdl} (\eg VHDL, SystemVerilog). Out of all the framework, one was
            chosen as the best candidate and was then used for the rest of this project.

    \item   \textbf{Define, train and test a \acrlong{ml} model}    \\
            Before synthesizing the model into an accelerator, we defined the model used to perform hand-written digits
            recognition. Once we defined its architecture, we then had to prepare the training data and train the model.
            Finally, we tested its accuracy.

    \item   \textbf{Use the framework}  \\
            The framework previously chosen in point 1. to generate an hardware accelerator of the model described
            in point 2. Every step is presented and documented. We completed this step with a critical analysis of the
            framework's generated code and utilization.

    \item   \textbf{Manual optimizations}  \\
            Some optimizations were added manually to the generated accelerator, to see what tradeoffs could be reached.
            This resulted in a newly optimized version of the accelerator that had to be compared with the other
            solutions.

    \item   \textbf{Analyze results}    \\
            The performance of the generated accelerator was then compared with the other solutions. The comparison was
            performed between a solution running on a \acrshort{cpu}, a solution developed by a human programmer using
            an \acrshort{hdl} and finally, the solution generated by the framework, and finally the solution with
            manual optimizations applied after-hand.

    \item   \textbf{Conclude}   \\
            We highlighted the different outcomes of this project, while presenting the possible future works that can
            follow.
\end{enumerate}


\subsection{System description}
\label{sec:system-desc}

This subsection explains how the system, which was used as a testbench for the accelerator generated later in this
work, has been put in place.

For acquiring image data, the Pcam 5C module \cite{pcam5c} is used. It embeds the Omnivision OV5640 5 MP color image
sensor \cite{ov5640-ds}. Data is transferred over a dual-lane MIPI CSI-2 interface and offers common video streaming
formats such as 1080p at 30 frames per second and 720p at 60 frames per second. The camera module is connected to the
\acrshort{fpga} board via a 15-pin \acrfull{ffc} which is pin-compatible with a Pcam connector.

Initially, we tried to deploy the system on a Xilinx Zynq-7000 \acrshort{soc} ZC702 Evaluation Kit \cite{zc702-ug}.
Since the ZC702 board doesn't have a Pcam connector, we had to interface the camera module to the board by using a FMC
Pcam adapter \cite{pcam-fmc-rm}. However, we had so many problems for communicating with the camera module from the
board -- in particular with the \acrfull{i2c} protocol -- that we preferred to change the development board for a Zybo
Z7-20 \cite{zybo-rm}. \\
The reason for this change was that Digilent, the Pcam 5C module constructor, made a demonstration project available
online \cite{zybo-demo}. This project targets the Zybo Z7-20 as the \acrshort{fpga} development board. The board's main
features are presented in \autoref{appx:zybo-z7}.

Once data is successfully streamed from the camera module, the data must be stored into the board's \acrfull{sdram}. To
do so, we use a \acrfull{vdma} \cite{axi-vdma}. It provides high-bandwidth direct memory access between the board's
\acrshort{sdram} memory and its peripherals, such as the Pcam 5C camera module. With the \acrshort{vdma} set up, we
simply have to stream out the data to the \acrshort{hdmi} port. \vspace{10pt} \\

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=\linewidth]{figures/block_diagram.png}
    \captionof{figure}{Block design of the testbench system.}
\end{minipage}


\begin{comment}
\subsection{Quick introduction to Machine Learning}
\label{sec:intro-ml}

\todo{Find a way to introduce this subsection in the main introduction above.}

This subsection will do a quick introduction about the basics of \acrlong{ml} and subjects that might be helpful to
better understand this study.

\acrfull{ml} is a data analytics technique that teaches computers to learn as humans or animals: from experience.
\acrlong{ml} algorithms use computational methods to \textit{learn} information directly from data without relying on a
predetermined equation as a model \cite{what-is-ml}.

\acrlong{ml} uses two types of techniques: \textit{supervised learning}, which trains a model on known input and output
data so that it can predict future outputs; and \textit{unsupervised learning}, which finds hidden patterns by itself in
input data without output data.

\textbf{Supervised learning} uses classification and regression techniques to develop predictive models. \\
Classification techniques predict discrete responses, they classify input data into categories. Typical applications
include medical imaging, speech recognition and credit scoring. \\
Regression techniques predict continuous responses, such as changes in temperature or fluctuations in power demand.
Typical applications include electricity load forecasting and algorithmic trading.

\textbf{Unsupervised learning} finds hidden patterns or intrinsic structures in data. It is used to draw inferences from
datasets consisting of input data without labeled responses. \\
Clustering is the most common unsupervised learning technique. It is used for exploratory data analysis to find hidden
patterns or groupings in data. Applications for cluster analysis include gene sequence analysis, market research and
object recognition.

In this study, we will talk about \acrshort{ml} algorithms based on supervised learning only.

\subsubsection*{What is a \acrlong{nn}?}

To get started, we first have to define what is a \textit{perceptron}. A perceptron is an artificial neuron that takes
one or several inputs and produces a single output, just like human neurons.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.4\linewidth]{figures/intro-perceptron.png}
    \captionof{figure}{A perceptron with three inputs \cite{nielsen-neural}.}
    \label{fig:intro-perceptron}
\end{minipage}

In the example shown above, the perceptron has three inputs $x_1$, $x_2$ and $x_3$. In general, it could have more or
fewer inputs. Each input has a weight ($w_1$, $w_2$, $w_3$) expressing its importance relative to the others. The
neuron's output, $0$ or $1$, is determined by whether the weighted sum $\sum_i w_i x_i$ is less than or greater than a
\textit{threshold} value. To put it in more algebraic terms:
\begin{equation}
\text{output} =
    \begin{cases}
        0 & \text{if } \sum_i w_i x_i \leq \text{threshold} \\
        1 & \text{if } \sum_i w_i x_i > \text{threshold}
    \end{cases}
\end{equation}

Obviously, the perceptron is not a complete model of human decision-making but it
seems plausible that a complex network of perceptrons could make quite subtle
decisions. This is what we call a \acrlong{nn}.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.5\linewidth]{figures/intro-neural_network.png}
    \captionof{figure}{Simple \acrfull{mlp} network \cite{nielsen-neural}.}
    \label{fig:intro-simple_mlp}
\end{minipage}

A \acrfull{nn} is composed of several layers (three in the example shown in \autoref{fig:intro-simple_mlp}).
The leftmost layer in this network is called the \textit{input layer}, and the
neurons within the layer are called \textit{input neurons}. \\
The rightmost layer, or \textit{output layer}, contains the \textit{output
neurons}, or, as in this case, a single output neuron. \\
The middle layer is called a \textit{hidden layer} since the neurons in this
layer are neither inputs nor outputs. The network above has just a single hidden
layer but some networks have multiple hidden layers.

\subsubsection*{Training, validating and testing a \acrlong{nn}}

The data used to build the final model usually comes from multiple datasets. In
particular, three datasets are commonly used in different stages of the creation
of the model \cite{training-testing-nn}.

The model is initially fit on a \textit{training dataset}. The \acrshort{nn} is
run with the training dataset and produces a result, which is then compared with
the \textit{target} (or \textit{label}), for each input of the training dataset.
Based on the result of the comparison, the parameters of the model are adjusted.

Successively, the fitted model is used to predict the responses for the
observations in a second dataset, called the \textit{validation dataset}.
Validation datasets can be used for stopping the training when the error on the
validation dataset increase, as this is a sign of over-fitting to the training
dataset.

Finally, the \textit{test dataset} is a dataset used to provide an unbiased
evaluation of the final model predictions. A test dataset is independent of the
training dataset.

\subsubsection*{Inference}

Once the \acrlong{nn} has been trained, it is ready for inference -- to classify,
recognize and process new inputs. For instance, the voice of someone talking to
Siri on his iPhone is sent to an Apple server on which a \acrshort{cnn} trained
for speech recognition has been deployed.
\end{comment}