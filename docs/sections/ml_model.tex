\cleardoublepage
\section{The Machine Learning model}

This section describes the second objective of this project. We first describe the \acrlong{ml} model architecture and
then present how we did prepare the dataset used for training and validating the model.


\subsection{The model architecture}

For this work, we chose to deploy a \acrshort{cnn} that recognizes hand-written digits. This choice was motivated by
the fact that the second solution, which is taken from the \citetitle{solovyev-2019} paper, written by
\citeauthor{solovyev-2019} \cite{solovyev-2019}. This paper was the reference for the \textit{Inference developed
by a -- human -- developer with a \acrshort{hdl}} solution, as stated in \autoref{sec:aim-objectives}. The
\autoref{fig:workflow-network} shows its detailed architecture. \\
This network uses the MNIST dataset \cite{mnist}, which is composed of 60'000 train images and 10'000 test images.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[scale=0.37]{figures/workflow-network.png}
    \captionof{figure}{\acrshort{cnn} architecture used for describing the workflow.}
    \label{fig:workflow-network}
\end{minipage}

The model takes a grayscale (1 component per pixel) image of dimensions 28x28 as input. The data is then processed by
6 convolutional layers (each activated by the \acrshort{relu} function). The last layer performs a \texttt{Dense}
operation, which is finally activated by the \texttt{Softmax} function. \\
The output is an array of 10 values (from 0 to 9), each corresponding to the probability of being the digit in the input
image.

\begin{minipage}{\linewidth}
    \centering
    \begin{minipage}{0.45\linewidth}
        \centering
        \fbox{
            \includegraphics[height=42mm]{figures/workflow-mnist_example.png}
        }
    \end{minipage}
    \hfill{}
    \begin{minipage}{0.45\linewidth}
        \begin{minted}{python}
[
    0.10118849, # 0
    0.07356359, # 1
    0.08777632, # 2
    0.09791587, # 3
    0.10229186, # 4
    0.08554156, # 5
    0.07353777, # 6
    0.16658820, # 7 (highest probability)
    0.09403618, # 8
    0.11756021  # 9
]
        \end{minted}
    \end{minipage}
    \captionof{figure}{Inference example.}
\end{minipage}


\subsection{Preparing the dataset}

We then prepared the dataset that was used to train the network. The function \mintinline{python}{load_mnist_data()}
shown below does such a task.

\inputminted[linenos=true,firstline=38,lastline=79,label=src/python/train\_nn.py]{python}{../src/python/train_nn.py}

The line 52 loads the MNIST dataset. Train (resp. test) images are stored in the array \mintinline{python}{train_x}
(resp. \mintinline{python}{train_y}) and train (resp. test) labels (\ie classes) are stored in the array
\mintinline{python}{train_x} (resp. \mintinline{python}{test_y}).

At lines 55 to 62, we adapt the format of the dataset. \acrlongpl{nn} can have different different data layouts: NCHW or
NHWC, where:
\begin{itemize}
    \item[\textbf{N:}]  batch size
    \item[\textbf{C:}]  channels
    \item[\textbf{H:}]  height
    \item[\textbf{W:}]  width
\end{itemize}
Thus, if \mintinline{python}{data_format} is \mintinline{python}{channels_first} (resp.
\mintinline{python}{channels_last}), NCHW (resp. NHWC) data layout will be applied on the whole dataset. Otherwise,
an exception is raised. Default is NCHW.

On lines 72 and 73, we convert classes to binary representation (\eg \texttt{4} becomes \texttt{0100}). This step is
optional. We apply it to have the exact same network output as the one proposed by \citeauthor{solovyev-2019}
\cite{solovyev-2019}.

Finally, lines 76 and 77 invert the MNIST colors. It is also an optional step as it depends on the data we want to train
the network with. The original images provided by the MNIST dataset are white digits written on a black background. Such
configuration is rare in real-life applications thus we decided to invert the colors on the whole dataset. Since MNIST
images have their pixels coded with 8-bit unsigned integers, we simply have to subtract the pixel value from 255.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.6\linewidth]{figures/workflow-before_after.png}
    \captionof{figure}{MNIST image before and after dataset preparation.}
\end{minipage}


\subsection{Training the network}
\label{sec:workflow-training}

To train the network, we used the Keras framework \cite{keras}. The first thing to do was to define and build the whole
\acrshort{nn} architecture, as described by \citeauthor{solovyev-2019} \cite{solovyev-2019}.

\newpage
\inputminted[linenos=true,firstline=81,lastline=123,label=src/python/train\_nn.py]{python}{../src/python/train_nn.py}

From line 98 to line 103, the input shape is defined accordingly to the specified data layout.

The first two blocks (lines 105 to 107 and lines 109 to 111) are each composed of two 2D-Convolutional layers (with
\acrshort{relu} being the activation function for both) and a 2D-MaxPooling layer.

The third block (lines 113 to 115) is also composed of two 2D-Convolutional layers (with \acrshort{relu} being the
activation function for both) but composed of a 2D-GlobalMaxPooling layer.

Finally, a Dense layer is used to connect all neurons from the previous 2D-GlobalMaxPooling layer to the output layer.
The Dense layer has the Softmax function acting as the activation function.

The execution of line 121 gives the following output:

\begin{minipage}{\linewidth}
    \centering
    \begin{BVerbatim}
_________________________________________________________________
Layer (type)                 Output Shape              Param #
=================================================================
input_1 (InputLayer)         (None, 1, 28, 28)         0
_________________________________________________________________
conv1 (Conv2D)               (None, 4, 28, 28)         36
_________________________________________________________________
conv2 (Conv2D)               (None, 4, 28, 28)         144
_________________________________________________________________
pool1 (MaxPooling2D)         (None, 4, 14, 14)         0
_________________________________________________________________
conv3 (Conv2D)               (None, 8, 14, 14)         288
_________________________________________________________________
conv4 (Conv2D)               (None, 8, 14, 14)         576
_________________________________________________________________
pool2 (MaxPooling2D)         (None, 8, 7, 7)           0
_________________________________________________________________
conv5 (Conv2D)               (None, 16, 7, 7)          1152
_________________________________________________________________
conv6 (Conv2D)               (None, 16, 7, 7)          2304
_________________________________________________________________
pool3 (GlobalMaxPooling2D)   (None, 16)                0
_________________________________________________________________
dense_1 (Dense)              (None, 10)                160
_________________________________________________________________
activation_1 (Activation)    (None, 10)                0
=================================================================
Total params: 4,660
Trainable params: 4,660
Non-trainable params: 0
_________________________________________________________________
    \end{BVerbatim}
    \captionof{figure}{Keras model summary.}
\end{minipage}

This gives us more information about the output shape of each layer and their respective number of trainable parameters.
The total count of trainable parameters included in the model is 4'660, which should easily fit in any \acrshort{fpga}
design or embedded \acrfull{sdram}.


After the network definition step came the training. This was performed by the
\mintinline{python}{train_model()} function. We will not describe its code in details here but only describe what it
does. The complete Python code is available in \autoref{appx:train-nn}. \\
The \mintinline{python}{train_model()} function first searches for an already trained model that might have been
saved into a file named \texttt{mnist\_weights.h5}. If this file exists, the model is loaded from there. Otherwise, the
model is trained and then saved into the same file mentioned previously. \\
Upon completion, the function outputs the model score and accuracy: \\
\begin{center}
\begin{BVerbatim}
Model score   : 0.07440621950239874
Model accuracy: 0.975600004196167
\end{BVerbatim}
\end{center}

\begin{minipage}{\linewidth}
    \centering
    \begin{minipage}{0.45\linewidth}
        \centering
        \fbox{
            \includegraphics[height=42mm]{figures/workflow-test_image.png}
        }
    \end{minipage}
    \hfill{}
    \begin{minipage}{0.45\linewidth}
        \centering
        \begin{minted}{python}
[
    1.1960877e-11,  # 0
    4.3369418e-13,  # 1
    7.8879259e-09,  # 2
    9.9999964e-01,  # 3 (highest)
    9.1875462e-16,  # 4
    3.9355601e-07,  # 5
    5.5602975e-16,  # 6
    3.8441872e-09,  # 7
    1.0099800e-09,  # 8
    1.9590207e-08   # 9
]
        \end{minted}
    \end{minipage}
    \captionof{figure}{Model predictions for a non-MNIST image.}
\end{minipage}

The last step in the network training was to rescale all the model weights. It is necessary if we want to use fixed-point
arithmetics, which should speed-up performance. In fact, when implemented at the hardware level, floating-point
calculations are slower than fixed-point calculations due to the difficulty of controlling the mantissa and the exponent
of the values.

The method used for rescaling the weights is the same as the one used by \citeauthor{solovyev-2019}
\cite{solovyev-2019}. Basically, it consists of looking at all weights and all possible input and output values and
normalizing them. The lowest value is translated to \texttt{-1.0} and the highest to \texttt{1.0}. \\
The function \mintinline{python}{rescale_weights()} in the file \texttt{src/python/train\_nn.py} takes care of rescaling
the model weights. The code is available in \autoref{appx:train-nn}. \\
The accuracy of the rescaled model might differ from the initial one, due to precision loss during normalization.

At this point, we had our model trained. We then moved on to actually use HeteroCL, the framework previously selected
in \autoref{sec:state-art}.