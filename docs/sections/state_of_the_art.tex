\cleardoublepage
\section{State of the art}
\label{sec:state-art}

This section reviews existing frameworks that aim at translating a \acrlong{ml} model to synthesizable hardware. This
corresponds to the first objective of this project. \\
This review also serves as a state of the art, since it describes the most recent state in the development of this
technology.

The most interesting candidate among those frameworks was chosen and used for the rest of this project. For each tool,
a small description as well as the tool's objectives are presented and the following criteria are evaluated:
\begin{enumerate}[label=\textbf{\arabic*.}]
    \item   \textbf{Maintenance and Update frequency}   \\
            It is important to know if the tool is under active development and who is responsible for the development.
            This also allows us to know whether we can expect potential errors to be corrected quickly, and also to have
            an idea of the current stage of development of the tool.

    \item   \textbf{License}    \\
            The license informs us of the conditions under which the tool may be used, distributed or modified. This
            criteria is crucial in this project since we might want to modify the tool so it better suits our needs.

    \item   \textbf{Which input(s) the tool accepts}    \\
            Does it use already existing \acrshort{ml} description framework(s) or its own? It is preferred if the
            framework accepts already existing description, so it saves us the trouble to discover and learn another
            description framework.

    \item   \textbf{Which output(s) the tool targets}   \\
            This criteria informs on the versatility and flexibility of  the tool. More flexible tools will be easier
            to use. Therefore, the more outputs a tool supports, the more will it be an interesting candidate.

    \item   (Optional) \textbf{Additional remarks}
\end{enumerate}

A table summarizing this state of the art is available in \autoref{sec:frameworks-summary}.


\subsection{DnnWeaver 2.0}

DnnWeaver is an open-source framework for accelerating \acrfullpl{dnn} on \acrshortpl{fpga}. It aims to bridge the
semantic gap between the high-level specifications of \acrshort{dnn} models used by programmers and \acrshort{fpga}
acceleration \cite{dnnweaver}.


\vspace{5pt} \textbf{Maintenance and Update frequency} \vspace{2pt} \\
DnnWeaver is maintained by a team of six developers who are part of the Georgia Institute of Technology.

The project doesn't seem to be updated frequently. Based on its Github repository \cite{dnnweaver-repo}, some commits
occurred in April 2019 but the previous ones were made in November 2018.

The fact that the project is maintained by a scholar team probably means that they have other works besides DnnWeaver
and it is reflected on the commits history.

\vspace{5pt} \textbf{License} \vspace{2pt} \\
According to its website, DnnWeaver is licensed under the Apache License 2.0 \cite{license-apache2}, which requires
preservation of the copyright notice and disclaimer but allows the user of the software the freedom to use the software
for any purpose, to distribute it, to modify it and to distribute modified versions of the software, under the terms of
the license, without concerns for royalties.

\vspace{5pt} \textbf{Input} \vspace{2pt} \\
The programmer specifies the \acrlong{dnn} using Caffe format.

\newpage \textbf{Output} \vspace{2pt} \\
Given the input, the framework automatically generates the accelerator SystemVerilog code specialized for the given
network, using hand-optimized SystemVerilog templates (included in DnnWeaver).

As of January 2020, the implemented layers are Convolution, \acrfull{relu}, Fully Connected Layer (InnerProduct),
Pooling and \acrfull{lrn}.

\vspace{5pt} \textbf{Remarks} \vspace{2pt} \\
The available documentation only concerns the version 1.0 of the tool, which makes it difficult to gather valid
information about the current version.

A colleague has used DnnWeaver in the past and, according to him, the framework uses a mix of \textit{Python2.7} and
\textit{Python3.6}, raising errors during utilization. The only way to fix this is to manually update the incompatible
files to \textit{Python3.6}.

Additionally, it seems that adding an unknown \acrshort{fpga} to the framework as an output target requires a lot of
efforts.


\subsection{FPGA Caffe}

FPGA Caffe is a custom version of Caffe with \acrshort{fpga} kernels. The kernels use custom-precision floating-point
arithmetic to save area and improve the throughput of the kernels, while also allowing for experimentation with
different floating-point precisions and rounding for training and inference with \acrshortpl{cnn} \cite{fpga-caffe}.

\vspace{5pt} \textbf{Maintenance and Update frequency} \vspace{2pt} \\
FPGA Caffe is maintained by a team of six developers who are part of University of Toronto and University of Guelph,
both based in Ontario, Canada.

The project has not been updated since December 2017, making it quite obsolete.

\vspace{5pt} \textbf{License} \vspace{2pt} \\
FPGA Caffe is released under the 2-Clause BSD License \cite{license-bsd2}, which allows almost unlimited freedom with
the software as long as the modified versions of the software include the same license.

\vspace{5pt} \textbf{Input} \vspace{2pt} \\
The programmer specifies the \acrlong{dnn} using Caffe format.

\vspace{5pt} \textbf{Output} \vspace{2pt} \\
The kernels target the Xilinx SDAccel OpenCL environment, thus only Xilinx \acrshortpl{fpga} are supported.

\vspace{5pt} \textbf{Remarks} \vspace{2pt} \\
Only a few layers are implemented. There are forward and backward convolutions, forward and backward \acrshortpl{relu},
forward and backward MaxPooling, and forward and backward InnerProduct.


\subsection{NVDLA}

The \acrfull{nvdla} is a free and open architecture that promotes a standard way to design \acrlong{dl} inference
accelerators \cite{nvdla}. With its modular architecture, \acrshort{nvdla} is scalable, highly configurable and designed
to simplify integration and portability. The hardware supports a wide range of IoT devices. According to their website,
all of the software, hardware and documentation \textit{will} be available on GitHub \cite{nvdla-github}.

\newpage \textbf{Maintenance and Update frequency} \vspace{2pt} \\
The project is maintained by an internal team so it's quite professional.

\acrshort{nvdla} is divided in two parts: software and hardware. \\
The hardware part (available under the \texttt{hw} Github repository) has not been updated since April 2018. \\
The software part (available under the \texttt{sw} Github repository) has multiple commits in September 2019 but nothing
between September 2019 and April 2019 as well as between April 2019 and August 2018 (at least publicly). \\
It seems that they commit changes only when a major development milestone is released.

\vspace{5pt} \textbf{License} \vspace{2pt} \\
\acrshort{nvdla} is delivered as an open-source project under the NVIDIA Open NVDLA License \cite{nvdla-license}. This
license allows us to modify the tool as we please.

\vspace{5pt} \textbf{Input} \vspace{2pt} \\
The programmer specifies the \acrlong{dnn} using Caffe format.

\vspace{5pt} \textbf{Output} \vspace{2pt} \\
\acrshort{nvdla} supports two sample platforms: simulation and \acrshort{fpga}. These platforms are provided to observe,
evaluate and test \acrshort{nvdla} in a minimal \acrfull{soc}. \\
The simulation platform is based on GreenSocs QBox \cite{greensocs}. A QEMU CPU model (\texttt{x86} or \texttt{ARMv8})
is combined with the \acrshort{nvdla} SystemC model to provide a register-accurate system for quick development and
debugging. \\
The \acrshort{fpga} platform provides a synthesizable example of instantiating \acrshort{nvdla} in a real design. The
\acrshort{fpga} model is intended for inference only, no effort has been made to optimize cycle time, design size, power
consumption or performance.

The \acrshort{fpga} platform is Xilinx-based, thus only Xilinx \acrshortpl{fpga} are supported.

\vspace{5pt} \textbf{Remarks} \vspace{2pt} \\
The documentation is well-structured and precise.

The source code is quite closed yet. Commits are pushed whenever there is a major version. And even if NVIDIA claims
that the project is open-source, it seems that some code (\textit{e.g.} the compiler) will not be released. This already
causes problems because, according to Toshiba \cite{toshiba}, some errors are raised during compilation, and we don't
have information on what is making compilation fail. \\
However, \acrfull{onnc} \cite{onnc}, a retargetable compilation framework, has a \acrshort{nvdla} backend that can
compile a model into an executable \acrshort{nvdla} loadable file.

It also seems that people are having a hard time testing \acrshort{nvdla} on \acrshortpl{fpga}
\cite{nvdla-pull-request}.

The team seems to be quite responsive: we wrote them an email to get more information about the compiler source code and
they replied within hours.


\subsection{hls4ml}

hls4ml is a package for \acrlong{ml} inference in \acrshortpl{fpga}. It translates traditional open-source \acrlong{ml}
models into \acrfull{hls} language that can be configured according to the output platform \cite{hls4ml}.

\vspace{5pt} \textbf{Maintenance and Update frequency} \vspace{2pt} \\
hls4ml is maintained by different people around the world. Some come from the CERN, others from MIT, making it more of a
community project than something professional.

The project was updated quite frequently, about 1 commit was made per week until September 2019. Since, there has been
no commit made. Project might be discontinued.

\vspace{5pt} \textbf{License} \vspace{2pt} \\
hls4ml is licensed under the Apache License 2.0 \cite{license-apache2}.

\newpage \textbf{Input} \vspace{2pt} \\
\acrlong{nn} can be specified with Keras/Tensorflow or PyTorch.

\vspace{5pt} \textbf{Output} \vspace{2pt} \\
Given the input, the framework generates an \acrshort{hls} project that can be used to produce an IP core which can be
plugged into more complex designs or be used to create a kernel for \acrshort{cpu} co-processing.

As of January 2020, only \acrfull{mlp} and \texttt{Conv1D} and \texttt{Conv2D} architectures are supported.

\vspace{5pt} \textbf{Remarks} \vspace{2pt} \\
Unfortunately, the project is not well-documented.


\subsection{TVM}

TVM is an open deep learning compiler stack for \acrshortpl{cpu}, \acrshortpl{gpu} and specialized accelerators
(\acrshortpl{fpga}). It aims to close the gap between the productivity-focused deep learning frameworks, and the
performance- or efficiency-oriented hardware backends \cite{tvm}.

\vspace{5pt} \textbf{Maintenance and Update frequency} \vspace{2pt} \\
The TVM stack began as research project at the SAMPL group of University of Washington. The project is now driven by an
open-source community involving multiple industries and academic institutions.

The project is under active development: several commits are pushed every day.

\vspace{5pt} \textbf{License} \vspace{2pt} \\
The TVM stack is licensed under the Apache License 2.0 \cite{license-apache2}.

\vspace{5pt} \textbf{Input} \vspace{2pt} \\
\acrlong{nn} can be specified in Keras, MXNet, PyTorch, Tensorflow, CoreML and DarkNet.

\vspace{5pt} \textbf{Output} \vspace{2pt} \\
Given the input, TVM compiles the \acrfull{dl} models into deployable modules on diverse hardware backends such as
\acrshortpl{cpu}, \acrshortpl{gpu} and \acrshortpl{fpga}.

\vspace{5pt} \textbf{Remarks} \vspace{2pt} \\
The documentation is really exhaustive and up-to-date.

A lot of tutorials are available for every thing that TVM can achieve.

A forum is available where users can ask questions which are then rapidly answered.

\acrfull{vta} is an extension of the TVM framework that includes drivers, a \acrfull{jit} runtime and an optimizing
compiler stack. \acrshort{vta} offers a micro-architecture on which compiled TVM modules can be run. Initially, only
Xilinx \acrshortpl{fpga} were supported. However, a pull request adding Intel \acrshort{fpga} and Chisel support has
been merged on June 5, 2019 \cite{tvm-intel-pr}.

TVM is now part of the Apache Incubator \cite{apache-incubator}. In December 2019, the \citetitle{tvm-conference}
\cite{tvm-conference} has been organized by Apache. A lot of talkings (\eg Microsoft, ARM, Xilinx) are worth a look. We
believe that such an event promise a bright future for this project.

This framework has already been used in the past so we are quite comfortable with its use.


\subsection{LeFlow}

LeFlow is a tool that relies on LegUp \cite{legup} to map numerical computation models written in Tensorflow to
synthesizable hardware \cite{leflow}. It bridges Google's XLA compiler and LegUp high-level synthesis to automatically
generate a SystemVerilog module.

\newpage \textbf{Maintenance and Update frequency} \vspace{2pt} \\
LeFlow is maintained by three people who work at The University of British Columbia.

Even if the last update was made in February 2019, the previous one was four months before, thus we assume that it is
not frequently updated.

\vspace{5pt} \textbf{License} \vspace{2pt} \\
LeFlow is licensed under the MIT License \cite{license-mit} which has only one restriction: if the project is reused
within proprietary software, all copies of the licensed software must include a copy of the MIT License.

\vspace{5pt} \textbf{Input} \vspace{2pt} \\
\acrlong{nn} models must be specified with a customized version of Tensorflow.

\vspace{5pt} \textbf{Output} \vspace{2pt} \\
Since LeFlow relies on LegUp, it supports the output that LegUp supports. Those are Intel \acrshortpl{fpga}.

\vspace{5pt} \textbf{Remarks} \vspace{2pt} \\
Documentation is inexistent.

Some simple examples are included in the repository.

The maintainer is quite responsive: we wrote him an email to better understand the project's structure and he replied
within hours.


\subsection{nGraph}

nGraph is an open-source graph compiler for artificial \acrlongpl{nn}. The nGraph Compiler stack provides an inherently
efficient graph-based compilation infrastructure designed to be compatible with many upcoming integrated circuits while
also unlocking a massive performance boost on any existing hardware targets \cite{ngraph}.

\vspace{5pt} \textbf{Maintenance and Update frequency} \vspace{2pt} \\
nGraph is maintained by an artificial intelligence software company called Nervana Systems (acquired by Intel in August
2016) \cite{wiki-nervanasys}.

The Github repository is updated with several commits every day \cite{github-ngraph}.

\vspace{5pt} \textbf{License} \vspace{2pt} \\
nGraph is licensed under the Apache License 2.0 \cite{license-apache2}.

\vspace{5pt} \textbf{Input} \vspace{2pt} \\
As of January 2020, nGraph takes Tensorflow 1.12, MXNet 1.3 and ONNX 1.3 as inputs for model description.

\vspace{5pt} \textbf{Output} \vspace{2pt} \\
nGraph currently supports Intel \acrshortpl{cpu}, Intel Neural Network Processor, NVIDIA CUDA \acrshortpl{gpu} and AMD
\acrshortpl{gpu} as output.

\acrshortpl{fpga} are set to be fully supported "in the near future" \cite{ngraph-backends}.

\vspace{5pt} \textbf{Remarks} \vspace{2pt} \\
The documentation seems to be very qualitative.

Since the project is maintained by an Intel company, it might be possible that only Intel hardware (\acrshortpl{cpu},
\acrshortpl{gpu} and \acrshort{fpga}) will be supported.


\newpage
\subsection{HeteroCL}

HeteroCL is a programming infrastructure composed of a Python-based \acrfull{dsl} and a compilation flow. The HeteroCL
\acrshort{dsl} provides a clean abstraction that decouples algorithm specification from three important types of
hardware customization in compute, data types and memory architectures.

\vspace{5pt} \textbf{Maintenance and Update frequency} \vspace{2pt} \\
The project is maintained by a team of developers of the Cornell University.

Commits are pushed frequently on the HeteroCL Github repository \cite{github-heterocl}.

\vspace{5pt} \textbf{License} \vspace{2pt} \\
HeteroCL is licensed under the Apache License 2.0 \cite{license-apache2}.

\vspace{5pt} \textbf{Input} \vspace{2pt} \\
HeteroCL takes any format as input as long as weights are loadable from the input model.

\vspace{5pt} \textbf{Output} \vspace{2pt} \\
Cloud (AWS), Xilinx and Intel \acrshortpl{fpga} are supported. \acrshortpl{cpu} are also supported.

\vspace{5pt} \textbf{Remarks} \vspace{2pt} \\
The documentation seems to be good although not complete. We assume it will improve with future releases.

It is possible to follow development roadmap by going on their respective pull request on the HeteroCL Github repository
\cite{github-heterocl}.

We already used this framework in the past so we are comfortable with its use.


\subsection{ESP}

\acrfull{esp} is an open-source platform for heterogeneous \acrfull{soc} design and prototype on \acrshort{fpga}. It
provides a flexible tile-based architecture built on a multi-plane \acrfull{noc} \cite{esp-platform}. \\
In addition to the architecture, \acrshort{esp} provides users with templates and scripts to create new accelerators
from SystemC, Chisel, and C. The \acrshort{esp} design methodology eases the integration process by offering platform
services (\ie \acrfull{dma}, distributed interrupt, run-time coherence selection) that hide the complexity of hardware
and software integration from the accelerator designer.

\vspace{5pt} \textbf{Maintenance and Update frequency} \vspace{2pt} \\
\acrshort{esp} is maintained by the System-Level Design group at Columbia University, led by Professor Luca P. Carloni.

Commits are pushed frequently on the project's Github repository \cite{esp-github}.

\vspace{5pt} \textbf{License} \vspace{2pt} \\
The project is licensed under the Apache License 2.0 \cite{license-apache2}.

\vspace{5pt} \textbf{Input} \vspace{2pt} \\
\acrshort{esp} takes SystemC, C and Keras Tensorflow as input.

\vspace{5pt} \textbf{Output} \vspace{2pt} \\
It generates \acrshort{fpga} accelerators as modules for the ESP micro-architecture, like TVM does.

\vspace{5pt} \textbf{Remarks} \vspace{2pt} \\
The documentation is not yet complete. A lot of tutorials are still missing, making it difficult to use this project.
However, since the project seems to be quite active, we believe documentation will quickly be completed.

This framework has been put online at the beginning of January 2020, thus being too recent to be added to the list of
potential candidates for this project.


\subsection{Comparison}

Among the listed existing frameworks, we based ourselves on the previously presented criteria to choose the most
suitable candidate for this project.

Among these criteria, we decided to put a higher weight on the update frequency, the supported outputs and
their reliability.

The selection will be based on several criteria: we want to focus on the update frequency and the supported outputs. The
\textit{License} criterion has been put aside because every framework is under a license allowing a lot of freedom
(except maybe for \acrshort{nvdla}). \\
Also, the \textit{Supported inputs} criterion has not been taken into account since input frameworks were found very
similar and switching from one to another is quite feasible.

\textbf{Supported outputs}  \vspace{2pt}    \\
All frameworks listed above support \acrshortpl{fpga} as an output, except for nGraph. Thus, we eliminated it from the
list of interesting candidates.

\textbf{Maintenance and update frequency}   \vspace{2pt}    \\
FPGA Caffe has not been updated for more than a year, so it was obviously not an interesting candidate. Unfortunately,
we could not retain \acrshort{esp} as a candidate either, because it has been released just days before the end of this
work. But it might be a truly interesting solution to keep track of in the future.

\textbf{Reliability}    \vspace{2pt}    \\
Because its compiler is still close-source and contains bugs, \acrshort{nvdla} didn't make it to the list of interesting
candidates. \\
DnnWeaver 2.0 seems to be broken: some files need manual updates to be compatible with \textit{Python3.6} so it has been
pulled out of the list as well. \\
In one of our previous works, \citetitle{elisei-pa} \cite{elisei-pa}, we tried to use LeFlow to synthesize a simple
\acrlong{mlp} without any success. Our previous tests were not very conclusive, thus we preferred to rule out LeFlow as
a possible candidate.

\textbf{Produced output}    \vspace{2pt}    \\
This lefts us with three candidates: hls4ml, TVM and HeteroCL. hls4ml and HeteroCL are similar in the way that they
produce \acrshort{hls} code that can be synthesized for both Intel and Xilinx \acrshortpl{fpga}, while TVM produces
modules that must be run on the \acrshort{vta} micro-architecture. This creates two groups: \textit{\acrshort{hls}} and
\textit{Micro-architecture}. \\
The only and thus best candidate for the \textit{Micro-architecture} group is TVM. The other two candidates are part of
the \textit{\acrshort{hls} code} group. Between HeteroCL and hls4ml, we believe that HeteroCL is the most suitable
choice because it is well-documented and it is more frequently updated than hls4ml. \\
Moreover, HeteroCL has already been used in the \citetitle{sdr-makerspace} \cite{sdr-makerspace} project, so we also are
more comfortable using it.

Finally, between TVM and HeteroCL, we preferred to go with HeteroCL. Since we aim at creating an FPGA-based system, a
synthesizable module is preferred over a module that can only run with a dedicated micro-architecture, thus eliminating
TVM from the possible candidates.

The choice of HeteroCL as our chosen framework being made, we were able to move on to the second objective of this
paper: Define, train and test a \acrlong{ml} model.

% Summary table
\input{sections/frameworks_summary.tex}