-------------------------------------------------------------------------------
-- File: axi_perf.vhd
--
-- Created by: Lucas Elisei <lucas.elisei@master.hes-so.ch>
-- Created on: 25.09.2019
--
-- Simple VHDL block to measure AXI4Lite interface throughput.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity axi4lite_perf is
    generic (
        -- Internal counters width (thus also AXI4Lite MM data width)
        COUNTER_WIDTH       : integer   := 32;
        -- Width of AXI4Lite MM address bus
        AXI_ADDR_WIDTH      : integer   := 4
    );
    port (
        -- Clock and Reset inputs
        ap_clk_i            :  in std_logic;
        ap_reset_n_i        :  in std_logic;

        -- AXI4Lite Slave interface
        slave_awaddr_i      :  in std_logic_vector(13 downto 0);
        slave_awvalid_i     :  in std_logic;
        slave_awready_o     : out std_logic;

        slave_wdata_i       :  in std_logic_vector(31 downto 0);
        slave_wstrb_i       :  in std_logic_vector( 3 downto 0);
        slave_wvalid_i      :  in std_logic;
        slave_wready_o      : out std_logic;

        slave_bresp_o       : out std_logic_vector( 1 downto 0);
        slave_bvalid_o      : out std_logic;
        slave_bready_i      :  in std_logic;

        slave_araddr_i      :  in std_logic_vector(13 downto 0);
        slave_arvalid_i     :  in std_logic;
        slave_arready_o     : out std_logic;

        slave_rdata_o       : out std_logic_vector(31 downto 0);
        slave_rresp_o       : out std_logic_vector( 1 downto 0);
        slave_rvalid_o      : out std_logic;
        slave_rready_i      :  in std_logic;

        -- Clock and Reset outputs
        ap_clk_o            : out std_logic;
        ap_reset_n_o        : out std_logic;

        -- AXI4Lite Master inferface
        master_awaddr_o     : out std_logic_vector(13 downto 0);
        master_awvalid_o    : out std_logic;
        master_awready_i    :  in std_logic;

        master_wdata_o      : out std_logic_vector(31 downto 0);
        master_wstrb_o      : out std_logic_vector( 3 downto 0);
        master_wvalid_o     : out std_logic;
        master_wready_i     :  in std_logic;

        master_bresp_i      :  in std_logic_vector( 1 downto 0);
        master_bvalid_i     :  in std_logic;
        master_bready_o     : out std_logic;

        master_araddr_o     : out std_logic_vector(13 downto 0);
        master_arvalid_o    : out std_logic;
        master_arready_i    :  in std_logic;

        master_rdata_i      :  in std_logic_vector(31 downto 0);
        master_rresp_i      :  in std_logic_vector( 1 downto 0);
        master_rvalid_i     :  in std_logic;
        master_rready_o     : out std_logic

        -- AXI4Lite Memory-Mapped interface
        -- Clock and Reset
        axi_clk_i           :  in std_logic;
        axi_reset_i         :  in std_logic;
        -- Read address channel
        axi_araddr_i        :  in std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
        axi_arprot_i        :  in std_logic_vector(2 downto 0);
        axi_arvalid_i       :  in std_logic;
        axi_arready_i       : out std_logic;
        -- Read data channel
        axi_rdata_o         : out std_logic_vector(COUNTER_WIDTH-1 downto 0);
        axi_rresp_o         : out std_logic_vector(1 downto 0);
        axi_rvalid_o        : out std_logic;
        axi_rready_i        :  in std_logic
    );
end entity axi4lite_perf;

architecture arch of axi4lite_perf is
    -- Counters
    signal ap_clk_counter_s : unsigned(COUNTER_WIDTH-1 downto 0)    := (others => '0');
    signal read_counter_s   : unsigned(COUNTER_WIDTH-1 downto 0)    := (others => '0');
    signal write_counter_s  : unsigned(COUNTER_WIDTH-1 downto 0)    := (others => '0');

    -- Constant (for testing purpose)
    constant AXI_CST        : integer   := 16#1234#;

    -- AXI4Lite MM interface signals
    signal axi_read_en_s    : std_logic;
    -- Read address channel
    signal axi_araddr_s     : std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
    signal axi_arready_s    : std_logic;
    -- Read data channel
    signal axi_rdata_s      : std_logic_vector(COUNTER_WIDTH-1 downto 0);
    signal axi_rvalid_s     : std_logic;
    signal axi_rresp_s      : std_logic_vector(1 downto 0);

begin

    -- Connect Clock and Reset signals
    ap_clk_o            <= ap_clk_i;
    ap_reset_n_o        <= ap_reset_n_i;

    -- Connect Master and Slave signals
    master_awaddr_o     <= slave_awaddr_i;
    master_awvalid_o    <= slave_awvalid_i;
    master_wdata_o      <= slave_wdata_i;
    master_wstrb_o      <= slave_wstrb_i;
    master_wvalid_o     <= slave_wvalid_i;
    master_bready_o     <= slave_bready_i;
    master_araddr_o     <= slave_araddr_i;
    master_arvalid_o    <= slave_arvalid_i;
    master_rready_o     <= slave_rready_i;

    slave_awready_o     <= master_awready_i;
    slave_wready_o      <= master_wready_i;
    slave_bresp_o       <= master_bresp_i;
    slave_bvalid_o      <= master_bvalid_i;
    slave_arready_o     <= master_arready_i;
    slave_rdata_o       <= master_rdata_i;
    slave_rresp_o       <= master_rresp_i;
    slave_rvalid_o      <= master_rvalid_i;

    -- Clock counter process
    ap_clk_counter_p: process(ap_reset_n_i, ap_clk_i)
    begin
        -- Asynchronous reset (active low)
        if (ap_reset_n_i = '0') then
            ap_clk_counter_s <= (others => '0');
        -- Increase counter on each clock tick
        elsif (rising_edge(ap_clk_i)) then
            ap_clk_counter_s <= ap_clk_counter_s + 1;
        end if;
    end process ap_clk_counter_p;

    -- Read counter process
    read_counter_p: process(ap_reset_n_i, ap_clk_i, master_rvalid_i, slave_rready_i)
    begin
        -- Asynchronous reset (active low)
        if (ap_reset_n_i = '0') then
            read_counter_s <= (others => '0');
        -- Increase counter on each reading
        elsif (rising_edge(ap_clk_i)) then
            if (master_rvalid_i = '1' and slave_rready_i = '1') then
                read_counter_s <= read_counter_s + 1;
            end if;
        end if;
    end process read_counter_p;

    -- Write counter process
    write_counter_p: process(ap_reset_n_i, ap_clk_i, master_wready_i, slave_wvalid_i)
    begin
        -- Asynchronous reset (active low)
        if (ap_reset_n_i = '0') then
            write_counter_s <= (others => '0');
        -- Increase counter on each writing
        elsif (rising_edge(ap_clk_i)) then
            if (master_wready_i = '1' and slave_wvalid_i = '1') then
                write_counter_s <= write_counter_s + 1;
            end if;
        end if;
    end process write_counter_p;

    -- AXI4Lite MM Read Address Channel
    axi_ar_p: process(axi_reset_i, axi_clk_i)
    begin
        -- Asynchronous reset (active high)
        if (axi_reset_i = '1') then
            axi_arready_s   <= '0';
            axi_araddr_s    <= (others => '1');
        elsif (rising_edge(axi_clk_i)) then
            if (axi_arready_s = '0' and axi_arvalid_i = '1') then
                axi_arready_s   <= '1';
                axi_araddr_s    <= axi_araddr_i;
            else
                axi_arready_s   <= '0';
            end if;
        end if;
    end process axi_ar_p;
    axi_arready_o   <= axi_arready_s;

    -- AXI4Lite MM Read Data Channel
    axi_read_en_s <= axi_arready_s and axi_arvalid_i and (not axi_rvalid_s);
    axi_r_p: process(axi_reset_i, axi_clk_i)
    begin
        -- Asynchronous reset (active high)
        if (axi_reset_i = '1') then
            axi_rdata_s <= (others => '0');
        elsif (rising_edge(axi_clk_i)) then
            if (axi_read_en_s <= '1') then
                axi_rdata_s <= std_logic_vector(to_unsigned(AXI_CST, axi_rdata_s'length));
            end if;
        end if;
    end process axi_r_p;
    axi_rdata_o <= axi_rdata_s;

    axi_r_sig_p: process(axi_reset_i, axi_clk_i)
    begin
        -- Asynchronous reset (active high)
        if (axi_reset_i = '1') then
            axi_rvalid_s    <= '0';
            axi_rresp_s     <= "00";
        elsif (rising_edge(axi_clk_i)) then
            if (axi_arready_s = '1' and axi_rready_i = '1' and axi_rvalid_s = '0') then
                axi_rvalid_s    <= '1';
                axi_rresp_s     <= "00";
            elsif (axi_rready_i = '1' and axi_rvalid_s = '1') then
                axi_rvalid_s    <= '0';
            end if;
        end if;
    end process axi_r_sig_p;
    axi_rvalid_o    <= axi_rvalid_s;
    axi_rresp_o     <= axi_rresp_s;

end architecture arch;