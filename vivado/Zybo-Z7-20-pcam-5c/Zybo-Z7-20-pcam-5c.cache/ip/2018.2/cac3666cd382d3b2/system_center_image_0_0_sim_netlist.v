// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Tue Dec  3 13:57:12 2019
// Host        : A23B-PC10 running 64-bit Ubuntu 16.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_center_image_0_0_sim_netlist.v
// Design      : system_center_image_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_center_image
   (ap_clk,
    ap_rst_n,
    ap_start,
    ap_done,
    ap_idle,
    ap_ready,
    image_in_TDATA,
    image_in_TVALID,
    image_in_TREADY,
    image_in_TKEEP,
    image_in_TSTRB,
    image_in_TUSER,
    image_in_TLAST,
    image_in_TID,
    image_in_TDEST,
    image_out_TDATA,
    image_out_TVALID,
    image_out_TREADY,
    image_out_TKEEP,
    image_out_TSTRB,
    image_out_TUSER,
    image_out_TLAST,
    image_out_TID,
    image_out_TDEST,
    crop_out_TDATA,
    crop_out_TVALID,
    crop_out_TREADY,
    crop_out_TKEEP,
    crop_out_TSTRB,
    crop_out_TUSER,
    crop_out_TLAST,
    crop_out_TID,
    crop_out_TDEST);
  input ap_clk;
  input ap_rst_n;
  input ap_start;
  output ap_done;
  output ap_idle;
  output ap_ready;
  input [23:0]image_in_TDATA;
  input image_in_TVALID;
  output image_in_TREADY;
  input [2:0]image_in_TKEEP;
  input [2:0]image_in_TSTRB;
  input [0:0]image_in_TUSER;
  input [0:0]image_in_TLAST;
  input [0:0]image_in_TID;
  input [0:0]image_in_TDEST;
  output [23:0]image_out_TDATA;
  output image_out_TVALID;
  input image_out_TREADY;
  output [2:0]image_out_TKEEP;
  output [2:0]image_out_TSTRB;
  output [0:0]image_out_TUSER;
  output [0:0]image_out_TLAST;
  output [0:0]image_out_TID;
  output [0:0]image_out_TDEST;
  output [23:0]crop_out_TDATA;
  output crop_out_TVALID;
  input crop_out_TREADY;
  output [2:0]crop_out_TKEEP;
  output [2:0]crop_out_TSTRB;
  output [0:0]crop_out_TUSER;
  output [0:0]crop_out_TLAST;
  output [0:0]crop_out_TID;
  output [0:0]crop_out_TDEST;

  wire \ap_CS_fsm[8]_i_3_n_0 ;
  wire \ap_CS_fsm[8]_i_4_n_0 ;
  wire \ap_CS_fsm_reg_n_0_[0] ;
  wire ap_CS_fsm_state10;
  wire ap_CS_fsm_state11;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state4;
  wire ap_CS_fsm_state5;
  wire ap_CS_fsm_state6;
  wire ap_CS_fsm_state7;
  wire ap_CS_fsm_state8;
  wire ap_CS_fsm_state9;
  wire [10:0]ap_NS_fsm;
  wire ap_NS_fsm1;
  wire ap_NS_fsm1105_out;
  wire ap_NS_fsm1107_out;
  wire ap_NS_fsm1109_out;
  wire ap_NS_fsm1110_out;
  wire ap_NS_fsm1112_out;
  wire ap_clk;
  wire ap_done;
  wire ap_idle;
  wire ap_ready_INST_0_i_3_n_0;
  wire ap_ready_INST_0_i_4_n_0;
  wire ap_ready_INST_0_i_5_n_0;
  wire ap_ready_INST_0_i_6_n_0;
  wire ap_ready_INST_0_i_7_n_0;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire ap_start;
  wire [23:0]crop_out_TDATA;
  wire [0:0]crop_out_TDEST;
  wire [0:0]crop_out_TID;
  wire [2:0]crop_out_TKEEP;
  wire [0:0]crop_out_TLAST;
  wire crop_out_TREADY;
  wire [2:0]crop_out_TSTRB;
  wire [0:0]crop_out_TUSER;
  wire crop_out_TVALID;
  wire crop_out_V_data_V_1_ack_in;
  wire crop_out_V_data_V_1_load_A;
  wire crop_out_V_data_V_1_load_B;
  wire [23:0]crop_out_V_data_V_1_payload_A;
  wire [23:0]crop_out_V_data_V_1_payload_B;
  wire crop_out_V_data_V_1_sel;
  wire crop_out_V_data_V_1_sel_rd_i_1_n_0;
  wire crop_out_V_data_V_1_sel_wr;
  wire crop_out_V_data_V_1_sel_wr_i_1_n_0;
  wire [1:1]crop_out_V_data_V_1_state;
  wire \crop_out_V_data_V_1_state[0]_i_1_n_0 ;
  wire \crop_out_V_data_V_1_state_reg_n_0_[0] ;
  wire crop_out_V_dest_V_1_ack_in;
  wire crop_out_V_dest_V_1_payload_A;
  wire \crop_out_V_dest_V_1_payload_A[0]_i_1_n_0 ;
  wire crop_out_V_dest_V_1_payload_B;
  wire \crop_out_V_dest_V_1_payload_B[0]_i_1_n_0 ;
  wire crop_out_V_dest_V_1_sel;
  wire crop_out_V_dest_V_1_sel_rd_i_1_n_0;
  wire crop_out_V_dest_V_1_sel_wr;
  wire crop_out_V_dest_V_1_sel_wr_i_1_n_0;
  wire [1:1]crop_out_V_dest_V_1_state;
  wire \crop_out_V_dest_V_1_state[0]_i_1_n_0 ;
  wire crop_out_V_id_V_1_ack_in;
  wire crop_out_V_id_V_1_payload_A;
  wire \crop_out_V_id_V_1_payload_A[0]_i_1_n_0 ;
  wire crop_out_V_id_V_1_payload_B;
  wire \crop_out_V_id_V_1_payload_B[0]_i_1_n_0 ;
  wire crop_out_V_id_V_1_sel;
  wire crop_out_V_id_V_1_sel_rd_i_1_n_0;
  wire crop_out_V_id_V_1_sel_wr;
  wire crop_out_V_id_V_1_sel_wr_i_1_n_0;
  wire [1:1]crop_out_V_id_V_1_state;
  wire \crop_out_V_id_V_1_state[0]_i_1_n_0 ;
  wire \crop_out_V_id_V_1_state_reg_n_0_[0] ;
  wire crop_out_V_keep_V_1_ack_in;
  wire [2:0]crop_out_V_keep_V_1_payload_A;
  wire \crop_out_V_keep_V_1_payload_A[0]_i_1_n_0 ;
  wire \crop_out_V_keep_V_1_payload_A[1]_i_1_n_0 ;
  wire \crop_out_V_keep_V_1_payload_A[2]_i_1_n_0 ;
  wire [2:0]crop_out_V_keep_V_1_payload_B;
  wire \crop_out_V_keep_V_1_payload_B[0]_i_1_n_0 ;
  wire \crop_out_V_keep_V_1_payload_B[1]_i_1_n_0 ;
  wire \crop_out_V_keep_V_1_payload_B[2]_i_1_n_0 ;
  wire crop_out_V_keep_V_1_sel;
  wire crop_out_V_keep_V_1_sel_rd_i_1_n_0;
  wire crop_out_V_keep_V_1_sel_wr;
  wire crop_out_V_keep_V_1_sel_wr_i_1_n_0;
  wire [1:1]crop_out_V_keep_V_1_state;
  wire \crop_out_V_keep_V_1_state[0]_i_1_n_0 ;
  wire \crop_out_V_keep_V_1_state_reg_n_0_[0] ;
  wire crop_out_V_last_V_1_ack_in;
  wire crop_out_V_last_V_1_payload_A;
  wire \crop_out_V_last_V_1_payload_A[0]_i_1_n_0 ;
  wire crop_out_V_last_V_1_payload_B;
  wire \crop_out_V_last_V_1_payload_B[0]_i_1_n_0 ;
  wire crop_out_V_last_V_1_sel;
  wire crop_out_V_last_V_1_sel_rd_i_1_n_0;
  wire crop_out_V_last_V_1_sel_wr;
  wire crop_out_V_last_V_1_sel_wr_i_1_n_0;
  wire [1:1]crop_out_V_last_V_1_state;
  wire \crop_out_V_last_V_1_state[0]_i_1_n_0 ;
  wire \crop_out_V_last_V_1_state_reg_n_0_[0] ;
  wire crop_out_V_strb_V_1_ack_in;
  wire [2:0]crop_out_V_strb_V_1_payload_A;
  wire \crop_out_V_strb_V_1_payload_A[0]_i_1_n_0 ;
  wire \crop_out_V_strb_V_1_payload_A[1]_i_1_n_0 ;
  wire \crop_out_V_strb_V_1_payload_A[2]_i_1_n_0 ;
  wire [2:0]crop_out_V_strb_V_1_payload_B;
  wire \crop_out_V_strb_V_1_payload_B[0]_i_1_n_0 ;
  wire \crop_out_V_strb_V_1_payload_B[1]_i_1_n_0 ;
  wire \crop_out_V_strb_V_1_payload_B[2]_i_1_n_0 ;
  wire crop_out_V_strb_V_1_sel;
  wire crop_out_V_strb_V_1_sel_rd_i_1_n_0;
  wire crop_out_V_strb_V_1_sel_wr;
  wire crop_out_V_strb_V_1_sel_wr_i_1_n_0;
  wire [1:1]crop_out_V_strb_V_1_state;
  wire \crop_out_V_strb_V_1_state[0]_i_1_n_0 ;
  wire \crop_out_V_strb_V_1_state_reg_n_0_[0] ;
  wire crop_out_V_user_V_1_ack_in;
  wire crop_out_V_user_V_1_payload_A;
  wire \crop_out_V_user_V_1_payload_A[0]_i_1_n_0 ;
  wire crop_out_V_user_V_1_payload_B;
  wire \crop_out_V_user_V_1_payload_B[0]_i_1_n_0 ;
  wire crop_out_V_user_V_1_sel;
  wire crop_out_V_user_V_1_sel_rd_i_1_n_0;
  wire crop_out_V_user_V_1_sel_wr;
  wire crop_out_V_user_V_1_sel_wr_i_1_n_0;
  wire [1:1]crop_out_V_user_V_1_state;
  wire \crop_out_V_user_V_1_state[0]_i_1_n_0 ;
  wire \crop_out_V_user_V_1_state_reg_n_0_[0] ;
  wire [10:0]i_V_fu_278_p2;
  wire [10:0]i_V_reg_352;
  wire i_V_reg_3520;
  wire \i_V_reg_352[10]_i_2_n_0 ;
  wire \i_V_reg_352[7]_i_2_n_0 ;
  wire [23:0]image_in_TDATA;
  wire [0:0]image_in_TDEST;
  wire [0:0]image_in_TID;
  wire [2:0]image_in_TKEEP;
  wire [0:0]image_in_TLAST;
  wire image_in_TREADY;
  wire [2:0]image_in_TSTRB;
  wire [0:0]image_in_TUSER;
  wire image_in_TVALID;
  wire image_in_V_data_V_0_ack_in;
  wire image_in_V_data_V_0_ack_out;
  wire [23:0]image_in_V_data_V_0_data_out;
  wire image_in_V_data_V_0_load_A;
  wire image_in_V_data_V_0_load_B;
  wire [23:0]image_in_V_data_V_0_payload_A;
  wire [23:0]image_in_V_data_V_0_payload_B;
  wire image_in_V_data_V_0_sel;
  wire image_in_V_data_V_0_sel_rd_i_1_n_0;
  wire image_in_V_data_V_0_sel_wr;
  wire image_in_V_data_V_0_sel_wr_i_1_n_0;
  wire [1:1]image_in_V_data_V_0_state;
  wire \image_in_V_data_V_0_state[0]_i_1_n_0 ;
  wire \image_in_V_data_V_0_state_reg_n_0_[0] ;
  wire image_in_V_dest_V_0_data_out;
  wire image_in_V_dest_V_0_payload_A;
  wire \image_in_V_dest_V_0_payload_A[0]_i_1_n_0 ;
  wire image_in_V_dest_V_0_payload_B;
  wire \image_in_V_dest_V_0_payload_B[0]_i_1_n_0 ;
  wire image_in_V_dest_V_0_sel;
  wire image_in_V_dest_V_0_sel_rd_i_1_n_0;
  wire image_in_V_dest_V_0_sel_wr;
  wire image_in_V_dest_V_0_sel_wr_i_1_n_0;
  wire [1:1]image_in_V_dest_V_0_state;
  wire \image_in_V_dest_V_0_state[0]_i_1_n_0 ;
  wire \image_in_V_dest_V_0_state_reg_n_0_[0] ;
  wire image_in_V_id_V_0_ack_in;
  wire image_in_V_id_V_0_data_out;
  wire image_in_V_id_V_0_payload_A;
  wire \image_in_V_id_V_0_payload_A[0]_i_1_n_0 ;
  wire image_in_V_id_V_0_payload_B;
  wire \image_in_V_id_V_0_payload_B[0]_i_1_n_0 ;
  wire image_in_V_id_V_0_sel;
  wire image_in_V_id_V_0_sel_rd_i_1_n_0;
  wire image_in_V_id_V_0_sel_wr;
  wire image_in_V_id_V_0_sel_wr_i_1_n_0;
  wire [1:1]image_in_V_id_V_0_state;
  wire \image_in_V_id_V_0_state[0]_i_1_n_0 ;
  wire \image_in_V_id_V_0_state_reg_n_0_[0] ;
  wire image_in_V_keep_V_0_ack_in;
  wire [2:0]image_in_V_keep_V_0_data_out;
  wire [2:0]image_in_V_keep_V_0_payload_A;
  wire \image_in_V_keep_V_0_payload_A[0]_i_1_n_0 ;
  wire \image_in_V_keep_V_0_payload_A[1]_i_1_n_0 ;
  wire \image_in_V_keep_V_0_payload_A[2]_i_1_n_0 ;
  wire [2:0]image_in_V_keep_V_0_payload_B;
  wire \image_in_V_keep_V_0_payload_B[0]_i_1_n_0 ;
  wire \image_in_V_keep_V_0_payload_B[1]_i_1_n_0 ;
  wire \image_in_V_keep_V_0_payload_B[2]_i_1_n_0 ;
  wire image_in_V_keep_V_0_sel;
  wire image_in_V_keep_V_0_sel_rd_i_1_n_0;
  wire image_in_V_keep_V_0_sel_wr;
  wire image_in_V_keep_V_0_sel_wr_i_1_n_0;
  wire [1:1]image_in_V_keep_V_0_state;
  wire \image_in_V_keep_V_0_state[0]_i_1_n_0 ;
  wire \image_in_V_keep_V_0_state_reg_n_0_[0] ;
  wire image_in_V_last_V_0_ack_in;
  wire image_in_V_last_V_0_payload_A;
  wire \image_in_V_last_V_0_payload_A[0]_i_1_n_0 ;
  wire image_in_V_last_V_0_payload_B;
  wire \image_in_V_last_V_0_payload_B[0]_i_1_n_0 ;
  wire image_in_V_last_V_0_sel;
  wire image_in_V_last_V_0_sel_rd_i_1_n_0;
  wire image_in_V_last_V_0_sel_wr;
  wire image_in_V_last_V_0_sel_wr_i_1_n_0;
  wire [1:1]image_in_V_last_V_0_state;
  wire \image_in_V_last_V_0_state[0]_i_1_n_0 ;
  wire \image_in_V_last_V_0_state_reg_n_0_[0] ;
  wire image_in_V_strb_V_0_ack_in;
  wire [2:0]image_in_V_strb_V_0_data_out;
  wire [2:0]image_in_V_strb_V_0_payload_A;
  wire \image_in_V_strb_V_0_payload_A[0]_i_1_n_0 ;
  wire \image_in_V_strb_V_0_payload_A[1]_i_1_n_0 ;
  wire \image_in_V_strb_V_0_payload_A[2]_i_1_n_0 ;
  wire [2:0]image_in_V_strb_V_0_payload_B;
  wire \image_in_V_strb_V_0_payload_B[0]_i_1_n_0 ;
  wire \image_in_V_strb_V_0_payload_B[1]_i_1_n_0 ;
  wire \image_in_V_strb_V_0_payload_B[2]_i_1_n_0 ;
  wire image_in_V_strb_V_0_sel;
  wire image_in_V_strb_V_0_sel_rd_i_1_n_0;
  wire image_in_V_strb_V_0_sel_wr;
  wire image_in_V_strb_V_0_sel_wr_i_1_n_0;
  wire [1:1]image_in_V_strb_V_0_state;
  wire \image_in_V_strb_V_0_state[0]_i_1_n_0 ;
  wire \image_in_V_strb_V_0_state_reg_n_0_[0] ;
  wire image_in_V_user_V_0_ack_in;
  wire image_in_V_user_V_0_payload_A;
  wire \image_in_V_user_V_0_payload_A[0]_i_1_n_0 ;
  wire image_in_V_user_V_0_payload_B;
  wire \image_in_V_user_V_0_payload_B[0]_i_1_n_0 ;
  wire image_in_V_user_V_0_sel;
  wire image_in_V_user_V_0_sel_rd_i_1_n_0;
  wire image_in_V_user_V_0_sel_wr;
  wire image_in_V_user_V_0_sel_wr_i_1_n_0;
  wire [1:1]image_in_V_user_V_0_state;
  wire \image_in_V_user_V_0_state[0]_i_1_n_0 ;
  wire \image_in_V_user_V_0_state_reg_n_0_[0] ;
  wire [23:0]image_out_TDATA;
  wire [0:0]image_out_TDEST;
  wire [0:0]image_out_TID;
  wire [2:0]image_out_TKEEP;
  wire [0:0]image_out_TLAST;
  wire image_out_TREADY;
  wire [2:0]image_out_TSTRB;
  wire [0:0]image_out_TUSER;
  wire image_out_TVALID;
  wire image_out_V_data_V_1_ack_in;
  wire [23:0]image_out_V_data_V_1_data_in;
  wire image_out_V_data_V_1_load_A;
  wire image_out_V_data_V_1_load_B;
  wire [23:0]image_out_V_data_V_1_payload_A;
  wire [23:0]image_out_V_data_V_1_payload_B;
  wire image_out_V_data_V_1_sel;
  wire image_out_V_data_V_1_sel_rd_i_1_n_0;
  wire image_out_V_data_V_1_sel_wr;
  wire image_out_V_data_V_1_sel_wr_i_1_n_0;
  wire [1:1]image_out_V_data_V_1_state;
  wire \image_out_V_data_V_1_state[0]_i_1_n_0 ;
  wire \image_out_V_data_V_1_state_reg_n_0_[0] ;
  wire image_out_V_dest_V_1_ack_in;
  wire image_out_V_dest_V_1_payload_A;
  wire \image_out_V_dest_V_1_payload_A[0]_i_1_n_0 ;
  wire image_out_V_dest_V_1_payload_B;
  wire \image_out_V_dest_V_1_payload_B[0]_i_1_n_0 ;
  wire image_out_V_dest_V_1_sel;
  wire image_out_V_dest_V_1_sel_rd_i_1_n_0;
  wire image_out_V_dest_V_1_sel_wr;
  wire image_out_V_dest_V_1_sel_wr_i_1_n_0;
  wire [1:1]image_out_V_dest_V_1_state;
  wire \image_out_V_dest_V_1_state[0]_i_1_n_0 ;
  wire image_out_V_id_V_1_ack_in;
  wire image_out_V_id_V_1_payload_A;
  wire \image_out_V_id_V_1_payload_A[0]_i_1_n_0 ;
  wire image_out_V_id_V_1_payload_B;
  wire \image_out_V_id_V_1_payload_B[0]_i_1_n_0 ;
  wire image_out_V_id_V_1_sel;
  wire image_out_V_id_V_1_sel_rd_i_1_n_0;
  wire image_out_V_id_V_1_sel_wr;
  wire image_out_V_id_V_1_sel_wr_i_1_n_0;
  wire [1:1]image_out_V_id_V_1_state;
  wire \image_out_V_id_V_1_state[0]_i_1_n_0 ;
  wire \image_out_V_id_V_1_state_reg_n_0_[0] ;
  wire image_out_V_keep_V_1_ack_in;
  wire [2:0]image_out_V_keep_V_1_payload_A;
  wire \image_out_V_keep_V_1_payload_A[0]_i_1_n_0 ;
  wire \image_out_V_keep_V_1_payload_A[1]_i_1_n_0 ;
  wire \image_out_V_keep_V_1_payload_A[2]_i_1_n_0 ;
  wire [2:0]image_out_V_keep_V_1_payload_B;
  wire \image_out_V_keep_V_1_payload_B[0]_i_1_n_0 ;
  wire \image_out_V_keep_V_1_payload_B[1]_i_1_n_0 ;
  wire \image_out_V_keep_V_1_payload_B[2]_i_1_n_0 ;
  wire image_out_V_keep_V_1_sel;
  wire image_out_V_keep_V_1_sel_rd_i_1_n_0;
  wire image_out_V_keep_V_1_sel_wr;
  wire image_out_V_keep_V_1_sel_wr_i_1_n_0;
  wire [1:1]image_out_V_keep_V_1_state;
  wire \image_out_V_keep_V_1_state[0]_i_1_n_0 ;
  wire \image_out_V_keep_V_1_state_reg_n_0_[0] ;
  wire image_out_V_last_V_1_ack_in;
  wire image_out_V_last_V_1_payload_A;
  wire \image_out_V_last_V_1_payload_A[0]_i_1_n_0 ;
  wire \image_out_V_last_V_1_payload_A[0]_i_2_n_0 ;
  wire image_out_V_last_V_1_payload_B;
  wire \image_out_V_last_V_1_payload_B[0]_i_1_n_0 ;
  wire image_out_V_last_V_1_sel;
  wire image_out_V_last_V_1_sel_rd_i_1_n_0;
  wire image_out_V_last_V_1_sel_wr;
  wire image_out_V_last_V_1_sel_wr_i_1_n_0;
  wire [1:1]image_out_V_last_V_1_state;
  wire \image_out_V_last_V_1_state[0]_i_1_n_0 ;
  wire \image_out_V_last_V_1_state_reg_n_0_[0] ;
  wire image_out_V_strb_V_1_ack_in;
  wire [2:0]image_out_V_strb_V_1_payload_A;
  wire \image_out_V_strb_V_1_payload_A[0]_i_1_n_0 ;
  wire \image_out_V_strb_V_1_payload_A[1]_i_1_n_0 ;
  wire \image_out_V_strb_V_1_payload_A[2]_i_1_n_0 ;
  wire [2:0]image_out_V_strb_V_1_payload_B;
  wire \image_out_V_strb_V_1_payload_B[0]_i_1_n_0 ;
  wire \image_out_V_strb_V_1_payload_B[1]_i_1_n_0 ;
  wire \image_out_V_strb_V_1_payload_B[2]_i_1_n_0 ;
  wire image_out_V_strb_V_1_sel;
  wire image_out_V_strb_V_1_sel_rd_i_1_n_0;
  wire image_out_V_strb_V_1_sel_wr;
  wire image_out_V_strb_V_1_sel_wr_i_1_n_0;
  wire [1:1]image_out_V_strb_V_1_state;
  wire \image_out_V_strb_V_1_state[0]_i_1_n_0 ;
  wire \image_out_V_strb_V_1_state_reg_n_0_[0] ;
  wire image_out_V_user_V_1_ack_in;
  wire image_out_V_user_V_1_payload_A;
  wire \image_out_V_user_V_1_payload_A[0]_i_1_n_0 ;
  wire \image_out_V_user_V_1_payload_A[0]_i_2_n_0 ;
  wire image_out_V_user_V_1_payload_B;
  wire \image_out_V_user_V_1_payload_B[0]_i_1_n_0 ;
  wire image_out_V_user_V_1_sel;
  wire image_out_V_user_V_1_sel_rd_i_1_n_0;
  wire image_out_V_user_V_1_sel_wr;
  wire image_out_V_user_V_1_sel_wr_i_1_n_0;
  wire [1:1]image_out_V_user_V_1_state;
  wire \image_out_V_user_V_1_state[0]_i_1_n_0 ;
  wire \image_out_V_user_V_1_state_reg_n_0_[0] ;
  wire [10:0]j_V_1_fu_308_p2;
  wire [10:0]j_V_1_reg_393;
  wire \j_V_1_reg_393[10]_i_2_n_0 ;
  wire \j_V_1_reg_393[7]_i_2_n_0 ;
  wire [8:0]j_V_2_fu_343_p2;
  wire [8:0]j_V_2_reg_433;
  wire \j_V_2_reg_433[8]_i_2_n_0 ;
  wire [8:0]j_V_fu_290_p2;
  wire [8:0]j_V_reg_360;
  wire \j_V_reg_360[8]_i_2_n_0 ;
  wire [10:0]p_0129_1_reg_192;
  wire \p_0129_1_reg_192[10]_i_3_n_0 ;
  wire [8:0]p_0129_2_reg_203;
  wire p_1_reg_181;
  wire \p_1_reg_181_reg_n_0_[0] ;
  wire \p_1_reg_181_reg_n_0_[1] ;
  wire \p_1_reg_181_reg_n_0_[2] ;
  wire \p_1_reg_181_reg_n_0_[3] ;
  wire \p_1_reg_181_reg_n_0_[4] ;
  wire \p_1_reg_181_reg_n_0_[5] ;
  wire \p_1_reg_181_reg_n_0_[6] ;
  wire \p_1_reg_181_reg_n_0_[7] ;
  wire \p_1_reg_181_reg_n_0_[8] ;
  wire p_255_in;
  wire p_s_reg_169;
  wire \p_s_reg_169[10]_i_3_n_0 ;
  wire \p_s_reg_169_reg_n_0_[0] ;
  wire \p_s_reg_169_reg_n_0_[10] ;
  wire \p_s_reg_169_reg_n_0_[1] ;
  wire \p_s_reg_169_reg_n_0_[2] ;
  wire \p_s_reg_169_reg_n_0_[3] ;
  wire \p_s_reg_169_reg_n_0_[4] ;
  wire \p_s_reg_169_reg_n_0_[5] ;
  wire \p_s_reg_169_reg_n_0_[6] ;
  wire \p_s_reg_169_reg_n_0_[7] ;
  wire \p_s_reg_169_reg_n_0_[8] ;
  wire \p_s_reg_169_reg_n_0_[9] ;
  wire \tmp_2_reg_365[0]_i_1_n_0 ;
  wire \tmp_2_reg_365[0]_i_2_n_0 ;
  wire \tmp_2_reg_365_reg_n_0_[0] ;
  wire tmp_6_fu_302_p2;
  wire tmp_fu_272_p2;
  wire tmp_last_V_2_reg_403;
  wire \tmp_last_V_2_reg_403[0]_i_1_n_0 ;
  wire \tmp_last_V_2_reg_403[0]_i_2_n_0 ;
  wire \tmp_last_V_2_reg_403[0]_i_3_n_0 ;
  wire \tmp_user_V_2_reg_398[0]_i_1_n_0 ;
  wire \tmp_user_V_2_reg_398[0]_i_2_n_0 ;
  wire \tmp_user_V_2_reg_398[0]_i_3_n_0 ;
  wire \tmp_user_V_2_reg_398[0]_i_4_n_0 ;
  wire \tmp_user_V_2_reg_398[0]_i_5_n_0 ;
  wire \tmp_user_V_2_reg_398_reg_n_0_[0] ;

  assign ap_ready = ap_done;
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h80FF8080)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(tmp_fu_272_p2),
        .I1(i_V_reg_3520),
        .I2(ap_CS_fsm_state2),
        .I3(ap_start),
        .I4(\ap_CS_fsm_reg_n_0_[0] ),
        .O(ap_NS_fsm[0]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \ap_CS_fsm[10]_i_1 
       (.I0(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I1(ap_CS_fsm_state10),
        .I2(image_out_V_data_V_1_ack_in),
        .I3(ap_CS_fsm_state11),
        .O(ap_NS_fsm[10]));
  LUT6 #(
    .INIT(64'hFFFF88F888F888F8)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_start),
        .I1(\ap_CS_fsm_reg_n_0_[0] ),
        .I2(ap_CS_fsm_state2),
        .I3(i_V_reg_3520),
        .I4(ap_NS_fsm1105_out),
        .I5(ap_CS_fsm_state9),
        .O(ap_NS_fsm[1]));
  LUT5 #(
    .INIT(32'hFF404040)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(tmp_fu_272_p2),
        .I1(i_V_reg_3520),
        .I2(ap_CS_fsm_state2),
        .I3(image_out_V_data_V_1_ack_in),
        .I4(ap_CS_fsm_state5),
        .O(ap_NS_fsm[2]));
  LUT5 #(
    .INIT(32'h44F4F4F4)) 
    \ap_CS_fsm[3]_i_1 
       (.I0(ap_NS_fsm1112_out),
        .I1(ap_CS_fsm_state3),
        .I2(ap_CS_fsm_state4),
        .I3(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I4(image_out_V_data_V_1_ack_in),
        .O(ap_NS_fsm[3]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \ap_CS_fsm[4]_i_1 
       (.I0(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I1(ap_CS_fsm_state4),
        .I2(image_out_V_data_V_1_ack_in),
        .I3(ap_CS_fsm_state5),
        .O(ap_NS_fsm[4]));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \ap_CS_fsm[5]_i_1 
       (.I0(crop_out_V_data_V_1_ack_in),
        .I1(image_out_V_data_V_1_ack_in),
        .I2(ap_CS_fsm_state8),
        .I3(ap_CS_fsm_state3),
        .I4(ap_NS_fsm1112_out),
        .O(ap_NS_fsm[5]));
  LUT6 #(
    .INIT(64'h2AAAFFFF2AAA2AAA)) 
    \ap_CS_fsm[6]_i_1 
       (.I0(ap_CS_fsm_state7),
        .I1(crop_out_V_data_V_1_ack_in),
        .I2(image_out_V_data_V_1_ack_in),
        .I3(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I4(tmp_6_fu_302_p2),
        .I5(ap_CS_fsm_state6),
        .O(ap_NS_fsm[6]));
  LUT5 #(
    .INIT(32'h88F0F0F0)) 
    \ap_CS_fsm[7]_i_1 
       (.I0(ap_CS_fsm_state7),
        .I1(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I2(ap_CS_fsm_state8),
        .I3(crop_out_V_data_V_1_ack_in),
        .I4(image_out_V_data_V_1_ack_in),
        .O(ap_NS_fsm[7]));
  LUT4 #(
    .INIT(16'hF888)) 
    \ap_CS_fsm[8]_i_1 
       (.I0(image_out_V_data_V_1_ack_in),
        .I1(ap_CS_fsm_state11),
        .I2(ap_CS_fsm_state6),
        .I3(tmp_6_fu_302_p2),
        .O(ap_NS_fsm[8]));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \ap_CS_fsm[8]_i_2 
       (.I0(\ap_CS_fsm[8]_i_3_n_0 ),
        .I1(p_0129_1_reg_192[7]),
        .I2(p_0129_1_reg_192[10]),
        .I3(p_0129_1_reg_192[9]),
        .I4(p_0129_1_reg_192[8]),
        .I5(\ap_CS_fsm[8]_i_4_n_0 ),
        .O(tmp_6_fu_302_p2));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \ap_CS_fsm[8]_i_3 
       (.I0(p_0129_1_reg_192[6]),
        .I1(p_0129_1_reg_192[5]),
        .I2(p_0129_1_reg_192[4]),
        .I3(p_0129_1_reg_192[3]),
        .O(\ap_CS_fsm[8]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \ap_CS_fsm[8]_i_4 
       (.I0(p_0129_1_reg_192[2]),
        .I1(p_0129_1_reg_192[1]),
        .I2(p_0129_1_reg_192[0]),
        .O(\ap_CS_fsm[8]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h44F4F4F4)) 
    \ap_CS_fsm[9]_i_1 
       (.I0(ap_NS_fsm1105_out),
        .I1(ap_CS_fsm_state9),
        .I2(ap_CS_fsm_state10),
        .I3(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I4(image_out_V_data_V_1_ack_in),
        .O(ap_NS_fsm[9]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_0_[0] ),
        .S(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[10] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[10]),
        .Q(ap_CS_fsm_state11),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_state3),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state4),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_state5),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_state6),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[6]),
        .Q(ap_CS_fsm_state7),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[7] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[7]),
        .Q(ap_CS_fsm_state8),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[8] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[8]),
        .Q(ap_CS_fsm_state9),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[9] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[9]),
        .Q(ap_CS_fsm_state10),
        .R(ap_rst_n_inv));
  LUT2 #(
    .INIT(4'h2)) 
    ap_idle_INST_0
       (.I0(\ap_CS_fsm_reg_n_0_[0] ),
        .I1(ap_start),
        .O(ap_idle));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    ap_ready_INST_0
       (.I0(i_V_reg_3520),
        .I1(tmp_fu_272_p2),
        .O(ap_done));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    ap_ready_INST_0_i_1
       (.I0(ap_ready_INST_0_i_3_n_0),
        .I1(image_out_V_last_V_1_ack_in),
        .I2(image_out_V_id_V_1_ack_in),
        .I3(crop_out_V_user_V_1_ack_in),
        .I4(ap_ready_INST_0_i_4_n_0),
        .I5(ap_ready_INST_0_i_5_n_0),
        .O(i_V_reg_3520));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    ap_ready_INST_0_i_2
       (.I0(ap_ready_INST_0_i_6_n_0),
        .I1(ap_ready_INST_0_i_7_n_0),
        .I2(\p_s_reg_169_reg_n_0_[0] ),
        .I3(\p_s_reg_169_reg_n_0_[1] ),
        .I4(\p_s_reg_169_reg_n_0_[2] ),
        .O(tmp_fu_272_p2));
  LUT4 #(
    .INIT(16'h8000)) 
    ap_ready_INST_0_i_3
       (.I0(image_out_V_dest_V_1_ack_in),
        .I1(crop_out_V_dest_V_1_ack_in),
        .I2(crop_out_V_keep_V_1_ack_in),
        .I3(image_out_V_keep_V_1_ack_in),
        .O(ap_ready_INST_0_i_3_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    ap_ready_INST_0_i_4
       (.I0(image_out_V_user_V_1_ack_in),
        .I1(crop_out_V_last_V_1_ack_in),
        .I2(image_out_V_strb_V_1_ack_in),
        .I3(crop_out_V_id_V_1_ack_in),
        .O(ap_ready_INST_0_i_4_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    ap_ready_INST_0_i_5
       (.I0(ap_CS_fsm_state2),
        .I1(crop_out_V_strb_V_1_ack_in),
        .I2(image_out_V_data_V_1_ack_in),
        .I3(crop_out_V_data_V_1_ack_in),
        .O(ap_ready_INST_0_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    ap_ready_INST_0_i_6
       (.I0(\p_s_reg_169_reg_n_0_[6] ),
        .I1(\p_s_reg_169_reg_n_0_[5] ),
        .I2(\p_s_reg_169_reg_n_0_[4] ),
        .I3(\p_s_reg_169_reg_n_0_[3] ),
        .O(ap_ready_INST_0_i_6_n_0));
  LUT4 #(
    .INIT(16'h0004)) 
    ap_ready_INST_0_i_7
       (.I0(\p_s_reg_169_reg_n_0_[7] ),
        .I1(\p_s_reg_169_reg_n_0_[10] ),
        .I2(\p_s_reg_169_reg_n_0_[9] ),
        .I3(\p_s_reg_169_reg_n_0_[8] ),
        .O(ap_ready_INST_0_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[0]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[0]),
        .I1(crop_out_V_data_V_1_payload_A[0]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[0]));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[10]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[10]),
        .I1(crop_out_V_data_V_1_payload_A[10]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[10]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[11]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[11]),
        .I1(crop_out_V_data_V_1_payload_A[11]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[11]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[12]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[12]),
        .I1(crop_out_V_data_V_1_payload_A[12]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[12]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[13]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[13]),
        .I1(crop_out_V_data_V_1_payload_A[13]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[13]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[14]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[14]),
        .I1(crop_out_V_data_V_1_payload_A[14]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[14]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[15]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[15]),
        .I1(crop_out_V_data_V_1_payload_A[15]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[15]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[16]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[16]),
        .I1(crop_out_V_data_V_1_payload_A[16]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[16]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[17]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[17]),
        .I1(crop_out_V_data_V_1_payload_A[17]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[17]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[18]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[18]),
        .I1(crop_out_V_data_V_1_payload_A[18]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[18]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[19]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[19]),
        .I1(crop_out_V_data_V_1_payload_A[19]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[19]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[1]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[1]),
        .I1(crop_out_V_data_V_1_payload_A[1]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[1]));
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[20]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[20]),
        .I1(crop_out_V_data_V_1_payload_A[20]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[20]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[21]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[21]),
        .I1(crop_out_V_data_V_1_payload_A[21]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[21]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[22]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[22]),
        .I1(crop_out_V_data_V_1_payload_A[22]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[22]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[23]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[23]),
        .I1(crop_out_V_data_V_1_payload_A[23]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[23]));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[2]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[2]),
        .I1(crop_out_V_data_V_1_payload_A[2]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[2]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[3]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[3]),
        .I1(crop_out_V_data_V_1_payload_A[3]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[3]));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[4]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[4]),
        .I1(crop_out_V_data_V_1_payload_A[4]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[4]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[5]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[5]),
        .I1(crop_out_V_data_V_1_payload_A[5]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[5]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[6]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[6]),
        .I1(crop_out_V_data_V_1_payload_A[6]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[6]));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[7]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[7]),
        .I1(crop_out_V_data_V_1_payload_A[7]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[7]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[8]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[8]),
        .I1(crop_out_V_data_V_1_payload_A[8]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[8]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[9]_INST_0 
       (.I0(crop_out_V_data_V_1_payload_B[9]),
        .I1(crop_out_V_data_V_1_payload_A[9]),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_TDATA[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TDEST[0]_INST_0 
       (.I0(crop_out_V_dest_V_1_payload_B),
        .I1(crop_out_V_dest_V_1_sel),
        .I2(crop_out_V_dest_V_1_payload_A),
        .O(crop_out_TDEST));
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TID[0]_INST_0 
       (.I0(crop_out_V_id_V_1_payload_B),
        .I1(crop_out_V_id_V_1_sel),
        .I2(crop_out_V_id_V_1_payload_A),
        .O(crop_out_TID));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TKEEP[0]_INST_0 
       (.I0(crop_out_V_keep_V_1_payload_B[0]),
        .I1(crop_out_V_keep_V_1_sel),
        .I2(crop_out_V_keep_V_1_payload_A[0]),
        .O(crop_out_TKEEP[0]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TKEEP[1]_INST_0 
       (.I0(crop_out_V_keep_V_1_payload_B[1]),
        .I1(crop_out_V_keep_V_1_sel),
        .I2(crop_out_V_keep_V_1_payload_A[1]),
        .O(crop_out_TKEEP[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TKEEP[2]_INST_0 
       (.I0(crop_out_V_keep_V_1_payload_B[2]),
        .I1(crop_out_V_keep_V_1_sel),
        .I2(crop_out_V_keep_V_1_payload_A[2]),
        .O(crop_out_TKEEP[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TLAST[0]_INST_0 
       (.I0(crop_out_V_last_V_1_payload_B),
        .I1(crop_out_V_last_V_1_sel),
        .I2(crop_out_V_last_V_1_payload_A),
        .O(crop_out_TLAST));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TSTRB[0]_INST_0 
       (.I0(crop_out_V_strb_V_1_payload_B[0]),
        .I1(crop_out_V_strb_V_1_sel),
        .I2(crop_out_V_strb_V_1_payload_A[0]),
        .O(crop_out_TSTRB[0]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TSTRB[1]_INST_0 
       (.I0(crop_out_V_strb_V_1_payload_B[1]),
        .I1(crop_out_V_strb_V_1_sel),
        .I2(crop_out_V_strb_V_1_payload_A[1]),
        .O(crop_out_TSTRB[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TSTRB[2]_INST_0 
       (.I0(crop_out_V_strb_V_1_payload_B[2]),
        .I1(crop_out_V_strb_V_1_sel),
        .I2(crop_out_V_strb_V_1_payload_A[2]),
        .O(crop_out_TSTRB[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TUSER[0]_INST_0 
       (.I0(crop_out_V_user_V_1_payload_B),
        .I1(crop_out_V_user_V_1_sel),
        .I2(crop_out_V_user_V_1_payload_A),
        .O(crop_out_TUSER));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[0]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[0]),
        .O(image_in_V_data_V_0_data_out[0]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[10]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[10]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[10]),
        .O(image_in_V_data_V_0_data_out[10]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[11]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[11]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[11]),
        .O(image_in_V_data_V_0_data_out[11]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[12]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[12]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[12]),
        .O(image_in_V_data_V_0_data_out[12]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[13]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[13]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[13]),
        .O(image_in_V_data_V_0_data_out[13]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[14]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[14]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[14]),
        .O(image_in_V_data_V_0_data_out[14]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[15]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[15]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[15]),
        .O(image_in_V_data_V_0_data_out[15]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[16]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[16]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[16]),
        .O(image_in_V_data_V_0_data_out[16]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[17]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[17]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[17]),
        .O(image_in_V_data_V_0_data_out[17]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[18]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[18]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[18]),
        .O(image_in_V_data_V_0_data_out[18]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[19]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[19]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[19]),
        .O(image_in_V_data_V_0_data_out[19]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[1]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[1]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[1]),
        .O(image_in_V_data_V_0_data_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[20]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[20]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[20]),
        .O(image_in_V_data_V_0_data_out[20]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[21]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[21]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[21]),
        .O(image_in_V_data_V_0_data_out[21]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[22]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[22]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[22]),
        .O(image_in_V_data_V_0_data_out[22]));
  LUT3 #(
    .INIT(8'h0D)) 
    \crop_out_V_data_V_1_payload_A[23]_i_1 
       (.I0(\crop_out_V_data_V_1_state_reg_n_0_[0] ),
        .I1(crop_out_V_data_V_1_ack_in),
        .I2(crop_out_V_data_V_1_sel_wr),
        .O(crop_out_V_data_V_1_load_A));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[23]_i_2 
       (.I0(image_in_V_data_V_0_payload_B[23]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[23]),
        .O(image_in_V_data_V_0_data_out[23]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[2]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[2]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[2]),
        .O(image_in_V_data_V_0_data_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[3]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[3]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[3]),
        .O(image_in_V_data_V_0_data_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[4]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[4]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[4]),
        .O(image_in_V_data_V_0_data_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[5]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[5]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[5]),
        .O(image_in_V_data_V_0_data_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[6]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[6]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[6]),
        .O(image_in_V_data_V_0_data_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[7]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[7]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[7]),
        .O(image_in_V_data_V_0_data_out[7]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[8]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[8]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[8]),
        .O(image_in_V_data_V_0_data_out[8]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_V_data_V_1_payload_A[9]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[9]),
        .I1(image_in_V_data_V_0_sel),
        .I2(image_in_V_data_V_0_payload_A[9]),
        .O(image_in_V_data_V_0_data_out[9]));
  FDRE \crop_out_V_data_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[0]),
        .Q(crop_out_V_data_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[10]),
        .Q(crop_out_V_data_V_1_payload_A[10]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[11]),
        .Q(crop_out_V_data_V_1_payload_A[11]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[12]),
        .Q(crop_out_V_data_V_1_payload_A[12]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[13]),
        .Q(crop_out_V_data_V_1_payload_A[13]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[14]),
        .Q(crop_out_V_data_V_1_payload_A[14]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[15]),
        .Q(crop_out_V_data_V_1_payload_A[15]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[16]),
        .Q(crop_out_V_data_V_1_payload_A[16]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[17]),
        .Q(crop_out_V_data_V_1_payload_A[17]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[18]),
        .Q(crop_out_V_data_V_1_payload_A[18]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[19]),
        .Q(crop_out_V_data_V_1_payload_A[19]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[1]),
        .Q(crop_out_V_data_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[20]),
        .Q(crop_out_V_data_V_1_payload_A[20]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[21]),
        .Q(crop_out_V_data_V_1_payload_A[21]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[22]),
        .Q(crop_out_V_data_V_1_payload_A[22]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[23]),
        .Q(crop_out_V_data_V_1_payload_A[23]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[2]),
        .Q(crop_out_V_data_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[3]),
        .Q(crop_out_V_data_V_1_payload_A[3]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[4]),
        .Q(crop_out_V_data_V_1_payload_A[4]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[5]),
        .Q(crop_out_V_data_V_1_payload_A[5]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[6]),
        .Q(crop_out_V_data_V_1_payload_A[6]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[7]),
        .Q(crop_out_V_data_V_1_payload_A[7]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[8]),
        .Q(crop_out_V_data_V_1_payload_A[8]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_A),
        .D(image_in_V_data_V_0_data_out[9]),
        .Q(crop_out_V_data_V_1_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \crop_out_V_data_V_1_payload_B[23]_i_1 
       (.I0(\crop_out_V_data_V_1_state_reg_n_0_[0] ),
        .I1(crop_out_V_data_V_1_ack_in),
        .I2(crop_out_V_data_V_1_sel_wr),
        .O(crop_out_V_data_V_1_load_B));
  FDRE \crop_out_V_data_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[0]),
        .Q(crop_out_V_data_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[10]),
        .Q(crop_out_V_data_V_1_payload_B[10]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[11]),
        .Q(crop_out_V_data_V_1_payload_B[11]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[12]),
        .Q(crop_out_V_data_V_1_payload_B[12]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[13]),
        .Q(crop_out_V_data_V_1_payload_B[13]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[14]),
        .Q(crop_out_V_data_V_1_payload_B[14]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[15]),
        .Q(crop_out_V_data_V_1_payload_B[15]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[16]),
        .Q(crop_out_V_data_V_1_payload_B[16]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[17]),
        .Q(crop_out_V_data_V_1_payload_B[17]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[18]),
        .Q(crop_out_V_data_V_1_payload_B[18]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[19]),
        .Q(crop_out_V_data_V_1_payload_B[19]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[1]),
        .Q(crop_out_V_data_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[20]),
        .Q(crop_out_V_data_V_1_payload_B[20]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[21]),
        .Q(crop_out_V_data_V_1_payload_B[21]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[22]),
        .Q(crop_out_V_data_V_1_payload_B[22]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[23]),
        .Q(crop_out_V_data_V_1_payload_B[23]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[2]),
        .Q(crop_out_V_data_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[3]),
        .Q(crop_out_V_data_V_1_payload_B[3]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[4]),
        .Q(crop_out_V_data_V_1_payload_B[4]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[5]),
        .Q(crop_out_V_data_V_1_payload_B[5]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[6]),
        .Q(crop_out_V_data_V_1_payload_B[6]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[7]),
        .Q(crop_out_V_data_V_1_payload_B[7]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[8]),
        .Q(crop_out_V_data_V_1_payload_B[8]),
        .R(1'b0));
  FDRE \crop_out_V_data_V_1_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(crop_out_V_data_V_1_load_B),
        .D(image_in_V_data_V_0_data_out[9]),
        .Q(crop_out_V_data_V_1_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'h78)) 
    crop_out_V_data_V_1_sel_rd_i_1
       (.I0(crop_out_TREADY),
        .I1(\crop_out_V_data_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_data_V_1_sel),
        .O(crop_out_V_data_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_data_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_data_V_1_sel_rd_i_1_n_0),
        .Q(crop_out_V_data_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    crop_out_V_data_V_1_sel_wr_i_1
       (.I0(crop_out_V_data_V_1_ack_in),
        .I1(ap_CS_fsm_state7),
        .I2(image_out_V_data_V_1_ack_in),
        .I3(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I4(crop_out_V_data_V_1_sel_wr),
        .O(crop_out_V_data_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_data_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_data_V_1_sel_wr_i_1_n_0),
        .Q(crop_out_V_data_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \crop_out_V_data_V_1_state[0]_i_1 
       (.I0(crop_out_TREADY),
        .I1(p_255_in),
        .I2(crop_out_V_data_V_1_ack_in),
        .I3(\crop_out_V_data_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\crop_out_V_data_V_1_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF4CCCFFFF)) 
    \crop_out_V_data_V_1_state[1]_i_1 
       (.I0(ap_CS_fsm_state7),
        .I1(crop_out_V_data_V_1_ack_in),
        .I2(image_out_V_data_V_1_ack_in),
        .I3(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I4(\crop_out_V_data_V_1_state_reg_n_0_[0] ),
        .I5(crop_out_TREADY),
        .O(crop_out_V_data_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_data_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_data_V_1_state[0]_i_1_n_0 ),
        .Q(\crop_out_V_data_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_data_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_data_V_1_state),
        .Q(crop_out_V_data_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \crop_out_V_dest_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_dest_V_0_data_out),
        .I1(crop_out_TVALID),
        .I2(crop_out_V_dest_V_1_ack_in),
        .I3(crop_out_V_dest_V_1_sel_wr),
        .I4(crop_out_V_dest_V_1_payload_A),
        .O(\crop_out_V_dest_V_1_payload_A[0]_i_1_n_0 ));
  FDRE \crop_out_V_dest_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_dest_V_1_payload_A[0]_i_1_n_0 ),
        .Q(crop_out_V_dest_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \crop_out_V_dest_V_1_payload_B[0]_i_1 
       (.I0(image_in_V_dest_V_0_data_out),
        .I1(crop_out_TVALID),
        .I2(crop_out_V_dest_V_1_ack_in),
        .I3(crop_out_V_dest_V_1_sel_wr),
        .I4(crop_out_V_dest_V_1_payload_B),
        .O(\crop_out_V_dest_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \crop_out_V_dest_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_dest_V_1_payload_B[0]_i_1_n_0 ),
        .Q(crop_out_V_dest_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h78)) 
    crop_out_V_dest_V_1_sel_rd_i_1
       (.I0(crop_out_TREADY),
        .I1(crop_out_TVALID),
        .I2(crop_out_V_dest_V_1_sel),
        .O(crop_out_V_dest_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_dest_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_dest_V_1_sel_rd_i_1_n_0),
        .Q(crop_out_V_dest_V_1_sel),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    crop_out_V_dest_V_1_sel_wr_i_1
       (.I0(crop_out_V_dest_V_1_ack_in),
        .I1(ap_CS_fsm_state7),
        .I2(crop_out_V_data_V_1_ack_in),
        .I3(image_out_V_data_V_1_ack_in),
        .I4(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I5(crop_out_V_dest_V_1_sel_wr),
        .O(crop_out_V_dest_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_dest_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_dest_V_1_sel_wr_i_1_n_0),
        .Q(crop_out_V_dest_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \crop_out_V_dest_V_1_state[0]_i_1 
       (.I0(crop_out_TREADY),
        .I1(p_255_in),
        .I2(crop_out_V_dest_V_1_ack_in),
        .I3(crop_out_TVALID),
        .I4(ap_rst_n),
        .O(\crop_out_V_dest_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \crop_out_V_dest_V_1_state[0]_i_2 
       (.I0(ap_CS_fsm_state7),
        .I1(crop_out_V_data_V_1_ack_in),
        .I2(image_out_V_data_V_1_ack_in),
        .I3(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .O(p_255_in));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFF4F)) 
    \crop_out_V_dest_V_1_state[1]_i_1 
       (.I0(p_255_in),
        .I1(crop_out_V_dest_V_1_ack_in),
        .I2(crop_out_TVALID),
        .I3(crop_out_TREADY),
        .O(crop_out_V_dest_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_dest_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_dest_V_1_state[0]_i_1_n_0 ),
        .Q(crop_out_TVALID),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_dest_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_dest_V_1_state),
        .Q(crop_out_V_dest_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \crop_out_V_id_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_id_V_0_data_out),
        .I1(\crop_out_V_id_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_id_V_1_ack_in),
        .I3(crop_out_V_id_V_1_sel_wr),
        .I4(crop_out_V_id_V_1_payload_A),
        .O(\crop_out_V_id_V_1_payload_A[0]_i_1_n_0 ));
  FDRE \crop_out_V_id_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_id_V_1_payload_A[0]_i_1_n_0 ),
        .Q(crop_out_V_id_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \crop_out_V_id_V_1_payload_B[0]_i_1 
       (.I0(image_in_V_id_V_0_data_out),
        .I1(\crop_out_V_id_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_id_V_1_ack_in),
        .I3(crop_out_V_id_V_1_sel_wr),
        .I4(crop_out_V_id_V_1_payload_B),
        .O(\crop_out_V_id_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \crop_out_V_id_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_id_V_1_payload_B[0]_i_1_n_0 ),
        .Q(crop_out_V_id_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h78)) 
    crop_out_V_id_V_1_sel_rd_i_1
       (.I0(crop_out_TREADY),
        .I1(\crop_out_V_id_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_id_V_1_sel),
        .O(crop_out_V_id_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_id_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_id_V_1_sel_rd_i_1_n_0),
        .Q(crop_out_V_id_V_1_sel),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    crop_out_V_id_V_1_sel_wr_i_1
       (.I0(crop_out_V_id_V_1_ack_in),
        .I1(ap_CS_fsm_state7),
        .I2(crop_out_V_data_V_1_ack_in),
        .I3(image_out_V_data_V_1_ack_in),
        .I4(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I5(crop_out_V_id_V_1_sel_wr),
        .O(crop_out_V_id_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_id_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_id_V_1_sel_wr_i_1_n_0),
        .Q(crop_out_V_id_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \crop_out_V_id_V_1_state[0]_i_1 
       (.I0(crop_out_TREADY),
        .I1(p_255_in),
        .I2(crop_out_V_id_V_1_ack_in),
        .I3(\crop_out_V_id_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\crop_out_V_id_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'hFF4F)) 
    \crop_out_V_id_V_1_state[1]_i_1 
       (.I0(p_255_in),
        .I1(crop_out_V_id_V_1_ack_in),
        .I2(\crop_out_V_id_V_1_state_reg_n_0_[0] ),
        .I3(crop_out_TREADY),
        .O(crop_out_V_id_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_id_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_id_V_1_state[0]_i_1_n_0 ),
        .Q(\crop_out_V_id_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_id_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_id_V_1_state),
        .Q(crop_out_V_id_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \crop_out_V_keep_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[0]),
        .I1(\crop_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_keep_V_1_ack_in),
        .I3(crop_out_V_keep_V_1_sel_wr),
        .I4(crop_out_V_keep_V_1_payload_A[0]),
        .O(\crop_out_V_keep_V_1_payload_A[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \crop_out_V_keep_V_1_payload_A[1]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[1]),
        .I1(\crop_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_keep_V_1_ack_in),
        .I3(crop_out_V_keep_V_1_sel_wr),
        .I4(crop_out_V_keep_V_1_payload_A[1]),
        .O(\crop_out_V_keep_V_1_payload_A[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \crop_out_V_keep_V_1_payload_A[2]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[2]),
        .I1(\crop_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_keep_V_1_ack_in),
        .I3(crop_out_V_keep_V_1_sel_wr),
        .I4(crop_out_V_keep_V_1_payload_A[2]),
        .O(\crop_out_V_keep_V_1_payload_A[2]_i_1_n_0 ));
  FDRE \crop_out_V_keep_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_keep_V_1_payload_A[0]_i_1_n_0 ),
        .Q(crop_out_V_keep_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \crop_out_V_keep_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_keep_V_1_payload_A[1]_i_1_n_0 ),
        .Q(crop_out_V_keep_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \crop_out_V_keep_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_keep_V_1_payload_A[2]_i_1_n_0 ),
        .Q(crop_out_V_keep_V_1_payload_A[2]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \crop_out_V_keep_V_1_payload_B[0]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[0]),
        .I1(\crop_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_keep_V_1_ack_in),
        .I3(crop_out_V_keep_V_1_sel_wr),
        .I4(crop_out_V_keep_V_1_payload_B[0]),
        .O(\crop_out_V_keep_V_1_payload_B[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \crop_out_V_keep_V_1_payload_B[1]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[1]),
        .I1(\crop_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_keep_V_1_ack_in),
        .I3(crop_out_V_keep_V_1_sel_wr),
        .I4(crop_out_V_keep_V_1_payload_B[1]),
        .O(\crop_out_V_keep_V_1_payload_B[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \crop_out_V_keep_V_1_payload_B[2]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[2]),
        .I1(\crop_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_keep_V_1_ack_in),
        .I3(crop_out_V_keep_V_1_sel_wr),
        .I4(crop_out_V_keep_V_1_payload_B[2]),
        .O(\crop_out_V_keep_V_1_payload_B[2]_i_1_n_0 ));
  FDRE \crop_out_V_keep_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_keep_V_1_payload_B[0]_i_1_n_0 ),
        .Q(crop_out_V_keep_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \crop_out_V_keep_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_keep_V_1_payload_B[1]_i_1_n_0 ),
        .Q(crop_out_V_keep_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \crop_out_V_keep_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_keep_V_1_payload_B[2]_i_1_n_0 ),
        .Q(crop_out_V_keep_V_1_payload_B[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h78)) 
    crop_out_V_keep_V_1_sel_rd_i_1
       (.I0(crop_out_TREADY),
        .I1(\crop_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_keep_V_1_sel),
        .O(crop_out_V_keep_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_keep_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_keep_V_1_sel_rd_i_1_n_0),
        .Q(crop_out_V_keep_V_1_sel),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    crop_out_V_keep_V_1_sel_wr_i_1
       (.I0(crop_out_V_keep_V_1_ack_in),
        .I1(ap_CS_fsm_state7),
        .I2(crop_out_V_data_V_1_ack_in),
        .I3(image_out_V_data_V_1_ack_in),
        .I4(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I5(crop_out_V_keep_V_1_sel_wr),
        .O(crop_out_V_keep_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_keep_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_keep_V_1_sel_wr_i_1_n_0),
        .Q(crop_out_V_keep_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \crop_out_V_keep_V_1_state[0]_i_1 
       (.I0(crop_out_TREADY),
        .I1(p_255_in),
        .I2(crop_out_V_keep_V_1_ack_in),
        .I3(\crop_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\crop_out_V_keep_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'hFF4F)) 
    \crop_out_V_keep_V_1_state[1]_i_1 
       (.I0(p_255_in),
        .I1(crop_out_V_keep_V_1_ack_in),
        .I2(\crop_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I3(crop_out_TREADY),
        .O(crop_out_V_keep_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_keep_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_keep_V_1_state[0]_i_1_n_0 ),
        .Q(\crop_out_V_keep_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_keep_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_keep_V_1_state),
        .Q(crop_out_V_keep_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \crop_out_V_last_V_1_payload_A[0]_i_1 
       (.I0(tmp_last_V_2_reg_403),
        .I1(\crop_out_V_last_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_last_V_1_ack_in),
        .I3(crop_out_V_last_V_1_sel_wr),
        .I4(crop_out_V_last_V_1_payload_A),
        .O(\crop_out_V_last_V_1_payload_A[0]_i_1_n_0 ));
  FDRE \crop_out_V_last_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_last_V_1_payload_A[0]_i_1_n_0 ),
        .Q(crop_out_V_last_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \crop_out_V_last_V_1_payload_B[0]_i_1 
       (.I0(tmp_last_V_2_reg_403),
        .I1(\crop_out_V_last_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_last_V_1_ack_in),
        .I3(crop_out_V_last_V_1_sel_wr),
        .I4(crop_out_V_last_V_1_payload_B),
        .O(\crop_out_V_last_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \crop_out_V_last_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_last_V_1_payload_B[0]_i_1_n_0 ),
        .Q(crop_out_V_last_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h78)) 
    crop_out_V_last_V_1_sel_rd_i_1
       (.I0(crop_out_TREADY),
        .I1(\crop_out_V_last_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_last_V_1_sel),
        .O(crop_out_V_last_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_last_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_last_V_1_sel_rd_i_1_n_0),
        .Q(crop_out_V_last_V_1_sel),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    crop_out_V_last_V_1_sel_wr_i_1
       (.I0(crop_out_V_last_V_1_ack_in),
        .I1(ap_CS_fsm_state7),
        .I2(crop_out_V_data_V_1_ack_in),
        .I3(image_out_V_data_V_1_ack_in),
        .I4(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I5(crop_out_V_last_V_1_sel_wr),
        .O(crop_out_V_last_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_last_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_last_V_1_sel_wr_i_1_n_0),
        .Q(crop_out_V_last_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \crop_out_V_last_V_1_state[0]_i_1 
       (.I0(crop_out_TREADY),
        .I1(p_255_in),
        .I2(crop_out_V_last_V_1_ack_in),
        .I3(\crop_out_V_last_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\crop_out_V_last_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'hFF4F)) 
    \crop_out_V_last_V_1_state[1]_i_1 
       (.I0(p_255_in),
        .I1(crop_out_V_last_V_1_ack_in),
        .I2(\crop_out_V_last_V_1_state_reg_n_0_[0] ),
        .I3(crop_out_TREADY),
        .O(crop_out_V_last_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_last_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_last_V_1_state[0]_i_1_n_0 ),
        .Q(\crop_out_V_last_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_last_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_last_V_1_state),
        .Q(crop_out_V_last_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \crop_out_V_strb_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[0]),
        .I1(\crop_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_strb_V_1_ack_in),
        .I3(crop_out_V_strb_V_1_sel_wr),
        .I4(crop_out_V_strb_V_1_payload_A[0]),
        .O(\crop_out_V_strb_V_1_payload_A[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \crop_out_V_strb_V_1_payload_A[1]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[1]),
        .I1(\crop_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_strb_V_1_ack_in),
        .I3(crop_out_V_strb_V_1_sel_wr),
        .I4(crop_out_V_strb_V_1_payload_A[1]),
        .O(\crop_out_V_strb_V_1_payload_A[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \crop_out_V_strb_V_1_payload_A[2]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[2]),
        .I1(\crop_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_strb_V_1_ack_in),
        .I3(crop_out_V_strb_V_1_sel_wr),
        .I4(crop_out_V_strb_V_1_payload_A[2]),
        .O(\crop_out_V_strb_V_1_payload_A[2]_i_1_n_0 ));
  FDRE \crop_out_V_strb_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_strb_V_1_payload_A[0]_i_1_n_0 ),
        .Q(crop_out_V_strb_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \crop_out_V_strb_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_strb_V_1_payload_A[1]_i_1_n_0 ),
        .Q(crop_out_V_strb_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \crop_out_V_strb_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_strb_V_1_payload_A[2]_i_1_n_0 ),
        .Q(crop_out_V_strb_V_1_payload_A[2]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \crop_out_V_strb_V_1_payload_B[0]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[0]),
        .I1(\crop_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_strb_V_1_ack_in),
        .I3(crop_out_V_strb_V_1_sel_wr),
        .I4(crop_out_V_strb_V_1_payload_B[0]),
        .O(\crop_out_V_strb_V_1_payload_B[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \crop_out_V_strb_V_1_payload_B[1]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[1]),
        .I1(\crop_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_strb_V_1_ack_in),
        .I3(crop_out_V_strb_V_1_sel_wr),
        .I4(crop_out_V_strb_V_1_payload_B[1]),
        .O(\crop_out_V_strb_V_1_payload_B[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \crop_out_V_strb_V_1_payload_B[2]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[2]),
        .I1(\crop_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_strb_V_1_ack_in),
        .I3(crop_out_V_strb_V_1_sel_wr),
        .I4(crop_out_V_strb_V_1_payload_B[2]),
        .O(\crop_out_V_strb_V_1_payload_B[2]_i_1_n_0 ));
  FDRE \crop_out_V_strb_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_strb_V_1_payload_B[0]_i_1_n_0 ),
        .Q(crop_out_V_strb_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \crop_out_V_strb_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_strb_V_1_payload_B[1]_i_1_n_0 ),
        .Q(crop_out_V_strb_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \crop_out_V_strb_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_strb_V_1_payload_B[2]_i_1_n_0 ),
        .Q(crop_out_V_strb_V_1_payload_B[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h78)) 
    crop_out_V_strb_V_1_sel_rd_i_1
       (.I0(crop_out_TREADY),
        .I1(\crop_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_strb_V_1_sel),
        .O(crop_out_V_strb_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_strb_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_strb_V_1_sel_rd_i_1_n_0),
        .Q(crop_out_V_strb_V_1_sel),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    crop_out_V_strb_V_1_sel_wr_i_1
       (.I0(crop_out_V_strb_V_1_ack_in),
        .I1(ap_CS_fsm_state7),
        .I2(crop_out_V_data_V_1_ack_in),
        .I3(image_out_V_data_V_1_ack_in),
        .I4(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I5(crop_out_V_strb_V_1_sel_wr),
        .O(crop_out_V_strb_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_strb_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_strb_V_1_sel_wr_i_1_n_0),
        .Q(crop_out_V_strb_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \crop_out_V_strb_V_1_state[0]_i_1 
       (.I0(crop_out_TREADY),
        .I1(p_255_in),
        .I2(crop_out_V_strb_V_1_ack_in),
        .I3(\crop_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\crop_out_V_strb_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'hFF4F)) 
    \crop_out_V_strb_V_1_state[1]_i_1 
       (.I0(p_255_in),
        .I1(crop_out_V_strb_V_1_ack_in),
        .I2(\crop_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I3(crop_out_TREADY),
        .O(crop_out_V_strb_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_strb_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_strb_V_1_state[0]_i_1_n_0 ),
        .Q(\crop_out_V_strb_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_strb_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_strb_V_1_state),
        .Q(crop_out_V_strb_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \crop_out_V_user_V_1_payload_A[0]_i_1 
       (.I0(\tmp_user_V_2_reg_398_reg_n_0_[0] ),
        .I1(\crop_out_V_user_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_user_V_1_ack_in),
        .I3(crop_out_V_user_V_1_sel_wr),
        .I4(crop_out_V_user_V_1_payload_A),
        .O(\crop_out_V_user_V_1_payload_A[0]_i_1_n_0 ));
  FDRE \crop_out_V_user_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_user_V_1_payload_A[0]_i_1_n_0 ),
        .Q(crop_out_V_user_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \crop_out_V_user_V_1_payload_B[0]_i_1 
       (.I0(\tmp_user_V_2_reg_398_reg_n_0_[0] ),
        .I1(\crop_out_V_user_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_user_V_1_ack_in),
        .I3(crop_out_V_user_V_1_sel_wr),
        .I4(crop_out_V_user_V_1_payload_B),
        .O(\crop_out_V_user_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \crop_out_V_user_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_user_V_1_payload_B[0]_i_1_n_0 ),
        .Q(crop_out_V_user_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h78)) 
    crop_out_V_user_V_1_sel_rd_i_1
       (.I0(crop_out_TREADY),
        .I1(\crop_out_V_user_V_1_state_reg_n_0_[0] ),
        .I2(crop_out_V_user_V_1_sel),
        .O(crop_out_V_user_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_user_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_user_V_1_sel_rd_i_1_n_0),
        .Q(crop_out_V_user_V_1_sel),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    crop_out_V_user_V_1_sel_wr_i_1
       (.I0(crop_out_V_user_V_1_ack_in),
        .I1(ap_CS_fsm_state7),
        .I2(crop_out_V_data_V_1_ack_in),
        .I3(image_out_V_data_V_1_ack_in),
        .I4(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I5(crop_out_V_user_V_1_sel_wr),
        .O(crop_out_V_user_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    crop_out_V_user_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_user_V_1_sel_wr_i_1_n_0),
        .Q(crop_out_V_user_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \crop_out_V_user_V_1_state[0]_i_1 
       (.I0(crop_out_TREADY),
        .I1(p_255_in),
        .I2(crop_out_V_user_V_1_ack_in),
        .I3(\crop_out_V_user_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\crop_out_V_user_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hFF4F)) 
    \crop_out_V_user_V_1_state[1]_i_1 
       (.I0(p_255_in),
        .I1(crop_out_V_user_V_1_ack_in),
        .I2(\crop_out_V_user_V_1_state_reg_n_0_[0] ),
        .I3(crop_out_TREADY),
        .O(crop_out_V_user_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_user_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\crop_out_V_user_V_1_state[0]_i_1_n_0 ),
        .Q(\crop_out_V_user_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \crop_out_V_user_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(crop_out_V_user_V_1_state),
        .Q(crop_out_V_user_V_1_ack_in),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_V_reg_352[0]_i_1 
       (.I0(\p_s_reg_169_reg_n_0_[0] ),
        .O(i_V_fu_278_p2[0]));
  LUT6 #(
    .INIT(64'hDFFFFFFF20000000)) 
    \i_V_reg_352[10]_i_1 
       (.I0(\p_s_reg_169_reg_n_0_[9] ),
        .I1(\i_V_reg_352[10]_i_2_n_0 ),
        .I2(\p_s_reg_169_reg_n_0_[6] ),
        .I3(\p_s_reg_169_reg_n_0_[7] ),
        .I4(\p_s_reg_169_reg_n_0_[8] ),
        .I5(\p_s_reg_169_reg_n_0_[10] ),
        .O(i_V_fu_278_p2[10]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \i_V_reg_352[10]_i_2 
       (.I0(\p_s_reg_169_reg_n_0_[4] ),
        .I1(\p_s_reg_169_reg_n_0_[3] ),
        .I2(\p_s_reg_169_reg_n_0_[2] ),
        .I3(\p_s_reg_169_reg_n_0_[1] ),
        .I4(\p_s_reg_169_reg_n_0_[0] ),
        .I5(\p_s_reg_169_reg_n_0_[5] ),
        .O(\i_V_reg_352[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_352[1]_i_1 
       (.I0(\p_s_reg_169_reg_n_0_[0] ),
        .I1(\p_s_reg_169_reg_n_0_[1] ),
        .O(i_V_fu_278_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_V_reg_352[2]_i_1 
       (.I0(\p_s_reg_169_reg_n_0_[0] ),
        .I1(\p_s_reg_169_reg_n_0_[1] ),
        .I2(\p_s_reg_169_reg_n_0_[2] ),
        .O(i_V_fu_278_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_V_reg_352[3]_i_1 
       (.I0(\p_s_reg_169_reg_n_0_[2] ),
        .I1(\p_s_reg_169_reg_n_0_[1] ),
        .I2(\p_s_reg_169_reg_n_0_[0] ),
        .I3(\p_s_reg_169_reg_n_0_[3] ),
        .O(i_V_fu_278_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_V_reg_352[4]_i_1 
       (.I0(\p_s_reg_169_reg_n_0_[0] ),
        .I1(\p_s_reg_169_reg_n_0_[1] ),
        .I2(\p_s_reg_169_reg_n_0_[2] ),
        .I3(\p_s_reg_169_reg_n_0_[3] ),
        .I4(\p_s_reg_169_reg_n_0_[4] ),
        .O(i_V_fu_278_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_V_reg_352[5]_i_1 
       (.I0(\p_s_reg_169_reg_n_0_[4] ),
        .I1(\p_s_reg_169_reg_n_0_[3] ),
        .I2(\p_s_reg_169_reg_n_0_[2] ),
        .I3(\p_s_reg_169_reg_n_0_[1] ),
        .I4(\p_s_reg_169_reg_n_0_[0] ),
        .I5(\p_s_reg_169_reg_n_0_[5] ),
        .O(i_V_fu_278_p2[5]));
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    \i_V_reg_352[6]_i_1 
       (.I0(\p_s_reg_169_reg_n_0_[5] ),
        .I1(\i_V_reg_352[7]_i_2_n_0 ),
        .I2(\p_s_reg_169_reg_n_0_[3] ),
        .I3(\p_s_reg_169_reg_n_0_[4] ),
        .I4(\p_s_reg_169_reg_n_0_[6] ),
        .O(i_V_fu_278_p2[6]));
  LUT6 #(
    .INIT(64'hFF7FFFFF00800000)) 
    \i_V_reg_352[7]_i_1 
       (.I0(\p_s_reg_169_reg_n_0_[6] ),
        .I1(\p_s_reg_169_reg_n_0_[4] ),
        .I2(\p_s_reg_169_reg_n_0_[3] ),
        .I3(\i_V_reg_352[7]_i_2_n_0 ),
        .I4(\p_s_reg_169_reg_n_0_[5] ),
        .I5(\p_s_reg_169_reg_n_0_[7] ),
        .O(i_V_fu_278_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \i_V_reg_352[7]_i_2 
       (.I0(\p_s_reg_169_reg_n_0_[0] ),
        .I1(\p_s_reg_169_reg_n_0_[1] ),
        .I2(\p_s_reg_169_reg_n_0_[2] ),
        .O(\i_V_reg_352[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \i_V_reg_352[8]_i_1 
       (.I0(\i_V_reg_352[10]_i_2_n_0 ),
        .I1(\p_s_reg_169_reg_n_0_[6] ),
        .I2(\p_s_reg_169_reg_n_0_[7] ),
        .I3(\p_s_reg_169_reg_n_0_[8] ),
        .O(i_V_fu_278_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hFF7F0080)) 
    \i_V_reg_352[9]_i_1 
       (.I0(\p_s_reg_169_reg_n_0_[8] ),
        .I1(\p_s_reg_169_reg_n_0_[7] ),
        .I2(\p_s_reg_169_reg_n_0_[6] ),
        .I3(\i_V_reg_352[10]_i_2_n_0 ),
        .I4(\p_s_reg_169_reg_n_0_[9] ),
        .O(i_V_fu_278_p2[9]));
  FDRE \i_V_reg_352_reg[0] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[0]),
        .Q(i_V_reg_352[0]),
        .R(1'b0));
  FDRE \i_V_reg_352_reg[10] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[10]),
        .Q(i_V_reg_352[10]),
        .R(1'b0));
  FDRE \i_V_reg_352_reg[1] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[1]),
        .Q(i_V_reg_352[1]),
        .R(1'b0));
  FDRE \i_V_reg_352_reg[2] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[2]),
        .Q(i_V_reg_352[2]),
        .R(1'b0));
  FDRE \i_V_reg_352_reg[3] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[3]),
        .Q(i_V_reg_352[3]),
        .R(1'b0));
  FDRE \i_V_reg_352_reg[4] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[4]),
        .Q(i_V_reg_352[4]),
        .R(1'b0));
  FDRE \i_V_reg_352_reg[5] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[5]),
        .Q(i_V_reg_352[5]),
        .R(1'b0));
  FDRE \i_V_reg_352_reg[6] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[6]),
        .Q(i_V_reg_352[6]),
        .R(1'b0));
  FDRE \i_V_reg_352_reg[7] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[7]),
        .Q(i_V_reg_352[7]),
        .R(1'b0));
  FDRE \i_V_reg_352_reg[8] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[8]),
        .Q(i_V_reg_352[8]),
        .R(1'b0));
  FDRE \i_V_reg_352_reg[9] 
       (.C(ap_clk),
        .CE(i_V_reg_3520),
        .D(i_V_fu_278_p2[9]),
        .Q(i_V_reg_352[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h0D)) 
    \image_in_V_data_V_0_payload_A[23]_i_1 
       (.I0(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I1(image_in_V_data_V_0_ack_in),
        .I2(image_in_V_data_V_0_sel_wr),
        .O(image_in_V_data_V_0_load_A));
  FDRE \image_in_V_data_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[0]),
        .Q(image_in_V_data_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[10]),
        .Q(image_in_V_data_V_0_payload_A[10]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[11]),
        .Q(image_in_V_data_V_0_payload_A[11]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[12]),
        .Q(image_in_V_data_V_0_payload_A[12]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[13]),
        .Q(image_in_V_data_V_0_payload_A[13]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[14]),
        .Q(image_in_V_data_V_0_payload_A[14]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[15]),
        .Q(image_in_V_data_V_0_payload_A[15]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[16]),
        .Q(image_in_V_data_V_0_payload_A[16]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[17]),
        .Q(image_in_V_data_V_0_payload_A[17]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[18]),
        .Q(image_in_V_data_V_0_payload_A[18]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[19]),
        .Q(image_in_V_data_V_0_payload_A[19]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[1]),
        .Q(image_in_V_data_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[20]),
        .Q(image_in_V_data_V_0_payload_A[20]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[21]),
        .Q(image_in_V_data_V_0_payload_A[21]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[22]),
        .Q(image_in_V_data_V_0_payload_A[22]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[23]),
        .Q(image_in_V_data_V_0_payload_A[23]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[2]),
        .Q(image_in_V_data_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[3]),
        .Q(image_in_V_data_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[4]),
        .Q(image_in_V_data_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[5]),
        .Q(image_in_V_data_V_0_payload_A[5]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[6]),
        .Q(image_in_V_data_V_0_payload_A[6]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[7]),
        .Q(image_in_V_data_V_0_payload_A[7]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[8]),
        .Q(image_in_V_data_V_0_payload_A[8]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_A),
        .D(image_in_TDATA[9]),
        .Q(image_in_V_data_V_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \image_in_V_data_V_0_payload_B[23]_i_1 
       (.I0(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I1(image_in_V_data_V_0_ack_in),
        .I2(image_in_V_data_V_0_sel_wr),
        .O(image_in_V_data_V_0_load_B));
  FDRE \image_in_V_data_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[0]),
        .Q(image_in_V_data_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[10]),
        .Q(image_in_V_data_V_0_payload_B[10]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[11]),
        .Q(image_in_V_data_V_0_payload_B[11]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[12]),
        .Q(image_in_V_data_V_0_payload_B[12]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[13]),
        .Q(image_in_V_data_V_0_payload_B[13]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[14]),
        .Q(image_in_V_data_V_0_payload_B[14]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[15]),
        .Q(image_in_V_data_V_0_payload_B[15]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[16]),
        .Q(image_in_V_data_V_0_payload_B[16]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[17]),
        .Q(image_in_V_data_V_0_payload_B[17]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[18]),
        .Q(image_in_V_data_V_0_payload_B[18]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[19]),
        .Q(image_in_V_data_V_0_payload_B[19]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[1]),
        .Q(image_in_V_data_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[20]),
        .Q(image_in_V_data_V_0_payload_B[20]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[21]),
        .Q(image_in_V_data_V_0_payload_B[21]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[22]),
        .Q(image_in_V_data_V_0_payload_B[22]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[23]),
        .Q(image_in_V_data_V_0_payload_B[23]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[2]),
        .Q(image_in_V_data_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[3]),
        .Q(image_in_V_data_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[4]),
        .Q(image_in_V_data_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[5]),
        .Q(image_in_V_data_V_0_payload_B[5]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[6]),
        .Q(image_in_V_data_V_0_payload_B[6]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[7]),
        .Q(image_in_V_data_V_0_payload_B[7]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[8]),
        .Q(image_in_V_data_V_0_payload_B[8]),
        .R(1'b0));
  FDRE \image_in_V_data_V_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(image_in_V_data_V_0_load_B),
        .D(image_in_TDATA[9]),
        .Q(image_in_V_data_V_0_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_data_V_0_sel_rd_i_1
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_data_V_0_sel),
        .O(image_in_V_data_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_data_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_data_V_0_sel_rd_i_1_n_0),
        .Q(image_in_V_data_V_0_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_data_V_0_sel_wr_i_1
       (.I0(image_in_TVALID),
        .I1(image_in_V_data_V_0_ack_in),
        .I2(image_in_V_data_V_0_sel_wr),
        .O(image_in_V_data_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_data_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_data_V_0_sel_wr_i_1_n_0),
        .Q(image_in_V_data_V_0_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFC4C0000)) 
    \image_in_V_data_V_0_state[0]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_data_V_0_ack_in),
        .I3(image_in_TVALID),
        .I4(ap_rst_n),
        .O(\image_in_V_data_V_0_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_in_V_data_V_0_state[1]_i_1 
       (.I0(image_in_TVALID),
        .I1(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_data_V_0_ack_out),
        .I3(image_in_V_data_V_0_ack_in),
        .O(image_in_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_data_V_0_state[0]_i_1_n_0 ),
        .Q(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_data_V_0_state),
        .Q(image_in_V_data_V_0_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_in_V_dest_V_0_payload_A[0]_i_1 
       (.I0(image_in_TDEST),
        .I1(\image_in_V_dest_V_0_state_reg_n_0_[0] ),
        .I2(image_in_TREADY),
        .I3(image_in_V_dest_V_0_sel_wr),
        .I4(image_in_V_dest_V_0_payload_A),
        .O(\image_in_V_dest_V_0_payload_A[0]_i_1_n_0 ));
  FDRE \image_in_V_dest_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_dest_V_0_payload_A[0]_i_1_n_0 ),
        .Q(image_in_V_dest_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_in_V_dest_V_0_payload_B[0]_i_1 
       (.I0(image_in_TDEST),
        .I1(\image_in_V_dest_V_0_state_reg_n_0_[0] ),
        .I2(image_in_TREADY),
        .I3(image_in_V_dest_V_0_sel_wr),
        .I4(image_in_V_dest_V_0_payload_B),
        .O(\image_in_V_dest_V_0_payload_B[0]_i_1_n_0 ));
  FDRE \image_in_V_dest_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_dest_V_0_payload_B[0]_i_1_n_0 ),
        .Q(image_in_V_dest_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_dest_V_0_sel_rd_i_1
       (.I0(\image_in_V_dest_V_0_state_reg_n_0_[0] ),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_in_V_dest_V_0_sel),
        .O(image_in_V_dest_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_dest_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_dest_V_0_sel_rd_i_1_n_0),
        .Q(image_in_V_dest_V_0_sel),
        .R(ap_rst_n_inv));
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_dest_V_0_sel_wr_i_1
       (.I0(image_in_TVALID),
        .I1(image_in_TREADY),
        .I2(image_in_V_dest_V_0_sel_wr),
        .O(image_in_V_dest_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_dest_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_dest_V_0_sel_wr_i_1_n_0),
        .Q(image_in_V_dest_V_0_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFC4C0000)) 
    \image_in_V_dest_V_0_state[0]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_dest_V_0_state_reg_n_0_[0] ),
        .I2(image_in_TREADY),
        .I3(image_in_TVALID),
        .I4(ap_rst_n),
        .O(\image_in_V_dest_V_0_state[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \image_in_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_in_V_dest_V_0_state[1]_i_2 
       (.I0(image_in_TVALID),
        .I1(\image_in_V_dest_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_data_V_0_ack_out),
        .I3(image_in_TREADY),
        .O(image_in_V_dest_V_0_state));
  LUT6 #(
    .INIT(64'hFF000000F8000000)) 
    \image_in_V_dest_V_0_state[1]_i_3 
       (.I0(ap_CS_fsm_state7),
        .I1(crop_out_V_data_V_1_ack_in),
        .I2(ap_CS_fsm_state4),
        .I3(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I4(image_out_V_data_V_1_ack_in),
        .I5(ap_CS_fsm_state10),
        .O(image_in_V_data_V_0_ack_out));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_dest_V_0_state[0]_i_1_n_0 ),
        .Q(\image_in_V_dest_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_dest_V_0_state),
        .Q(image_in_TREADY),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_in_V_id_V_0_payload_A[0]_i_1 
       (.I0(image_in_TID),
        .I1(\image_in_V_id_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_id_V_0_ack_in),
        .I3(image_in_V_id_V_0_sel_wr),
        .I4(image_in_V_id_V_0_payload_A),
        .O(\image_in_V_id_V_0_payload_A[0]_i_1_n_0 ));
  FDRE \image_in_V_id_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_id_V_0_payload_A[0]_i_1_n_0 ),
        .Q(image_in_V_id_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_in_V_id_V_0_payload_B[0]_i_1 
       (.I0(image_in_TID),
        .I1(\image_in_V_id_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_id_V_0_ack_in),
        .I3(image_in_V_id_V_0_sel_wr),
        .I4(image_in_V_id_V_0_payload_B),
        .O(\image_in_V_id_V_0_payload_B[0]_i_1_n_0 ));
  FDRE \image_in_V_id_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_id_V_0_payload_B[0]_i_1_n_0 ),
        .Q(image_in_V_id_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_id_V_0_sel_rd_i_1
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_id_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_id_V_0_sel),
        .O(image_in_V_id_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_id_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_id_V_0_sel_rd_i_1_n_0),
        .Q(image_in_V_id_V_0_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_id_V_0_sel_wr_i_1
       (.I0(image_in_V_id_V_0_ack_in),
        .I1(image_in_TVALID),
        .I2(image_in_V_id_V_0_sel_wr),
        .O(image_in_V_id_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_id_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_id_V_0_sel_wr_i_1_n_0),
        .Q(image_in_V_id_V_0_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFC4C0000)) 
    \image_in_V_id_V_0_state[0]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_id_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_id_V_0_ack_in),
        .I3(image_in_TVALID),
        .I4(ap_rst_n),
        .O(\image_in_V_id_V_0_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_in_V_id_V_0_state[1]_i_1 
       (.I0(image_in_TVALID),
        .I1(\image_in_V_id_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_data_V_0_ack_out),
        .I3(image_in_V_id_V_0_ack_in),
        .O(image_in_V_id_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_id_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_id_V_0_state[0]_i_1_n_0 ),
        .Q(\image_in_V_id_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_id_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_id_V_0_state),
        .Q(image_in_V_id_V_0_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_in_V_keep_V_0_payload_A[0]_i_1 
       (.I0(image_in_TKEEP[0]),
        .I1(\image_in_V_keep_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_keep_V_0_ack_in),
        .I3(image_in_V_keep_V_0_sel_wr),
        .I4(image_in_V_keep_V_0_payload_A[0]),
        .O(\image_in_V_keep_V_0_payload_A[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_in_V_keep_V_0_payload_A[1]_i_1 
       (.I0(image_in_TKEEP[1]),
        .I1(\image_in_V_keep_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_keep_V_0_ack_in),
        .I3(image_in_V_keep_V_0_sel_wr),
        .I4(image_in_V_keep_V_0_payload_A[1]),
        .O(\image_in_V_keep_V_0_payload_A[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_in_V_keep_V_0_payload_A[2]_i_1 
       (.I0(image_in_TKEEP[2]),
        .I1(\image_in_V_keep_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_keep_V_0_ack_in),
        .I3(image_in_V_keep_V_0_sel_wr),
        .I4(image_in_V_keep_V_0_payload_A[2]),
        .O(\image_in_V_keep_V_0_payload_A[2]_i_1_n_0 ));
  FDRE \image_in_V_keep_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_keep_V_0_payload_A[0]_i_1_n_0 ),
        .Q(image_in_V_keep_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \image_in_V_keep_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_keep_V_0_payload_A[1]_i_1_n_0 ),
        .Q(image_in_V_keep_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \image_in_V_keep_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_keep_V_0_payload_A[2]_i_1_n_0 ),
        .Q(image_in_V_keep_V_0_payload_A[2]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_in_V_keep_V_0_payload_B[0]_i_1 
       (.I0(image_in_TKEEP[0]),
        .I1(\image_in_V_keep_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_keep_V_0_ack_in),
        .I3(image_in_V_keep_V_0_sel_wr),
        .I4(image_in_V_keep_V_0_payload_B[0]),
        .O(\image_in_V_keep_V_0_payload_B[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_in_V_keep_V_0_payload_B[1]_i_1 
       (.I0(image_in_TKEEP[1]),
        .I1(\image_in_V_keep_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_keep_V_0_ack_in),
        .I3(image_in_V_keep_V_0_sel_wr),
        .I4(image_in_V_keep_V_0_payload_B[1]),
        .O(\image_in_V_keep_V_0_payload_B[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_in_V_keep_V_0_payload_B[2]_i_1 
       (.I0(image_in_TKEEP[2]),
        .I1(\image_in_V_keep_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_keep_V_0_ack_in),
        .I3(image_in_V_keep_V_0_sel_wr),
        .I4(image_in_V_keep_V_0_payload_B[2]),
        .O(\image_in_V_keep_V_0_payload_B[2]_i_1_n_0 ));
  FDRE \image_in_V_keep_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_keep_V_0_payload_B[0]_i_1_n_0 ),
        .Q(image_in_V_keep_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \image_in_V_keep_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_keep_V_0_payload_B[1]_i_1_n_0 ),
        .Q(image_in_V_keep_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \image_in_V_keep_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_keep_V_0_payload_B[2]_i_1_n_0 ),
        .Q(image_in_V_keep_V_0_payload_B[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_keep_V_0_sel_rd_i_1
       (.I0(\image_in_V_keep_V_0_state_reg_n_0_[0] ),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_in_V_keep_V_0_sel),
        .O(image_in_V_keep_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_keep_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_keep_V_0_sel_rd_i_1_n_0),
        .Q(image_in_V_keep_V_0_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_keep_V_0_sel_wr_i_1
       (.I0(image_in_TVALID),
        .I1(image_in_V_keep_V_0_ack_in),
        .I2(image_in_V_keep_V_0_sel_wr),
        .O(image_in_V_keep_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_keep_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_keep_V_0_sel_wr_i_1_n_0),
        .Q(image_in_V_keep_V_0_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFC4C0000)) 
    \image_in_V_keep_V_0_state[0]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_keep_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_keep_V_0_ack_in),
        .I3(image_in_TVALID),
        .I4(ap_rst_n),
        .O(\image_in_V_keep_V_0_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_in_V_keep_V_0_state[1]_i_1 
       (.I0(image_in_TVALID),
        .I1(\image_in_V_keep_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_data_V_0_ack_out),
        .I3(image_in_V_keep_V_0_ack_in),
        .O(image_in_V_keep_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_keep_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_keep_V_0_state[0]_i_1_n_0 ),
        .Q(\image_in_V_keep_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_keep_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_keep_V_0_state),
        .Q(image_in_V_keep_V_0_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_in_V_last_V_0_payload_A[0]_i_1 
       (.I0(image_in_TLAST),
        .I1(\image_in_V_last_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_last_V_0_ack_in),
        .I3(image_in_V_last_V_0_sel_wr),
        .I4(image_in_V_last_V_0_payload_A),
        .O(\image_in_V_last_V_0_payload_A[0]_i_1_n_0 ));
  FDRE \image_in_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_last_V_0_payload_A[0]_i_1_n_0 ),
        .Q(image_in_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_in_V_last_V_0_payload_B[0]_i_1 
       (.I0(image_in_TLAST),
        .I1(\image_in_V_last_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_last_V_0_ack_in),
        .I3(image_in_V_last_V_0_sel_wr),
        .I4(image_in_V_last_V_0_payload_B),
        .O(\image_in_V_last_V_0_payload_B[0]_i_1_n_0 ));
  FDRE \image_in_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_last_V_0_payload_B[0]_i_1_n_0 ),
        .Q(image_in_V_last_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_last_V_0_sel_rd_i_1
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_last_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_last_V_0_sel),
        .O(image_in_V_last_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_last_V_0_sel_rd_i_1_n_0),
        .Q(image_in_V_last_V_0_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_last_V_0_sel_wr_i_1
       (.I0(image_in_TVALID),
        .I1(image_in_V_last_V_0_ack_in),
        .I2(image_in_V_last_V_0_sel_wr),
        .O(image_in_V_last_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_last_V_0_sel_wr_i_1_n_0),
        .Q(image_in_V_last_V_0_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFC4C0000)) 
    \image_in_V_last_V_0_state[0]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_last_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_last_V_0_ack_in),
        .I3(image_in_TVALID),
        .I4(ap_rst_n),
        .O(\image_in_V_last_V_0_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_in_V_last_V_0_state[1]_i_1 
       (.I0(image_in_TVALID),
        .I1(\image_in_V_last_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_data_V_0_ack_out),
        .I3(image_in_V_last_V_0_ack_in),
        .O(image_in_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_last_V_0_state[0]_i_1_n_0 ),
        .Q(\image_in_V_last_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_last_V_0_state),
        .Q(image_in_V_last_V_0_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_in_V_strb_V_0_payload_A[0]_i_1 
       (.I0(image_in_TSTRB[0]),
        .I1(\image_in_V_strb_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_strb_V_0_ack_in),
        .I3(image_in_V_strb_V_0_sel_wr),
        .I4(image_in_V_strb_V_0_payload_A[0]),
        .O(\image_in_V_strb_V_0_payload_A[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_in_V_strb_V_0_payload_A[1]_i_1 
       (.I0(image_in_TSTRB[1]),
        .I1(\image_in_V_strb_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_strb_V_0_ack_in),
        .I3(image_in_V_strb_V_0_sel_wr),
        .I4(image_in_V_strb_V_0_payload_A[1]),
        .O(\image_in_V_strb_V_0_payload_A[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_in_V_strb_V_0_payload_A[2]_i_1 
       (.I0(image_in_TSTRB[2]),
        .I1(\image_in_V_strb_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_strb_V_0_ack_in),
        .I3(image_in_V_strb_V_0_sel_wr),
        .I4(image_in_V_strb_V_0_payload_A[2]),
        .O(\image_in_V_strb_V_0_payload_A[2]_i_1_n_0 ));
  FDRE \image_in_V_strb_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_strb_V_0_payload_A[0]_i_1_n_0 ),
        .Q(image_in_V_strb_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \image_in_V_strb_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_strb_V_0_payload_A[1]_i_1_n_0 ),
        .Q(image_in_V_strb_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \image_in_V_strb_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_strb_V_0_payload_A[2]_i_1_n_0 ),
        .Q(image_in_V_strb_V_0_payload_A[2]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_in_V_strb_V_0_payload_B[0]_i_1 
       (.I0(image_in_TSTRB[0]),
        .I1(\image_in_V_strb_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_strb_V_0_ack_in),
        .I3(image_in_V_strb_V_0_sel_wr),
        .I4(image_in_V_strb_V_0_payload_B[0]),
        .O(\image_in_V_strb_V_0_payload_B[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_in_V_strb_V_0_payload_B[1]_i_1 
       (.I0(image_in_TSTRB[1]),
        .I1(\image_in_V_strb_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_strb_V_0_ack_in),
        .I3(image_in_V_strb_V_0_sel_wr),
        .I4(image_in_V_strb_V_0_payload_B[1]),
        .O(\image_in_V_strb_V_0_payload_B[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_in_V_strb_V_0_payload_B[2]_i_1 
       (.I0(image_in_TSTRB[2]),
        .I1(\image_in_V_strb_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_strb_V_0_ack_in),
        .I3(image_in_V_strb_V_0_sel_wr),
        .I4(image_in_V_strb_V_0_payload_B[2]),
        .O(\image_in_V_strb_V_0_payload_B[2]_i_1_n_0 ));
  FDRE \image_in_V_strb_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_strb_V_0_payload_B[0]_i_1_n_0 ),
        .Q(image_in_V_strb_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \image_in_V_strb_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_strb_V_0_payload_B[1]_i_1_n_0 ),
        .Q(image_in_V_strb_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \image_in_V_strb_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_strb_V_0_payload_B[2]_i_1_n_0 ),
        .Q(image_in_V_strb_V_0_payload_B[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_strb_V_0_sel_rd_i_1
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_strb_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_strb_V_0_sel),
        .O(image_in_V_strb_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_strb_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_strb_V_0_sel_rd_i_1_n_0),
        .Q(image_in_V_strb_V_0_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_strb_V_0_sel_wr_i_1
       (.I0(image_in_TVALID),
        .I1(image_in_V_strb_V_0_ack_in),
        .I2(image_in_V_strb_V_0_sel_wr),
        .O(image_in_V_strb_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_strb_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_strb_V_0_sel_wr_i_1_n_0),
        .Q(image_in_V_strb_V_0_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFC4C0000)) 
    \image_in_V_strb_V_0_state[0]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_strb_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_strb_V_0_ack_in),
        .I3(image_in_TVALID),
        .I4(ap_rst_n),
        .O(\image_in_V_strb_V_0_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_in_V_strb_V_0_state[1]_i_1 
       (.I0(image_in_TVALID),
        .I1(\image_in_V_strb_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_data_V_0_ack_out),
        .I3(image_in_V_strb_V_0_ack_in),
        .O(image_in_V_strb_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_strb_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_strb_V_0_state[0]_i_1_n_0 ),
        .Q(\image_in_V_strb_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_strb_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_strb_V_0_state),
        .Q(image_in_V_strb_V_0_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_in_V_user_V_0_payload_A[0]_i_1 
       (.I0(image_in_TUSER),
        .I1(\image_in_V_user_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_user_V_0_ack_in),
        .I3(image_in_V_user_V_0_sel_wr),
        .I4(image_in_V_user_V_0_payload_A),
        .O(\image_in_V_user_V_0_payload_A[0]_i_1_n_0 ));
  FDRE \image_in_V_user_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_user_V_0_payload_A[0]_i_1_n_0 ),
        .Q(image_in_V_user_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_in_V_user_V_0_payload_B[0]_i_1 
       (.I0(image_in_TUSER),
        .I1(\image_in_V_user_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_user_V_0_ack_in),
        .I3(image_in_V_user_V_0_sel_wr),
        .I4(image_in_V_user_V_0_payload_B),
        .O(\image_in_V_user_V_0_payload_B[0]_i_1_n_0 ));
  FDRE \image_in_V_user_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_user_V_0_payload_B[0]_i_1_n_0 ),
        .Q(image_in_V_user_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_user_V_0_sel_rd_i_1
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_user_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_user_V_0_sel),
        .O(image_in_V_user_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_user_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_user_V_0_sel_rd_i_1_n_0),
        .Q(image_in_V_user_V_0_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_in_V_user_V_0_sel_wr_i_1
       (.I0(image_in_TVALID),
        .I1(image_in_V_user_V_0_ack_in),
        .I2(image_in_V_user_V_0_sel_wr),
        .O(image_in_V_user_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_in_V_user_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_user_V_0_sel_wr_i_1_n_0),
        .Q(image_in_V_user_V_0_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFC4C0000)) 
    \image_in_V_user_V_0_state[0]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_in_V_user_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_user_V_0_ack_in),
        .I3(image_in_TVALID),
        .I4(ap_rst_n),
        .O(\image_in_V_user_V_0_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_in_V_user_V_0_state[1]_i_1 
       (.I0(image_in_TVALID),
        .I1(\image_in_V_user_V_0_state_reg_n_0_[0] ),
        .I2(image_in_V_data_V_0_ack_out),
        .I3(image_in_V_user_V_0_ack_in),
        .O(image_in_V_user_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_user_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_in_V_user_V_0_state[0]_i_1_n_0 ),
        .Q(\image_in_V_user_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_in_V_user_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_in_V_user_V_0_state),
        .Q(image_in_V_user_V_0_ack_in),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[0]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[0]),
        .I1(image_out_V_data_V_1_payload_A[0]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[0]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[10]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[10]),
        .I1(image_out_V_data_V_1_payload_A[10]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[10]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[11]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[11]),
        .I1(image_out_V_data_V_1_payload_A[11]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[11]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[12]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[12]),
        .I1(image_out_V_data_V_1_payload_A[12]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[12]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[13]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[13]),
        .I1(image_out_V_data_V_1_payload_A[13]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[13]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[14]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[14]),
        .I1(image_out_V_data_V_1_payload_A[14]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[14]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[15]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[15]),
        .I1(image_out_V_data_V_1_payload_A[15]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[15]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[16]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[16]),
        .I1(image_out_V_data_V_1_payload_A[16]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[16]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[17]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[17]),
        .I1(image_out_V_data_V_1_payload_A[17]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[17]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[18]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[18]),
        .I1(image_out_V_data_V_1_payload_A[18]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[18]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[19]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[19]),
        .I1(image_out_V_data_V_1_payload_A[19]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[19]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[1]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[1]),
        .I1(image_out_V_data_V_1_payload_A[1]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[1]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[20]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[20]),
        .I1(image_out_V_data_V_1_payload_A[20]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[20]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[21]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[21]),
        .I1(image_out_V_data_V_1_payload_A[21]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[21]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[22]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[22]),
        .I1(image_out_V_data_V_1_payload_A[22]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[22]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[23]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[23]),
        .I1(image_out_V_data_V_1_payload_A[23]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[23]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[2]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[2]),
        .I1(image_out_V_data_V_1_payload_A[2]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[3]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[3]),
        .I1(image_out_V_data_V_1_payload_A[3]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[3]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[4]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[4]),
        .I1(image_out_V_data_V_1_payload_A[4]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[4]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[5]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[5]),
        .I1(image_out_V_data_V_1_payload_A[5]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[5]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[6]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[6]),
        .I1(image_out_V_data_V_1_payload_A[6]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[6]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[7]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[7]),
        .I1(image_out_V_data_V_1_payload_A[7]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[7]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[8]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[8]),
        .I1(image_out_V_data_V_1_payload_A[8]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[8]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[9]_INST_0 
       (.I0(image_out_V_data_V_1_payload_B[9]),
        .I1(image_out_V_data_V_1_payload_A[9]),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_TDATA[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TDEST[0]_INST_0 
       (.I0(image_out_V_dest_V_1_payload_B),
        .I1(image_out_V_dest_V_1_sel),
        .I2(image_out_V_dest_V_1_payload_A),
        .O(image_out_TDEST));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TID[0]_INST_0 
       (.I0(image_out_V_id_V_1_payload_B),
        .I1(image_out_V_id_V_1_sel),
        .I2(image_out_V_id_V_1_payload_A),
        .O(image_out_TID));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TKEEP[0]_INST_0 
       (.I0(image_out_V_keep_V_1_payload_B[0]),
        .I1(image_out_V_keep_V_1_sel),
        .I2(image_out_V_keep_V_1_payload_A[0]),
        .O(image_out_TKEEP[0]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TKEEP[1]_INST_0 
       (.I0(image_out_V_keep_V_1_payload_B[1]),
        .I1(image_out_V_keep_V_1_sel),
        .I2(image_out_V_keep_V_1_payload_A[1]),
        .O(image_out_TKEEP[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TKEEP[2]_INST_0 
       (.I0(image_out_V_keep_V_1_payload_B[2]),
        .I1(image_out_V_keep_V_1_sel),
        .I2(image_out_V_keep_V_1_payload_A[2]),
        .O(image_out_TKEEP[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TLAST[0]_INST_0 
       (.I0(image_out_V_last_V_1_payload_B),
        .I1(image_out_V_last_V_1_sel),
        .I2(image_out_V_last_V_1_payload_A),
        .O(image_out_TLAST));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TSTRB[0]_INST_0 
       (.I0(image_out_V_strb_V_1_payload_B[0]),
        .I1(image_out_V_strb_V_1_sel),
        .I2(image_out_V_strb_V_1_payload_A[0]),
        .O(image_out_TSTRB[0]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TSTRB[1]_INST_0 
       (.I0(image_out_V_strb_V_1_payload_B[1]),
        .I1(image_out_V_strb_V_1_sel),
        .I2(image_out_V_strb_V_1_payload_A[1]),
        .O(image_out_TSTRB[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TSTRB[2]_INST_0 
       (.I0(image_out_V_strb_V_1_payload_B[2]),
        .I1(image_out_V_strb_V_1_sel),
        .I2(image_out_V_strb_V_1_payload_A[2]),
        .O(image_out_TSTRB[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TUSER[0]_INST_0 
       (.I0(image_out_V_user_V_1_payload_B),
        .I1(image_out_V_user_V_1_sel),
        .I2(image_out_V_user_V_1_payload_A),
        .O(image_out_TUSER));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[0]),
        .I1(image_in_V_data_V_0_payload_A[0]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[0]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[10]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[10]),
        .I1(image_in_V_data_V_0_payload_A[10]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[10]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[11]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[11]),
        .I1(image_in_V_data_V_0_payload_A[11]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[11]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[12]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[12]),
        .I1(image_in_V_data_V_0_payload_A[12]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[12]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[13]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[13]),
        .I1(image_in_V_data_V_0_payload_A[13]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[13]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[14]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[14]),
        .I1(image_in_V_data_V_0_payload_A[14]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[14]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[15]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[15]),
        .I1(image_in_V_data_V_0_payload_A[15]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[15]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[16]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[16]),
        .I1(image_in_V_data_V_0_payload_A[16]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[16]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[17]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[17]),
        .I1(image_in_V_data_V_0_payload_A[17]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[17]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[18]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[18]),
        .I1(image_in_V_data_V_0_payload_A[18]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[18]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[19]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[19]),
        .I1(image_in_V_data_V_0_payload_A[19]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[19]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[1]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[1]),
        .I1(image_in_V_data_V_0_payload_A[1]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[1]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[20]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[20]),
        .I1(image_in_V_data_V_0_payload_A[20]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[20]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[21]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[21]),
        .I1(image_in_V_data_V_0_payload_A[21]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[21]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[22]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[22]),
        .I1(image_in_V_data_V_0_payload_A[22]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[22]));
  LUT3 #(
    .INIT(8'h0D)) 
    \image_out_V_data_V_1_payload_A[23]_i_1 
       (.I0(\image_out_V_data_V_1_state_reg_n_0_[0] ),
        .I1(image_out_V_data_V_1_ack_in),
        .I2(image_out_V_data_V_1_sel_wr),
        .O(image_out_V_data_V_1_load_A));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[23]_i_2 
       (.I0(image_in_V_data_V_0_payload_B[23]),
        .I1(image_in_V_data_V_0_payload_A[23]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[23]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[2]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[2]),
        .I1(image_in_V_data_V_0_payload_A[2]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[2]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[3]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[3]),
        .I1(image_in_V_data_V_0_payload_A[3]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[3]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[4]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[4]),
        .I1(image_in_V_data_V_0_payload_A[4]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[4]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[5]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[5]),
        .I1(image_in_V_data_V_0_payload_A[5]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[5]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[6]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[6]),
        .I1(image_in_V_data_V_0_payload_A[6]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[6]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[7]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[7]),
        .I1(image_in_V_data_V_0_payload_A[7]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[7]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[8]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[8]),
        .I1(image_in_V_data_V_0_payload_A[8]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[8]));
  LUT6 #(
    .INIT(64'hA0A0A000C0C0C000)) 
    \image_out_V_data_V_1_payload_A[9]_i_1 
       (.I0(image_in_V_data_V_0_payload_B[9]),
        .I1(image_in_V_data_V_0_payload_A[9]),
        .I2(\image_in_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state7),
        .I4(ap_CS_fsm_state10),
        .I5(image_in_V_data_V_0_sel),
        .O(image_out_V_data_V_1_data_in[9]));
  FDRE \image_out_V_data_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[0]),
        .Q(image_out_V_data_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[10]),
        .Q(image_out_V_data_V_1_payload_A[10]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[11]),
        .Q(image_out_V_data_V_1_payload_A[11]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[12]),
        .Q(image_out_V_data_V_1_payload_A[12]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[13]),
        .Q(image_out_V_data_V_1_payload_A[13]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[14]),
        .Q(image_out_V_data_V_1_payload_A[14]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[15]),
        .Q(image_out_V_data_V_1_payload_A[15]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[16]),
        .Q(image_out_V_data_V_1_payload_A[16]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[17]),
        .Q(image_out_V_data_V_1_payload_A[17]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[18]),
        .Q(image_out_V_data_V_1_payload_A[18]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[19]),
        .Q(image_out_V_data_V_1_payload_A[19]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[1]),
        .Q(image_out_V_data_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[20]),
        .Q(image_out_V_data_V_1_payload_A[20]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[21]),
        .Q(image_out_V_data_V_1_payload_A[21]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[22]),
        .Q(image_out_V_data_V_1_payload_A[22]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[23]),
        .Q(image_out_V_data_V_1_payload_A[23]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[2]),
        .Q(image_out_V_data_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[3]),
        .Q(image_out_V_data_V_1_payload_A[3]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[4]),
        .Q(image_out_V_data_V_1_payload_A[4]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[5]),
        .Q(image_out_V_data_V_1_payload_A[5]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[6]),
        .Q(image_out_V_data_V_1_payload_A[6]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[7]),
        .Q(image_out_V_data_V_1_payload_A[7]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[8]),
        .Q(image_out_V_data_V_1_payload_A[8]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_A),
        .D(image_out_V_data_V_1_data_in[9]),
        .Q(image_out_V_data_V_1_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \image_out_V_data_V_1_payload_B[23]_i_1 
       (.I0(\image_out_V_data_V_1_state_reg_n_0_[0] ),
        .I1(image_out_V_data_V_1_ack_in),
        .I2(image_out_V_data_V_1_sel_wr),
        .O(image_out_V_data_V_1_load_B));
  FDRE \image_out_V_data_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[0]),
        .Q(image_out_V_data_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[10]),
        .Q(image_out_V_data_V_1_payload_B[10]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[11]),
        .Q(image_out_V_data_V_1_payload_B[11]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[12]),
        .Q(image_out_V_data_V_1_payload_B[12]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[13]),
        .Q(image_out_V_data_V_1_payload_B[13]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[14]),
        .Q(image_out_V_data_V_1_payload_B[14]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[15]),
        .Q(image_out_V_data_V_1_payload_B[15]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[16]),
        .Q(image_out_V_data_V_1_payload_B[16]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[17]),
        .Q(image_out_V_data_V_1_payload_B[17]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[18]),
        .Q(image_out_V_data_V_1_payload_B[18]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[19]),
        .Q(image_out_V_data_V_1_payload_B[19]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[1]),
        .Q(image_out_V_data_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[20]),
        .Q(image_out_V_data_V_1_payload_B[20]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[21]),
        .Q(image_out_V_data_V_1_payload_B[21]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[22]),
        .Q(image_out_V_data_V_1_payload_B[22]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[23]),
        .Q(image_out_V_data_V_1_payload_B[23]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[2]),
        .Q(image_out_V_data_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[3]),
        .Q(image_out_V_data_V_1_payload_B[3]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[4]),
        .Q(image_out_V_data_V_1_payload_B[4]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[5]),
        .Q(image_out_V_data_V_1_payload_B[5]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[6]),
        .Q(image_out_V_data_V_1_payload_B[6]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[7]),
        .Q(image_out_V_data_V_1_payload_B[7]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[8]),
        .Q(image_out_V_data_V_1_payload_B[8]),
        .R(1'b0));
  FDRE \image_out_V_data_V_1_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(image_out_V_data_V_1_load_B),
        .D(image_out_V_data_V_1_data_in[9]),
        .Q(image_out_V_data_V_1_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_data_V_1_sel_rd_i_1
       (.I0(image_out_TREADY),
        .I1(\image_out_V_data_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_data_V_1_sel),
        .O(image_out_V_data_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_data_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_data_V_1_sel_rd_i_1_n_0),
        .Q(image_out_V_data_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_data_V_1_sel_wr_i_1
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(image_out_V_data_V_1_ack_in),
        .I2(image_out_V_data_V_1_sel_wr),
        .O(image_out_V_data_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_data_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_data_V_1_sel_wr_i_1_n_0),
        .Q(image_out_V_data_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \image_out_V_data_V_1_state[0]_i_1 
       (.I0(image_out_TREADY),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_data_V_1_ack_in),
        .I3(\image_out_V_data_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\image_out_V_data_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_out_V_data_V_1_state[1]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_out_V_data_V_1_state_reg_n_0_[0] ),
        .I2(image_out_TREADY),
        .I3(image_out_V_data_V_1_ack_in),
        .O(image_out_V_data_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_data_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_data_V_1_state[0]_i_1_n_0 ),
        .Q(\image_out_V_data_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_data_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_data_V_1_state),
        .Q(image_out_V_data_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_out_V_dest_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_dest_V_0_data_out),
        .I1(image_out_TVALID),
        .I2(image_out_V_dest_V_1_ack_in),
        .I3(image_out_V_dest_V_1_sel_wr),
        .I4(image_out_V_dest_V_1_payload_A),
        .O(\image_out_V_dest_V_1_payload_A[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_V_dest_V_1_payload_A[0]_i_2 
       (.I0(image_in_V_dest_V_0_payload_B),
        .I1(image_in_V_dest_V_0_sel),
        .I2(image_in_V_dest_V_0_payload_A),
        .O(image_in_V_dest_V_0_data_out));
  FDRE \image_out_V_dest_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_dest_V_1_payload_A[0]_i_1_n_0 ),
        .Q(image_out_V_dest_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_out_V_dest_V_1_payload_B[0]_i_1 
       (.I0(image_in_V_dest_V_0_data_out),
        .I1(image_out_TVALID),
        .I2(image_out_V_dest_V_1_ack_in),
        .I3(image_out_V_dest_V_1_sel_wr),
        .I4(image_out_V_dest_V_1_payload_B),
        .O(\image_out_V_dest_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \image_out_V_dest_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_dest_V_1_payload_B[0]_i_1_n_0 ),
        .Q(image_out_V_dest_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_dest_V_1_sel_rd_i_1
       (.I0(image_out_TREADY),
        .I1(image_out_TVALID),
        .I2(image_out_V_dest_V_1_sel),
        .O(image_out_V_dest_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_dest_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_dest_V_1_sel_rd_i_1_n_0),
        .Q(image_out_V_dest_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_dest_V_1_sel_wr_i_1
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(image_out_V_dest_V_1_ack_in),
        .I2(image_out_V_dest_V_1_sel_wr),
        .O(image_out_V_dest_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_dest_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_dest_V_1_sel_wr_i_1_n_0),
        .Q(image_out_V_dest_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \image_out_V_dest_V_1_state[0]_i_1 
       (.I0(image_out_TREADY),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_dest_V_1_ack_in),
        .I3(image_out_TVALID),
        .I4(ap_rst_n),
        .O(\image_out_V_dest_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_out_V_dest_V_1_state[1]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(image_out_TVALID),
        .I2(image_out_TREADY),
        .I3(image_out_V_dest_V_1_ack_in),
        .O(image_out_V_dest_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_dest_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_dest_V_1_state[0]_i_1_n_0 ),
        .Q(image_out_TVALID),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_dest_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_dest_V_1_state),
        .Q(image_out_V_dest_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_out_V_id_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_id_V_0_data_out),
        .I1(\image_out_V_id_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_id_V_1_ack_in),
        .I3(image_out_V_id_V_1_sel_wr),
        .I4(image_out_V_id_V_1_payload_A),
        .O(\image_out_V_id_V_1_payload_A[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_V_id_V_1_payload_A[0]_i_2 
       (.I0(image_in_V_id_V_0_payload_B),
        .I1(image_in_V_id_V_0_sel),
        .I2(image_in_V_id_V_0_payload_A),
        .O(image_in_V_id_V_0_data_out));
  FDRE \image_out_V_id_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_id_V_1_payload_A[0]_i_1_n_0 ),
        .Q(image_out_V_id_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_out_V_id_V_1_payload_B[0]_i_1 
       (.I0(image_in_V_id_V_0_data_out),
        .I1(\image_out_V_id_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_id_V_1_ack_in),
        .I3(image_out_V_id_V_1_sel_wr),
        .I4(image_out_V_id_V_1_payload_B),
        .O(\image_out_V_id_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \image_out_V_id_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_id_V_1_payload_B[0]_i_1_n_0 ),
        .Q(image_out_V_id_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_id_V_1_sel_rd_i_1
       (.I0(\image_out_V_id_V_1_state_reg_n_0_[0] ),
        .I1(image_out_TREADY),
        .I2(image_out_V_id_V_1_sel),
        .O(image_out_V_id_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_id_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_id_V_1_sel_rd_i_1_n_0),
        .Q(image_out_V_id_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_id_V_1_sel_wr_i_1
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(image_out_V_id_V_1_ack_in),
        .I2(image_out_V_id_V_1_sel_wr),
        .O(image_out_V_id_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_id_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_id_V_1_sel_wr_i_1_n_0),
        .Q(image_out_V_id_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \image_out_V_id_V_1_state[0]_i_1 
       (.I0(image_out_TREADY),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_id_V_1_ack_in),
        .I3(\image_out_V_id_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\image_out_V_id_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_out_V_id_V_1_state[1]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_out_V_id_V_1_state_reg_n_0_[0] ),
        .I2(image_out_TREADY),
        .I3(image_out_V_id_V_1_ack_in),
        .O(image_out_V_id_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_id_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_id_V_1_state[0]_i_1_n_0 ),
        .Q(\image_out_V_id_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_id_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_id_V_1_state),
        .Q(image_out_V_id_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_out_V_keep_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[0]),
        .I1(\image_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_keep_V_1_ack_in),
        .I3(image_out_V_keep_V_1_sel_wr),
        .I4(image_out_V_keep_V_1_payload_A[0]),
        .O(\image_out_V_keep_V_1_payload_A[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_V_keep_V_1_payload_A[0]_i_2 
       (.I0(image_in_V_keep_V_0_payload_B[0]),
        .I1(image_in_V_keep_V_0_sel),
        .I2(image_in_V_keep_V_0_payload_A[0]),
        .O(image_in_V_keep_V_0_data_out[0]));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_out_V_keep_V_1_payload_A[1]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[1]),
        .I1(\image_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_keep_V_1_ack_in),
        .I3(image_out_V_keep_V_1_sel_wr),
        .I4(image_out_V_keep_V_1_payload_A[1]),
        .O(\image_out_V_keep_V_1_payload_A[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_V_keep_V_1_payload_A[1]_i_2 
       (.I0(image_in_V_keep_V_0_payload_B[1]),
        .I1(image_in_V_keep_V_0_sel),
        .I2(image_in_V_keep_V_0_payload_A[1]),
        .O(image_in_V_keep_V_0_data_out[1]));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_out_V_keep_V_1_payload_A[2]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[2]),
        .I1(\image_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_keep_V_1_ack_in),
        .I3(image_out_V_keep_V_1_sel_wr),
        .I4(image_out_V_keep_V_1_payload_A[2]),
        .O(\image_out_V_keep_V_1_payload_A[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_V_keep_V_1_payload_A[2]_i_2 
       (.I0(image_in_V_keep_V_0_payload_B[2]),
        .I1(image_in_V_keep_V_0_sel),
        .I2(image_in_V_keep_V_0_payload_A[2]),
        .O(image_in_V_keep_V_0_data_out[2]));
  FDRE \image_out_V_keep_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_keep_V_1_payload_A[0]_i_1_n_0 ),
        .Q(image_out_V_keep_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \image_out_V_keep_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_keep_V_1_payload_A[1]_i_1_n_0 ),
        .Q(image_out_V_keep_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \image_out_V_keep_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_keep_V_1_payload_A[2]_i_1_n_0 ),
        .Q(image_out_V_keep_V_1_payload_A[2]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_out_V_keep_V_1_payload_B[0]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[0]),
        .I1(\image_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_keep_V_1_ack_in),
        .I3(image_out_V_keep_V_1_sel_wr),
        .I4(image_out_V_keep_V_1_payload_B[0]),
        .O(\image_out_V_keep_V_1_payload_B[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_out_V_keep_V_1_payload_B[1]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[1]),
        .I1(\image_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_keep_V_1_ack_in),
        .I3(image_out_V_keep_V_1_sel_wr),
        .I4(image_out_V_keep_V_1_payload_B[1]),
        .O(\image_out_V_keep_V_1_payload_B[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_out_V_keep_V_1_payload_B[2]_i_1 
       (.I0(image_in_V_keep_V_0_data_out[2]),
        .I1(\image_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_keep_V_1_ack_in),
        .I3(image_out_V_keep_V_1_sel_wr),
        .I4(image_out_V_keep_V_1_payload_B[2]),
        .O(\image_out_V_keep_V_1_payload_B[2]_i_1_n_0 ));
  FDRE \image_out_V_keep_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_keep_V_1_payload_B[0]_i_1_n_0 ),
        .Q(image_out_V_keep_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \image_out_V_keep_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_keep_V_1_payload_B[1]_i_1_n_0 ),
        .Q(image_out_V_keep_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \image_out_V_keep_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_keep_V_1_payload_B[2]_i_1_n_0 ),
        .Q(image_out_V_keep_V_1_payload_B[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_keep_V_1_sel_rd_i_1
       (.I0(\image_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I1(image_out_TREADY),
        .I2(image_out_V_keep_V_1_sel),
        .O(image_out_V_keep_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_keep_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_keep_V_1_sel_rd_i_1_n_0),
        .Q(image_out_V_keep_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_keep_V_1_sel_wr_i_1
       (.I0(image_out_V_keep_V_1_ack_in),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_keep_V_1_sel_wr),
        .O(image_out_V_keep_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_keep_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_keep_V_1_sel_wr_i_1_n_0),
        .Q(image_out_V_keep_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \image_out_V_keep_V_1_state[0]_i_1 
       (.I0(image_out_TREADY),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_keep_V_1_ack_in),
        .I3(\image_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\image_out_V_keep_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_out_V_keep_V_1_state[1]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(image_out_TREADY),
        .I3(image_out_V_keep_V_1_ack_in),
        .O(image_out_V_keep_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_keep_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_keep_V_1_state[0]_i_1_n_0 ),
        .Q(\image_out_V_keep_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_keep_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_keep_V_1_state),
        .Q(image_out_V_keep_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    \image_out_V_last_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_last_V_0_payload_B),
        .I1(image_in_V_last_V_0_sel),
        .I2(image_in_V_last_V_0_payload_A),
        .I3(\image_out_V_last_V_1_payload_A[0]_i_2_n_0 ),
        .I4(image_out_V_last_V_1_sel_wr),
        .I5(image_out_V_last_V_1_payload_A),
        .O(\image_out_V_last_V_1_payload_A[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \image_out_V_last_V_1_payload_A[0]_i_2 
       (.I0(image_out_V_last_V_1_ack_in),
        .I1(\image_out_V_last_V_1_state_reg_n_0_[0] ),
        .O(\image_out_V_last_V_1_payload_A[0]_i_2_n_0 ));
  FDRE \image_out_V_last_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_last_V_1_payload_A[0]_i_1_n_0 ),
        .Q(image_out_V_last_V_1_payload_A),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hB8FFFFFFB8000000)) 
    \image_out_V_last_V_1_payload_B[0]_i_1 
       (.I0(image_in_V_last_V_0_payload_B),
        .I1(image_in_V_last_V_0_sel),
        .I2(image_in_V_last_V_0_payload_A),
        .I3(\image_out_V_last_V_1_payload_A[0]_i_2_n_0 ),
        .I4(image_out_V_last_V_1_sel_wr),
        .I5(image_out_V_last_V_1_payload_B),
        .O(\image_out_V_last_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \image_out_V_last_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_last_V_1_payload_B[0]_i_1_n_0 ),
        .Q(image_out_V_last_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_last_V_1_sel_rd_i_1
       (.I0(\image_out_V_last_V_1_state_reg_n_0_[0] ),
        .I1(image_out_TREADY),
        .I2(image_out_V_last_V_1_sel),
        .O(image_out_V_last_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_last_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_last_V_1_sel_rd_i_1_n_0),
        .Q(image_out_V_last_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_last_V_1_sel_wr_i_1
       (.I0(image_out_V_last_V_1_ack_in),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_last_V_1_sel_wr),
        .O(image_out_V_last_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_last_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_last_V_1_sel_wr_i_1_n_0),
        .Q(image_out_V_last_V_1_sel_wr),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \image_out_V_last_V_1_state[0]_i_1 
       (.I0(image_out_TREADY),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_last_V_1_ack_in),
        .I3(\image_out_V_last_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\image_out_V_last_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_out_V_last_V_1_state[1]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_out_V_last_V_1_state_reg_n_0_[0] ),
        .I2(image_out_TREADY),
        .I3(image_out_V_last_V_1_ack_in),
        .O(image_out_V_last_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_last_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_last_V_1_state[0]_i_1_n_0 ),
        .Q(\image_out_V_last_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_last_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_last_V_1_state),
        .Q(image_out_V_last_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_out_V_strb_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[0]),
        .I1(\image_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_strb_V_1_ack_in),
        .I3(image_out_V_strb_V_1_sel_wr),
        .I4(image_out_V_strb_V_1_payload_A[0]),
        .O(\image_out_V_strb_V_1_payload_A[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_V_strb_V_1_payload_A[0]_i_2 
       (.I0(image_in_V_strb_V_0_payload_B[0]),
        .I1(image_in_V_strb_V_0_sel),
        .I2(image_in_V_strb_V_0_payload_A[0]),
        .O(image_in_V_strb_V_0_data_out[0]));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_out_V_strb_V_1_payload_A[1]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[1]),
        .I1(\image_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_strb_V_1_ack_in),
        .I3(image_out_V_strb_V_1_sel_wr),
        .I4(image_out_V_strb_V_1_payload_A[1]),
        .O(\image_out_V_strb_V_1_payload_A[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_V_strb_V_1_payload_A[1]_i_2 
       (.I0(image_in_V_strb_V_0_payload_B[1]),
        .I1(image_in_V_strb_V_0_sel),
        .I2(image_in_V_strb_V_0_payload_A[1]),
        .O(image_in_V_strb_V_0_data_out[1]));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \image_out_V_strb_V_1_payload_A[2]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[2]),
        .I1(\image_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_strb_V_1_ack_in),
        .I3(image_out_V_strb_V_1_sel_wr),
        .I4(image_out_V_strb_V_1_payload_A[2]),
        .O(\image_out_V_strb_V_1_payload_A[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_V_strb_V_1_payload_A[2]_i_2 
       (.I0(image_in_V_strb_V_0_payload_B[2]),
        .I1(image_in_V_strb_V_0_sel),
        .I2(image_in_V_strb_V_0_payload_A[2]),
        .O(image_in_V_strb_V_0_data_out[2]));
  FDRE \image_out_V_strb_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_strb_V_1_payload_A[0]_i_1_n_0 ),
        .Q(image_out_V_strb_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \image_out_V_strb_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_strb_V_1_payload_A[1]_i_1_n_0 ),
        .Q(image_out_V_strb_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \image_out_V_strb_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_strb_V_1_payload_A[2]_i_1_n_0 ),
        .Q(image_out_V_strb_V_1_payload_A[2]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_out_V_strb_V_1_payload_B[0]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[0]),
        .I1(\image_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_strb_V_1_ack_in),
        .I3(image_out_V_strb_V_1_sel_wr),
        .I4(image_out_V_strb_V_1_payload_B[0]),
        .O(\image_out_V_strb_V_1_payload_B[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_out_V_strb_V_1_payload_B[1]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[1]),
        .I1(\image_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_strb_V_1_ack_in),
        .I3(image_out_V_strb_V_1_sel_wr),
        .I4(image_out_V_strb_V_1_payload_B[1]),
        .O(\image_out_V_strb_V_1_payload_B[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \image_out_V_strb_V_1_payload_B[2]_i_1 
       (.I0(image_in_V_strb_V_0_data_out[2]),
        .I1(\image_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(image_out_V_strb_V_1_ack_in),
        .I3(image_out_V_strb_V_1_sel_wr),
        .I4(image_out_V_strb_V_1_payload_B[2]),
        .O(\image_out_V_strb_V_1_payload_B[2]_i_1_n_0 ));
  FDRE \image_out_V_strb_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_strb_V_1_payload_B[0]_i_1_n_0 ),
        .Q(image_out_V_strb_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \image_out_V_strb_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_strb_V_1_payload_B[1]_i_1_n_0 ),
        .Q(image_out_V_strb_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \image_out_V_strb_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_strb_V_1_payload_B[2]_i_1_n_0 ),
        .Q(image_out_V_strb_V_1_payload_B[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_strb_V_1_sel_rd_i_1
       (.I0(\image_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I1(image_out_TREADY),
        .I2(image_out_V_strb_V_1_sel),
        .O(image_out_V_strb_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_strb_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_strb_V_1_sel_rd_i_1_n_0),
        .Q(image_out_V_strb_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_strb_V_1_sel_wr_i_1
       (.I0(image_out_V_strb_V_1_ack_in),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_strb_V_1_sel_wr),
        .O(image_out_V_strb_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_strb_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_strb_V_1_sel_wr_i_1_n_0),
        .Q(image_out_V_strb_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \image_out_V_strb_V_1_state[0]_i_1 
       (.I0(image_out_TREADY),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_strb_V_1_ack_in),
        .I3(\image_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\image_out_V_strb_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_out_V_strb_V_1_state[1]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(image_out_TREADY),
        .I3(image_out_V_strb_V_1_ack_in),
        .O(image_out_V_strb_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_strb_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_strb_V_1_state[0]_i_1_n_0 ),
        .Q(\image_out_V_strb_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_strb_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_strb_V_1_state),
        .Q(image_out_V_strb_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    \image_out_V_user_V_1_payload_A[0]_i_1 
       (.I0(image_in_V_user_V_0_payload_B),
        .I1(image_in_V_user_V_0_sel),
        .I2(image_in_V_user_V_0_payload_A),
        .I3(\image_out_V_user_V_1_payload_A[0]_i_2_n_0 ),
        .I4(image_out_V_user_V_1_sel_wr),
        .I5(image_out_V_user_V_1_payload_A),
        .O(\image_out_V_user_V_1_payload_A[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \image_out_V_user_V_1_payload_A[0]_i_2 
       (.I0(image_out_V_user_V_1_ack_in),
        .I1(\image_out_V_user_V_1_state_reg_n_0_[0] ),
        .O(\image_out_V_user_V_1_payload_A[0]_i_2_n_0 ));
  FDRE \image_out_V_user_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_user_V_1_payload_A[0]_i_1_n_0 ),
        .Q(image_out_V_user_V_1_payload_A),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hB8FFFFFFB8000000)) 
    \image_out_V_user_V_1_payload_B[0]_i_1 
       (.I0(image_in_V_user_V_0_payload_B),
        .I1(image_in_V_user_V_0_sel),
        .I2(image_in_V_user_V_0_payload_A),
        .I3(\image_out_V_user_V_1_payload_A[0]_i_2_n_0 ),
        .I4(image_out_V_user_V_1_sel_wr),
        .I5(image_out_V_user_V_1_payload_B),
        .O(\image_out_V_user_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \image_out_V_user_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_user_V_1_payload_B[0]_i_1_n_0 ),
        .Q(image_out_V_user_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_user_V_1_sel_rd_i_1
       (.I0(\image_out_V_user_V_1_state_reg_n_0_[0] ),
        .I1(image_out_TREADY),
        .I2(image_out_V_user_V_1_sel),
        .O(image_out_V_user_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_user_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_user_V_1_sel_rd_i_1_n_0),
        .Q(image_out_V_user_V_1_sel),
        .R(ap_rst_n_inv));
  LUT3 #(
    .INIT(8'h78)) 
    image_out_V_user_V_1_sel_wr_i_1
       (.I0(image_out_V_user_V_1_ack_in),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_user_V_1_sel_wr),
        .O(image_out_V_user_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    image_out_V_user_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_user_V_1_sel_wr_i_1_n_0),
        .Q(image_out_V_user_V_1_sel_wr),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hDFC00000)) 
    \image_out_V_user_V_1_state[0]_i_1 
       (.I0(image_out_TREADY),
        .I1(image_in_V_data_V_0_ack_out),
        .I2(image_out_V_user_V_1_ack_in),
        .I3(\image_out_V_user_V_1_state_reg_n_0_[0] ),
        .I4(ap_rst_n),
        .O(\image_out_V_user_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'hF7F3)) 
    \image_out_V_user_V_1_state[1]_i_1 
       (.I0(image_in_V_data_V_0_ack_out),
        .I1(\image_out_V_user_V_1_state_reg_n_0_[0] ),
        .I2(image_out_TREADY),
        .I3(image_out_V_user_V_1_ack_in),
        .O(image_out_V_user_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_user_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\image_out_V_user_V_1_state[0]_i_1_n_0 ),
        .Q(\image_out_V_user_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \image_out_V_user_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(image_out_V_user_V_1_state),
        .Q(image_out_V_user_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT1 #(
    .INIT(2'h1)) 
    \j_V_1_reg_393[0]_i_1 
       (.I0(p_0129_1_reg_192[0]),
        .O(j_V_1_fu_308_p2[0]));
  LUT6 #(
    .INIT(64'hFF7FFFFF00800000)) 
    \j_V_1_reg_393[10]_i_1 
       (.I0(p_0129_1_reg_192[9]),
        .I1(p_0129_1_reg_192[7]),
        .I2(p_0129_1_reg_192[6]),
        .I3(\j_V_1_reg_393[10]_i_2_n_0 ),
        .I4(p_0129_1_reg_192[8]),
        .I5(p_0129_1_reg_192[10]),
        .O(j_V_1_fu_308_p2[10]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \j_V_1_reg_393[10]_i_2 
       (.I0(p_0129_1_reg_192[2]),
        .I1(p_0129_1_reg_192[1]),
        .I2(p_0129_1_reg_192[0]),
        .I3(p_0129_1_reg_192[3]),
        .I4(p_0129_1_reg_192[4]),
        .I5(p_0129_1_reg_192[5]),
        .O(\j_V_1_reg_393[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \j_V_1_reg_393[1]_i_1 
       (.I0(p_0129_1_reg_192[0]),
        .I1(p_0129_1_reg_192[1]),
        .O(j_V_1_fu_308_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \j_V_1_reg_393[2]_i_1 
       (.I0(p_0129_1_reg_192[0]),
        .I1(p_0129_1_reg_192[1]),
        .I2(p_0129_1_reg_192[2]),
        .O(j_V_1_fu_308_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \j_V_1_reg_393[3]_i_1 
       (.I0(p_0129_1_reg_192[2]),
        .I1(p_0129_1_reg_192[1]),
        .I2(p_0129_1_reg_192[0]),
        .I3(p_0129_1_reg_192[3]),
        .O(j_V_1_fu_308_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \j_V_1_reg_393[4]_i_1 
       (.I0(p_0129_1_reg_192[3]),
        .I1(p_0129_1_reg_192[0]),
        .I2(p_0129_1_reg_192[1]),
        .I3(p_0129_1_reg_192[2]),
        .I4(p_0129_1_reg_192[4]),
        .O(j_V_1_fu_308_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \j_V_1_reg_393[5]_i_1 
       (.I0(p_0129_1_reg_192[2]),
        .I1(p_0129_1_reg_192[1]),
        .I2(p_0129_1_reg_192[0]),
        .I3(p_0129_1_reg_192[3]),
        .I4(p_0129_1_reg_192[4]),
        .I5(p_0129_1_reg_192[5]),
        .O(j_V_1_fu_308_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFF7F0080)) 
    \j_V_1_reg_393[6]_i_1 
       (.I0(p_0129_1_reg_192[5]),
        .I1(p_0129_1_reg_192[4]),
        .I2(p_0129_1_reg_192[3]),
        .I3(\j_V_1_reg_393[7]_i_2_n_0 ),
        .I4(p_0129_1_reg_192[6]),
        .O(j_V_1_fu_308_p2[6]));
  LUT6 #(
    .INIT(64'hBFFFFFFF40000000)) 
    \j_V_1_reg_393[7]_i_1 
       (.I0(\j_V_1_reg_393[7]_i_2_n_0 ),
        .I1(p_0129_1_reg_192[3]),
        .I2(p_0129_1_reg_192[4]),
        .I3(p_0129_1_reg_192[5]),
        .I4(p_0129_1_reg_192[6]),
        .I5(p_0129_1_reg_192[7]),
        .O(j_V_1_fu_308_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \j_V_1_reg_393[7]_i_2 
       (.I0(p_0129_1_reg_192[0]),
        .I1(p_0129_1_reg_192[1]),
        .I2(p_0129_1_reg_192[2]),
        .O(\j_V_1_reg_393[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \j_V_1_reg_393[8]_i_1 
       (.I0(p_0129_1_reg_192[7]),
        .I1(p_0129_1_reg_192[6]),
        .I2(\j_V_1_reg_393[10]_i_2_n_0 ),
        .I3(p_0129_1_reg_192[8]),
        .O(j_V_1_fu_308_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    \j_V_1_reg_393[9]_i_1 
       (.I0(p_0129_1_reg_192[8]),
        .I1(\j_V_1_reg_393[10]_i_2_n_0 ),
        .I2(p_0129_1_reg_192[6]),
        .I3(p_0129_1_reg_192[7]),
        .I4(p_0129_1_reg_192[9]),
        .O(j_V_1_fu_308_p2[9]));
  FDRE \j_V_1_reg_393_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[0]),
        .Q(j_V_1_reg_393[0]),
        .R(1'b0));
  FDRE \j_V_1_reg_393_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[10]),
        .Q(j_V_1_reg_393[10]),
        .R(1'b0));
  FDRE \j_V_1_reg_393_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[1]),
        .Q(j_V_1_reg_393[1]),
        .R(1'b0));
  FDRE \j_V_1_reg_393_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[2]),
        .Q(j_V_1_reg_393[2]),
        .R(1'b0));
  FDRE \j_V_1_reg_393_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[3]),
        .Q(j_V_1_reg_393[3]),
        .R(1'b0));
  FDRE \j_V_1_reg_393_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[4]),
        .Q(j_V_1_reg_393[4]),
        .R(1'b0));
  FDRE \j_V_1_reg_393_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[5]),
        .Q(j_V_1_reg_393[5]),
        .R(1'b0));
  FDRE \j_V_1_reg_393_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[6]),
        .Q(j_V_1_reg_393[6]),
        .R(1'b0));
  FDRE \j_V_1_reg_393_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[7]),
        .Q(j_V_1_reg_393[7]),
        .R(1'b0));
  FDRE \j_V_1_reg_393_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[8]),
        .Q(j_V_1_reg_393[8]),
        .R(1'b0));
  FDRE \j_V_1_reg_393_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(j_V_1_fu_308_p2[9]),
        .Q(j_V_1_reg_393[9]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \j_V_2_reg_433[0]_i_1 
       (.I0(p_0129_2_reg_203[0]),
        .O(j_V_2_fu_343_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \j_V_2_reg_433[1]_i_1 
       (.I0(p_0129_2_reg_203[0]),
        .I1(p_0129_2_reg_203[1]),
        .O(j_V_2_fu_343_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \j_V_2_reg_433[2]_i_1 
       (.I0(p_0129_2_reg_203[0]),
        .I1(p_0129_2_reg_203[1]),
        .I2(p_0129_2_reg_203[2]),
        .O(j_V_2_fu_343_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \j_V_2_reg_433[3]_i_1 
       (.I0(p_0129_2_reg_203[2]),
        .I1(p_0129_2_reg_203[1]),
        .I2(p_0129_2_reg_203[0]),
        .I3(p_0129_2_reg_203[3]),
        .O(j_V_2_fu_343_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \j_V_2_reg_433[4]_i_1 
       (.I0(p_0129_2_reg_203[3]),
        .I1(p_0129_2_reg_203[0]),
        .I2(p_0129_2_reg_203[1]),
        .I3(p_0129_2_reg_203[2]),
        .I4(p_0129_2_reg_203[4]),
        .O(j_V_2_fu_343_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \j_V_2_reg_433[5]_i_1 
       (.I0(p_0129_2_reg_203[2]),
        .I1(p_0129_2_reg_203[1]),
        .I2(p_0129_2_reg_203[0]),
        .I3(p_0129_2_reg_203[3]),
        .I4(p_0129_2_reg_203[4]),
        .I5(p_0129_2_reg_203[5]),
        .O(j_V_2_fu_343_p2[5]));
  LUT2 #(
    .INIT(4'h9)) 
    \j_V_2_reg_433[6]_i_1 
       (.I0(\j_V_2_reg_433[8]_i_2_n_0 ),
        .I1(p_0129_2_reg_203[6]),
        .O(j_V_2_fu_343_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \j_V_2_reg_433[7]_i_1 
       (.I0(p_0129_2_reg_203[6]),
        .I1(\j_V_2_reg_433[8]_i_2_n_0 ),
        .I2(p_0129_2_reg_203[7]),
        .O(j_V_2_fu_343_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \j_V_2_reg_433[8]_i_1 
       (.I0(p_0129_2_reg_203[7]),
        .I1(\j_V_2_reg_433[8]_i_2_n_0 ),
        .I2(p_0129_2_reg_203[6]),
        .I3(p_0129_2_reg_203[8]),
        .O(j_V_2_fu_343_p2[8]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \j_V_2_reg_433[8]_i_2 
       (.I0(p_0129_2_reg_203[2]),
        .I1(p_0129_2_reg_203[1]),
        .I2(p_0129_2_reg_203[0]),
        .I3(p_0129_2_reg_203[3]),
        .I4(p_0129_2_reg_203[4]),
        .I5(p_0129_2_reg_203[5]),
        .O(\j_V_2_reg_433[8]_i_2_n_0 ));
  FDRE \j_V_2_reg_433_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(j_V_2_fu_343_p2[0]),
        .Q(j_V_2_reg_433[0]),
        .R(1'b0));
  FDRE \j_V_2_reg_433_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(j_V_2_fu_343_p2[1]),
        .Q(j_V_2_reg_433[1]),
        .R(1'b0));
  FDRE \j_V_2_reg_433_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(j_V_2_fu_343_p2[2]),
        .Q(j_V_2_reg_433[2]),
        .R(1'b0));
  FDRE \j_V_2_reg_433_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(j_V_2_fu_343_p2[3]),
        .Q(j_V_2_reg_433[3]),
        .R(1'b0));
  FDRE \j_V_2_reg_433_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(j_V_2_fu_343_p2[4]),
        .Q(j_V_2_reg_433[4]),
        .R(1'b0));
  FDRE \j_V_2_reg_433_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(j_V_2_fu_343_p2[5]),
        .Q(j_V_2_reg_433[5]),
        .R(1'b0));
  FDRE \j_V_2_reg_433_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(j_V_2_fu_343_p2[6]),
        .Q(j_V_2_reg_433[6]),
        .R(1'b0));
  FDRE \j_V_2_reg_433_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(j_V_2_fu_343_p2[7]),
        .Q(j_V_2_reg_433[7]),
        .R(1'b0));
  FDRE \j_V_2_reg_433_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(j_V_2_fu_343_p2[8]),
        .Q(j_V_2_reg_433[8]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \j_V_reg_360[0]_i_1 
       (.I0(\p_1_reg_181_reg_n_0_[0] ),
        .O(j_V_fu_290_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \j_V_reg_360[1]_i_1 
       (.I0(\p_1_reg_181_reg_n_0_[0] ),
        .I1(\p_1_reg_181_reg_n_0_[1] ),
        .O(j_V_fu_290_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \j_V_reg_360[2]_i_1 
       (.I0(\p_1_reg_181_reg_n_0_[0] ),
        .I1(\p_1_reg_181_reg_n_0_[1] ),
        .I2(\p_1_reg_181_reg_n_0_[2] ),
        .O(j_V_fu_290_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \j_V_reg_360[3]_i_1 
       (.I0(\p_1_reg_181_reg_n_0_[2] ),
        .I1(\p_1_reg_181_reg_n_0_[1] ),
        .I2(\p_1_reg_181_reg_n_0_[0] ),
        .I3(\p_1_reg_181_reg_n_0_[3] ),
        .O(j_V_fu_290_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \j_V_reg_360[4]_i_1 
       (.I0(\p_1_reg_181_reg_n_0_[3] ),
        .I1(\p_1_reg_181_reg_n_0_[0] ),
        .I2(\p_1_reg_181_reg_n_0_[1] ),
        .I3(\p_1_reg_181_reg_n_0_[2] ),
        .I4(\p_1_reg_181_reg_n_0_[4] ),
        .O(j_V_fu_290_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \j_V_reg_360[5]_i_1 
       (.I0(\p_1_reg_181_reg_n_0_[2] ),
        .I1(\p_1_reg_181_reg_n_0_[1] ),
        .I2(\p_1_reg_181_reg_n_0_[0] ),
        .I3(\p_1_reg_181_reg_n_0_[3] ),
        .I4(\p_1_reg_181_reg_n_0_[4] ),
        .I5(\p_1_reg_181_reg_n_0_[5] ),
        .O(j_V_fu_290_p2[5]));
  LUT2 #(
    .INIT(4'h9)) 
    \j_V_reg_360[6]_i_1 
       (.I0(\j_V_reg_360[8]_i_2_n_0 ),
        .I1(\p_1_reg_181_reg_n_0_[6] ),
        .O(j_V_fu_290_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \j_V_reg_360[7]_i_1 
       (.I0(\p_1_reg_181_reg_n_0_[6] ),
        .I1(\j_V_reg_360[8]_i_2_n_0 ),
        .I2(\p_1_reg_181_reg_n_0_[7] ),
        .O(j_V_fu_290_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \j_V_reg_360[8]_i_1 
       (.I0(\p_1_reg_181_reg_n_0_[7] ),
        .I1(\j_V_reg_360[8]_i_2_n_0 ),
        .I2(\p_1_reg_181_reg_n_0_[6] ),
        .I3(\p_1_reg_181_reg_n_0_[8] ),
        .O(j_V_fu_290_p2[8]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \j_V_reg_360[8]_i_2 
       (.I0(\p_1_reg_181_reg_n_0_[2] ),
        .I1(\p_1_reg_181_reg_n_0_[1] ),
        .I2(\p_1_reg_181_reg_n_0_[0] ),
        .I3(\p_1_reg_181_reg_n_0_[3] ),
        .I4(\p_1_reg_181_reg_n_0_[4] ),
        .I5(\p_1_reg_181_reg_n_0_[5] ),
        .O(\j_V_reg_360[8]_i_2_n_0 ));
  FDRE \j_V_reg_360_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(j_V_fu_290_p2[0]),
        .Q(j_V_reg_360[0]),
        .R(1'b0));
  FDRE \j_V_reg_360_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(j_V_fu_290_p2[1]),
        .Q(j_V_reg_360[1]),
        .R(1'b0));
  FDRE \j_V_reg_360_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(j_V_fu_290_p2[2]),
        .Q(j_V_reg_360[2]),
        .R(1'b0));
  FDRE \j_V_reg_360_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(j_V_fu_290_p2[3]),
        .Q(j_V_reg_360[3]),
        .R(1'b0));
  FDRE \j_V_reg_360_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(j_V_fu_290_p2[4]),
        .Q(j_V_reg_360[4]),
        .R(1'b0));
  FDRE \j_V_reg_360_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(j_V_fu_290_p2[5]),
        .Q(j_V_reg_360[5]),
        .R(1'b0));
  FDRE \j_V_reg_360_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(j_V_fu_290_p2[6]),
        .Q(j_V_reg_360[6]),
        .R(1'b0));
  FDRE \j_V_reg_360_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(j_V_fu_290_p2[7]),
        .Q(j_V_reg_360[7]),
        .R(1'b0));
  FDRE \j_V_reg_360_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(j_V_fu_290_p2[8]),
        .Q(j_V_reg_360[8]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00000008)) 
    \p_0129_1_reg_192[10]_i_1 
       (.I0(\p_0129_1_reg_192[10]_i_3_n_0 ),
        .I1(\p_1_reg_181_reg_n_0_[2] ),
        .I2(\p_1_reg_181_reg_n_0_[3] ),
        .I3(\p_1_reg_181_reg_n_0_[0] ),
        .I4(\p_1_reg_181_reg_n_0_[1] ),
        .O(ap_NS_fsm1112_out));
  LUT3 #(
    .INIT(8'h80)) 
    \p_0129_1_reg_192[10]_i_2 
       (.I0(ap_CS_fsm_state8),
        .I1(image_out_V_data_V_1_ack_in),
        .I2(crop_out_V_data_V_1_ack_in),
        .O(ap_NS_fsm1107_out));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \p_0129_1_reg_192[10]_i_3 
       (.I0(\p_1_reg_181_reg_n_0_[5] ),
        .I1(\p_1_reg_181_reg_n_0_[4] ),
        .I2(\p_1_reg_181_reg_n_0_[7] ),
        .I3(\p_1_reg_181_reg_n_0_[6] ),
        .I4(ap_CS_fsm_state3),
        .I5(\p_1_reg_181_reg_n_0_[8] ),
        .O(\p_0129_1_reg_192[10]_i_3_n_0 ));
  FDRE \p_0129_1_reg_192_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[0]),
        .Q(p_0129_1_reg_192[0]),
        .R(ap_NS_fsm1112_out));
  FDRE \p_0129_1_reg_192_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[10]),
        .Q(p_0129_1_reg_192[10]),
        .R(ap_NS_fsm1112_out));
  FDRE \p_0129_1_reg_192_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[1]),
        .Q(p_0129_1_reg_192[1]),
        .R(ap_NS_fsm1112_out));
  FDRE \p_0129_1_reg_192_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[2]),
        .Q(p_0129_1_reg_192[2]),
        .R(ap_NS_fsm1112_out));
  FDRE \p_0129_1_reg_192_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[3]),
        .Q(p_0129_1_reg_192[3]),
        .R(ap_NS_fsm1112_out));
  FDRE \p_0129_1_reg_192_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[4]),
        .Q(p_0129_1_reg_192[4]),
        .R(ap_NS_fsm1112_out));
  FDRE \p_0129_1_reg_192_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[5]),
        .Q(p_0129_1_reg_192[5]),
        .R(ap_NS_fsm1112_out));
  FDRE \p_0129_1_reg_192_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[6]),
        .Q(p_0129_1_reg_192[6]),
        .R(ap_NS_fsm1112_out));
  FDRE \p_0129_1_reg_192_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[7]),
        .Q(p_0129_1_reg_192[7]),
        .R(ap_NS_fsm1112_out));
  FDRE \p_0129_1_reg_192_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[8]),
        .Q(p_0129_1_reg_192[8]),
        .R(ap_NS_fsm1112_out));
  FDRE \p_0129_1_reg_192_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1107_out),
        .D(j_V_1_reg_393[9]),
        .Q(p_0129_1_reg_192[9]),
        .R(ap_NS_fsm1112_out));
  LUT2 #(
    .INIT(4'h8)) 
    \p_0129_2_reg_203[8]_i_1 
       (.I0(tmp_6_fu_302_p2),
        .I1(ap_CS_fsm_state6),
        .O(ap_NS_fsm1109_out));
  LUT2 #(
    .INIT(4'h8)) 
    \p_0129_2_reg_203[8]_i_2 
       (.I0(ap_CS_fsm_state11),
        .I1(image_out_V_data_V_1_ack_in),
        .O(ap_NS_fsm1));
  FDRE \p_0129_2_reg_203_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(j_V_2_reg_433[0]),
        .Q(p_0129_2_reg_203[0]),
        .R(ap_NS_fsm1109_out));
  FDRE \p_0129_2_reg_203_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(j_V_2_reg_433[1]),
        .Q(p_0129_2_reg_203[1]),
        .R(ap_NS_fsm1109_out));
  FDRE \p_0129_2_reg_203_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(j_V_2_reg_433[2]),
        .Q(p_0129_2_reg_203[2]),
        .R(ap_NS_fsm1109_out));
  FDRE \p_0129_2_reg_203_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(j_V_2_reg_433[3]),
        .Q(p_0129_2_reg_203[3]),
        .R(ap_NS_fsm1109_out));
  FDRE \p_0129_2_reg_203_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(j_V_2_reg_433[4]),
        .Q(p_0129_2_reg_203[4]),
        .R(ap_NS_fsm1109_out));
  FDRE \p_0129_2_reg_203_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(j_V_2_reg_433[5]),
        .Q(p_0129_2_reg_203[5]),
        .R(ap_NS_fsm1109_out));
  FDRE \p_0129_2_reg_203_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(j_V_2_reg_433[6]),
        .Q(p_0129_2_reg_203[6]),
        .R(ap_NS_fsm1109_out));
  FDRE \p_0129_2_reg_203_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(j_V_2_reg_433[7]),
        .Q(p_0129_2_reg_203[7]),
        .R(ap_NS_fsm1109_out));
  FDRE \p_0129_2_reg_203_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(j_V_2_reg_433[8]),
        .Q(p_0129_2_reg_203[8]),
        .R(ap_NS_fsm1109_out));
  LUT4 #(
    .INIT(16'h0070)) 
    \p_1_reg_181[8]_i_1 
       (.I0(ap_CS_fsm_state5),
        .I1(image_out_V_data_V_1_ack_in),
        .I2(i_V_reg_3520),
        .I3(tmp_fu_272_p2),
        .O(p_1_reg_181));
  LUT2 #(
    .INIT(4'h8)) 
    \p_1_reg_181[8]_i_2 
       (.I0(image_out_V_data_V_1_ack_in),
        .I1(ap_CS_fsm_state5),
        .O(ap_NS_fsm1110_out));
  FDRE \p_1_reg_181_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1110_out),
        .D(j_V_reg_360[0]),
        .Q(\p_1_reg_181_reg_n_0_[0] ),
        .R(p_1_reg_181));
  FDRE \p_1_reg_181_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1110_out),
        .D(j_V_reg_360[1]),
        .Q(\p_1_reg_181_reg_n_0_[1] ),
        .R(p_1_reg_181));
  FDRE \p_1_reg_181_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1110_out),
        .D(j_V_reg_360[2]),
        .Q(\p_1_reg_181_reg_n_0_[2] ),
        .R(p_1_reg_181));
  FDRE \p_1_reg_181_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1110_out),
        .D(j_V_reg_360[3]),
        .Q(\p_1_reg_181_reg_n_0_[3] ),
        .R(p_1_reg_181));
  FDRE \p_1_reg_181_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1110_out),
        .D(j_V_reg_360[4]),
        .Q(\p_1_reg_181_reg_n_0_[4] ),
        .R(p_1_reg_181));
  FDRE \p_1_reg_181_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1110_out),
        .D(j_V_reg_360[5]),
        .Q(\p_1_reg_181_reg_n_0_[5] ),
        .R(p_1_reg_181));
  FDRE \p_1_reg_181_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1110_out),
        .D(j_V_reg_360[6]),
        .Q(\p_1_reg_181_reg_n_0_[6] ),
        .R(p_1_reg_181));
  FDRE \p_1_reg_181_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1110_out),
        .D(j_V_reg_360[7]),
        .Q(\p_1_reg_181_reg_n_0_[7] ),
        .R(p_1_reg_181));
  FDRE \p_1_reg_181_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1110_out),
        .D(j_V_reg_360[8]),
        .Q(\p_1_reg_181_reg_n_0_[8] ),
        .R(p_1_reg_181));
  LUT3 #(
    .INIT(8'h08)) 
    \p_s_reg_169[10]_i_1 
       (.I0(\ap_CS_fsm_reg_n_0_[0] ),
        .I1(ap_start),
        .I2(ap_NS_fsm1105_out),
        .O(p_s_reg_169));
  LUT5 #(
    .INIT(32'h00000008)) 
    \p_s_reg_169[10]_i_2 
       (.I0(\p_s_reg_169[10]_i_3_n_0 ),
        .I1(p_0129_2_reg_203[2]),
        .I2(p_0129_2_reg_203[3]),
        .I3(p_0129_2_reg_203[0]),
        .I4(p_0129_2_reg_203[1]),
        .O(ap_NS_fsm1105_out));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \p_s_reg_169[10]_i_3 
       (.I0(p_0129_2_reg_203[5]),
        .I1(p_0129_2_reg_203[4]),
        .I2(p_0129_2_reg_203[7]),
        .I3(p_0129_2_reg_203[6]),
        .I4(ap_CS_fsm_state9),
        .I5(p_0129_2_reg_203[8]),
        .O(\p_s_reg_169[10]_i_3_n_0 ));
  FDRE \p_s_reg_169_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[0]),
        .Q(\p_s_reg_169_reg_n_0_[0] ),
        .R(p_s_reg_169));
  FDRE \p_s_reg_169_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[10]),
        .Q(\p_s_reg_169_reg_n_0_[10] ),
        .R(p_s_reg_169));
  FDRE \p_s_reg_169_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[1]),
        .Q(\p_s_reg_169_reg_n_0_[1] ),
        .R(p_s_reg_169));
  FDRE \p_s_reg_169_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[2]),
        .Q(\p_s_reg_169_reg_n_0_[2] ),
        .R(p_s_reg_169));
  FDRE \p_s_reg_169_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[3]),
        .Q(\p_s_reg_169_reg_n_0_[3] ),
        .R(p_s_reg_169));
  FDRE \p_s_reg_169_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[4]),
        .Q(\p_s_reg_169_reg_n_0_[4] ),
        .R(p_s_reg_169));
  FDRE \p_s_reg_169_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[5]),
        .Q(\p_s_reg_169_reg_n_0_[5] ),
        .R(p_s_reg_169));
  FDRE \p_s_reg_169_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[6]),
        .Q(\p_s_reg_169_reg_n_0_[6] ),
        .R(p_s_reg_169));
  FDRE \p_s_reg_169_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[7]),
        .Q(\p_s_reg_169_reg_n_0_[7] ),
        .R(p_s_reg_169));
  FDRE \p_s_reg_169_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[8]),
        .Q(\p_s_reg_169_reg_n_0_[8] ),
        .R(p_s_reg_169));
  FDRE \p_s_reg_169_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1105_out),
        .D(i_V_reg_352[9]),
        .Q(\p_s_reg_169_reg_n_0_[9] ),
        .R(p_s_reg_169));
  LUT6 #(
    .INIT(64'h8000FFFF80000000)) 
    \tmp_2_reg_365[0]_i_1 
       (.I0(\tmp_2_reg_365[0]_i_2_n_0 ),
        .I1(\p_s_reg_169_reg_n_0_[2] ),
        .I2(\p_s_reg_169_reg_n_0_[1] ),
        .I3(\p_s_reg_169_reg_n_0_[0] ),
        .I4(ap_NS_fsm1112_out),
        .I5(\tmp_2_reg_365_reg_n_0_[0] ),
        .O(\tmp_2_reg_365[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000800)) 
    \tmp_2_reg_365[0]_i_2 
       (.I0(ap_ready_INST_0_i_7_n_0),
        .I1(\p_s_reg_169_reg_n_0_[5] ),
        .I2(\p_s_reg_169_reg_n_0_[6] ),
        .I3(\p_s_reg_169_reg_n_0_[4] ),
        .I4(\p_s_reg_169_reg_n_0_[3] ),
        .O(\tmp_2_reg_365[0]_i_2_n_0 ));
  FDRE \tmp_2_reg_365_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_2_reg_365[0]_i_1_n_0 ),
        .Q(\tmp_2_reg_365_reg_n_0_[0] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFF8F0080)) 
    \tmp_last_V_2_reg_403[0]_i_1 
       (.I0(\tmp_last_V_2_reg_403[0]_i_2_n_0 ),
        .I1(\tmp_last_V_2_reg_403[0]_i_3_n_0 ),
        .I2(ap_CS_fsm_state6),
        .I3(tmp_6_fu_302_p2),
        .I4(tmp_last_V_2_reg_403),
        .O(\tmp_last_V_2_reg_403[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \tmp_last_V_2_reg_403[0]_i_2 
       (.I0(p_0129_1_reg_192[3]),
        .I1(p_0129_1_reg_192[4]),
        .I2(p_0129_1_reg_192[5]),
        .I3(p_0129_1_reg_192[2]),
        .I4(p_0129_1_reg_192[1]),
        .I5(p_0129_1_reg_192[0]),
        .O(\tmp_last_V_2_reg_403[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \tmp_last_V_2_reg_403[0]_i_3 
       (.I0(p_0129_1_reg_192[6]),
        .I1(p_0129_1_reg_192[7]),
        .I2(p_0129_1_reg_192[10]),
        .I3(\tmp_2_reg_365_reg_n_0_[0] ),
        .I4(p_0129_1_reg_192[9]),
        .I5(p_0129_1_reg_192[8]),
        .O(\tmp_last_V_2_reg_403[0]_i_3_n_0 ));
  FDRE \tmp_last_V_2_reg_403_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_last_V_2_reg_403[0]_i_1_n_0 ),
        .Q(tmp_last_V_2_reg_403),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hC4CFC4C4C4C4C4C4)) 
    \tmp_user_V_2_reg_398[0]_i_1 
       (.I0(ap_CS_fsm_state6),
        .I1(\tmp_user_V_2_reg_398_reg_n_0_[0] ),
        .I2(tmp_6_fu_302_p2),
        .I3(\tmp_user_V_2_reg_398[0]_i_2_n_0 ),
        .I4(\tmp_user_V_2_reg_398[0]_i_3_n_0 ),
        .I5(\tmp_user_V_2_reg_398[0]_i_4_n_0 ),
        .O(\tmp_user_V_2_reg_398[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \tmp_user_V_2_reg_398[0]_i_2 
       (.I0(p_0129_1_reg_192[10]),
        .I1(\p_s_reg_169_reg_n_0_[10] ),
        .I2(p_0129_1_reg_192[8]),
        .I3(p_0129_1_reg_192[9]),
        .I4(\p_s_reg_169_reg_n_0_[8] ),
        .I5(\p_s_reg_169_reg_n_0_[9] ),
        .O(\tmp_user_V_2_reg_398[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \tmp_user_V_2_reg_398[0]_i_3 
       (.I0(\p_s_reg_169_reg_n_0_[3] ),
        .I1(\p_s_reg_169_reg_n_0_[4] ),
        .I2(\p_s_reg_169_reg_n_0_[5] ),
        .I3(\p_s_reg_169_reg_n_0_[6] ),
        .I4(\p_s_reg_169_reg_n_0_[7] ),
        .I5(ap_CS_fsm_state6),
        .O(\tmp_user_V_2_reg_398[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \tmp_user_V_2_reg_398[0]_i_4 
       (.I0(\tmp_user_V_2_reg_398[0]_i_5_n_0 ),
        .I1(p_0129_1_reg_192[6]),
        .I2(p_0129_1_reg_192[5]),
        .I3(p_0129_1_reg_192[4]),
        .I4(p_0129_1_reg_192[3]),
        .I5(\ap_CS_fsm[8]_i_4_n_0 ),
        .O(\tmp_user_V_2_reg_398[0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \tmp_user_V_2_reg_398[0]_i_5 
       (.I0(\p_s_reg_169_reg_n_0_[2] ),
        .I1(\p_s_reg_169_reg_n_0_[1] ),
        .I2(\p_s_reg_169_reg_n_0_[0] ),
        .I3(p_0129_1_reg_192[7]),
        .O(\tmp_user_V_2_reg_398[0]_i_5_n_0 ));
  FDRE \tmp_user_V_2_reg_398_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_user_V_2_reg_398[0]_i_1_n_0 ),
        .Q(\tmp_user_V_2_reg_398_reg_n_0_[0] ),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "system_center_image_0_0,center_image,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "HLS" *) 
(* x_core_info = "center_image,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (ap_clk,
    ap_rst_n,
    ap_start,
    ap_done,
    ap_idle,
    ap_ready,
    image_in_TVALID,
    image_in_TREADY,
    image_in_TDATA,
    image_in_TDEST,
    image_in_TKEEP,
    image_in_TSTRB,
    image_in_TUSER,
    image_in_TLAST,
    image_in_TID,
    image_out_TVALID,
    image_out_TREADY,
    image_out_TDATA,
    image_out_TDEST,
    image_out_TKEEP,
    image_out_TSTRB,
    image_out_TUSER,
    image_out_TLAST,
    image_out_TID,
    crop_out_TVALID,
    crop_out_TREADY,
    crop_out_TDATA,
    crop_out_TDEST,
    crop_out_TKEEP,
    crop_out_TSTRB,
    crop_out_TUSER,
    crop_out_TLAST,
    crop_out_TID);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF image_in:image_out:crop_out, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl start" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_ctrl, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {start {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} done {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} idle {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} ready {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_start;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl done" *) output ap_done;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl idle" *) output ap_idle;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ready" *) output ap_ready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME image_in, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) input image_in_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TREADY" *) output image_in_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TDATA" *) input [23:0]image_in_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TDEST" *) input [0:0]image_in_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TKEEP" *) input [2:0]image_in_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TSTRB" *) input [2:0]image_in_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TUSER" *) input [0:0]image_in_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TLAST" *) input [0:0]image_in_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TID" *) input [0:0]image_in_TID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME image_out, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 24 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) output image_out_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TREADY" *) input image_out_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TDATA" *) output [23:0]image_out_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TDEST" *) output [0:0]image_out_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TKEEP" *) output [2:0]image_out_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TSTRB" *) output [2:0]image_out_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TUSER" *) output [0:0]image_out_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TLAST" *) output [0:0]image_out_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TID" *) output [0:0]image_out_TID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME crop_out, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 24 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) output crop_out_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TREADY" *) input crop_out_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TDATA" *) output [23:0]crop_out_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TDEST" *) output [0:0]crop_out_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TKEEP" *) output [2:0]crop_out_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TSTRB" *) output [2:0]crop_out_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TUSER" *) output [0:0]crop_out_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TLAST" *) output [0:0]crop_out_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TID" *) output [0:0]crop_out_TID;

  wire ap_clk;
  wire ap_done;
  wire ap_idle;
  wire ap_ready;
  wire ap_rst_n;
  wire ap_start;
  wire [23:0]crop_out_TDATA;
  wire [0:0]crop_out_TDEST;
  wire [0:0]crop_out_TID;
  wire [2:0]crop_out_TKEEP;
  wire [0:0]crop_out_TLAST;
  wire crop_out_TREADY;
  wire [2:0]crop_out_TSTRB;
  wire [0:0]crop_out_TUSER;
  wire crop_out_TVALID;
  wire [23:0]image_in_TDATA;
  wire [0:0]image_in_TDEST;
  wire [0:0]image_in_TID;
  wire [2:0]image_in_TKEEP;
  wire [0:0]image_in_TLAST;
  wire image_in_TREADY;
  wire [2:0]image_in_TSTRB;
  wire [0:0]image_in_TUSER;
  wire image_in_TVALID;
  wire [23:0]image_out_TDATA;
  wire [0:0]image_out_TDEST;
  wire [0:0]image_out_TID;
  wire [2:0]image_out_TKEEP;
  wire [0:0]image_out_TLAST;
  wire image_out_TREADY;
  wire [2:0]image_out_TSTRB;
  wire [0:0]image_out_TUSER;
  wire image_out_TVALID;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_center_image U0
       (.ap_clk(ap_clk),
        .ap_done(ap_done),
        .ap_idle(ap_idle),
        .ap_ready(ap_ready),
        .ap_rst_n(ap_rst_n),
        .ap_start(ap_start),
        .crop_out_TDATA(crop_out_TDATA),
        .crop_out_TDEST(crop_out_TDEST),
        .crop_out_TID(crop_out_TID),
        .crop_out_TKEEP(crop_out_TKEEP),
        .crop_out_TLAST(crop_out_TLAST),
        .crop_out_TREADY(crop_out_TREADY),
        .crop_out_TSTRB(crop_out_TSTRB),
        .crop_out_TUSER(crop_out_TUSER),
        .crop_out_TVALID(crop_out_TVALID),
        .image_in_TDATA(image_in_TDATA),
        .image_in_TDEST(image_in_TDEST),
        .image_in_TID(image_in_TID),
        .image_in_TKEEP(image_in_TKEEP),
        .image_in_TLAST(image_in_TLAST),
        .image_in_TREADY(image_in_TREADY),
        .image_in_TSTRB(image_in_TSTRB),
        .image_in_TUSER(image_in_TUSER),
        .image_in_TVALID(image_in_TVALID),
        .image_out_TDATA(image_out_TDATA),
        .image_out_TDEST(image_out_TDEST),
        .image_out_TID(image_out_TID),
        .image_out_TKEEP(image_out_TKEEP),
        .image_out_TLAST(image_out_TLAST),
        .image_out_TREADY(image_out_TREADY),
        .image_out_TSTRB(image_out_TSTRB),
        .image_out_TUSER(image_out_TUSER),
        .image_out_TVALID(image_out_TVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
