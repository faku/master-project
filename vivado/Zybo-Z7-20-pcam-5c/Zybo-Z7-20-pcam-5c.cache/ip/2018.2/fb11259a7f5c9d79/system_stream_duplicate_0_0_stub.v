// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Tue Dec  3 12:41:06 2019
// Host        : A23B-PC10 running 64-bit Ubuntu 16.04.3 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_stream_duplicate_0_0_stub.v
// Design      : system_stream_duplicate_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "stream_duplicate,Vivado 2018.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(stream_in_TVALID, stream_in_TREADY, 
  stream_in_TDATA, stream_in_TKEEP, stream_in_TSTRB, stream_in_TUSER, stream_in_TLAST, 
  stream_in_TID, stream_in_TDEST, stream0_out_TVALID, stream0_out_TREADY, 
  stream0_out_TDATA, stream0_out_TKEEP, stream0_out_TSTRB, stream0_out_TUSER, 
  stream0_out_TLAST, stream0_out_TID, stream0_out_TDEST, stream1_out_TVALID, 
  stream1_out_TREADY, stream1_out_TDATA, stream1_out_TKEEP, stream1_out_TSTRB, 
  stream1_out_TUSER, stream1_out_TLAST, stream1_out_TID, stream1_out_TDEST, ap_clk, ap_rst_n, 
  ap_start, ap_done, ap_ready, ap_idle)
/* synthesis syn_black_box black_box_pad_pin="stream_in_TVALID,stream_in_TREADY,stream_in_TDATA[23:0],stream_in_TKEEP[2:0],stream_in_TSTRB[2:0],stream_in_TUSER[0:0],stream_in_TLAST[0:0],stream_in_TID[0:0],stream_in_TDEST[0:0],stream0_out_TVALID,stream0_out_TREADY,stream0_out_TDATA[23:0],stream0_out_TKEEP[2:0],stream0_out_TSTRB[2:0],stream0_out_TUSER[0:0],stream0_out_TLAST[0:0],stream0_out_TID[0:0],stream0_out_TDEST[0:0],stream1_out_TVALID,stream1_out_TREADY,stream1_out_TDATA[23:0],stream1_out_TKEEP[2:0],stream1_out_TSTRB[2:0],stream1_out_TUSER[0:0],stream1_out_TLAST[0:0],stream1_out_TID[0:0],stream1_out_TDEST[0:0],ap_clk,ap_rst_n,ap_start,ap_done,ap_ready,ap_idle" */;
  input stream_in_TVALID;
  output stream_in_TREADY;
  input [23:0]stream_in_TDATA;
  input [2:0]stream_in_TKEEP;
  input [2:0]stream_in_TSTRB;
  input [0:0]stream_in_TUSER;
  input [0:0]stream_in_TLAST;
  input [0:0]stream_in_TID;
  input [0:0]stream_in_TDEST;
  output stream0_out_TVALID;
  input stream0_out_TREADY;
  output [23:0]stream0_out_TDATA;
  output [2:0]stream0_out_TKEEP;
  output [2:0]stream0_out_TSTRB;
  output [0:0]stream0_out_TUSER;
  output [0:0]stream0_out_TLAST;
  output [0:0]stream0_out_TID;
  output [0:0]stream0_out_TDEST;
  output stream1_out_TVALID;
  input stream1_out_TREADY;
  output [23:0]stream1_out_TDATA;
  output [2:0]stream1_out_TKEEP;
  output [2:0]stream1_out_TSTRB;
  output [0:0]stream1_out_TUSER;
  output [0:0]stream1_out_TLAST;
  output [0:0]stream1_out_TID;
  output [0:0]stream1_out_TDEST;
  input ap_clk;
  input ap_rst_n;
  input ap_start;
  output ap_done;
  output ap_ready;
  output ap_idle;
endmodule
