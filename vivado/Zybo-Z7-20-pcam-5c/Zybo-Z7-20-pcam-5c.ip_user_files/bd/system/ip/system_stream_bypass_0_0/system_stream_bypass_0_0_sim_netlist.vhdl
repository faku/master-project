-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Sun Dec  8 16:55:28 2019
-- Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_stream_bypass_0_0/system_stream_bypass_0_0_sim_netlist.vhdl
-- Design      : system_stream_bypass_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_stream_bypass_0_0_stream_bypass is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_done : out STD_LOGIC;
    ap_idle : out STD_LOGIC;
    ap_ready : out STD_LOGIC;
    stream_in_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    stream_in_TVALID : in STD_LOGIC;
    stream_in_TREADY : out STD_LOGIC;
    stream_in_TKEEP : in STD_LOGIC_VECTOR ( 3 downto 0 );
    stream_in_TSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    stream_in_TUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TID : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TDEST : in STD_LOGIC_VECTOR ( 0 to 0 );
    enable_V : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    stream_out_TVALID : out STD_LOGIC;
    stream_out_TREADY : in STD_LOGIC;
    stream_out_TKEEP : out STD_LOGIC_VECTOR ( 3 downto 0 );
    stream_out_TSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    stream_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TID : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TDEST : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_stream_bypass_0_0_stream_bypass : entity is "stream_bypass";
end system_stream_bypass_0_0_stream_bypass;

architecture STRUCTURE of system_stream_bypass_0_0_stream_bypass is
  signal \ap_CS_fsm[0]_i_1_n_0\ : STD_LOGIC;
  signal \ap_CS_fsm[1]_i_1_n_0\ : STD_LOGIC;
  signal \ap_CS_fsm[2]_i_1_n_0\ : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_0_[0]\ : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state3 : STD_LOGIC;
  signal ap_NS_fsm1 : STD_LOGIC;
  signal ap_NS_fsm1104_out : STD_LOGIC;
  signal \^ap_ready\ : STD_LOGIC;
  signal ap_ready_INST_0_i_1_n_0 : STD_LOGIC;
  signal ap_ready_INST_0_i_2_n_0 : STD_LOGIC;
  signal ap_rst_n_inv : STD_LOGIC;
  signal \^stream_out_tvalid\ : STD_LOGIC;
  signal stream_out_V_data_V_1_ack_in : STD_LOGIC;
  signal stream_out_V_data_V_1_load_B : STD_LOGIC;
  signal stream_out_V_data_V_1_payload_A : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_data_V_1_payload_B : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal stream_out_V_data_V_1_sel : STD_LOGIC;
  signal stream_out_V_data_V_1_sel_rd_i_1_n_0 : STD_LOGIC;
  signal stream_out_V_data_V_1_sel_wr : STD_LOGIC;
  signal stream_out_V_data_V_1_sel_wr_i_1_n_0 : STD_LOGIC;
  signal \stream_out_V_data_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_data_V_1_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \stream_out_V_data_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal stream_out_V_dest_V_1_ack_in : STD_LOGIC;
  signal stream_out_V_dest_V_1_payload_A : STD_LOGIC;
  signal \stream_out_V_dest_V_1_payload_A[0]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_dest_V_1_payload_B : STD_LOGIC;
  signal \stream_out_V_dest_V_1_payload_B[0]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_dest_V_1_sel : STD_LOGIC;
  signal stream_out_V_dest_V_1_sel_rd_i_1_n_0 : STD_LOGIC;
  signal stream_out_V_dest_V_1_sel_wr : STD_LOGIC;
  signal stream_out_V_dest_V_1_sel_wr_i_1_n_0 : STD_LOGIC;
  signal \stream_out_V_dest_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_dest_V_1_state[1]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_id_V_1_ack_in : STD_LOGIC;
  signal stream_out_V_id_V_1_payload_A : STD_LOGIC;
  signal \stream_out_V_id_V_1_payload_A[0]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_id_V_1_payload_B : STD_LOGIC;
  signal \stream_out_V_id_V_1_payload_B[0]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_id_V_1_sel : STD_LOGIC;
  signal stream_out_V_id_V_1_sel_rd_i_1_n_0 : STD_LOGIC;
  signal stream_out_V_id_V_1_sel_wr : STD_LOGIC;
  signal stream_out_V_id_V_1_sel_wr_i_1_n_0 : STD_LOGIC;
  signal \stream_out_V_id_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_id_V_1_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_id_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal stream_out_V_keep_V_1_ack_in : STD_LOGIC;
  signal stream_out_V_keep_V_1_load_B : STD_LOGIC;
  signal stream_out_V_keep_V_1_payload_A : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \stream_out_V_keep_V_1_payload_A[3]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_keep_V_1_payload_B : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal stream_out_V_keep_V_1_sel : STD_LOGIC;
  signal stream_out_V_keep_V_1_sel_rd_i_1_n_0 : STD_LOGIC;
  signal stream_out_V_keep_V_1_sel_wr : STD_LOGIC;
  signal stream_out_V_keep_V_1_sel_wr_i_1_n_0 : STD_LOGIC;
  signal \stream_out_V_keep_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_keep_V_1_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_keep_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal stream_out_V_last_V_1_ack_in : STD_LOGIC;
  signal stream_out_V_last_V_1_payload_A : STD_LOGIC;
  signal \stream_out_V_last_V_1_payload_A[0]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_last_V_1_payload_B : STD_LOGIC;
  signal \stream_out_V_last_V_1_payload_B[0]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_last_V_1_sel : STD_LOGIC;
  signal stream_out_V_last_V_1_sel_rd_i_1_n_0 : STD_LOGIC;
  signal stream_out_V_last_V_1_sel_wr : STD_LOGIC;
  signal stream_out_V_last_V_1_sel_wr_i_1_n_0 : STD_LOGIC;
  signal \stream_out_V_last_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_last_V_1_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_last_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal stream_out_V_strb_V_1_ack_in : STD_LOGIC;
  signal stream_out_V_strb_V_1_load_B : STD_LOGIC;
  signal stream_out_V_strb_V_1_payload_A : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \stream_out_V_strb_V_1_payload_A[3]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_strb_V_1_payload_B : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal stream_out_V_strb_V_1_sel : STD_LOGIC;
  signal stream_out_V_strb_V_1_sel_rd_i_1_n_0 : STD_LOGIC;
  signal stream_out_V_strb_V_1_sel_wr : STD_LOGIC;
  signal stream_out_V_strb_V_1_sel_wr_i_1_n_0 : STD_LOGIC;
  signal \stream_out_V_strb_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_strb_V_1_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_strb_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal stream_out_V_user_V_1_ack_in : STD_LOGIC;
  signal stream_out_V_user_V_1_payload_A : STD_LOGIC;
  signal \stream_out_V_user_V_1_payload_A[0]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_user_V_1_payload_B : STD_LOGIC;
  signal \stream_out_V_user_V_1_payload_B[0]_i_1_n_0\ : STD_LOGIC;
  signal stream_out_V_user_V_1_sel : STD_LOGIC;
  signal stream_out_V_user_V_1_sel_rd_i_1_n_0 : STD_LOGIC;
  signal stream_out_V_user_V_1_sel_wr : STD_LOGIC;
  signal stream_out_V_user_V_1_sel_wr_i_1_n_0 : STD_LOGIC;
  signal \stream_out_V_user_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_user_V_1_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \stream_out_V_user_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \ap_CS_fsm[1]_i_2\ : label is "soft_lutpair9";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute SOFT_HLUTNM of ap_idle_INST_0 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of ap_ready_INST_0 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of stream_in_TREADY_INST_0 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \stream_out_TDATA[0]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \stream_out_TDATA[10]_INST_0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \stream_out_TDATA[11]_INST_0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \stream_out_TDATA[12]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \stream_out_TDATA[13]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \stream_out_TDATA[14]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \stream_out_TDATA[15]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \stream_out_TDATA[16]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \stream_out_TDATA[17]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \stream_out_TDATA[18]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \stream_out_TDATA[19]_INST_0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \stream_out_TDATA[1]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \stream_out_TDATA[20]_INST_0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \stream_out_TDATA[21]_INST_0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \stream_out_TDATA[22]_INST_0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \stream_out_TDATA[23]_INST_0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \stream_out_TDATA[24]_INST_0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \stream_out_TDATA[25]_INST_0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \stream_out_TDATA[26]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \stream_out_TDATA[27]_INST_0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \stream_out_TDATA[28]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \stream_out_TDATA[29]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \stream_out_TDATA[2]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \stream_out_TDATA[30]_INST_0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \stream_out_TDATA[31]_INST_0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \stream_out_TDATA[3]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \stream_out_TDATA[4]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \stream_out_TDATA[5]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \stream_out_TDATA[6]_INST_0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \stream_out_TDATA[7]_INST_0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \stream_out_TDATA[8]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \stream_out_TDATA[9]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \stream_out_TKEEP[0]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \stream_out_TKEEP[1]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \stream_out_TKEEP[2]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \stream_out_TKEEP[3]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \stream_out_TSTRB[0]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \stream_out_TSTRB[1]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \stream_out_TSTRB[2]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \stream_out_TSTRB[3]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of stream_out_V_data_V_1_sel_rd_i_1 : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of stream_out_V_data_V_1_sel_wr_i_1 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \stream_out_V_data_V_1_state[1]_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of stream_out_V_dest_V_1_sel_rd_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of stream_out_V_dest_V_1_sel_wr_i_1 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \stream_out_V_dest_V_1_state[1]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of stream_out_V_id_V_1_sel_rd_i_1 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of stream_out_V_id_V_1_sel_wr_i_1 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \stream_out_V_id_V_1_state[1]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of stream_out_V_keep_V_1_sel_rd_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of stream_out_V_keep_V_1_sel_wr_i_1 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \stream_out_V_keep_V_1_state[1]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of stream_out_V_last_V_1_sel_rd_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of stream_out_V_last_V_1_sel_wr_i_1 : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \stream_out_V_last_V_1_state[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of stream_out_V_strb_V_1_sel_rd_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of stream_out_V_strb_V_1_sel_wr_i_1 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \stream_out_V_strb_V_1_state[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of stream_out_V_user_V_1_sel_rd_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of stream_out_V_user_V_1_sel_wr_i_1 : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \stream_out_V_user_V_1_state[1]_i_1\ : label is "soft_lutpair1";
begin
  ap_done <= \^ap_ready\;
  ap_ready <= \^ap_ready\;
  stream_out_TVALID <= \^stream_out_tvalid\;
\ap_CS_fsm[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FA30"
    )
        port map (
      I0 => \^ap_ready\,
      I1 => ap_start,
      I2 => \ap_CS_fsm_reg_n_0_[0]\,
      I3 => ap_CS_fsm_state2,
      O => \ap_CS_fsm[0]_i_1_n_0\
    );
\ap_CS_fsm[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5F5F0C5C00000C5C"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => ap_NS_fsm1104_out,
      I2 => ap_CS_fsm_state2,
      I3 => \^ap_ready\,
      I4 => \ap_CS_fsm_reg_n_0_[0]\,
      I5 => ap_start,
      O => \ap_CS_fsm[1]_i_1_n_0\
    );
\ap_CS_fsm[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => stream_out_V_data_V_1_ack_in,
      I1 => ap_CS_fsm_state3,
      O => ap_NS_fsm1104_out
    );
\ap_CS_fsm[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222000022220FFF"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => \^ap_ready\,
      I2 => stream_out_V_data_V_1_ack_in,
      I3 => ap_CS_fsm_state3,
      I4 => ap_CS_fsm_state2,
      I5 => \ap_CS_fsm_reg_n_0_[0]\,
      O => \ap_CS_fsm[2]_i_1_n_0\
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_CS_fsm[0]_i_1_n_0\,
      Q => \ap_CS_fsm_reg_n_0_[0]\,
      S => ap_rst_n_inv
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_CS_fsm[1]_i_1_n_0\,
      Q => ap_CS_fsm_state2,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_CS_fsm[2]_i_1_n_0\,
      Q => ap_CS_fsm_state3,
      R => ap_rst_n_inv
    );
ap_idle_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \ap_CS_fsm_reg_n_0_[0]\,
      I1 => ap_start,
      O => ap_idle
    );
ap_ready_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000200"
    )
        port map (
      I0 => stream_out_V_data_V_1_ack_in,
      I1 => ap_ready_INST_0_i_1_n_0,
      I2 => ap_ready_INST_0_i_2_n_0,
      I3 => ap_CS_fsm_state2,
      I4 => stream_in_TVALID,
      O => \^ap_ready\
    );
ap_ready_INST_0_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => stream_out_V_last_V_1_ack_in,
      I1 => stream_out_V_user_V_1_ack_in,
      I2 => stream_out_V_dest_V_1_ack_in,
      I3 => stream_out_V_id_V_1_ack_in,
      O => ap_ready_INST_0_i_1_n_0
    );
ap_ready_INST_0_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08FFFFFF"
    )
        port map (
      I0 => stream_in_TVALID,
      I1 => enable_V(0),
      I2 => stream_out_V_data_V_1_ack_in,
      I3 => stream_out_V_strb_V_1_ack_in,
      I4 => stream_out_V_keep_V_1_ack_in,
      O => ap_ready_INST_0_i_2_n_0
    );
stream_in_TREADY_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => ap_ready_INST_0_i_2_n_0,
      I2 => ap_ready_INST_0_i_1_n_0,
      I3 => stream_out_V_data_V_1_ack_in,
      I4 => stream_in_TVALID,
      O => stream_in_TREADY
    );
\stream_out_TDATA[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(0),
      I1 => stream_out_V_data_V_1_payload_A(0),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(0)
    );
\stream_out_TDATA[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(10),
      I1 => stream_out_V_data_V_1_payload_A(10),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(10)
    );
\stream_out_TDATA[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(11),
      I1 => stream_out_V_data_V_1_payload_A(11),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(11)
    );
\stream_out_TDATA[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(12),
      I1 => stream_out_V_data_V_1_payload_A(12),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(12)
    );
\stream_out_TDATA[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(13),
      I1 => stream_out_V_data_V_1_payload_A(13),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(13)
    );
\stream_out_TDATA[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(14),
      I1 => stream_out_V_data_V_1_payload_A(14),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(14)
    );
\stream_out_TDATA[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(15),
      I1 => stream_out_V_data_V_1_payload_A(15),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(15)
    );
\stream_out_TDATA[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(16),
      I1 => stream_out_V_data_V_1_payload_A(16),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(16)
    );
\stream_out_TDATA[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(17),
      I1 => stream_out_V_data_V_1_payload_A(17),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(17)
    );
\stream_out_TDATA[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(18),
      I1 => stream_out_V_data_V_1_payload_A(18),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(18)
    );
\stream_out_TDATA[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(19),
      I1 => stream_out_V_data_V_1_payload_A(19),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(19)
    );
\stream_out_TDATA[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(1),
      I1 => stream_out_V_data_V_1_payload_A(1),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(1)
    );
\stream_out_TDATA[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(20),
      I1 => stream_out_V_data_V_1_payload_A(20),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(20)
    );
\stream_out_TDATA[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(21),
      I1 => stream_out_V_data_V_1_payload_A(21),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(21)
    );
\stream_out_TDATA[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(22),
      I1 => stream_out_V_data_V_1_payload_A(22),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(22)
    );
\stream_out_TDATA[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(23),
      I1 => stream_out_V_data_V_1_payload_A(23),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(23)
    );
\stream_out_TDATA[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(24),
      I1 => stream_out_V_data_V_1_payload_A(24),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(24)
    );
\stream_out_TDATA[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(25),
      I1 => stream_out_V_data_V_1_payload_A(25),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(25)
    );
\stream_out_TDATA[26]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(26),
      I1 => stream_out_V_data_V_1_payload_A(26),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(26)
    );
\stream_out_TDATA[27]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(27),
      I1 => stream_out_V_data_V_1_payload_A(27),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(27)
    );
\stream_out_TDATA[28]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(28),
      I1 => stream_out_V_data_V_1_payload_A(28),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(28)
    );
\stream_out_TDATA[29]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(29),
      I1 => stream_out_V_data_V_1_payload_A(29),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(29)
    );
\stream_out_TDATA[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(2),
      I1 => stream_out_V_data_V_1_payload_A(2),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(2)
    );
\stream_out_TDATA[30]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(30),
      I1 => stream_out_V_data_V_1_payload_A(30),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(30)
    );
\stream_out_TDATA[31]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(31),
      I1 => stream_out_V_data_V_1_payload_A(31),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(31)
    );
\stream_out_TDATA[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(3),
      I1 => stream_out_V_data_V_1_payload_A(3),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(3)
    );
\stream_out_TDATA[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(4),
      I1 => stream_out_V_data_V_1_payload_A(4),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(4)
    );
\stream_out_TDATA[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(5),
      I1 => stream_out_V_data_V_1_payload_A(5),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(5)
    );
\stream_out_TDATA[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(6),
      I1 => stream_out_V_data_V_1_payload_A(6),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(6)
    );
\stream_out_TDATA[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(7),
      I1 => stream_out_V_data_V_1_payload_A(7),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(7)
    );
\stream_out_TDATA[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(8),
      I1 => stream_out_V_data_V_1_payload_A(8),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(8)
    );
\stream_out_TDATA[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => stream_out_V_data_V_1_payload_B(9),
      I1 => stream_out_V_data_V_1_payload_A(9),
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_TDATA(9)
    );
\stream_out_TDEST[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_dest_V_1_payload_B,
      I1 => stream_out_V_dest_V_1_sel,
      I2 => stream_out_V_dest_V_1_payload_A,
      O => stream_out_TDEST(0)
    );
\stream_out_TID[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_id_V_1_payload_B,
      I1 => stream_out_V_id_V_1_sel,
      I2 => stream_out_V_id_V_1_payload_A,
      O => stream_out_TID(0)
    );
\stream_out_TKEEP[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_keep_V_1_payload_B(0),
      I1 => stream_out_V_keep_V_1_sel,
      I2 => stream_out_V_keep_V_1_payload_A(0),
      O => stream_out_TKEEP(0)
    );
\stream_out_TKEEP[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_keep_V_1_payload_B(1),
      I1 => stream_out_V_keep_V_1_sel,
      I2 => stream_out_V_keep_V_1_payload_A(1),
      O => stream_out_TKEEP(1)
    );
\stream_out_TKEEP[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_keep_V_1_payload_B(2),
      I1 => stream_out_V_keep_V_1_sel,
      I2 => stream_out_V_keep_V_1_payload_A(2),
      O => stream_out_TKEEP(2)
    );
\stream_out_TKEEP[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_keep_V_1_payload_B(3),
      I1 => stream_out_V_keep_V_1_sel,
      I2 => stream_out_V_keep_V_1_payload_A(3),
      O => stream_out_TKEEP(3)
    );
\stream_out_TLAST[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_last_V_1_payload_B,
      I1 => stream_out_V_last_V_1_sel,
      I2 => stream_out_V_last_V_1_payload_A,
      O => stream_out_TLAST(0)
    );
\stream_out_TSTRB[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_strb_V_1_payload_B(0),
      I1 => stream_out_V_strb_V_1_sel,
      I2 => stream_out_V_strb_V_1_payload_A(0),
      O => stream_out_TSTRB(0)
    );
\stream_out_TSTRB[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_strb_V_1_payload_B(1),
      I1 => stream_out_V_strb_V_1_sel,
      I2 => stream_out_V_strb_V_1_payload_A(1),
      O => stream_out_TSTRB(1)
    );
\stream_out_TSTRB[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_strb_V_1_payload_B(2),
      I1 => stream_out_V_strb_V_1_sel,
      I2 => stream_out_V_strb_V_1_payload_A(2),
      O => stream_out_TSTRB(2)
    );
\stream_out_TSTRB[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_strb_V_1_payload_B(3),
      I1 => stream_out_V_strb_V_1_sel,
      I2 => stream_out_V_strb_V_1_payload_A(3),
      O => stream_out_TSTRB(3)
    );
\stream_out_TUSER[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => stream_out_V_user_V_1_payload_B,
      I1 => stream_out_V_user_V_1_sel,
      I2 => stream_out_V_user_V_1_payload_A,
      O => stream_out_TUSER(0)
    );
\stream_out_V_data_V_1_payload_A[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \stream_out_V_data_V_1_state_reg_n_0_[0]\,
      I1 => stream_out_V_data_V_1_ack_in,
      I2 => stream_out_V_data_V_1_sel_wr,
      O => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\
    );
\stream_out_V_data_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(0),
      Q => stream_out_V_data_V_1_payload_A(0),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(10),
      Q => stream_out_V_data_V_1_payload_A(10),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(11),
      Q => stream_out_V_data_V_1_payload_A(11),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(12),
      Q => stream_out_V_data_V_1_payload_A(12),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(13),
      Q => stream_out_V_data_V_1_payload_A(13),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(14),
      Q => stream_out_V_data_V_1_payload_A(14),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(15),
      Q => stream_out_V_data_V_1_payload_A(15),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(16),
      Q => stream_out_V_data_V_1_payload_A(16),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(17),
      Q => stream_out_V_data_V_1_payload_A(17),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(18),
      Q => stream_out_V_data_V_1_payload_A(18),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(19),
      Q => stream_out_V_data_V_1_payload_A(19),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(1),
      Q => stream_out_V_data_V_1_payload_A(1),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(20),
      Q => stream_out_V_data_V_1_payload_A(20),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(21),
      Q => stream_out_V_data_V_1_payload_A(21),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(22),
      Q => stream_out_V_data_V_1_payload_A(22),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(23),
      Q => stream_out_V_data_V_1_payload_A(23),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(24),
      Q => stream_out_V_data_V_1_payload_A(24),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(25),
      Q => stream_out_V_data_V_1_payload_A(25),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(26),
      Q => stream_out_V_data_V_1_payload_A(26),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(27),
      Q => stream_out_V_data_V_1_payload_A(27),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(28),
      Q => stream_out_V_data_V_1_payload_A(28),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(29),
      Q => stream_out_V_data_V_1_payload_A(29),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(2),
      Q => stream_out_V_data_V_1_payload_A(2),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(30),
      Q => stream_out_V_data_V_1_payload_A(30),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(31),
      Q => stream_out_V_data_V_1_payload_A(31),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(3),
      Q => stream_out_V_data_V_1_payload_A(3),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(4),
      Q => stream_out_V_data_V_1_payload_A(4),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(5),
      Q => stream_out_V_data_V_1_payload_A(5),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(6),
      Q => stream_out_V_data_V_1_payload_A(6),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(7),
      Q => stream_out_V_data_V_1_payload_A(7),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(8),
      Q => stream_out_V_data_V_1_payload_A(8),
      R => '0'
    );
\stream_out_V_data_V_1_payload_A_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_data_V_1_payload_A[31]_i_1_n_0\,
      D => stream_in_TDATA(9),
      Q => stream_out_V_data_V_1_payload_A(9),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => stream_out_V_data_V_1_sel_wr,
      I1 => \stream_out_V_data_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_data_V_1_ack_in,
      O => stream_out_V_data_V_1_load_B
    );
\stream_out_V_data_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(0),
      Q => stream_out_V_data_V_1_payload_B(0),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(10),
      Q => stream_out_V_data_V_1_payload_B(10),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(11),
      Q => stream_out_V_data_V_1_payload_B(11),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(12),
      Q => stream_out_V_data_V_1_payload_B(12),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(13),
      Q => stream_out_V_data_V_1_payload_B(13),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(14),
      Q => stream_out_V_data_V_1_payload_B(14),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(15),
      Q => stream_out_V_data_V_1_payload_B(15),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(16),
      Q => stream_out_V_data_V_1_payload_B(16),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(17),
      Q => stream_out_V_data_V_1_payload_B(17),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(18),
      Q => stream_out_V_data_V_1_payload_B(18),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(19),
      Q => stream_out_V_data_V_1_payload_B(19),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(1),
      Q => stream_out_V_data_V_1_payload_B(1),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(20),
      Q => stream_out_V_data_V_1_payload_B(20),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(21),
      Q => stream_out_V_data_V_1_payload_B(21),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(22),
      Q => stream_out_V_data_V_1_payload_B(22),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(23),
      Q => stream_out_V_data_V_1_payload_B(23),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(24),
      Q => stream_out_V_data_V_1_payload_B(24),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(25),
      Q => stream_out_V_data_V_1_payload_B(25),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(26),
      Q => stream_out_V_data_V_1_payload_B(26),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(27),
      Q => stream_out_V_data_V_1_payload_B(27),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(28),
      Q => stream_out_V_data_V_1_payload_B(28),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(29),
      Q => stream_out_V_data_V_1_payload_B(29),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(2),
      Q => stream_out_V_data_V_1_payload_B(2),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(30),
      Q => stream_out_V_data_V_1_payload_B(30),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(31),
      Q => stream_out_V_data_V_1_payload_B(31),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(3),
      Q => stream_out_V_data_V_1_payload_B(3),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(4),
      Q => stream_out_V_data_V_1_payload_B(4),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(5),
      Q => stream_out_V_data_V_1_payload_B(5),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(6),
      Q => stream_out_V_data_V_1_payload_B(6),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(7),
      Q => stream_out_V_data_V_1_payload_B(7),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(8),
      Q => stream_out_V_data_V_1_payload_B(8),
      R => '0'
    );
\stream_out_V_data_V_1_payload_B_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_data_V_1_load_B,
      D => stream_in_TDATA(9),
      Q => stream_out_V_data_V_1_payload_B(9),
      R => '0'
    );
stream_out_V_data_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_TREADY,
      I1 => \stream_out_V_data_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_data_V_1_sel,
      O => stream_out_V_data_V_1_sel_rd_i_1_n_0
    );
stream_out_V_data_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_data_V_1_sel_rd_i_1_n_0,
      Q => stream_out_V_data_V_1_sel,
      R => ap_rst_n_inv
    );
stream_out_V_data_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_V_data_V_1_ack_in,
      I1 => ap_NS_fsm1,
      I2 => stream_out_V_data_V_1_sel_wr,
      O => stream_out_V_data_V_1_sel_wr_i_1_n_0
    );
stream_out_V_data_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_data_V_1_sel_wr_i_1_n_0,
      Q => stream_out_V_data_V_1_sel_wr,
      R => ap_rst_n_inv
    );
\stream_out_V_data_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \stream_out_V_data_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_data_V_1_ack_in,
      I3 => stream_out_TREADY,
      I4 => ap_NS_fsm1,
      O => \stream_out_V_data_V_1_state[0]_i_1_n_0\
    );
\stream_out_V_data_V_1_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ap_rst_n,
      O => ap_rst_n_inv
    );
\stream_out_V_data_V_1_state[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F3FB"
    )
        port map (
      I0 => stream_out_V_data_V_1_ack_in,
      I1 => \stream_out_V_data_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_TREADY,
      I3 => ap_NS_fsm1,
      O => \stream_out_V_data_V_1_state[1]_i_2_n_0\
    );
\stream_out_V_data_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_data_V_1_state[0]_i_1_n_0\,
      Q => \stream_out_V_data_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\stream_out_V_data_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_data_V_1_state[1]_i_2_n_0\,
      Q => stream_out_V_data_V_1_ack_in,
      R => ap_rst_n_inv
    );
\stream_out_V_dest_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => stream_in_TDEST(0),
      I1 => \^stream_out_tvalid\,
      I2 => stream_out_V_dest_V_1_ack_in,
      I3 => stream_out_V_dest_V_1_sel_wr,
      I4 => stream_out_V_dest_V_1_payload_A,
      O => \stream_out_V_dest_V_1_payload_A[0]_i_1_n_0\
    );
\stream_out_V_dest_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_dest_V_1_payload_A[0]_i_1_n_0\,
      Q => stream_out_V_dest_V_1_payload_A,
      R => '0'
    );
\stream_out_V_dest_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBFB8808"
    )
        port map (
      I0 => stream_in_TDEST(0),
      I1 => stream_out_V_dest_V_1_sel_wr,
      I2 => \^stream_out_tvalid\,
      I3 => stream_out_V_dest_V_1_ack_in,
      I4 => stream_out_V_dest_V_1_payload_B,
      O => \stream_out_V_dest_V_1_payload_B[0]_i_1_n_0\
    );
\stream_out_V_dest_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_dest_V_1_payload_B[0]_i_1_n_0\,
      Q => stream_out_V_dest_V_1_payload_B,
      R => '0'
    );
stream_out_V_dest_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_TREADY,
      I1 => \^stream_out_tvalid\,
      I2 => stream_out_V_dest_V_1_sel,
      O => stream_out_V_dest_V_1_sel_rd_i_1_n_0
    );
stream_out_V_dest_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_dest_V_1_sel_rd_i_1_n_0,
      Q => stream_out_V_dest_V_1_sel,
      R => ap_rst_n_inv
    );
stream_out_V_dest_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_V_dest_V_1_ack_in,
      I1 => ap_NS_fsm1,
      I2 => stream_out_V_dest_V_1_sel_wr,
      O => stream_out_V_dest_V_1_sel_wr_i_1_n_0
    );
stream_out_V_dest_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_dest_V_1_sel_wr_i_1_n_0,
      Q => stream_out_V_dest_V_1_sel_wr,
      R => ap_rst_n_inv
    );
\stream_out_V_dest_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^stream_out_tvalid\,
      I2 => stream_out_V_dest_V_1_ack_in,
      I3 => stream_out_TREADY,
      I4 => ap_NS_fsm1,
      O => \stream_out_V_dest_V_1_state[0]_i_1_n_0\
    );
\stream_out_V_dest_V_1_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0200000000000000"
    )
        port map (
      I0 => stream_out_V_data_V_1_ack_in,
      I1 => ap_ready_INST_0_i_1_n_0,
      I2 => ap_ready_INST_0_i_2_n_0,
      I3 => ap_CS_fsm_state2,
      I4 => stream_in_TVALID,
      I5 => enable_V(0),
      O => ap_NS_fsm1
    );
\stream_out_V_dest_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCF"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => stream_out_TREADY,
      I2 => \^stream_out_tvalid\,
      I3 => stream_out_V_dest_V_1_ack_in,
      O => \stream_out_V_dest_V_1_state[1]_i_1_n_0\
    );
\stream_out_V_dest_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_dest_V_1_state[0]_i_1_n_0\,
      Q => \^stream_out_tvalid\,
      R => '0'
    );
\stream_out_V_dest_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_dest_V_1_state[1]_i_1_n_0\,
      Q => stream_out_V_dest_V_1_ack_in,
      R => ap_rst_n_inv
    );
\stream_out_V_id_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => stream_in_TID(0),
      I1 => \stream_out_V_id_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_id_V_1_ack_in,
      I3 => stream_out_V_id_V_1_sel_wr,
      I4 => stream_out_V_id_V_1_payload_A,
      O => \stream_out_V_id_V_1_payload_A[0]_i_1_n_0\
    );
\stream_out_V_id_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_id_V_1_payload_A[0]_i_1_n_0\,
      Q => stream_out_V_id_V_1_payload_A,
      R => '0'
    );
\stream_out_V_id_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBFB8808"
    )
        port map (
      I0 => stream_in_TID(0),
      I1 => stream_out_V_id_V_1_sel_wr,
      I2 => \stream_out_V_id_V_1_state_reg_n_0_[0]\,
      I3 => stream_out_V_id_V_1_ack_in,
      I4 => stream_out_V_id_V_1_payload_B,
      O => \stream_out_V_id_V_1_payload_B[0]_i_1_n_0\
    );
\stream_out_V_id_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_id_V_1_payload_B[0]_i_1_n_0\,
      Q => stream_out_V_id_V_1_payload_B,
      R => '0'
    );
stream_out_V_id_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_TREADY,
      I1 => \stream_out_V_id_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_id_V_1_sel,
      O => stream_out_V_id_V_1_sel_rd_i_1_n_0
    );
stream_out_V_id_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_id_V_1_sel_rd_i_1_n_0,
      Q => stream_out_V_id_V_1_sel,
      R => ap_rst_n_inv
    );
stream_out_V_id_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_V_id_V_1_ack_in,
      I1 => ap_NS_fsm1,
      I2 => stream_out_V_id_V_1_sel_wr,
      O => stream_out_V_id_V_1_sel_wr_i_1_n_0
    );
stream_out_V_id_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_id_V_1_sel_wr_i_1_n_0,
      Q => stream_out_V_id_V_1_sel_wr,
      R => ap_rst_n_inv
    );
\stream_out_V_id_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \stream_out_V_id_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_id_V_1_ack_in,
      I3 => stream_out_TREADY,
      I4 => ap_NS_fsm1,
      O => \stream_out_V_id_V_1_state[0]_i_1_n_0\
    );
\stream_out_V_id_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCF"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => stream_out_TREADY,
      I2 => \stream_out_V_id_V_1_state_reg_n_0_[0]\,
      I3 => stream_out_V_id_V_1_ack_in,
      O => \stream_out_V_id_V_1_state[1]_i_1_n_0\
    );
\stream_out_V_id_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_id_V_1_state[0]_i_1_n_0\,
      Q => \stream_out_V_id_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\stream_out_V_id_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_id_V_1_state[1]_i_1_n_0\,
      Q => stream_out_V_id_V_1_ack_in,
      R => ap_rst_n_inv
    );
\stream_out_V_keep_V_1_payload_A[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \stream_out_V_keep_V_1_state_reg_n_0_[0]\,
      I1 => stream_out_V_keep_V_1_ack_in,
      I2 => stream_out_V_keep_V_1_sel_wr,
      O => \stream_out_V_keep_V_1_payload_A[3]_i_1_n_0\
    );
\stream_out_V_keep_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_keep_V_1_payload_A[3]_i_1_n_0\,
      D => stream_in_TKEEP(0),
      Q => stream_out_V_keep_V_1_payload_A(0),
      R => '0'
    );
\stream_out_V_keep_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_keep_V_1_payload_A[3]_i_1_n_0\,
      D => stream_in_TKEEP(1),
      Q => stream_out_V_keep_V_1_payload_A(1),
      R => '0'
    );
\stream_out_V_keep_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_keep_V_1_payload_A[3]_i_1_n_0\,
      D => stream_in_TKEEP(2),
      Q => stream_out_V_keep_V_1_payload_A(2),
      R => '0'
    );
\stream_out_V_keep_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_keep_V_1_payload_A[3]_i_1_n_0\,
      D => stream_in_TKEEP(3),
      Q => stream_out_V_keep_V_1_payload_A(3),
      R => '0'
    );
\stream_out_V_keep_V_1_payload_B[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => stream_out_V_keep_V_1_sel_wr,
      I1 => \stream_out_V_keep_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_keep_V_1_ack_in,
      O => stream_out_V_keep_V_1_load_B
    );
\stream_out_V_keep_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_keep_V_1_load_B,
      D => stream_in_TKEEP(0),
      Q => stream_out_V_keep_V_1_payload_B(0),
      R => '0'
    );
\stream_out_V_keep_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_keep_V_1_load_B,
      D => stream_in_TKEEP(1),
      Q => stream_out_V_keep_V_1_payload_B(1),
      R => '0'
    );
\stream_out_V_keep_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_keep_V_1_load_B,
      D => stream_in_TKEEP(2),
      Q => stream_out_V_keep_V_1_payload_B(2),
      R => '0'
    );
\stream_out_V_keep_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_keep_V_1_load_B,
      D => stream_in_TKEEP(3),
      Q => stream_out_V_keep_V_1_payload_B(3),
      R => '0'
    );
stream_out_V_keep_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \stream_out_V_keep_V_1_state_reg_n_0_[0]\,
      I1 => stream_out_TREADY,
      I2 => stream_out_V_keep_V_1_sel,
      O => stream_out_V_keep_V_1_sel_rd_i_1_n_0
    );
stream_out_V_keep_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_keep_V_1_sel_rd_i_1_n_0,
      Q => stream_out_V_keep_V_1_sel,
      R => ap_rst_n_inv
    );
stream_out_V_keep_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_V_keep_V_1_ack_in,
      I1 => ap_NS_fsm1,
      I2 => stream_out_V_keep_V_1_sel_wr,
      O => stream_out_V_keep_V_1_sel_wr_i_1_n_0
    );
stream_out_V_keep_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_keep_V_1_sel_wr_i_1_n_0,
      Q => stream_out_V_keep_V_1_sel_wr,
      R => ap_rst_n_inv
    );
\stream_out_V_keep_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \stream_out_V_keep_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_keep_V_1_ack_in,
      I3 => stream_out_TREADY,
      I4 => ap_NS_fsm1,
      O => \stream_out_V_keep_V_1_state[0]_i_1_n_0\
    );
\stream_out_V_keep_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCF"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => stream_out_TREADY,
      I2 => \stream_out_V_keep_V_1_state_reg_n_0_[0]\,
      I3 => stream_out_V_keep_V_1_ack_in,
      O => \stream_out_V_keep_V_1_state[1]_i_1_n_0\
    );
\stream_out_V_keep_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_keep_V_1_state[0]_i_1_n_0\,
      Q => \stream_out_V_keep_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\stream_out_V_keep_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_keep_V_1_state[1]_i_1_n_0\,
      Q => stream_out_V_keep_V_1_ack_in,
      R => ap_rst_n_inv
    );
\stream_out_V_last_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => stream_in_TLAST(0),
      I1 => \stream_out_V_last_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_last_V_1_ack_in,
      I3 => stream_out_V_last_V_1_sel_wr,
      I4 => stream_out_V_last_V_1_payload_A,
      O => \stream_out_V_last_V_1_payload_A[0]_i_1_n_0\
    );
\stream_out_V_last_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_last_V_1_payload_A[0]_i_1_n_0\,
      Q => stream_out_V_last_V_1_payload_A,
      R => '0'
    );
\stream_out_V_last_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBFB8808"
    )
        port map (
      I0 => stream_in_TLAST(0),
      I1 => stream_out_V_last_V_1_sel_wr,
      I2 => \stream_out_V_last_V_1_state_reg_n_0_[0]\,
      I3 => stream_out_V_last_V_1_ack_in,
      I4 => stream_out_V_last_V_1_payload_B,
      O => \stream_out_V_last_V_1_payload_B[0]_i_1_n_0\
    );
\stream_out_V_last_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_last_V_1_payload_B[0]_i_1_n_0\,
      Q => stream_out_V_last_V_1_payload_B,
      R => '0'
    );
stream_out_V_last_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \stream_out_V_last_V_1_state_reg_n_0_[0]\,
      I1 => stream_out_TREADY,
      I2 => stream_out_V_last_V_1_sel,
      O => stream_out_V_last_V_1_sel_rd_i_1_n_0
    );
stream_out_V_last_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_last_V_1_sel_rd_i_1_n_0,
      Q => stream_out_V_last_V_1_sel,
      R => ap_rst_n_inv
    );
stream_out_V_last_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_V_last_V_1_ack_in,
      I1 => ap_NS_fsm1,
      I2 => stream_out_V_last_V_1_sel_wr,
      O => stream_out_V_last_V_1_sel_wr_i_1_n_0
    );
stream_out_V_last_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_last_V_1_sel_wr_i_1_n_0,
      Q => stream_out_V_last_V_1_sel_wr,
      R => ap_rst_n_inv
    );
\stream_out_V_last_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \stream_out_V_last_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_last_V_1_ack_in,
      I3 => stream_out_TREADY,
      I4 => ap_NS_fsm1,
      O => \stream_out_V_last_V_1_state[0]_i_1_n_0\
    );
\stream_out_V_last_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCF"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => stream_out_TREADY,
      I2 => \stream_out_V_last_V_1_state_reg_n_0_[0]\,
      I3 => stream_out_V_last_V_1_ack_in,
      O => \stream_out_V_last_V_1_state[1]_i_1_n_0\
    );
\stream_out_V_last_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_last_V_1_state[0]_i_1_n_0\,
      Q => \stream_out_V_last_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\stream_out_V_last_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_last_V_1_state[1]_i_1_n_0\,
      Q => stream_out_V_last_V_1_ack_in,
      R => ap_rst_n_inv
    );
\stream_out_V_strb_V_1_payload_A[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \stream_out_V_strb_V_1_state_reg_n_0_[0]\,
      I1 => stream_out_V_strb_V_1_ack_in,
      I2 => stream_out_V_strb_V_1_sel_wr,
      O => \stream_out_V_strb_V_1_payload_A[3]_i_1_n_0\
    );
\stream_out_V_strb_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_strb_V_1_payload_A[3]_i_1_n_0\,
      D => stream_in_TSTRB(0),
      Q => stream_out_V_strb_V_1_payload_A(0),
      R => '0'
    );
\stream_out_V_strb_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_strb_V_1_payload_A[3]_i_1_n_0\,
      D => stream_in_TSTRB(1),
      Q => stream_out_V_strb_V_1_payload_A(1),
      R => '0'
    );
\stream_out_V_strb_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_strb_V_1_payload_A[3]_i_1_n_0\,
      D => stream_in_TSTRB(2),
      Q => stream_out_V_strb_V_1_payload_A(2),
      R => '0'
    );
\stream_out_V_strb_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \stream_out_V_strb_V_1_payload_A[3]_i_1_n_0\,
      D => stream_in_TSTRB(3),
      Q => stream_out_V_strb_V_1_payload_A(3),
      R => '0'
    );
\stream_out_V_strb_V_1_payload_B[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => stream_out_V_strb_V_1_sel_wr,
      I1 => \stream_out_V_strb_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_strb_V_1_ack_in,
      O => stream_out_V_strb_V_1_load_B
    );
\stream_out_V_strb_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_strb_V_1_load_B,
      D => stream_in_TSTRB(0),
      Q => stream_out_V_strb_V_1_payload_B(0),
      R => '0'
    );
\stream_out_V_strb_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_strb_V_1_load_B,
      D => stream_in_TSTRB(1),
      Q => stream_out_V_strb_V_1_payload_B(1),
      R => '0'
    );
\stream_out_V_strb_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_strb_V_1_load_B,
      D => stream_in_TSTRB(2),
      Q => stream_out_V_strb_V_1_payload_B(2),
      R => '0'
    );
\stream_out_V_strb_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => stream_out_V_strb_V_1_load_B,
      D => stream_in_TSTRB(3),
      Q => stream_out_V_strb_V_1_payload_B(3),
      R => '0'
    );
stream_out_V_strb_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \stream_out_V_strb_V_1_state_reg_n_0_[0]\,
      I1 => stream_out_TREADY,
      I2 => stream_out_V_strb_V_1_sel,
      O => stream_out_V_strb_V_1_sel_rd_i_1_n_0
    );
stream_out_V_strb_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_strb_V_1_sel_rd_i_1_n_0,
      Q => stream_out_V_strb_V_1_sel,
      R => ap_rst_n_inv
    );
stream_out_V_strb_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_V_strb_V_1_ack_in,
      I1 => ap_NS_fsm1,
      I2 => stream_out_V_strb_V_1_sel_wr,
      O => stream_out_V_strb_V_1_sel_wr_i_1_n_0
    );
stream_out_V_strb_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_strb_V_1_sel_wr_i_1_n_0,
      Q => stream_out_V_strb_V_1_sel_wr,
      R => ap_rst_n_inv
    );
\stream_out_V_strb_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \stream_out_V_strb_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_strb_V_1_ack_in,
      I3 => stream_out_TREADY,
      I4 => ap_NS_fsm1,
      O => \stream_out_V_strb_V_1_state[0]_i_1_n_0\
    );
\stream_out_V_strb_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCF"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => stream_out_TREADY,
      I2 => \stream_out_V_strb_V_1_state_reg_n_0_[0]\,
      I3 => stream_out_V_strb_V_1_ack_in,
      O => \stream_out_V_strb_V_1_state[1]_i_1_n_0\
    );
\stream_out_V_strb_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_strb_V_1_state[0]_i_1_n_0\,
      Q => \stream_out_V_strb_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\stream_out_V_strb_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_strb_V_1_state[1]_i_1_n_0\,
      Q => stream_out_V_strb_V_1_ack_in,
      R => ap_rst_n_inv
    );
\stream_out_V_user_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => stream_in_TUSER(0),
      I1 => \stream_out_V_user_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_user_V_1_ack_in,
      I3 => stream_out_V_user_V_1_sel_wr,
      I4 => stream_out_V_user_V_1_payload_A,
      O => \stream_out_V_user_V_1_payload_A[0]_i_1_n_0\
    );
\stream_out_V_user_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_user_V_1_payload_A[0]_i_1_n_0\,
      Q => stream_out_V_user_V_1_payload_A,
      R => '0'
    );
\stream_out_V_user_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBFB8808"
    )
        port map (
      I0 => stream_in_TUSER(0),
      I1 => stream_out_V_user_V_1_sel_wr,
      I2 => \stream_out_V_user_V_1_state_reg_n_0_[0]\,
      I3 => stream_out_V_user_V_1_ack_in,
      I4 => stream_out_V_user_V_1_payload_B,
      O => \stream_out_V_user_V_1_payload_B[0]_i_1_n_0\
    );
\stream_out_V_user_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_user_V_1_payload_B[0]_i_1_n_0\,
      Q => stream_out_V_user_V_1_payload_B,
      R => '0'
    );
stream_out_V_user_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_TREADY,
      I1 => \stream_out_V_user_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_user_V_1_sel,
      O => stream_out_V_user_V_1_sel_rd_i_1_n_0
    );
stream_out_V_user_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_user_V_1_sel_rd_i_1_n_0,
      Q => stream_out_V_user_V_1_sel,
      R => ap_rst_n_inv
    );
stream_out_V_user_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => stream_out_V_user_V_1_ack_in,
      I1 => ap_NS_fsm1,
      I2 => stream_out_V_user_V_1_sel_wr,
      O => stream_out_V_user_V_1_sel_wr_i_1_n_0
    );
stream_out_V_user_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => stream_out_V_user_V_1_sel_wr_i_1_n_0,
      Q => stream_out_V_user_V_1_sel_wr,
      R => ap_rst_n_inv
    );
\stream_out_V_user_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \stream_out_V_user_V_1_state_reg_n_0_[0]\,
      I2 => stream_out_V_user_V_1_ack_in,
      I3 => stream_out_TREADY,
      I4 => ap_NS_fsm1,
      O => \stream_out_V_user_V_1_state[0]_i_1_n_0\
    );
\stream_out_V_user_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCF"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => stream_out_TREADY,
      I2 => \stream_out_V_user_V_1_state_reg_n_0_[0]\,
      I3 => stream_out_V_user_V_1_ack_in,
      O => \stream_out_V_user_V_1_state[1]_i_1_n_0\
    );
\stream_out_V_user_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_user_V_1_state[0]_i_1_n_0\,
      Q => \stream_out_V_user_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\stream_out_V_user_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \stream_out_V_user_V_1_state[1]_i_1_n_0\,
      Q => stream_out_V_user_V_1_ack_in,
      R => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_stream_bypass_0_0 is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_done : out STD_LOGIC;
    ap_idle : out STD_LOGIC;
    ap_ready : out STD_LOGIC;
    stream_in_TVALID : in STD_LOGIC;
    stream_in_TREADY : out STD_LOGIC;
    stream_in_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    stream_in_TDEST : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TKEEP : in STD_LOGIC_VECTOR ( 3 downto 0 );
    stream_in_TSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    stream_in_TUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TID : in STD_LOGIC_VECTOR ( 0 to 0 );
    enable_V : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TVALID : out STD_LOGIC;
    stream_out_TREADY : in STD_LOGIC;
    stream_out_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    stream_out_TDEST : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TKEEP : out STD_LOGIC_VECTOR ( 3 downto 0 );
    stream_out_TSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    stream_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TID : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of system_stream_bypass_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of system_stream_bypass_0_0 : entity is "system_stream_bypass_0_0,stream_bypass,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of system_stream_bypass_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of system_stream_bypass_0_0 : entity is "HLS";
  attribute x_core_info : string;
  attribute x_core_info of system_stream_bypass_0_0 : entity is "stream_bypass,Vivado 2018.2";
end system_stream_bypass_0_0;

architecture STRUCTURE of system_stream_bypass_0_0 is
  attribute x_interface_info : string;
  attribute x_interface_info of ap_clk : signal is "xilinx.com:signal:clock:1.0 ap_clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of ap_clk : signal is "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF stream_in:stream_out, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  attribute x_interface_info of ap_done : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl done";
  attribute x_interface_info of ap_idle : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl idle";
  attribute x_interface_info of ap_ready : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ready";
  attribute x_interface_info of ap_rst_n : signal is "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  attribute x_interface_parameter of ap_rst_n : signal is "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}";
  attribute x_interface_info of ap_start : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl start";
  attribute x_interface_parameter of ap_start : signal is "XIL_INTERFACENAME ap_ctrl, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {start {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} done {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} idle {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} ready {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}";
  attribute x_interface_info of stream_in_TREADY : signal is "xilinx.com:interface:axis:1.0 stream_in TREADY";
  attribute x_interface_info of stream_in_TVALID : signal is "xilinx.com:interface:axis:1.0 stream_in TVALID";
  attribute x_interface_parameter of stream_in_TVALID : signal is "XIL_INTERFACENAME stream_in, TDATA_NUM_BYTES 4, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 32 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  attribute x_interface_info of stream_out_TREADY : signal is "xilinx.com:interface:axis:1.0 stream_out TREADY";
  attribute x_interface_info of stream_out_TVALID : signal is "xilinx.com:interface:axis:1.0 stream_out TVALID";
  attribute x_interface_parameter of stream_out_TVALID : signal is "XIL_INTERFACENAME stream_out, TDATA_NUM_BYTES 4, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 32 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  attribute x_interface_info of enable_V : signal is "xilinx.com:signal:data:1.0 enable_V DATA";
  attribute x_interface_parameter of enable_V : signal is "XIL_INTERFACENAME enable_V, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}";
  attribute x_interface_info of stream_in_TDATA : signal is "xilinx.com:interface:axis:1.0 stream_in TDATA";
  attribute x_interface_info of stream_in_TDEST : signal is "xilinx.com:interface:axis:1.0 stream_in TDEST";
  attribute x_interface_info of stream_in_TID : signal is "xilinx.com:interface:axis:1.0 stream_in TID";
  attribute x_interface_info of stream_in_TKEEP : signal is "xilinx.com:interface:axis:1.0 stream_in TKEEP";
  attribute x_interface_info of stream_in_TLAST : signal is "xilinx.com:interface:axis:1.0 stream_in TLAST";
  attribute x_interface_info of stream_in_TSTRB : signal is "xilinx.com:interface:axis:1.0 stream_in TSTRB";
  attribute x_interface_info of stream_in_TUSER : signal is "xilinx.com:interface:axis:1.0 stream_in TUSER";
  attribute x_interface_info of stream_out_TDATA : signal is "xilinx.com:interface:axis:1.0 stream_out TDATA";
  attribute x_interface_info of stream_out_TDEST : signal is "xilinx.com:interface:axis:1.0 stream_out TDEST";
  attribute x_interface_info of stream_out_TID : signal is "xilinx.com:interface:axis:1.0 stream_out TID";
  attribute x_interface_info of stream_out_TKEEP : signal is "xilinx.com:interface:axis:1.0 stream_out TKEEP";
  attribute x_interface_info of stream_out_TLAST : signal is "xilinx.com:interface:axis:1.0 stream_out TLAST";
  attribute x_interface_info of stream_out_TSTRB : signal is "xilinx.com:interface:axis:1.0 stream_out TSTRB";
  attribute x_interface_info of stream_out_TUSER : signal is "xilinx.com:interface:axis:1.0 stream_out TUSER";
begin
U0: entity work.system_stream_bypass_0_0_stream_bypass
     port map (
      ap_clk => ap_clk,
      ap_done => ap_done,
      ap_idle => ap_idle,
      ap_ready => ap_ready,
      ap_rst_n => ap_rst_n,
      ap_start => ap_start,
      enable_V(0) => enable_V(0),
      stream_in_TDATA(31 downto 0) => stream_in_TDATA(31 downto 0),
      stream_in_TDEST(0) => stream_in_TDEST(0),
      stream_in_TID(0) => stream_in_TID(0),
      stream_in_TKEEP(3 downto 0) => stream_in_TKEEP(3 downto 0),
      stream_in_TLAST(0) => stream_in_TLAST(0),
      stream_in_TREADY => stream_in_TREADY,
      stream_in_TSTRB(3 downto 0) => stream_in_TSTRB(3 downto 0),
      stream_in_TUSER(0) => stream_in_TUSER(0),
      stream_in_TVALID => stream_in_TVALID,
      stream_out_TDATA(31 downto 0) => stream_out_TDATA(31 downto 0),
      stream_out_TDEST(0) => stream_out_TDEST(0),
      stream_out_TID(0) => stream_out_TID(0),
      stream_out_TKEEP(3 downto 0) => stream_out_TKEEP(3 downto 0),
      stream_out_TLAST(0) => stream_out_TLAST(0),
      stream_out_TREADY => stream_out_TREADY,
      stream_out_TSTRB(3 downto 0) => stream_out_TSTRB(3 downto 0),
      stream_out_TUSER(0) => stream_out_TUSER(0),
      stream_out_TVALID => stream_out_TVALID
    );
end STRUCTURE;
