# 
# Synthesis run script generated by Vivado
# 

set TIME_start [clock seconds] 
proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
set_msg_config  -ruleid {1}  -id {Designutils 20-1280}  -string {{CRITICAL WARNING: [Designutils 20-1280] Could not find module 'ila_sfen_rxclk'. The XDC file d:/repos/Zybo-Z7-20-pcam-5c/proj/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_MIPI_D_PHY_RX_0_0/hdl/ila_sfen_rxclk/ila_v6_2/constraints/ila.xdc will not be read for any cell of this module.}}  -suppress 
set_msg_config  -ruleid {2}  -id {Designutils 20-1280}  -suppress 
set_msg_config  -ruleid {3}  -id {BD 41-758}  -string {{ERROR: [BD 41-758] The following clock pins are not connected to a valid clock source: 
/system_ila_0/clk}}  -suppress 
set_msg_config  -ruleid {4}  -id {BD 41-237}  -string {{ERROR: [BD 41-237] Bus Interface property CLK_DOMAIN does not match between /AXI_GammaCorrection_0/AXI_Slave_Interface(system_clk_wiz_0_0_clk_out2) and /AXI_BayerToRGB_1/AXI_Stream_Master(system_clk_wiz_0_0_clk_out1)}}  -suppress 
set_msg_config  -ruleid {5}  -id {Common 17-229}  -string {{ERROR: [Common 17-229] Tcl reset_property command for property <CONFIG.CLK_DOMAIN> is currently not supported.  The property <CONFIG.CLK_DOMAIN> cannot be reset.}}  -suppress 
set_msg_config  -ruleid {6}  -id {BD 41-237}  -string {{ERROR: [BD 41-237] Bus Interface property CLK_DOMAIN does not match between /center_image_0/image_in(system_clk_wiz_0_0_clk_out1) and /AXI_GammaCorrection_0/AXI_Stream_Master(system_clk_wiz_0_0_clk_out2)}}  -suppress 
set_msg_config  -ruleid {7}  -id {BD 41-238}  -string {{ERROR: [BD 41-238] Port/Pin property CLK_DOMAIN does not match between /AXI_GammaCorrection_0/StreamClk(system_clk_wiz_0_0_clk_out2) and /clk_wiz_0/clk_out2(system_clk_wiz_0_0_clk_out1)}}  -suppress 
set_param project.vivado.isBlockSynthRun true
set_msg_config -msgmgr_mode ooc_run
create_project -in_memory -part xc7z020clg400-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.cache/wt [current_project]
set_property parent.project_path /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.xpr [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_FIFO XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property ip_repo_paths {
  /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.ipdefs/repo_0
  /home/faku/master-project/vivado_hls/mnist_fp32/solution1/impl/ip
  /home/faku/master-project/vivado_hls/float_converter/solution1/impl/ip
  /home/faku/master-project/vivado_hls/stream_bypass/solution1/impl/ip
  /home/faku/master-project/vivado_hls/mnist_inference/axis/impl/ip
  /home/faku/master-project/vivado_hls/gray_rescale/solution1/impl/ip
  /home/faku/master-project/vivado_hls/stream_duplicate/default_solution/impl/ip
  /home/faku/master-project/vivado_hls/center_image/axis/impl/ip
} [current_project]
set_property ip_output_repo /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
read_ip -quiet /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2.xci
set_property used_in_implementation false [get_files -all /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_ooc.xdc]

# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]
set_param ips.enableIPCacheLiteLoad 0

set cached_ip [config_ip_cache -export -no_bom -use_project_ipc -dir /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.runs/system_auto_pc_2_synth_1 -new_name system_auto_pc_2 -ip [get_ips system_auto_pc_2]]

if { $cached_ip eq {} } {
close [open __synthesis_is_running__ w]

synth_design -top system_auto_pc_2 -part xc7z020clg400-1 -mode out_of_context

#---------------------------------------------------------
# Generate Checkpoint/Stub/Simulation Files For IP Cache
#---------------------------------------------------------
# disable binary constraint mode for IPCache checkpoints
set_param constraints.enableBinaryConstraints false

catch {
 write_checkpoint -force -noxdef -rename_prefix system_auto_pc_2_ system_auto_pc_2.dcp

 set ipCachedFiles {}
 write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_auto_pc_2_stub.v
 lappend ipCachedFiles system_auto_pc_2_stub.v

 write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_auto_pc_2_stub.vhdl
 lappend ipCachedFiles system_auto_pc_2_stub.vhdl

 write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_auto_pc_2_sim_netlist.v
 lappend ipCachedFiles system_auto_pc_2_sim_netlist.v

 write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_auto_pc_2_sim_netlist.vhdl
 lappend ipCachedFiles system_auto_pc_2_sim_netlist.vhdl
set TIME_taken [expr [clock seconds] - $TIME_start]

 config_ip_cache -add -dcp system_auto_pc_2.dcp -move_files $ipCachedFiles -use_project_ipc  -synth_runtime $TIME_taken  -ip [get_ips system_auto_pc_2]
}

rename_ref -prefix_all system_auto_pc_2_

# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef system_auto_pc_2.dcp
create_report "system_auto_pc_2_synth_1_synth_report_utilization_0" "report_utilization -file system_auto_pc_2_utilization_synth.rpt -pb system_auto_pc_2_utilization_synth.pb"

if { [catch {
  file copy -force /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.runs/system_auto_pc_2_synth_1/system_auto_pc_2.dcp /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2.dcp
} _RESULT ] } { 
  send_msg_id runtcl-3 error "ERROR: Unable to successfully create or copy the sub-design checkpoint file."
  error "ERROR: Unable to successfully create or copy the sub-design checkpoint file."
}

if { [catch {
  write_verilog -force -mode synth_stub /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_stub.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a Verilog synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  write_vhdl -force -mode synth_stub /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_stub.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a VHDL synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  write_verilog -force -mode funcsim /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_sim_netlist.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the Verilog functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}

if { [catch {
  write_vhdl -force -mode funcsim /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_sim_netlist.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the VHDL functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}


} else {


if { [catch {
  file copy -force /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.runs/system_auto_pc_2_synth_1/system_auto_pc_2.dcp /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2.dcp
} _RESULT ] } { 
  send_msg_id runtcl-3 error "ERROR: Unable to successfully create or copy the sub-design checkpoint file."
  error "ERROR: Unable to successfully create or copy the sub-design checkpoint file."
}

if { [catch {
  file rename -force /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.runs/system_auto_pc_2_synth_1/system_auto_pc_2_stub.v /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_stub.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a Verilog synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  file rename -force /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.runs/system_auto_pc_2_synth_1/system_auto_pc_2_stub.vhdl /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_stub.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a VHDL synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  file rename -force /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.runs/system_auto_pc_2_synth_1/system_auto_pc_2_sim_netlist.v /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_sim_netlist.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the Verilog functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}

if { [catch {
  file rename -force /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.runs/system_auto_pc_2_synth_1/system_auto_pc_2_sim_netlist.vhdl /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_sim_netlist.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the VHDL functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}

}; # end if cached_ip 

if {[file isdir /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.ip_user_files/ip/system_auto_pc_2]} {
  catch { 
    file copy -force /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_stub.v /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.ip_user_files/ip/system_auto_pc_2
  }
}

if {[file isdir /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.ip_user_files/ip/system_auto_pc_2]} {
  catch { 
    file copy -force /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_auto_pc_2/system_auto_pc_2_stub.vhdl /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.ip_user_files/ip/system_auto_pc_2
  }
}
file delete __synthesis_is_running__
close [open __synthesis_is_complete__ w]
