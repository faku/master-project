-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Thu Dec  5 21:47:02 2019
-- Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_center_image_0_0_sim_netlist.vhdl
-- Design      : system_center_image_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIvideo2Mat is
  port (
    ap_rst : out STD_LOGIC;
    image_in_TREADY : out STD_LOGIC;
    start_once_reg : out STD_LOGIC;
    ap_ready : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \mOutPtr_reg[1]\ : out STD_LOGIC;
    AXIvideo2Mat_U0_img_data_stream_2_V_write : out STD_LOGIC;
    internal_empty_n_reg : out STD_LOGIC;
    internal_empty_n_reg_0 : out STD_LOGIC;
    internal_empty_n_reg_1 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \SRL_SIG_reg[0][7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \SRL_SIG_reg[0][7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    start_for_Loop_loop_height_pro_U0_full_n : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    input_data_data_stre_2_full_n : in STD_LOGIC;
    input_data_data_stre_1_full_n : in STD_LOGIC;
    input_data_data_stre_full_n : in STD_LOGIC;
    image_in_TVALID : in STD_LOGIC;
    image_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    image_in_TUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    image_in_TDATA : in STD_LOGIC_VECTOR ( 23 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIvideo2Mat;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIvideo2Mat is
  signal AXI_video_strm_V_data_V_0_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_ack_out : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_data_out : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal AXI_video_strm_V_data_V_0_load_A : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_load_B : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_payload_A : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal AXI_video_strm_V_data_V_0_payload_B : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal AXI_video_strm_V_data_V_0_sel : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_sel2 : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_0_sel3__0\ : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_sel_rd_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_sel_wr_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_data_V_0_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\ : STD_LOGIC;
  signal AXI_video_strm_V_dest_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_dest_V_0_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_dest_V_0_state_reg_n_0_[0]\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_data_out : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_payload_A : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_payload_B : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_sel : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_sel_rd_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_sel_wr_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_last_V_0_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_0_state_reg_n_0_[0]\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_payload_A : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_payload_B : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_sel : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_sel_rd_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_sel_wr_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_user_V_0_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_0_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \^axivideo2mat_u0_img_data_stream_2_v_write\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \ap_CS_fsm[1]_i_2_n_0\ : STD_LOGIC;
  signal ap_CS_fsm_pp1_stage0 : STD_LOGIC;
  signal ap_CS_fsm_pp2_stage0 : STD_LOGIC;
  signal ap_CS_fsm_state10 : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state3 : STD_LOGIC;
  signal ap_CS_fsm_state4 : STD_LOGIC;
  signal ap_CS_fsm_state7 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal ap_block_pp1_stage0_110011 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter0 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter0_i_1_n_0 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter1_i_1_n_0 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter1_reg_n_0 : STD_LOGIC;
  signal ap_enable_reg_pp2_iter0 : STD_LOGIC;
  signal ap_enable_reg_pp2_iter0_i_1_n_0 : STD_LOGIC;
  signal ap_enable_reg_pp2_iter0_i_2_n_0 : STD_LOGIC;
  signal ap_enable_reg_pp2_iter1_i_1_n_0 : STD_LOGIC;
  signal ap_enable_reg_pp2_iter1_reg_n_0 : STD_LOGIC;
  signal \ap_phi_mux_axi_last_V_2_phi_fu_248_p4__0\ : STD_LOGIC;
  signal ap_ready_INST_0_i_2_n_0 : STD_LOGIC;
  signal ap_ready_INST_0_i_3_n_0 : STD_LOGIC;
  signal \^ap_rst\ : STD_LOGIC;
  signal axi_data_V1_reg_177 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \axi_data_V1_reg_177[0]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[10]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[11]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[12]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[13]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[14]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[15]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[16]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[17]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[18]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[19]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[1]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[20]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[21]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[22]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[23]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[4]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[5]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[6]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[7]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[8]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V1_reg_177[9]_i_1_n_0\ : STD_LOGIC;
  signal axi_data_V_1_reg_232 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \axi_data_V_1_reg_232[0]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[10]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[11]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[12]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[13]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[14]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[15]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[16]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[17]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[18]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[19]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[1]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[20]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[21]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[22]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[23]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[4]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[5]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[6]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[7]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[8]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_1_reg_232[9]_i_1_n_0\ : STD_LOGIC;
  signal axi_data_V_3_reg_291 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \axi_data_V_3_reg_291[0]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[10]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[11]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[12]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[13]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[14]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[15]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[16]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[17]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[18]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[19]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[1]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[20]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[21]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[22]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[23]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[4]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[5]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[6]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[7]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[8]_i_1_n_0\ : STD_LOGIC;
  signal \axi_data_V_3_reg_291[9]_i_1_n_0\ : STD_LOGIC;
  signal axi_last_V1_reg_167 : STD_LOGIC;
  signal \axi_last_V1_reg_167[0]_i_1_n_0\ : STD_LOGIC;
  signal axi_last_V_3_reg_279 : STD_LOGIC;
  signal \axi_last_V_3_reg_279[0]_i_1_n_0\ : STD_LOGIC;
  signal brmerge_fu_349_p2 : STD_LOGIC;
  signal brmerge_reg_425 : STD_LOGIC;
  signal \brmerge_reg_425[0]_i_1_n_0\ : STD_LOGIC;
  signal eol_1_reg_221 : STD_LOGIC;
  signal \eol_1_reg_221[0]_i_2_n_0\ : STD_LOGIC;
  signal \eol_2_reg_268[0]_i_1_n_0\ : STD_LOGIC;
  signal \eol_2_reg_268[0]_i_2_n_0\ : STD_LOGIC;
  signal \eol_2_reg_268_reg_n_0_[0]\ : STD_LOGIC;
  signal eol_reg_209 : STD_LOGIC;
  signal \eol_reg_209[0]_i_1_n_0\ : STD_LOGIC;
  signal \eol_reg_209_reg_n_0_[0]\ : STD_LOGIC;
  signal \exitcond3_fu_322_p2__14\ : STD_LOGIC;
  signal exitcond_fu_334_p2 : STD_LOGIC;
  signal exitcond_reg_4160 : STD_LOGIC;
  signal \exitcond_reg_416[0]_i_1_n_0\ : STD_LOGIC;
  signal \exitcond_reg_416_reg_n_0_[0]\ : STD_LOGIC;
  signal i_V_fu_328_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal i_V_reg_411 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \i_V_reg_411[10]_i_2_n_0\ : STD_LOGIC;
  signal \^image_in_tready\ : STD_LOGIC;
  signal j_V_fu_340_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal sof_1_fu_124 : STD_LOGIC;
  signal sof_1_fu_1240 : STD_LOGIC;
  signal \sof_1_fu_124[0]_i_1_n_0\ : STD_LOGIC;
  signal \^start_once_reg\ : STD_LOGIC;
  signal \start_once_reg_i_1__0_n_0\ : STD_LOGIC;
  signal t_V_2_reg_198 : STD_LOGIC;
  signal \t_V_2_reg_198[10]_i_5_n_0\ : STD_LOGIC;
  signal \t_V_2_reg_198[10]_i_6_n_0\ : STD_LOGIC;
  signal \t_V_2_reg_198[10]_i_7_n_0\ : STD_LOGIC;
  signal \t_V_2_reg_198_reg__0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal t_V_reg_187 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal tmp_data_V_reg_387 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal tmp_last_V_reg_395 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of AXI_video_strm_V_data_V_0_sel_rd_i_1 : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of AXI_video_strm_V_data_V_0_sel_wr_i_1 : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_data_V_0_state[1]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_dest_V_0_state[1]_i_4\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of AXI_video_strm_V_last_V_0_sel_wr_i_1 : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_last_V_0_state[1]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of AXI_video_strm_V_user_V_0_sel_rd_i_1 : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of AXI_video_strm_V_user_V_0_sel_wr_i_1 : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_user_V_0_state[1]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \SRL_SIG[0][0]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \SRL_SIG[0][0]_i_1__0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \SRL_SIG[0][1]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \SRL_SIG[0][1]_i_1__0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \SRL_SIG[0][2]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \SRL_SIG[0][2]_i_1__0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \SRL_SIG[0][2]_i_1__1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \SRL_SIG[0][3]_i_1__0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \SRL_SIG[0][3]_i_1__1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \SRL_SIG[0][4]_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \SRL_SIG[0][4]_i_1__1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \SRL_SIG[0][5]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \SRL_SIG[0][5]_i_1__0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \SRL_SIG[0][5]_i_1__1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \SRL_SIG[0][6]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \SRL_SIG[0][6]_i_1__0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \SRL_SIG[0][6]_i_1__1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_2__2\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_2__4\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \ap_CS_fsm[1]_i_2\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \ap_CS_fsm[5]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \ap_CS_fsm[7]_i_1\ : label is "soft_lutpair25";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[4]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[5]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[6]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[7]\ : label is "none";
  attribute SOFT_HLUTNM of ap_ready_INST_0_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of ap_ready_INST_0_i_2 : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[10]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[11]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[12]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[13]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[14]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[15]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[16]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[17]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[18]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[19]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[1]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[20]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[21]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[22]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[23]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[2]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[3]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[4]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[5]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[6]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[7]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[8]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_177[9]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_291[0]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_291[15]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_291[19]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_291[1]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_291[20]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_last_V1_reg_167[0]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \brmerge_reg_425[0]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \eol_1_reg_221[0]_i_3\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \eol_2_reg_268[0]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \exitcond_reg_416[0]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \i_V_reg_411[0]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \i_V_reg_411[1]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \i_V_reg_411[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \i_V_reg_411[3]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \i_V_reg_411[4]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \i_V_reg_411[6]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \i_V_reg_411[8]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \i_V_reg_411[9]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of internal_empty_n_i_2 : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \internal_empty_n_i_2__1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \start_once_reg_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \t_V_2_reg_198[0]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \t_V_2_reg_198[10]_i_4\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \t_V_2_reg_198[10]_i_6\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \t_V_2_reg_198[1]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \t_V_2_reg_198[2]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \t_V_2_reg_198[3]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \t_V_2_reg_198[4]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \t_V_2_reg_198[6]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \t_V_2_reg_198[8]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \t_V_2_reg_198[9]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[0]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[10]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[11]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[12]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[13]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[14]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[15]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[16]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[17]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[18]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[19]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[1]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[20]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[21]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[22]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[23]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[3]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[4]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[5]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[6]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[7]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[8]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_387[9]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \tmp_last_V_reg_395[0]_i_2\ : label is "soft_lutpair3";
begin
  AXIvideo2Mat_U0_img_data_stream_2_V_write <= \^axivideo2mat_u0_img_data_stream_2_v_write\;
  Q(0) <= \^q\(0);
  ap_rst <= \^ap_rst\;
  image_in_TREADY <= \^image_in_tready\;
  start_once_reg <= \^start_once_reg\;
\AXI_video_strm_V_data_V_0_payload_A[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_data_V_0_ack_in,
      I2 => AXI_video_strm_V_data_V_0_sel_wr,
      O => AXI_video_strm_V_data_V_0_load_A
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(0),
      Q => AXI_video_strm_V_data_V_0_payload_A(0),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(10),
      Q => AXI_video_strm_V_data_V_0_payload_A(10),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(11),
      Q => AXI_video_strm_V_data_V_0_payload_A(11),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(12),
      Q => AXI_video_strm_V_data_V_0_payload_A(12),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(13),
      Q => AXI_video_strm_V_data_V_0_payload_A(13),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(14),
      Q => AXI_video_strm_V_data_V_0_payload_A(14),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(15),
      Q => AXI_video_strm_V_data_V_0_payload_A(15),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(16),
      Q => AXI_video_strm_V_data_V_0_payload_A(16),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(17),
      Q => AXI_video_strm_V_data_V_0_payload_A(17),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(18),
      Q => AXI_video_strm_V_data_V_0_payload_A(18),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(19),
      Q => AXI_video_strm_V_data_V_0_payload_A(19),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(1),
      Q => AXI_video_strm_V_data_V_0_payload_A(1),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(20),
      Q => AXI_video_strm_V_data_V_0_payload_A(20),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(21),
      Q => AXI_video_strm_V_data_V_0_payload_A(21),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(22),
      Q => AXI_video_strm_V_data_V_0_payload_A(22),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(23),
      Q => AXI_video_strm_V_data_V_0_payload_A(23),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(2),
      Q => AXI_video_strm_V_data_V_0_payload_A(2),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(3),
      Q => AXI_video_strm_V_data_V_0_payload_A(3),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(4),
      Q => AXI_video_strm_V_data_V_0_payload_A(4),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(5),
      Q => AXI_video_strm_V_data_V_0_payload_A(5),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(6),
      Q => AXI_video_strm_V_data_V_0_payload_A(6),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(7),
      Q => AXI_video_strm_V_data_V_0_payload_A(7),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(8),
      Q => AXI_video_strm_V_data_V_0_payload_A(8),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => image_in_TDATA(9),
      Q => AXI_video_strm_V_data_V_0_payload_A(9),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_sel_wr,
      I1 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_0_ack_in,
      O => AXI_video_strm_V_data_V_0_load_B
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(0),
      Q => AXI_video_strm_V_data_V_0_payload_B(0),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(10),
      Q => AXI_video_strm_V_data_V_0_payload_B(10),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(11),
      Q => AXI_video_strm_V_data_V_0_payload_B(11),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(12),
      Q => AXI_video_strm_V_data_V_0_payload_B(12),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(13),
      Q => AXI_video_strm_V_data_V_0_payload_B(13),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(14),
      Q => AXI_video_strm_V_data_V_0_payload_B(14),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(15),
      Q => AXI_video_strm_V_data_V_0_payload_B(15),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(16),
      Q => AXI_video_strm_V_data_V_0_payload_B(16),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(17),
      Q => AXI_video_strm_V_data_V_0_payload_B(17),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(18),
      Q => AXI_video_strm_V_data_V_0_payload_B(18),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(19),
      Q => AXI_video_strm_V_data_V_0_payload_B(19),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(1),
      Q => AXI_video_strm_V_data_V_0_payload_B(1),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(20),
      Q => AXI_video_strm_V_data_V_0_payload_B(20),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(21),
      Q => AXI_video_strm_V_data_V_0_payload_B(21),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(22),
      Q => AXI_video_strm_V_data_V_0_payload_B(22),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(23),
      Q => AXI_video_strm_V_data_V_0_payload_B(23),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(2),
      Q => AXI_video_strm_V_data_V_0_payload_B(2),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(3),
      Q => AXI_video_strm_V_data_V_0_payload_B(3),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(4),
      Q => AXI_video_strm_V_data_V_0_payload_B(4),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(5),
      Q => AXI_video_strm_V_data_V_0_payload_B(5),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(6),
      Q => AXI_video_strm_V_data_V_0_payload_B(6),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(7),
      Q => AXI_video_strm_V_data_V_0_payload_B(7),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(8),
      Q => AXI_video_strm_V_data_V_0_payload_B(8),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => image_in_TDATA(9),
      Q => AXI_video_strm_V_data_V_0_payload_B(9),
      R => '0'
    );
AXI_video_strm_V_data_V_0_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_data_V_0_ack_out,
      I2 => AXI_video_strm_V_data_V_0_sel,
      O => AXI_video_strm_V_data_V_0_sel_rd_i_1_n_0
    );
AXI_video_strm_V_data_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_0_sel_rd_i_1_n_0,
      Q => AXI_video_strm_V_data_V_0_sel,
      R => \^ap_rst\
    );
AXI_video_strm_V_data_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => image_in_TVALID,
      I1 => AXI_video_strm_V_data_V_0_ack_in,
      I2 => AXI_video_strm_V_data_V_0_sel_wr,
      O => AXI_video_strm_V_data_V_0_sel_wr_i_1_n_0
    );
AXI_video_strm_V_data_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_0_sel_wr_i_1_n_0,
      Q => AXI_video_strm_V_data_V_0_sel_wr,
      R => \^ap_rst\
    );
\AXI_video_strm_V_data_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A820A0"
    )
        port map (
      I0 => ap_rst_n,
      I1 => AXI_video_strm_V_data_V_0_ack_in,
      I2 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I3 => AXI_video_strm_V_data_V_0_ack_out,
      I4 => image_in_TVALID,
      O => \AXI_video_strm_V_data_V_0_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_data_V_0_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F3FB"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_ack_in,
      I1 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_0_ack_out,
      I3 => image_in_TVALID,
      O => AXI_video_strm_V_data_V_0_state(1)
    );
\AXI_video_strm_V_data_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_data_V_0_state[0]_i_1_n_0\,
      Q => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_0_state(1),
      Q => AXI_video_strm_V_data_V_0_ack_in,
      R => \^ap_rst\
    );
\AXI_video_strm_V_dest_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A2AAA000"
    )
        port map (
      I0 => ap_rst_n,
      I1 => AXI_video_strm_V_data_V_0_ack_out,
      I2 => image_in_TVALID,
      I3 => \^image_in_tready\,
      I4 => \AXI_video_strm_V_dest_V_0_state_reg_n_0_[0]\,
      O => \AXI_video_strm_V_dest_V_0_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_dest_V_0_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ap_rst_n,
      O => \^ap_rst\
    );
\AXI_video_strm_V_dest_V_0_state[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BAFF"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_ack_out,
      I1 => image_in_TVALID,
      I2 => \^image_in_tready\,
      I3 => \AXI_video_strm_V_dest_V_0_state_reg_n_0_[0]\,
      O => AXI_video_strm_V_dest_V_0_state(1)
    );
\AXI_video_strm_V_dest_V_0_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEEEEFEEEEE"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_sel2,
      I1 => \AXI_video_strm_V_data_V_0_sel3__0\,
      I2 => exitcond_reg_4160,
      I3 => \exitcond_reg_416_reg_n_0_[0]\,
      I4 => ap_enable_reg_pp1_iter1_reg_n_0,
      I5 => brmerge_reg_425,
      O => AXI_video_strm_V_data_V_0_ack_out
    );
\AXI_video_strm_V_dest_V_0_state[1]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => ap_CS_fsm_pp2_stage0,
      I1 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I2 => ap_enable_reg_pp2_iter1_reg_n_0,
      I3 => \eol_2_reg_268_reg_n_0_[0]\,
      O => \AXI_video_strm_V_data_V_0_sel3__0\
    );
\AXI_video_strm_V_dest_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_dest_V_0_state[0]_i_1_n_0\,
      Q => \AXI_video_strm_V_dest_V_0_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_dest_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_dest_V_0_state(1),
      Q => \^image_in_tready\,
      R => \^ap_rst\
    );
\AXI_video_strm_V_last_V_0_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => image_in_TLAST(0),
      I1 => \AXI_video_strm_V_last_V_0_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_last_V_0_ack_in,
      I3 => AXI_video_strm_V_last_V_0_sel_wr,
      I4 => AXI_video_strm_V_last_V_0_payload_A,
      O => \AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_0\
    );
\AXI_video_strm_V_last_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_0\,
      Q => AXI_video_strm_V_last_V_0_payload_A,
      R => '0'
    );
\AXI_video_strm_V_last_V_0_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBFB8808"
    )
        port map (
      I0 => image_in_TLAST(0),
      I1 => AXI_video_strm_V_last_V_0_sel_wr,
      I2 => \AXI_video_strm_V_last_V_0_state_reg_n_0_[0]\,
      I3 => AXI_video_strm_V_last_V_0_ack_in,
      I4 => AXI_video_strm_V_last_V_0_payload_B,
      O => \AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_0\
    );
\AXI_video_strm_V_last_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_0\,
      Q => AXI_video_strm_V_last_V_0_payload_B,
      R => '0'
    );
AXI_video_strm_V_last_V_0_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_ack_out,
      I1 => \AXI_video_strm_V_last_V_0_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_last_V_0_sel,
      O => AXI_video_strm_V_last_V_0_sel_rd_i_1_n_0
    );
AXI_video_strm_V_last_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_0_sel_rd_i_1_n_0,
      Q => AXI_video_strm_V_last_V_0_sel,
      R => \^ap_rst\
    );
AXI_video_strm_V_last_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => image_in_TVALID,
      I1 => AXI_video_strm_V_last_V_0_ack_in,
      I2 => AXI_video_strm_V_last_V_0_sel_wr,
      O => AXI_video_strm_V_last_V_0_sel_wr_i_1_n_0
    );
AXI_video_strm_V_last_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_0_sel_wr_i_1_n_0,
      Q => AXI_video_strm_V_last_V_0_sel_wr,
      R => \^ap_rst\
    );
\AXI_video_strm_V_last_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A2AAA000"
    )
        port map (
      I0 => ap_rst_n,
      I1 => AXI_video_strm_V_data_V_0_ack_out,
      I2 => image_in_TVALID,
      I3 => AXI_video_strm_V_last_V_0_ack_in,
      I4 => \AXI_video_strm_V_last_V_0_state_reg_n_0_[0]\,
      O => \AXI_video_strm_V_last_V_0_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_last_V_0_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BAFF"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_ack_out,
      I1 => image_in_TVALID,
      I2 => AXI_video_strm_V_last_V_0_ack_in,
      I3 => \AXI_video_strm_V_last_V_0_state_reg_n_0_[0]\,
      O => AXI_video_strm_V_last_V_0_state(1)
    );
\AXI_video_strm_V_last_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_0_state[0]_i_1_n_0\,
      Q => \AXI_video_strm_V_last_V_0_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_last_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_0_state(1),
      Q => AXI_video_strm_V_last_V_0_ack_in,
      R => \^ap_rst\
    );
\AXI_video_strm_V_user_V_0_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => image_in_TUSER(0),
      I1 => \AXI_video_strm_V_user_V_0_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_user_V_0_ack_in,
      I3 => AXI_video_strm_V_user_V_0_sel_wr,
      I4 => AXI_video_strm_V_user_V_0_payload_A,
      O => \AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_0\
    );
\AXI_video_strm_V_user_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_0\,
      Q => AXI_video_strm_V_user_V_0_payload_A,
      R => '0'
    );
\AXI_video_strm_V_user_V_0_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBFB8808"
    )
        port map (
      I0 => image_in_TUSER(0),
      I1 => AXI_video_strm_V_user_V_0_sel_wr,
      I2 => \AXI_video_strm_V_user_V_0_state_reg_n_0_[0]\,
      I3 => AXI_video_strm_V_user_V_0_ack_in,
      I4 => AXI_video_strm_V_user_V_0_payload_B,
      O => \AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_0\
    );
\AXI_video_strm_V_user_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_0\,
      Q => AXI_video_strm_V_user_V_0_payload_B,
      R => '0'
    );
AXI_video_strm_V_user_V_0_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \AXI_video_strm_V_user_V_0_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_data_V_0_ack_out,
      I2 => AXI_video_strm_V_user_V_0_sel,
      O => AXI_video_strm_V_user_V_0_sel_rd_i_1_n_0
    );
AXI_video_strm_V_user_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_0_sel_rd_i_1_n_0,
      Q => AXI_video_strm_V_user_V_0_sel,
      R => \^ap_rst\
    );
AXI_video_strm_V_user_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => image_in_TVALID,
      I1 => AXI_video_strm_V_user_V_0_ack_in,
      I2 => AXI_video_strm_V_user_V_0_sel_wr,
      O => AXI_video_strm_V_user_V_0_sel_wr_i_1_n_0
    );
AXI_video_strm_V_user_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_0_sel_wr_i_1_n_0,
      Q => AXI_video_strm_V_user_V_0_sel_wr,
      R => \^ap_rst\
    );
\AXI_video_strm_V_user_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A820A0"
    )
        port map (
      I0 => ap_rst_n,
      I1 => AXI_video_strm_V_user_V_0_ack_in,
      I2 => \AXI_video_strm_V_user_V_0_state_reg_n_0_[0]\,
      I3 => AXI_video_strm_V_data_V_0_ack_out,
      I4 => image_in_TVALID,
      O => \AXI_video_strm_V_user_V_0_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_user_V_0_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F3FB"
    )
        port map (
      I0 => AXI_video_strm_V_user_V_0_ack_in,
      I1 => \AXI_video_strm_V_user_V_0_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_0_ack_out,
      I3 => image_in_TVALID,
      O => AXI_video_strm_V_user_V_0_state(1)
    );
\AXI_video_strm_V_user_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_0_state[0]_i_1_n_0\,
      Q => \AXI_video_strm_V_user_V_0_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_user_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_0_state(1),
      Q => AXI_video_strm_V_user_V_0_ack_in,
      R => \^ap_rst\
    );
\SRL_SIG[0][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(16),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(16),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(16),
      O => D(0)
    );
\SRL_SIG[0][0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(8),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(8),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(8),
      O => \SRL_SIG_reg[0][7]\(0)
    );
\SRL_SIG[0][0]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(0),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(0),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(0),
      O => \SRL_SIG_reg[0][7]_0\(0)
    );
\SRL_SIG[0][1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(17),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(17),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(17),
      O => D(1)
    );
\SRL_SIG[0][1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(9),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(9),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(9),
      O => \SRL_SIG_reg[0][7]\(1)
    );
\SRL_SIG[0][1]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(1),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(1),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(1),
      O => \SRL_SIG_reg[0][7]_0\(1)
    );
\SRL_SIG[0][2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(18),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(18),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(18),
      O => D(2)
    );
\SRL_SIG[0][2]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(10),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(10),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(10),
      O => \SRL_SIG_reg[0][7]\(2)
    );
\SRL_SIG[0][2]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(2),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(2),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(2),
      O => \SRL_SIG_reg[0][7]_0\(2)
    );
\SRL_SIG[0][3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(19),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(19),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(19),
      O => D(3)
    );
\SRL_SIG[0][3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(11),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(11),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(11),
      O => \SRL_SIG_reg[0][7]\(3)
    );
\SRL_SIG[0][3]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(3),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(3),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(3),
      O => \SRL_SIG_reg[0][7]_0\(3)
    );
\SRL_SIG[0][4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(20),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(20),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(20),
      O => D(4)
    );
\SRL_SIG[0][4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(12),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(12),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(12),
      O => \SRL_SIG_reg[0][7]\(4)
    );
\SRL_SIG[0][4]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(4),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(4),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(4),
      O => \SRL_SIG_reg[0][7]_0\(4)
    );
\SRL_SIG[0][5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(21),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(21),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(21),
      O => D(5)
    );
\SRL_SIG[0][5]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(13),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(13),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(13),
      O => \SRL_SIG_reg[0][7]\(5)
    );
\SRL_SIG[0][5]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(5),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(5),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(5),
      O => \SRL_SIG_reg[0][7]_0\(5)
    );
\SRL_SIG[0][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(22),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(22),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(22),
      O => D(6)
    );
\SRL_SIG[0][6]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(14),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(14),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(14),
      O => \SRL_SIG_reg[0][7]\(6)
    );
\SRL_SIG[0][6]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(6),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(6),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(6),
      O => \SRL_SIG_reg[0][7]_0\(6)
    );
\SRL_SIG[0][7]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(23),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(23),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(23),
      O => D(7)
    );
\SRL_SIG[0][7]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(15),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(15),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(15),
      O => \SRL_SIG_reg[0][7]\(7)
    );
\SRL_SIG[0][7]_i_2__4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(7),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_payload_B(7),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(7),
      O => \SRL_SIG_reg[0][7]_0\(7)
    );
\ap_CS_fsm[0]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888FFFFF88888888"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \exitcond3_fu_322_p2__14\,
      I2 => start_for_Loop_loop_height_pro_U0_full_n,
      I3 => \^start_once_reg\,
      I4 => ap_start,
      I5 => \^q\(0),
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8888888888888"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => \ap_CS_fsm[1]_i_2_n_0\,
      I2 => start_for_Loop_loop_height_pro_U0_full_n,
      I3 => \^start_once_reg\,
      I4 => ap_start,
      I5 => \^q\(0),
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1DFFFFFF"
    )
        port map (
      I0 => AXI_video_strm_V_user_V_0_payload_A,
      I1 => AXI_video_strm_V_user_V_0_sel,
      I2 => AXI_video_strm_V_user_V_0_payload_B,
      I3 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I4 => ap_CS_fsm_state2,
      O => \ap_CS_fsm[1]_i_2_n_0\
    );
\ap_CS_fsm[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80888000"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_user_V_0_payload_B,
      I3 => AXI_video_strm_V_user_V_0_sel,
      I4 => AXI_video_strm_V_user_V_0_payload_A,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm[3]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => ap_CS_fsm_state3,
      I1 => ap_CS_fsm_state10,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F2FFFFFF22222222"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \exitcond3_fu_322_p2__14\,
      I2 => ap_enable_reg_pp1_iter0,
      I3 => ap_enable_reg_pp1_iter1_reg_n_0,
      I4 => exitcond_reg_4160,
      I5 => ap_CS_fsm_pp1_stage0,
      O => ap_NS_fsm(4)
    );
\ap_CS_fsm[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => exitcond_reg_4160,
      I2 => ap_enable_reg_pp1_iter1_reg_n_0,
      I3 => ap_enable_reg_pp1_iter0,
      O => ap_NS_fsm(5)
    );
\ap_CS_fsm[5]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => ap_enable_reg_pp1_iter1_reg_n_0,
      I2 => ap_block_pp1_stage0_110011,
      O => exitcond_reg_4160
    );
\ap_CS_fsm[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAFBF0000"
    )
        port map (
      I0 => ap_enable_reg_pp2_iter0,
      I1 => \eol_2_reg_268_reg_n_0_[0]\,
      I2 => ap_enable_reg_pp2_iter1_reg_n_0,
      I3 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I4 => ap_CS_fsm_pp2_stage0,
      I5 => ap_CS_fsm_state7,
      O => ap_NS_fsm(6)
    );
\ap_CS_fsm[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000A080"
    )
        port map (
      I0 => ap_CS_fsm_pp2_stage0,
      I1 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I2 => ap_enable_reg_pp2_iter1_reg_n_0,
      I3 => \eol_2_reg_268_reg_n_0_[0]\,
      I4 => ap_enable_reg_pp2_iter0,
      O => ap_NS_fsm(7)
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \^q\(0),
      S => \^ap_rst\
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => \^ap_rst\
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_CS_fsm_state3,
      R => \^ap_rst\
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => ap_CS_fsm_state4,
      R => \^ap_rst\
    );
\ap_CS_fsm_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(4),
      Q => ap_CS_fsm_pp1_stage0,
      R => \^ap_rst\
    );
\ap_CS_fsm_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(5),
      Q => ap_CS_fsm_state7,
      R => \^ap_rst\
    );
\ap_CS_fsm_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(6),
      Q => ap_CS_fsm_pp2_stage0,
      R => \^ap_rst\
    );
\ap_CS_fsm_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(7),
      Q => ap_CS_fsm_state10,
      R => \^ap_rst\
    );
ap_enable_reg_pp1_iter0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000F200F200F200"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \exitcond3_fu_322_p2__14\,
      I2 => ap_enable_reg_pp1_iter0,
      I3 => ap_rst_n,
      I4 => exitcond_reg_4160,
      I5 => exitcond_fu_334_p2,
      O => ap_enable_reg_pp1_iter0_i_1_n_0
    );
ap_enable_reg_pp1_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp1_iter0_i_1_n_0,
      Q => ap_enable_reg_pp1_iter0,
      R => '0'
    );
ap_enable_reg_pp1_iter1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DD00F000F000F000"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \exitcond3_fu_322_p2__14\,
      I2 => ap_enable_reg_pp1_iter0,
      I3 => ap_rst_n,
      I4 => ap_enable_reg_pp1_iter1_reg_n_0,
      I5 => ap_block_pp1_stage0_110011,
      O => ap_enable_reg_pp1_iter1_i_1_n_0
    );
ap_enable_reg_pp1_iter1_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000057FF0000FFFF"
    )
        port map (
      I0 => input_data_data_stre_2_full_n,
      I1 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I2 => brmerge_reg_425,
      I3 => input_data_data_stre_full_n,
      I4 => \exitcond_reg_416_reg_n_0_[0]\,
      I5 => input_data_data_stre_1_full_n,
      O => ap_block_pp1_stage0_110011
    );
ap_enable_reg_pp1_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp1_iter1_i_1_n_0,
      Q => ap_enable_reg_pp1_iter1_reg_n_0,
      R => '0'
    );
ap_enable_reg_pp2_iter0_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0E"
    )
        port map (
      I0 => ap_enable_reg_pp2_iter0,
      I1 => ap_CS_fsm_state7,
      I2 => ap_enable_reg_pp2_iter0_i_2_n_0,
      O => ap_enable_reg_pp2_iter0_i_1_n_0
    );
ap_enable_reg_pp2_iter0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDD555DDDD5555"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_CS_fsm_pp2_stage0,
      I2 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I3 => ap_enable_reg_pp2_iter1_reg_n_0,
      I4 => \eol_2_reg_268_reg_n_0_[0]\,
      I5 => AXI_video_strm_V_last_V_0_data_out,
      O => ap_enable_reg_pp2_iter0_i_2_n_0
    );
ap_enable_reg_pp2_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp2_iter0_i_1_n_0,
      Q => ap_enable_reg_pp2_iter0,
      R => '0'
    );
ap_enable_reg_pp2_iter1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88888888880C8888"
    )
        port map (
      I0 => ap_enable_reg_pp2_iter0,
      I1 => ap_rst_n,
      I2 => ap_CS_fsm_state7,
      I3 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I4 => ap_enable_reg_pp2_iter1_reg_n_0,
      I5 => \eol_2_reg_268_reg_n_0_[0]\,
      O => ap_enable_reg_pp2_iter1_i_1_n_0
    );
ap_enable_reg_pp2_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp2_iter1_i_1_n_0,
      Q => ap_enable_reg_pp2_iter1_reg_n_0,
      R => '0'
    );
ap_ready_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \exitcond3_fu_322_p2__14\,
      O => ap_ready
    );
ap_ready_INST_0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => ap_ready_INST_0_i_2_n_0,
      I1 => ap_ready_INST_0_i_3_n_0,
      I2 => t_V_reg_187(0),
      I3 => t_V_reg_187(1),
      I4 => t_V_reg_187(2),
      O => \exitcond3_fu_322_p2__14\
    );
ap_ready_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => t_V_reg_187(6),
      I1 => t_V_reg_187(5),
      I2 => t_V_reg_187(4),
      I3 => t_V_reg_187(3),
      O => ap_ready_INST_0_i_2_n_0
    );
ap_ready_INST_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => t_V_reg_187(9),
      I1 => t_V_reg_187(10),
      I2 => t_V_reg_187(8),
      I3 => t_V_reg_187(7),
      O => ap_ready_INST_0_i_3_n_0
    );
\axi_data_V1_reg_177[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(0),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(0),
      O => \axi_data_V1_reg_177[0]_i_1_n_0\
    );
\axi_data_V1_reg_177[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(10),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(10),
      O => \axi_data_V1_reg_177[10]_i_1_n_0\
    );
\axi_data_V1_reg_177[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(11),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(11),
      O => \axi_data_V1_reg_177[11]_i_1_n_0\
    );
\axi_data_V1_reg_177[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(12),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(12),
      O => \axi_data_V1_reg_177[12]_i_1_n_0\
    );
\axi_data_V1_reg_177[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(13),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(13),
      O => \axi_data_V1_reg_177[13]_i_1_n_0\
    );
\axi_data_V1_reg_177[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(14),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(14),
      O => \axi_data_V1_reg_177[14]_i_1_n_0\
    );
\axi_data_V1_reg_177[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(15),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(15),
      O => \axi_data_V1_reg_177[15]_i_1_n_0\
    );
\axi_data_V1_reg_177[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(16),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(16),
      O => \axi_data_V1_reg_177[16]_i_1_n_0\
    );
\axi_data_V1_reg_177[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(17),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(17),
      O => \axi_data_V1_reg_177[17]_i_1_n_0\
    );
\axi_data_V1_reg_177[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(18),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(18),
      O => \axi_data_V1_reg_177[18]_i_1_n_0\
    );
\axi_data_V1_reg_177[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(19),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(19),
      O => \axi_data_V1_reg_177[19]_i_1_n_0\
    );
\axi_data_V1_reg_177[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(1),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(1),
      O => \axi_data_V1_reg_177[1]_i_1_n_0\
    );
\axi_data_V1_reg_177[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(20),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(20),
      O => \axi_data_V1_reg_177[20]_i_1_n_0\
    );
\axi_data_V1_reg_177[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(21),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(21),
      O => \axi_data_V1_reg_177[21]_i_1_n_0\
    );
\axi_data_V1_reg_177[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(22),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(22),
      O => \axi_data_V1_reg_177[22]_i_1_n_0\
    );
\axi_data_V1_reg_177[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(23),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(23),
      O => \axi_data_V1_reg_177[23]_i_1_n_0\
    );
\axi_data_V1_reg_177[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(2),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(2),
      O => \axi_data_V1_reg_177[2]_i_1_n_0\
    );
\axi_data_V1_reg_177[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(3),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(3),
      O => \axi_data_V1_reg_177[3]_i_1_n_0\
    );
\axi_data_V1_reg_177[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(4),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(4),
      O => \axi_data_V1_reg_177[4]_i_1_n_0\
    );
\axi_data_V1_reg_177[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(5),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(5),
      O => \axi_data_V1_reg_177[5]_i_1_n_0\
    );
\axi_data_V1_reg_177[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(6),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(6),
      O => \axi_data_V1_reg_177[6]_i_1_n_0\
    );
\axi_data_V1_reg_177[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(7),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(7),
      O => \axi_data_V1_reg_177[7]_i_1_n_0\
    );
\axi_data_V1_reg_177[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(8),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(8),
      O => \axi_data_V1_reg_177[8]_i_1_n_0\
    );
\axi_data_V1_reg_177[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_387(9),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_291(9),
      O => \axi_data_V1_reg_177[9]_i_1_n_0\
    );
\axi_data_V1_reg_177_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[0]_i_1_n_0\,
      Q => axi_data_V1_reg_177(0),
      R => '0'
    );
\axi_data_V1_reg_177_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[10]_i_1_n_0\,
      Q => axi_data_V1_reg_177(10),
      R => '0'
    );
\axi_data_V1_reg_177_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[11]_i_1_n_0\,
      Q => axi_data_V1_reg_177(11),
      R => '0'
    );
\axi_data_V1_reg_177_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[12]_i_1_n_0\,
      Q => axi_data_V1_reg_177(12),
      R => '0'
    );
\axi_data_V1_reg_177_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[13]_i_1_n_0\,
      Q => axi_data_V1_reg_177(13),
      R => '0'
    );
\axi_data_V1_reg_177_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[14]_i_1_n_0\,
      Q => axi_data_V1_reg_177(14),
      R => '0'
    );
\axi_data_V1_reg_177_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[15]_i_1_n_0\,
      Q => axi_data_V1_reg_177(15),
      R => '0'
    );
\axi_data_V1_reg_177_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[16]_i_1_n_0\,
      Q => axi_data_V1_reg_177(16),
      R => '0'
    );
\axi_data_V1_reg_177_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[17]_i_1_n_0\,
      Q => axi_data_V1_reg_177(17),
      R => '0'
    );
\axi_data_V1_reg_177_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[18]_i_1_n_0\,
      Q => axi_data_V1_reg_177(18),
      R => '0'
    );
\axi_data_V1_reg_177_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[19]_i_1_n_0\,
      Q => axi_data_V1_reg_177(19),
      R => '0'
    );
\axi_data_V1_reg_177_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[1]_i_1_n_0\,
      Q => axi_data_V1_reg_177(1),
      R => '0'
    );
\axi_data_V1_reg_177_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[20]_i_1_n_0\,
      Q => axi_data_V1_reg_177(20),
      R => '0'
    );
\axi_data_V1_reg_177_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[21]_i_1_n_0\,
      Q => axi_data_V1_reg_177(21),
      R => '0'
    );
\axi_data_V1_reg_177_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[22]_i_1_n_0\,
      Q => axi_data_V1_reg_177(22),
      R => '0'
    );
\axi_data_V1_reg_177_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[23]_i_1_n_0\,
      Q => axi_data_V1_reg_177(23),
      R => '0'
    );
\axi_data_V1_reg_177_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[2]_i_1_n_0\,
      Q => axi_data_V1_reg_177(2),
      R => '0'
    );
\axi_data_V1_reg_177_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[3]_i_1_n_0\,
      Q => axi_data_V1_reg_177(3),
      R => '0'
    );
\axi_data_V1_reg_177_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[4]_i_1_n_0\,
      Q => axi_data_V1_reg_177(4),
      R => '0'
    );
\axi_data_V1_reg_177_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[5]_i_1_n_0\,
      Q => axi_data_V1_reg_177(5),
      R => '0'
    );
\axi_data_V1_reg_177_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[6]_i_1_n_0\,
      Q => axi_data_V1_reg_177(6),
      R => '0'
    );
\axi_data_V1_reg_177_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[7]_i_1_n_0\,
      Q => axi_data_V1_reg_177(7),
      R => '0'
    );
\axi_data_V1_reg_177_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[8]_i_1_n_0\,
      Q => axi_data_V1_reg_177(8),
      R => '0'
    );
\axi_data_V1_reg_177_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_177[9]_i_1_n_0\,
      Q => axi_data_V1_reg_177(9),
      R => '0'
    );
\axi_data_V_1_reg_232[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(0),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(0),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(0),
      O => \axi_data_V_1_reg_232[0]_i_1_n_0\
    );
\axi_data_V_1_reg_232[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(10),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(10),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(10),
      O => \axi_data_V_1_reg_232[10]_i_1_n_0\
    );
\axi_data_V_1_reg_232[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(11),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(11),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(11),
      O => \axi_data_V_1_reg_232[11]_i_1_n_0\
    );
\axi_data_V_1_reg_232[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(12),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(12),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(12),
      O => \axi_data_V_1_reg_232[12]_i_1_n_0\
    );
\axi_data_V_1_reg_232[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(13),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(13),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(13),
      O => \axi_data_V_1_reg_232[13]_i_1_n_0\
    );
\axi_data_V_1_reg_232[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(14),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(14),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(14),
      O => \axi_data_V_1_reg_232[14]_i_1_n_0\
    );
\axi_data_V_1_reg_232[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(15),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(15),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(15),
      O => \axi_data_V_1_reg_232[15]_i_1_n_0\
    );
\axi_data_V_1_reg_232[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(16),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(16),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(16),
      O => \axi_data_V_1_reg_232[16]_i_1_n_0\
    );
\axi_data_V_1_reg_232[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(17),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(17),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(17),
      O => \axi_data_V_1_reg_232[17]_i_1_n_0\
    );
\axi_data_V_1_reg_232[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(18),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(18),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(18),
      O => \axi_data_V_1_reg_232[18]_i_1_n_0\
    );
\axi_data_V_1_reg_232[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(19),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(19),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(19),
      O => \axi_data_V_1_reg_232[19]_i_1_n_0\
    );
\axi_data_V_1_reg_232[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(1),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(1),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(1),
      O => \axi_data_V_1_reg_232[1]_i_1_n_0\
    );
\axi_data_V_1_reg_232[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(20),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(20),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(20),
      O => \axi_data_V_1_reg_232[20]_i_1_n_0\
    );
\axi_data_V_1_reg_232[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(21),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(21),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(21),
      O => \axi_data_V_1_reg_232[21]_i_1_n_0\
    );
\axi_data_V_1_reg_232[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(22),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(22),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(22),
      O => \axi_data_V_1_reg_232[22]_i_1_n_0\
    );
\axi_data_V_1_reg_232[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(23),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(23),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(23),
      O => \axi_data_V_1_reg_232[23]_i_1_n_0\
    );
\axi_data_V_1_reg_232[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(2),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(2),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(2),
      O => \axi_data_V_1_reg_232[2]_i_1_n_0\
    );
\axi_data_V_1_reg_232[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(3),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(3),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(3),
      O => \axi_data_V_1_reg_232[3]_i_1_n_0\
    );
\axi_data_V_1_reg_232[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(4),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(4),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(4),
      O => \axi_data_V_1_reg_232[4]_i_1_n_0\
    );
\axi_data_V_1_reg_232[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(5),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(5),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(5),
      O => \axi_data_V_1_reg_232[5]_i_1_n_0\
    );
\axi_data_V_1_reg_232[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(6),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(6),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(6),
      O => \axi_data_V_1_reg_232[6]_i_1_n_0\
    );
\axi_data_V_1_reg_232[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(7),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(7),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(7),
      O => \axi_data_V_1_reg_232[7]_i_1_n_0\
    );
\axi_data_V_1_reg_232[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(8),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(8),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(8),
      O => \axi_data_V_1_reg_232[8]_i_1_n_0\
    );
\axi_data_V_1_reg_232[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_232(9),
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_data_V_0_data_out(9),
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_data_V1_reg_177(9),
      O => \axi_data_V_1_reg_232[9]_i_1_n_0\
    );
\axi_data_V_1_reg_232_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[0]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(0),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[10]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(10),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[11]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(11),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[12]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(12),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[13]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(13),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[14]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(14),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[15]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(15),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[16]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(16),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[17]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(17),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[18]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(18),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[19]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(19),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[1]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(1),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[20]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(20),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[21]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(21),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[22]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(22),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[23]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(23),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[2]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(2),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[3]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(3),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[4]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(4),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[5]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(5),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[6]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(6),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[7]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(7),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[8]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(8),
      R => '0'
    );
\axi_data_V_1_reg_232_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \axi_data_V_1_reg_232[9]_i_1_n_0\,
      Q => axi_data_V_1_reg_232(9),
      R => '0'
    );
\axi_data_V_3_reg_291[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(0),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(0),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(0),
      O => \axi_data_V_3_reg_291[0]_i_1_n_0\
    );
\axi_data_V_3_reg_291[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(10),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(10),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(10),
      O => \axi_data_V_3_reg_291[10]_i_1_n_0\
    );
\axi_data_V_3_reg_291[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(11),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(11),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(11),
      O => \axi_data_V_3_reg_291[11]_i_1_n_0\
    );
\axi_data_V_3_reg_291[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(12),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(12),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(12),
      O => \axi_data_V_3_reg_291[12]_i_1_n_0\
    );
\axi_data_V_3_reg_291[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(13),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(13),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(13),
      O => \axi_data_V_3_reg_291[13]_i_1_n_0\
    );
\axi_data_V_3_reg_291[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(14),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(14),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(14),
      O => \axi_data_V_3_reg_291[14]_i_1_n_0\
    );
\axi_data_V_3_reg_291[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(15),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(15),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(15),
      O => \axi_data_V_3_reg_291[15]_i_1_n_0\
    );
\axi_data_V_3_reg_291[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(16),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(16),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(16),
      O => \axi_data_V_3_reg_291[16]_i_1_n_0\
    );
\axi_data_V_3_reg_291[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(17),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(17),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(17),
      O => \axi_data_V_3_reg_291[17]_i_1_n_0\
    );
\axi_data_V_3_reg_291[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(18),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(18),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(18),
      O => \axi_data_V_3_reg_291[18]_i_1_n_0\
    );
\axi_data_V_3_reg_291[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(19),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(19),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(19),
      O => \axi_data_V_3_reg_291[19]_i_1_n_0\
    );
\axi_data_V_3_reg_291[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(1),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(1),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(1),
      O => \axi_data_V_3_reg_291[1]_i_1_n_0\
    );
\axi_data_V_3_reg_291[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(20),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(20),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(20),
      O => \axi_data_V_3_reg_291[20]_i_1_n_0\
    );
\axi_data_V_3_reg_291[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(21),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(21),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(21),
      O => \axi_data_V_3_reg_291[21]_i_1_n_0\
    );
\axi_data_V_3_reg_291[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(22),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(22),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(22),
      O => \axi_data_V_3_reg_291[22]_i_1_n_0\
    );
\axi_data_V_3_reg_291[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(23),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(23),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(23),
      O => \axi_data_V_3_reg_291[23]_i_1_n_0\
    );
\axi_data_V_3_reg_291[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(2),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(2),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(2),
      O => \axi_data_V_3_reg_291[2]_i_1_n_0\
    );
\axi_data_V_3_reg_291[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(3),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(3),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(3),
      O => \axi_data_V_3_reg_291[3]_i_1_n_0\
    );
\axi_data_V_3_reg_291[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(4),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(4),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(4),
      O => \axi_data_V_3_reg_291[4]_i_1_n_0\
    );
\axi_data_V_3_reg_291[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(5),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(5),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(5),
      O => \axi_data_V_3_reg_291[5]_i_1_n_0\
    );
\axi_data_V_3_reg_291[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(6),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(6),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(6),
      O => \axi_data_V_3_reg_291[6]_i_1_n_0\
    );
\axi_data_V_3_reg_291[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(7),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(7),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(7),
      O => \axi_data_V_3_reg_291[7]_i_1_n_0\
    );
\axi_data_V_3_reg_291[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(8),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(8),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(8),
      O => \axi_data_V_3_reg_291[8]_i_1_n_0\
    );
\axi_data_V_3_reg_291[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_232(9),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(9),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(9),
      O => \axi_data_V_3_reg_291[9]_i_1_n_0\
    );
\axi_data_V_3_reg_291_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[0]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(0),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[10]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(10),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[11]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(11),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[12]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(12),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[13]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(13),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[14]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(14),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[15]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(15),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[16]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(16),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[17]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(17),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[18]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(18),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[19]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(19),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[1]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(1),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[20]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(20),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[21]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(21),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[22]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(22),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[23]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(23),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[2]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(2),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[3]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(3),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[4]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(4),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[5]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(5),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[6]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(6),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[7]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(7),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[8]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(8),
      R => '0'
    );
\axi_data_V_3_reg_291_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_data_V_3_reg_291[9]_i_1_n_0\,
      Q => axi_data_V_3_reg_291(9),
      R => '0'
    );
\axi_last_V1_reg_167[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_last_V_reg_395,
      I1 => ap_CS_fsm_state3,
      I2 => axi_last_V_3_reg_279,
      O => \axi_last_V1_reg_167[0]_i_1_n_0\
    );
\axi_last_V1_reg_167_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_last_V1_reg_167[0]_i_1_n_0\,
      Q => axi_last_V1_reg_167,
      R => '0'
    );
\axi_last_V_3_reg_279[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => eol_1_reg_221,
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_last_V_0_payload_B,
      I3 => AXI_video_strm_V_last_V_0_sel,
      I4 => AXI_video_strm_V_last_V_0_payload_A,
      O => \axi_last_V_3_reg_279[0]_i_1_n_0\
    );
\axi_last_V_3_reg_279_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \axi_last_V_3_reg_279[0]_i_1_n_0\,
      Q => axi_last_V_3_reg_279,
      R => '0'
    );
\brmerge_reg_425[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => brmerge_fu_349_p2,
      I1 => exitcond_reg_4160,
      I2 => exitcond_fu_334_p2,
      I3 => brmerge_reg_425,
      O => \brmerge_reg_425[0]_i_1_n_0\
    );
\brmerge_reg_425[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEEEEEEEAEEEEEE"
    )
        port map (
      I0 => sof_1_fu_124,
      I1 => \eol_reg_209_reg_n_0_[0]\,
      I2 => \exitcond_reg_416_reg_n_0_[0]\,
      I3 => ap_CS_fsm_pp1_stage0,
      I4 => ap_enable_reg_pp1_iter1_reg_n_0,
      I5 => \ap_phi_mux_axi_last_V_2_phi_fu_248_p4__0\,
      O => brmerge_fu_349_p2
    );
\brmerge_reg_425[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => eol_1_reg_221,
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_last_V_0_payload_B,
      I3 => AXI_video_strm_V_last_V_0_sel,
      I4 => AXI_video_strm_V_last_V_0_payload_A,
      O => \ap_phi_mux_axi_last_V_2_phi_fu_248_p4__0\
    );
\brmerge_reg_425_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \brmerge_reg_425[0]_i_1_n_0\,
      Q => brmerge_reg_425,
      R => '0'
    );
\eol_1_reg_221[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F2"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \exitcond3_fu_322_p2__14\,
      I2 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      O => eol_reg_209
    );
\eol_1_reg_221[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => eol_1_reg_221,
      I1 => brmerge_reg_425,
      I2 => AXI_video_strm_V_last_V_0_data_out,
      I3 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      I4 => axi_last_V1_reg_167,
      O => \eol_1_reg_221[0]_i_2_n_0\
    );
\eol_1_reg_221[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \exitcond_reg_416_reg_n_0_[0]\,
      I1 => ap_enable_reg_pp1_iter1_reg_n_0,
      I2 => exitcond_reg_4160,
      O => \^axivideo2mat_u0_img_data_stream_2_v_write\
    );
\eol_1_reg_221_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \eol_1_reg_221[0]_i_2_n_0\,
      Q => eol_1_reg_221,
      R => '0'
    );
\eol_2_reg_268[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BAAAAAAA"
    )
        port map (
      I0 => ap_CS_fsm_state7,
      I1 => \eol_2_reg_268_reg_n_0_[0]\,
      I2 => ap_enable_reg_pp2_iter1_reg_n_0,
      I3 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      I4 => ap_CS_fsm_pp2_stage0,
      O => \eol_2_reg_268[0]_i_1_n_0\
    );
\eol_2_reg_268[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \eol_reg_209_reg_n_0_[0]\,
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_last_V_0_payload_B,
      I3 => AXI_video_strm_V_last_V_0_sel,
      I4 => AXI_video_strm_V_last_V_0_payload_A,
      O => \eol_2_reg_268[0]_i_2_n_0\
    );
\eol_2_reg_268_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_268[0]_i_1_n_0\,
      D => \eol_2_reg_268[0]_i_2_n_0\,
      Q => \eol_2_reg_268_reg_n_0_[0]\,
      R => '0'
    );
\eol_reg_209[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFE200E200000000"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_0_payload_A,
      I1 => AXI_video_strm_V_last_V_0_sel,
      I2 => AXI_video_strm_V_last_V_0_payload_B,
      I3 => brmerge_reg_425,
      I4 => eol_1_reg_221,
      I5 => \^axivideo2mat_u0_img_data_stream_2_v_write\,
      O => \eol_reg_209[0]_i_1_n_0\
    );
\eol_reg_209_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_209,
      D => \eol_reg_209[0]_i_1_n_0\,
      Q => \eol_reg_209_reg_n_0_[0]\,
      R => '0'
    );
\exitcond_reg_416[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => exitcond_fu_334_p2,
      I1 => exitcond_reg_4160,
      I2 => \exitcond_reg_416_reg_n_0_[0]\,
      O => \exitcond_reg_416[0]_i_1_n_0\
    );
\exitcond_reg_416_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \exitcond_reg_416[0]_i_1_n_0\,
      Q => \exitcond_reg_416_reg_n_0_[0]\,
      R => '0'
    );
\i_V_reg_411[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => t_V_reg_187(0),
      O => i_V_fu_328_p2(0)
    );
\i_V_reg_411[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => t_V_reg_187(8),
      I1 => t_V_reg_187(6),
      I2 => \i_V_reg_411[10]_i_2_n_0\,
      I3 => t_V_reg_187(7),
      I4 => t_V_reg_187(9),
      I5 => t_V_reg_187(10),
      O => i_V_fu_328_p2(10)
    );
\i_V_reg_411[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => t_V_reg_187(5),
      I1 => t_V_reg_187(3),
      I2 => t_V_reg_187(1),
      I3 => t_V_reg_187(0),
      I4 => t_V_reg_187(2),
      I5 => t_V_reg_187(4),
      O => \i_V_reg_411[10]_i_2_n_0\
    );
\i_V_reg_411[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => t_V_reg_187(0),
      I1 => t_V_reg_187(1),
      O => i_V_fu_328_p2(1)
    );
\i_V_reg_411[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => t_V_reg_187(0),
      I1 => t_V_reg_187(1),
      I2 => t_V_reg_187(2),
      O => i_V_fu_328_p2(2)
    );
\i_V_reg_411[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => t_V_reg_187(1),
      I1 => t_V_reg_187(0),
      I2 => t_V_reg_187(2),
      I3 => t_V_reg_187(3),
      O => i_V_fu_328_p2(3)
    );
\i_V_reg_411[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => t_V_reg_187(2),
      I1 => t_V_reg_187(0),
      I2 => t_V_reg_187(1),
      I3 => t_V_reg_187(3),
      I4 => t_V_reg_187(4),
      O => i_V_fu_328_p2(4)
    );
\i_V_reg_411[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => t_V_reg_187(3),
      I1 => t_V_reg_187(1),
      I2 => t_V_reg_187(0),
      I3 => t_V_reg_187(2),
      I4 => t_V_reg_187(4),
      I5 => t_V_reg_187(5),
      O => i_V_fu_328_p2(5)
    );
\i_V_reg_411[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_V_reg_411[10]_i_2_n_0\,
      I1 => t_V_reg_187(6),
      O => i_V_fu_328_p2(6)
    );
\i_V_reg_411[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \i_V_reg_411[10]_i_2_n_0\,
      I1 => t_V_reg_187(6),
      I2 => t_V_reg_187(7),
      O => i_V_fu_328_p2(7)
    );
\i_V_reg_411[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => t_V_reg_187(6),
      I1 => \i_V_reg_411[10]_i_2_n_0\,
      I2 => t_V_reg_187(7),
      I3 => t_V_reg_187(8),
      O => i_V_fu_328_p2(8)
    );
\i_V_reg_411[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => t_V_reg_187(7),
      I1 => \i_V_reg_411[10]_i_2_n_0\,
      I2 => t_V_reg_187(6),
      I3 => t_V_reg_187(8),
      I4 => t_V_reg_187(9),
      O => i_V_fu_328_p2(9)
    );
\i_V_reg_411_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(0),
      Q => i_V_reg_411(0),
      R => '0'
    );
\i_V_reg_411_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(10),
      Q => i_V_reg_411(10),
      R => '0'
    );
\i_V_reg_411_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(1),
      Q => i_V_reg_411(1),
      R => '0'
    );
\i_V_reg_411_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(2),
      Q => i_V_reg_411(2),
      R => '0'
    );
\i_V_reg_411_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(3),
      Q => i_V_reg_411(3),
      R => '0'
    );
\i_V_reg_411_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(4),
      Q => i_V_reg_411(4),
      R => '0'
    );
\i_V_reg_411_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(5),
      Q => i_V_reg_411(5),
      R => '0'
    );
\i_V_reg_411_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(6),
      Q => i_V_reg_411(6),
      R => '0'
    );
\i_V_reg_411_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(7),
      Q => i_V_reg_411(7),
      R => '0'
    );
\i_V_reg_411_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(8),
      Q => i_V_reg_411(8),
      R => '0'
    );
\i_V_reg_411_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_328_p2(9),
      Q => i_V_reg_411(9),
      R => '0'
    );
internal_empty_n_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F7FF"
    )
        port map (
      I0 => exitcond_reg_4160,
      I1 => ap_enable_reg_pp1_iter1_reg_n_0,
      I2 => \exitcond_reg_416_reg_n_0_[0]\,
      I3 => input_data_data_stre_2_full_n,
      O => internal_empty_n_reg
    );
\internal_empty_n_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F7FF"
    )
        port map (
      I0 => exitcond_reg_4160,
      I1 => ap_enable_reg_pp1_iter1_reg_n_0,
      I2 => \exitcond_reg_416_reg_n_0_[0]\,
      I3 => input_data_data_stre_1_full_n,
      O => internal_empty_n_reg_0
    );
\internal_empty_n_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F7FF"
    )
        port map (
      I0 => exitcond_reg_4160,
      I1 => ap_enable_reg_pp1_iter1_reg_n_0,
      I2 => \exitcond_reg_416_reg_n_0_[0]\,
      I3 => input_data_data_stre_full_n,
      O => internal_empty_n_reg_1
    );
\mOutPtr[1]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => ap_start,
      I1 => \^start_once_reg\,
      I2 => start_for_Loop_loop_height_pro_U0_full_n,
      O => \mOutPtr_reg[1]\
    );
\sof_1_fu_124[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F7F7F700"
    )
        port map (
      I0 => ap_enable_reg_pp1_iter0,
      I1 => exitcond_reg_4160,
      I2 => exitcond_fu_334_p2,
      I3 => sof_1_fu_124,
      I4 => ap_CS_fsm_state3,
      O => \sof_1_fu_124[0]_i_1_n_0\
    );
\sof_1_fu_124_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \sof_1_fu_124[0]_i_1_n_0\,
      Q => sof_1_fu_124,
      R => '0'
    );
\start_once_reg_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"77707700"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \exitcond3_fu_322_p2__14\,
      I2 => ap_start,
      I3 => \^start_once_reg\,
      I4 => start_for_Loop_loop_height_pro_U0_full_n,
      O => \start_once_reg_i_1__0_n_0\
    );
start_once_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \start_once_reg_i_1__0_n_0\,
      Q => \^start_once_reg\,
      R => \^ap_rst\
    );
\t_V_2_reg_198[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(0),
      O => j_V_fu_340_p2(0)
    );
\t_V_2_reg_198[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000F700"
    )
        port map (
      I0 => ap_enable_reg_pp1_iter0,
      I1 => exitcond_reg_4160,
      I2 => exitcond_fu_334_p2,
      I3 => ap_CS_fsm_state4,
      I4 => \exitcond3_fu_322_p2__14\,
      O => t_V_2_reg_198
    );
\t_V_2_reg_198[10]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => ap_enable_reg_pp1_iter0,
      I1 => exitcond_reg_4160,
      I2 => exitcond_fu_334_p2,
      O => sof_1_fu_1240
    );
\t_V_2_reg_198[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(8),
      I1 => \t_V_2_reg_198_reg__0\(6),
      I2 => \t_V_2_reg_198[10]_i_5_n_0\,
      I3 => \t_V_2_reg_198_reg__0\(7),
      I4 => \t_V_2_reg_198_reg__0\(9),
      I5 => \t_V_2_reg_198_reg__0\(10),
      O => j_V_fu_340_p2(10)
    );
\t_V_2_reg_198[10]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => \t_V_2_reg_198[10]_i_6_n_0\,
      I1 => \t_V_2_reg_198[10]_i_7_n_0\,
      I2 => \t_V_2_reg_198_reg__0\(0),
      I3 => \t_V_2_reg_198_reg__0\(1),
      I4 => \t_V_2_reg_198_reg__0\(2),
      O => exitcond_fu_334_p2
    );
\t_V_2_reg_198[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(5),
      I1 => \t_V_2_reg_198_reg__0\(3),
      I2 => \t_V_2_reg_198_reg__0\(1),
      I3 => \t_V_2_reg_198_reg__0\(0),
      I4 => \t_V_2_reg_198_reg__0\(2),
      I5 => \t_V_2_reg_198_reg__0\(4),
      O => \t_V_2_reg_198[10]_i_5_n_0\
    );
\t_V_2_reg_198[10]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(6),
      I1 => \t_V_2_reg_198_reg__0\(5),
      I2 => \t_V_2_reg_198_reg__0\(4),
      I3 => \t_V_2_reg_198_reg__0\(3),
      O => \t_V_2_reg_198[10]_i_6_n_0\
    );
\t_V_2_reg_198[10]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(10),
      I1 => \t_V_2_reg_198_reg__0\(9),
      I2 => \t_V_2_reg_198_reg__0\(8),
      I3 => \t_V_2_reg_198_reg__0\(7),
      O => \t_V_2_reg_198[10]_i_7_n_0\
    );
\t_V_2_reg_198[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(0),
      I1 => \t_V_2_reg_198_reg__0\(1),
      O => j_V_fu_340_p2(1)
    );
\t_V_2_reg_198[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(0),
      I1 => \t_V_2_reg_198_reg__0\(1),
      I2 => \t_V_2_reg_198_reg__0\(2),
      O => j_V_fu_340_p2(2)
    );
\t_V_2_reg_198[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(1),
      I1 => \t_V_2_reg_198_reg__0\(0),
      I2 => \t_V_2_reg_198_reg__0\(2),
      I3 => \t_V_2_reg_198_reg__0\(3),
      O => j_V_fu_340_p2(3)
    );
\t_V_2_reg_198[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(2),
      I1 => \t_V_2_reg_198_reg__0\(0),
      I2 => \t_V_2_reg_198_reg__0\(1),
      I3 => \t_V_2_reg_198_reg__0\(3),
      I4 => \t_V_2_reg_198_reg__0\(4),
      O => j_V_fu_340_p2(4)
    );
\t_V_2_reg_198[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(3),
      I1 => \t_V_2_reg_198_reg__0\(1),
      I2 => \t_V_2_reg_198_reg__0\(0),
      I3 => \t_V_2_reg_198_reg__0\(2),
      I4 => \t_V_2_reg_198_reg__0\(4),
      I5 => \t_V_2_reg_198_reg__0\(5),
      O => j_V_fu_340_p2(5)
    );
\t_V_2_reg_198[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_2_reg_198[10]_i_5_n_0\,
      I1 => \t_V_2_reg_198_reg__0\(6),
      O => j_V_fu_340_p2(6)
    );
\t_V_2_reg_198[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \t_V_2_reg_198[10]_i_5_n_0\,
      I1 => \t_V_2_reg_198_reg__0\(6),
      I2 => \t_V_2_reg_198_reg__0\(7),
      O => j_V_fu_340_p2(7)
    );
\t_V_2_reg_198[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(6),
      I1 => \t_V_2_reg_198[10]_i_5_n_0\,
      I2 => \t_V_2_reg_198_reg__0\(7),
      I3 => \t_V_2_reg_198_reg__0\(8),
      O => j_V_fu_340_p2(8)
    );
\t_V_2_reg_198[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \t_V_2_reg_198_reg__0\(7),
      I1 => \t_V_2_reg_198[10]_i_5_n_0\,
      I2 => \t_V_2_reg_198_reg__0\(6),
      I3 => \t_V_2_reg_198_reg__0\(8),
      I4 => \t_V_2_reg_198_reg__0\(9),
      O => j_V_fu_340_p2(9)
    );
\t_V_2_reg_198_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(0),
      Q => \t_V_2_reg_198_reg__0\(0),
      R => t_V_2_reg_198
    );
\t_V_2_reg_198_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(10),
      Q => \t_V_2_reg_198_reg__0\(10),
      R => t_V_2_reg_198
    );
\t_V_2_reg_198_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(1),
      Q => \t_V_2_reg_198_reg__0\(1),
      R => t_V_2_reg_198
    );
\t_V_2_reg_198_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(2),
      Q => \t_V_2_reg_198_reg__0\(2),
      R => t_V_2_reg_198
    );
\t_V_2_reg_198_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(3),
      Q => \t_V_2_reg_198_reg__0\(3),
      R => t_V_2_reg_198
    );
\t_V_2_reg_198_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(4),
      Q => \t_V_2_reg_198_reg__0\(4),
      R => t_V_2_reg_198
    );
\t_V_2_reg_198_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(5),
      Q => \t_V_2_reg_198_reg__0\(5),
      R => t_V_2_reg_198
    );
\t_V_2_reg_198_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(6),
      Q => \t_V_2_reg_198_reg__0\(6),
      R => t_V_2_reg_198
    );
\t_V_2_reg_198_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(7),
      Q => \t_V_2_reg_198_reg__0\(7),
      R => t_V_2_reg_198
    );
\t_V_2_reg_198_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(8),
      Q => \t_V_2_reg_198_reg__0\(8),
      R => t_V_2_reg_198
    );
\t_V_2_reg_198_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1240,
      D => j_V_fu_340_p2(9),
      Q => \t_V_2_reg_198_reg__0\(9),
      R => t_V_2_reg_198
    );
\t_V_reg_187_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(0),
      Q => t_V_reg_187(0),
      R => ap_CS_fsm_state3
    );
\t_V_reg_187_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(10),
      Q => t_V_reg_187(10),
      R => ap_CS_fsm_state3
    );
\t_V_reg_187_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(1),
      Q => t_V_reg_187(1),
      R => ap_CS_fsm_state3
    );
\t_V_reg_187_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(2),
      Q => t_V_reg_187(2),
      R => ap_CS_fsm_state3
    );
\t_V_reg_187_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(3),
      Q => t_V_reg_187(3),
      R => ap_CS_fsm_state3
    );
\t_V_reg_187_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(4),
      Q => t_V_reg_187(4),
      R => ap_CS_fsm_state3
    );
\t_V_reg_187_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(5),
      Q => t_V_reg_187(5),
      R => ap_CS_fsm_state3
    );
\t_V_reg_187_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(6),
      Q => t_V_reg_187(6),
      R => ap_CS_fsm_state3
    );
\t_V_reg_187_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(7),
      Q => t_V_reg_187(7),
      R => ap_CS_fsm_state3
    );
\t_V_reg_187_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(8),
      Q => t_V_reg_187(8),
      R => ap_CS_fsm_state3
    );
\t_V_reg_187_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_411(9),
      Q => t_V_reg_187(9),
      R => ap_CS_fsm_state3
    );
\tmp_data_V_reg_387[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(0),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(0),
      O => AXI_video_strm_V_data_V_0_data_out(0)
    );
\tmp_data_V_reg_387[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(10),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(10),
      O => AXI_video_strm_V_data_V_0_data_out(10)
    );
\tmp_data_V_reg_387[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(11),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(11),
      O => AXI_video_strm_V_data_V_0_data_out(11)
    );
\tmp_data_V_reg_387[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(12),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(12),
      O => AXI_video_strm_V_data_V_0_data_out(12)
    );
\tmp_data_V_reg_387[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(13),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(13),
      O => AXI_video_strm_V_data_V_0_data_out(13)
    );
\tmp_data_V_reg_387[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(14),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(14),
      O => AXI_video_strm_V_data_V_0_data_out(14)
    );
\tmp_data_V_reg_387[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(15),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(15),
      O => AXI_video_strm_V_data_V_0_data_out(15)
    );
\tmp_data_V_reg_387[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(16),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(16),
      O => AXI_video_strm_V_data_V_0_data_out(16)
    );
\tmp_data_V_reg_387[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(17),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(17),
      O => AXI_video_strm_V_data_V_0_data_out(17)
    );
\tmp_data_V_reg_387[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(18),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(18),
      O => AXI_video_strm_V_data_V_0_data_out(18)
    );
\tmp_data_V_reg_387[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(19),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(19),
      O => AXI_video_strm_V_data_V_0_data_out(19)
    );
\tmp_data_V_reg_387[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(1),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(1),
      O => AXI_video_strm_V_data_V_0_data_out(1)
    );
\tmp_data_V_reg_387[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(20),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(20),
      O => AXI_video_strm_V_data_V_0_data_out(20)
    );
\tmp_data_V_reg_387[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(21),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(21),
      O => AXI_video_strm_V_data_V_0_data_out(21)
    );
\tmp_data_V_reg_387[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(22),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(22),
      O => AXI_video_strm_V_data_V_0_data_out(22)
    );
\tmp_data_V_reg_387[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(23),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(23),
      O => AXI_video_strm_V_data_V_0_data_out(23)
    );
\tmp_data_V_reg_387[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(2),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(2),
      O => AXI_video_strm_V_data_V_0_data_out(2)
    );
\tmp_data_V_reg_387[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(3),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(3),
      O => AXI_video_strm_V_data_V_0_data_out(3)
    );
\tmp_data_V_reg_387[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(4),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(4),
      O => AXI_video_strm_V_data_V_0_data_out(4)
    );
\tmp_data_V_reg_387[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(5),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(5),
      O => AXI_video_strm_V_data_V_0_data_out(5)
    );
\tmp_data_V_reg_387[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(6),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(6),
      O => AXI_video_strm_V_data_V_0_data_out(6)
    );
\tmp_data_V_reg_387[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(7),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(7),
      O => AXI_video_strm_V_data_V_0_data_out(7)
    );
\tmp_data_V_reg_387[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(8),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(8),
      O => AXI_video_strm_V_data_V_0_data_out(8)
    );
\tmp_data_V_reg_387[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(9),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(9),
      O => AXI_video_strm_V_data_V_0_data_out(9)
    );
\tmp_data_V_reg_387_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(0),
      Q => tmp_data_V_reg_387(0),
      R => '0'
    );
\tmp_data_V_reg_387_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(10),
      Q => tmp_data_V_reg_387(10),
      R => '0'
    );
\tmp_data_V_reg_387_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(11),
      Q => tmp_data_V_reg_387(11),
      R => '0'
    );
\tmp_data_V_reg_387_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(12),
      Q => tmp_data_V_reg_387(12),
      R => '0'
    );
\tmp_data_V_reg_387_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(13),
      Q => tmp_data_V_reg_387(13),
      R => '0'
    );
\tmp_data_V_reg_387_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(14),
      Q => tmp_data_V_reg_387(14),
      R => '0'
    );
\tmp_data_V_reg_387_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(15),
      Q => tmp_data_V_reg_387(15),
      R => '0'
    );
\tmp_data_V_reg_387_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(16),
      Q => tmp_data_V_reg_387(16),
      R => '0'
    );
\tmp_data_V_reg_387_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(17),
      Q => tmp_data_V_reg_387(17),
      R => '0'
    );
\tmp_data_V_reg_387_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(18),
      Q => tmp_data_V_reg_387(18),
      R => '0'
    );
\tmp_data_V_reg_387_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(19),
      Q => tmp_data_V_reg_387(19),
      R => '0'
    );
\tmp_data_V_reg_387_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(1),
      Q => tmp_data_V_reg_387(1),
      R => '0'
    );
\tmp_data_V_reg_387_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(20),
      Q => tmp_data_V_reg_387(20),
      R => '0'
    );
\tmp_data_V_reg_387_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(21),
      Q => tmp_data_V_reg_387(21),
      R => '0'
    );
\tmp_data_V_reg_387_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(22),
      Q => tmp_data_V_reg_387(22),
      R => '0'
    );
\tmp_data_V_reg_387_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(23),
      Q => tmp_data_V_reg_387(23),
      R => '0'
    );
\tmp_data_V_reg_387_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(2),
      Q => tmp_data_V_reg_387(2),
      R => '0'
    );
\tmp_data_V_reg_387_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(3),
      Q => tmp_data_V_reg_387(3),
      R => '0'
    );
\tmp_data_V_reg_387_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(4),
      Q => tmp_data_V_reg_387(4),
      R => '0'
    );
\tmp_data_V_reg_387_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(5),
      Q => tmp_data_V_reg_387(5),
      R => '0'
    );
\tmp_data_V_reg_387_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(6),
      Q => tmp_data_V_reg_387(6),
      R => '0'
    );
\tmp_data_V_reg_387_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(7),
      Q => tmp_data_V_reg_387(7),
      R => '0'
    );
\tmp_data_V_reg_387_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(8),
      Q => tmp_data_V_reg_387(8),
      R => '0'
    );
\tmp_data_V_reg_387_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(9),
      Q => tmp_data_V_reg_387(9),
      R => '0'
    );
\tmp_last_V_reg_395[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => \AXI_video_strm_V_data_V_0_state_reg_n_0_[0]\,
      O => AXI_video_strm_V_data_V_0_sel2
    );
\tmp_last_V_reg_395[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_0_payload_B,
      I1 => AXI_video_strm_V_last_V_0_sel,
      I2 => AXI_video_strm_V_last_V_0_payload_A,
      O => AXI_video_strm_V_last_V_0_data_out
    );
\tmp_last_V_reg_395_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_last_V_0_data_out,
      Q => tmp_last_V_reg_395,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Loop_loop_height_pro is
  port (
    start_once_reg_reg_0 : out STD_LOGIC;
    Loop_loop_height_pro_U0_ap_ready : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ce : out STD_LOGIC;
    ce_0 : out STD_LOGIC;
    ce_1 : out STD_LOGIC;
    Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write : out STD_LOGIC;
    ce_2 : out STD_LOGIC;
    ce_3 : out STD_LOGIC;
    ce_4 : out STD_LOGIC;
    \SRL_SIG_reg[0][7]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][7]_0\ : out STD_LOGIC;
    \SRL_SIG_reg[0][7]_1\ : out STD_LOGIC;
    ap_rst : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    internal_full_n_reg : in STD_LOGIC;
    start_for_Mat2AXIvideo_U0_full_n : in STD_LOGIC;
    start_for_Mat2AXIvideo_1_U0_full_n : in STD_LOGIC;
    start_for_Loop_loop_height_pro_U0_empty_n : in STD_LOGIC;
    passthrough_data_str_2_full_n : in STD_LOGIC;
    passthrough_data_str_1_full_n : in STD_LOGIC;
    passthrough_data_str_full_n : in STD_LOGIC;
    internal_full_n_reg_0 : in STD_LOGIC;
    input_data_data_stre_empty_n : in STD_LOGIC;
    crop_data_data_strea_full_n : in STD_LOGIC;
    crop_data_data_strea_1_full_n : in STD_LOGIC;
    crop_data_data_strea_2_full_n : in STD_LOGIC;
    internal_full_n_reg_1 : in STD_LOGIC;
    internal_full_n_reg_2 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Loop_loop_height_pro;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Loop_loop_height_pro is
  signal \^loop_loop_height_pro_u0_ap_ready\ : STD_LOGIC;
  signal Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state3 : STD_LOGIC;
  signal ap_CS_fsm_state5 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal ap_NS_fsm1 : STD_LOGIC;
  signal ap_NS_fsm14_out : STD_LOGIC;
  signal ap_NS_fsm16_out : STD_LOGIC;
  signal \^ce\ : STD_LOGIC;
  signal \^ce_0\ : STD_LOGIC;
  signal \^ce_1\ : STD_LOGIC;
  signal i_V_fu_303_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal i_V_reg_348 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \i_V_reg_348[10]_i_2_n_0\ : STD_LOGIC;
  signal j_V_1_fu_339_p2 : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal j_V_2_fu_327_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal j_V_fu_315_p2 : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \p_070_1_reg_275[10]_i_10_n_0\ : STD_LOGIC;
  signal \p_070_1_reg_275[10]_i_4_n_0\ : STD_LOGIC;
  signal \p_070_1_reg_275[10]_i_7_n_0\ : STD_LOGIC;
  signal \p_070_1_reg_275[10]_i_8_n_0\ : STD_LOGIC;
  signal \p_070_1_reg_275[10]_i_9_n_0\ : STD_LOGIC;
  signal \p_070_1_reg_275_reg__0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \p_070_2_reg_286[8]_i_2_n_0\ : STD_LOGIC;
  signal \p_070_2_reg_286[8]_i_5_n_0\ : STD_LOGIC;
  signal \p_070_2_reg_286[8]_i_6_n_0\ : STD_LOGIC;
  signal \p_070_2_reg_286_reg__0\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal p_1_reg_264 : STD_LOGIC;
  signal p_1_reg_2640 : STD_LOGIC;
  signal \p_1_reg_264[8]_i_5_n_0\ : STD_LOGIC;
  signal \p_1_reg_264[8]_i_6_n_0\ : STD_LOGIC;
  signal \p_1_reg_264[8]_i_7_n_0\ : STD_LOGIC;
  signal \p_1_reg_264[8]_i_8_n_0\ : STD_LOGIC;
  signal \p_1_reg_264_reg__0\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal p_s_reg_253 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \p_s_reg_253[10]_i_3_n_0\ : STD_LOGIC;
  signal p_s_reg_253_0 : STD_LOGIC;
  signal start_once_reg_i_1_n_0 : STD_LOGIC;
  signal \^start_once_reg_reg_0\ : STD_LOGIC;
  signal \tmp_fu_297_p2__12\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_1__2\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_1__3\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_1__4\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_1__5\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_1__6\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_1__7\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_2\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_2__0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_2__1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_1__0\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_2\ : label is "soft_lutpair65";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[4]\ : label is "none";
  attribute SOFT_HLUTNM of \i_V_reg_348[0]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \i_V_reg_348[1]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \i_V_reg_348[2]_i_1\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \i_V_reg_348[3]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \i_V_reg_348[4]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \i_V_reg_348[6]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \i_V_reg_348[8]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \i_V_reg_348[9]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_2__1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \p_070_1_reg_275[0]_i_1\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \p_070_1_reg_275[10]_i_7\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \p_070_1_reg_275[10]_i_9\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \p_070_1_reg_275[1]_i_1\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \p_070_1_reg_275[2]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \p_070_1_reg_275[3]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \p_070_1_reg_275[4]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \p_070_1_reg_275[6]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \p_070_1_reg_275[8]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \p_070_1_reg_275[9]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \p_070_2_reg_286[0]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \p_070_2_reg_286[1]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \p_070_2_reg_286[2]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \p_070_2_reg_286[3]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \p_070_2_reg_286[4]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \p_070_2_reg_286[7]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \p_070_2_reg_286[8]_i_3\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \p_070_2_reg_286[8]_i_5\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \p_1_reg_264[0]_i_1\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \p_1_reg_264[1]_i_1\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \p_1_reg_264[2]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \p_1_reg_264[3]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \p_1_reg_264[4]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \p_1_reg_264[7]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \p_1_reg_264[8]_i_3\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \p_1_reg_264[8]_i_4\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \p_1_reg_264[8]_i_5\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \p_1_reg_264[8]_i_7\ : label is "soft_lutpair64";
begin
  Loop_loop_height_pro_U0_ap_ready <= \^loop_loop_height_pro_u0_ap_ready\;
  Q(1 downto 0) <= \^q\(1 downto 0);
  ce <= \^ce\;
  ce_0 <= \^ce_0\;
  ce_1 <= \^ce_1\;
  start_once_reg_reg_0 <= \^start_once_reg_reg_0\;
\SRL_SIG[0][7]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      I1 => crop_data_data_strea_full_n,
      O => ce_2
    );
\SRL_SIG[0][7]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      I1 => crop_data_data_strea_1_full_n,
      O => ce_3
    );
\SRL_SIG[0][7]_i_1__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      I1 => crop_data_data_strea_2_full_n,
      O => ce_4
    );
\SRL_SIG[0][7]_i_1__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^ce\,
      I1 => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      O => \SRL_SIG_reg[0][7]\
    );
\SRL_SIG[0][7]_i_1__6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^ce_0\,
      I1 => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      O => \SRL_SIG_reg[0][7]_0\
    );
\SRL_SIG[0][7]_i_1__7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^ce_1\,
      I1 => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      O => \SRL_SIG_reg[0][7]_1\
    );
\SRL_SIG[0][7]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE00"
    )
        port map (
      I0 => \p_070_2_reg_286[8]_i_2_n_0\,
      I1 => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      I2 => p_1_reg_2640,
      I3 => passthrough_data_str_2_full_n,
      O => \^ce\
    );
\SRL_SIG[0][7]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE00"
    )
        port map (
      I0 => \p_070_2_reg_286[8]_i_2_n_0\,
      I1 => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      I2 => p_1_reg_2640,
      I3 => passthrough_data_str_1_full_n,
      O => \^ce_0\
    );
\SRL_SIG[0][7]_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE00"
    )
        port map (
      I0 => \p_070_2_reg_286[8]_i_2_n_0\,
      I1 => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      I2 => p_1_reg_2640,
      I3 => passthrough_data_str_full_n,
      O => \^ce_1\
    );
\ap_CS_fsm[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88F8"
    )
        port map (
      I0 => \^loop_loop_height_pro_u0_ap_ready\,
      I1 => ap_CS_fsm_state2,
      I2 => \^q\(0),
      I3 => internal_full_n_reg,
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => ap_CS_fsm_state5,
      I2 => \^q\(0),
      I3 => internal_full_n_reg,
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[2]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => ap_NS_fsm16_out,
      I1 => ap_CS_fsm_state3,
      I2 => \^loop_loop_height_pro_u0_ap_ready\,
      I3 => ap_CS_fsm_state2,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \tmp_fu_297_p2__12\,
      I1 => ap_CS_fsm_state2,
      O => \^loop_loop_height_pro_u0_ap_ready\
    );
\ap_CS_fsm[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F444"
    )
        port map (
      I0 => ap_NS_fsm14_out,
      I1 => \^q\(1),
      I2 => ap_NS_fsm16_out,
      I3 => ap_CS_fsm_state3,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F444"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => ap_CS_fsm_state5,
      I2 => ap_NS_fsm14_out,
      I3 => \^q\(1),
      O => ap_NS_fsm(4)
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \^q\(0),
      S => ap_rst
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => ap_rst
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_CS_fsm_state3,
      R => ap_rst
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => \^q\(1),
      R => ap_rst
    );
\ap_CS_fsm_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(4),
      Q => ap_CS_fsm_state5,
      R => ap_rst
    );
\i_V_reg_348[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => p_s_reg_253(0),
      O => i_V_fu_303_p2(0)
    );
\i_V_reg_348[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => p_s_reg_253(8),
      I1 => p_s_reg_253(6),
      I2 => \i_V_reg_348[10]_i_2_n_0\,
      I3 => p_s_reg_253(7),
      I4 => p_s_reg_253(9),
      I5 => p_s_reg_253(10),
      O => i_V_fu_303_p2(10)
    );
\i_V_reg_348[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_s_reg_253(5),
      I1 => p_s_reg_253(3),
      I2 => p_s_reg_253(1),
      I3 => p_s_reg_253(0),
      I4 => p_s_reg_253(2),
      I5 => p_s_reg_253(4),
      O => \i_V_reg_348[10]_i_2_n_0\
    );
\i_V_reg_348[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_s_reg_253(0),
      I1 => p_s_reg_253(1),
      O => i_V_fu_303_p2(1)
    );
\i_V_reg_348[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => p_s_reg_253(0),
      I1 => p_s_reg_253(1),
      I2 => p_s_reg_253(2),
      O => i_V_fu_303_p2(2)
    );
\i_V_reg_348[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => p_s_reg_253(1),
      I1 => p_s_reg_253(0),
      I2 => p_s_reg_253(2),
      I3 => p_s_reg_253(3),
      O => i_V_fu_303_p2(3)
    );
\i_V_reg_348[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => p_s_reg_253(2),
      I1 => p_s_reg_253(0),
      I2 => p_s_reg_253(1),
      I3 => p_s_reg_253(3),
      I4 => p_s_reg_253(4),
      O => i_V_fu_303_p2(4)
    );
\i_V_reg_348[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => p_s_reg_253(3),
      I1 => p_s_reg_253(1),
      I2 => p_s_reg_253(0),
      I3 => p_s_reg_253(2),
      I4 => p_s_reg_253(4),
      I5 => p_s_reg_253(5),
      O => i_V_fu_303_p2(5)
    );
\i_V_reg_348[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_V_reg_348[10]_i_2_n_0\,
      I1 => p_s_reg_253(6),
      O => i_V_fu_303_p2(6)
    );
\i_V_reg_348[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \i_V_reg_348[10]_i_2_n_0\,
      I1 => p_s_reg_253(6),
      I2 => p_s_reg_253(7),
      O => i_V_fu_303_p2(7)
    );
\i_V_reg_348[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => p_s_reg_253(6),
      I1 => \i_V_reg_348[10]_i_2_n_0\,
      I2 => p_s_reg_253(7),
      I3 => p_s_reg_253(8),
      O => i_V_fu_303_p2(8)
    );
\i_V_reg_348[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => p_s_reg_253(7),
      I1 => \i_V_reg_348[10]_i_2_n_0\,
      I2 => p_s_reg_253(6),
      I3 => p_s_reg_253(8),
      I4 => p_s_reg_253(9),
      O => i_V_fu_303_p2(9)
    );
\i_V_reg_348_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(0),
      Q => i_V_reg_348(0),
      R => '0'
    );
\i_V_reg_348_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(10),
      Q => i_V_reg_348(10),
      R => '0'
    );
\i_V_reg_348_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(1),
      Q => i_V_reg_348(1),
      R => '0'
    );
\i_V_reg_348_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(2),
      Q => i_V_reg_348(2),
      R => '0'
    );
\i_V_reg_348_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(3),
      Q => i_V_reg_348(3),
      R => '0'
    );
\i_V_reg_348_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(4),
      Q => i_V_reg_348(4),
      R => '0'
    );
\i_V_reg_348_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(5),
      Q => i_V_reg_348(5),
      R => '0'
    );
\i_V_reg_348_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(6),
      Q => i_V_reg_348(6),
      R => '0'
    );
\i_V_reg_348_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(7),
      Q => i_V_reg_348(7),
      R => '0'
    );
\i_V_reg_348_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(8),
      Q => i_V_reg_348(8),
      R => '0'
    );
\i_V_reg_348_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_V_fu_303_p2(9),
      Q => i_V_reg_348(9),
      R => '0'
    );
\mOutPtr[1]_i_2__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => p_1_reg_2640,
      I1 => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      I2 => \p_070_2_reg_286[8]_i_2_n_0\,
      O => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write
    );
\p_070_1_reg_275[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(0),
      O => j_V_2_fu_327_p2(0)
    );
\p_070_1_reg_275[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00200000"
    )
        port map (
      I0 => \p_070_1_reg_275[10]_i_4_n_0\,
      I1 => \p_1_reg_264_reg__0\(0),
      I2 => \p_1_reg_264_reg__0\(2),
      I3 => \p_1_reg_264_reg__0\(1),
      I4 => ap_CS_fsm_state3,
      O => ap_NS_fsm16_out
    );
\p_070_1_reg_275[10]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(5),
      I1 => \p_070_1_reg_275_reg__0\(3),
      I2 => \p_070_1_reg_275_reg__0\(1),
      I3 => \p_070_1_reg_275_reg__0\(0),
      I4 => \p_070_1_reg_275_reg__0\(2),
      I5 => \p_070_1_reg_275_reg__0\(4),
      O => \p_070_1_reg_275[10]_i_10_n_0\
    );
\p_070_1_reg_275[10]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08888888"
    )
        port map (
      I0 => internal_full_n_reg_1,
      I1 => internal_full_n_reg_2,
      I2 => \p_070_1_reg_275[10]_i_7_n_0\,
      I3 => \p_070_1_reg_275[10]_i_8_n_0\,
      I4 => \p_070_1_reg_275[10]_i_9_n_0\,
      O => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write
    );
\p_070_1_reg_275[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(8),
      I1 => \p_070_1_reg_275_reg__0\(6),
      I2 => \p_070_1_reg_275[10]_i_10_n_0\,
      I3 => \p_070_1_reg_275_reg__0\(7),
      I4 => \p_070_1_reg_275_reg__0\(9),
      I5 => \p_070_1_reg_275_reg__0\(10),
      O => j_V_2_fu_327_p2(10)
    );
\p_070_1_reg_275[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010000000000000"
    )
        port map (
      I0 => \p_1_reg_264_reg__0\(3),
      I1 => \p_1_reg_264_reg__0\(4),
      I2 => \p_1_reg_264_reg__0\(5),
      I3 => \p_1_reg_264_reg__0\(6),
      I4 => \p_1_reg_264_reg__0\(8),
      I5 => \p_1_reg_264_reg__0\(7),
      O => \p_070_1_reg_275[10]_i_4_n_0\
    );
\p_070_1_reg_275[10]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(2),
      I1 => \p_070_1_reg_275_reg__0\(1),
      I2 => \p_070_1_reg_275_reg__0\(0),
      O => \p_070_1_reg_275[10]_i_7_n_0\
    );
\p_070_1_reg_275[10]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(9),
      I1 => \p_070_1_reg_275_reg__0\(10),
      I2 => \p_070_1_reg_275_reg__0\(8),
      I3 => \p_070_1_reg_275_reg__0\(7),
      O => \p_070_1_reg_275[10]_i_8_n_0\
    );
\p_070_1_reg_275[10]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(6),
      I1 => \p_070_1_reg_275_reg__0\(5),
      I2 => \p_070_1_reg_275_reg__0\(4),
      I3 => \p_070_1_reg_275_reg__0\(3),
      O => \p_070_1_reg_275[10]_i_9_n_0\
    );
\p_070_1_reg_275[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(0),
      I1 => \p_070_1_reg_275_reg__0\(1),
      O => j_V_2_fu_327_p2(1)
    );
\p_070_1_reg_275[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(0),
      I1 => \p_070_1_reg_275_reg__0\(1),
      I2 => \p_070_1_reg_275_reg__0\(2),
      O => j_V_2_fu_327_p2(2)
    );
\p_070_1_reg_275[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(1),
      I1 => \p_070_1_reg_275_reg__0\(0),
      I2 => \p_070_1_reg_275_reg__0\(2),
      I3 => \p_070_1_reg_275_reg__0\(3),
      O => j_V_2_fu_327_p2(3)
    );
\p_070_1_reg_275[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(2),
      I1 => \p_070_1_reg_275_reg__0\(0),
      I2 => \p_070_1_reg_275_reg__0\(1),
      I3 => \p_070_1_reg_275_reg__0\(3),
      I4 => \p_070_1_reg_275_reg__0\(4),
      O => j_V_2_fu_327_p2(4)
    );
\p_070_1_reg_275[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(3),
      I1 => \p_070_1_reg_275_reg__0\(1),
      I2 => \p_070_1_reg_275_reg__0\(0),
      I3 => \p_070_1_reg_275_reg__0\(2),
      I4 => \p_070_1_reg_275_reg__0\(4),
      I5 => \p_070_1_reg_275_reg__0\(5),
      O => j_V_2_fu_327_p2(5)
    );
\p_070_1_reg_275[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \p_070_1_reg_275[10]_i_10_n_0\,
      I1 => \p_070_1_reg_275_reg__0\(6),
      O => j_V_2_fu_327_p2(6)
    );
\p_070_1_reg_275[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \p_070_1_reg_275[10]_i_10_n_0\,
      I1 => \p_070_1_reg_275_reg__0\(6),
      I2 => \p_070_1_reg_275_reg__0\(7),
      O => j_V_2_fu_327_p2(7)
    );
\p_070_1_reg_275[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(6),
      I1 => \p_070_1_reg_275[10]_i_10_n_0\,
      I2 => \p_070_1_reg_275_reg__0\(7),
      I3 => \p_070_1_reg_275_reg__0\(8),
      O => j_V_2_fu_327_p2(8)
    );
\p_070_1_reg_275[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \p_070_1_reg_275_reg__0\(7),
      I1 => \p_070_1_reg_275[10]_i_10_n_0\,
      I2 => \p_070_1_reg_275_reg__0\(6),
      I3 => \p_070_1_reg_275_reg__0\(8),
      I4 => \p_070_1_reg_275_reg__0\(9),
      O => j_V_2_fu_327_p2(9)
    );
\p_070_1_reg_275_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(0),
      Q => \p_070_1_reg_275_reg__0\(0),
      R => ap_NS_fsm16_out
    );
\p_070_1_reg_275_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(10),
      Q => \p_070_1_reg_275_reg__0\(10),
      R => ap_NS_fsm16_out
    );
\p_070_1_reg_275_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(1),
      Q => \p_070_1_reg_275_reg__0\(1),
      R => ap_NS_fsm16_out
    );
\p_070_1_reg_275_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(2),
      Q => \p_070_1_reg_275_reg__0\(2),
      R => ap_NS_fsm16_out
    );
\p_070_1_reg_275_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(3),
      Q => \p_070_1_reg_275_reg__0\(3),
      R => ap_NS_fsm16_out
    );
\p_070_1_reg_275_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(4),
      Q => \p_070_1_reg_275_reg__0\(4),
      R => ap_NS_fsm16_out
    );
\p_070_1_reg_275_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(5),
      Q => \p_070_1_reg_275_reg__0\(5),
      R => ap_NS_fsm16_out
    );
\p_070_1_reg_275_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(6),
      Q => \p_070_1_reg_275_reg__0\(6),
      R => ap_NS_fsm16_out
    );
\p_070_1_reg_275_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(7),
      Q => \p_070_1_reg_275_reg__0\(7),
      R => ap_NS_fsm16_out
    );
\p_070_1_reg_275_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(8),
      Q => \p_070_1_reg_275_reg__0\(8),
      R => ap_NS_fsm16_out
    );
\p_070_1_reg_275_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write,
      D => j_V_2_fu_327_p2(9),
      Q => \p_070_1_reg_275_reg__0\(9),
      R => ap_NS_fsm16_out
    );
\p_070_2_reg_286[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \p_070_2_reg_286_reg__0\(0),
      O => j_V_1_fu_339_p2(0)
    );
\p_070_2_reg_286[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \p_070_2_reg_286_reg__0\(0),
      I1 => \p_070_2_reg_286_reg__0\(1),
      O => j_V_1_fu_339_p2(1)
    );
\p_070_2_reg_286[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \p_070_2_reg_286_reg__0\(0),
      I1 => \p_070_2_reg_286_reg__0\(1),
      I2 => \p_070_2_reg_286_reg__0\(2),
      O => j_V_1_fu_339_p2(2)
    );
\p_070_2_reg_286[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \p_070_2_reg_286_reg__0\(1),
      I1 => \p_070_2_reg_286_reg__0\(0),
      I2 => \p_070_2_reg_286_reg__0\(2),
      I3 => \p_070_2_reg_286_reg__0\(3),
      O => j_V_1_fu_339_p2(3)
    );
\p_070_2_reg_286[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \p_070_2_reg_286_reg__0\(2),
      I1 => \p_070_2_reg_286_reg__0\(0),
      I2 => \p_070_2_reg_286_reg__0\(1),
      I3 => \p_070_2_reg_286_reg__0\(3),
      I4 => \p_070_2_reg_286_reg__0\(4),
      O => j_V_1_fu_339_p2(4)
    );
\p_070_2_reg_286[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \p_070_2_reg_286_reg__0\(3),
      I1 => \p_070_2_reg_286_reg__0\(1),
      I2 => \p_070_2_reg_286_reg__0\(0),
      I3 => \p_070_2_reg_286_reg__0\(2),
      I4 => \p_070_2_reg_286_reg__0\(4),
      I5 => \p_070_2_reg_286_reg__0\(5),
      O => j_V_1_fu_339_p2(5)
    );
\p_070_2_reg_286[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \p_070_2_reg_286[8]_i_6_n_0\,
      I1 => \p_070_2_reg_286_reg__0\(6),
      O => j_V_1_fu_339_p2(6)
    );
\p_070_2_reg_286[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \p_070_2_reg_286[8]_i_6_n_0\,
      I1 => \p_070_2_reg_286_reg__0\(6),
      I2 => \p_070_2_reg_286_reg__0\(7),
      O => j_V_1_fu_339_p2(7)
    );
\p_070_2_reg_286[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \p_070_1_reg_275[10]_i_9_n_0\,
      I1 => \p_070_1_reg_275[10]_i_8_n_0\,
      I2 => \p_070_1_reg_275_reg__0\(0),
      I3 => \p_070_1_reg_275_reg__0\(1),
      I4 => \p_070_1_reg_275_reg__0\(2),
      I5 => \^q\(1),
      O => ap_NS_fsm14_out
    );
\p_070_2_reg_286[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800080008000"
    )
        port map (
      I0 => ap_CS_fsm_state5,
      I1 => internal_full_n_reg_0,
      I2 => passthrough_data_str_2_full_n,
      I3 => input_data_data_stre_empty_n,
      I4 => \p_070_2_reg_286[8]_i_5_n_0\,
      I5 => \p_s_reg_253[10]_i_3_n_0\,
      O => \p_070_2_reg_286[8]_i_2_n_0\
    );
\p_070_2_reg_286[8]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \p_070_2_reg_286_reg__0\(6),
      I1 => \p_070_2_reg_286[8]_i_6_n_0\,
      I2 => \p_070_2_reg_286_reg__0\(7),
      I3 => \p_070_2_reg_286_reg__0\(8),
      O => j_V_1_fu_339_p2(8)
    );
\p_070_2_reg_286[8]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \p_070_2_reg_286_reg__0\(1),
      I1 => \p_070_2_reg_286_reg__0\(2),
      I2 => \p_070_2_reg_286_reg__0\(0),
      O => \p_070_2_reg_286[8]_i_5_n_0\
    );
\p_070_2_reg_286[8]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \p_070_2_reg_286_reg__0\(5),
      I1 => \p_070_2_reg_286_reg__0\(3),
      I2 => \p_070_2_reg_286_reg__0\(1),
      I3 => \p_070_2_reg_286_reg__0\(0),
      I4 => \p_070_2_reg_286_reg__0\(2),
      I5 => \p_070_2_reg_286_reg__0\(4),
      O => \p_070_2_reg_286[8]_i_6_n_0\
    );
\p_070_2_reg_286_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \p_070_2_reg_286[8]_i_2_n_0\,
      D => j_V_1_fu_339_p2(0),
      Q => \p_070_2_reg_286_reg__0\(0),
      R => ap_NS_fsm14_out
    );
\p_070_2_reg_286_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \p_070_2_reg_286[8]_i_2_n_0\,
      D => j_V_1_fu_339_p2(1),
      Q => \p_070_2_reg_286_reg__0\(1),
      R => ap_NS_fsm14_out
    );
\p_070_2_reg_286_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \p_070_2_reg_286[8]_i_2_n_0\,
      D => j_V_1_fu_339_p2(2),
      Q => \p_070_2_reg_286_reg__0\(2),
      R => ap_NS_fsm14_out
    );
\p_070_2_reg_286_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \p_070_2_reg_286[8]_i_2_n_0\,
      D => j_V_1_fu_339_p2(3),
      Q => \p_070_2_reg_286_reg__0\(3),
      R => ap_NS_fsm14_out
    );
\p_070_2_reg_286_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \p_070_2_reg_286[8]_i_2_n_0\,
      D => j_V_1_fu_339_p2(4),
      Q => \p_070_2_reg_286_reg__0\(4),
      R => ap_NS_fsm14_out
    );
\p_070_2_reg_286_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \p_070_2_reg_286[8]_i_2_n_0\,
      D => j_V_1_fu_339_p2(5),
      Q => \p_070_2_reg_286_reg__0\(5),
      R => ap_NS_fsm14_out
    );
\p_070_2_reg_286_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \p_070_2_reg_286[8]_i_2_n_0\,
      D => j_V_1_fu_339_p2(6),
      Q => \p_070_2_reg_286_reg__0\(6),
      R => ap_NS_fsm14_out
    );
\p_070_2_reg_286_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \p_070_2_reg_286[8]_i_2_n_0\,
      D => j_V_1_fu_339_p2(7),
      Q => \p_070_2_reg_286_reg__0\(7),
      R => ap_NS_fsm14_out
    );
\p_070_2_reg_286_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \p_070_2_reg_286[8]_i_2_n_0\,
      D => j_V_1_fu_339_p2(8),
      Q => \p_070_2_reg_286_reg__0\(8),
      R => ap_NS_fsm14_out
    );
\p_1_reg_264[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \p_1_reg_264_reg__0\(0),
      O => j_V_fu_315_p2(0)
    );
\p_1_reg_264[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \p_1_reg_264_reg__0\(0),
      I1 => \p_1_reg_264_reg__0\(1),
      O => j_V_fu_315_p2(1)
    );
\p_1_reg_264[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \p_1_reg_264_reg__0\(0),
      I1 => \p_1_reg_264_reg__0\(1),
      I2 => \p_1_reg_264_reg__0\(2),
      O => j_V_fu_315_p2(2)
    );
\p_1_reg_264[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \p_1_reg_264_reg__0\(1),
      I1 => \p_1_reg_264_reg__0\(0),
      I2 => \p_1_reg_264_reg__0\(2),
      I3 => \p_1_reg_264_reg__0\(3),
      O => j_V_fu_315_p2(3)
    );
\p_1_reg_264[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \p_1_reg_264_reg__0\(2),
      I1 => \p_1_reg_264_reg__0\(0),
      I2 => \p_1_reg_264_reg__0\(1),
      I3 => \p_1_reg_264_reg__0\(3),
      I4 => \p_1_reg_264_reg__0\(4),
      O => j_V_fu_315_p2(4)
    );
\p_1_reg_264[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \p_1_reg_264_reg__0\(3),
      I1 => \p_1_reg_264_reg__0\(1),
      I2 => \p_1_reg_264_reg__0\(0),
      I3 => \p_1_reg_264_reg__0\(2),
      I4 => \p_1_reg_264_reg__0\(4),
      I5 => \p_1_reg_264_reg__0\(5),
      O => j_V_fu_315_p2(5)
    );
\p_1_reg_264[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \p_1_reg_264[8]_i_6_n_0\,
      I1 => \p_1_reg_264_reg__0\(6),
      O => j_V_fu_315_p2(6)
    );
\p_1_reg_264[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \p_1_reg_264[8]_i_6_n_0\,
      I1 => \p_1_reg_264_reg__0\(6),
      I2 => \p_1_reg_264_reg__0\(7),
      O => j_V_fu_315_p2(7)
    );
\p_1_reg_264[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => p_1_reg_2640,
      I1 => ap_CS_fsm_state2,
      I2 => \tmp_fu_297_p2__12\,
      O => p_1_reg_264
    );
\p_1_reg_264[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800080008000"
    )
        port map (
      I0 => ap_CS_fsm_state3,
      I1 => internal_full_n_reg_0,
      I2 => passthrough_data_str_2_full_n,
      I3 => input_data_data_stre_empty_n,
      I4 => \p_1_reg_264[8]_i_5_n_0\,
      I5 => \p_070_1_reg_275[10]_i_4_n_0\,
      O => p_1_reg_2640
    );
\p_1_reg_264[8]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \p_1_reg_264_reg__0\(6),
      I1 => \p_1_reg_264[8]_i_6_n_0\,
      I2 => \p_1_reg_264_reg__0\(7),
      I3 => \p_1_reg_264_reg__0\(8),
      O => j_V_fu_315_p2(8)
    );
\p_1_reg_264[8]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => \p_1_reg_264[8]_i_7_n_0\,
      I1 => \p_1_reg_264[8]_i_8_n_0\,
      I2 => p_s_reg_253(0),
      I3 => p_s_reg_253(1),
      I4 => p_s_reg_253(2),
      O => \tmp_fu_297_p2__12\
    );
\p_1_reg_264[8]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \p_1_reg_264_reg__0\(1),
      I1 => \p_1_reg_264_reg__0\(2),
      I2 => \p_1_reg_264_reg__0\(0),
      O => \p_1_reg_264[8]_i_5_n_0\
    );
\p_1_reg_264[8]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \p_1_reg_264_reg__0\(5),
      I1 => \p_1_reg_264_reg__0\(3),
      I2 => \p_1_reg_264_reg__0\(1),
      I3 => \p_1_reg_264_reg__0\(0),
      I4 => \p_1_reg_264_reg__0\(2),
      I5 => \p_1_reg_264_reg__0\(4),
      O => \p_1_reg_264[8]_i_6_n_0\
    );
\p_1_reg_264[8]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => p_s_reg_253(6),
      I1 => p_s_reg_253(5),
      I2 => p_s_reg_253(4),
      I3 => p_s_reg_253(3),
      O => \p_1_reg_264[8]_i_7_n_0\
    );
\p_1_reg_264[8]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => p_s_reg_253(9),
      I1 => p_s_reg_253(10),
      I2 => p_s_reg_253(8),
      I3 => p_s_reg_253(7),
      O => \p_1_reg_264[8]_i_8_n_0\
    );
\p_1_reg_264_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_1_reg_2640,
      D => j_V_fu_315_p2(0),
      Q => \p_1_reg_264_reg__0\(0),
      R => p_1_reg_264
    );
\p_1_reg_264_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_1_reg_2640,
      D => j_V_fu_315_p2(1),
      Q => \p_1_reg_264_reg__0\(1),
      R => p_1_reg_264
    );
\p_1_reg_264_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_1_reg_2640,
      D => j_V_fu_315_p2(2),
      Q => \p_1_reg_264_reg__0\(2),
      R => p_1_reg_264
    );
\p_1_reg_264_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_1_reg_2640,
      D => j_V_fu_315_p2(3),
      Q => \p_1_reg_264_reg__0\(3),
      R => p_1_reg_264
    );
\p_1_reg_264_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_1_reg_2640,
      D => j_V_fu_315_p2(4),
      Q => \p_1_reg_264_reg__0\(4),
      R => p_1_reg_264
    );
\p_1_reg_264_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_1_reg_2640,
      D => j_V_fu_315_p2(5),
      Q => \p_1_reg_264_reg__0\(5),
      R => p_1_reg_264
    );
\p_1_reg_264_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_1_reg_2640,
      D => j_V_fu_315_p2(6),
      Q => \p_1_reg_264_reg__0\(6),
      R => p_1_reg_264
    );
\p_1_reg_264_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_1_reg_2640,
      D => j_V_fu_315_p2(7),
      Q => \p_1_reg_264_reg__0\(7),
      R => p_1_reg_264
    );
\p_1_reg_264_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_1_reg_2640,
      D => j_V_fu_315_p2(8),
      Q => \p_1_reg_264_reg__0\(8),
      R => p_1_reg_264
    );
\p_s_reg_253[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F8000000"
    )
        port map (
      I0 => start_for_Mat2AXIvideo_U0_full_n,
      I1 => start_for_Mat2AXIvideo_1_U0_full_n,
      I2 => \^start_once_reg_reg_0\,
      I3 => start_for_Loop_loop_height_pro_U0_empty_n,
      I4 => \^q\(0),
      I5 => ap_NS_fsm1,
      O => p_s_reg_253_0
    );
\p_s_reg_253[10]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00200000"
    )
        port map (
      I0 => \p_s_reg_253[10]_i_3_n_0\,
      I1 => \p_070_2_reg_286_reg__0\(0),
      I2 => \p_070_2_reg_286_reg__0\(2),
      I3 => \p_070_2_reg_286_reg__0\(1),
      I4 => ap_CS_fsm_state5,
      O => ap_NS_fsm1
    );
\p_s_reg_253[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010000000000000"
    )
        port map (
      I0 => \p_070_2_reg_286_reg__0\(3),
      I1 => \p_070_2_reg_286_reg__0\(4),
      I2 => \p_070_2_reg_286_reg__0\(5),
      I3 => \p_070_2_reg_286_reg__0\(6),
      I4 => \p_070_2_reg_286_reg__0\(8),
      I5 => \p_070_2_reg_286_reg__0\(7),
      O => \p_s_reg_253[10]_i_3_n_0\
    );
\p_s_reg_253_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(0),
      Q => p_s_reg_253(0),
      R => p_s_reg_253_0
    );
\p_s_reg_253_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(10),
      Q => p_s_reg_253(10),
      R => p_s_reg_253_0
    );
\p_s_reg_253_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(1),
      Q => p_s_reg_253(1),
      R => p_s_reg_253_0
    );
\p_s_reg_253_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(2),
      Q => p_s_reg_253(2),
      R => p_s_reg_253_0
    );
\p_s_reg_253_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(3),
      Q => p_s_reg_253(3),
      R => p_s_reg_253_0
    );
\p_s_reg_253_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(4),
      Q => p_s_reg_253(4),
      R => p_s_reg_253_0
    );
\p_s_reg_253_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(5),
      Q => p_s_reg_253(5),
      R => p_s_reg_253_0
    );
\p_s_reg_253_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(6),
      Q => p_s_reg_253(6),
      R => p_s_reg_253_0
    );
\p_s_reg_253_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(7),
      Q => p_s_reg_253(7),
      R => p_s_reg_253_0
    );
\p_s_reg_253_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(8),
      Q => p_s_reg_253(8),
      R => p_s_reg_253_0
    );
\p_s_reg_253_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm1,
      D => i_V_reg_348(9),
      Q => p_s_reg_253(9),
      R => p_s_reg_253_0
    );
start_once_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000ECCC"
    )
        port map (
      I0 => start_for_Loop_loop_height_pro_U0_empty_n,
      I1 => \^start_once_reg_reg_0\,
      I2 => start_for_Mat2AXIvideo_1_U0_full_n,
      I3 => start_for_Mat2AXIvideo_U0_full_n,
      I4 => \^loop_loop_height_pro_u0_ap_ready\,
      O => start_once_reg_i_1_n_0
    );
start_once_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => start_once_reg_i_1_n_0,
      Q => \^start_once_reg_reg_0\,
      R => ap_rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo is
  port (
    image_out_TVALID : out STD_LOGIC;
    Mat2AXIvideo_U0_ap_ready : out STD_LOGIC;
    Mat2AXIvideo_U0_img_data_stream_2_V_read : out STD_LOGIC;
    ap_done_reg_reg_0 : out STD_LOGIC;
    ap_done : out STD_LOGIC;
    ap_idle : out STD_LOGIC;
    image_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    image_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    image_out_TDATA : out STD_LOGIC_VECTOR ( 23 downto 0 );
    ap_rst : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    start_for_Mat2AXIvideo_U0_empty_n : in STD_LOGIC;
    image_out_TREADY : in STD_LOGIC;
    passthrough_data_str_empty_n : in STD_LOGIC;
    passthrough_data_str_2_empty_n : in STD_LOGIC;
    passthrough_data_str_1_empty_n : in STD_LOGIC;
    ap_done_reg : in STD_LOGIC;
    Mat2AXIvideo_1_U0_ap_ready : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    start_for_Mat2AXIvideo_1_U0_empty_n : in STD_LOGIC;
    \ap_CS_fsm_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \ap_CS_fsm_reg[0]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 23 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo is
  signal AXI_video_strm_V_data_V_1_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_load_A : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_load_B : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_payload_A : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal AXI_video_strm_V_data_V_1_payload_B : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal AXI_video_strm_V_data_V_1_sel : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_sel_rd_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_sel_wr_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_data_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal AXI_video_strm_V_dest_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_dest_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\ : STD_LOGIC;
  signal AXI_video_strm_V_id_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_id_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_id_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\ : STD_LOGIC;
  signal AXI_video_strm_V_keep_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_keep_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_keep_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1]\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_payload_A : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_payload_B : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_sel : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_sel_rd_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_sel_wr_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_last_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal AXI_video_strm_V_strb_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_strb_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_strb_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1]\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_payload_A : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_payload_B : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_sel : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_sel_rd_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_sel_wr_i_1_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_user_V_1_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \^mat2axivideo_u0_ap_ready\ : STD_LOGIC;
  signal \^mat2axivideo_u0_img_data_stream_2_v_read\ : STD_LOGIC;
  signal \ap_CS_fsm[2]_i_2__1_n_0\ : STD_LOGIC;
  signal \ap_CS_fsm[2]_i_3_n_0\ : STD_LOGIC;
  signal \ap_CS_fsm[3]_i_4_n_0\ : STD_LOGIC;
  signal ap_CS_fsm_pp0_stage0 : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_0_[0]\ : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state6 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ap_NS_fsm1 : STD_LOGIC;
  signal ap_NS_fsm112_out : STD_LOGIC;
  signal ap_block_pp0_stage0_subdone : STD_LOGIC;
  signal ap_done_INST_0_i_3_n_0 : STD_LOGIC;
  signal ap_done_INST_0_i_7_n_0 : STD_LOGIC;
  signal ap_done_INST_0_i_8_n_0 : STD_LOGIC;
  signal ap_done_reg_0 : STD_LOGIC;
  signal ap_done_reg_i_1_n_0 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter0 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter0_i_1_n_0 : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter1__0\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1_i_1_n_0 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1_reg_n_0 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter2_i_1_n_0 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter2_reg_n_0 : STD_LOGIC;
  signal axi_last_V_reg_2750 : STD_LOGIC;
  signal \axi_last_V_reg_275[0]_i_1_n_0\ : STD_LOGIC;
  signal \axi_last_V_reg_275[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_last_V_reg_275[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_last_V_reg_275_reg_n_0_[0]\ : STD_LOGIC;
  signal \exitcond1_fu_200_p2__13\ : STD_LOGIC;
  signal exitcond_fu_212_p2 : STD_LOGIC;
  signal \exitcond_reg_266[0]_i_1_n_0\ : STD_LOGIC;
  signal \exitcond_reg_266[0]_i_3_n_0\ : STD_LOGIC;
  signal \exitcond_reg_266[0]_i_4_n_0\ : STD_LOGIC;
  signal exitcond_reg_266_pp0_iter1_reg : STD_LOGIC;
  signal \exitcond_reg_266_pp0_iter1_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \exitcond_reg_266_reg_n_0_[0]\ : STD_LOGIC;
  signal i_V_fu_206_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal i_V_reg_261 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal i_V_reg_2610 : STD_LOGIC;
  signal \i_V_reg_261[10]_i_3_n_0\ : STD_LOGIC;
  signal \^image_out_tvalid\ : STD_LOGIC;
  signal j_V_fu_218_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal t_V_1_reg_184 : STD_LOGIC;
  signal t_V_1_reg_1840 : STD_LOGIC;
  signal \t_V_1_reg_184[10]_i_5_n_0\ : STD_LOGIC;
  signal \t_V_1_reg_184_reg__0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal t_V_reg_173 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal tmp_user_V_fu_122 : STD_LOGIC;
  signal \tmp_user_V_fu_122[0]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of AXI_video_strm_V_data_V_1_sel_rd_i_1 : label is "soft_lutpair124";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_data_V_1_state[1]_i_1\ : label is "soft_lutpair124";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_dest_V_1_state[0]_i_2\ : label is "soft_lutpair122";
  attribute SOFT_HLUTNM of AXI_video_strm_V_last_V_1_sel_rd_i_1 : label is "soft_lutpair126";
  attribute SOFT_HLUTNM of AXI_video_strm_V_last_V_1_sel_wr_i_1 : label is "soft_lutpair129";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_last_V_1_state[1]_i_1\ : label is "soft_lutpair126";
  attribute SOFT_HLUTNM of AXI_video_strm_V_user_V_1_sel_rd_i_1 : label is "soft_lutpair127";
  attribute SOFT_HLUTNM of AXI_video_strm_V_user_V_1_sel_wr_i_1 : label is "soft_lutpair129";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_user_V_1_state[1]_i_1\ : label is "soft_lutpair127";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_3\ : label is "soft_lutpair117";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute SOFT_HLUTNM of ap_done_INST_0_i_4 : label is "soft_lutpair113";
  attribute SOFT_HLUTNM of ap_done_INST_0_i_7 : label is "soft_lutpair125";
  attribute SOFT_HLUTNM of ap_done_reg_i_1 : label is "soft_lutpair114";
  attribute SOFT_HLUTNM of \ap_done_reg_i_1__0\ : label is "soft_lutpair114";
  attribute SOFT_HLUTNM of ap_enable_reg_pp0_iter2_i_1 : label is "soft_lutpair117";
  attribute SOFT_HLUTNM of \axi_last_V_reg_275[0]_i_2\ : label is "soft_lutpair120";
  attribute SOFT_HLUTNM of \axi_last_V_reg_275[0]_i_3\ : label is "soft_lutpair123";
  attribute SOFT_HLUTNM of \exitcond_reg_266[0]_i_1\ : label is "soft_lutpair121";
  attribute SOFT_HLUTNM of \exitcond_reg_266[0]_i_2\ : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of \exitcond_reg_266[0]_i_3\ : label is "soft_lutpair123";
  attribute SOFT_HLUTNM of \exitcond_reg_266[0]_i_4\ : label is "soft_lutpair120";
  attribute SOFT_HLUTNM of \exitcond_reg_266_pp0_iter1_reg[0]_i_1\ : label is "soft_lutpair122";
  attribute SOFT_HLUTNM of \i_V_reg_261[0]_i_1\ : label is "soft_lutpair143";
  attribute SOFT_HLUTNM of \i_V_reg_261[1]_i_1\ : label is "soft_lutpair143";
  attribute SOFT_HLUTNM of \i_V_reg_261[2]_i_1\ : label is "soft_lutpair113";
  attribute SOFT_HLUTNM of \i_V_reg_261[3]_i_1\ : label is "soft_lutpair115";
  attribute SOFT_HLUTNM of \i_V_reg_261[4]_i_1\ : label is "soft_lutpair115";
  attribute SOFT_HLUTNM of \i_V_reg_261[6]_i_1\ : label is "soft_lutpair125";
  attribute SOFT_HLUTNM of \i_V_reg_261[8]_i_1\ : label is "soft_lutpair119";
  attribute SOFT_HLUTNM of \i_V_reg_261[9]_i_1\ : label is "soft_lutpair119";
  attribute SOFT_HLUTNM of \image_out_TDATA[0]_INST_0\ : label is "soft_lutpair130";
  attribute SOFT_HLUTNM of \image_out_TDATA[10]_INST_0\ : label is "soft_lutpair136";
  attribute SOFT_HLUTNM of \image_out_TDATA[11]_INST_0\ : label is "soft_lutpair137";
  attribute SOFT_HLUTNM of \image_out_TDATA[12]_INST_0\ : label is "soft_lutpair138";
  attribute SOFT_HLUTNM of \image_out_TDATA[13]_INST_0\ : label is "soft_lutpair137";
  attribute SOFT_HLUTNM of \image_out_TDATA[14]_INST_0\ : label is "soft_lutpair138";
  attribute SOFT_HLUTNM of \image_out_TDATA[15]_INST_0\ : label is "soft_lutpair139";
  attribute SOFT_HLUTNM of \image_out_TDATA[16]_INST_0\ : label is "soft_lutpair139";
  attribute SOFT_HLUTNM of \image_out_TDATA[17]_INST_0\ : label is "soft_lutpair140";
  attribute SOFT_HLUTNM of \image_out_TDATA[18]_INST_0\ : label is "soft_lutpair141";
  attribute SOFT_HLUTNM of \image_out_TDATA[19]_INST_0\ : label is "soft_lutpair141";
  attribute SOFT_HLUTNM of \image_out_TDATA[1]_INST_0\ : label is "soft_lutpair131";
  attribute SOFT_HLUTNM of \image_out_TDATA[20]_INST_0\ : label is "soft_lutpair140";
  attribute SOFT_HLUTNM of \image_out_TDATA[21]_INST_0\ : label is "soft_lutpair134";
  attribute SOFT_HLUTNM of \image_out_TDATA[22]_INST_0\ : label is "soft_lutpair132";
  attribute SOFT_HLUTNM of \image_out_TDATA[23]_INST_0\ : label is "soft_lutpair131";
  attribute SOFT_HLUTNM of \image_out_TDATA[2]_INST_0\ : label is "soft_lutpair132";
  attribute SOFT_HLUTNM of \image_out_TDATA[3]_INST_0\ : label is "soft_lutpair133";
  attribute SOFT_HLUTNM of \image_out_TDATA[4]_INST_0\ : label is "soft_lutpair134";
  attribute SOFT_HLUTNM of \image_out_TDATA[5]_INST_0\ : label is "soft_lutpair133";
  attribute SOFT_HLUTNM of \image_out_TDATA[6]_INST_0\ : label is "soft_lutpair130";
  attribute SOFT_HLUTNM of \image_out_TDATA[7]_INST_0\ : label is "soft_lutpair135";
  attribute SOFT_HLUTNM of \image_out_TDATA[8]_INST_0\ : label is "soft_lutpair135";
  attribute SOFT_HLUTNM of \image_out_TDATA[9]_INST_0\ : label is "soft_lutpair136";
  attribute SOFT_HLUTNM of \t_V_1_reg_184[0]_i_1\ : label is "soft_lutpair142";
  attribute SOFT_HLUTNM of \t_V_1_reg_184[10]_i_4\ : label is "soft_lutpair121";
  attribute SOFT_HLUTNM of \t_V_1_reg_184[1]_i_1\ : label is "soft_lutpair142";
  attribute SOFT_HLUTNM of \t_V_1_reg_184[2]_i_1\ : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of \t_V_1_reg_184[3]_i_1\ : label is "soft_lutpair118";
  attribute SOFT_HLUTNM of \t_V_1_reg_184[4]_i_1\ : label is "soft_lutpair118";
  attribute SOFT_HLUTNM of \t_V_1_reg_184[6]_i_1\ : label is "soft_lutpair128";
  attribute SOFT_HLUTNM of \t_V_1_reg_184[7]_i_1\ : label is "soft_lutpair128";
  attribute SOFT_HLUTNM of \t_V_1_reg_184[8]_i_1\ : label is "soft_lutpair116";
  attribute SOFT_HLUTNM of \t_V_1_reg_184[9]_i_1\ : label is "soft_lutpair116";
begin
  Mat2AXIvideo_U0_ap_ready <= \^mat2axivideo_u0_ap_ready\;
  Mat2AXIvideo_U0_img_data_stream_2_V_read <= \^mat2axivideo_u0_img_data_stream_2_v_read\;
  image_out_TVALID <= \^image_out_tvalid\;
\AXI_video_strm_V_data_V_1_payload_A[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_data_V_1_ack_in,
      I2 => AXI_video_strm_V_data_V_1_sel_wr,
      O => AXI_video_strm_V_data_V_1_load_A
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(0),
      Q => AXI_video_strm_V_data_V_1_payload_A(0),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(10),
      Q => AXI_video_strm_V_data_V_1_payload_A(10),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(11),
      Q => AXI_video_strm_V_data_V_1_payload_A(11),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(12),
      Q => AXI_video_strm_V_data_V_1_payload_A(12),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(13),
      Q => AXI_video_strm_V_data_V_1_payload_A(13),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(14),
      Q => AXI_video_strm_V_data_V_1_payload_A(14),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(15),
      Q => AXI_video_strm_V_data_V_1_payload_A(15),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(16),
      Q => AXI_video_strm_V_data_V_1_payload_A(16),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(17),
      Q => AXI_video_strm_V_data_V_1_payload_A(17),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(18),
      Q => AXI_video_strm_V_data_V_1_payload_A(18),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(19),
      Q => AXI_video_strm_V_data_V_1_payload_A(19),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(1),
      Q => AXI_video_strm_V_data_V_1_payload_A(1),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(20),
      Q => AXI_video_strm_V_data_V_1_payload_A(20),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(21),
      Q => AXI_video_strm_V_data_V_1_payload_A(21),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(22),
      Q => AXI_video_strm_V_data_V_1_payload_A(22),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(23),
      Q => AXI_video_strm_V_data_V_1_payload_A(23),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(2),
      Q => AXI_video_strm_V_data_V_1_payload_A(2),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(3),
      Q => AXI_video_strm_V_data_V_1_payload_A(3),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(4),
      Q => AXI_video_strm_V_data_V_1_payload_A(4),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(5),
      Q => AXI_video_strm_V_data_V_1_payload_A(5),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(6),
      Q => AXI_video_strm_V_data_V_1_payload_A(6),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(7),
      Q => AXI_video_strm_V_data_V_1_payload_A(7),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(8),
      Q => AXI_video_strm_V_data_V_1_payload_A(8),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(9),
      Q => AXI_video_strm_V_data_V_1_payload_A(9),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_sel_wr,
      I1 => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_1_ack_in,
      O => AXI_video_strm_V_data_V_1_load_B
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(0),
      Q => AXI_video_strm_V_data_V_1_payload_B(0),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(10),
      Q => AXI_video_strm_V_data_V_1_payload_B(10),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(11),
      Q => AXI_video_strm_V_data_V_1_payload_B(11),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(12),
      Q => AXI_video_strm_V_data_V_1_payload_B(12),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(13),
      Q => AXI_video_strm_V_data_V_1_payload_B(13),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(14),
      Q => AXI_video_strm_V_data_V_1_payload_B(14),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(15),
      Q => AXI_video_strm_V_data_V_1_payload_B(15),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(16),
      Q => AXI_video_strm_V_data_V_1_payload_B(16),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(17),
      Q => AXI_video_strm_V_data_V_1_payload_B(17),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(18),
      Q => AXI_video_strm_V_data_V_1_payload_B(18),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(19),
      Q => AXI_video_strm_V_data_V_1_payload_B(19),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(1),
      Q => AXI_video_strm_V_data_V_1_payload_B(1),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(20),
      Q => AXI_video_strm_V_data_V_1_payload_B(20),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(21),
      Q => AXI_video_strm_V_data_V_1_payload_B(21),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(22),
      Q => AXI_video_strm_V_data_V_1_payload_B(22),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(23),
      Q => AXI_video_strm_V_data_V_1_payload_B(23),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(2),
      Q => AXI_video_strm_V_data_V_1_payload_B(2),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(3),
      Q => AXI_video_strm_V_data_V_1_payload_B(3),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(4),
      Q => AXI_video_strm_V_data_V_1_payload_B(4),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(5),
      Q => AXI_video_strm_V_data_V_1_payload_B(5),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(6),
      Q => AXI_video_strm_V_data_V_1_payload_B(6),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(7),
      Q => AXI_video_strm_V_data_V_1_payload_B(7),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(8),
      Q => AXI_video_strm_V_data_V_1_payload_B(8),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(9),
      Q => AXI_video_strm_V_data_V_1_payload_B(9),
      R => '0'
    );
AXI_video_strm_V_data_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => image_out_TREADY,
      I1 => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => AXI_video_strm_V_data_V_1_sel_rd_i_1_n_0
    );
AXI_video_strm_V_data_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_1_sel_rd_i_1_n_0,
      Q => AXI_video_strm_V_data_V_1_sel,
      R => ap_rst
    );
AXI_video_strm_V_data_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_ack_in,
      I1 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      I2 => AXI_video_strm_V_data_V_1_sel_wr,
      O => AXI_video_strm_V_data_V_1_sel_wr_i_1_n_0
    );
AXI_video_strm_V_data_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_1_sel_wr_i_1_n_0,
      Q => AXI_video_strm_V_data_V_1_sel_wr,
      R => ap_rst
    );
\AXI_video_strm_V_data_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_1_ack_in,
      I3 => image_out_TREADY,
      I4 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_data_V_1_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_data_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F5FD"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_data_V_1_ack_in,
      I2 => image_out_TREADY,
      I3 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_data_V_1_state(1)
    );
\AXI_video_strm_V_data_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_data_V_1_state[0]_i_1_n_0\,
      Q => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_1_state(1),
      Q => AXI_video_strm_V_data_V_1_ack_in,
      R => ap_rst
    );
\AXI_video_strm_V_dest_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA02A00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => image_out_TREADY,
      I2 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I3 => \^image_out_tvalid\,
      I4 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_dest_V_1_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_dest_V_1_state[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter1_reg_n_0,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \exitcond_reg_266_reg_n_0_[0]\,
      I3 => ap_block_pp0_stage0_subdone,
      O => \^mat2axivideo_u0_img_data_stream_2_v_read\
    );
\AXI_video_strm_V_dest_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AFEF"
    )
        port map (
      I0 => image_out_TREADY,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I2 => \^image_out_tvalid\,
      I3 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_dest_V_1_state(1)
    );
\AXI_video_strm_V_dest_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_dest_V_1_state[0]_i_1_n_0\,
      Q => \^image_out_tvalid\,
      R => '0'
    );
\AXI_video_strm_V_dest_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_dest_V_1_state(1),
      Q => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      R => ap_rst
    );
\AXI_video_strm_V_id_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA02A00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => image_out_TREADY,
      I2 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I3 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[0]\,
      I4 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_id_V_1_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_id_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AFEF"
    )
        port map (
      I0 => image_out_TREADY,
      I1 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I2 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[0]\,
      I3 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_id_V_1_state(1)
    );
\AXI_video_strm_V_id_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_id_V_1_state[0]_i_1_n_0\,
      Q => \AXI_video_strm_V_id_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_id_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_id_V_1_state(1),
      Q => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      R => ap_rst
    );
\AXI_video_strm_V_keep_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA02A00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => image_out_TREADY,
      I2 => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1]\,
      I3 => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[0]\,
      I4 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_keep_V_1_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_keep_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AFEF"
    )
        port map (
      I0 => image_out_TREADY,
      I1 => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1]\,
      I2 => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[0]\,
      I3 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_keep_V_1_state(1)
    );
\AXI_video_strm_V_keep_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_keep_V_1_state[0]_i_1_n_0\,
      Q => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_keep_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_keep_V_1_state(1),
      Q => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1]\,
      R => ap_rst
    );
\AXI_video_strm_V_last_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => \axi_last_V_reg_275_reg_n_0_[0]\,
      I1 => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_last_V_1_ack_in,
      I3 => AXI_video_strm_V_last_V_1_sel_wr,
      I4 => AXI_video_strm_V_last_V_1_payload_A,
      O => \AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_0\
    );
\AXI_video_strm_V_last_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_0\,
      Q => AXI_video_strm_V_last_V_1_payload_A,
      R => '0'
    );
\AXI_video_strm_V_last_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBFB8808"
    )
        port map (
      I0 => \axi_last_V_reg_275_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_last_V_1_sel_wr,
      I2 => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      I3 => AXI_video_strm_V_last_V_1_ack_in,
      I4 => AXI_video_strm_V_last_V_1_payload_B,
      O => \AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_0\
    );
\AXI_video_strm_V_last_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_0\,
      Q => AXI_video_strm_V_last_V_1_payload_B,
      R => '0'
    );
AXI_video_strm_V_last_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => image_out_TREADY,
      I1 => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_last_V_1_sel,
      O => AXI_video_strm_V_last_V_1_sel_rd_i_1_n_0
    );
AXI_video_strm_V_last_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_1_sel_rd_i_1_n_0,
      Q => AXI_video_strm_V_last_V_1_sel,
      R => ap_rst
    );
AXI_video_strm_V_last_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_1_ack_in,
      I1 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      I2 => AXI_video_strm_V_last_V_1_sel_wr,
      O => AXI_video_strm_V_last_V_1_sel_wr_i_1_n_0
    );
AXI_video_strm_V_last_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_1_sel_wr_i_1_n_0,
      Q => AXI_video_strm_V_last_V_1_sel_wr,
      R => ap_rst
    );
\AXI_video_strm_V_last_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_last_V_1_ack_in,
      I3 => image_out_TREADY,
      I4 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_last_V_1_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_last_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F5FD"
    )
        port map (
      I0 => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_last_V_1_ack_in,
      I2 => image_out_TREADY,
      I3 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_last_V_1_state(1)
    );
\AXI_video_strm_V_last_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_state[0]_i_1_n_0\,
      Q => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_last_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_1_state(1),
      Q => AXI_video_strm_V_last_V_1_ack_in,
      R => ap_rst
    );
\AXI_video_strm_V_strb_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA02A00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => image_out_TREADY,
      I2 => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1]\,
      I3 => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[0]\,
      I4 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_strb_V_1_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_strb_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AFEF"
    )
        port map (
      I0 => image_out_TREADY,
      I1 => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1]\,
      I2 => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[0]\,
      I3 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_strb_V_1_state(1)
    );
\AXI_video_strm_V_strb_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_strb_V_1_state[0]_i_1_n_0\,
      Q => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_strb_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_strb_V_1_state(1),
      Q => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1]\,
      R => ap_rst
    );
\AXI_video_strm_V_user_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => tmp_user_V_fu_122,
      I1 => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_user_V_1_ack_in,
      I3 => AXI_video_strm_V_user_V_1_sel_wr,
      I4 => AXI_video_strm_V_user_V_1_payload_A,
      O => \AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_0\
    );
\AXI_video_strm_V_user_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_0\,
      Q => AXI_video_strm_V_user_V_1_payload_A,
      R => '0'
    );
\AXI_video_strm_V_user_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBFB8808"
    )
        port map (
      I0 => tmp_user_V_fu_122,
      I1 => AXI_video_strm_V_user_V_1_sel_wr,
      I2 => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      I3 => AXI_video_strm_V_user_V_1_ack_in,
      I4 => AXI_video_strm_V_user_V_1_payload_B,
      O => \AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_0\
    );
\AXI_video_strm_V_user_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_0\,
      Q => AXI_video_strm_V_user_V_1_payload_B,
      R => '0'
    );
AXI_video_strm_V_user_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => image_out_TREADY,
      I1 => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_user_V_1_sel,
      O => AXI_video_strm_V_user_V_1_sel_rd_i_1_n_0
    );
AXI_video_strm_V_user_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_1_sel_rd_i_1_n_0,
      Q => AXI_video_strm_V_user_V_1_sel,
      R => ap_rst
    );
AXI_video_strm_V_user_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => AXI_video_strm_V_user_V_1_ack_in,
      I1 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      I2 => AXI_video_strm_V_user_V_1_sel_wr,
      O => AXI_video_strm_V_user_V_1_sel_wr_i_1_n_0
    );
AXI_video_strm_V_user_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_1_sel_wr_i_1_n_0,
      Q => AXI_video_strm_V_user_V_1_sel_wr,
      R => ap_rst
    );
\AXI_video_strm_V_user_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_user_V_1_ack_in,
      I3 => image_out_TREADY,
      I4 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_user_V_1_state[0]_i_1_n_0\
    );
\AXI_video_strm_V_user_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F5FD"
    )
        port map (
      I0 => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_user_V_1_ack_in,
      I2 => image_out_TREADY,
      I3 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_user_V_1_state(1)
    );
\AXI_video_strm_V_user_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_state[0]_i_1_n_0\,
      Q => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_user_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_1_state(1),
      Q => AXI_video_strm_V_user_V_1_ack_in,
      R => ap_rst
    );
\ap_CS_fsm[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF888F88"
    )
        port map (
      I0 => \^mat2axivideo_u0_ap_ready\,
      I1 => ap_CS_fsm_state2,
      I2 => start_for_Mat2AXIvideo_U0_empty_n,
      I3 => \ap_CS_fsm_reg_n_0_[0]\,
      I4 => ap_done_reg_0,
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[1]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAEAEAEAEAFFEAEA"
    )
        port map (
      I0 => ap_CS_fsm_state6,
      I1 => \ap_CS_fsm_reg_n_0_[0]\,
      I2 => ap_NS_fsm112_out,
      I3 => \^mat2axivideo_u0_ap_ready\,
      I4 => ap_CS_fsm_state2,
      I5 => ap_NS_fsm1,
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[1]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I2 => ap_CS_fsm_state2,
      I3 => AXI_video_strm_V_data_V_1_ack_in,
      I4 => ap_done_INST_0_i_3_n_0,
      I5 => \exitcond1_fu_200_p2__13\,
      O => ap_NS_fsm1
    );
\ap_CS_fsm[2]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBABBBAAAAAAAA"
    )
        port map (
      I0 => \ap_CS_fsm[2]_i_2__1_n_0\,
      I1 => \ap_CS_fsm[2]_i_3_n_0\,
      I2 => \ap_enable_reg_pp0_iter1__0\,
      I3 => ap_enable_reg_pp0_iter0,
      I4 => ap_enable_reg_pp0_iter1_reg_n_0,
      I5 => ap_CS_fsm_pp0_stage0,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm[2]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I2 => ap_CS_fsm_state2,
      I3 => AXI_video_strm_V_data_V_1_ack_in,
      I4 => ap_done_INST_0_i_3_n_0,
      I5 => \exitcond1_fu_200_p2__13\,
      O => \ap_CS_fsm[2]_i_2__1_n_0\
    );
\ap_CS_fsm[2]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter1_reg_n_0,
      I1 => ap_enable_reg_pp0_iter2_reg_n_0,
      I2 => ap_block_pp0_stage0_subdone,
      O => \ap_CS_fsm[2]_i_3_n_0\
    );
\ap_CS_fsm[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000200022222000"
    )
        port map (
      I0 => ap_CS_fsm_pp0_stage0,
      I1 => ap_enable_reg_pp0_iter1_reg_n_0,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => \ap_enable_reg_pp0_iter1__0\,
      I4 => ap_enable_reg_pp0_iter2_reg_n_0,
      I5 => ap_block_pp0_stage0_subdone,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => exitcond_fu_212_p2,
      I1 => ap_block_pp0_stage0_subdone,
      O => \ap_enable_reg_pp0_iter1__0\
    );
\ap_CS_fsm[3]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF04"
    )
        port map (
      I0 => exitcond_reg_266_pp0_iter1_reg,
      I1 => ap_enable_reg_pp0_iter2_reg_n_0,
      I2 => AXI_video_strm_V_data_V_1_ack_in,
      I3 => \ap_CS_fsm[3]_i_4_n_0\,
      O => ap_block_pp0_stage0_subdone
    );
\ap_CS_fsm[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"070F0F0F00000000"
    )
        port map (
      I0 => passthrough_data_str_empty_n,
      I1 => AXI_video_strm_V_data_V_1_ack_in,
      I2 => \exitcond_reg_266_reg_n_0_[0]\,
      I3 => passthrough_data_str_2_empty_n,
      I4 => passthrough_data_str_1_empty_n,
      I5 => ap_enable_reg_pp0_iter1_reg_n_0,
      O => \ap_CS_fsm[3]_i_4_n_0\
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \ap_CS_fsm_reg_n_0_[0]\,
      S => ap_rst
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => ap_rst
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_CS_fsm_pp0_stage0,
      R => ap_rst
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => ap_CS_fsm_state6,
      R => ap_rst
    );
ap_done_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EEE0"
    )
        port map (
      I0 => ap_done_reg_0,
      I1 => \^mat2axivideo_u0_ap_ready\,
      I2 => ap_done_reg,
      I3 => Mat2AXIvideo_1_U0_ap_ready,
      O => ap_done
    );
ap_done_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I2 => ap_CS_fsm_state2,
      I3 => AXI_video_strm_V_data_V_1_ack_in,
      I4 => ap_done_INST_0_i_3_n_0,
      I5 => \exitcond1_fu_200_p2__13\,
      O => \^mat2axivideo_u0_ap_ready\
    );
ap_done_INST_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1]\,
      I2 => AXI_video_strm_V_last_V_1_ack_in,
      I3 => AXI_video_strm_V_user_V_1_ack_in,
      O => ap_done_INST_0_i_3_n_0
    );
ap_done_INST_0_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => ap_done_INST_0_i_7_n_0,
      I1 => ap_done_INST_0_i_8_n_0,
      I2 => t_V_reg_173(0),
      I3 => t_V_reg_173(1),
      I4 => t_V_reg_173(2),
      O => \exitcond1_fu_200_p2__13\
    );
ap_done_INST_0_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => t_V_reg_173(6),
      I1 => t_V_reg_173(5),
      I2 => t_V_reg_173(4),
      I3 => t_V_reg_173(3),
      O => ap_done_INST_0_i_7_n_0
    );
ap_done_INST_0_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => t_V_reg_173(9),
      I1 => t_V_reg_173(10),
      I2 => t_V_reg_173(8),
      I3 => t_V_reg_173(7),
      O => ap_done_INST_0_i_8_n_0
    );
ap_done_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000A8"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_done_reg_0,
      I2 => \^mat2axivideo_u0_ap_ready\,
      I3 => ap_done_reg,
      I4 => Mat2AXIvideo_1_U0_ap_ready,
      O => ap_done_reg_i_1_n_0
    );
\ap_done_reg_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02020200"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_done_reg_0,
      I2 => \^mat2axivideo_u0_ap_ready\,
      I3 => ap_done_reg,
      I4 => Mat2AXIvideo_1_U0_ap_ready,
      O => ap_done_reg_reg_0
    );
ap_done_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_done_reg_i_1_n_0,
      Q => ap_done_reg_0,
      R => '0'
    );
ap_enable_reg_pp0_iter0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00E0E0E0"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter0,
      I1 => ap_NS_fsm1,
      I2 => ap_rst_n,
      I3 => \ap_enable_reg_pp0_iter1__0\,
      I4 => ap_CS_fsm_pp0_stage0,
      O => ap_enable_reg_pp0_iter0_i_1_n_0
    );
ap_enable_reg_pp0_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter0_i_1_n_0,
      Q => ap_enable_reg_pp0_iter0,
      R => '0'
    );
ap_enable_reg_pp0_iter1_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008A80"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_enable_reg_pp0_iter1_reg_n_0,
      I2 => ap_block_pp0_stage0_subdone,
      I3 => ap_enable_reg_pp0_iter0,
      I4 => \ap_enable_reg_pp0_iter1__0\,
      O => ap_enable_reg_pp0_iter1_i_1_n_0
    );
ap_enable_reg_pp0_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter1_i_1_n_0,
      Q => ap_enable_reg_pp0_iter1_reg_n_0,
      R => '0'
    );
ap_enable_reg_pp0_iter2_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00C0A0A0"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter1_reg_n_0,
      I1 => ap_enable_reg_pp0_iter2_reg_n_0,
      I2 => ap_rst_n,
      I3 => ap_NS_fsm1,
      I4 => ap_block_pp0_stage0_subdone,
      O => ap_enable_reg_pp0_iter2_i_1_n_0
    );
ap_enable_reg_pp0_iter2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter2_i_1_n_0,
      Q => ap_enable_reg_pp0_iter2_reg_n_0,
      R => '0'
    );
ap_idle_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => \ap_CS_fsm_reg_n_0_[0]\,
      I1 => start_for_Mat2AXIvideo_U0_empty_n,
      I2 => Q(0),
      I3 => start_for_Mat2AXIvideo_1_U0_empty_n,
      I4 => \ap_CS_fsm_reg[0]_0\(0),
      I5 => \ap_CS_fsm_reg[0]_1\(0),
      O => ap_idle
    );
\axi_last_V_reg_275[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000FFFF80000000"
    )
        port map (
      I0 => \axi_last_V_reg_275[0]_i_2_n_0\,
      I1 => \t_V_1_reg_184_reg__0\(0),
      I2 => \t_V_1_reg_184_reg__0\(1),
      I3 => \t_V_1_reg_184_reg__0\(2),
      I4 => axi_last_V_reg_2750,
      I5 => \axi_last_V_reg_275_reg_n_0_[0]\,
      O => \axi_last_V_reg_275[0]_i_1_n_0\
    );
\axi_last_V_reg_275[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(8),
      I1 => \t_V_1_reg_184_reg__0\(7),
      I2 => \t_V_1_reg_184_reg__0\(9),
      I3 => \t_V_1_reg_184_reg__0\(10),
      I4 => \axi_last_V_reg_275[0]_i_3_n_0\,
      O => \axi_last_V_reg_275[0]_i_2_n_0\
    );
\axi_last_V_reg_275[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(6),
      I1 => \t_V_1_reg_184_reg__0\(5),
      I2 => \t_V_1_reg_184_reg__0\(4),
      I3 => \t_V_1_reg_184_reg__0\(3),
      O => \axi_last_V_reg_275[0]_i_3_n_0\
    );
\axi_last_V_reg_275_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \axi_last_V_reg_275[0]_i_1_n_0\,
      Q => \axi_last_V_reg_275_reg_n_0_[0]\,
      R => '0'
    );
\exitcond_reg_266[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => exitcond_fu_212_p2,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => ap_block_pp0_stage0_subdone,
      I3 => \exitcond_reg_266_reg_n_0_[0]\,
      O => \exitcond_reg_266[0]_i_1_n_0\
    );
\exitcond_reg_266[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => \exitcond_reg_266[0]_i_3_n_0\,
      I1 => \exitcond_reg_266[0]_i_4_n_0\,
      I2 => \t_V_1_reg_184_reg__0\(0),
      I3 => \t_V_1_reg_184_reg__0\(1),
      I4 => \t_V_1_reg_184_reg__0\(2),
      O => exitcond_fu_212_p2
    );
\exitcond_reg_266[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(6),
      I1 => \t_V_1_reg_184_reg__0\(5),
      I2 => \t_V_1_reg_184_reg__0\(4),
      I3 => \t_V_1_reg_184_reg__0\(3),
      O => \exitcond_reg_266[0]_i_3_n_0\
    );
\exitcond_reg_266[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(10),
      I1 => \t_V_1_reg_184_reg__0\(9),
      I2 => \t_V_1_reg_184_reg__0\(8),
      I3 => \t_V_1_reg_184_reg__0\(7),
      O => \exitcond_reg_266[0]_i_4_n_0\
    );
\exitcond_reg_266_pp0_iter1_reg[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \exitcond_reg_266_reg_n_0_[0]\,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => ap_block_pp0_stage0_subdone,
      I3 => exitcond_reg_266_pp0_iter1_reg,
      O => \exitcond_reg_266_pp0_iter1_reg[0]_i_1_n_0\
    );
\exitcond_reg_266_pp0_iter1_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \exitcond_reg_266_pp0_iter1_reg[0]_i_1_n_0\,
      Q => exitcond_reg_266_pp0_iter1_reg,
      R => '0'
    );
\exitcond_reg_266_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \exitcond_reg_266[0]_i_1_n_0\,
      Q => \exitcond_reg_266_reg_n_0_[0]\,
      R => '0'
    );
\i_V_reg_261[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => t_V_reg_173(0),
      O => i_V_fu_206_p2(0)
    );
\i_V_reg_261[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I2 => ap_CS_fsm_state2,
      I3 => AXI_video_strm_V_data_V_1_ack_in,
      I4 => ap_done_INST_0_i_3_n_0,
      O => i_V_reg_2610
    );
\i_V_reg_261[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => t_V_reg_173(8),
      I1 => t_V_reg_173(6),
      I2 => \i_V_reg_261[10]_i_3_n_0\,
      I3 => t_V_reg_173(7),
      I4 => t_V_reg_173(9),
      I5 => t_V_reg_173(10),
      O => i_V_fu_206_p2(10)
    );
\i_V_reg_261[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => t_V_reg_173(5),
      I1 => t_V_reg_173(3),
      I2 => t_V_reg_173(1),
      I3 => t_V_reg_173(0),
      I4 => t_V_reg_173(2),
      I5 => t_V_reg_173(4),
      O => \i_V_reg_261[10]_i_3_n_0\
    );
\i_V_reg_261[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => t_V_reg_173(0),
      I1 => t_V_reg_173(1),
      O => i_V_fu_206_p2(1)
    );
\i_V_reg_261[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => t_V_reg_173(0),
      I1 => t_V_reg_173(1),
      I2 => t_V_reg_173(2),
      O => i_V_fu_206_p2(2)
    );
\i_V_reg_261[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => t_V_reg_173(1),
      I1 => t_V_reg_173(0),
      I2 => t_V_reg_173(2),
      I3 => t_V_reg_173(3),
      O => i_V_fu_206_p2(3)
    );
\i_V_reg_261[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => t_V_reg_173(2),
      I1 => t_V_reg_173(0),
      I2 => t_V_reg_173(1),
      I3 => t_V_reg_173(3),
      I4 => t_V_reg_173(4),
      O => i_V_fu_206_p2(4)
    );
\i_V_reg_261[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => t_V_reg_173(3),
      I1 => t_V_reg_173(1),
      I2 => t_V_reg_173(0),
      I3 => t_V_reg_173(2),
      I4 => t_V_reg_173(4),
      I5 => t_V_reg_173(5),
      O => i_V_fu_206_p2(5)
    );
\i_V_reg_261[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_V_reg_261[10]_i_3_n_0\,
      I1 => t_V_reg_173(6),
      O => i_V_fu_206_p2(6)
    );
\i_V_reg_261[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \i_V_reg_261[10]_i_3_n_0\,
      I1 => t_V_reg_173(6),
      I2 => t_V_reg_173(7),
      O => i_V_fu_206_p2(7)
    );
\i_V_reg_261[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => t_V_reg_173(6),
      I1 => \i_V_reg_261[10]_i_3_n_0\,
      I2 => t_V_reg_173(7),
      I3 => t_V_reg_173(8),
      O => i_V_fu_206_p2(8)
    );
\i_V_reg_261[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => t_V_reg_173(7),
      I1 => \i_V_reg_261[10]_i_3_n_0\,
      I2 => t_V_reg_173(6),
      I3 => t_V_reg_173(8),
      I4 => t_V_reg_173(9),
      O => i_V_fu_206_p2(9)
    );
\i_V_reg_261_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(0),
      Q => i_V_reg_261(0),
      R => '0'
    );
\i_V_reg_261_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(10),
      Q => i_V_reg_261(10),
      R => '0'
    );
\i_V_reg_261_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(1),
      Q => i_V_reg_261(1),
      R => '0'
    );
\i_V_reg_261_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(2),
      Q => i_V_reg_261(2),
      R => '0'
    );
\i_V_reg_261_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(3),
      Q => i_V_reg_261(3),
      R => '0'
    );
\i_V_reg_261_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(4),
      Q => i_V_reg_261(4),
      R => '0'
    );
\i_V_reg_261_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(5),
      Q => i_V_reg_261(5),
      R => '0'
    );
\i_V_reg_261_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(6),
      Q => i_V_reg_261(6),
      R => '0'
    );
\i_V_reg_261_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(7),
      Q => i_V_reg_261(7),
      R => '0'
    );
\i_V_reg_261_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(8),
      Q => i_V_reg_261(8),
      R => '0'
    );
\i_V_reg_261_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2610,
      D => i_V_fu_206_p2(9),
      Q => i_V_reg_261(9),
      R => '0'
    );
\image_out_TDATA[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(0),
      I1 => AXI_video_strm_V_data_V_1_payload_A(0),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(0)
    );
\image_out_TDATA[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(10),
      I1 => AXI_video_strm_V_data_V_1_payload_A(10),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(10)
    );
\image_out_TDATA[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(11),
      I1 => AXI_video_strm_V_data_V_1_payload_A(11),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(11)
    );
\image_out_TDATA[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(12),
      I1 => AXI_video_strm_V_data_V_1_payload_A(12),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(12)
    );
\image_out_TDATA[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(13),
      I1 => AXI_video_strm_V_data_V_1_payload_A(13),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(13)
    );
\image_out_TDATA[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(14),
      I1 => AXI_video_strm_V_data_V_1_payload_A(14),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(14)
    );
\image_out_TDATA[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(15),
      I1 => AXI_video_strm_V_data_V_1_payload_A(15),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(15)
    );
\image_out_TDATA[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(16),
      I1 => AXI_video_strm_V_data_V_1_payload_A(16),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(16)
    );
\image_out_TDATA[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(17),
      I1 => AXI_video_strm_V_data_V_1_payload_A(17),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(17)
    );
\image_out_TDATA[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(18),
      I1 => AXI_video_strm_V_data_V_1_payload_A(18),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(18)
    );
\image_out_TDATA[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(19),
      I1 => AXI_video_strm_V_data_V_1_payload_A(19),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(19)
    );
\image_out_TDATA[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(1),
      I1 => AXI_video_strm_V_data_V_1_payload_A(1),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(1)
    );
\image_out_TDATA[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(20),
      I1 => AXI_video_strm_V_data_V_1_payload_A(20),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(20)
    );
\image_out_TDATA[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(21),
      I1 => AXI_video_strm_V_data_V_1_payload_A(21),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(21)
    );
\image_out_TDATA[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(22),
      I1 => AXI_video_strm_V_data_V_1_payload_A(22),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(22)
    );
\image_out_TDATA[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(23),
      I1 => AXI_video_strm_V_data_V_1_payload_A(23),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(23)
    );
\image_out_TDATA[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(2),
      I1 => AXI_video_strm_V_data_V_1_payload_A(2),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(2)
    );
\image_out_TDATA[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(3),
      I1 => AXI_video_strm_V_data_V_1_payload_A(3),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(3)
    );
\image_out_TDATA[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(4),
      I1 => AXI_video_strm_V_data_V_1_payload_A(4),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(4)
    );
\image_out_TDATA[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(5),
      I1 => AXI_video_strm_V_data_V_1_payload_A(5),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(5)
    );
\image_out_TDATA[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(6),
      I1 => AXI_video_strm_V_data_V_1_payload_A(6),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(6)
    );
\image_out_TDATA[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(7),
      I1 => AXI_video_strm_V_data_V_1_payload_A(7),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(7)
    );
\image_out_TDATA[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(8),
      I1 => AXI_video_strm_V_data_V_1_payload_A(8),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(8)
    );
\image_out_TDATA[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(9),
      I1 => AXI_video_strm_V_data_V_1_payload_A(9),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => image_out_TDATA(9)
    );
\image_out_TLAST[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_1_payload_B,
      I1 => AXI_video_strm_V_last_V_1_sel,
      I2 => AXI_video_strm_V_last_V_1_payload_A,
      O => image_out_TLAST(0)
    );
\image_out_TUSER[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_user_V_1_payload_B,
      I1 => AXI_video_strm_V_user_V_1_sel,
      I2 => AXI_video_strm_V_user_V_1_payload_A,
      O => image_out_TUSER(0)
    );
\t_V_1_reg_184[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(0),
      O => j_V_fu_218_p2(0)
    );
\t_V_1_reg_184[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => axi_last_V_reg_2750,
      I2 => ap_enable_reg_pp0_iter0,
      O => t_V_1_reg_184
    );
\t_V_1_reg_184[10]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter0,
      I1 => axi_last_V_reg_2750,
      O => t_V_1_reg_1840
    );
\t_V_1_reg_184[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(8),
      I1 => \t_V_1_reg_184_reg__0\(6),
      I2 => \t_V_1_reg_184[10]_i_5_n_0\,
      I3 => \t_V_1_reg_184_reg__0\(7),
      I4 => \t_V_1_reg_184_reg__0\(9),
      I5 => \t_V_1_reg_184_reg__0\(10),
      O => j_V_fu_218_p2(10)
    );
\t_V_1_reg_184[10]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => exitcond_fu_212_p2,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => ap_block_pp0_stage0_subdone,
      O => axi_last_V_reg_2750
    );
\t_V_1_reg_184[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(5),
      I1 => \t_V_1_reg_184_reg__0\(3),
      I2 => \t_V_1_reg_184_reg__0\(1),
      I3 => \t_V_1_reg_184_reg__0\(0),
      I4 => \t_V_1_reg_184_reg__0\(2),
      I5 => \t_V_1_reg_184_reg__0\(4),
      O => \t_V_1_reg_184[10]_i_5_n_0\
    );
\t_V_1_reg_184[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(0),
      I1 => \t_V_1_reg_184_reg__0\(1),
      O => j_V_fu_218_p2(1)
    );
\t_V_1_reg_184[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(0),
      I1 => \t_V_1_reg_184_reg__0\(1),
      I2 => \t_V_1_reg_184_reg__0\(2),
      O => j_V_fu_218_p2(2)
    );
\t_V_1_reg_184[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(1),
      I1 => \t_V_1_reg_184_reg__0\(0),
      I2 => \t_V_1_reg_184_reg__0\(2),
      I3 => \t_V_1_reg_184_reg__0\(3),
      O => j_V_fu_218_p2(3)
    );
\t_V_1_reg_184[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(2),
      I1 => \t_V_1_reg_184_reg__0\(0),
      I2 => \t_V_1_reg_184_reg__0\(1),
      I3 => \t_V_1_reg_184_reg__0\(3),
      I4 => \t_V_1_reg_184_reg__0\(4),
      O => j_V_fu_218_p2(4)
    );
\t_V_1_reg_184[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(3),
      I1 => \t_V_1_reg_184_reg__0\(1),
      I2 => \t_V_1_reg_184_reg__0\(0),
      I3 => \t_V_1_reg_184_reg__0\(2),
      I4 => \t_V_1_reg_184_reg__0\(4),
      I5 => \t_V_1_reg_184_reg__0\(5),
      O => j_V_fu_218_p2(5)
    );
\t_V_1_reg_184[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_1_reg_184[10]_i_5_n_0\,
      I1 => \t_V_1_reg_184_reg__0\(6),
      O => j_V_fu_218_p2(6)
    );
\t_V_1_reg_184[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \t_V_1_reg_184[10]_i_5_n_0\,
      I1 => \t_V_1_reg_184_reg__0\(6),
      I2 => \t_V_1_reg_184_reg__0\(7),
      O => j_V_fu_218_p2(7)
    );
\t_V_1_reg_184[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(6),
      I1 => \t_V_1_reg_184[10]_i_5_n_0\,
      I2 => \t_V_1_reg_184_reg__0\(7),
      I3 => \t_V_1_reg_184_reg__0\(8),
      O => j_V_fu_218_p2(8)
    );
\t_V_1_reg_184[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \t_V_1_reg_184_reg__0\(7),
      I1 => \t_V_1_reg_184[10]_i_5_n_0\,
      I2 => \t_V_1_reg_184_reg__0\(6),
      I3 => \t_V_1_reg_184_reg__0\(8),
      I4 => \t_V_1_reg_184_reg__0\(9),
      O => j_V_fu_218_p2(9)
    );
\t_V_1_reg_184_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(0),
      Q => \t_V_1_reg_184_reg__0\(0),
      R => t_V_1_reg_184
    );
\t_V_1_reg_184_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(10),
      Q => \t_V_1_reg_184_reg__0\(10),
      R => t_V_1_reg_184
    );
\t_V_1_reg_184_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(1),
      Q => \t_V_1_reg_184_reg__0\(1),
      R => t_V_1_reg_184
    );
\t_V_1_reg_184_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(2),
      Q => \t_V_1_reg_184_reg__0\(2),
      R => t_V_1_reg_184
    );
\t_V_1_reg_184_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(3),
      Q => \t_V_1_reg_184_reg__0\(3),
      R => t_V_1_reg_184
    );
\t_V_1_reg_184_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(4),
      Q => \t_V_1_reg_184_reg__0\(4),
      R => t_V_1_reg_184
    );
\t_V_1_reg_184_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(5),
      Q => \t_V_1_reg_184_reg__0\(5),
      R => t_V_1_reg_184
    );
\t_V_1_reg_184_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(6),
      Q => \t_V_1_reg_184_reg__0\(6),
      R => t_V_1_reg_184
    );
\t_V_1_reg_184_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(7),
      Q => \t_V_1_reg_184_reg__0\(7),
      R => t_V_1_reg_184
    );
\t_V_1_reg_184_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(8),
      Q => \t_V_1_reg_184_reg__0\(8),
      R => t_V_1_reg_184
    );
\t_V_1_reg_184_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1840,
      D => j_V_fu_218_p2(9),
      Q => \t_V_1_reg_184_reg__0\(9),
      R => t_V_1_reg_184
    );
\t_V_reg_173[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => start_for_Mat2AXIvideo_U0_empty_n,
      I1 => \ap_CS_fsm_reg_n_0_[0]\,
      I2 => ap_done_reg_0,
      O => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(0),
      Q => t_V_reg_173(0),
      R => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(10),
      Q => t_V_reg_173(10),
      R => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(1),
      Q => t_V_reg_173(1),
      R => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(2),
      Q => t_V_reg_173(2),
      R => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(3),
      Q => t_V_reg_173(3),
      R => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(4),
      Q => t_V_reg_173(4),
      R => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(5),
      Q => t_V_reg_173(5),
      R => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(6),
      Q => t_V_reg_173(6),
      R => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(7),
      Q => t_V_reg_173(7),
      R => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(8),
      Q => t_V_reg_173(8),
      R => ap_NS_fsm112_out
    );
\t_V_reg_173_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_261(9),
      Q => t_V_reg_173(9),
      R => ap_NS_fsm112_out
    );
\tmp_user_V_fu_122[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AAEA"
    )
        port map (
      I0 => tmp_user_V_fu_122,
      I1 => start_for_Mat2AXIvideo_U0_empty_n,
      I2 => \ap_CS_fsm_reg_n_0_[0]\,
      I3 => ap_done_reg_0,
      I4 => \^mat2axivideo_u0_img_data_stream_2_v_read\,
      O => \tmp_user_V_fu_122[0]_i_1_n_0\
    );
\tmp_user_V_fu_122_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_user_V_fu_122[0]_i_1_n_0\,
      Q => tmp_user_V_fu_122,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo_1 is
  port (
    ap_done_reg : out STD_LOGIC;
    crop_out_TVALID : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    Mat2AXIvideo_1_U0_ap_ready : out STD_LOGIC;
    Mat2AXIvideo_1_U0_img_data_stream_2_V_read : out STD_LOGIC;
    crop_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    crop_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    crop_out_TDATA : out STD_LOGIC_VECTOR ( 23 downto 0 );
    ap_rst : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_done_reg_reg_0 : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    start_for_Mat2AXIvideo_1_U0_empty_n : in STD_LOGIC;
    crop_out_TREADY : in STD_LOGIC;
    crop_data_data_strea_empty_n : in STD_LOGIC;
    crop_data_data_strea_2_empty_n : in STD_LOGIC;
    crop_data_data_strea_1_empty_n : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 23 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo_1 is
  signal AXI_video_strm_V_data_V_1_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_load_A : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_load_B : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[10]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[11]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[12]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[13]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[14]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[15]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[16]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[17]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[18]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[19]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[1]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[20]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[21]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[22]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[23]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[2]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[3]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[4]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[5]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[6]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[7]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[8]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[9]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[10]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[11]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[12]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[13]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[14]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[15]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[16]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[17]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[18]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[19]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[1]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[20]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[21]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[22]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[23]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[2]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[3]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[4]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[5]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[6]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[7]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[8]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[9]\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_sel_rd_i_1__0_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_sel_rd_reg_n_0 : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_sel_wr : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_sel_wr_i_1__0_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_data_V_1_state[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal AXI_video_strm_V_dest_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_dest_V_1_state[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\ : STD_LOGIC;
  signal AXI_video_strm_V_id_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_id_V_1_state[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_id_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\ : STD_LOGIC;
  signal AXI_video_strm_V_keep_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_keep_V_1_state[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_keep_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1]\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_payload_A : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_payload_A[0]_i_1__0_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_payload_B : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_payload_B[0]_i_1__0_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_sel : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_sel_rd_i_1__0_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_sel_wr : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_sel_wr_i_1__0_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_last_V_1_state[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal AXI_video_strm_V_strb_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_strb_V_1_state[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_strb_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1]\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_payload_A : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_payload_A[0]_i_1__0_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_payload_B : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_payload_B[0]_i_1__0_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_sel : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_sel_rd_i_1__0_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_sel_wr : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_sel_wr_i_1__0_n_0\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_user_V_1_state[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \^mat2axivideo_1_u0_ap_ready\ : STD_LOGIC;
  signal \^mat2axivideo_1_u0_img_data_stream_2_v_read\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \ap_CS_fsm[2]_i_3__0_n_0\ : STD_LOGIC;
  signal \ap_CS_fsm[2]_i_5_n_0\ : STD_LOGIC;
  signal \ap_CS_fsm[3]_i_4__0_n_0\ : STD_LOGIC;
  signal ap_CS_fsm_pp0_stage0 : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_0_[3]\ : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ap_NS_fsm1 : STD_LOGIC;
  signal ap_NS_fsm112_out : STD_LOGIC;
  signal \ap_NS_fsm39_out__1\ : STD_LOGIC;
  signal ap_block_pp0_stage0_subdone : STD_LOGIC;
  signal ap_done_INST_0_i_10_n_0 : STD_LOGIC;
  signal ap_done_INST_0_i_5_n_0 : STD_LOGIC;
  signal ap_done_INST_0_i_9_n_0 : STD_LOGIC;
  signal \^ap_done_reg\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter0 : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter0_i_1__0_n_0\ : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter1_i_1__0_n_0\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1_reg_n_0 : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter2_i_1__0_n_0\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter2_reg_n_0 : STD_LOGIC;
  signal axi_last_V_reg_2710 : STD_LOGIC;
  signal \axi_last_V_reg_271[0]_i_1_n_0\ : STD_LOGIC;
  signal \axi_last_V_reg_271[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_last_V_reg_271_reg_n_0_[0]\ : STD_LOGIC;
  signal \^crop_out_tvalid\ : STD_LOGIC;
  signal \exitcond2_fu_196_p2__13\ : STD_LOGIC;
  signal exitcond_fu_208_p2 : STD_LOGIC;
  signal \exitcond_reg_262[0]_i_1_n_0\ : STD_LOGIC;
  signal \exitcond_reg_262[0]_i_3_n_0\ : STD_LOGIC;
  signal \exitcond_reg_262[0]_i_4_n_0\ : STD_LOGIC;
  signal exitcond_reg_262_pp0_iter1_reg : STD_LOGIC;
  signal \exitcond_reg_262_pp0_iter1_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \exitcond_reg_262_reg_n_0_[0]\ : STD_LOGIC;
  signal i_V_fu_202_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal i_V_reg_257 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal i_V_reg_2570 : STD_LOGIC;
  signal \i_V_reg_257[10]_i_3_n_0\ : STD_LOGIC;
  signal j_V_fu_214_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal p_10_in : STD_LOGIC;
  signal t_V_1_reg_180 : STD_LOGIC;
  signal t_V_1_reg_1800 : STD_LOGIC;
  signal \t_V_1_reg_180[10]_i_5_n_0\ : STD_LOGIC;
  signal \t_V_1_reg_180_reg__0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal t_V_reg_169 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal tmp_user_V_fu_118 : STD_LOGIC;
  signal \tmp_user_V_fu_118[0]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \AXI_video_strm_V_data_V_1_sel_rd_i_1__0\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_data_V_1_state[1]_i_1__0\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_dest_V_1_state[0]_i_2__0\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_last_V_1_sel_rd_i_1__0\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_last_V_1_sel_wr_i_1__0\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_last_V_1_state[1]_i_1__0\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_user_V_1_sel_rd_i_1__0\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_user_V_1_sel_wr_i_1__0\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_user_V_1_state[1]_i_1__0\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_2__0\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_3__0\ : label is "soft_lutpair83";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute SOFT_HLUTNM of ap_done_INST_0_i_6 : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of ap_done_INST_0_i_9 : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \ap_enable_reg_pp0_iter1_i_1__0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \ap_enable_reg_pp0_iter2_i_1__0\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \axi_last_V_reg_271[0]_i_2\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \crop_out_TDATA[0]_INST_0\ : label is "soft_lutpair98";
  attribute SOFT_HLUTNM of \crop_out_TDATA[10]_INST_0\ : label is "soft_lutpair106";
  attribute SOFT_HLUTNM of \crop_out_TDATA[11]_INST_0\ : label is "soft_lutpair107";
  attribute SOFT_HLUTNM of \crop_out_TDATA[12]_INST_0\ : label is "soft_lutpair106";
  attribute SOFT_HLUTNM of \crop_out_TDATA[13]_INST_0\ : label is "soft_lutpair108";
  attribute SOFT_HLUTNM of \crop_out_TDATA[14]_INST_0\ : label is "soft_lutpair108";
  attribute SOFT_HLUTNM of \crop_out_TDATA[15]_INST_0\ : label is "soft_lutpair109";
  attribute SOFT_HLUTNM of \crop_out_TDATA[16]_INST_0\ : label is "soft_lutpair109";
  attribute SOFT_HLUTNM of \crop_out_TDATA[17]_INST_0\ : label is "soft_lutpair107";
  attribute SOFT_HLUTNM of \crop_out_TDATA[18]_INST_0\ : label is "soft_lutpair105";
  attribute SOFT_HLUTNM of \crop_out_TDATA[19]_INST_0\ : label is "soft_lutpair104";
  attribute SOFT_HLUTNM of \crop_out_TDATA[1]_INST_0\ : label is "soft_lutpair99";
  attribute SOFT_HLUTNM of \crop_out_TDATA[20]_INST_0\ : label is "soft_lutpair103";
  attribute SOFT_HLUTNM of \crop_out_TDATA[21]_INST_0\ : label is "soft_lutpair102";
  attribute SOFT_HLUTNM of \crop_out_TDATA[22]_INST_0\ : label is "soft_lutpair100";
  attribute SOFT_HLUTNM of \crop_out_TDATA[23]_INST_0\ : label is "soft_lutpair101";
  attribute SOFT_HLUTNM of \crop_out_TDATA[2]_INST_0\ : label is "soft_lutpair100";
  attribute SOFT_HLUTNM of \crop_out_TDATA[3]_INST_0\ : label is "soft_lutpair101";
  attribute SOFT_HLUTNM of \crop_out_TDATA[4]_INST_0\ : label is "soft_lutpair102";
  attribute SOFT_HLUTNM of \crop_out_TDATA[5]_INST_0\ : label is "soft_lutpair98";
  attribute SOFT_HLUTNM of \crop_out_TDATA[6]_INST_0\ : label is "soft_lutpair99";
  attribute SOFT_HLUTNM of \crop_out_TDATA[7]_INST_0\ : label is "soft_lutpair103";
  attribute SOFT_HLUTNM of \crop_out_TDATA[8]_INST_0\ : label is "soft_lutpair104";
  attribute SOFT_HLUTNM of \crop_out_TDATA[9]_INST_0\ : label is "soft_lutpair105";
  attribute SOFT_HLUTNM of \exitcond_reg_262[0]_i_1\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \exitcond_reg_262[0]_i_2\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \exitcond_reg_262[0]_i_3\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \exitcond_reg_262_pp0_iter1_reg[0]_i_1\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \i_V_reg_257[0]_i_1\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \i_V_reg_257[1]_i_1\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \i_V_reg_257[2]_i_1\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \i_V_reg_257[3]_i_1\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \i_V_reg_257[4]_i_1\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \i_V_reg_257[6]_i_1\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \i_V_reg_257[8]_i_1\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \i_V_reg_257[9]_i_1\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \t_V_1_reg_180[0]_i_1\ : label is "soft_lutpair110";
  attribute SOFT_HLUTNM of \t_V_1_reg_180[10]_i_4\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \t_V_1_reg_180[1]_i_1\ : label is "soft_lutpair110";
  attribute SOFT_HLUTNM of \t_V_1_reg_180[2]_i_1\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \t_V_1_reg_180[3]_i_1\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \t_V_1_reg_180[4]_i_1\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \t_V_1_reg_180[6]_i_1\ : label is "soft_lutpair97";
  attribute SOFT_HLUTNM of \t_V_1_reg_180[7]_i_1\ : label is "soft_lutpair97";
  attribute SOFT_HLUTNM of \t_V_1_reg_180[8]_i_1\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \t_V_1_reg_180[9]_i_1\ : label is "soft_lutpair84";
begin
  Mat2AXIvideo_1_U0_ap_ready <= \^mat2axivideo_1_u0_ap_ready\;
  Mat2AXIvideo_1_U0_img_data_stream_2_V_read <= \^mat2axivideo_1_u0_img_data_stream_2_v_read\;
  Q(0) <= \^q\(0);
  ap_done_reg <= \^ap_done_reg\;
  crop_out_TVALID <= \^crop_out_tvalid\;
\AXI_video_strm_V_data_V_1_payload_A[23]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_data_V_1_ack_in,
      I2 => AXI_video_strm_V_data_V_1_sel_wr,
      O => AXI_video_strm_V_data_V_1_load_A
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(0),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(10),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[10]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(11),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[11]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(12),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[12]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(13),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[13]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(14),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[14]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(15),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[15]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(16),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[16]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(17),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[17]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(18),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[18]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(19),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[19]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(1),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[1]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(20),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[20]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(21),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[21]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(22),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[22]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(23),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[23]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(2),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[2]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(3),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[3]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(4),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[4]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(5),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[5]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(6),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[6]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(7),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[7]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(8),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[8]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(9),
      Q => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[9]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B[23]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_sel_wr,
      I1 => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_1_ack_in,
      O => AXI_video_strm_V_data_V_1_load_B
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(0),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(10),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[10]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(11),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[11]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(12),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[12]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(13),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[13]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(14),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[14]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(15),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[15]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(16),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[16]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(17),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[17]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(18),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[18]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(19),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[19]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(1),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[1]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(20),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[20]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(21),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[21]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(22),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[22]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(23),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[23]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(2),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[2]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(3),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[3]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(4),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[4]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(5),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[5]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(6),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[6]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(7),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[7]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(8),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[8]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(9),
      Q => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[9]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_sel_rd_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => crop_out_TREADY,
      I1 => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => \AXI_video_strm_V_data_V_1_sel_rd_i_1__0_n_0\
    );
AXI_video_strm_V_data_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_data_V_1_sel_rd_i_1__0_n_0\,
      Q => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      R => ap_rst
    );
\AXI_video_strm_V_data_V_1_sel_wr_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_ack_in,
      I1 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      I2 => AXI_video_strm_V_data_V_1_sel_wr,
      O => \AXI_video_strm_V_data_V_1_sel_wr_i_1__0_n_0\
    );
AXI_video_strm_V_data_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_data_V_1_sel_wr_i_1__0_n_0\,
      Q => AXI_video_strm_V_data_V_1_sel_wr,
      R => ap_rst
    );
\AXI_video_strm_V_data_V_1_state[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_1_ack_in,
      I3 => crop_out_TREADY,
      I4 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_data_V_1_state[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_data_V_1_state[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F5FD"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_data_V_1_ack_in,
      I2 => crop_out_TREADY,
      I3 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_data_V_1_state(1)
    );
\AXI_video_strm_V_data_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_data_V_1_state[0]_i_1__0_n_0\,
      Q => \AXI_video_strm_V_data_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_data_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_1_state(1),
      Q => AXI_video_strm_V_data_V_1_ack_in,
      R => ap_rst
    );
\AXI_video_strm_V_dest_V_1_state[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA02A00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => crop_out_TREADY,
      I2 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I3 => \^crop_out_tvalid\,
      I4 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_dest_V_1_state[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_dest_V_1_state[0]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter1_reg_n_0,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \exitcond_reg_262_reg_n_0_[0]\,
      I3 => ap_block_pp0_stage0_subdone,
      O => \^mat2axivideo_1_u0_img_data_stream_2_v_read\
    );
\AXI_video_strm_V_dest_V_1_state[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AFEF"
    )
        port map (
      I0 => crop_out_TREADY,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I2 => \^crop_out_tvalid\,
      I3 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_dest_V_1_state(1)
    );
\AXI_video_strm_V_dest_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_dest_V_1_state[0]_i_1__0_n_0\,
      Q => \^crop_out_tvalid\,
      R => '0'
    );
\AXI_video_strm_V_dest_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_dest_V_1_state(1),
      Q => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      R => ap_rst
    );
\AXI_video_strm_V_id_V_1_state[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA02A00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => crop_out_TREADY,
      I2 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I3 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[0]\,
      I4 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_id_V_1_state[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_id_V_1_state[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AFEF"
    )
        port map (
      I0 => crop_out_TREADY,
      I1 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I2 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[0]\,
      I3 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_id_V_1_state(1)
    );
\AXI_video_strm_V_id_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_id_V_1_state[0]_i_1__0_n_0\,
      Q => \AXI_video_strm_V_id_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_id_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_id_V_1_state(1),
      Q => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      R => ap_rst
    );
\AXI_video_strm_V_keep_V_1_state[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA02A00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => crop_out_TREADY,
      I2 => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1]\,
      I3 => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[0]\,
      I4 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_keep_V_1_state[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_keep_V_1_state[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AFEF"
    )
        port map (
      I0 => crop_out_TREADY,
      I1 => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1]\,
      I2 => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[0]\,
      I3 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_keep_V_1_state(1)
    );
\AXI_video_strm_V_keep_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_keep_V_1_state[0]_i_1__0_n_0\,
      Q => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_keep_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_keep_V_1_state(1),
      Q => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1]\,
      R => ap_rst
    );
\AXI_video_strm_V_last_V_1_payload_A[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => \axi_last_V_reg_271_reg_n_0_[0]\,
      I1 => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_last_V_1_ack_in,
      I3 => AXI_video_strm_V_last_V_1_sel_wr,
      I4 => AXI_video_strm_V_last_V_1_payload_A,
      O => \AXI_video_strm_V_last_V_1_payload_A[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_last_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_payload_A[0]_i_1__0_n_0\,
      Q => AXI_video_strm_V_last_V_1_payload_A,
      R => '0'
    );
\AXI_video_strm_V_last_V_1_payload_B[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBFB8808"
    )
        port map (
      I0 => \axi_last_V_reg_271_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_last_V_1_sel_wr,
      I2 => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      I3 => AXI_video_strm_V_last_V_1_ack_in,
      I4 => AXI_video_strm_V_last_V_1_payload_B,
      O => \AXI_video_strm_V_last_V_1_payload_B[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_last_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_payload_B[0]_i_1__0_n_0\,
      Q => AXI_video_strm_V_last_V_1_payload_B,
      R => '0'
    );
\AXI_video_strm_V_last_V_1_sel_rd_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => crop_out_TREADY,
      I1 => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_last_V_1_sel,
      O => \AXI_video_strm_V_last_V_1_sel_rd_i_1__0_n_0\
    );
AXI_video_strm_V_last_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_sel_rd_i_1__0_n_0\,
      Q => AXI_video_strm_V_last_V_1_sel,
      R => ap_rst
    );
\AXI_video_strm_V_last_V_1_sel_wr_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_1_ack_in,
      I1 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      I2 => AXI_video_strm_V_last_V_1_sel_wr,
      O => \AXI_video_strm_V_last_V_1_sel_wr_i_1__0_n_0\
    );
AXI_video_strm_V_last_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_sel_wr_i_1__0_n_0\,
      Q => AXI_video_strm_V_last_V_1_sel_wr,
      R => ap_rst
    );
\AXI_video_strm_V_last_V_1_state[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_last_V_1_ack_in,
      I3 => crop_out_TREADY,
      I4 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_last_V_1_state[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_last_V_1_state[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F5FD"
    )
        port map (
      I0 => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_last_V_1_ack_in,
      I2 => crop_out_TREADY,
      I3 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_last_V_1_state(1)
    );
\AXI_video_strm_V_last_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_state[0]_i_1__0_n_0\,
      Q => \AXI_video_strm_V_last_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_last_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_1_state(1),
      Q => AXI_video_strm_V_last_V_1_ack_in,
      R => ap_rst
    );
\AXI_video_strm_V_strb_V_1_state[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA02A00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => crop_out_TREADY,
      I2 => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1]\,
      I3 => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[0]\,
      I4 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_strb_V_1_state[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_strb_V_1_state[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AFEF"
    )
        port map (
      I0 => crop_out_TREADY,
      I1 => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1]\,
      I2 => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[0]\,
      I3 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_strb_V_1_state(1)
    );
\AXI_video_strm_V_strb_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_strb_V_1_state[0]_i_1__0_n_0\,
      Q => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_strb_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_strb_V_1_state(1),
      Q => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1]\,
      R => ap_rst
    );
\AXI_video_strm_V_user_V_1_payload_A[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => tmp_user_V_fu_118,
      I1 => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_user_V_1_ack_in,
      I3 => AXI_video_strm_V_user_V_1_sel_wr,
      I4 => AXI_video_strm_V_user_V_1_payload_A,
      O => \AXI_video_strm_V_user_V_1_payload_A[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_user_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_payload_A[0]_i_1__0_n_0\,
      Q => AXI_video_strm_V_user_V_1_payload_A,
      R => '0'
    );
\AXI_video_strm_V_user_V_1_payload_B[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBFB8808"
    )
        port map (
      I0 => tmp_user_V_fu_118,
      I1 => AXI_video_strm_V_user_V_1_sel_wr,
      I2 => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      I3 => AXI_video_strm_V_user_V_1_ack_in,
      I4 => AXI_video_strm_V_user_V_1_payload_B,
      O => \AXI_video_strm_V_user_V_1_payload_B[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_user_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_payload_B[0]_i_1__0_n_0\,
      Q => AXI_video_strm_V_user_V_1_payload_B,
      R => '0'
    );
\AXI_video_strm_V_user_V_1_sel_rd_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => crop_out_TREADY,
      I1 => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_user_V_1_sel,
      O => \AXI_video_strm_V_user_V_1_sel_rd_i_1__0_n_0\
    );
AXI_video_strm_V_user_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_sel_rd_i_1__0_n_0\,
      Q => AXI_video_strm_V_user_V_1_sel,
      R => ap_rst
    );
\AXI_video_strm_V_user_V_1_sel_wr_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => AXI_video_strm_V_user_V_1_ack_in,
      I1 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      I2 => AXI_video_strm_V_user_V_1_sel_wr,
      O => \AXI_video_strm_V_user_V_1_sel_wr_i_1__0_n_0\
    );
AXI_video_strm_V_user_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_sel_wr_i_1__0_n_0\,
      Q => AXI_video_strm_V_user_V_1_sel_wr,
      R => ap_rst
    );
\AXI_video_strm_V_user_V_1_state[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A80888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_user_V_1_ack_in,
      I3 => crop_out_TREADY,
      I4 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => \AXI_video_strm_V_user_V_1_state[0]_i_1__0_n_0\
    );
\AXI_video_strm_V_user_V_1_state[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F5FD"
    )
        port map (
      I0 => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      I1 => AXI_video_strm_V_user_V_1_ack_in,
      I2 => crop_out_TREADY,
      I3 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => AXI_video_strm_V_user_V_1_state(1)
    );
\AXI_video_strm_V_user_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_state[0]_i_1__0_n_0\,
      Q => \AXI_video_strm_V_user_V_1_state_reg_n_0_[0]\,
      R => '0'
    );
\AXI_video_strm_V_user_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_1_state(1),
      Q => AXI_video_strm_V_user_V_1_ack_in,
      R => ap_rst
    );
\ap_CS_fsm[0]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF888F88"
    )
        port map (
      I0 => \^mat2axivideo_1_u0_ap_ready\,
      I1 => ap_CS_fsm_state2,
      I2 => start_for_Mat2AXIvideo_1_U0_empty_n,
      I3 => \^q\(0),
      I4 => \^ap_done_reg\,
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[1]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAEAEAEAEAFFEAEA"
    )
        port map (
      I0 => \ap_CS_fsm_reg_n_0_[3]\,
      I1 => \^q\(0),
      I2 => ap_NS_fsm112_out,
      I3 => ap_NS_fsm1,
      I4 => ap_CS_fsm_state2,
      I5 => \^mat2axivideo_1_u0_ap_ready\,
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[2]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF150015001500"
    )
        port map (
      I0 => \ap_NS_fsm39_out__1\,
      I1 => p_10_in,
      I2 => \ap_CS_fsm[2]_i_3__0_n_0\,
      I3 => ap_CS_fsm_pp0_stage0,
      I4 => ap_NS_fsm1,
      I5 => \ap_CS_fsm[2]_i_5_n_0\,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm[2]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter1_reg_n_0,
      I1 => ap_enable_reg_pp0_iter2_reg_n_0,
      I2 => ap_block_pp0_stage0_subdone,
      O => \ap_NS_fsm39_out__1\
    );
\ap_CS_fsm[2]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter0,
      I1 => ap_enable_reg_pp0_iter1_reg_n_0,
      O => \ap_CS_fsm[2]_i_3__0_n_0\
    );
\ap_CS_fsm[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I2 => ap_CS_fsm_state2,
      I3 => AXI_video_strm_V_data_V_1_ack_in,
      I4 => ap_done_INST_0_i_5_n_0,
      I5 => \exitcond2_fu_196_p2__13\,
      O => ap_NS_fsm1
    );
\ap_CS_fsm[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"70F0F0F0F0F0F0F0"
    )
        port map (
      I0 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I2 => ap_CS_fsm_state2,
      I3 => AXI_video_strm_V_data_V_1_ack_in,
      I4 => ap_done_INST_0_i_5_n_0,
      I5 => \exitcond2_fu_196_p2__13\,
      O => \ap_CS_fsm[2]_i_5_n_0\
    );
\ap_CS_fsm[3]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4040554000000000"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter1_reg_n_0,
      I1 => ap_enable_reg_pp0_iter0,
      I2 => p_10_in,
      I3 => ap_enable_reg_pp0_iter2_reg_n_0,
      I4 => ap_block_pp0_stage0_subdone,
      I5 => ap_CS_fsm_pp0_stage0,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[3]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => exitcond_fu_208_p2,
      I1 => ap_block_pp0_stage0_subdone,
      O => p_10_in
    );
\ap_CS_fsm[3]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF04"
    )
        port map (
      I0 => exitcond_reg_262_pp0_iter1_reg,
      I1 => ap_enable_reg_pp0_iter2_reg_n_0,
      I2 => AXI_video_strm_V_data_V_1_ack_in,
      I3 => \ap_CS_fsm[3]_i_4__0_n_0\,
      O => ap_block_pp0_stage0_subdone
    );
\ap_CS_fsm[3]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1333333300000000"
    )
        port map (
      I0 => crop_data_data_strea_empty_n,
      I1 => \exitcond_reg_262_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_1_ack_in,
      I3 => crop_data_data_strea_2_empty_n,
      I4 => crop_data_data_strea_1_empty_n,
      I5 => ap_enable_reg_pp0_iter1_reg_n_0,
      O => \ap_CS_fsm[3]_i_4__0_n_0\
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \^q\(0),
      S => ap_rst
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => ap_rst
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_CS_fsm_pp0_stage0,
      R => ap_rst
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => \ap_CS_fsm_reg_n_0_[3]\,
      R => ap_rst
    );
ap_done_INST_0_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => t_V_reg_169(9),
      I1 => t_V_reg_169(10),
      I2 => t_V_reg_169(8),
      I3 => t_V_reg_169(7),
      O => ap_done_INST_0_i_10_n_0
    );
ap_done_INST_0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I2 => ap_CS_fsm_state2,
      I3 => AXI_video_strm_V_data_V_1_ack_in,
      I4 => ap_done_INST_0_i_5_n_0,
      I5 => \exitcond2_fu_196_p2__13\,
      O => \^mat2axivideo_1_u0_ap_ready\
    );
ap_done_INST_0_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1]\,
      I2 => AXI_video_strm_V_last_V_1_ack_in,
      I3 => AXI_video_strm_V_user_V_1_ack_in,
      O => ap_done_INST_0_i_5_n_0
    );
ap_done_INST_0_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => ap_done_INST_0_i_9_n_0,
      I1 => ap_done_INST_0_i_10_n_0,
      I2 => t_V_reg_169(0),
      I3 => t_V_reg_169(1),
      I4 => t_V_reg_169(2),
      O => \exitcond2_fu_196_p2__13\
    );
ap_done_INST_0_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => t_V_reg_169(6),
      I1 => t_V_reg_169(5),
      I2 => t_V_reg_169(4),
      I3 => t_V_reg_169(3),
      O => ap_done_INST_0_i_9_n_0
    );
ap_done_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_done_reg_reg_0,
      Q => \^ap_done_reg\,
      R => '0'
    );
\ap_enable_reg_pp0_iter0_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00E0E0E0"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter0,
      I1 => ap_NS_fsm1,
      I2 => ap_rst_n,
      I3 => p_10_in,
      I4 => ap_CS_fsm_pp0_stage0,
      O => \ap_enable_reg_pp0_iter0_i_1__0_n_0\
    );
ap_enable_reg_pp0_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter0_i_1__0_n_0\,
      Q => ap_enable_reg_pp0_iter0,
      R => '0'
    );
\ap_enable_reg_pp0_iter1_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008A80"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_enable_reg_pp0_iter1_reg_n_0,
      I2 => ap_block_pp0_stage0_subdone,
      I3 => ap_enable_reg_pp0_iter0,
      I4 => p_10_in,
      O => \ap_enable_reg_pp0_iter1_i_1__0_n_0\
    );
ap_enable_reg_pp0_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter1_i_1__0_n_0\,
      Q => ap_enable_reg_pp0_iter1_reg_n_0,
      R => '0'
    );
\ap_enable_reg_pp0_iter2_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00C0A0A0"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter1_reg_n_0,
      I1 => ap_enable_reg_pp0_iter2_reg_n_0,
      I2 => ap_rst_n,
      I3 => ap_NS_fsm1,
      I4 => ap_block_pp0_stage0_subdone,
      O => \ap_enable_reg_pp0_iter2_i_1__0_n_0\
    );
ap_enable_reg_pp0_iter2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter2_i_1__0_n_0\,
      Q => ap_enable_reg_pp0_iter2_reg_n_0,
      R => '0'
    );
\axi_last_V_reg_271[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000FFFF80000000"
    )
        port map (
      I0 => \axi_last_V_reg_271[0]_i_2_n_0\,
      I1 => \t_V_1_reg_180_reg__0\(0),
      I2 => \t_V_1_reg_180_reg__0\(1),
      I3 => \t_V_1_reg_180_reg__0\(2),
      I4 => axi_last_V_reg_2710,
      I5 => \axi_last_V_reg_271_reg_n_0_[0]\,
      O => \axi_last_V_reg_271[0]_i_1_n_0\
    );
\axi_last_V_reg_271[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000800"
    )
        port map (
      I0 => \exitcond_reg_262[0]_i_4_n_0\,
      I1 => \t_V_1_reg_180_reg__0\(4),
      I2 => \t_V_1_reg_180_reg__0\(3),
      I3 => \t_V_1_reg_180_reg__0\(5),
      I4 => \t_V_1_reg_180_reg__0\(6),
      O => \axi_last_V_reg_271[0]_i_2_n_0\
    );
\axi_last_V_reg_271_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \axi_last_V_reg_271[0]_i_1_n_0\,
      Q => \axi_last_V_reg_271_reg_n_0_[0]\,
      R => '0'
    );
\crop_out_TDATA[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[0]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[0]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(0)
    );
\crop_out_TDATA[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[10]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[10]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(10)
    );
\crop_out_TDATA[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[11]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[11]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(11)
    );
\crop_out_TDATA[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[12]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[12]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(12)
    );
\crop_out_TDATA[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[13]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[13]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(13)
    );
\crop_out_TDATA[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[14]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[14]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(14)
    );
\crop_out_TDATA[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[15]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[15]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(15)
    );
\crop_out_TDATA[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[16]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[16]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(16)
    );
\crop_out_TDATA[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[17]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[17]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(17)
    );
\crop_out_TDATA[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[18]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[18]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(18)
    );
\crop_out_TDATA[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[19]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[19]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(19)
    );
\crop_out_TDATA[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[1]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(1)
    );
\crop_out_TDATA[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[20]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[20]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(20)
    );
\crop_out_TDATA[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[21]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[21]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(21)
    );
\crop_out_TDATA[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[22]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[22]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(22)
    );
\crop_out_TDATA[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[23]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[23]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(23)
    );
\crop_out_TDATA[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[2]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[2]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(2)
    );
\crop_out_TDATA[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[3]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[3]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(3)
    );
\crop_out_TDATA[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[4]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[4]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(4)
    );
\crop_out_TDATA[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[5]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[5]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(5)
    );
\crop_out_TDATA[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[6]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[6]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(6)
    );
\crop_out_TDATA[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[7]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[7]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(7)
    );
\crop_out_TDATA[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[8]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[8]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(8)
    );
\crop_out_TDATA[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[9]\,
      I1 => \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[9]\,
      I2 => AXI_video_strm_V_data_V_1_sel_rd_reg_n_0,
      O => crop_out_TDATA(9)
    );
\crop_out_TLAST[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_1_payload_B,
      I1 => AXI_video_strm_V_last_V_1_sel,
      I2 => AXI_video_strm_V_last_V_1_payload_A,
      O => crop_out_TLAST(0)
    );
\crop_out_TUSER[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_user_V_1_payload_B,
      I1 => AXI_video_strm_V_user_V_1_sel,
      I2 => AXI_video_strm_V_user_V_1_payload_A,
      O => crop_out_TUSER(0)
    );
\exitcond_reg_262[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => exitcond_fu_208_p2,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => ap_block_pp0_stage0_subdone,
      I3 => \exitcond_reg_262_reg_n_0_[0]\,
      O => \exitcond_reg_262[0]_i_1_n_0\
    );
\exitcond_reg_262[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => \exitcond_reg_262[0]_i_3_n_0\,
      I1 => \exitcond_reg_262[0]_i_4_n_0\,
      I2 => \t_V_1_reg_180_reg__0\(0),
      I3 => \t_V_1_reg_180_reg__0\(1),
      I4 => \t_V_1_reg_180_reg__0\(2),
      O => exitcond_fu_208_p2
    );
\exitcond_reg_262[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(6),
      I1 => \t_V_1_reg_180_reg__0\(5),
      I2 => \t_V_1_reg_180_reg__0\(4),
      I3 => \t_V_1_reg_180_reg__0\(3),
      O => \exitcond_reg_262[0]_i_3_n_0\
    );
\exitcond_reg_262[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(9),
      I1 => \t_V_1_reg_180_reg__0\(10),
      I2 => \t_V_1_reg_180_reg__0\(8),
      I3 => \t_V_1_reg_180_reg__0\(7),
      O => \exitcond_reg_262[0]_i_4_n_0\
    );
\exitcond_reg_262_pp0_iter1_reg[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \exitcond_reg_262_reg_n_0_[0]\,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => ap_block_pp0_stage0_subdone,
      I3 => exitcond_reg_262_pp0_iter1_reg,
      O => \exitcond_reg_262_pp0_iter1_reg[0]_i_1_n_0\
    );
\exitcond_reg_262_pp0_iter1_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \exitcond_reg_262_pp0_iter1_reg[0]_i_1_n_0\,
      Q => exitcond_reg_262_pp0_iter1_reg,
      R => '0'
    );
\exitcond_reg_262_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \exitcond_reg_262[0]_i_1_n_0\,
      Q => \exitcond_reg_262_reg_n_0_[0]\,
      R => '0'
    );
\i_V_reg_257[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => t_V_reg_169(0),
      O => i_V_fu_202_p2(0)
    );
\i_V_reg_257[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \AXI_video_strm_V_id_V_1_state_reg_n_0_[1]\,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1]\,
      I2 => ap_CS_fsm_state2,
      I3 => AXI_video_strm_V_data_V_1_ack_in,
      I4 => ap_done_INST_0_i_5_n_0,
      O => i_V_reg_2570
    );
\i_V_reg_257[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => t_V_reg_169(8),
      I1 => t_V_reg_169(6),
      I2 => \i_V_reg_257[10]_i_3_n_0\,
      I3 => t_V_reg_169(7),
      I4 => t_V_reg_169(9),
      I5 => t_V_reg_169(10),
      O => i_V_fu_202_p2(10)
    );
\i_V_reg_257[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => t_V_reg_169(5),
      I1 => t_V_reg_169(3),
      I2 => t_V_reg_169(1),
      I3 => t_V_reg_169(0),
      I4 => t_V_reg_169(2),
      I5 => t_V_reg_169(4),
      O => \i_V_reg_257[10]_i_3_n_0\
    );
\i_V_reg_257[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => t_V_reg_169(0),
      I1 => t_V_reg_169(1),
      O => i_V_fu_202_p2(1)
    );
\i_V_reg_257[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => t_V_reg_169(0),
      I1 => t_V_reg_169(1),
      I2 => t_V_reg_169(2),
      O => i_V_fu_202_p2(2)
    );
\i_V_reg_257[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => t_V_reg_169(1),
      I1 => t_V_reg_169(0),
      I2 => t_V_reg_169(2),
      I3 => t_V_reg_169(3),
      O => i_V_fu_202_p2(3)
    );
\i_V_reg_257[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => t_V_reg_169(2),
      I1 => t_V_reg_169(0),
      I2 => t_V_reg_169(1),
      I3 => t_V_reg_169(3),
      I4 => t_V_reg_169(4),
      O => i_V_fu_202_p2(4)
    );
\i_V_reg_257[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => t_V_reg_169(3),
      I1 => t_V_reg_169(1),
      I2 => t_V_reg_169(0),
      I3 => t_V_reg_169(2),
      I4 => t_V_reg_169(4),
      I5 => t_V_reg_169(5),
      O => i_V_fu_202_p2(5)
    );
\i_V_reg_257[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_V_reg_257[10]_i_3_n_0\,
      I1 => t_V_reg_169(6),
      O => i_V_fu_202_p2(6)
    );
\i_V_reg_257[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \i_V_reg_257[10]_i_3_n_0\,
      I1 => t_V_reg_169(6),
      I2 => t_V_reg_169(7),
      O => i_V_fu_202_p2(7)
    );
\i_V_reg_257[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => t_V_reg_169(6),
      I1 => \i_V_reg_257[10]_i_3_n_0\,
      I2 => t_V_reg_169(7),
      I3 => t_V_reg_169(8),
      O => i_V_fu_202_p2(8)
    );
\i_V_reg_257[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => t_V_reg_169(7),
      I1 => \i_V_reg_257[10]_i_3_n_0\,
      I2 => t_V_reg_169(6),
      I3 => t_V_reg_169(8),
      I4 => t_V_reg_169(9),
      O => i_V_fu_202_p2(9)
    );
\i_V_reg_257_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(0),
      Q => i_V_reg_257(0),
      R => '0'
    );
\i_V_reg_257_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(10),
      Q => i_V_reg_257(10),
      R => '0'
    );
\i_V_reg_257_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(1),
      Q => i_V_reg_257(1),
      R => '0'
    );
\i_V_reg_257_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(2),
      Q => i_V_reg_257(2),
      R => '0'
    );
\i_V_reg_257_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(3),
      Q => i_V_reg_257(3),
      R => '0'
    );
\i_V_reg_257_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(4),
      Q => i_V_reg_257(4),
      R => '0'
    );
\i_V_reg_257_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(5),
      Q => i_V_reg_257(5),
      R => '0'
    );
\i_V_reg_257_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(6),
      Q => i_V_reg_257(6),
      R => '0'
    );
\i_V_reg_257_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(7),
      Q => i_V_reg_257(7),
      R => '0'
    );
\i_V_reg_257_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(8),
      Q => i_V_reg_257(8),
      R => '0'
    );
\i_V_reg_257_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2570,
      D => i_V_fu_202_p2(9),
      Q => i_V_reg_257(9),
      R => '0'
    );
\t_V_1_reg_180[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(0),
      O => j_V_fu_214_p2(0)
    );
\t_V_1_reg_180[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => axi_last_V_reg_2710,
      I2 => ap_enable_reg_pp0_iter0,
      O => t_V_1_reg_180
    );
\t_V_1_reg_180[10]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter0,
      I1 => axi_last_V_reg_2710,
      O => t_V_1_reg_1800
    );
\t_V_1_reg_180[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(8),
      I1 => \t_V_1_reg_180_reg__0\(6),
      I2 => \t_V_1_reg_180[10]_i_5_n_0\,
      I3 => \t_V_1_reg_180_reg__0\(7),
      I4 => \t_V_1_reg_180_reg__0\(9),
      I5 => \t_V_1_reg_180_reg__0\(10),
      O => j_V_fu_214_p2(10)
    );
\t_V_1_reg_180[10]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => exitcond_fu_208_p2,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => ap_block_pp0_stage0_subdone,
      O => axi_last_V_reg_2710
    );
\t_V_1_reg_180[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(5),
      I1 => \t_V_1_reg_180_reg__0\(3),
      I2 => \t_V_1_reg_180_reg__0\(1),
      I3 => \t_V_1_reg_180_reg__0\(0),
      I4 => \t_V_1_reg_180_reg__0\(2),
      I5 => \t_V_1_reg_180_reg__0\(4),
      O => \t_V_1_reg_180[10]_i_5_n_0\
    );
\t_V_1_reg_180[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(0),
      I1 => \t_V_1_reg_180_reg__0\(1),
      O => j_V_fu_214_p2(1)
    );
\t_V_1_reg_180[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(0),
      I1 => \t_V_1_reg_180_reg__0\(1),
      I2 => \t_V_1_reg_180_reg__0\(2),
      O => j_V_fu_214_p2(2)
    );
\t_V_1_reg_180[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(1),
      I1 => \t_V_1_reg_180_reg__0\(0),
      I2 => \t_V_1_reg_180_reg__0\(2),
      I3 => \t_V_1_reg_180_reg__0\(3),
      O => j_V_fu_214_p2(3)
    );
\t_V_1_reg_180[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(2),
      I1 => \t_V_1_reg_180_reg__0\(0),
      I2 => \t_V_1_reg_180_reg__0\(1),
      I3 => \t_V_1_reg_180_reg__0\(3),
      I4 => \t_V_1_reg_180_reg__0\(4),
      O => j_V_fu_214_p2(4)
    );
\t_V_1_reg_180[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(3),
      I1 => \t_V_1_reg_180_reg__0\(1),
      I2 => \t_V_1_reg_180_reg__0\(0),
      I3 => \t_V_1_reg_180_reg__0\(2),
      I4 => \t_V_1_reg_180_reg__0\(4),
      I5 => \t_V_1_reg_180_reg__0\(5),
      O => j_V_fu_214_p2(5)
    );
\t_V_1_reg_180[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_1_reg_180[10]_i_5_n_0\,
      I1 => \t_V_1_reg_180_reg__0\(6),
      O => j_V_fu_214_p2(6)
    );
\t_V_1_reg_180[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \t_V_1_reg_180[10]_i_5_n_0\,
      I1 => \t_V_1_reg_180_reg__0\(6),
      I2 => \t_V_1_reg_180_reg__0\(7),
      O => j_V_fu_214_p2(7)
    );
\t_V_1_reg_180[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(6),
      I1 => \t_V_1_reg_180[10]_i_5_n_0\,
      I2 => \t_V_1_reg_180_reg__0\(7),
      I3 => \t_V_1_reg_180_reg__0\(8),
      O => j_V_fu_214_p2(8)
    );
\t_V_1_reg_180[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \t_V_1_reg_180_reg__0\(7),
      I1 => \t_V_1_reg_180[10]_i_5_n_0\,
      I2 => \t_V_1_reg_180_reg__0\(6),
      I3 => \t_V_1_reg_180_reg__0\(8),
      I4 => \t_V_1_reg_180_reg__0\(9),
      O => j_V_fu_214_p2(9)
    );
\t_V_1_reg_180_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(0),
      Q => \t_V_1_reg_180_reg__0\(0),
      R => t_V_1_reg_180
    );
\t_V_1_reg_180_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(10),
      Q => \t_V_1_reg_180_reg__0\(10),
      R => t_V_1_reg_180
    );
\t_V_1_reg_180_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(1),
      Q => \t_V_1_reg_180_reg__0\(1),
      R => t_V_1_reg_180
    );
\t_V_1_reg_180_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(2),
      Q => \t_V_1_reg_180_reg__0\(2),
      R => t_V_1_reg_180
    );
\t_V_1_reg_180_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(3),
      Q => \t_V_1_reg_180_reg__0\(3),
      R => t_V_1_reg_180
    );
\t_V_1_reg_180_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(4),
      Q => \t_V_1_reg_180_reg__0\(4),
      R => t_V_1_reg_180
    );
\t_V_1_reg_180_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(5),
      Q => \t_V_1_reg_180_reg__0\(5),
      R => t_V_1_reg_180
    );
\t_V_1_reg_180_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(6),
      Q => \t_V_1_reg_180_reg__0\(6),
      R => t_V_1_reg_180
    );
\t_V_1_reg_180_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(7),
      Q => \t_V_1_reg_180_reg__0\(7),
      R => t_V_1_reg_180
    );
\t_V_1_reg_180_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(8),
      Q => \t_V_1_reg_180_reg__0\(8),
      R => t_V_1_reg_180
    );
\t_V_1_reg_180_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1800,
      D => j_V_fu_214_p2(9),
      Q => \t_V_1_reg_180_reg__0\(9),
      R => t_V_1_reg_180
    );
\t_V_reg_169[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => start_for_Mat2AXIvideo_1_U0_empty_n,
      I1 => \^q\(0),
      I2 => \^ap_done_reg\,
      O => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(0),
      Q => t_V_reg_169(0),
      R => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(10),
      Q => t_V_reg_169(10),
      R => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(1),
      Q => t_V_reg_169(1),
      R => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(2),
      Q => t_V_reg_169(2),
      R => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(3),
      Q => t_V_reg_169(3),
      R => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(4),
      Q => t_V_reg_169(4),
      R => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(5),
      Q => t_V_reg_169(5),
      R => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(6),
      Q => t_V_reg_169(6),
      R => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(7),
      Q => t_V_reg_169(7),
      R => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(8),
      Q => t_V_reg_169(8),
      R => ap_NS_fsm112_out
    );
\t_V_reg_169_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg_n_0_[3]\,
      D => i_V_reg_257(9),
      Q => t_V_reg_169(9),
      R => ap_NS_fsm112_out
    );
\tmp_user_V_fu_118[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AAEA"
    )
        port map (
      I0 => tmp_user_V_fu_118,
      I1 => start_for_Mat2AXIvideo_1_U0_empty_n,
      I2 => \^q\(0),
      I3 => \^ap_done_reg\,
      I4 => \^mat2axivideo_1_u0_img_data_stream_2_v_read\,
      O => \tmp_user_V_fu_118[0]_i_1_n_0\
    );
\tmp_user_V_fu_118_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_user_V_fu_118[0]_i_1_n_0\,
      Q => tmp_user_V_fu_118,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \mOutPtr_reg[0]\ : in STD_LOGIC;
    \mOutPtr_reg[1]\ : in STD_LOGIC;
    internal_full_n_reg : in STD_LOGIC;
    ce : in STD_LOGIC;
    \SRL_SIG_reg[1][7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg is
  signal \SRL_SIG_reg_n_0_[0][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][7]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][7]\ : STD_LOGIC;
begin
\AXI_video_strm_V_data_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][0]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][0]\,
      O => D(0)
    );
\AXI_video_strm_V_data_V_1_payload_A[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][1]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][1]\,
      O => D(1)
    );
\AXI_video_strm_V_data_V_1_payload_A[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][2]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][2]\,
      O => D(2)
    );
\AXI_video_strm_V_data_V_1_payload_A[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][3]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][3]\,
      O => D(3)
    );
\AXI_video_strm_V_data_V_1_payload_A[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][4]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][4]\,
      O => D(4)
    );
\AXI_video_strm_V_data_V_1_payload_A[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][5]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][5]\,
      O => D(5)
    );
\AXI_video_strm_V_data_V_1_payload_A[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][6]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][6]\,
      O => D(6)
    );
\AXI_video_strm_V_data_V_1_payload_A[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][7]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][7]\,
      O => D(7)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(0),
      Q => \SRL_SIG_reg_n_0_[0][0]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(1),
      Q => \SRL_SIG_reg_n_0_[0][1]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(2),
      Q => \SRL_SIG_reg_n_0_[0][2]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(3),
      Q => \SRL_SIG_reg_n_0_[0][3]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(4),
      Q => \SRL_SIG_reg_n_0_[0][4]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(5),
      Q => \SRL_SIG_reg_n_0_[0][5]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(6),
      Q => \SRL_SIG_reg_n_0_[0][6]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(7),
      Q => \SRL_SIG_reg_n_0_[0][7]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][0]\,
      Q => \SRL_SIG_reg_n_0_[1][0]\,
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][1]\,
      Q => \SRL_SIG_reg_n_0_[1][1]\,
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][2]\,
      Q => \SRL_SIG_reg_n_0_[1][2]\,
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][3]\,
      Q => \SRL_SIG_reg_n_0_[1][3]\,
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][4]\,
      Q => \SRL_SIG_reg_n_0_[1][4]\,
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][5]\,
      Q => \SRL_SIG_reg_n_0_[1][5]\,
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][6]\,
      Q => \SRL_SIG_reg_n_0_[1][6]\,
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][7]\,
      Q => \SRL_SIG_reg_n_0_[1][7]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_10 is
  port (
    \SRL_SIG_reg[0][7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    internal_full_n_reg : in STD_LOGIC;
    AXIvideo2Mat_U0_img_data_stream_2_V_write : in STD_LOGIC;
    mOutPtr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    D : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_10 : entity is "fifo_w8_d2_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_10;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_10 is
  signal \SRL_SIG_reg[0]\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \SRL_SIG_reg[1]\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal ce : STD_LOGIC;
begin
\SRL_SIG[0][0]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg[1]\(0),
      I1 => mOutPtr(0),
      I2 => mOutPtr(1),
      I3 => \SRL_SIG_reg[0]\(0),
      O => \SRL_SIG_reg[0][7]_0\(0)
    );
\SRL_SIG[0][1]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg[1]\(1),
      I1 => mOutPtr(0),
      I2 => mOutPtr(1),
      I3 => \SRL_SIG_reg[0]\(1),
      O => \SRL_SIG_reg[0][7]_0\(1)
    );
\SRL_SIG[0][2]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg[1]\(2),
      I1 => mOutPtr(0),
      I2 => mOutPtr(1),
      I3 => \SRL_SIG_reg[0]\(2),
      O => \SRL_SIG_reg[0][7]_0\(2)
    );
\SRL_SIG[0][3]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg[1]\(3),
      I1 => mOutPtr(0),
      I2 => mOutPtr(1),
      I3 => \SRL_SIG_reg[0]\(3),
      O => \SRL_SIG_reg[0][7]_0\(3)
    );
\SRL_SIG[0][4]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg[1]\(4),
      I1 => mOutPtr(0),
      I2 => mOutPtr(1),
      I3 => \SRL_SIG_reg[0]\(4),
      O => \SRL_SIG_reg[0][7]_0\(4)
    );
\SRL_SIG[0][5]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg[1]\(5),
      I1 => mOutPtr(0),
      I2 => mOutPtr(1),
      I3 => \SRL_SIG_reg[0]\(5),
      O => \SRL_SIG_reg[0][7]_0\(5)
    );
\SRL_SIG[0][6]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg[1]\(6),
      I1 => mOutPtr(0),
      I2 => mOutPtr(1),
      I3 => \SRL_SIG_reg[0]\(6),
      O => \SRL_SIG_reg[0][7]_0\(6)
    );
\SRL_SIG[0][7]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => internal_full_n_reg,
      I1 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      O => ce
    );
\SRL_SIG[0][7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg[1]\(7),
      I1 => mOutPtr(0),
      I2 => mOutPtr(1),
      I3 => \SRL_SIG_reg[0]\(7),
      O => \SRL_SIG_reg[0][7]_0\(7)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(0),
      Q => \SRL_SIG_reg[0]\(0),
      R => '0'
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(1),
      Q => \SRL_SIG_reg[0]\(1),
      R => '0'
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(2),
      Q => \SRL_SIG_reg[0]\(2),
      R => '0'
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(3),
      Q => \SRL_SIG_reg[0]\(3),
      R => '0'
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(4),
      Q => \SRL_SIG_reg[0]\(4),
      R => '0'
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(5),
      Q => \SRL_SIG_reg[0]\(5),
      R => '0'
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(6),
      Q => \SRL_SIG_reg[0]\(6),
      R => '0'
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(7),
      Q => \SRL_SIG_reg[0]\(7),
      R => '0'
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[0]\(0),
      Q => \SRL_SIG_reg[1]\(0),
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[0]\(1),
      Q => \SRL_SIG_reg[1]\(1),
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[0]\(2),
      Q => \SRL_SIG_reg[1]\(2),
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[0]\(3),
      Q => \SRL_SIG_reg[1]\(3),
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[0]\(4),
      Q => \SRL_SIG_reg[1]\(4),
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[0]\(5),
      Q => \SRL_SIG_reg[1]\(5),
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[0]\(6),
      Q => \SRL_SIG_reg[1]\(6),
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[0]\(7),
      Q => \SRL_SIG_reg[1]\(7),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_11 is
  port (
    \SRL_SIG_reg[0][7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    internal_full_n_reg : in STD_LOGIC;
    AXIvideo2Mat_U0_img_data_stream_2_V_write : in STD_LOGIC;
    \mOutPtr_reg[0]\ : in STD_LOGIC;
    \mOutPtr_reg[1]\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_11 : entity is "fifo_w8_d2_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_11;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_11 is
  signal \SRL_SIG_reg_n_0_[0][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][7]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][7]\ : STD_LOGIC;
  signal ce : STD_LOGIC;
begin
\SRL_SIG[0][0]_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][0]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][0]\,
      O => \SRL_SIG_reg[0][7]_0\(0)
    );
\SRL_SIG[0][1]_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][1]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][1]\,
      O => \SRL_SIG_reg[0][7]_0\(1)
    );
\SRL_SIG[0][2]_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][2]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][2]\,
      O => \SRL_SIG_reg[0][7]_0\(2)
    );
\SRL_SIG[0][3]_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][3]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][3]\,
      O => \SRL_SIG_reg[0][7]_0\(3)
    );
\SRL_SIG[0][4]_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][4]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][4]\,
      O => \SRL_SIG_reg[0][7]_0\(4)
    );
\SRL_SIG[0][5]_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][5]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][5]\,
      O => \SRL_SIG_reg[0][7]_0\(5)
    );
\SRL_SIG[0][6]_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][6]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][6]\,
      O => \SRL_SIG_reg[0][7]_0\(6)
    );
\SRL_SIG[0][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => internal_full_n_reg,
      I1 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      O => ce
    );
\SRL_SIG[0][7]_i_3__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][7]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][7]\,
      O => \SRL_SIG_reg[0][7]_0\(7)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(0),
      Q => \SRL_SIG_reg_n_0_[0][0]\,
      R => '0'
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(1),
      Q => \SRL_SIG_reg_n_0_[0][1]\,
      R => '0'
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(2),
      Q => \SRL_SIG_reg_n_0_[0][2]\,
      R => '0'
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(3),
      Q => \SRL_SIG_reg_n_0_[0][3]\,
      R => '0'
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(4),
      Q => \SRL_SIG_reg_n_0_[0][4]\,
      R => '0'
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(5),
      Q => \SRL_SIG_reg_n_0_[0][5]\,
      R => '0'
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(6),
      Q => \SRL_SIG_reg_n_0_[0][6]\,
      R => '0'
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(7),
      Q => \SRL_SIG_reg_n_0_[0][7]\,
      R => '0'
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][0]\,
      Q => \SRL_SIG_reg_n_0_[1][0]\,
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][1]\,
      Q => \SRL_SIG_reg_n_0_[1][1]\,
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][2]\,
      Q => \SRL_SIG_reg_n_0_[1][2]\,
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][3]\,
      Q => \SRL_SIG_reg_n_0_[1][3]\,
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][4]\,
      Q => \SRL_SIG_reg_n_0_[1][4]\,
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][5]\,
      Q => \SRL_SIG_reg_n_0_[1][5]\,
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][6]\,
      Q => \SRL_SIG_reg_n_0_[1][6]\,
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][7]\,
      Q => \SRL_SIG_reg_n_0_[1][7]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_12 is
  port (
    \SRL_SIG_reg[0][7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    internal_full_n_reg : in STD_LOGIC;
    AXIvideo2Mat_U0_img_data_stream_2_V_write : in STD_LOGIC;
    \mOutPtr_reg[0]\ : in STD_LOGIC;
    \mOutPtr_reg[1]\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_12 : entity is "fifo_w8_d2_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_12;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_12 is
  signal \SRL_SIG_reg_n_0_[0][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][7]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][7]\ : STD_LOGIC;
  signal ce : STD_LOGIC;
begin
\SRL_SIG[0][0]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][0]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][0]\,
      O => \SRL_SIG_reg[0][7]_0\(0)
    );
\SRL_SIG[0][1]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][1]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][1]\,
      O => \SRL_SIG_reg[0][7]_0\(1)
    );
\SRL_SIG[0][2]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][2]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][2]\,
      O => \SRL_SIG_reg[0][7]_0\(2)
    );
\SRL_SIG[0][3]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][3]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][3]\,
      O => \SRL_SIG_reg[0][7]_0\(3)
    );
\SRL_SIG[0][4]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][4]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][4]\,
      O => \SRL_SIG_reg[0][7]_0\(4)
    );
\SRL_SIG[0][5]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][5]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][5]\,
      O => \SRL_SIG_reg[0][7]_0\(5)
    );
\SRL_SIG[0][6]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][6]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][6]\,
      O => \SRL_SIG_reg[0][7]_0\(6)
    );
\SRL_SIG[0][7]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => internal_full_n_reg,
      I1 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      O => ce
    );
\SRL_SIG[0][7]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][7]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][7]\,
      O => \SRL_SIG_reg[0][7]_0\(7)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(0),
      Q => \SRL_SIG_reg_n_0_[0][0]\,
      R => '0'
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(1),
      Q => \SRL_SIG_reg_n_0_[0][1]\,
      R => '0'
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(2),
      Q => \SRL_SIG_reg_n_0_[0][2]\,
      R => '0'
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(3),
      Q => \SRL_SIG_reg_n_0_[0][3]\,
      R => '0'
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(4),
      Q => \SRL_SIG_reg_n_0_[0][4]\,
      R => '0'
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(5),
      Q => \SRL_SIG_reg_n_0_[0][5]\,
      R => '0'
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(6),
      Q => \SRL_SIG_reg_n_0_[0][6]\,
      R => '0'
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => D(7),
      Q => \SRL_SIG_reg_n_0_[0][7]\,
      R => '0'
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][0]\,
      Q => \SRL_SIG_reg_n_0_[1][0]\,
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][1]\,
      Q => \SRL_SIG_reg_n_0_[1][1]\,
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][2]\,
      Q => \SRL_SIG_reg_n_0_[1][2]\,
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][3]\,
      Q => \SRL_SIG_reg_n_0_[1][3]\,
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][4]\,
      Q => \SRL_SIG_reg_n_0_[1][4]\,
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][5]\,
      Q => \SRL_SIG_reg_n_0_[1][5]\,
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][6]\,
      Q => \SRL_SIG_reg_n_0_[1][6]\,
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][7]\,
      Q => \SRL_SIG_reg_n_0_[1][7]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_13 is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \mOutPtr_reg[0]\ : in STD_LOGIC;
    \mOutPtr_reg[1]\ : in STD_LOGIC;
    ce : in STD_LOGIC;
    \SRL_SIG_reg[1][7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_13 : entity is "fifo_w8_d2_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_13;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_13 is
  signal \SRL_SIG_reg_n_0_[0][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][7]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][7]\ : STD_LOGIC;
begin
\AXI_video_strm_V_data_V_1_payload_A[0]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][0]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][0]\,
      O => D(0)
    );
\AXI_video_strm_V_data_V_1_payload_A[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][1]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][1]\,
      O => D(1)
    );
\AXI_video_strm_V_data_V_1_payload_A[2]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][2]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][2]\,
      O => D(2)
    );
\AXI_video_strm_V_data_V_1_payload_A[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][3]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][3]\,
      O => D(3)
    );
\AXI_video_strm_V_data_V_1_payload_A[4]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][4]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][4]\,
      O => D(4)
    );
\AXI_video_strm_V_data_V_1_payload_A[5]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][5]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][5]\,
      O => D(5)
    );
\AXI_video_strm_V_data_V_1_payload_A[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][6]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][6]\,
      O => D(6)
    );
\AXI_video_strm_V_data_V_1_payload_A[7]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][7]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][7]\,
      O => D(7)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(0),
      Q => \SRL_SIG_reg_n_0_[0][0]\,
      R => '0'
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(1),
      Q => \SRL_SIG_reg_n_0_[0][1]\,
      R => '0'
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(2),
      Q => \SRL_SIG_reg_n_0_[0][2]\,
      R => '0'
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(3),
      Q => \SRL_SIG_reg_n_0_[0][3]\,
      R => '0'
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(4),
      Q => \SRL_SIG_reg_n_0_[0][4]\,
      R => '0'
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(5),
      Q => \SRL_SIG_reg_n_0_[0][5]\,
      R => '0'
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(6),
      Q => \SRL_SIG_reg_n_0_[0][6]\,
      R => '0'
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(7),
      Q => \SRL_SIG_reg_n_0_[0][7]\,
      R => '0'
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][0]\,
      Q => \SRL_SIG_reg_n_0_[1][0]\,
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][1]\,
      Q => \SRL_SIG_reg_n_0_[1][1]\,
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][2]\,
      Q => \SRL_SIG_reg_n_0_[1][2]\,
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][3]\,
      Q => \SRL_SIG_reg_n_0_[1][3]\,
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][4]\,
      Q => \SRL_SIG_reg_n_0_[1][4]\,
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][5]\,
      Q => \SRL_SIG_reg_n_0_[1][5]\,
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][6]\,
      Q => \SRL_SIG_reg_n_0_[1][6]\,
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][7]\,
      Q => \SRL_SIG_reg_n_0_[1][7]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_14 is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \mOutPtr_reg[0]\ : in STD_LOGIC;
    \mOutPtr_reg[1]\ : in STD_LOGIC;
    ce : in STD_LOGIC;
    \SRL_SIG_reg[1][7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_14 : entity is "fifo_w8_d2_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_14 is
  signal \SRL_SIG_reg_n_0_[0][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][7]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][7]\ : STD_LOGIC;
begin
\AXI_video_strm_V_data_V_1_payload_A[16]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][0]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][0]\,
      O => D(0)
    );
\AXI_video_strm_V_data_V_1_payload_A[17]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][1]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][1]\,
      O => D(1)
    );
\AXI_video_strm_V_data_V_1_payload_A[18]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][2]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][2]\,
      O => D(2)
    );
\AXI_video_strm_V_data_V_1_payload_A[19]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][3]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][3]\,
      O => D(3)
    );
\AXI_video_strm_V_data_V_1_payload_A[20]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][4]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][4]\,
      O => D(4)
    );
\AXI_video_strm_V_data_V_1_payload_A[21]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][5]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][5]\,
      O => D(5)
    );
\AXI_video_strm_V_data_V_1_payload_A[22]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][6]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][6]\,
      O => D(6)
    );
\AXI_video_strm_V_data_V_1_payload_A[23]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][7]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][7]\,
      O => D(7)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(0),
      Q => \SRL_SIG_reg_n_0_[0][0]\,
      R => '0'
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(1),
      Q => \SRL_SIG_reg_n_0_[0][1]\,
      R => '0'
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(2),
      Q => \SRL_SIG_reg_n_0_[0][2]\,
      R => '0'
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(3),
      Q => \SRL_SIG_reg_n_0_[0][3]\,
      R => '0'
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(4),
      Q => \SRL_SIG_reg_n_0_[0][4]\,
      R => '0'
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(5),
      Q => \SRL_SIG_reg_n_0_[0][5]\,
      R => '0'
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(6),
      Q => \SRL_SIG_reg_n_0_[0][6]\,
      R => '0'
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(7),
      Q => \SRL_SIG_reg_n_0_[0][7]\,
      R => '0'
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][0]\,
      Q => \SRL_SIG_reg_n_0_[1][0]\,
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][1]\,
      Q => \SRL_SIG_reg_n_0_[1][1]\,
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][2]\,
      Q => \SRL_SIG_reg_n_0_[1][2]\,
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][3]\,
      Q => \SRL_SIG_reg_n_0_[1][3]\,
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][4]\,
      Q => \SRL_SIG_reg_n_0_[1][4]\,
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][5]\,
      Q => \SRL_SIG_reg_n_0_[1][5]\,
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][6]\,
      Q => \SRL_SIG_reg_n_0_[1][6]\,
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][7]\,
      Q => \SRL_SIG_reg_n_0_[1][7]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_15 is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \mOutPtr_reg[0]\ : in STD_LOGIC;
    \mOutPtr_reg[1]\ : in STD_LOGIC;
    ce : in STD_LOGIC;
    \SRL_SIG_reg[1][7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_15 : entity is "fifo_w8_d2_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_15;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_15 is
  signal \SRL_SIG_reg_n_0_[0][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][7]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][7]\ : STD_LOGIC;
begin
\AXI_video_strm_V_data_V_1_payload_A[10]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][2]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][2]\,
      O => D(2)
    );
\AXI_video_strm_V_data_V_1_payload_A[11]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][3]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][3]\,
      O => D(3)
    );
\AXI_video_strm_V_data_V_1_payload_A[12]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][4]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][4]\,
      O => D(4)
    );
\AXI_video_strm_V_data_V_1_payload_A[13]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][5]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][5]\,
      O => D(5)
    );
\AXI_video_strm_V_data_V_1_payload_A[14]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][6]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][6]\,
      O => D(6)
    );
\AXI_video_strm_V_data_V_1_payload_A[15]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][7]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][7]\,
      O => D(7)
    );
\AXI_video_strm_V_data_V_1_payload_A[8]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][0]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][0]\,
      O => D(0)
    );
\AXI_video_strm_V_data_V_1_payload_A[9]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][1]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][1]\,
      O => D(1)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(0),
      Q => \SRL_SIG_reg_n_0_[0][0]\,
      R => '0'
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(1),
      Q => \SRL_SIG_reg_n_0_[0][1]\,
      R => '0'
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(2),
      Q => \SRL_SIG_reg_n_0_[0][2]\,
      R => '0'
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(3),
      Q => \SRL_SIG_reg_n_0_[0][3]\,
      R => '0'
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(4),
      Q => \SRL_SIG_reg_n_0_[0][4]\,
      R => '0'
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(5),
      Q => \SRL_SIG_reg_n_0_[0][5]\,
      R => '0'
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(6),
      Q => \SRL_SIG_reg_n_0_[0][6]\,
      R => '0'
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(7),
      Q => \SRL_SIG_reg_n_0_[0][7]\,
      R => '0'
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][0]\,
      Q => \SRL_SIG_reg_n_0_[1][0]\,
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][1]\,
      Q => \SRL_SIG_reg_n_0_[1][1]\,
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][2]\,
      Q => \SRL_SIG_reg_n_0_[1][2]\,
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][3]\,
      Q => \SRL_SIG_reg_n_0_[1][3]\,
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][4]\,
      Q => \SRL_SIG_reg_n_0_[1][4]\,
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][5]\,
      Q => \SRL_SIG_reg_n_0_[1][5]\,
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][6]\,
      Q => \SRL_SIG_reg_n_0_[1][6]\,
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][7]\,
      Q => \SRL_SIG_reg_n_0_[1][7]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_8 is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \mOutPtr_reg[0]\ : in STD_LOGIC;
    \mOutPtr_reg[1]\ : in STD_LOGIC;
    internal_full_n_reg : in STD_LOGIC;
    ce : in STD_LOGIC;
    \SRL_SIG_reg[1][7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_8 : entity is "fifo_w8_d2_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_8;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_8 is
  signal \SRL_SIG_reg_n_0_[0][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][7]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][7]\ : STD_LOGIC;
begin
\AXI_video_strm_V_data_V_1_payload_A[16]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][0]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][0]\,
      O => D(0)
    );
\AXI_video_strm_V_data_V_1_payload_A[17]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][1]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][1]\,
      O => D(1)
    );
\AXI_video_strm_V_data_V_1_payload_A[18]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][2]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][2]\,
      O => D(2)
    );
\AXI_video_strm_V_data_V_1_payload_A[19]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][3]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][3]\,
      O => D(3)
    );
\AXI_video_strm_V_data_V_1_payload_A[20]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][4]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][4]\,
      O => D(4)
    );
\AXI_video_strm_V_data_V_1_payload_A[21]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][5]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][5]\,
      O => D(5)
    );
\AXI_video_strm_V_data_V_1_payload_A[22]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][6]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][6]\,
      O => D(6)
    );
\AXI_video_strm_V_data_V_1_payload_A[23]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][7]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][7]\,
      O => D(7)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(0),
      Q => \SRL_SIG_reg_n_0_[0][0]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(1),
      Q => \SRL_SIG_reg_n_0_[0][1]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(2),
      Q => \SRL_SIG_reg_n_0_[0][2]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(3),
      Q => \SRL_SIG_reg_n_0_[0][3]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(4),
      Q => \SRL_SIG_reg_n_0_[0][4]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(5),
      Q => \SRL_SIG_reg_n_0_[0][5]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(6),
      Q => \SRL_SIG_reg_n_0_[0][6]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(7),
      Q => \SRL_SIG_reg_n_0_[0][7]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][0]\,
      Q => \SRL_SIG_reg_n_0_[1][0]\,
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][1]\,
      Q => \SRL_SIG_reg_n_0_[1][1]\,
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][2]\,
      Q => \SRL_SIG_reg_n_0_[1][2]\,
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][3]\,
      Q => \SRL_SIG_reg_n_0_[1][3]\,
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][4]\,
      Q => \SRL_SIG_reg_n_0_[1][4]\,
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][5]\,
      Q => \SRL_SIG_reg_n_0_[1][5]\,
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][6]\,
      Q => \SRL_SIG_reg_n_0_[1][6]\,
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][7]\,
      Q => \SRL_SIG_reg_n_0_[1][7]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_9 is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \mOutPtr_reg[0]\ : in STD_LOGIC;
    \mOutPtr_reg[1]\ : in STD_LOGIC;
    internal_full_n_reg : in STD_LOGIC;
    ce : in STD_LOGIC;
    \SRL_SIG_reg[1][7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_9 : entity is "fifo_w8_d2_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_9;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_9 is
  signal \SRL_SIG_reg_n_0_[0][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[0][7]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][0]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][1]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][2]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][3]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][4]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][5]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][6]\ : STD_LOGIC;
  signal \SRL_SIG_reg_n_0_[1][7]\ : STD_LOGIC;
begin
\AXI_video_strm_V_data_V_1_payload_A[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][2]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][2]\,
      O => D(2)
    );
\AXI_video_strm_V_data_V_1_payload_A[11]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][3]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][3]\,
      O => D(3)
    );
\AXI_video_strm_V_data_V_1_payload_A[12]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][4]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][4]\,
      O => D(4)
    );
\AXI_video_strm_V_data_V_1_payload_A[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][5]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][5]\,
      O => D(5)
    );
\AXI_video_strm_V_data_V_1_payload_A[14]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][6]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][6]\,
      O => D(6)
    );
\AXI_video_strm_V_data_V_1_payload_A[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][7]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][7]\,
      O => D(7)
    );
\AXI_video_strm_V_data_V_1_payload_A[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][0]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][0]\,
      O => D(0)
    );
\AXI_video_strm_V_data_V_1_payload_A[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \SRL_SIG_reg_n_0_[1][1]\,
      I1 => \mOutPtr_reg[0]\,
      I2 => \mOutPtr_reg[1]\,
      I3 => \SRL_SIG_reg_n_0_[0][1]\,
      O => D(1)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(0),
      Q => \SRL_SIG_reg_n_0_[0][0]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(1),
      Q => \SRL_SIG_reg_n_0_[0][1]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(2),
      Q => \SRL_SIG_reg_n_0_[0][2]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(3),
      Q => \SRL_SIG_reg_n_0_[0][3]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(4),
      Q => \SRL_SIG_reg_n_0_[0][4]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(5),
      Q => \SRL_SIG_reg_n_0_[0][5]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(6),
      Q => \SRL_SIG_reg_n_0_[0][6]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg[1][7]_0\(7),
      Q => \SRL_SIG_reg_n_0_[0][7]\,
      R => internal_full_n_reg
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][0]\,
      Q => \SRL_SIG_reg_n_0_[1][0]\,
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][1]\,
      Q => \SRL_SIG_reg_n_0_[1][1]\,
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][2]\,
      Q => \SRL_SIG_reg_n_0_[1][2]\,
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][3]\,
      Q => \SRL_SIG_reg_n_0_[1][3]\,
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][4]\,
      Q => \SRL_SIG_reg_n_0_[1][4]\,
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][5]\,
      Q => \SRL_SIG_reg_n_0_[1][5]\,
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][6]\,
      Q => \SRL_SIG_reg_n_0_[1][6]\,
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce,
      D => \SRL_SIG_reg_n_0_[0][7]\,
      Q => \SRL_SIG_reg_n_0_[1][7]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Loop_lobkb is
  port (
    start_for_Loop_loop_height_pro_U0_full_n : out STD_LOGIC;
    start_for_Loop_loop_height_pro_U0_empty_n : out STD_LOGIC;
    ap_idle : out STD_LOGIC;
    internal_full_n_reg_0 : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    start_once_reg : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    \ap_CS_fsm_reg[0]\ : in STD_LOGIC;
    internal_full_n_reg_1 : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    Loop_loop_height_pro_U0_ap_ready : in STD_LOGIC;
    start_once_reg_reg : in STD_LOGIC;
    start_once_reg_reg_0 : in STD_LOGIC;
    start_for_Mat2AXIvideo_1_U0_full_n : in STD_LOGIC;
    start_for_Mat2AXIvideo_U0_full_n : in STD_LOGIC;
    ap_rst : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Loop_lobkb;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Loop_lobkb is
  signal internal_empty_n_i_1_n_0 : STD_LOGIC;
  signal internal_full_n_i_1_n_0 : STD_LOGIC;
  signal \internal_full_n_i_2__10_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[1]\ : STD_LOGIC;
  signal \^start_for_loop_loop_height_pro_u0_empty_n\ : STD_LOGIC;
  signal \^start_for_loop_loop_height_pro_u0_full_n\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \internal_full_n_i_2__10\ : label is "soft_lutpair150";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1\ : label is "soft_lutpair150";
begin
  start_for_Loop_loop_height_pro_U0_empty_n <= \^start_for_loop_loop_height_pro_u0_empty_n\;
  start_for_Loop_loop_height_pro_U0_full_n <= \^start_for_loop_loop_height_pro_u0_full_n\;
ap_idle_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00001F00"
    )
        port map (
      I0 => \^start_for_loop_loop_height_pro_u0_full_n\,
      I1 => start_once_reg,
      I2 => ap_start,
      I3 => \ap_CS_fsm_reg[0]\,
      I4 => internal_full_n_reg_1,
      O => ap_idle
    );
internal_empty_n_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8AA0000AAAAAAAA"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_0_[1]\,
      I2 => \mOutPtr_reg_n_0_[0]\,
      I3 => Loop_loop_height_pro_U0_ap_ready,
      I4 => \^start_for_loop_loop_height_pro_u0_empty_n\,
      I5 => start_once_reg_reg,
      O => internal_empty_n_i_1_n_0
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => internal_empty_n_i_1_n_0,
      Q => \^start_for_loop_loop_height_pro_u0_empty_n\,
      R => '0'
    );
internal_full_n_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDDDDDDDD5D5D5D"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^start_for_loop_loop_height_pro_u0_full_n\,
      I2 => \internal_full_n_i_2__10_n_0\,
      I3 => Loop_loop_height_pro_U0_ap_ready,
      I4 => \^start_for_loop_loop_height_pro_u0_empty_n\,
      I5 => start_once_reg_reg,
      O => internal_full_n_i_1_n_0
    );
\internal_full_n_i_2__10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => \mOutPtr_reg_n_0_[1]\,
      O => \internal_full_n_i_2__10_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => internal_full_n_i_1_n_0,
      Q => \^start_for_loop_loop_height_pro_u0_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7787777788788888"
    )
        port map (
      I0 => \^start_for_loop_loop_height_pro_u0_empty_n\,
      I1 => Loop_loop_height_pro_U0_ap_ready,
      I2 => \^start_for_loop_loop_height_pro_u0_full_n\,
      I3 => start_once_reg,
      I4 => ap_start,
      I5 => \mOutPtr_reg_n_0_[0]\,
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BDDD4222"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => start_once_reg_reg,
      I2 => Loop_loop_height_pro_U0_ap_ready,
      I3 => \^start_for_loop_loop_height_pro_u0_empty_n\,
      I4 => \mOutPtr_reg_n_0_[1]\,
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr[1]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => \^start_for_loop_loop_height_pro_u0_empty_n\,
      I1 => start_once_reg_reg_0,
      I2 => start_for_Mat2AXIvideo_1_U0_full_n,
      I3 => start_for_Mat2AXIvideo_U0_full_n,
      O => internal_full_n_reg_0
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[0]\,
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[1]\,
      S => ap_rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIcud is
  port (
    start_for_Mat2AXIvideo_U0_full_n : out STD_LOGIC;
    start_for_Mat2AXIvideo_U0_empty_n : out STD_LOGIC;
    \mOutPtr_reg[0]_0\ : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    Mat2AXIvideo_U0_ap_ready : in STD_LOGIC;
    internal_empty_n_reg_0 : in STD_LOGIC;
    start_for_Mat2AXIvideo_1_U0_full_n : in STD_LOGIC;
    start_once_reg_reg : in STD_LOGIC;
    start_for_Loop_loop_height_pro_U0_empty_n : in STD_LOGIC;
    ap_rst : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIcud;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIcud is
  signal A : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \internal_empty_n_i_1__4_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__4_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_2__2_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \^moutptr_reg[0]_0\ : STD_LOGIC;
  signal \^start_for_mat2axivideo_u0_empty_n\ : STD_LOGIC;
  signal \^start_for_mat2axivideo_u0_full_n\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \internal_full_n_i_2__2\ : label is "soft_lutpair151";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1\ : label is "soft_lutpair151";
begin
  \mOutPtr_reg[0]_0\ <= \^moutptr_reg[0]_0\;
  start_for_Mat2AXIvideo_U0_empty_n <= \^start_for_mat2axivideo_u0_empty_n\;
  start_for_Mat2AXIvideo_U0_full_n <= \^start_for_mat2axivideo_u0_full_n\;
ap_idle_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F800"
    )
        port map (
      I0 => \^start_for_mat2axivideo_u0_full_n\,
      I1 => start_for_Mat2AXIvideo_1_U0_full_n,
      I2 => start_once_reg_reg,
      I3 => start_for_Loop_loop_height_pro_U0_empty_n,
      O => \^moutptr_reg[0]_0\
    );
\internal_empty_n_i_1__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8AA0000AAAAAAAA"
    )
        port map (
      I0 => ap_rst_n,
      I1 => A(1),
      I2 => A(0),
      I3 => Mat2AXIvideo_U0_ap_ready,
      I4 => \^start_for_mat2axivideo_u0_empty_n\,
      I5 => internal_empty_n_reg_0,
      O => \internal_empty_n_i_1__4_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__4_n_0\,
      Q => \^start_for_mat2axivideo_u0_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDDDDDDDD5D5D5D"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^start_for_mat2axivideo_u0_full_n\,
      I2 => \internal_full_n_i_2__2_n_0\,
      I3 => Mat2AXIvideo_U0_ap_ready,
      I4 => \^start_for_mat2axivideo_u0_empty_n\,
      I5 => internal_empty_n_reg_0,
      O => \internal_full_n_i_1__4_n_0\
    );
\internal_full_n_i_2__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => A(0),
      I1 => A(1),
      O => \internal_full_n_i_2__2_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__4_n_0\,
      Q => \^start_for_mat2axivideo_u0_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7787777788788888"
    )
        port map (
      I0 => \^start_for_mat2axivideo_u0_empty_n\,
      I1 => Mat2AXIvideo_U0_ap_ready,
      I2 => \^moutptr_reg[0]_0\,
      I3 => start_once_reg_reg,
      I4 => \^start_for_mat2axivideo_u0_full_n\,
      I5 => A(0),
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BDDD4222"
    )
        port map (
      I0 => A(0),
      I1 => internal_empty_n_reg_0,
      I2 => Mat2AXIvideo_U0_ap_ready,
      I3 => \^start_for_mat2axivideo_u0_empty_n\,
      I4 => A(1),
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => A(0),
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => A(1),
      S => ap_rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIdEe is
  port (
    start_for_Mat2AXIvideo_1_U0_full_n : out STD_LOGIC;
    start_for_Mat2AXIvideo_1_U0_empty_n : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    Mat2AXIvideo_1_U0_ap_ready : in STD_LOGIC;
    internal_empty_n_reg_0 : in STD_LOGIC;
    internal_full_n_reg_0 : in STD_LOGIC;
    start_once_reg_reg : in STD_LOGIC;
    ap_rst : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIdEe;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIdEe is
  signal \internal_empty_n_i_1__3_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__3_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_2__3_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[1]\ : STD_LOGIC;
  signal \^start_for_mat2axivideo_1_u0_empty_n\ : STD_LOGIC;
  signal \^start_for_mat2axivideo_1_u0_full_n\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \internal_full_n_i_2__3\ : label is "soft_lutpair152";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1\ : label is "soft_lutpair152";
begin
  start_for_Mat2AXIvideo_1_U0_empty_n <= \^start_for_mat2axivideo_1_u0_empty_n\;
  start_for_Mat2AXIvideo_1_U0_full_n <= \^start_for_mat2axivideo_1_u0_full_n\;
\internal_empty_n_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A800AA00AAAAAAAA"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_0_[1]\,
      I2 => \mOutPtr_reg_n_0_[0]\,
      I3 => \^start_for_mat2axivideo_1_u0_empty_n\,
      I4 => Mat2AXIvideo_1_U0_ap_ready,
      I5 => internal_empty_n_reg_0,
      O => \internal_empty_n_i_1__3_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__3_n_0\,
      Q => \^start_for_mat2axivideo_1_u0_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDDDDDDDD5D5D5D"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^start_for_mat2axivideo_1_u0_full_n\,
      I2 => \internal_full_n_i_2__3_n_0\,
      I3 => \^start_for_mat2axivideo_1_u0_empty_n\,
      I4 => Mat2AXIvideo_1_U0_ap_ready,
      I5 => internal_empty_n_reg_0,
      O => \internal_full_n_i_1__3_n_0\
    );
\internal_full_n_i_2__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => \mOutPtr_reg_n_0_[1]\,
      O => \internal_full_n_i_2__3_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__3_n_0\,
      Q => \^start_for_mat2axivideo_1_u0_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7787777788788888"
    )
        port map (
      I0 => Mat2AXIvideo_1_U0_ap_ready,
      I1 => \^start_for_mat2axivideo_1_u0_empty_n\,
      I2 => internal_full_n_reg_0,
      I3 => start_once_reg_reg,
      I4 => \^start_for_mat2axivideo_1_u0_full_n\,
      I5 => \mOutPtr_reg_n_0_[0]\,
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BDDD4222"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => internal_empty_n_reg_0,
      I2 => \^start_for_mat2axivideo_1_u0_empty_n\,
      I3 => Mat2AXIvideo_1_U0_ap_ready,
      I4 => \mOutPtr_reg_n_0_[1]\,
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[0]\,
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[1]\,
      S => ap_rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A is
  port (
    crop_data_data_strea_1_full_n : out STD_LOGIC;
    crop_data_data_strea_1_empty_n : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ce : in STD_LOGIC;
    Mat2AXIvideo_1_U0_img_data_stream_2_V_read : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    \SRL_SIG_reg[1][7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A is
  signal \^crop_data_data_strea_1_empty_n\ : STD_LOGIC;
  signal \^crop_data_data_strea_1_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__9_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__9_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_2__8_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1\ : label is "soft_lutpair144";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1\ : label is "soft_lutpair144";
begin
  crop_data_data_strea_1_empty_n <= \^crop_data_data_strea_1_empty_n\;
  crop_data_data_strea_1_full_n <= \^crop_data_data_strea_1_full_n\;
U_fifo_w8_d2_A_shiftReg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_15
     port map (
      D(7 downto 0) => D(7 downto 0),
      \SRL_SIG_reg[1][7]_0\(7 downto 0) => \SRL_SIG_reg[1][7]\(7 downto 0),
      ap_clk => ap_clk,
      ce => ce,
      \mOutPtr_reg[0]\ => \mOutPtr_reg_n_0_[0]\,
      \mOutPtr_reg[1]\ => \mOutPtr_reg_n_0_[1]\
    );
\internal_empty_n_i_1__9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8AAAAAA00AA00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_0_[1]\,
      I2 => \mOutPtr_reg_n_0_[0]\,
      I3 => ce,
      I4 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I5 => \^crop_data_data_strea_1_empty_n\,
      O => \internal_empty_n_i_1__9_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__9_n_0\,
      Q => \^crop_data_data_strea_1_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDFF5DDD5DDD5DDD"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^crop_data_data_strea_1_full_n\,
      I2 => \internal_full_n_i_2__8_n_0\,
      I3 => ce,
      I4 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I5 => \^crop_data_data_strea_1_empty_n\,
      O => \internal_full_n_i_1__9_n_0\
    );
\internal_full_n_i_2__8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => \mOutPtr_reg_n_0_[1]\,
      O => \internal_full_n_i_2__8_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__9_n_0\,
      Q => \^crop_data_data_strea_1_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \^crop_data_data_strea_1_empty_n\,
      I1 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I2 => ce,
      I3 => \mOutPtr_reg_n_0_[0]\,
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E7771888"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => ce,
      I2 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I3 => \^crop_data_data_strea_1_empty_n\,
      I4 => \mOutPtr_reg_n_0_[1]\,
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[0]\,
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[1]\,
      S => ap_rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_0 is
  port (
    crop_data_data_strea_2_full_n : out STD_LOGIC;
    crop_data_data_strea_2_empty_n : out STD_LOGIC;
    \p_070_1_reg_275_reg[0]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ce : in STD_LOGIC;
    Mat2AXIvideo_1_U0_img_data_stream_2_V_read : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    crop_data_data_strea_full_n : in STD_LOGIC;
    crop_data_data_strea_1_full_n : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    \SRL_SIG_reg[1][7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_0 : entity is "fifo_w8_d2_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_0 is
  signal \^crop_data_data_strea_2_empty_n\ : STD_LOGIC;
  signal \^crop_data_data_strea_2_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__10_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__10_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_2__9_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1\ : label is "soft_lutpair145";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1\ : label is "soft_lutpair145";
begin
  crop_data_data_strea_2_empty_n <= \^crop_data_data_strea_2_empty_n\;
  crop_data_data_strea_2_full_n <= \^crop_data_data_strea_2_full_n\;
U_fifo_w8_d2_A_shiftReg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_14
     port map (
      D(7 downto 0) => D(7 downto 0),
      \SRL_SIG_reg[1][7]_0\(7 downto 0) => \SRL_SIG_reg[1][7]\(7 downto 0),
      ap_clk => ap_clk,
      ce => ce,
      \mOutPtr_reg[0]\ => \mOutPtr_reg_n_0_[0]\,
      \mOutPtr_reg[1]\ => \mOutPtr_reg_n_0_[1]\
    );
\internal_empty_n_i_1__10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8AAAAAA00AA00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_0_[1]\,
      I2 => \mOutPtr_reg_n_0_[0]\,
      I3 => ce,
      I4 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I5 => \^crop_data_data_strea_2_empty_n\,
      O => \internal_empty_n_i_1__10_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__10_n_0\,
      Q => \^crop_data_data_strea_2_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDFF5DDD5DDD5DDD"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^crop_data_data_strea_2_full_n\,
      I2 => \internal_full_n_i_2__9_n_0\,
      I3 => ce,
      I4 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I5 => \^crop_data_data_strea_2_empty_n\,
      O => \internal_full_n_i_1__10_n_0\
    );
\internal_full_n_i_2__9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => \mOutPtr_reg_n_0_[1]\,
      O => \internal_full_n_i_2__9_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__10_n_0\,
      Q => \^crop_data_data_strea_2_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \^crop_data_data_strea_2_empty_n\,
      I1 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I2 => ce,
      I3 => \mOutPtr_reg_n_0_[0]\,
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E7771888"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => ce,
      I2 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I3 => \^crop_data_data_strea_2_empty_n\,
      I4 => \mOutPtr_reg_n_0_[1]\,
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[0]\,
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[1]\,
      S => ap_rst
    );
\p_070_1_reg_275[10]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^crop_data_data_strea_2_full_n\,
      I1 => Q(0),
      I2 => crop_data_data_strea_full_n,
      I3 => crop_data_data_strea_1_full_n,
      O => \p_070_1_reg_275_reg[0]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_1 is
  port (
    crop_data_data_strea_full_n : out STD_LOGIC;
    crop_data_data_strea_empty_n : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ce : in STD_LOGIC;
    Mat2AXIvideo_1_U0_img_data_stream_2_V_read : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    \SRL_SIG_reg[1][7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_1 : entity is "fifo_w8_d2_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_1 is
  signal \^crop_data_data_strea_empty_n\ : STD_LOGIC;
  signal \^crop_data_data_strea_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__8_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__8_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_2__7_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1\ : label is "soft_lutpair146";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1\ : label is "soft_lutpair146";
begin
  crop_data_data_strea_empty_n <= \^crop_data_data_strea_empty_n\;
  crop_data_data_strea_full_n <= \^crop_data_data_strea_full_n\;
U_fifo_w8_d2_A_shiftReg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_13
     port map (
      D(7 downto 0) => D(7 downto 0),
      \SRL_SIG_reg[1][7]_0\(7 downto 0) => \SRL_SIG_reg[1][7]\(7 downto 0),
      ap_clk => ap_clk,
      ce => ce,
      \mOutPtr_reg[0]\ => \mOutPtr_reg_n_0_[0]\,
      \mOutPtr_reg[1]\ => \mOutPtr_reg_n_0_[1]\
    );
\internal_empty_n_i_1__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8AAAAAA00AA00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_0_[1]\,
      I2 => \mOutPtr_reg_n_0_[0]\,
      I3 => ce,
      I4 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I5 => \^crop_data_data_strea_empty_n\,
      O => \internal_empty_n_i_1__8_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__8_n_0\,
      Q => \^crop_data_data_strea_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDFF5DDD5DDD5DDD"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^crop_data_data_strea_full_n\,
      I2 => \internal_full_n_i_2__7_n_0\,
      I3 => ce,
      I4 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I5 => \^crop_data_data_strea_empty_n\,
      O => \internal_full_n_i_1__8_n_0\
    );
\internal_full_n_i_2__7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => \mOutPtr_reg_n_0_[1]\,
      O => \internal_full_n_i_2__7_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__8_n_0\,
      Q => \^crop_data_data_strea_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \^crop_data_data_strea_empty_n\,
      I1 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I2 => ce,
      I3 => \mOutPtr_reg_n_0_[0]\,
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E7771888"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => ce,
      I2 => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      I3 => \^crop_data_data_strea_empty_n\,
      I4 => \mOutPtr_reg_n_0_[1]\,
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[0]\,
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[1]\,
      S => ap_rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_2 is
  port (
    input_data_data_stre_1_full_n : out STD_LOGIC;
    input_data_data_stre_1_empty_n : out STD_LOGIC;
    \SRL_SIG_reg[0][7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    AXIvideo2Mat_U0_img_data_stream_2_V_write : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write : in STD_LOGIC;
    ap_enable_reg_pp1_iter1_reg : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_2 : entity is "fifo_w8_d2_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_2 is
  signal \^input_data_data_stre_1_empty_n\ : STD_LOGIC;
  signal \^input_data_data_stre_1_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__1_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__1_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_2__0_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[1]\ : STD_LOGIC;
begin
  input_data_data_stre_1_empty_n <= \^input_data_data_stre_1_empty_n\;
  input_data_data_stre_1_full_n <= \^input_data_data_stre_1_full_n\;
U_fifo_w8_d2_A_shiftReg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_12
     port map (
      AXIvideo2Mat_U0_img_data_stream_2_V_write => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      D(7 downto 0) => D(7 downto 0),
      \SRL_SIG_reg[0][7]_0\(7 downto 0) => \SRL_SIG_reg[0][7]\(7 downto 0),
      ap_clk => ap_clk,
      internal_full_n_reg => \^input_data_data_stre_1_full_n\,
      \mOutPtr_reg[0]\ => \mOutPtr_reg_n_0_[0]\,
      \mOutPtr_reg[1]\ => \mOutPtr_reg_n_0_[1]\
    );
\internal_empty_n_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8AA0000AAAAAAAA"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_0_[1]\,
      I2 => \mOutPtr_reg_n_0_[0]\,
      I3 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I4 => \^input_data_data_stre_1_empty_n\,
      I5 => ap_enable_reg_pp1_iter1_reg,
      O => \internal_empty_n_i_1__1_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__1_n_0\,
      Q => \^input_data_data_stre_1_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F777FFFFF555F555"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \internal_full_n_i_2__0_n_0\,
      I2 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I3 => \^input_data_data_stre_1_empty_n\,
      I4 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      I5 => \^input_data_data_stre_1_full_n\,
      O => \internal_full_n_i_1__1_n_0\
    );
\internal_full_n_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => \mOutPtr_reg_n_0_[1]\,
      O => \internal_full_n_i_2__0_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__1_n_0\,
      Q => \^input_data_data_stre_1_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"87777888"
    )
        port map (
      I0 => \^input_data_data_stre_1_empty_n\,
      I1 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I2 => \^input_data_data_stre_1_full_n\,
      I3 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      I4 => \mOutPtr_reg_n_0_[0]\,
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EA7F7F7F15808080"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      I2 => \^input_data_data_stre_1_full_n\,
      I3 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I4 => \^input_data_data_stre_1_empty_n\,
      I5 => \mOutPtr_reg_n_0_[1]\,
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[0]\,
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[1]\,
      S => ap_rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_3 is
  port (
    input_data_data_stre_2_full_n : out STD_LOGIC;
    input_data_data_stre_2_empty_n : out STD_LOGIC;
    \SRL_SIG_reg[0][7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    AXIvideo2Mat_U0_img_data_stream_2_V_write : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write : in STD_LOGIC;
    ap_enable_reg_pp1_iter1_reg : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_3 : entity is "fifo_w8_d2_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_3 is
  signal \^input_data_data_stre_2_empty_n\ : STD_LOGIC;
  signal \^input_data_data_stre_2_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__0_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__0_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_2__1_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[1]\ : STD_LOGIC;
begin
  input_data_data_stre_2_empty_n <= \^input_data_data_stre_2_empty_n\;
  input_data_data_stre_2_full_n <= \^input_data_data_stre_2_full_n\;
U_fifo_w8_d2_A_shiftReg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_11
     port map (
      AXIvideo2Mat_U0_img_data_stream_2_V_write => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      D(7 downto 0) => D(7 downto 0),
      \SRL_SIG_reg[0][7]_0\(7 downto 0) => \SRL_SIG_reg[0][7]\(7 downto 0),
      ap_clk => ap_clk,
      internal_full_n_reg => \^input_data_data_stre_2_full_n\,
      \mOutPtr_reg[0]\ => \mOutPtr_reg_n_0_[0]\,
      \mOutPtr_reg[1]\ => \mOutPtr_reg_n_0_[1]\
    );
\internal_empty_n_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8AA0000AAAAAAAA"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_0_[1]\,
      I2 => \mOutPtr_reg_n_0_[0]\,
      I3 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I4 => \^input_data_data_stre_2_empty_n\,
      I5 => ap_enable_reg_pp1_iter1_reg,
      O => \internal_empty_n_i_1__0_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__0_n_0\,
      Q => \^input_data_data_stre_2_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F777FFFFF555F555"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \internal_full_n_i_2__1_n_0\,
      I2 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I3 => \^input_data_data_stre_2_empty_n\,
      I4 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      I5 => \^input_data_data_stre_2_full_n\,
      O => \internal_full_n_i_1__0_n_0\
    );
\internal_full_n_i_2__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => \mOutPtr_reg_n_0_[1]\,
      O => \internal_full_n_i_2__1_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__0_n_0\,
      Q => \^input_data_data_stre_2_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"87777888"
    )
        port map (
      I0 => \^input_data_data_stre_2_empty_n\,
      I1 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I2 => \^input_data_data_stre_2_full_n\,
      I3 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      I4 => \mOutPtr_reg_n_0_[0]\,
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EA7F7F7F15808080"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      I2 => \^input_data_data_stre_2_full_n\,
      I3 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I4 => \^input_data_data_stre_2_empty_n\,
      I5 => \mOutPtr_reg_n_0_[1]\,
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[0]\,
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[1]\,
      S => ap_rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_4 is
  port (
    input_data_data_stre_full_n : out STD_LOGIC;
    input_data_data_stre_empty_n : out STD_LOGIC;
    \SRL_SIG_reg[0][7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    AXIvideo2Mat_U0_img_data_stream_2_V_write : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write : in STD_LOGIC;
    ap_enable_reg_pp1_iter1_reg : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_4 : entity is "fifo_w8_d2_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_4;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_4 is
  signal \^input_data_data_stre_empty_n\ : STD_LOGIC;
  signal \^input_data_data_stre_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__2_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__2_n_0\ : STD_LOGIC;
  signal internal_full_n_i_2_n_0 : STD_LOGIC;
  signal mOutPtr : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
begin
  input_data_data_stre_empty_n <= \^input_data_data_stre_empty_n\;
  input_data_data_stre_full_n <= \^input_data_data_stre_full_n\;
U_fifo_w8_d2_A_shiftReg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_10
     port map (
      AXIvideo2Mat_U0_img_data_stream_2_V_write => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      D(7 downto 0) => D(7 downto 0),
      \SRL_SIG_reg[0][7]_0\(7 downto 0) => \SRL_SIG_reg[0][7]\(7 downto 0),
      ap_clk => ap_clk,
      internal_full_n_reg => \^input_data_data_stre_full_n\,
      mOutPtr(1 downto 0) => mOutPtr(1 downto 0)
    );
\internal_empty_n_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8AA0000AAAAAAAA"
    )
        port map (
      I0 => ap_rst_n,
      I1 => mOutPtr(1),
      I2 => mOutPtr(0),
      I3 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I4 => \^input_data_data_stre_empty_n\,
      I5 => ap_enable_reg_pp1_iter1_reg,
      O => \internal_empty_n_i_1__2_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__2_n_0\,
      Q => \^input_data_data_stre_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F777FFFFF555F555"
    )
        port map (
      I0 => ap_rst_n,
      I1 => internal_full_n_i_2_n_0,
      I2 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I3 => \^input_data_data_stre_empty_n\,
      I4 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      I5 => \^input_data_data_stre_full_n\,
      O => \internal_full_n_i_1__2_n_0\
    );
internal_full_n_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mOutPtr(0),
      I1 => mOutPtr(1),
      O => internal_full_n_i_2_n_0
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__2_n_0\,
      Q => \^input_data_data_stre_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"87777888"
    )
        port map (
      I0 => \^input_data_data_stre_empty_n\,
      I1 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I2 => \^input_data_data_stre_full_n\,
      I3 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      I4 => mOutPtr(0),
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EA7F7F7F15808080"
    )
        port map (
      I0 => mOutPtr(0),
      I1 => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      I2 => \^input_data_data_stre_full_n\,
      I3 => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      I4 => \^input_data_data_stre_empty_n\,
      I5 => mOutPtr(1),
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => mOutPtr(0),
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => mOutPtr(1),
      S => ap_rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_5 is
  port (
    passthrough_data_str_1_full_n : out STD_LOGIC;
    passthrough_data_str_1_empty_n : out STD_LOGIC;
    \p_1_reg_264_reg[0]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ce : in STD_LOGIC;
    Mat2AXIvideo_U0_img_data_stream_2_V_read : in STD_LOGIC;
    passthrough_data_str_full_n : in STD_LOGIC;
    input_data_data_stre_2_empty_n : in STD_LOGIC;
    input_data_data_stre_1_empty_n : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    internal_full_n_reg_0 : in STD_LOGIC;
    \SRL_SIG_reg[1][7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_5 : entity is "fifo_w8_d2_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_5 is
  signal \internal_empty_n_i_1__6_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__6_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_2__5_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[1]\ : STD_LOGIC;
  signal \^passthrough_data_str_1_empty_n\ : STD_LOGIC;
  signal \^passthrough_data_str_1_full_n\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1\ : label is "soft_lutpair147";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1\ : label is "soft_lutpair147";
begin
  passthrough_data_str_1_empty_n <= \^passthrough_data_str_1_empty_n\;
  passthrough_data_str_1_full_n <= \^passthrough_data_str_1_full_n\;
U_fifo_w8_d2_A_shiftReg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_9
     port map (
      D(7 downto 0) => D(7 downto 0),
      \SRL_SIG_reg[1][7]_0\(7 downto 0) => \SRL_SIG_reg[1][7]\(7 downto 0),
      ap_clk => ap_clk,
      ce => ce,
      internal_full_n_reg => internal_full_n_reg_0,
      \mOutPtr_reg[0]\ => \mOutPtr_reg_n_0_[0]\,
      \mOutPtr_reg[1]\ => \mOutPtr_reg_n_0_[1]\
    );
\internal_empty_n_i_1__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8AAAAAA00AA00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_0_[1]\,
      I2 => \mOutPtr_reg_n_0_[0]\,
      I3 => ce,
      I4 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I5 => \^passthrough_data_str_1_empty_n\,
      O => \internal_empty_n_i_1__6_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__6_n_0\,
      Q => \^passthrough_data_str_1_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDFF5DDD5DDD5DDD"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^passthrough_data_str_1_full_n\,
      I2 => \internal_full_n_i_2__5_n_0\,
      I3 => ce,
      I4 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I5 => \^passthrough_data_str_1_empty_n\,
      O => \internal_full_n_i_1__6_n_0\
    );
\internal_full_n_i_2__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => \mOutPtr_reg_n_0_[1]\,
      O => \internal_full_n_i_2__5_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__6_n_0\,
      Q => \^passthrough_data_str_1_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \^passthrough_data_str_1_empty_n\,
      I1 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I2 => ce,
      I3 => \mOutPtr_reg_n_0_[0]\,
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E7771888"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => ce,
      I2 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I3 => \^passthrough_data_str_1_empty_n\,
      I4 => \mOutPtr_reg_n_0_[1]\,
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[0]\,
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[1]\,
      S => ap_rst
    );
\p_070_2_reg_286[8]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^passthrough_data_str_1_full_n\,
      I1 => passthrough_data_str_full_n,
      I2 => input_data_data_stre_2_empty_n,
      I3 => input_data_data_stre_1_empty_n,
      O => \p_1_reg_264_reg[0]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_6 is
  port (
    passthrough_data_str_2_full_n : out STD_LOGIC;
    passthrough_data_str_2_empty_n : out STD_LOGIC;
    \p_070_1_reg_275_reg[0]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ce : in STD_LOGIC;
    Mat2AXIvideo_U0_img_data_stream_2_V_read : in STD_LOGIC;
    input_data_data_stre_empty_n : in STD_LOGIC;
    input_data_data_stre_1_empty_n : in STD_LOGIC;
    input_data_data_stre_2_empty_n : in STD_LOGIC;
    passthrough_data_str_1_full_n : in STD_LOGIC;
    passthrough_data_str_full_n : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    internal_full_n_reg_0 : in STD_LOGIC;
    \SRL_SIG_reg[1][7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_6 : entity is "fifo_w8_d2_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_6;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_6 is
  signal \internal_empty_n_i_1__5_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__5_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_2__6_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[1]\ : STD_LOGIC;
  signal \^passthrough_data_str_2_empty_n\ : STD_LOGIC;
  signal \^passthrough_data_str_2_full_n\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1\ : label is "soft_lutpair148";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1\ : label is "soft_lutpair148";
begin
  passthrough_data_str_2_empty_n <= \^passthrough_data_str_2_empty_n\;
  passthrough_data_str_2_full_n <= \^passthrough_data_str_2_full_n\;
U_fifo_w8_d2_A_shiftReg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_8
     port map (
      D(7 downto 0) => D(7 downto 0),
      \SRL_SIG_reg[1][7]_0\(7 downto 0) => \SRL_SIG_reg[1][7]\(7 downto 0),
      ap_clk => ap_clk,
      ce => ce,
      internal_full_n_reg => internal_full_n_reg_0,
      \mOutPtr_reg[0]\ => \mOutPtr_reg_n_0_[0]\,
      \mOutPtr_reg[1]\ => \mOutPtr_reg_n_0_[1]\
    );
\internal_empty_n_i_1__5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8AAAAAA00AA00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_0_[1]\,
      I2 => \mOutPtr_reg_n_0_[0]\,
      I3 => ce,
      I4 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I5 => \^passthrough_data_str_2_empty_n\,
      O => \internal_empty_n_i_1__5_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__5_n_0\,
      Q => \^passthrough_data_str_2_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDFF5DDD5DDD5DDD"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^passthrough_data_str_2_full_n\,
      I2 => \internal_full_n_i_2__6_n_0\,
      I3 => ce,
      I4 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I5 => \^passthrough_data_str_2_empty_n\,
      O => \internal_full_n_i_1__5_n_0\
    );
\internal_full_n_i_2__6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => \mOutPtr_reg_n_0_[1]\,
      O => \internal_full_n_i_2__6_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__5_n_0\,
      Q => \^passthrough_data_str_2_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \^passthrough_data_str_2_empty_n\,
      I1 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I2 => ce,
      I3 => \mOutPtr_reg_n_0_[0]\,
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E7771888"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => ce,
      I2 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I3 => \^passthrough_data_str_2_empty_n\,
      I4 => \mOutPtr_reg_n_0_[1]\,
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[0]\,
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[1]\,
      S => ap_rst
    );
\p_070_1_reg_275[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^passthrough_data_str_2_full_n\,
      I1 => input_data_data_stre_empty_n,
      I2 => input_data_data_stre_1_empty_n,
      I3 => input_data_data_stre_2_empty_n,
      I4 => passthrough_data_str_1_full_n,
      I5 => passthrough_data_str_full_n,
      O => \p_070_1_reg_275_reg[0]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_7 is
  port (
    passthrough_data_str_full_n : out STD_LOGIC;
    passthrough_data_str_empty_n : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ce : in STD_LOGIC;
    Mat2AXIvideo_U0_img_data_stream_2_V_read : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    internal_full_n_reg_0 : in STD_LOGIC;
    \SRL_SIG_reg[1][7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_7 : entity is "fifo_w8_d2_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_7;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_7 is
  signal \internal_empty_n_i_1__7_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_1__7_n_0\ : STD_LOGIC;
  signal \internal_full_n_i_2__4_n_0\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_0\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_0_[1]\ : STD_LOGIC;
  signal \^passthrough_data_str_empty_n\ : STD_LOGIC;
  signal \^passthrough_data_str_full_n\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1\ : label is "soft_lutpair149";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1\ : label is "soft_lutpair149";
begin
  passthrough_data_str_empty_n <= \^passthrough_data_str_empty_n\;
  passthrough_data_str_full_n <= \^passthrough_data_str_full_n\;
U_fifo_w8_d2_A_shiftReg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg
     port map (
      D(7 downto 0) => D(7 downto 0),
      \SRL_SIG_reg[1][7]_0\(7 downto 0) => \SRL_SIG_reg[1][7]\(7 downto 0),
      ap_clk => ap_clk,
      ce => ce,
      internal_full_n_reg => internal_full_n_reg_0,
      \mOutPtr_reg[0]\ => \mOutPtr_reg_n_0_[0]\,
      \mOutPtr_reg[1]\ => \mOutPtr_reg_n_0_[1]\
    );
\internal_empty_n_i_1__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8AAAAAA00AA00"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_0_[1]\,
      I2 => \mOutPtr_reg_n_0_[0]\,
      I3 => ce,
      I4 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I5 => \^passthrough_data_str_empty_n\,
      O => \internal_empty_n_i_1__7_n_0\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__7_n_0\,
      Q => \^passthrough_data_str_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDFF5DDD5DDD5DDD"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^passthrough_data_str_full_n\,
      I2 => \internal_full_n_i_2__4_n_0\,
      I3 => ce,
      I4 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I5 => \^passthrough_data_str_empty_n\,
      O => \internal_full_n_i_1__7_n_0\
    );
\internal_full_n_i_2__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => \mOutPtr_reg_n_0_[1]\,
      O => \internal_full_n_i_2__4_n_0\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__7_n_0\,
      Q => \^passthrough_data_str_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \^passthrough_data_str_empty_n\,
      I1 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I2 => ce,
      I3 => \mOutPtr_reg_n_0_[0]\,
      O => \mOutPtr[0]_i_1_n_0\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E7771888"
    )
        port map (
      I0 => \mOutPtr_reg_n_0_[0]\,
      I1 => ce,
      I2 => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      I3 => \^passthrough_data_str_empty_n\,
      I4 => \mOutPtr_reg_n_0_[1]\,
      O => \mOutPtr[1]_i_1_n_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[0]\,
      S => ap_rst
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_0\,
      Q => \mOutPtr_reg_n_0_[1]\,
      S => ap_rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_center_image is
  port (
    image_in_TDATA : in STD_LOGIC_VECTOR ( 23 downto 0 );
    image_in_TKEEP : in STD_LOGIC_VECTOR ( 2 downto 0 );
    image_in_TSTRB : in STD_LOGIC_VECTOR ( 2 downto 0 );
    image_in_TUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    image_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    image_in_TID : in STD_LOGIC_VECTOR ( 0 to 0 );
    image_in_TDEST : in STD_LOGIC_VECTOR ( 0 to 0 );
    image_out_TDATA : out STD_LOGIC_VECTOR ( 23 downto 0 );
    image_out_TKEEP : out STD_LOGIC_VECTOR ( 2 downto 0 );
    image_out_TSTRB : out STD_LOGIC_VECTOR ( 2 downto 0 );
    image_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    image_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    image_out_TID : out STD_LOGIC_VECTOR ( 0 to 0 );
    image_out_TDEST : out STD_LOGIC_VECTOR ( 0 to 0 );
    crop_out_TDATA : out STD_LOGIC_VECTOR ( 23 downto 0 );
    crop_out_TKEEP : out STD_LOGIC_VECTOR ( 2 downto 0 );
    crop_out_TSTRB : out STD_LOGIC_VECTOR ( 2 downto 0 );
    crop_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    crop_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    crop_out_TID : out STD_LOGIC_VECTOR ( 0 to 0 );
    crop_out_TDEST : out STD_LOGIC_VECTOR ( 0 to 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    image_in_TVALID : in STD_LOGIC;
    image_in_TREADY : out STD_LOGIC;
    image_out_TVALID : out STD_LOGIC;
    image_out_TREADY : in STD_LOGIC;
    ap_done : out STD_LOGIC;
    crop_out_TVALID : out STD_LOGIC;
    crop_out_TREADY : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_ready : out STD_LOGIC;
    ap_idle : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_center_image;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_center_image is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal AXIvideo2Mat_U0_img_data_stream_2_V_write : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_10 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_11 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_12 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_13 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_14 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_15 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_16 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_17 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_26 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_27 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_28 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_29 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_30 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_31 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_32 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_33 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_4 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_5 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_7 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_8 : STD_LOGIC;
  signal AXIvideo2Mat_U0_n_9 : STD_LOGIC;
  signal Loop_loop_height_pro_U0_ap_ready : STD_LOGIC;
  signal Loop_loop_height_pro_U0_n_0 : STD_LOGIC;
  signal Loop_loop_height_pro_U0_n_11 : STD_LOGIC;
  signal Loop_loop_height_pro_U0_n_12 : STD_LOGIC;
  signal Loop_loop_height_pro_U0_n_13 : STD_LOGIC;
  signal Loop_loop_height_pro_U0_n_3 : STD_LOGIC;
  signal Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write : STD_LOGIC;
  signal Mat2AXIvideo_1_U0_ap_ready : STD_LOGIC;
  signal Mat2AXIvideo_1_U0_img_data_stream_2_V_read : STD_LOGIC;
  signal Mat2AXIvideo_1_U0_n_2 : STD_LOGIC;
  signal Mat2AXIvideo_U0_ap_ready : STD_LOGIC;
  signal Mat2AXIvideo_U0_img_data_stream_2_V_read : STD_LOGIC;
  signal Mat2AXIvideo_U0_n_3 : STD_LOGIC;
  signal Mat2AXIvideo_U0_n_5 : STD_LOGIC;
  signal ap_CS_fsm_state4 : STD_LOGIC;
  signal ap_done_reg : STD_LOGIC;
  signal ap_rst : STD_LOGIC;
  signal ce : STD_LOGIC;
  signal ce_0 : STD_LOGIC;
  signal ce_1 : STD_LOGIC;
  signal ce_2 : STD_LOGIC;
  signal ce_3 : STD_LOGIC;
  signal ce_4 : STD_LOGIC;
  signal crop_data_data_strea_1_empty_n : STD_LOGIC;
  signal crop_data_data_strea_1_full_n : STD_LOGIC;
  signal crop_data_data_strea_2_U_n_2 : STD_LOGIC;
  signal crop_data_data_strea_2_empty_n : STD_LOGIC;
  signal crop_data_data_strea_2_full_n : STD_LOGIC;
  signal crop_data_data_strea_empty_n : STD_LOGIC;
  signal crop_data_data_strea_full_n : STD_LOGIC;
  signal data : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal input_data_data_stre_1_U_n_2 : STD_LOGIC;
  signal input_data_data_stre_1_U_n_3 : STD_LOGIC;
  signal input_data_data_stre_1_U_n_4 : STD_LOGIC;
  signal input_data_data_stre_1_U_n_5 : STD_LOGIC;
  signal input_data_data_stre_1_U_n_6 : STD_LOGIC;
  signal input_data_data_stre_1_U_n_7 : STD_LOGIC;
  signal input_data_data_stre_1_U_n_8 : STD_LOGIC;
  signal input_data_data_stre_1_U_n_9 : STD_LOGIC;
  signal input_data_data_stre_1_empty_n : STD_LOGIC;
  signal input_data_data_stre_1_full_n : STD_LOGIC;
  signal input_data_data_stre_2_U_n_2 : STD_LOGIC;
  signal input_data_data_stre_2_U_n_3 : STD_LOGIC;
  signal input_data_data_stre_2_U_n_4 : STD_LOGIC;
  signal input_data_data_stre_2_U_n_5 : STD_LOGIC;
  signal input_data_data_stre_2_U_n_6 : STD_LOGIC;
  signal input_data_data_stre_2_U_n_7 : STD_LOGIC;
  signal input_data_data_stre_2_U_n_8 : STD_LOGIC;
  signal input_data_data_stre_2_U_n_9 : STD_LOGIC;
  signal input_data_data_stre_2_empty_n : STD_LOGIC;
  signal input_data_data_stre_2_full_n : STD_LOGIC;
  signal input_data_data_stre_U_n_2 : STD_LOGIC;
  signal input_data_data_stre_U_n_3 : STD_LOGIC;
  signal input_data_data_stre_U_n_4 : STD_LOGIC;
  signal input_data_data_stre_U_n_5 : STD_LOGIC;
  signal input_data_data_stre_U_n_6 : STD_LOGIC;
  signal input_data_data_stre_U_n_7 : STD_LOGIC;
  signal input_data_data_stre_U_n_8 : STD_LOGIC;
  signal input_data_data_stre_U_n_9 : STD_LOGIC;
  signal input_data_data_stre_empty_n : STD_LOGIC;
  signal input_data_data_stre_full_n : STD_LOGIC;
  signal passthrough_data_str_1_U_n_2 : STD_LOGIC;
  signal passthrough_data_str_1_empty_n : STD_LOGIC;
  signal passthrough_data_str_1_full_n : STD_LOGIC;
  signal passthrough_data_str_2_U_n_2 : STD_LOGIC;
  signal passthrough_data_str_2_empty_n : STD_LOGIC;
  signal passthrough_data_str_2_full_n : STD_LOGIC;
  signal passthrough_data_str_empty_n : STD_LOGIC;
  signal passthrough_data_str_full_n : STD_LOGIC;
  signal start_for_Loop_lobkb_U_n_3 : STD_LOGIC;
  signal start_for_Loop_loop_height_pro_U0_empty_n : STD_LOGIC;
  signal start_for_Loop_loop_height_pro_U0_full_n : STD_LOGIC;
  signal start_for_Mat2AXIcud_U_n_2 : STD_LOGIC;
  signal start_for_Mat2AXIvideo_1_U0_empty_n : STD_LOGIC;
  signal start_for_Mat2AXIvideo_1_U0_full_n : STD_LOGIC;
  signal start_for_Mat2AXIvideo_U0_empty_n : STD_LOGIC;
  signal start_for_Mat2AXIvideo_U0_full_n : STD_LOGIC;
  signal start_once_reg : STD_LOGIC;
  signal tmp_data_V_fu_230_p4 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal tmp_data_V_fu_234_p4 : STD_LOGIC_VECTOR ( 23 downto 0 );
begin
  crop_out_TDEST(0) <= \<const0>\;
  crop_out_TID(0) <= \<const0>\;
  crop_out_TKEEP(2) <= \<const1>\;
  crop_out_TKEEP(1) <= \<const1>\;
  crop_out_TKEEP(0) <= \<const1>\;
  crop_out_TSTRB(2) <= \<const0>\;
  crop_out_TSTRB(1) <= \<const0>\;
  crop_out_TSTRB(0) <= \<const0>\;
  image_out_TDEST(0) <= \<const0>\;
  image_out_TID(0) <= \<const0>\;
  image_out_TKEEP(2) <= \<const1>\;
  image_out_TKEEP(1) <= \<const1>\;
  image_out_TKEEP(0) <= \<const1>\;
  image_out_TSTRB(2) <= \<const0>\;
  image_out_TSTRB(1) <= \<const0>\;
  image_out_TSTRB(0) <= \<const0>\;
AXIvideo2Mat_U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIvideo2Mat
     port map (
      AXIvideo2Mat_U0_img_data_stream_2_V_write => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      D(7) => AXIvideo2Mat_U0_n_10,
      D(6) => AXIvideo2Mat_U0_n_11,
      D(5) => AXIvideo2Mat_U0_n_12,
      D(4) => AXIvideo2Mat_U0_n_13,
      D(3) => AXIvideo2Mat_U0_n_14,
      D(2) => AXIvideo2Mat_U0_n_15,
      D(1) => AXIvideo2Mat_U0_n_16,
      D(0) => AXIvideo2Mat_U0_n_17,
      Q(0) => AXIvideo2Mat_U0_n_4,
      \SRL_SIG_reg[0][7]\(7 downto 0) => data(7 downto 0),
      \SRL_SIG_reg[0][7]_0\(7) => AXIvideo2Mat_U0_n_26,
      \SRL_SIG_reg[0][7]_0\(6) => AXIvideo2Mat_U0_n_27,
      \SRL_SIG_reg[0][7]_0\(5) => AXIvideo2Mat_U0_n_28,
      \SRL_SIG_reg[0][7]_0\(4) => AXIvideo2Mat_U0_n_29,
      \SRL_SIG_reg[0][7]_0\(3) => AXIvideo2Mat_U0_n_30,
      \SRL_SIG_reg[0][7]_0\(2) => AXIvideo2Mat_U0_n_31,
      \SRL_SIG_reg[0][7]_0\(1) => AXIvideo2Mat_U0_n_32,
      \SRL_SIG_reg[0][7]_0\(0) => AXIvideo2Mat_U0_n_33,
      ap_clk => ap_clk,
      ap_ready => ap_ready,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      ap_start => ap_start,
      image_in_TDATA(23 downto 0) => image_in_TDATA(23 downto 0),
      image_in_TLAST(0) => image_in_TLAST(0),
      image_in_TREADY => image_in_TREADY,
      image_in_TUSER(0) => image_in_TUSER(0),
      image_in_TVALID => image_in_TVALID,
      input_data_data_stre_1_full_n => input_data_data_stre_1_full_n,
      input_data_data_stre_2_full_n => input_data_data_stre_2_full_n,
      input_data_data_stre_full_n => input_data_data_stre_full_n,
      internal_empty_n_reg => AXIvideo2Mat_U0_n_7,
      internal_empty_n_reg_0 => AXIvideo2Mat_U0_n_8,
      internal_empty_n_reg_1 => AXIvideo2Mat_U0_n_9,
      \mOutPtr_reg[1]\ => AXIvideo2Mat_U0_n_5,
      start_for_Loop_loop_height_pro_U0_full_n => start_for_Loop_loop_height_pro_U0_full_n,
      start_once_reg => start_once_reg
    );
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
Loop_loop_height_pro_U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Loop_loop_height_pro
     port map (
      Loop_loop_height_pro_U0_ap_ready => Loop_loop_height_pro_U0_ap_ready,
      Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      Q(1) => ap_CS_fsm_state4,
      Q(0) => Loop_loop_height_pro_U0_n_3,
      \SRL_SIG_reg[0][7]\ => Loop_loop_height_pro_U0_n_11,
      \SRL_SIG_reg[0][7]_0\ => Loop_loop_height_pro_U0_n_12,
      \SRL_SIG_reg[0][7]_1\ => Loop_loop_height_pro_U0_n_13,
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ce => ce_4,
      ce_0 => ce_3,
      ce_1 => ce_2,
      ce_2 => ce_1,
      ce_3 => ce_0,
      ce_4 => ce,
      crop_data_data_strea_1_full_n => crop_data_data_strea_1_full_n,
      crop_data_data_strea_2_full_n => crop_data_data_strea_2_full_n,
      crop_data_data_strea_full_n => crop_data_data_strea_full_n,
      input_data_data_stre_empty_n => input_data_data_stre_empty_n,
      internal_full_n_reg => start_for_Mat2AXIcud_U_n_2,
      internal_full_n_reg_0 => passthrough_data_str_1_U_n_2,
      internal_full_n_reg_1 => passthrough_data_str_2_U_n_2,
      internal_full_n_reg_2 => crop_data_data_strea_2_U_n_2,
      passthrough_data_str_1_full_n => passthrough_data_str_1_full_n,
      passthrough_data_str_2_full_n => passthrough_data_str_2_full_n,
      passthrough_data_str_full_n => passthrough_data_str_full_n,
      start_for_Loop_loop_height_pro_U0_empty_n => start_for_Loop_loop_height_pro_U0_empty_n,
      start_for_Mat2AXIvideo_1_U0_full_n => start_for_Mat2AXIvideo_1_U0_full_n,
      start_for_Mat2AXIvideo_U0_full_n => start_for_Mat2AXIvideo_U0_full_n,
      start_once_reg_reg_0 => Loop_loop_height_pro_U0_n_0
    );
Mat2AXIvideo_1_U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo_1
     port map (
      D(23 downto 0) => tmp_data_V_fu_230_p4(23 downto 0),
      Mat2AXIvideo_1_U0_ap_ready => Mat2AXIvideo_1_U0_ap_ready,
      Mat2AXIvideo_1_U0_img_data_stream_2_V_read => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      Q(0) => Mat2AXIvideo_1_U0_n_2,
      ap_clk => ap_clk,
      ap_done_reg => ap_done_reg,
      ap_done_reg_reg_0 => Mat2AXIvideo_U0_n_3,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      crop_data_data_strea_1_empty_n => crop_data_data_strea_1_empty_n,
      crop_data_data_strea_2_empty_n => crop_data_data_strea_2_empty_n,
      crop_data_data_strea_empty_n => crop_data_data_strea_empty_n,
      crop_out_TDATA(23 downto 0) => crop_out_TDATA(23 downto 0),
      crop_out_TLAST(0) => crop_out_TLAST(0),
      crop_out_TREADY => crop_out_TREADY,
      crop_out_TUSER(0) => crop_out_TUSER(0),
      crop_out_TVALID => crop_out_TVALID,
      start_for_Mat2AXIvideo_1_U0_empty_n => start_for_Mat2AXIvideo_1_U0_empty_n
    );
Mat2AXIvideo_U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo
     port map (
      D(23 downto 0) => tmp_data_V_fu_234_p4(23 downto 0),
      Mat2AXIvideo_1_U0_ap_ready => Mat2AXIvideo_1_U0_ap_ready,
      Mat2AXIvideo_U0_ap_ready => Mat2AXIvideo_U0_ap_ready,
      Mat2AXIvideo_U0_img_data_stream_2_V_read => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      Q(0) => Mat2AXIvideo_1_U0_n_2,
      \ap_CS_fsm_reg[0]_0\(0) => AXIvideo2Mat_U0_n_4,
      \ap_CS_fsm_reg[0]_1\(0) => Loop_loop_height_pro_U0_n_3,
      ap_clk => ap_clk,
      ap_done => ap_done,
      ap_done_reg => ap_done_reg,
      ap_done_reg_reg_0 => Mat2AXIvideo_U0_n_3,
      ap_idle => Mat2AXIvideo_U0_n_5,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      image_out_TDATA(23 downto 0) => image_out_TDATA(23 downto 0),
      image_out_TLAST(0) => image_out_TLAST(0),
      image_out_TREADY => image_out_TREADY,
      image_out_TUSER(0) => image_out_TUSER(0),
      image_out_TVALID => image_out_TVALID,
      passthrough_data_str_1_empty_n => passthrough_data_str_1_empty_n,
      passthrough_data_str_2_empty_n => passthrough_data_str_2_empty_n,
      passthrough_data_str_empty_n => passthrough_data_str_empty_n,
      start_for_Mat2AXIvideo_1_U0_empty_n => start_for_Mat2AXIvideo_1_U0_empty_n,
      start_for_Mat2AXIvideo_U0_empty_n => start_for_Mat2AXIvideo_U0_empty_n
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
crop_data_data_strea_1_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A
     port map (
      D(7 downto 0) => tmp_data_V_fu_230_p4(15 downto 8),
      Mat2AXIvideo_1_U0_img_data_stream_2_V_read => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      \SRL_SIG_reg[1][7]\(7) => input_data_data_stre_1_U_n_2,
      \SRL_SIG_reg[1][7]\(6) => input_data_data_stre_1_U_n_3,
      \SRL_SIG_reg[1][7]\(5) => input_data_data_stre_1_U_n_4,
      \SRL_SIG_reg[1][7]\(4) => input_data_data_stre_1_U_n_5,
      \SRL_SIG_reg[1][7]\(3) => input_data_data_stre_1_U_n_6,
      \SRL_SIG_reg[1][7]\(2) => input_data_data_stre_1_U_n_7,
      \SRL_SIG_reg[1][7]\(1) => input_data_data_stre_1_U_n_8,
      \SRL_SIG_reg[1][7]\(0) => input_data_data_stre_1_U_n_9,
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      ce => ce_0,
      crop_data_data_strea_1_empty_n => crop_data_data_strea_1_empty_n,
      crop_data_data_strea_1_full_n => crop_data_data_strea_1_full_n
    );
crop_data_data_strea_2_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_0
     port map (
      D(7 downto 0) => tmp_data_V_fu_230_p4(23 downto 16),
      Mat2AXIvideo_1_U0_img_data_stream_2_V_read => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      Q(0) => ap_CS_fsm_state4,
      \SRL_SIG_reg[1][7]\(7) => input_data_data_stre_2_U_n_2,
      \SRL_SIG_reg[1][7]\(6) => input_data_data_stre_2_U_n_3,
      \SRL_SIG_reg[1][7]\(5) => input_data_data_stre_2_U_n_4,
      \SRL_SIG_reg[1][7]\(4) => input_data_data_stre_2_U_n_5,
      \SRL_SIG_reg[1][7]\(3) => input_data_data_stre_2_U_n_6,
      \SRL_SIG_reg[1][7]\(2) => input_data_data_stre_2_U_n_7,
      \SRL_SIG_reg[1][7]\(1) => input_data_data_stre_2_U_n_8,
      \SRL_SIG_reg[1][7]\(0) => input_data_data_stre_2_U_n_9,
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      ce => ce,
      crop_data_data_strea_1_full_n => crop_data_data_strea_1_full_n,
      crop_data_data_strea_2_empty_n => crop_data_data_strea_2_empty_n,
      crop_data_data_strea_2_full_n => crop_data_data_strea_2_full_n,
      crop_data_data_strea_full_n => crop_data_data_strea_full_n,
      \p_070_1_reg_275_reg[0]\ => crop_data_data_strea_2_U_n_2
    );
crop_data_data_strea_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_1
     port map (
      D(7 downto 0) => tmp_data_V_fu_230_p4(7 downto 0),
      Mat2AXIvideo_1_U0_img_data_stream_2_V_read => Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
      \SRL_SIG_reg[1][7]\(7) => input_data_data_stre_U_n_2,
      \SRL_SIG_reg[1][7]\(6) => input_data_data_stre_U_n_3,
      \SRL_SIG_reg[1][7]\(5) => input_data_data_stre_U_n_4,
      \SRL_SIG_reg[1][7]\(4) => input_data_data_stre_U_n_5,
      \SRL_SIG_reg[1][7]\(3) => input_data_data_stre_U_n_6,
      \SRL_SIG_reg[1][7]\(2) => input_data_data_stre_U_n_7,
      \SRL_SIG_reg[1][7]\(1) => input_data_data_stre_U_n_8,
      \SRL_SIG_reg[1][7]\(0) => input_data_data_stre_U_n_9,
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      ce => ce_1,
      crop_data_data_strea_empty_n => crop_data_data_strea_empty_n,
      crop_data_data_strea_full_n => crop_data_data_strea_full_n
    );
input_data_data_stre_1_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_2
     port map (
      AXIvideo2Mat_U0_img_data_stream_2_V_write => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      D(7 downto 0) => data(7 downto 0),
      Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      \SRL_SIG_reg[0][7]\(7) => input_data_data_stre_1_U_n_2,
      \SRL_SIG_reg[0][7]\(6) => input_data_data_stre_1_U_n_3,
      \SRL_SIG_reg[0][7]\(5) => input_data_data_stre_1_U_n_4,
      \SRL_SIG_reg[0][7]\(4) => input_data_data_stre_1_U_n_5,
      \SRL_SIG_reg[0][7]\(3) => input_data_data_stre_1_U_n_6,
      \SRL_SIG_reg[0][7]\(2) => input_data_data_stre_1_U_n_7,
      \SRL_SIG_reg[0][7]\(1) => input_data_data_stre_1_U_n_8,
      \SRL_SIG_reg[0][7]\(0) => input_data_data_stre_1_U_n_9,
      ap_clk => ap_clk,
      ap_enable_reg_pp1_iter1_reg => AXIvideo2Mat_U0_n_8,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      input_data_data_stre_1_empty_n => input_data_data_stre_1_empty_n,
      input_data_data_stre_1_full_n => input_data_data_stre_1_full_n
    );
input_data_data_stre_2_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_3
     port map (
      AXIvideo2Mat_U0_img_data_stream_2_V_write => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      D(7) => AXIvideo2Mat_U0_n_10,
      D(6) => AXIvideo2Mat_U0_n_11,
      D(5) => AXIvideo2Mat_U0_n_12,
      D(4) => AXIvideo2Mat_U0_n_13,
      D(3) => AXIvideo2Mat_U0_n_14,
      D(2) => AXIvideo2Mat_U0_n_15,
      D(1) => AXIvideo2Mat_U0_n_16,
      D(0) => AXIvideo2Mat_U0_n_17,
      Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      \SRL_SIG_reg[0][7]\(7) => input_data_data_stre_2_U_n_2,
      \SRL_SIG_reg[0][7]\(6) => input_data_data_stre_2_U_n_3,
      \SRL_SIG_reg[0][7]\(5) => input_data_data_stre_2_U_n_4,
      \SRL_SIG_reg[0][7]\(4) => input_data_data_stre_2_U_n_5,
      \SRL_SIG_reg[0][7]\(3) => input_data_data_stre_2_U_n_6,
      \SRL_SIG_reg[0][7]\(2) => input_data_data_stre_2_U_n_7,
      \SRL_SIG_reg[0][7]\(1) => input_data_data_stre_2_U_n_8,
      \SRL_SIG_reg[0][7]\(0) => input_data_data_stre_2_U_n_9,
      ap_clk => ap_clk,
      ap_enable_reg_pp1_iter1_reg => AXIvideo2Mat_U0_n_7,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      input_data_data_stre_2_empty_n => input_data_data_stre_2_empty_n,
      input_data_data_stre_2_full_n => input_data_data_stre_2_full_n
    );
input_data_data_stre_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_4
     port map (
      AXIvideo2Mat_U0_img_data_stream_2_V_write => AXIvideo2Mat_U0_img_data_stream_2_V_write,
      D(7) => AXIvideo2Mat_U0_n_26,
      D(6) => AXIvideo2Mat_U0_n_27,
      D(5) => AXIvideo2Mat_U0_n_28,
      D(4) => AXIvideo2Mat_U0_n_29,
      D(3) => AXIvideo2Mat_U0_n_30,
      D(2) => AXIvideo2Mat_U0_n_31,
      D(1) => AXIvideo2Mat_U0_n_32,
      D(0) => AXIvideo2Mat_U0_n_33,
      Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write => Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
      \SRL_SIG_reg[0][7]\(7) => input_data_data_stre_U_n_2,
      \SRL_SIG_reg[0][7]\(6) => input_data_data_stre_U_n_3,
      \SRL_SIG_reg[0][7]\(5) => input_data_data_stre_U_n_4,
      \SRL_SIG_reg[0][7]\(4) => input_data_data_stre_U_n_5,
      \SRL_SIG_reg[0][7]\(3) => input_data_data_stre_U_n_6,
      \SRL_SIG_reg[0][7]\(2) => input_data_data_stre_U_n_7,
      \SRL_SIG_reg[0][7]\(1) => input_data_data_stre_U_n_8,
      \SRL_SIG_reg[0][7]\(0) => input_data_data_stre_U_n_9,
      ap_clk => ap_clk,
      ap_enable_reg_pp1_iter1_reg => AXIvideo2Mat_U0_n_9,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      input_data_data_stre_empty_n => input_data_data_stre_empty_n,
      input_data_data_stre_full_n => input_data_data_stre_full_n
    );
passthrough_data_str_1_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_5
     port map (
      D(7 downto 0) => tmp_data_V_fu_234_p4(15 downto 8),
      Mat2AXIvideo_U0_img_data_stream_2_V_read => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      \SRL_SIG_reg[1][7]\(7) => input_data_data_stre_1_U_n_2,
      \SRL_SIG_reg[1][7]\(6) => input_data_data_stre_1_U_n_3,
      \SRL_SIG_reg[1][7]\(5) => input_data_data_stre_1_U_n_4,
      \SRL_SIG_reg[1][7]\(4) => input_data_data_stre_1_U_n_5,
      \SRL_SIG_reg[1][7]\(3) => input_data_data_stre_1_U_n_6,
      \SRL_SIG_reg[1][7]\(2) => input_data_data_stre_1_U_n_7,
      \SRL_SIG_reg[1][7]\(1) => input_data_data_stre_1_U_n_8,
      \SRL_SIG_reg[1][7]\(0) => input_data_data_stre_1_U_n_9,
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      ce => ce_3,
      input_data_data_stre_1_empty_n => input_data_data_stre_1_empty_n,
      input_data_data_stre_2_empty_n => input_data_data_stre_2_empty_n,
      internal_full_n_reg_0 => Loop_loop_height_pro_U0_n_12,
      \p_1_reg_264_reg[0]\ => passthrough_data_str_1_U_n_2,
      passthrough_data_str_1_empty_n => passthrough_data_str_1_empty_n,
      passthrough_data_str_1_full_n => passthrough_data_str_1_full_n,
      passthrough_data_str_full_n => passthrough_data_str_full_n
    );
passthrough_data_str_2_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_6
     port map (
      D(7 downto 0) => tmp_data_V_fu_234_p4(23 downto 16),
      Mat2AXIvideo_U0_img_data_stream_2_V_read => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      \SRL_SIG_reg[1][7]\(7) => input_data_data_stre_2_U_n_2,
      \SRL_SIG_reg[1][7]\(6) => input_data_data_stre_2_U_n_3,
      \SRL_SIG_reg[1][7]\(5) => input_data_data_stre_2_U_n_4,
      \SRL_SIG_reg[1][7]\(4) => input_data_data_stre_2_U_n_5,
      \SRL_SIG_reg[1][7]\(3) => input_data_data_stre_2_U_n_6,
      \SRL_SIG_reg[1][7]\(2) => input_data_data_stre_2_U_n_7,
      \SRL_SIG_reg[1][7]\(1) => input_data_data_stre_2_U_n_8,
      \SRL_SIG_reg[1][7]\(0) => input_data_data_stre_2_U_n_9,
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      ce => ce_4,
      input_data_data_stre_1_empty_n => input_data_data_stre_1_empty_n,
      input_data_data_stre_2_empty_n => input_data_data_stre_2_empty_n,
      input_data_data_stre_empty_n => input_data_data_stre_empty_n,
      internal_full_n_reg_0 => Loop_loop_height_pro_U0_n_11,
      \p_070_1_reg_275_reg[0]\ => passthrough_data_str_2_U_n_2,
      passthrough_data_str_1_full_n => passthrough_data_str_1_full_n,
      passthrough_data_str_2_empty_n => passthrough_data_str_2_empty_n,
      passthrough_data_str_2_full_n => passthrough_data_str_2_full_n,
      passthrough_data_str_full_n => passthrough_data_str_full_n
    );
passthrough_data_str_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_7
     port map (
      D(7 downto 0) => tmp_data_V_fu_234_p4(7 downto 0),
      Mat2AXIvideo_U0_img_data_stream_2_V_read => Mat2AXIvideo_U0_img_data_stream_2_V_read,
      \SRL_SIG_reg[1][7]\(7) => input_data_data_stre_U_n_2,
      \SRL_SIG_reg[1][7]\(6) => input_data_data_stre_U_n_3,
      \SRL_SIG_reg[1][7]\(5) => input_data_data_stre_U_n_4,
      \SRL_SIG_reg[1][7]\(4) => input_data_data_stre_U_n_5,
      \SRL_SIG_reg[1][7]\(3) => input_data_data_stre_U_n_6,
      \SRL_SIG_reg[1][7]\(2) => input_data_data_stre_U_n_7,
      \SRL_SIG_reg[1][7]\(1) => input_data_data_stre_U_n_8,
      \SRL_SIG_reg[1][7]\(0) => input_data_data_stre_U_n_9,
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      ce => ce_2,
      internal_full_n_reg_0 => Loop_loop_height_pro_U0_n_13,
      passthrough_data_str_empty_n => passthrough_data_str_empty_n,
      passthrough_data_str_full_n => passthrough_data_str_full_n
    );
start_for_Loop_lobkb_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Loop_lobkb
     port map (
      Loop_loop_height_pro_U0_ap_ready => Loop_loop_height_pro_U0_ap_ready,
      \ap_CS_fsm_reg[0]\ => Mat2AXIvideo_U0_n_5,
      ap_clk => ap_clk,
      ap_idle => ap_idle,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      ap_start => ap_start,
      internal_full_n_reg_0 => start_for_Loop_lobkb_U_n_3,
      internal_full_n_reg_1 => start_for_Mat2AXIcud_U_n_2,
      start_for_Loop_loop_height_pro_U0_empty_n => start_for_Loop_loop_height_pro_U0_empty_n,
      start_for_Loop_loop_height_pro_U0_full_n => start_for_Loop_loop_height_pro_U0_full_n,
      start_for_Mat2AXIvideo_1_U0_full_n => start_for_Mat2AXIvideo_1_U0_full_n,
      start_for_Mat2AXIvideo_U0_full_n => start_for_Mat2AXIvideo_U0_full_n,
      start_once_reg => start_once_reg,
      start_once_reg_reg => AXIvideo2Mat_U0_n_5,
      start_once_reg_reg_0 => Loop_loop_height_pro_U0_n_0
    );
start_for_Mat2AXIcud_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIcud
     port map (
      Mat2AXIvideo_U0_ap_ready => Mat2AXIvideo_U0_ap_ready,
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      internal_empty_n_reg_0 => start_for_Loop_lobkb_U_n_3,
      \mOutPtr_reg[0]_0\ => start_for_Mat2AXIcud_U_n_2,
      start_for_Loop_loop_height_pro_U0_empty_n => start_for_Loop_loop_height_pro_U0_empty_n,
      start_for_Mat2AXIvideo_1_U0_full_n => start_for_Mat2AXIvideo_1_U0_full_n,
      start_for_Mat2AXIvideo_U0_empty_n => start_for_Mat2AXIvideo_U0_empty_n,
      start_for_Mat2AXIvideo_U0_full_n => start_for_Mat2AXIvideo_U0_full_n,
      start_once_reg_reg => Loop_loop_height_pro_U0_n_0
    );
start_for_Mat2AXIdEe_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIdEe
     port map (
      Mat2AXIvideo_1_U0_ap_ready => Mat2AXIvideo_1_U0_ap_ready,
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ap_rst_n => ap_rst_n,
      internal_empty_n_reg_0 => start_for_Loop_lobkb_U_n_3,
      internal_full_n_reg_0 => start_for_Mat2AXIcud_U_n_2,
      start_for_Mat2AXIvideo_1_U0_empty_n => start_for_Mat2AXIvideo_1_U0_empty_n,
      start_for_Mat2AXIvideo_1_U0_full_n => start_for_Mat2AXIvideo_1_U0_full_n,
      start_once_reg_reg => Loop_loop_height_pro_U0_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    image_in_TVALID : in STD_LOGIC;
    image_in_TREADY : out STD_LOGIC;
    image_in_TDATA : in STD_LOGIC_VECTOR ( 23 downto 0 );
    image_in_TKEEP : in STD_LOGIC_VECTOR ( 2 downto 0 );
    image_in_TSTRB : in STD_LOGIC_VECTOR ( 2 downto 0 );
    image_in_TUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    image_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    image_in_TID : in STD_LOGIC_VECTOR ( 0 to 0 );
    image_in_TDEST : in STD_LOGIC_VECTOR ( 0 to 0 );
    image_out_TVALID : out STD_LOGIC;
    image_out_TREADY : in STD_LOGIC;
    image_out_TDATA : out STD_LOGIC_VECTOR ( 23 downto 0 );
    image_out_TKEEP : out STD_LOGIC_VECTOR ( 2 downto 0 );
    image_out_TSTRB : out STD_LOGIC_VECTOR ( 2 downto 0 );
    image_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    image_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    image_out_TID : out STD_LOGIC_VECTOR ( 0 to 0 );
    image_out_TDEST : out STD_LOGIC_VECTOR ( 0 to 0 );
    crop_out_TVALID : out STD_LOGIC;
    crop_out_TREADY : in STD_LOGIC;
    crop_out_TDATA : out STD_LOGIC_VECTOR ( 23 downto 0 );
    crop_out_TKEEP : out STD_LOGIC_VECTOR ( 2 downto 0 );
    crop_out_TSTRB : out STD_LOGIC_VECTOR ( 2 downto 0 );
    crop_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    crop_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    crop_out_TID : out STD_LOGIC_VECTOR ( 0 to 0 );
    crop_out_TDEST : out STD_LOGIC_VECTOR ( 0 to 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ap_done : out STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_ready : out STD_LOGIC;
    ap_idle : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "system_center_image_0_0,center_image,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "HLS";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "center_image,Vivado 2018.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute x_interface_info : string;
  attribute x_interface_info of ap_clk : signal is "xilinx.com:signal:clock:1.0 ap_clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of ap_clk : signal is "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF image_in:image_out:crop_out, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  attribute x_interface_info of ap_done : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl done";
  attribute x_interface_parameter of ap_done : signal is "XIL_INTERFACENAME ap_ctrl, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {done {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} start {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} ready {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} idle {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}";
  attribute x_interface_info of ap_idle : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl idle";
  attribute x_interface_info of ap_ready : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ready";
  attribute x_interface_info of ap_rst_n : signal is "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  attribute x_interface_parameter of ap_rst_n : signal is "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}";
  attribute x_interface_info of ap_start : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl start";
  attribute x_interface_info of crop_out_TREADY : signal is "xilinx.com:interface:axis:1.0 crop_out TREADY";
  attribute x_interface_info of crop_out_TVALID : signal is "xilinx.com:interface:axis:1.0 crop_out TVALID";
  attribute x_interface_parameter of crop_out_TVALID : signal is "XIL_INTERFACENAME crop_out, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 24 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  attribute x_interface_info of image_in_TREADY : signal is "xilinx.com:interface:axis:1.0 image_in TREADY";
  attribute x_interface_info of image_in_TVALID : signal is "xilinx.com:interface:axis:1.0 image_in TVALID";
  attribute x_interface_parameter of image_in_TVALID : signal is "XIL_INTERFACENAME image_in, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  attribute x_interface_info of image_out_TREADY : signal is "xilinx.com:interface:axis:1.0 image_out TREADY";
  attribute x_interface_info of image_out_TVALID : signal is "xilinx.com:interface:axis:1.0 image_out TVALID";
  attribute x_interface_parameter of image_out_TVALID : signal is "XIL_INTERFACENAME image_out, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 24 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  attribute x_interface_info of crop_out_TDATA : signal is "xilinx.com:interface:axis:1.0 crop_out TDATA";
  attribute x_interface_info of crop_out_TDEST : signal is "xilinx.com:interface:axis:1.0 crop_out TDEST";
  attribute x_interface_info of crop_out_TID : signal is "xilinx.com:interface:axis:1.0 crop_out TID";
  attribute x_interface_info of crop_out_TKEEP : signal is "xilinx.com:interface:axis:1.0 crop_out TKEEP";
  attribute x_interface_info of crop_out_TLAST : signal is "xilinx.com:interface:axis:1.0 crop_out TLAST";
  attribute x_interface_info of crop_out_TSTRB : signal is "xilinx.com:interface:axis:1.0 crop_out TSTRB";
  attribute x_interface_info of crop_out_TUSER : signal is "xilinx.com:interface:axis:1.0 crop_out TUSER";
  attribute x_interface_info of image_in_TDATA : signal is "xilinx.com:interface:axis:1.0 image_in TDATA";
  attribute x_interface_info of image_in_TDEST : signal is "xilinx.com:interface:axis:1.0 image_in TDEST";
  attribute x_interface_info of image_in_TID : signal is "xilinx.com:interface:axis:1.0 image_in TID";
  attribute x_interface_info of image_in_TKEEP : signal is "xilinx.com:interface:axis:1.0 image_in TKEEP";
  attribute x_interface_info of image_in_TLAST : signal is "xilinx.com:interface:axis:1.0 image_in TLAST";
  attribute x_interface_info of image_in_TSTRB : signal is "xilinx.com:interface:axis:1.0 image_in TSTRB";
  attribute x_interface_info of image_in_TUSER : signal is "xilinx.com:interface:axis:1.0 image_in TUSER";
  attribute x_interface_info of image_out_TDATA : signal is "xilinx.com:interface:axis:1.0 image_out TDATA";
  attribute x_interface_info of image_out_TDEST : signal is "xilinx.com:interface:axis:1.0 image_out TDEST";
  attribute x_interface_info of image_out_TID : signal is "xilinx.com:interface:axis:1.0 image_out TID";
  attribute x_interface_info of image_out_TKEEP : signal is "xilinx.com:interface:axis:1.0 image_out TKEEP";
  attribute x_interface_info of image_out_TLAST : signal is "xilinx.com:interface:axis:1.0 image_out TLAST";
  attribute x_interface_info of image_out_TSTRB : signal is "xilinx.com:interface:axis:1.0 image_out TSTRB";
  attribute x_interface_info of image_out_TUSER : signal is "xilinx.com:interface:axis:1.0 image_out TUSER";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_center_image
     port map (
      ap_clk => ap_clk,
      ap_done => ap_done,
      ap_idle => ap_idle,
      ap_ready => ap_ready,
      ap_rst_n => ap_rst_n,
      ap_start => ap_start,
      crop_out_TDATA(23 downto 0) => crop_out_TDATA(23 downto 0),
      crop_out_TDEST(0) => crop_out_TDEST(0),
      crop_out_TID(0) => crop_out_TID(0),
      crop_out_TKEEP(2 downto 0) => crop_out_TKEEP(2 downto 0),
      crop_out_TLAST(0) => crop_out_TLAST(0),
      crop_out_TREADY => crop_out_TREADY,
      crop_out_TSTRB(2 downto 0) => crop_out_TSTRB(2 downto 0),
      crop_out_TUSER(0) => crop_out_TUSER(0),
      crop_out_TVALID => crop_out_TVALID,
      image_in_TDATA(23 downto 0) => image_in_TDATA(23 downto 0),
      image_in_TDEST(0) => image_in_TDEST(0),
      image_in_TID(0) => image_in_TID(0),
      image_in_TKEEP(2 downto 0) => image_in_TKEEP(2 downto 0),
      image_in_TLAST(0) => image_in_TLAST(0),
      image_in_TREADY => image_in_TREADY,
      image_in_TSTRB(2 downto 0) => image_in_TSTRB(2 downto 0),
      image_in_TUSER(0) => image_in_TUSER(0),
      image_in_TVALID => image_in_TVALID,
      image_out_TDATA(23 downto 0) => image_out_TDATA(23 downto 0),
      image_out_TDEST(0) => image_out_TDEST(0),
      image_out_TID(0) => image_out_TID(0),
      image_out_TKEEP(2 downto 0) => image_out_TKEEP(2 downto 0),
      image_out_TLAST(0) => image_out_TLAST(0),
      image_out_TREADY => image_out_TREADY,
      image_out_TSTRB(2 downto 0) => image_out_TSTRB(2 downto 0),
      image_out_TUSER(0) => image_out_TUSER(0),
      image_out_TVALID => image_out_TVALID
    );
end STRUCTURE;
