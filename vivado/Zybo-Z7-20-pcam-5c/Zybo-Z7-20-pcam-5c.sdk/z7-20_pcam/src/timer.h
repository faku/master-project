#ifndef __AXI_TIMER_H__
#define __AXI_TIMER_H__

#include <xparameters.h>
#include <xstatus.h>
#include <xtmrctr.h>

#include "ov5640/ScuGicInterruptController.h"

#define TIMER_NB    0

void timer_irq_handler(void *cookie) {
    XTmrCtr *instance;
    u32 value, reg;

    instance = (XTmrCtr *)cookie;
    value = XTmrCtr_GetCaptureValue(instance, TIMER_NB);

    xil_printf("[timer_irq_handler] value: 0x%08X\n", value);

    // Clear interrupt.
    reg = XTmrCtr_ReadReg(instance->BaseAddress, TIMER_NB, 0);
    XTmrCtr_WriteReg(instance->BaseAddress, TIMER_NB, 0, reg | 0x100);
}

using namespace digilent;

class Timer {
private:
    ScuGicInterruptController &gic;
    XTmrCtr instance;

public:
    Timer(u16 dev_id, ScuGicInterruptController &gic, u32 irq_id) : gic(gic) {
        XStatus status;

        status = XTmrCtr_Initialize(&instance, dev_id);
        if (status != XST_SUCCESS) {
            throw std::runtime_error(__FILE__ ": XTmrCtr_Initialize()");
        }

        // Setup interrupt.
        gic.registerHandler(irq_id, timer_irq_handler, (void *)&instance);
        // XTmrCtr_SetHandler(&instance, timer_irq_handler, &instance);
        gic.enableInterrupt(irq_id);
        gic.enableInterrupts();

        // Setup timer.
        // XTmrCtr_SetOptions(&instance, TIMER_NB, 0x7Du);
        XTmrCtr_SetOptions(&instance, TIMER_NB, XTC_CAPTURE_MODE_OPTION | XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION);
        XTmrCtr_SetResetValue(&instance, TIMER_NB, 0x0);

        // Start timer.
        this->start();
    }

    void start() {
        XTmrCtr_Start(&instance, TIMER_NB);
    }

    void stop() {
        XTmrCtr_Stop(&instance, TIMER_NB);
    }
};

#endif
