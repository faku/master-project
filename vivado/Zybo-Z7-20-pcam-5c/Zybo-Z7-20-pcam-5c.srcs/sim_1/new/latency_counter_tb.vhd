library ieee;
        use ieee.std_logic_1164.all;

entity latency_counter_tb is
    -- Ports
end entity latency_counter_tb;

architecture arch of latency_counter_tb is
    -- DUT instantiation
    component latency_counter is
        port (
            clk_i       :  in std_logic;
            start_i     :  in std_logic;
            end_i       :  in std_logic;
            
            result_o    : out std_logic_vector(31 downto 0)
        );
    end component latency_counter;
    
    -- Constants
    constant clk_period : time      := 10ns;
    
    -- Signals
    signal clk_s        : std_logic := '0';
    signal start_s      : std_logic := '0';
    signal end_s        : std_logic := '0';
    signal result_s     : std_logic_vector(31 downto 0);

begin

    -- DUT instantiation
    DUT: latency_counter
        port map (
            clk_i       => clk_s,
            start_i     => start_s,
            end_i       => end_s,
            result_o    => result_s
        );

    -- Process generating the clock
    p_clk: process
    begin
        clk_s   <= '0';
        wait for (clk_period / 2);
        clk_s   <= '1';
        wait for (clk_period / 2);
    end process;
    
    -- Simulation process
    p_sim: process
    begin
        -- Reset
        start_s <= '1';
        end_s   <= '0';
        wait for (clk_period * 10);
        
        -- Start timer
        start_s <= '0';
        wait for (clk_period * 15);
        
        -- End timer
        end_s   <= '1';
        wait for (clk_period);
        end_s   <= '0';
        wait for (clk_period * 2);
        assert (result_s = x"0000000F") report "result_o has wrong value";
        
        -- Reset
        start_s <= '1';
        wait for (clk_period);
        
        -- Start timer
        start_s <= '0';
        wait for (clk_period * 10);
        
        -- End timer
        end_s   <= '1';
        wait for (clk_period);
        end_s   <= '0';
        wait for (clk_period * 2);
        assert (result_s = x"0000000A") report "result_o has wrong value";
        
        -- End simulation
        wait;
    end process;

end architecture arch;