-- (c) Copyright 1995-2019 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:hls:center_image:1.0
-- IP Revision: 1912052140

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY system_center_image_0_0 IS
  PORT (
    image_in_TVALID : IN STD_LOGIC;
    image_in_TREADY : OUT STD_LOGIC;
    image_in_TDATA : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    image_in_TKEEP : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    image_in_TSTRB : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    image_in_TUSER : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    image_in_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    image_in_TID : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    image_in_TDEST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    image_out_TVALID : OUT STD_LOGIC;
    image_out_TREADY : IN STD_LOGIC;
    image_out_TDATA : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    image_out_TKEEP : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    image_out_TSTRB : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    image_out_TUSER : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    image_out_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    image_out_TID : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    image_out_TDEST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    crop_out_TVALID : OUT STD_LOGIC;
    crop_out_TREADY : IN STD_LOGIC;
    crop_out_TDATA : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    crop_out_TKEEP : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    crop_out_TSTRB : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    crop_out_TUSER : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    crop_out_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    crop_out_TID : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    crop_out_TDEST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    ap_clk : IN STD_LOGIC;
    ap_rst_n : IN STD_LOGIC;
    ap_done : OUT STD_LOGIC;
    ap_start : IN STD_LOGIC;
    ap_ready : OUT STD_LOGIC;
    ap_idle : OUT STD_LOGIC
  );
END system_center_image_0_0;

ARCHITECTURE system_center_image_0_0_arch OF system_center_image_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF system_center_image_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT center_image IS
    PORT (
      image_in_TVALID : IN STD_LOGIC;
      image_in_TREADY : OUT STD_LOGIC;
      image_in_TDATA : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
      image_in_TKEEP : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      image_in_TSTRB : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      image_in_TUSER : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      image_in_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      image_in_TID : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      image_in_TDEST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      image_out_TVALID : OUT STD_LOGIC;
      image_out_TREADY : IN STD_LOGIC;
      image_out_TDATA : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
      image_out_TKEEP : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      image_out_TSTRB : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      image_out_TUSER : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      image_out_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      image_out_TID : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      image_out_TDEST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      crop_out_TVALID : OUT STD_LOGIC;
      crop_out_TREADY : IN STD_LOGIC;
      crop_out_TDATA : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
      crop_out_TKEEP : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      crop_out_TSTRB : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      crop_out_TUSER : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      crop_out_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      crop_out_TID : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      crop_out_TDEST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      ap_clk : IN STD_LOGIC;
      ap_rst_n : IN STD_LOGIC;
      ap_done : OUT STD_LOGIC;
      ap_start : IN STD_LOGIC;
      ap_ready : OUT STD_LOGIC;
      ap_idle : OUT STD_LOGIC
    );
  END COMPONENT center_image;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF system_center_image_0_0_arch: ARCHITECTURE IS "center_image,Vivado 2018.2";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF system_center_image_0_0_arch : ARCHITECTURE IS "system_center_image_0_0,center_image,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF system_center_image_0_0_arch: ARCHITECTURE IS "system_center_image_0_0,center_image,{x_ipProduct=Vivado 2018.2,x_ipVendor=xilinx.com,x_ipLibrary=hls,x_ipName=center_image,x_ipVersion=1.0,x_ipCoreRevision=1912052140,x_ipLanguage=VHDL,x_ipSimLanguage=MIXED}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF system_center_image_0_0_arch: ARCHITECTURE IS "HLS";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF ap_idle: SIGNAL IS "xilinx.com:interface:acc_handshake:1.0 ap_ctrl idle";
  ATTRIBUTE X_INTERFACE_INFO OF ap_ready: SIGNAL IS "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ready";
  ATTRIBUTE X_INTERFACE_INFO OF ap_start: SIGNAL IS "xilinx.com:interface:acc_handshake:1.0 ap_ctrl start";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_done: SIGNAL IS "XIL_INTERFACENAME ap_ctrl, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {done {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} start {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum " & 
"{}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} ready {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format lon" & 
"g minimum {} maximum {}} value 0}}} idle {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}";
  ATTRIBUTE X_INTERFACE_INFO OF ap_done: SIGNAL IS "xilinx.com:interface:acc_handshake:1.0 ap_ctrl done";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_rst_n: SIGNAL IS "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}";
  ATTRIBUTE X_INTERFACE_INFO OF ap_rst_n: SIGNAL IS "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_clk: SIGNAL IS "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF image_in:image_out:crop_out, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 150000000, PHASE 0.0," & 
" CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  ATTRIBUTE X_INTERFACE_INFO OF ap_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 ap_clk CLK";
  ATTRIBUTE X_INTERFACE_INFO OF crop_out_TDEST: SIGNAL IS "xilinx.com:interface:axis:1.0 crop_out TDEST";
  ATTRIBUTE X_INTERFACE_INFO OF crop_out_TID: SIGNAL IS "xilinx.com:interface:axis:1.0 crop_out TID";
  ATTRIBUTE X_INTERFACE_INFO OF crop_out_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 crop_out TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF crop_out_TUSER: SIGNAL IS "xilinx.com:interface:axis:1.0 crop_out TUSER";
  ATTRIBUTE X_INTERFACE_INFO OF crop_out_TSTRB: SIGNAL IS "xilinx.com:interface:axis:1.0 crop_out TSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF crop_out_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 crop_out TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF crop_out_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 crop_out TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF crop_out_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 crop_out TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF crop_out_TVALID: SIGNAL IS "XIL_INTERFACENAME crop_out, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_" & 
"type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 24 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {}" & 
" maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  ATTRIBUTE X_INTERFACE_INFO OF crop_out_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 crop_out TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF image_out_TDEST: SIGNAL IS "xilinx.com:interface:axis:1.0 image_out TDEST";
  ATTRIBUTE X_INTERFACE_INFO OF image_out_TID: SIGNAL IS "xilinx.com:interface:axis:1.0 image_out TID";
  ATTRIBUTE X_INTERFACE_INFO OF image_out_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 image_out TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF image_out_TUSER: SIGNAL IS "xilinx.com:interface:axis:1.0 image_out TUSER";
  ATTRIBUTE X_INTERFACE_INFO OF image_out_TSTRB: SIGNAL IS "xilinx.com:interface:axis:1.0 image_out TSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF image_out_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 image_out TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF image_out_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 image_out TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF image_out_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 image_out TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF image_out_TVALID: SIGNAL IS "XIL_INTERFACENAME image_out, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve" & 
"_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 24 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {" & 
"} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  ATTRIBUTE X_INTERFACE_INFO OF image_out_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 image_out TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF image_in_TDEST: SIGNAL IS "xilinx.com:interface:axis:1.0 image_in TDEST";
  ATTRIBUTE X_INTERFACE_INFO OF image_in_TID: SIGNAL IS "xilinx.com:interface:axis:1.0 image_in TID";
  ATTRIBUTE X_INTERFACE_INFO OF image_in_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 image_in TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF image_in_TUSER: SIGNAL IS "xilinx.com:interface:axis:1.0 image_in TUSER";
  ATTRIBUTE X_INTERFACE_INFO OF image_in_TSTRB: SIGNAL IS "xilinx.com:interface:axis:1.0 image_in TSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF image_in_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 image_in TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF image_in_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 image_in TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF image_in_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 image_in TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF image_in_TVALID: SIGNAL IS "XIL_INTERFACENAME image_in, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  ATTRIBUTE X_INTERFACE_INFO OF image_in_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 image_in TVALID";
BEGIN
  U0 : center_image
    PORT MAP (
      image_in_TVALID => image_in_TVALID,
      image_in_TREADY => image_in_TREADY,
      image_in_TDATA => image_in_TDATA,
      image_in_TKEEP => image_in_TKEEP,
      image_in_TSTRB => image_in_TSTRB,
      image_in_TUSER => image_in_TUSER,
      image_in_TLAST => image_in_TLAST,
      image_in_TID => image_in_TID,
      image_in_TDEST => image_in_TDEST,
      image_out_TVALID => image_out_TVALID,
      image_out_TREADY => image_out_TREADY,
      image_out_TDATA => image_out_TDATA,
      image_out_TKEEP => image_out_TKEEP,
      image_out_TSTRB => image_out_TSTRB,
      image_out_TUSER => image_out_TUSER,
      image_out_TLAST => image_out_TLAST,
      image_out_TID => image_out_TID,
      image_out_TDEST => image_out_TDEST,
      crop_out_TVALID => crop_out_TVALID,
      crop_out_TREADY => crop_out_TREADY,
      crop_out_TDATA => crop_out_TDATA,
      crop_out_TKEEP => crop_out_TKEEP,
      crop_out_TSTRB => crop_out_TSTRB,
      crop_out_TUSER => crop_out_TUSER,
      crop_out_TLAST => crop_out_TLAST,
      crop_out_TID => crop_out_TID,
      crop_out_TDEST => crop_out_TDEST,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_done => ap_done,
      ap_start => ap_start,
      ap_ready => ap_ready,
      ap_idle => ap_idle
    );
END system_center_image_0_0_arch;
