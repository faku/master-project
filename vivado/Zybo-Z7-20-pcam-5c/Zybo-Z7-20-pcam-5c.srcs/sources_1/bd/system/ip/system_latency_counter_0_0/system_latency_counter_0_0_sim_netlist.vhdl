-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Fri Jan  3 13:13:56 2020
-- Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_latency_counter_0_0/system_latency_counter_0_0_sim_netlist.vhdl
-- Design      : system_latency_counter_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_latency_counter_0_0_latency_counter is
  port (
    result_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    end_i : in STD_LOGIC;
    start_i : in STD_LOGIC;
    clk_i : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_latency_counter_0_0_latency_counter : entity is "latency_counter";
end system_latency_counter_0_0_latency_counter;

architecture STRUCTURE of system_latency_counter_0_0_latency_counter is
  signal \^result_o\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal result_s0 : STD_LOGIC;
  signal \result_s[3]_i_2_n_0\ : STD_LOGIC;
  signal \result_s_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \result_s_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \result_s_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \result_s_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \result_s_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \result_s_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \result_s_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \result_s_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \result_s_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \result_s_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \result_s_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \result_s_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \result_s_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \result_s_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \result_s_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \result_s_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \result_s_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \result_s_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \result_s_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \result_s_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \result_s_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \result_s_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \result_s_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \result_s_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \result_s_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \result_s_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \result_s_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \result_s_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \result_s_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \result_s_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \result_s_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \result_s_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \result_s_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \result_s_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \result_s_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \result_s_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \result_s_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \result_s_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \result_s_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \result_s_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \result_s_reg[31]_i_2_n_1\ : STD_LOGIC;
  signal \result_s_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \result_s_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \result_s_reg[31]_i_2_n_4\ : STD_LOGIC;
  signal \result_s_reg[31]_i_2_n_5\ : STD_LOGIC;
  signal \result_s_reg[31]_i_2_n_6\ : STD_LOGIC;
  signal \result_s_reg[31]_i_2_n_7\ : STD_LOGIC;
  signal \result_s_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \result_s_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \result_s_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \result_s_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \result_s_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \result_s_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \result_s_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \result_s_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \result_s_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \result_s_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \result_s_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \result_s_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \result_s_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \result_s_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \result_s_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \result_s_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal stop_s : STD_LOGIC;
  signal \NLW_result_s_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of stop_s_reg : label is "LDC";
begin
  result_o(31 downto 0) <= \^result_o\(31 downto 0);
\result_s[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => end_i,
      I1 => stop_s,
      O => result_s0
    );
\result_s[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^result_o\(0),
      O => \result_s[3]_i_2_n_0\
    );
\result_s_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[3]_i_1_n_7\,
      Q => \^result_o\(0)
    );
\result_s_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[11]_i_1_n_5\,
      Q => \^result_o\(10)
    );
\result_s_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[11]_i_1_n_4\,
      Q => \^result_o\(11)
    );
\result_s_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_s_reg[7]_i_1_n_0\,
      CO(3) => \result_s_reg[11]_i_1_n_0\,
      CO(2) => \result_s_reg[11]_i_1_n_1\,
      CO(1) => \result_s_reg[11]_i_1_n_2\,
      CO(0) => \result_s_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \result_s_reg[11]_i_1_n_4\,
      O(2) => \result_s_reg[11]_i_1_n_5\,
      O(1) => \result_s_reg[11]_i_1_n_6\,
      O(0) => \result_s_reg[11]_i_1_n_7\,
      S(3 downto 0) => \^result_o\(11 downto 8)
    );
\result_s_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[15]_i_1_n_7\,
      Q => \^result_o\(12)
    );
\result_s_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[15]_i_1_n_6\,
      Q => \^result_o\(13)
    );
\result_s_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[15]_i_1_n_5\,
      Q => \^result_o\(14)
    );
\result_s_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[15]_i_1_n_4\,
      Q => \^result_o\(15)
    );
\result_s_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_s_reg[11]_i_1_n_0\,
      CO(3) => \result_s_reg[15]_i_1_n_0\,
      CO(2) => \result_s_reg[15]_i_1_n_1\,
      CO(1) => \result_s_reg[15]_i_1_n_2\,
      CO(0) => \result_s_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \result_s_reg[15]_i_1_n_4\,
      O(2) => \result_s_reg[15]_i_1_n_5\,
      O(1) => \result_s_reg[15]_i_1_n_6\,
      O(0) => \result_s_reg[15]_i_1_n_7\,
      S(3 downto 0) => \^result_o\(15 downto 12)
    );
\result_s_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[19]_i_1_n_7\,
      Q => \^result_o\(16)
    );
\result_s_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[19]_i_1_n_6\,
      Q => \^result_o\(17)
    );
\result_s_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[19]_i_1_n_5\,
      Q => \^result_o\(18)
    );
\result_s_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[19]_i_1_n_4\,
      Q => \^result_o\(19)
    );
\result_s_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_s_reg[15]_i_1_n_0\,
      CO(3) => \result_s_reg[19]_i_1_n_0\,
      CO(2) => \result_s_reg[19]_i_1_n_1\,
      CO(1) => \result_s_reg[19]_i_1_n_2\,
      CO(0) => \result_s_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \result_s_reg[19]_i_1_n_4\,
      O(2) => \result_s_reg[19]_i_1_n_5\,
      O(1) => \result_s_reg[19]_i_1_n_6\,
      O(0) => \result_s_reg[19]_i_1_n_7\,
      S(3 downto 0) => \^result_o\(19 downto 16)
    );
\result_s_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[3]_i_1_n_6\,
      Q => \^result_o\(1)
    );
\result_s_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[23]_i_1_n_7\,
      Q => \^result_o\(20)
    );
\result_s_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[23]_i_1_n_6\,
      Q => \^result_o\(21)
    );
\result_s_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[23]_i_1_n_5\,
      Q => \^result_o\(22)
    );
\result_s_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[23]_i_1_n_4\,
      Q => \^result_o\(23)
    );
\result_s_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_s_reg[19]_i_1_n_0\,
      CO(3) => \result_s_reg[23]_i_1_n_0\,
      CO(2) => \result_s_reg[23]_i_1_n_1\,
      CO(1) => \result_s_reg[23]_i_1_n_2\,
      CO(0) => \result_s_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \result_s_reg[23]_i_1_n_4\,
      O(2) => \result_s_reg[23]_i_1_n_5\,
      O(1) => \result_s_reg[23]_i_1_n_6\,
      O(0) => \result_s_reg[23]_i_1_n_7\,
      S(3 downto 0) => \^result_o\(23 downto 20)
    );
\result_s_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[27]_i_1_n_7\,
      Q => \^result_o\(24)
    );
\result_s_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[27]_i_1_n_6\,
      Q => \^result_o\(25)
    );
\result_s_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[27]_i_1_n_5\,
      Q => \^result_o\(26)
    );
\result_s_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[27]_i_1_n_4\,
      Q => \^result_o\(27)
    );
\result_s_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_s_reg[23]_i_1_n_0\,
      CO(3) => \result_s_reg[27]_i_1_n_0\,
      CO(2) => \result_s_reg[27]_i_1_n_1\,
      CO(1) => \result_s_reg[27]_i_1_n_2\,
      CO(0) => \result_s_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \result_s_reg[27]_i_1_n_4\,
      O(2) => \result_s_reg[27]_i_1_n_5\,
      O(1) => \result_s_reg[27]_i_1_n_6\,
      O(0) => \result_s_reg[27]_i_1_n_7\,
      S(3 downto 0) => \^result_o\(27 downto 24)
    );
\result_s_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[31]_i_2_n_7\,
      Q => \^result_o\(28)
    );
\result_s_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[31]_i_2_n_6\,
      Q => \^result_o\(29)
    );
\result_s_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[3]_i_1_n_5\,
      Q => \^result_o\(2)
    );
\result_s_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[31]_i_2_n_5\,
      Q => \^result_o\(30)
    );
\result_s_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[31]_i_2_n_4\,
      Q => \^result_o\(31)
    );
\result_s_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_s_reg[27]_i_1_n_0\,
      CO(3) => \NLW_result_s_reg[31]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \result_s_reg[31]_i_2_n_1\,
      CO(1) => \result_s_reg[31]_i_2_n_2\,
      CO(0) => \result_s_reg[31]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \result_s_reg[31]_i_2_n_4\,
      O(2) => \result_s_reg[31]_i_2_n_5\,
      O(1) => \result_s_reg[31]_i_2_n_6\,
      O(0) => \result_s_reg[31]_i_2_n_7\,
      S(3 downto 0) => \^result_o\(31 downto 28)
    );
\result_s_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[3]_i_1_n_4\,
      Q => \^result_o\(3)
    );
\result_s_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \result_s_reg[3]_i_1_n_0\,
      CO(2) => \result_s_reg[3]_i_1_n_1\,
      CO(1) => \result_s_reg[3]_i_1_n_2\,
      CO(0) => \result_s_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \result_s_reg[3]_i_1_n_4\,
      O(2) => \result_s_reg[3]_i_1_n_5\,
      O(1) => \result_s_reg[3]_i_1_n_6\,
      O(0) => \result_s_reg[3]_i_1_n_7\,
      S(3 downto 1) => \^result_o\(3 downto 1),
      S(0) => \result_s[3]_i_2_n_0\
    );
\result_s_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[7]_i_1_n_7\,
      Q => \^result_o\(4)
    );
\result_s_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[7]_i_1_n_6\,
      Q => \^result_o\(5)
    );
\result_s_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[7]_i_1_n_5\,
      Q => \^result_o\(6)
    );
\result_s_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[7]_i_1_n_4\,
      Q => \^result_o\(7)
    );
\result_s_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_s_reg[3]_i_1_n_0\,
      CO(3) => \result_s_reg[7]_i_1_n_0\,
      CO(2) => \result_s_reg[7]_i_1_n_1\,
      CO(1) => \result_s_reg[7]_i_1_n_2\,
      CO(0) => \result_s_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \result_s_reg[7]_i_1_n_4\,
      O(2) => \result_s_reg[7]_i_1_n_5\,
      O(1) => \result_s_reg[7]_i_1_n_6\,
      O(0) => \result_s_reg[7]_i_1_n_7\,
      S(3 downto 0) => \^result_o\(7 downto 4)
    );
\result_s_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[11]_i_1_n_7\,
      Q => \^result_o\(8)
    );
\result_s_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_i,
      CE => result_s0,
      CLR => start_i,
      D => \result_s_reg[11]_i_1_n_6\,
      Q => \^result_o\(9)
    );
stop_s_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => start_i,
      D => end_i,
      G => end_i,
      GE => '1',
      Q => stop_s
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_latency_counter_0_0 is
  port (
    clk_i : in STD_LOGIC;
    start_i : in STD_LOGIC;
    end_i : in STD_LOGIC;
    result_o : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of system_latency_counter_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of system_latency_counter_0_0 : entity is "system_latency_counter_0_0,latency_counter,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of system_latency_counter_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of system_latency_counter_0_0 : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of system_latency_counter_0_0 : entity is "latency_counter,Vivado 2018.2";
end system_latency_counter_0_0;

architecture STRUCTURE of system_latency_counter_0_0 is
begin
U0: entity work.system_latency_counter_0_0_latency_counter
     port map (
      clk_i => clk_i,
      end_i => end_i,
      result_o(31 downto 0) => result_o(31 downto 0),
      start_i => start_i
    );
end STRUCTURE;
