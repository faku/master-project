// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Sun Dec  8 16:55:28 2019
// Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_stream_bypass_0_0/system_stream_bypass_0_0_sim_netlist.v
// Design      : system_stream_bypass_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "system_stream_bypass_0_0,stream_bypass,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "HLS" *) 
(* x_core_info = "stream_bypass,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module system_stream_bypass_0_0
   (ap_clk,
    ap_rst_n,
    ap_start,
    ap_done,
    ap_idle,
    ap_ready,
    stream_in_TVALID,
    stream_in_TREADY,
    stream_in_TDATA,
    stream_in_TDEST,
    stream_in_TKEEP,
    stream_in_TSTRB,
    stream_in_TUSER,
    stream_in_TLAST,
    stream_in_TID,
    enable_V,
    stream_out_TVALID,
    stream_out_TREADY,
    stream_out_TDATA,
    stream_out_TDEST,
    stream_out_TKEEP,
    stream_out_TSTRB,
    stream_out_TUSER,
    stream_out_TLAST,
    stream_out_TID);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF stream_in:stream_out, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl start" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_ctrl, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {start {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} done {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} idle {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} ready {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_start;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl done" *) output ap_done;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl idle" *) output ap_idle;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ready" *) output ap_ready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_in TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME stream_in, TDATA_NUM_BYTES 4, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 32 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) input stream_in_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_in TREADY" *) output stream_in_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_in TDATA" *) input [31:0]stream_in_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_in TDEST" *) input [0:0]stream_in_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_in TKEEP" *) input [3:0]stream_in_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_in TSTRB" *) input [3:0]stream_in_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_in TUSER" *) input [0:0]stream_in_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_in TLAST" *) input [0:0]stream_in_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_in TID" *) input [0:0]stream_in_TID;
  (* x_interface_info = "xilinx.com:signal:data:1.0 enable_V DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME enable_V, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}" *) input [0:0]enable_V;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_out TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME stream_out, TDATA_NUM_BYTES 4, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 32 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) output stream_out_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_out TREADY" *) input stream_out_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_out TDATA" *) output [31:0]stream_out_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_out TDEST" *) output [0:0]stream_out_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_out TKEEP" *) output [3:0]stream_out_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_out TSTRB" *) output [3:0]stream_out_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_out TUSER" *) output [0:0]stream_out_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_out TLAST" *) output [0:0]stream_out_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 stream_out TID" *) output [0:0]stream_out_TID;

  wire ap_clk;
  wire ap_done;
  wire ap_idle;
  wire ap_ready;
  wire ap_rst_n;
  wire ap_start;
  wire [0:0]enable_V;
  wire [31:0]stream_in_TDATA;
  wire [0:0]stream_in_TDEST;
  wire [0:0]stream_in_TID;
  wire [3:0]stream_in_TKEEP;
  wire [0:0]stream_in_TLAST;
  wire stream_in_TREADY;
  wire [3:0]stream_in_TSTRB;
  wire [0:0]stream_in_TUSER;
  wire stream_in_TVALID;
  wire [31:0]stream_out_TDATA;
  wire [0:0]stream_out_TDEST;
  wire [0:0]stream_out_TID;
  wire [3:0]stream_out_TKEEP;
  wire [0:0]stream_out_TLAST;
  wire stream_out_TREADY;
  wire [3:0]stream_out_TSTRB;
  wire [0:0]stream_out_TUSER;
  wire stream_out_TVALID;

  system_stream_bypass_0_0_stream_bypass U0
       (.ap_clk(ap_clk),
        .ap_done(ap_done),
        .ap_idle(ap_idle),
        .ap_ready(ap_ready),
        .ap_rst_n(ap_rst_n),
        .ap_start(ap_start),
        .enable_V(enable_V),
        .stream_in_TDATA(stream_in_TDATA),
        .stream_in_TDEST(stream_in_TDEST),
        .stream_in_TID(stream_in_TID),
        .stream_in_TKEEP(stream_in_TKEEP),
        .stream_in_TLAST(stream_in_TLAST),
        .stream_in_TREADY(stream_in_TREADY),
        .stream_in_TSTRB(stream_in_TSTRB),
        .stream_in_TUSER(stream_in_TUSER),
        .stream_in_TVALID(stream_in_TVALID),
        .stream_out_TDATA(stream_out_TDATA),
        .stream_out_TDEST(stream_out_TDEST),
        .stream_out_TID(stream_out_TID),
        .stream_out_TKEEP(stream_out_TKEEP),
        .stream_out_TLAST(stream_out_TLAST),
        .stream_out_TREADY(stream_out_TREADY),
        .stream_out_TSTRB(stream_out_TSTRB),
        .stream_out_TUSER(stream_out_TUSER),
        .stream_out_TVALID(stream_out_TVALID));
endmodule

(* ORIG_REF_NAME = "stream_bypass" *) 
module system_stream_bypass_0_0_stream_bypass
   (ap_clk,
    ap_rst_n,
    ap_start,
    ap_done,
    ap_idle,
    ap_ready,
    stream_in_TDATA,
    stream_in_TVALID,
    stream_in_TREADY,
    stream_in_TKEEP,
    stream_in_TSTRB,
    stream_in_TUSER,
    stream_in_TLAST,
    stream_in_TID,
    stream_in_TDEST,
    enable_V,
    stream_out_TDATA,
    stream_out_TVALID,
    stream_out_TREADY,
    stream_out_TKEEP,
    stream_out_TSTRB,
    stream_out_TUSER,
    stream_out_TLAST,
    stream_out_TID,
    stream_out_TDEST);
  input ap_clk;
  input ap_rst_n;
  input ap_start;
  output ap_done;
  output ap_idle;
  output ap_ready;
  input [31:0]stream_in_TDATA;
  input stream_in_TVALID;
  output stream_in_TREADY;
  input [3:0]stream_in_TKEEP;
  input [3:0]stream_in_TSTRB;
  input [0:0]stream_in_TUSER;
  input [0:0]stream_in_TLAST;
  input [0:0]stream_in_TID;
  input [0:0]stream_in_TDEST;
  input [0:0]enable_V;
  output [31:0]stream_out_TDATA;
  output stream_out_TVALID;
  input stream_out_TREADY;
  output [3:0]stream_out_TKEEP;
  output [3:0]stream_out_TSTRB;
  output [0:0]stream_out_TUSER;
  output [0:0]stream_out_TLAST;
  output [0:0]stream_out_TID;
  output [0:0]stream_out_TDEST;

  wire \ap_CS_fsm[0]_i_1_n_0 ;
  wire \ap_CS_fsm[1]_i_1_n_0 ;
  wire \ap_CS_fsm[2]_i_1_n_0 ;
  wire \ap_CS_fsm_reg_n_0_[0] ;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state3;
  wire ap_NS_fsm1;
  wire ap_NS_fsm1104_out;
  wire ap_clk;
  wire ap_idle;
  wire ap_ready;
  wire ap_ready_INST_0_i_1_n_0;
  wire ap_ready_INST_0_i_2_n_0;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire ap_start;
  wire [0:0]enable_V;
  wire [31:0]stream_in_TDATA;
  wire [0:0]stream_in_TDEST;
  wire [0:0]stream_in_TID;
  wire [3:0]stream_in_TKEEP;
  wire [0:0]stream_in_TLAST;
  wire stream_in_TREADY;
  wire [3:0]stream_in_TSTRB;
  wire [0:0]stream_in_TUSER;
  wire stream_in_TVALID;
  wire [31:0]stream_out_TDATA;
  wire [0:0]stream_out_TDEST;
  wire [0:0]stream_out_TID;
  wire [3:0]stream_out_TKEEP;
  wire [0:0]stream_out_TLAST;
  wire stream_out_TREADY;
  wire [3:0]stream_out_TSTRB;
  wire [0:0]stream_out_TUSER;
  wire stream_out_TVALID;
  wire stream_out_V_data_V_1_ack_in;
  wire stream_out_V_data_V_1_load_B;
  wire [31:0]stream_out_V_data_V_1_payload_A;
  wire \stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ;
  wire [31:0]stream_out_V_data_V_1_payload_B;
  wire stream_out_V_data_V_1_sel;
  wire stream_out_V_data_V_1_sel_rd_i_1_n_0;
  wire stream_out_V_data_V_1_sel_wr;
  wire stream_out_V_data_V_1_sel_wr_i_1_n_0;
  wire \stream_out_V_data_V_1_state[0]_i_1_n_0 ;
  wire \stream_out_V_data_V_1_state[1]_i_2_n_0 ;
  wire \stream_out_V_data_V_1_state_reg_n_0_[0] ;
  wire stream_out_V_dest_V_1_ack_in;
  wire stream_out_V_dest_V_1_payload_A;
  wire \stream_out_V_dest_V_1_payload_A[0]_i_1_n_0 ;
  wire stream_out_V_dest_V_1_payload_B;
  wire \stream_out_V_dest_V_1_payload_B[0]_i_1_n_0 ;
  wire stream_out_V_dest_V_1_sel;
  wire stream_out_V_dest_V_1_sel_rd_i_1_n_0;
  wire stream_out_V_dest_V_1_sel_wr;
  wire stream_out_V_dest_V_1_sel_wr_i_1_n_0;
  wire \stream_out_V_dest_V_1_state[0]_i_1_n_0 ;
  wire \stream_out_V_dest_V_1_state[1]_i_1_n_0 ;
  wire stream_out_V_id_V_1_ack_in;
  wire stream_out_V_id_V_1_payload_A;
  wire \stream_out_V_id_V_1_payload_A[0]_i_1_n_0 ;
  wire stream_out_V_id_V_1_payload_B;
  wire \stream_out_V_id_V_1_payload_B[0]_i_1_n_0 ;
  wire stream_out_V_id_V_1_sel;
  wire stream_out_V_id_V_1_sel_rd_i_1_n_0;
  wire stream_out_V_id_V_1_sel_wr;
  wire stream_out_V_id_V_1_sel_wr_i_1_n_0;
  wire \stream_out_V_id_V_1_state[0]_i_1_n_0 ;
  wire \stream_out_V_id_V_1_state[1]_i_1_n_0 ;
  wire \stream_out_V_id_V_1_state_reg_n_0_[0] ;
  wire stream_out_V_keep_V_1_ack_in;
  wire stream_out_V_keep_V_1_load_B;
  wire [3:0]stream_out_V_keep_V_1_payload_A;
  wire \stream_out_V_keep_V_1_payload_A[3]_i_1_n_0 ;
  wire [3:0]stream_out_V_keep_V_1_payload_B;
  wire stream_out_V_keep_V_1_sel;
  wire stream_out_V_keep_V_1_sel_rd_i_1_n_0;
  wire stream_out_V_keep_V_1_sel_wr;
  wire stream_out_V_keep_V_1_sel_wr_i_1_n_0;
  wire \stream_out_V_keep_V_1_state[0]_i_1_n_0 ;
  wire \stream_out_V_keep_V_1_state[1]_i_1_n_0 ;
  wire \stream_out_V_keep_V_1_state_reg_n_0_[0] ;
  wire stream_out_V_last_V_1_ack_in;
  wire stream_out_V_last_V_1_payload_A;
  wire \stream_out_V_last_V_1_payload_A[0]_i_1_n_0 ;
  wire stream_out_V_last_V_1_payload_B;
  wire \stream_out_V_last_V_1_payload_B[0]_i_1_n_0 ;
  wire stream_out_V_last_V_1_sel;
  wire stream_out_V_last_V_1_sel_rd_i_1_n_0;
  wire stream_out_V_last_V_1_sel_wr;
  wire stream_out_V_last_V_1_sel_wr_i_1_n_0;
  wire \stream_out_V_last_V_1_state[0]_i_1_n_0 ;
  wire \stream_out_V_last_V_1_state[1]_i_1_n_0 ;
  wire \stream_out_V_last_V_1_state_reg_n_0_[0] ;
  wire stream_out_V_strb_V_1_ack_in;
  wire stream_out_V_strb_V_1_load_B;
  wire [3:0]stream_out_V_strb_V_1_payload_A;
  wire \stream_out_V_strb_V_1_payload_A[3]_i_1_n_0 ;
  wire [3:0]stream_out_V_strb_V_1_payload_B;
  wire stream_out_V_strb_V_1_sel;
  wire stream_out_V_strb_V_1_sel_rd_i_1_n_0;
  wire stream_out_V_strb_V_1_sel_wr;
  wire stream_out_V_strb_V_1_sel_wr_i_1_n_0;
  wire \stream_out_V_strb_V_1_state[0]_i_1_n_0 ;
  wire \stream_out_V_strb_V_1_state[1]_i_1_n_0 ;
  wire \stream_out_V_strb_V_1_state_reg_n_0_[0] ;
  wire stream_out_V_user_V_1_ack_in;
  wire stream_out_V_user_V_1_payload_A;
  wire \stream_out_V_user_V_1_payload_A[0]_i_1_n_0 ;
  wire stream_out_V_user_V_1_payload_B;
  wire \stream_out_V_user_V_1_payload_B[0]_i_1_n_0 ;
  wire stream_out_V_user_V_1_sel;
  wire stream_out_V_user_V_1_sel_rd_i_1_n_0;
  wire stream_out_V_user_V_1_sel_wr;
  wire stream_out_V_user_V_1_sel_wr_i_1_n_0;
  wire \stream_out_V_user_V_1_state[0]_i_1_n_0 ;
  wire \stream_out_V_user_V_1_state[1]_i_1_n_0 ;
  wire \stream_out_V_user_V_1_state_reg_n_0_[0] ;

  assign ap_done = ap_ready;
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hFA30)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(ap_ready),
        .I1(ap_start),
        .I2(\ap_CS_fsm_reg_n_0_[0] ),
        .I3(ap_CS_fsm_state2),
        .O(\ap_CS_fsm[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h5F5F0C5C00000C5C)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(ap_NS_fsm1104_out),
        .I2(ap_CS_fsm_state2),
        .I3(ap_ready),
        .I4(\ap_CS_fsm_reg_n_0_[0] ),
        .I5(ap_start),
        .O(\ap_CS_fsm[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \ap_CS_fsm[1]_i_2 
       (.I0(stream_out_V_data_V_1_ack_in),
        .I1(ap_CS_fsm_state3),
        .O(ap_NS_fsm1104_out));
  LUT6 #(
    .INIT(64'h2222000022220FFF)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(ap_ready),
        .I2(stream_out_V_data_V_1_ack_in),
        .I3(ap_CS_fsm_state3),
        .I4(ap_CS_fsm_state2),
        .I5(\ap_CS_fsm_reg_n_0_[0] ),
        .O(\ap_CS_fsm[2]_i_1_n_0 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm[0]_i_1_n_0 ),
        .Q(\ap_CS_fsm_reg_n_0_[0] ),
        .S(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm[1]_i_1_n_0 ),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm[2]_i_1_n_0 ),
        .Q(ap_CS_fsm_state3),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    ap_idle_INST_0
       (.I0(\ap_CS_fsm_reg_n_0_[0] ),
        .I1(ap_start),
        .O(ap_idle));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000200)) 
    ap_ready_INST_0
       (.I0(stream_out_V_data_V_1_ack_in),
        .I1(ap_ready_INST_0_i_1_n_0),
        .I2(ap_ready_INST_0_i_2_n_0),
        .I3(ap_CS_fsm_state2),
        .I4(stream_in_TVALID),
        .O(ap_ready));
  LUT4 #(
    .INIT(16'h7FFF)) 
    ap_ready_INST_0_i_1
       (.I0(stream_out_V_last_V_1_ack_in),
        .I1(stream_out_V_user_V_1_ack_in),
        .I2(stream_out_V_dest_V_1_ack_in),
        .I3(stream_out_V_id_V_1_ack_in),
        .O(ap_ready_INST_0_i_1_n_0));
  LUT5 #(
    .INIT(32'h08FFFFFF)) 
    ap_ready_INST_0_i_2
       (.I0(stream_in_TVALID),
        .I1(enable_V),
        .I2(stream_out_V_data_V_1_ack_in),
        .I3(stream_out_V_strb_V_1_ack_in),
        .I4(stream_out_V_keep_V_1_ack_in),
        .O(ap_ready_INST_0_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h02000000)) 
    stream_in_TREADY_INST_0
       (.I0(ap_CS_fsm_state2),
        .I1(ap_ready_INST_0_i_2_n_0),
        .I2(ap_ready_INST_0_i_1_n_0),
        .I3(stream_out_V_data_V_1_ack_in),
        .I4(stream_in_TVALID),
        .O(stream_in_TREADY));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[0]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[0]),
        .I1(stream_out_V_data_V_1_payload_A[0]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[0]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[10]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[10]),
        .I1(stream_out_V_data_V_1_payload_A[10]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[10]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[11]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[11]),
        .I1(stream_out_V_data_V_1_payload_A[11]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[11]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[12]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[12]),
        .I1(stream_out_V_data_V_1_payload_A[12]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[12]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[13]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[13]),
        .I1(stream_out_V_data_V_1_payload_A[13]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[13]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[14]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[14]),
        .I1(stream_out_V_data_V_1_payload_A[14]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[14]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[15]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[15]),
        .I1(stream_out_V_data_V_1_payload_A[15]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[15]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[16]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[16]),
        .I1(stream_out_V_data_V_1_payload_A[16]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[16]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[17]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[17]),
        .I1(stream_out_V_data_V_1_payload_A[17]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[17]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[18]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[18]),
        .I1(stream_out_V_data_V_1_payload_A[18]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[18]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[19]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[19]),
        .I1(stream_out_V_data_V_1_payload_A[19]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[19]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[1]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[1]),
        .I1(stream_out_V_data_V_1_payload_A[1]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[1]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[20]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[20]),
        .I1(stream_out_V_data_V_1_payload_A[20]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[20]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[21]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[21]),
        .I1(stream_out_V_data_V_1_payload_A[21]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[21]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[22]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[22]),
        .I1(stream_out_V_data_V_1_payload_A[22]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[22]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[23]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[23]),
        .I1(stream_out_V_data_V_1_payload_A[23]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[23]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[24]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[24]),
        .I1(stream_out_V_data_V_1_payload_A[24]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[24]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[25]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[25]),
        .I1(stream_out_V_data_V_1_payload_A[25]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[25]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[26]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[26]),
        .I1(stream_out_V_data_V_1_payload_A[26]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[26]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[27]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[27]),
        .I1(stream_out_V_data_V_1_payload_A[27]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[27]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[28]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[28]),
        .I1(stream_out_V_data_V_1_payload_A[28]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[28]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[29]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[29]),
        .I1(stream_out_V_data_V_1_payload_A[29]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[29]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[2]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[2]),
        .I1(stream_out_V_data_V_1_payload_A[2]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[2]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[30]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[30]),
        .I1(stream_out_V_data_V_1_payload_A[30]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[30]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[31]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[31]),
        .I1(stream_out_V_data_V_1_payload_A[31]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[31]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[3]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[3]),
        .I1(stream_out_V_data_V_1_payload_A[3]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[3]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[4]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[4]),
        .I1(stream_out_V_data_V_1_payload_A[4]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[4]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[5]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[5]),
        .I1(stream_out_V_data_V_1_payload_A[5]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[5]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[6]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[6]),
        .I1(stream_out_V_data_V_1_payload_A[6]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[6]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[7]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[7]),
        .I1(stream_out_V_data_V_1_payload_A[7]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[7]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[8]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[8]),
        .I1(stream_out_V_data_V_1_payload_A[8]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[8]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[9]_INST_0 
       (.I0(stream_out_V_data_V_1_payload_B[9]),
        .I1(stream_out_V_data_V_1_payload_A[9]),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_TDATA[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TDEST[0]_INST_0 
       (.I0(stream_out_V_dest_V_1_payload_B),
        .I1(stream_out_V_dest_V_1_sel),
        .I2(stream_out_V_dest_V_1_payload_A),
        .O(stream_out_TDEST));
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TID[0]_INST_0 
       (.I0(stream_out_V_id_V_1_payload_B),
        .I1(stream_out_V_id_V_1_sel),
        .I2(stream_out_V_id_V_1_payload_A),
        .O(stream_out_TID));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TKEEP[0]_INST_0 
       (.I0(stream_out_V_keep_V_1_payload_B[0]),
        .I1(stream_out_V_keep_V_1_sel),
        .I2(stream_out_V_keep_V_1_payload_A[0]),
        .O(stream_out_TKEEP[0]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TKEEP[1]_INST_0 
       (.I0(stream_out_V_keep_V_1_payload_B[1]),
        .I1(stream_out_V_keep_V_1_sel),
        .I2(stream_out_V_keep_V_1_payload_A[1]),
        .O(stream_out_TKEEP[1]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TKEEP[2]_INST_0 
       (.I0(stream_out_V_keep_V_1_payload_B[2]),
        .I1(stream_out_V_keep_V_1_sel),
        .I2(stream_out_V_keep_V_1_payload_A[2]),
        .O(stream_out_TKEEP[2]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TKEEP[3]_INST_0 
       (.I0(stream_out_V_keep_V_1_payload_B[3]),
        .I1(stream_out_V_keep_V_1_sel),
        .I2(stream_out_V_keep_V_1_payload_A[3]),
        .O(stream_out_TKEEP[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TLAST[0]_INST_0 
       (.I0(stream_out_V_last_V_1_payload_B),
        .I1(stream_out_V_last_V_1_sel),
        .I2(stream_out_V_last_V_1_payload_A),
        .O(stream_out_TLAST));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TSTRB[0]_INST_0 
       (.I0(stream_out_V_strb_V_1_payload_B[0]),
        .I1(stream_out_V_strb_V_1_sel),
        .I2(stream_out_V_strb_V_1_payload_A[0]),
        .O(stream_out_TSTRB[0]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TSTRB[1]_INST_0 
       (.I0(stream_out_V_strb_V_1_payload_B[1]),
        .I1(stream_out_V_strb_V_1_sel),
        .I2(stream_out_V_strb_V_1_payload_A[1]),
        .O(stream_out_TSTRB[1]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TSTRB[2]_INST_0 
       (.I0(stream_out_V_strb_V_1_payload_B[2]),
        .I1(stream_out_V_strb_V_1_sel),
        .I2(stream_out_V_strb_V_1_payload_A[2]),
        .O(stream_out_TSTRB[2]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TSTRB[3]_INST_0 
       (.I0(stream_out_V_strb_V_1_payload_B[3]),
        .I1(stream_out_V_strb_V_1_sel),
        .I2(stream_out_V_strb_V_1_payload_A[3]),
        .O(stream_out_TSTRB[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TUSER[0]_INST_0 
       (.I0(stream_out_V_user_V_1_payload_B),
        .I1(stream_out_V_user_V_1_sel),
        .I2(stream_out_V_user_V_1_payload_A),
        .O(stream_out_TUSER));
  LUT3 #(
    .INIT(8'h0D)) 
    \stream_out_V_data_V_1_payload_A[31]_i_1 
       (.I0(\stream_out_V_data_V_1_state_reg_n_0_[0] ),
        .I1(stream_out_V_data_V_1_ack_in),
        .I2(stream_out_V_data_V_1_sel_wr),
        .O(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ));
  FDRE \stream_out_V_data_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[0]),
        .Q(stream_out_V_data_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[10]),
        .Q(stream_out_V_data_V_1_payload_A[10]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[11]),
        .Q(stream_out_V_data_V_1_payload_A[11]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[12]),
        .Q(stream_out_V_data_V_1_payload_A[12]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[13]),
        .Q(stream_out_V_data_V_1_payload_A[13]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[14]),
        .Q(stream_out_V_data_V_1_payload_A[14]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[15]),
        .Q(stream_out_V_data_V_1_payload_A[15]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[16]),
        .Q(stream_out_V_data_V_1_payload_A[16]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[17]),
        .Q(stream_out_V_data_V_1_payload_A[17]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[18]),
        .Q(stream_out_V_data_V_1_payload_A[18]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[19]),
        .Q(stream_out_V_data_V_1_payload_A[19]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[1]),
        .Q(stream_out_V_data_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[20]),
        .Q(stream_out_V_data_V_1_payload_A[20]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[21]),
        .Q(stream_out_V_data_V_1_payload_A[21]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[22]),
        .Q(stream_out_V_data_V_1_payload_A[22]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[23]),
        .Q(stream_out_V_data_V_1_payload_A[23]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[24]),
        .Q(stream_out_V_data_V_1_payload_A[24]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[25]),
        .Q(stream_out_V_data_V_1_payload_A[25]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[26]),
        .Q(stream_out_V_data_V_1_payload_A[26]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[27]),
        .Q(stream_out_V_data_V_1_payload_A[27]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[28]),
        .Q(stream_out_V_data_V_1_payload_A[28]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[29]),
        .Q(stream_out_V_data_V_1_payload_A[29]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[2]),
        .Q(stream_out_V_data_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[30]),
        .Q(stream_out_V_data_V_1_payload_A[30]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[31]),
        .Q(stream_out_V_data_V_1_payload_A[31]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[3]),
        .Q(stream_out_V_data_V_1_payload_A[3]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[4]),
        .Q(stream_out_V_data_V_1_payload_A[4]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[5]),
        .Q(stream_out_V_data_V_1_payload_A[5]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[6]),
        .Q(stream_out_V_data_V_1_payload_A[6]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[7]),
        .Q(stream_out_V_data_V_1_payload_A[7]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[8]),
        .Q(stream_out_V_data_V_1_payload_A[8]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(\stream_out_V_data_V_1_payload_A[31]_i_1_n_0 ),
        .D(stream_in_TDATA[9]),
        .Q(stream_out_V_data_V_1_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA2)) 
    \stream_out_V_data_V_1_payload_B[31]_i_1 
       (.I0(stream_out_V_data_V_1_sel_wr),
        .I1(\stream_out_V_data_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_data_V_1_ack_in),
        .O(stream_out_V_data_V_1_load_B));
  FDRE \stream_out_V_data_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[0]),
        .Q(stream_out_V_data_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[10]),
        .Q(stream_out_V_data_V_1_payload_B[10]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[11]),
        .Q(stream_out_V_data_V_1_payload_B[11]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[12]),
        .Q(stream_out_V_data_V_1_payload_B[12]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[13]),
        .Q(stream_out_V_data_V_1_payload_B[13]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[14]),
        .Q(stream_out_V_data_V_1_payload_B[14]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[15]),
        .Q(stream_out_V_data_V_1_payload_B[15]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[16]),
        .Q(stream_out_V_data_V_1_payload_B[16]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[17]),
        .Q(stream_out_V_data_V_1_payload_B[17]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[18]),
        .Q(stream_out_V_data_V_1_payload_B[18]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[19]),
        .Q(stream_out_V_data_V_1_payload_B[19]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[1]),
        .Q(stream_out_V_data_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[20]),
        .Q(stream_out_V_data_V_1_payload_B[20]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[21]),
        .Q(stream_out_V_data_V_1_payload_B[21]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[22]),
        .Q(stream_out_V_data_V_1_payload_B[22]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[23]),
        .Q(stream_out_V_data_V_1_payload_B[23]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[24]),
        .Q(stream_out_V_data_V_1_payload_B[24]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[25]),
        .Q(stream_out_V_data_V_1_payload_B[25]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[26]),
        .Q(stream_out_V_data_V_1_payload_B[26]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[27]),
        .Q(stream_out_V_data_V_1_payload_B[27]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[28]),
        .Q(stream_out_V_data_V_1_payload_B[28]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[29]),
        .Q(stream_out_V_data_V_1_payload_B[29]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[2]),
        .Q(stream_out_V_data_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[30]),
        .Q(stream_out_V_data_V_1_payload_B[30]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[31]),
        .Q(stream_out_V_data_V_1_payload_B[31]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[3]),
        .Q(stream_out_V_data_V_1_payload_B[3]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[4]),
        .Q(stream_out_V_data_V_1_payload_B[4]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[5]),
        .Q(stream_out_V_data_V_1_payload_B[5]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[6]),
        .Q(stream_out_V_data_V_1_payload_B[6]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[7]),
        .Q(stream_out_V_data_V_1_payload_B[7]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[8]),
        .Q(stream_out_V_data_V_1_payload_B[8]),
        .R(1'b0));
  FDRE \stream_out_V_data_V_1_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(stream_out_V_data_V_1_load_B),
        .D(stream_in_TDATA[9]),
        .Q(stream_out_V_data_V_1_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_data_V_1_sel_rd_i_1
       (.I0(stream_out_TREADY),
        .I1(\stream_out_V_data_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_data_V_1_sel),
        .O(stream_out_V_data_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_data_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_data_V_1_sel_rd_i_1_n_0),
        .Q(stream_out_V_data_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_data_V_1_sel_wr_i_1
       (.I0(stream_out_V_data_V_1_ack_in),
        .I1(ap_NS_fsm1),
        .I2(stream_out_V_data_V_1_sel_wr),
        .O(stream_out_V_data_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_data_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_data_V_1_sel_wr_i_1_n_0),
        .Q(stream_out_V_data_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \stream_out_V_data_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\stream_out_V_data_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_data_V_1_ack_in),
        .I3(stream_out_TREADY),
        .I4(ap_NS_fsm1),
        .O(\stream_out_V_data_V_1_state[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \stream_out_V_data_V_1_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hF3FB)) 
    \stream_out_V_data_V_1_state[1]_i_2 
       (.I0(stream_out_V_data_V_1_ack_in),
        .I1(\stream_out_V_data_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_TREADY),
        .I3(ap_NS_fsm1),
        .O(\stream_out_V_data_V_1_state[1]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_data_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_data_V_1_state[0]_i_1_n_0 ),
        .Q(\stream_out_V_data_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_data_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_data_V_1_state[1]_i_2_n_0 ),
        .Q(stream_out_V_data_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \stream_out_V_dest_V_1_payload_A[0]_i_1 
       (.I0(stream_in_TDEST),
        .I1(stream_out_TVALID),
        .I2(stream_out_V_dest_V_1_ack_in),
        .I3(stream_out_V_dest_V_1_sel_wr),
        .I4(stream_out_V_dest_V_1_payload_A),
        .O(\stream_out_V_dest_V_1_payload_A[0]_i_1_n_0 ));
  FDRE \stream_out_V_dest_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_dest_V_1_payload_A[0]_i_1_n_0 ),
        .Q(stream_out_V_dest_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \stream_out_V_dest_V_1_payload_B[0]_i_1 
       (.I0(stream_in_TDEST),
        .I1(stream_out_V_dest_V_1_sel_wr),
        .I2(stream_out_TVALID),
        .I3(stream_out_V_dest_V_1_ack_in),
        .I4(stream_out_V_dest_V_1_payload_B),
        .O(\stream_out_V_dest_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \stream_out_V_dest_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_dest_V_1_payload_B[0]_i_1_n_0 ),
        .Q(stream_out_V_dest_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_dest_V_1_sel_rd_i_1
       (.I0(stream_out_TREADY),
        .I1(stream_out_TVALID),
        .I2(stream_out_V_dest_V_1_sel),
        .O(stream_out_V_dest_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_dest_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_dest_V_1_sel_rd_i_1_n_0),
        .Q(stream_out_V_dest_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_dest_V_1_sel_wr_i_1
       (.I0(stream_out_V_dest_V_1_ack_in),
        .I1(ap_NS_fsm1),
        .I2(stream_out_V_dest_V_1_sel_wr),
        .O(stream_out_V_dest_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_dest_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_dest_V_1_sel_wr_i_1_n_0),
        .Q(stream_out_V_dest_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \stream_out_V_dest_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(stream_out_TVALID),
        .I2(stream_out_V_dest_V_1_ack_in),
        .I3(stream_out_TREADY),
        .I4(ap_NS_fsm1),
        .O(\stream_out_V_dest_V_1_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0200000000000000)) 
    \stream_out_V_dest_V_1_state[0]_i_2 
       (.I0(stream_out_V_data_V_1_ack_in),
        .I1(ap_ready_INST_0_i_1_n_0),
        .I2(ap_ready_INST_0_i_2_n_0),
        .I3(ap_CS_fsm_state2),
        .I4(stream_in_TVALID),
        .I5(enable_V),
        .O(ap_NS_fsm1));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hDFCF)) 
    \stream_out_V_dest_V_1_state[1]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(stream_out_TREADY),
        .I2(stream_out_TVALID),
        .I3(stream_out_V_dest_V_1_ack_in),
        .O(\stream_out_V_dest_V_1_state[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_dest_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_dest_V_1_state[0]_i_1_n_0 ),
        .Q(stream_out_TVALID),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_dest_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_dest_V_1_state[1]_i_1_n_0 ),
        .Q(stream_out_V_dest_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \stream_out_V_id_V_1_payload_A[0]_i_1 
       (.I0(stream_in_TID),
        .I1(\stream_out_V_id_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_id_V_1_ack_in),
        .I3(stream_out_V_id_V_1_sel_wr),
        .I4(stream_out_V_id_V_1_payload_A),
        .O(\stream_out_V_id_V_1_payload_A[0]_i_1_n_0 ));
  FDRE \stream_out_V_id_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_id_V_1_payload_A[0]_i_1_n_0 ),
        .Q(stream_out_V_id_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \stream_out_V_id_V_1_payload_B[0]_i_1 
       (.I0(stream_in_TID),
        .I1(stream_out_V_id_V_1_sel_wr),
        .I2(\stream_out_V_id_V_1_state_reg_n_0_[0] ),
        .I3(stream_out_V_id_V_1_ack_in),
        .I4(stream_out_V_id_V_1_payload_B),
        .O(\stream_out_V_id_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \stream_out_V_id_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_id_V_1_payload_B[0]_i_1_n_0 ),
        .Q(stream_out_V_id_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_id_V_1_sel_rd_i_1
       (.I0(stream_out_TREADY),
        .I1(\stream_out_V_id_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_id_V_1_sel),
        .O(stream_out_V_id_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_id_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_id_V_1_sel_rd_i_1_n_0),
        .Q(stream_out_V_id_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_id_V_1_sel_wr_i_1
       (.I0(stream_out_V_id_V_1_ack_in),
        .I1(ap_NS_fsm1),
        .I2(stream_out_V_id_V_1_sel_wr),
        .O(stream_out_V_id_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_id_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_id_V_1_sel_wr_i_1_n_0),
        .Q(stream_out_V_id_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \stream_out_V_id_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\stream_out_V_id_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_id_V_1_ack_in),
        .I3(stream_out_TREADY),
        .I4(ap_NS_fsm1),
        .O(\stream_out_V_id_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hDFCF)) 
    \stream_out_V_id_V_1_state[1]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(stream_out_TREADY),
        .I2(\stream_out_V_id_V_1_state_reg_n_0_[0] ),
        .I3(stream_out_V_id_V_1_ack_in),
        .O(\stream_out_V_id_V_1_state[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_id_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_id_V_1_state[0]_i_1_n_0 ),
        .Q(\stream_out_V_id_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_id_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_id_V_1_state[1]_i_1_n_0 ),
        .Q(stream_out_V_id_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT3 #(
    .INIT(8'h0D)) 
    \stream_out_V_keep_V_1_payload_A[3]_i_1 
       (.I0(\stream_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I1(stream_out_V_keep_V_1_ack_in),
        .I2(stream_out_V_keep_V_1_sel_wr),
        .O(\stream_out_V_keep_V_1_payload_A[3]_i_1_n_0 ));
  FDRE \stream_out_V_keep_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(\stream_out_V_keep_V_1_payload_A[3]_i_1_n_0 ),
        .D(stream_in_TKEEP[0]),
        .Q(stream_out_V_keep_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \stream_out_V_keep_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(\stream_out_V_keep_V_1_payload_A[3]_i_1_n_0 ),
        .D(stream_in_TKEEP[1]),
        .Q(stream_out_V_keep_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \stream_out_V_keep_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(\stream_out_V_keep_V_1_payload_A[3]_i_1_n_0 ),
        .D(stream_in_TKEEP[2]),
        .Q(stream_out_V_keep_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \stream_out_V_keep_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(\stream_out_V_keep_V_1_payload_A[3]_i_1_n_0 ),
        .D(stream_in_TKEEP[3]),
        .Q(stream_out_V_keep_V_1_payload_A[3]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA2)) 
    \stream_out_V_keep_V_1_payload_B[3]_i_1 
       (.I0(stream_out_V_keep_V_1_sel_wr),
        .I1(\stream_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_keep_V_1_ack_in),
        .O(stream_out_V_keep_V_1_load_B));
  FDRE \stream_out_V_keep_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(stream_out_V_keep_V_1_load_B),
        .D(stream_in_TKEEP[0]),
        .Q(stream_out_V_keep_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \stream_out_V_keep_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(stream_out_V_keep_V_1_load_B),
        .D(stream_in_TKEEP[1]),
        .Q(stream_out_V_keep_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \stream_out_V_keep_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(stream_out_V_keep_V_1_load_B),
        .D(stream_in_TKEEP[2]),
        .Q(stream_out_V_keep_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \stream_out_V_keep_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(stream_out_V_keep_V_1_load_B),
        .D(stream_in_TKEEP[3]),
        .Q(stream_out_V_keep_V_1_payload_B[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_keep_V_1_sel_rd_i_1
       (.I0(\stream_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I1(stream_out_TREADY),
        .I2(stream_out_V_keep_V_1_sel),
        .O(stream_out_V_keep_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_keep_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_keep_V_1_sel_rd_i_1_n_0),
        .Q(stream_out_V_keep_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_keep_V_1_sel_wr_i_1
       (.I0(stream_out_V_keep_V_1_ack_in),
        .I1(ap_NS_fsm1),
        .I2(stream_out_V_keep_V_1_sel_wr),
        .O(stream_out_V_keep_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_keep_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_keep_V_1_sel_wr_i_1_n_0),
        .Q(stream_out_V_keep_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \stream_out_V_keep_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\stream_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_keep_V_1_ack_in),
        .I3(stream_out_TREADY),
        .I4(ap_NS_fsm1),
        .O(\stream_out_V_keep_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hDFCF)) 
    \stream_out_V_keep_V_1_state[1]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(stream_out_TREADY),
        .I2(\stream_out_V_keep_V_1_state_reg_n_0_[0] ),
        .I3(stream_out_V_keep_V_1_ack_in),
        .O(\stream_out_V_keep_V_1_state[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_keep_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_keep_V_1_state[0]_i_1_n_0 ),
        .Q(\stream_out_V_keep_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_keep_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_keep_V_1_state[1]_i_1_n_0 ),
        .Q(stream_out_V_keep_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \stream_out_V_last_V_1_payload_A[0]_i_1 
       (.I0(stream_in_TLAST),
        .I1(\stream_out_V_last_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_last_V_1_ack_in),
        .I3(stream_out_V_last_V_1_sel_wr),
        .I4(stream_out_V_last_V_1_payload_A),
        .O(\stream_out_V_last_V_1_payload_A[0]_i_1_n_0 ));
  FDRE \stream_out_V_last_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_last_V_1_payload_A[0]_i_1_n_0 ),
        .Q(stream_out_V_last_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \stream_out_V_last_V_1_payload_B[0]_i_1 
       (.I0(stream_in_TLAST),
        .I1(stream_out_V_last_V_1_sel_wr),
        .I2(\stream_out_V_last_V_1_state_reg_n_0_[0] ),
        .I3(stream_out_V_last_V_1_ack_in),
        .I4(stream_out_V_last_V_1_payload_B),
        .O(\stream_out_V_last_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \stream_out_V_last_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_last_V_1_payload_B[0]_i_1_n_0 ),
        .Q(stream_out_V_last_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_last_V_1_sel_rd_i_1
       (.I0(\stream_out_V_last_V_1_state_reg_n_0_[0] ),
        .I1(stream_out_TREADY),
        .I2(stream_out_V_last_V_1_sel),
        .O(stream_out_V_last_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_last_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_last_V_1_sel_rd_i_1_n_0),
        .Q(stream_out_V_last_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_last_V_1_sel_wr_i_1
       (.I0(stream_out_V_last_V_1_ack_in),
        .I1(ap_NS_fsm1),
        .I2(stream_out_V_last_V_1_sel_wr),
        .O(stream_out_V_last_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_last_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_last_V_1_sel_wr_i_1_n_0),
        .Q(stream_out_V_last_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \stream_out_V_last_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\stream_out_V_last_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_last_V_1_ack_in),
        .I3(stream_out_TREADY),
        .I4(ap_NS_fsm1),
        .O(\stream_out_V_last_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hDFCF)) 
    \stream_out_V_last_V_1_state[1]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(stream_out_TREADY),
        .I2(\stream_out_V_last_V_1_state_reg_n_0_[0] ),
        .I3(stream_out_V_last_V_1_ack_in),
        .O(\stream_out_V_last_V_1_state[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_last_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_last_V_1_state[0]_i_1_n_0 ),
        .Q(\stream_out_V_last_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_last_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_last_V_1_state[1]_i_1_n_0 ),
        .Q(stream_out_V_last_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT3 #(
    .INIT(8'h0D)) 
    \stream_out_V_strb_V_1_payload_A[3]_i_1 
       (.I0(\stream_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I1(stream_out_V_strb_V_1_ack_in),
        .I2(stream_out_V_strb_V_1_sel_wr),
        .O(\stream_out_V_strb_V_1_payload_A[3]_i_1_n_0 ));
  FDRE \stream_out_V_strb_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(\stream_out_V_strb_V_1_payload_A[3]_i_1_n_0 ),
        .D(stream_in_TSTRB[0]),
        .Q(stream_out_V_strb_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \stream_out_V_strb_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(\stream_out_V_strb_V_1_payload_A[3]_i_1_n_0 ),
        .D(stream_in_TSTRB[1]),
        .Q(stream_out_V_strb_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \stream_out_V_strb_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(\stream_out_V_strb_V_1_payload_A[3]_i_1_n_0 ),
        .D(stream_in_TSTRB[2]),
        .Q(stream_out_V_strb_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \stream_out_V_strb_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(\stream_out_V_strb_V_1_payload_A[3]_i_1_n_0 ),
        .D(stream_in_TSTRB[3]),
        .Q(stream_out_V_strb_V_1_payload_A[3]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA2)) 
    \stream_out_V_strb_V_1_payload_B[3]_i_1 
       (.I0(stream_out_V_strb_V_1_sel_wr),
        .I1(\stream_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_strb_V_1_ack_in),
        .O(stream_out_V_strb_V_1_load_B));
  FDRE \stream_out_V_strb_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(stream_out_V_strb_V_1_load_B),
        .D(stream_in_TSTRB[0]),
        .Q(stream_out_V_strb_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \stream_out_V_strb_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(stream_out_V_strb_V_1_load_B),
        .D(stream_in_TSTRB[1]),
        .Q(stream_out_V_strb_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \stream_out_V_strb_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(stream_out_V_strb_V_1_load_B),
        .D(stream_in_TSTRB[2]),
        .Q(stream_out_V_strb_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \stream_out_V_strb_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(stream_out_V_strb_V_1_load_B),
        .D(stream_in_TSTRB[3]),
        .Q(stream_out_V_strb_V_1_payload_B[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_strb_V_1_sel_rd_i_1
       (.I0(\stream_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I1(stream_out_TREADY),
        .I2(stream_out_V_strb_V_1_sel),
        .O(stream_out_V_strb_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_strb_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_strb_V_1_sel_rd_i_1_n_0),
        .Q(stream_out_V_strb_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_strb_V_1_sel_wr_i_1
       (.I0(stream_out_V_strb_V_1_ack_in),
        .I1(ap_NS_fsm1),
        .I2(stream_out_V_strb_V_1_sel_wr),
        .O(stream_out_V_strb_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_strb_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_strb_V_1_sel_wr_i_1_n_0),
        .Q(stream_out_V_strb_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \stream_out_V_strb_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\stream_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_strb_V_1_ack_in),
        .I3(stream_out_TREADY),
        .I4(ap_NS_fsm1),
        .O(\stream_out_V_strb_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hDFCF)) 
    \stream_out_V_strb_V_1_state[1]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(stream_out_TREADY),
        .I2(\stream_out_V_strb_V_1_state_reg_n_0_[0] ),
        .I3(stream_out_V_strb_V_1_ack_in),
        .O(\stream_out_V_strb_V_1_state[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_strb_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_strb_V_1_state[0]_i_1_n_0 ),
        .Q(\stream_out_V_strb_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_strb_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_strb_V_1_state[1]_i_1_n_0 ),
        .Q(stream_out_V_strb_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \stream_out_V_user_V_1_payload_A[0]_i_1 
       (.I0(stream_in_TUSER),
        .I1(\stream_out_V_user_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_user_V_1_ack_in),
        .I3(stream_out_V_user_V_1_sel_wr),
        .I4(stream_out_V_user_V_1_payload_A),
        .O(\stream_out_V_user_V_1_payload_A[0]_i_1_n_0 ));
  FDRE \stream_out_V_user_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_user_V_1_payload_A[0]_i_1_n_0 ),
        .Q(stream_out_V_user_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \stream_out_V_user_V_1_payload_B[0]_i_1 
       (.I0(stream_in_TUSER),
        .I1(stream_out_V_user_V_1_sel_wr),
        .I2(\stream_out_V_user_V_1_state_reg_n_0_[0] ),
        .I3(stream_out_V_user_V_1_ack_in),
        .I4(stream_out_V_user_V_1_payload_B),
        .O(\stream_out_V_user_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \stream_out_V_user_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_user_V_1_payload_B[0]_i_1_n_0 ),
        .Q(stream_out_V_user_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_user_V_1_sel_rd_i_1
       (.I0(stream_out_TREADY),
        .I1(\stream_out_V_user_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_user_V_1_sel),
        .O(stream_out_V_user_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_user_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_user_V_1_sel_rd_i_1_n_0),
        .Q(stream_out_V_user_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h78)) 
    stream_out_V_user_V_1_sel_wr_i_1
       (.I0(stream_out_V_user_V_1_ack_in),
        .I1(ap_NS_fsm1),
        .I2(stream_out_V_user_V_1_sel_wr),
        .O(stream_out_V_user_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stream_out_V_user_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(stream_out_V_user_V_1_sel_wr_i_1_n_0),
        .Q(stream_out_V_user_V_1_sel_wr),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \stream_out_V_user_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\stream_out_V_user_V_1_state_reg_n_0_[0] ),
        .I2(stream_out_V_user_V_1_ack_in),
        .I3(stream_out_TREADY),
        .I4(ap_NS_fsm1),
        .O(\stream_out_V_user_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hDFCF)) 
    \stream_out_V_user_V_1_state[1]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(stream_out_TREADY),
        .I2(\stream_out_V_user_V_1_state_reg_n_0_[0] ),
        .I3(stream_out_V_user_V_1_ack_in),
        .O(\stream_out_V_user_V_1_state[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_user_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_user_V_1_state[0]_i_1_n_0 ),
        .Q(\stream_out_V_user_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \stream_out_V_user_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\stream_out_V_user_V_1_state[1]_i_1_n_0 ),
        .Q(stream_out_V_user_V_1_ack_in),
        .R(ap_rst_n_inv));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
