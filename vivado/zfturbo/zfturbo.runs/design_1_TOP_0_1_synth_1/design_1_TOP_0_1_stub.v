// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Fri Jan 24 14:10:49 2020
// Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_TOP_0_1_stub.v
// Design      : design_1_TOP_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "TOP,Vivado 2018.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk, GO, RESULT, we_database, dp_database, 
  address_p_database, STOP)
/* synthesis syn_black_box black_box_pad_pin="clk,GO,RESULT[3:0],we_database,dp_database[3:0],address_p_database[12:0],STOP" */;
  input clk;
  input GO;
  output [3:0]RESULT;
  input we_database;
  input [3:0]dp_database;
  input [12:0]address_p_database;
  output STOP;
endmodule
