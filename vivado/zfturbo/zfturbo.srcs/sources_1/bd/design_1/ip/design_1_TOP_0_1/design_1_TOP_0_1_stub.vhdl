-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Fri Jan 24 14:10:51 2020
-- Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/faku/master-project/vivado/zfturbo/zfturbo.srcs/sources_1/bd/design_1/ip/design_1_TOP_0_1/design_1_TOP_0_1_stub.vhdl
-- Design      : design_1_TOP_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_TOP_0_1 is
  Port ( 
    clk : in STD_LOGIC;
    GO : in STD_LOGIC;
    RESULT : out STD_LOGIC_VECTOR ( 3 downto 0 );
    we_database : in STD_LOGIC;
    dp_database : in STD_LOGIC_VECTOR ( 3 downto 0 );
    address_p_database : in STD_LOGIC_VECTOR ( 12 downto 0 );
    STOP : out STD_LOGIC
  );

end design_1_TOP_0_1;

architecture stub of design_1_TOP_0_1 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,GO,RESULT[3:0],we_database,dp_database[3:0],address_p_database[12:0],STOP";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "TOP,Vivado 2018.2";
begin
end;
