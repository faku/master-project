// (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:module_ref:TOP:1.0
// IP Revision: 1

(* X_CORE_INFO = "TOP,Vivado 2018.2" *)
(* CHECK_LICENSE_TYPE = "design_1_TOP_0_1,TOP,{}" *)
(* CORE_GENERATION_INFO = "design_1_TOP_0_1,TOP,{x_ipProduct=Vivado 2018.2,x_ipVendor=xilinx.com,x_ipLibrary=module_ref,x_ipName=TOP,x_ipVersion=1.0,x_ipCoreRevision=1,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED,num_conv=4,SIZE_1=4,SIZE_2=8,SIZE_3=12,SIZE_4=16,SIZE_5=20,SIZE_6=24,SIZE_7=28,SIZE_8=32,SIZE_9=36,SIZE_address_pix=13,SIZE_address_pix_t=12,SIZE_address_wei=9,picture_size=28,picture_storage_limit=0,razmpar=14,razmpar2=7,picture_storage_limit_2=784,convolution_size=9}" *)
(* IP_DEFINITION_SOURCE = "module_ref" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_1_TOP_0_1 (
  clk,
  GO,
  RESULT,
  we_database,
  dp_database,
  address_p_database,
  STOP
);

(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_clk_out1" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *)
input wire clk;
input wire GO;
output wire [3 : 0] RESULT;
input wire we_database;
input wire [3 : 0] dp_database;
input wire [12 : 0] address_p_database;
output wire STOP;

  TOP #(
    .num_conv(4),
    .SIZE_1(4),
    .SIZE_2(8),
    .SIZE_3(12),
    .SIZE_4(16),
    .SIZE_5(20),
    .SIZE_6(24),
    .SIZE_7(28),
    .SIZE_8(32),
    .SIZE_9(36),
    .SIZE_address_pix(13),
    .SIZE_address_pix_t(12),
    .SIZE_address_wei(9),
    .picture_size(28),
    .picture_storage_limit(0),
    .razmpar(14),
    .razmpar2(7),
    .picture_storage_limit_2(784),
    .convolution_size(9)
  ) inst (
    .clk(clk),
    .GO(GO),
    .RESULT(RESULT),
    .we_database(we_database),
    .dp_database(dp_database),
    .address_p_database(address_p_database),
    .STOP(STOP)
  );
endmodule
