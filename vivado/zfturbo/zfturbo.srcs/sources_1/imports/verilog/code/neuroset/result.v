module result(clk,enable,STOP,memstartp,read_addressp,qp,re,RESULT);

parameter SIZE_1=0;
parameter SIZE_2=0;
parameter SIZE_3=0;
parameter SIZE_4=0;
parameter SIZE_address_pix=0;

input clk,enable;
output reg STOP;
input [SIZE_address_pix-1:0] memstartp;
input [SIZE_4-1:0] qp;
output reg re;
output reg [SIZE_address_pix-1:0] read_addressp;
output reg [3:0] RESULT;

reg [3:0] marker;
reg signed [SIZE_1-1:0] buff;

wire signed [SIZE_1-1:0]  p1, p2, p3, p4;
always @(posedge clk)
begin
if (enable==1)
begin
re=1;
case (marker)
	0: begin read_addressp=memstartp+0; end
	1: begin read_addressp=memstartp+1; buff = 0; end
	2: begin read_addressp=memstartp+2; if (p1>=buff) begin buff=p1; RESULT=0; end if (p2>=buff) begin buff=p2; RESULT=1; end if (p3>=buff) begin buff=p3; RESULT=2; end if (p4>=buff) begin buff=p4; RESULT=3; end end
	3: begin read_addressp=memstartp+3; if (p1>=buff) begin buff=p1; RESULT=4; end if (p2>=buff) begin buff=p2; RESULT=5; end if (p3>=buff) begin buff=p3; RESULT=6; end if (p4>=buff) begin buff=p4; RESULT=7; end end
	4: begin read_addressp=memstartp+4; if (p1>=buff) begin buff=p1; RESULT=8; end if (p2>=buff) begin buff=p2; RESULT=9; end if (p3>=buff) begin buff=p3; RESULT=10; end STOP=1; end
	default: $display("Check case result");
endcase
marker=marker+1;
end
else 
begin
re=0;
marker=0;
STOP=0;
end
end

assign p1=qp[SIZE_4-1:SIZE_3];
assign p2=qp[SIZE_3-1:SIZE_2];
assign p3=qp[SIZE_2-1:SIZE_1];
assign p4=qp[SIZE_1-1:0];
endmodule
