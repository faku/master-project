#include <ap_axi_sdata.h>
#include <hls_video.h>

#define HEIGHT      1080
#define WIDTH       1920
#define PADDING     420

typedef ap_axiu<24,1,1,1> pixel_t;
typedef hls::stream<pixel_t> stream_t;

#if 1
#define USE_MAT
#endif

void center_image(stream_t &image_in, stream_t &image_out, stream_t &crop_out) {
#pragma HLS INTERFACE axis port=image_in
#pragma HLS INTERFACE axis port=image_out
#pragma HLS INTERFACE axis port=crop_out

#ifdef USE_MAT
    hls::Mat<HEIGHT, WIDTH, HLS_8UC3> input_data(HEIGHT, WIDTH);
    hls::Mat<HEIGHT, WIDTH, HLS_8UC3> passthrough(HEIGHT, WIDTH);
    hls::Mat<HEIGHT, HEIGHT, HLS_8UC3> crop_data(HEIGHT, HEIGHT);

    hls::Scalar<HLS_MAT_CN(HLS_8UC3), HLS_TNAME(HLS_8UC3)> pixel;
    HLS_SIZE_T i, j;

#pragma HLS DATAFLOW

    hls::AXIvideo2Mat(image_in, input_data);

    loop_height: for (i = 0; i < HEIGHT; ++i) {
        loop_width_l: for (j = 0; j < PADDING; ++j) {
            input_data >> pixel;
            // Set pixel to black.
            pixel.val[0] = 0x0;
            pixel.val[1] = 0x0;
            pixel.val[2] = 0x0;
            passthrough << pixel;
        }

        loop_width_c: for (j = 0; j < HEIGHT; ++j) {
            input_data >> pixel;
            passthrough << pixel;
            crop_data << pixel;
        }

        loop_width_r: for (j = 0; j < PADDING; ++j) {
            input_data >> pixel;
            // Set pixel to black.
            pixel.val[0] = 0x0;
            pixel.val[1] = 0x0;
            pixel.val[2] = 0x0;
            passthrough << pixel;
        }
    }

    hls::Mat2AXIvideo(passthrough, image_out);
    hls::Mat2AXIvideo(crop_data, crop_out);
#else
    HLS_SIZE_T i, j;
    pixel_t pixel;

    // Wait for beginning of stream
    bool sof = 0;
    loop_wait_sof: do {
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_TRIPCOUNT avg=0 max=0
        image_in >> pixel;
        sof = pixel.user.to_int();
    } while (sof == 0);

    loop_height: for (i = 0; i < HEIGHT; ++i) {
        bool eol = 0;

        // Left part of the image, to black out
        loop_width_l: for (j = 0; j < PADDING; ++j) {
#pragma HLS LOOP_FLATTEN off
#pragma HLS PIPELINE II=1
            if (sof != 0 || eol != 0) {
                sof = 0;
                eol = pixel.last.to_int();
            }
            else {
                image_in >> pixel;
            }

            pixel.data = 0x0;
            image_out << pixel;
        }

        // Center part of the image, to keep
        loop_width_c: for (j = 0; j < HEIGHT; ++j) {
#pragma HLS LOOP_FLATTEN off
#pragma HLS PIPELINE II=1
            image_in >> pixel;
            image_out << pixel;
            crop_out << pixel;
        }

        // Right part of the image, to black out
        loop_width_r: for (j = 0; j < PADDING; ++j) {
#pragma HLS LOOP_FLATTEN off
#pragma HLS PIPELINE II=1
            image_in >> pixel;

            eol = pixel.last.to_int();
            pixel.data = 0x0;
            image_out << pixel;
        }

        loop_wait_eol: do {
            image_in >> pixel;
            eol = pixel.last.to_int();
        } while (eol != 0);
    }
#endif
}
