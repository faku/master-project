create_project prj -part xc7z020clg400-1 -force
set_property target_language vhdl [current_project]
set vivado_ver [version -short]
set COE_DIR "../../syn/vhdl"
source "/home/faku/master-project/vivado_hls/float_converter/solution1/syn/vhdl/float_converter_ap_uitodp_4_no_dsp_32_ip.tcl"
if {[regexp -nocase {2015\.3.*} $vivado_ver match] || [regexp -nocase {2016\.1.*} $vivado_ver match]} {
    extract_files -base_dir "./prjsrcs/sources_1/ip" [get_files -all -of [get_ips float_converter_ap_uitodp_4_no_dsp_32]]
}
source "/home/faku/master-project/vivado_hls/float_converter/solution1/syn/vhdl/float_converter_ap_ddiv_29_no_dsp_64_ip.tcl"
if {[regexp -nocase {2015\.3.*} $vivado_ver match] || [regexp -nocase {2016\.1.*} $vivado_ver match]} {
    extract_files -base_dir "./prjsrcs/sources_1/ip" [get_files -all -of [get_ips float_converter_ap_ddiv_29_no_dsp_64]]
}
source "/home/faku/master-project/vivado_hls/float_converter/solution1/syn/vhdl/float_converter_ap_fptrunc_0_no_dsp_64_ip.tcl"
if {[regexp -nocase {2015\.3.*} $vivado_ver match] || [regexp -nocase {2016\.1.*} $vivado_ver match]} {
    extract_files -base_dir "./prjsrcs/sources_1/ip" [get_files -all -of [get_ips float_converter_ap_fptrunc_0_no_dsp_64]]
}
