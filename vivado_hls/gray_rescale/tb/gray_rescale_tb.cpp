#include <hls_opencv.h>
#include <iostream>

#define HEIGHT_IN   1080
#define WIDTH_IN    1080

#define HEIGHT_OUT  28
#define WIDTH_OUT   28

typedef ap_axiu<24,1,1,1> pixel24_t;
typedef ap_axiu< 8,1,1,1> pixel8_t;
typedef hls::stream<pixel24_t> stream24_t;
typedef hls::stream<pixel8_t> stream8_t;

void gray_rescale(stream24_t &stream_in, stream8_t &stream_out);

int main(int argc, char **argv) {
    // Load reference image
    IplImage *image_in = cvLoadImage("image_crop.jpg");
    CvSize size_in = cvGetSize(image_in);

    // Create output image
    CvSize size_out = { WIDTH_OUT, HEIGHT_OUT };
    IplImage *image_out = cvCreateImage(size_out, IPL_DEPTH_8U, 1);

    // Create streams.
    stream24_t stream_in;
    stream8_t stream_out;

    // Convert OpenCV data to AXI-Stream.
    IplImage2AXIvideo(image_in, stream_in);

    // Call the center_image() function.
    gray_rescale(stream_in, stream_out);

    // Convert AXI-Stream data to OpenCV.
    AXIvideo2IplImage(stream_out, image_out);

    // Save output image.
    cvSaveImage("/home/faku/master-project/vivado_hls/gray_rescale/tb/image_out.jpg", image_out);

    cvReleaseImage(&image_in);
    cvReleaseImage(&image_out);

    return 0;
}
