#include "mnist_fp16_opt.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp16_opt::thread_tmp_130_fu_9161_p2() {
    tmp_130_fu_9161_p2 = (!F2_2_fu_9155_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_2_fu_9155_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_1310_fu_11450_p1() {
    tmp_1310_fu_11450_p1 = mul26_fu_22893_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1312_fu_11526_p4() {
    tmp_1312_fu_11526_p4 = neg_mul26_fu_11521_p2.read().range(24, 16);
}

void mnist_fp16_opt::thread_tmp_1314_fu_12614_p1() {
    tmp_1314_fu_12614_p1 = grp_fu_11635_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1315_fu_11955_p1() {
    tmp_1315_fu_11955_p1 = mul27_fu_22944_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1316_fu_11984_p4() {
    tmp_1316_fu_11984_p4 = neg_mul27_fu_11979_p2.read().range(24, 20);
}

void mnist_fp16_opt::thread_tmp_1318_fu_12008_p1() {
    tmp_1318_fu_12008_p1 = p_v7_fu_12001_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_1319_fu_12018_p1() {
    tmp_1319_fu_12018_p1 = p_v7_fu_12001_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_131_fu_9167_p2() {
    tmp_131_fu_9167_p2 = (!ap_const_lv12_FF2.is_01() || !F2_2_fu_9155_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_2_fu_9155_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1320_fu_12656_p1() {
    tmp_1320_fu_12656_p1 = tmp_1040_fu_12650_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_1321_fu_12668_p3() {
    tmp_1321_fu_12668_p3 = esl_concat<8,1>(tmp_1040_fu_12650_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1322_fu_5645_p1() {
    tmp_1322_fu_5645_p1 = mul28_fu_22788_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1324_fu_5662_p4() {
    tmp_1324_fu_5662_p4 = neg_mul28_fu_5657_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_1326_fu_6577_p1() {
    tmp_1326_fu_6577_p1 = grp_fu_5699_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_1327_fu_6589_p1() {
    tmp_1327_fu_6589_p1 = grp_fu_5699_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_1328_fu_16504_p1() {
    tmp_1328_fu_16504_p1 = tmp_1060_fu_16494_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_1329_fu_5124_p2() {
    tmp_1329_fu_5124_p2 = (!p_s_reg_2273.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_2273.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_132_fu_9173_p2() {
    tmp_132_fu_9173_p2 = (!ap_const_lv12_E.is_01() || !F2_2_fu_9155_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_2_fu_9155_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1330_fu_5130_p2() {
    tmp_1330_fu_5130_p2 = (tmp_1329_fu_5124_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_1331_fu_5135_p2() {
    tmp_1331_fu_5135_p2 = (tmp_11_reg_23152.read() | tmp_1330_fu_5130_p2.read());
}

void mnist_fp16_opt::thread_tmp_1332_fu_5140_p2() {
    tmp_1332_fu_5140_p2 = (tmp_25_reg_23169.read() | tmp_1331_fu_5135_p2.read());
}

void mnist_fp16_opt::thread_tmp_1333_fu_11566_p1() {
    tmp_1333_fu_11566_p1 = mul29_fu_22901_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1335_fu_11588_p4() {
    tmp_1335_fu_11588_p4 = neg_mul29_fu_11583_p2.read().range(24, 16);
}

void mnist_fp16_opt::thread_tmp_1337_fu_12706_p1() {
    tmp_1337_fu_12706_p1 = grp_fu_11640_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1338_fu_11967_p1() {
    tmp_1338_fu_11967_p1 = mul30_fu_22951_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1339_fu_12034_p4() {
    tmp_1339_fu_12034_p4 = neg_mul30_fu_12029_p2.read().range(24, 20);
}

void mnist_fp16_opt::thread_tmp_133_fu_9187_p2() {
    tmp_133_fu_9187_p2 = (!F2_2_fu_9155_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_2_fu_9155_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_1341_fu_12058_p1() {
    tmp_1341_fu_12058_p1 = p_v8_fu_12051_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_1342_fu_12068_p1() {
    tmp_1342_fu_12068_p1 = p_v8_fu_12051_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_1343_fu_12748_p1() {
    tmp_1343_fu_12748_p1 = tmp_1076_fu_12742_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_1344_fu_12760_p3() {
    tmp_1344_fu_12760_p3 = esl_concat<8,1>(tmp_1076_fu_12742_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1345_fu_5709_p1() {
    tmp_1345_fu_5709_p1 = mul31_fu_22796_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1347_fu_5742_p4() {
    tmp_1347_fu_5742_p4 = neg_mul31_fu_5737_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_1349_fu_6647_p1() {
    tmp_1349_fu_6647_p1 = grp_fu_5779_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_134_cast_fu_6622_p1() {
    tmp_134_cast_fu_6622_p1 = esl_sext<64,11>(tmp_70_fu_6617_p2.read());
}

void mnist_fp16_opt::thread_tmp_134_fu_7002_p2() {
    tmp_134_fu_7002_p2 = (!p_shl15_cast_fu_6998_p1.read().is_01() || !p_shl14_cast1_fu_6972_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl15_cast_fu_6998_p1.read()) - sc_biguint<9>(p_shl14_cast1_fu_6972_p1.read()));
}

void mnist_fp16_opt::thread_tmp_134_mid2_v_fu_13327_p3() {
    tmp_134_mid2_v_fu_13327_p3 = (!exitcond_flatten21_fu_13313_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten21_fu_13313_p2.read()[0].to_bool())? args02_V_fu_13307_p2.read(): ap_phi_mux_p_36_phi_fu_3239_p4.read());
}

void mnist_fp16_opt::thread_tmp_1350_fu_6659_p1() {
    tmp_1350_fu_6659_p1 = grp_fu_5779_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_1351_fu_16619_p1() {
    tmp_1351_fu_16619_p1 = tmp_1103_fu_16614_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1352_fu_16645_p3() {
    tmp_1352_fu_16645_p3 = esl_concat<10,1>(tmp_1103_reg_26387.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1354_fu_16732_p1() {
    tmp_1354_fu_16732_p1 = msb_idx_s_fu_16726_p2.read().range(31-1, 0);
}

void mnist_fp16_opt::thread_tmp_1356_fu_16754_p4() {
    tmp_1356_fu_16754_p4 = msb_idx_10_fu_16744_p3.read().range(30, 5);
}

void mnist_fp16_opt::thread_tmp_1357_fu_16785_p1() {
    tmp_1357_fu_16785_p1 = msb_idx_10_fu_16744_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_1358_fu_16789_p2() {
    tmp_1358_fu_16789_p2 = (!ap_const_lv4_1.is_01() || !tmp_1357_fu_16785_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_1357_fu_16785_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1359_fu_16795_p1() {
    tmp_1359_fu_16795_p1 = esl_zext<16,4>(tmp_1358_fu_16789_p2.read());
}

void mnist_fp16_opt::thread_tmp_135_fu_7618_p2() {
    tmp_135_fu_7618_p2 = (!sh_amt_reg_24014.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_reg_24014.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_1360_fu_16799_p2() {
    tmp_1360_fu_16799_p2 = (!tmp_1359_fu_16795_p1.read().is_01())? sc_lv<16>(): tmp_V_5_reg_26434.read() >> (unsigned short)tmp_1359_fu_16795_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_1361_fu_16836_p1() {
    tmp_1361_fu_16836_p1 = msb_idx_s_reg_26440_pp16_iter10_reg.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1362_fu_16894_p1() {
    tmp_1362_fu_16894_p1 = p_03_i3_to_int_fu_16881_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1363_fu_16912_p1() {
    tmp_1363_fu_16912_p1 = tmp_647_to_int_fu_16898_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1364_fu_17135_p1() {
    tmp_1364_fu_17135_p1 = mul32_fu_22983_p2.read().range(21-1, 0);
}

void mnist_fp16_opt::thread_tmp_1366_fu_17168_p4() {
    tmp_1366_fu_17168_p4 = neg_mul32_fu_17163_p2.read().range(20, 13);
}

void mnist_fp16_opt::thread_tmp_1368_fu_17537_p1() {
    tmp_1368_fu_17537_p1 = grp_fu_17205_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1369_fu_17389_p1() {
    tmp_1369_fu_17389_p1 = mul33_fu_23015_p2.read().range(21-1, 0);
}

void mnist_fp16_opt::thread_tmp_136_cast_fu_6840_p1() {
    tmp_136_cast_fu_6840_p1 = esl_sext<64,11>(tmp_73_fu_6835_p2.read());
}

void mnist_fp16_opt::thread_tmp_136_cast_mid2_ca_fu_10655_p1() {
    tmp_136_cast_mid2_ca_fu_10655_p1 = esl_zext<10,5>(tmp_136_cast_mid2_v_fu_10650_p2.read());
}

void mnist_fp16_opt::thread_tmp_136_cast_mid2_v_1_fu_10646_p1() {
    tmp_136_cast_mid2_v_1_fu_10646_p1 = esl_zext<5,2>(tmp_136_cast_mid2_v_s_fu_10638_p3.read());
}

void mnist_fp16_opt::thread_tmp_136_cast_mid2_v_fu_10650_p2() {
    tmp_136_cast_mid2_v_fu_10650_p2 = (!tmp_136_cast_mid2_v_1_fu_10646_p1.read().is_01() || !r_V_mid2_reg_24763.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_136_cast_mid2_v_1_fu_10646_p1.read()) + sc_biguint<5>(r_V_mid2_reg_24763.read()));
}

void mnist_fp16_opt::thread_tmp_136_cast_mid2_v_s_fu_10638_p3() {
    tmp_136_cast_mid2_v_s_fu_10638_p3 = (!exitcond12_fu_10624_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond12_fu_10624_p2.read()[0].to_bool())? ra63_V_fu_10618_p2.read(): ap_phi_mux_p_38_phi_fu_2948_p4.read());
}

void mnist_fp16_opt::thread_tmp_136_fu_6925_p2() {
    tmp_136_fu_6925_p2 = (exitcond9_mid_fu_6913_p2.read() | exitcond_flatten2_fu_6887_p2.read());
}

void mnist_fp16_opt::thread_tmp_1370_fu_17418_p4() {
    tmp_1370_fu_17418_p4 = neg_mul33_fu_17413_p2.read().range(20, 16);
}

void mnist_fp16_opt::thread_tmp_1372_fu_17442_p1() {
    tmp_1372_fu_17442_p1 = p_v9_fu_17435_p3.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1373_fu_17452_p1() {
    tmp_1373_fu_17452_p1 = p_v9_fu_17435_p3.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1374_fu_17575_p1() {
    tmp_1374_fu_17575_p1 = tmp_1119_fu_17565_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_1375_fu_18125_p1() {
    tmp_1375_fu_18125_p1 = tmp_1128_fu_18115_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1376_fu_17151_p1() {
    tmp_1376_fu_17151_p1 = mul34_fu_22991_p2.read().range(21-1, 0);
}

void mnist_fp16_opt::thread_tmp_1378_fu_17216_p4() {
    tmp_1378_fu_17216_p4 = neg_mul34_fu_17211_p2.read().range(20, 13);
}

void mnist_fp16_opt::thread_tmp_137_fu_7015_p2() {
    tmp_137_fu_7015_p2 = (!tmp_237_cast_fu_7008_p1.read().is_01() || !tmp_mid2_cast_fu_7012_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_237_cast_fu_7008_p1.read()) + sc_biguint<10>(tmp_mid2_cast_fu_7012_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1380_fu_17707_p1() {
    tmp_1380_fu_17707_p1 = grp_fu_17285_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1381_fu_17463_p1() {
    tmp_1381_fu_17463_p1 = mul35_fu_23029_p2.read().range(21-1, 0);
}

void mnist_fp16_opt::thread_tmp_1382_fu_17598_p4() {
    tmp_1382_fu_17598_p4 = neg_mul35_fu_17593_p2.read().range(20, 16);
}

void mnist_fp16_opt::thread_tmp_1384_fu_17622_p1() {
    tmp_1384_fu_17622_p1 = p_v10_fu_17615_p3.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1385_fu_17632_p1() {
    tmp_1385_fu_17632_p1 = p_v10_fu_17615_p3.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1386_fu_17745_p1() {
    tmp_1386_fu_17745_p1 = tmp_1142_fu_17735_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_1387_fu_18579_p1() {
    tmp_1387_fu_18579_p1 = tmp_1150_fu_18573_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1388_fu_18646_p1() {
    tmp_1388_fu_18646_p1 = conv5_0_load_to_int_fu_18633_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1389_fu_18683_p1() {
    tmp_1389_fu_18683_p1 = ireg_V_8_fu_18675_p3.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_138_cast_fu_6850_p1() {
    tmp_138_cast_fu_6850_p1 = esl_sext<64,11>(tmp_74_fu_6845_p2.read());
}

void mnist_fp16_opt::thread_tmp_138_fu_7021_p1() {
    tmp_138_fu_7021_p1 = tmp_137_fu_7015_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1391_fu_18705_p1() {
    tmp_1391_fu_18705_p1 = ireg_V_8_fu_18675_p3.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_1392_fu_18780_p1() {
    tmp_1392_fu_18780_p1 = man_V_21_fu_18735_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1393_fu_18784_p4() {
    tmp_1393_fu_18784_p4 = sh_amt_8_fu_18766_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_1394_fu_18852_p1() {
    tmp_1394_fu_18852_p1 = tmp_618_fu_18847_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1395_fu_18872_p1() {
    tmp_1395_fu_18872_p1 = tmp_621_fu_18866_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1396_fu_18295_p3() {
    tmp_1396_fu_18295_p3 = esl_concat<9,2>(tmp_1155_reg_26869.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_1397_fu_18252_p3() {
    tmp_1397_fu_18252_p3 = (!exitcond_flatten40_fu_18174_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten40_fu_18174_p2.read()[0].to_bool())? lhs_V_22_cast_mid2_c_reg_26836.read(): r_V_18_fu_18151_p2.read());
}

void mnist_fp16_opt::thread_tmp_1398_fu_18259_p3() {
    tmp_1398_fu_18259_p3 = (!exitcond67_mid_fu_18217_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond67_mid_fu_18217_p2.read()[0].to_bool())? r_V_19_mid1_fu_18247_p2.read(): tmp_1397_fu_18252_p3.read());
}

void mnist_fp16_opt::thread_tmp_1399_fu_18347_p1() {
    tmp_1399_fu_18347_p1 = tmp_1163_fu_18341_p2.read().range(12-1, 0);
}

void mnist_fp16_opt::thread_tmp_139_fu_7033_p3() {
    tmp_139_fu_7033_p3 = esl_concat<10,2>(tmp_137_fu_7015_p2.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_1400_fu_18351_p1() {
    tmp_1400_fu_18351_p1 = tmp_1163_fu_18341_p2.read().range(10-1, 0);
}

void mnist_fp16_opt::thread_tmp_1401_fu_17257_p1() {
    tmp_1401_fu_17257_p1 = mul36_fu_22999_p2.read().range(21-1, 0);
}

void mnist_fp16_opt::thread_tmp_1403_fu_17295_p4() {
    tmp_1403_fu_17295_p4 = neg_mul36_fu_17290_p2.read().range(20, 13);
}

void mnist_fp16_opt::thread_tmp_1405_fu_17803_p1() {
    tmp_1405_fu_17803_p1 = grp_fu_17379_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1406_fu_17475_p1() {
    tmp_1406_fu_17475_p1 = mul37_fu_23036_p2.read().range(21-1, 0);
}

void mnist_fp16_opt::thread_tmp_1407_fu_17648_p4() {
    tmp_1407_fu_17648_p4 = neg_mul37_fu_17643_p2.read().range(20, 16);
}

void mnist_fp16_opt::thread_tmp_1409_fu_17672_p1() {
    tmp_1409_fu_17672_p1 = p_v11_fu_17665_p3.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1410_fu_17682_p1() {
    tmp_1410_fu_17682_p1 = p_v11_fu_17665_p3.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1411_fu_17841_p1() {
    tmp_1411_fu_17841_p1 = tmp_1179_fu_17831_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_1412_fu_19249_p1() {
    tmp_1412_fu_19249_p1 = p_62_mid2_reg_27092.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1413_fu_19320_p1() {
    tmp_1413_fu_19320_p1 = mul38_fu_23043_p2.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1415_fu_19349_p4() {
    tmp_1415_fu_19349_p4 = neg_mul38_fu_19344_p2.read().range(22, 14);
}

void mnist_fp16_opt::thread_tmp_1417_fu_19442_p1() {
    tmp_1417_fu_19442_p1 = grp_fu_19386_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_1418_fu_19332_p1() {
    tmp_1418_fu_19332_p1 = mul39_fu_23051_p2.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1419_fu_19397_p4() {
    tmp_1419_fu_19397_p4 = neg_mul39_fu_19392_p2.read().range(22, 17);
}

void mnist_fp16_opt::thread_tmp_141_cast_fu_6860_p1() {
    tmp_141_cast_fu_6860_p1 = esl_sext<64,11>(tmp_80_fu_6855_p2.read());
}

void mnist_fp16_opt::thread_tmp_141_fu_7045_p2() {
    tmp_141_fu_7045_p2 = (!p_shl17_cast_fu_7025_p3.read().is_01() || !p_shl18_cast_fu_7041_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl17_cast_fu_7025_p3.read()) - sc_bigint<13>(p_shl18_cast_fu_7041_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1421_fu_19421_p1() {
    tmp_1421_fu_19421_p1 = p_v12_fu_19414_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_1422_fu_19431_p1() {
    tmp_1422_fu_19431_p1 = p_v12_fu_19414_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_1423_fu_19476_p1() {
    tmp_1423_fu_19476_p1 = tmp_1201_fu_19470_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1424_fu_17273_p1() {
    tmp_1424_fu_17273_p1 = mul40_fu_23007_p2.read().range(21-1, 0);
}

void mnist_fp16_opt::thread_tmp_1426_fu_17337_p4() {
    tmp_1426_fu_17337_p4 = neg_mul40_fu_17332_p2.read().range(20, 13);
}

void mnist_fp16_opt::thread_tmp_1428_fu_17899_p1() {
    tmp_1428_fu_17899_p1 = grp_fu_17384_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1429_fu_17401_p1() {
    tmp_1429_fu_17401_p1 = mul41_fu_23022_p2.read().range(21-1, 0);
}

void mnist_fp16_opt::thread_tmp_142_fu_11302_p3() {
    tmp_142_fu_11302_p3 = (!tmp_833_reg_24998.read()[0].is_01())? sc_lv<12>(): ((tmp_833_reg_24998.read()[0].to_bool())? neg_ti12_fu_11296_p2.read(): tmp_837_fu_11286_p1.read());
}

void mnist_fp16_opt::thread_tmp_1430_fu_17492_p4() {
    tmp_1430_fu_17492_p4 = neg_mul41_fu_17487_p2.read().range(20, 16);
}

void mnist_fp16_opt::thread_tmp_1432_fu_17516_p1() {
    tmp_1432_fu_17516_p1 = p_v13_fu_17509_p3.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1433_fu_17526_p1() {
    tmp_1433_fu_17526_p1 = p_v13_fu_17509_p3.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1434_fu_17937_p1() {
    tmp_1434_fu_17937_p1 = tmp_1216_fu_17927_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_1435_fu_19856_p1() {
    tmp_1435_fu_19856_p1 = tmp_1225_fu_19846_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1437_fu_19567_p1() {
    tmp_1437_fu_19567_p1 = msb_idx_11_fu_19561_p2.read().range(31-1, 0);
}

void mnist_fp16_opt::thread_tmp_1439_fu_19589_p4() {
    tmp_1439_fu_19589_p4 = msb_idx_12_fu_19579_p3.read().range(30, 5);
}

void mnist_fp16_opt::thread_tmp_1440_fu_19620_p1() {
    tmp_1440_fu_19620_p1 = msb_idx_12_fu_19579_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_1441_fu_19624_p2() {
    tmp_1441_fu_19624_p2 = (!ap_const_lv4_1.is_01() || !tmp_1440_fu_19620_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_1440_fu_19620_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1442_fu_19630_p1() {
    tmp_1442_fu_19630_p1 = esl_zext<16,4>(tmp_1441_fu_19624_p2.read());
}

void mnist_fp16_opt::thread_tmp_1444_fu_19671_p1() {
    tmp_1444_fu_19671_p1 = msb_idx_11_reg_27219_pp20_iter27_reg.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1445_fu_21082_p1() {
    tmp_1445_fu_21082_p1 = tmp_1232_fu_21076_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1446_fu_21149_p1() {
    tmp_1446_fu_21149_p1 = conv6_0_load_to_int_fu_21136_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1447_fu_21186_p1() {
    tmp_1447_fu_21186_p1 = ireg_V_1_fu_21178_p3.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_1449_fu_21208_p1() {
    tmp_1449_fu_21208_p1 = ireg_V_1_fu_21178_p3.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_144_fu_9301_p2() {
    tmp_144_fu_9301_p2 = (!sh_amt_2_reg_24441.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_2_reg_24441.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_1450_fu_21283_p1() {
    tmp_1450_fu_21283_p1 = man_V_28_fu_21238_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1451_fu_21287_p4() {
    tmp_1451_fu_21287_p4 = sh_amt_s_fu_21269_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_1452_fu_21355_p1() {
    tmp_1452_fu_21355_p1 = tmp_637_fu_21350_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1453_fu_21375_p1() {
    tmp_1453_fu_21375_p1 = tmp_536_fu_21369_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1455_fu_20800_p1() {
    tmp_1455_fu_20800_p1 = msb_idx_13_fu_20794_p2.read().range(31-1, 0);
}

void mnist_fp16_opt::thread_tmp_1457_fu_20828_p4() {
    tmp_1457_fu_20828_p4 = msb_idx_14_fu_20818_p3.read().range(30, 5);
}

void mnist_fp16_opt::thread_tmp_1458_fu_20859_p1() {
    tmp_1458_fu_20859_p1 = msb_idx_14_fu_20818_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_1459_fu_20863_p2() {
    tmp_1459_fu_20863_p2 = (!ap_const_lv4_1.is_01() || !tmp_1458_fu_20859_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_1458_fu_20859_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1460_fu_20869_p1() {
    tmp_1460_fu_20869_p1 = esl_zext<16,4>(tmp_1459_fu_20863_p2.read());
}

void mnist_fp16_opt::thread_tmp_1462_fu_20910_p1() {
    tmp_1462_fu_20910_p1 = msb_idx_13_reg_27539.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1463_fu_20032_p3() {
    tmp_1463_fu_20032_p3 = esl_concat<10,2>(tmp_1237_reg_27317.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_1464_fu_19983_p3() {
    tmp_1464_fu_19983_p3 = (!exitcond_flatten48_fu_19905_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten48_fu_19905_p2.read()[0].to_bool())? lhs_V_26_cast_mid2_c_reg_27284.read(): r_V_22_fu_19882_p2.read());
}

void mnist_fp16_opt::thread_tmp_1465_fu_19990_p3() {
    tmp_1465_fu_19990_p3 = (!exitcond73_mid_fu_19948_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond73_mid_fu_19948_p2.read()[0].to_bool())? r_V_23_mid1_fu_19978_p2.read(): tmp_1464_fu_19983_p3.read());
}

void mnist_fp16_opt::thread_tmp_1466_fu_20084_p1() {
    tmp_1466_fu_20084_p1 = tmp_1245_fu_20078_p2.read().range(13-1, 0);
}

void mnist_fp16_opt::thread_tmp_1467_fu_20088_p1() {
    tmp_1467_fu_20088_p1 = tmp_1245_fu_20078_p2.read().range(11-1, 0);
}

void mnist_fp16_opt::thread_tmp_1468_fu_20162_p1() {
    tmp_1468_fu_20162_p1 = ireg_V_16_fu_20158_p1.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_146_fu_4856_p2() {
    tmp_146_fu_4856_p2 = (grp_fu_4400_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_1470_fu_20184_p1() {
    tmp_1470_fu_20184_p1 = ireg_V_16_fu_20158_p1.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_1471_fu_20295_p1() {
    tmp_1471_fu_20295_p1 = man_V_20_fu_20250_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1472_fu_20299_p4() {
    tmp_1472_fu_20299_p4 = sh_amt_10_fu_20281_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_1473_fu_20452_p1() {
    tmp_1473_fu_20452_p1 = tmp_539_fu_20447_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1474_fu_20472_p1() {
    tmp_1474_fu_20472_p1 = tmp_542_fu_20466_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1475_fu_20198_p1() {
    tmp_1475_fu_20198_p1 = ireg_V_17_fu_20194_p1.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_1477_fu_20220_p1() {
    tmp_1477_fu_20220_p1 = ireg_V_17_fu_20194_p1.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_1478_fu_20415_p1() {
    tmp_1478_fu_20415_p1 = man_V_25_fu_20370_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1479_fu_20419_p4() {
    tmp_1479_fu_20419_p4 = sh_amt_11_fu_20401_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_147_cast_fu_6870_p1() {
    tmp_147_cast_fu_6870_p1 = esl_sext<64,11>(tmp_81_fu_6865_p2.read());
}

void mnist_fp16_opt::thread_tmp_147_fu_4861_p2() {
    tmp_147_fu_4861_p2 = (tmp_11_reg_23152.read() | tmp_146_fu_4856_p2.read());
}

void mnist_fp16_opt::thread_tmp_147_mid2_v_fu_14651_p3() {
    tmp_147_mid2_v_fu_14651_p3 = (!exitcond_flatten26_fu_14637_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten26_fu_14637_p2.read()[0].to_bool())? ff3_V_fu_14631_p2.read(): p_30_reg_3360.read());
}

void mnist_fp16_opt::thread_tmp_1480_fu_20566_p1() {
    tmp_1480_fu_20566_p1 = tmp_573_fu_20561_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1481_fu_20586_p1() {
    tmp_1481_fu_20586_p1 = tmp_576_fu_20580_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1482_fu_21902_p1() {
    tmp_1482_fu_21902_p1 = ireg_V_s_fu_21898_p1.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_1484_fu_21924_p1() {
    tmp_1484_fu_21924_p1 = ireg_V_s_fu_21898_p1.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_1485_fu_21998_p1() {
    tmp_1485_fu_21998_p1 = man_V_26_fu_21948_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1486_fu_22002_p4() {
    tmp_1486_fu_22002_p4 = sh_amt_9_fu_21984_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_1487_fu_22071_p1() {
    tmp_1487_fu_22071_p1 = tmp_505_fu_22066_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1488_fu_22091_p1() {
    tmp_1488_fu_22091_p1 = tmp_508_fu_22085_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1489_fu_21548_p1() {
    tmp_1489_fu_21548_p1 = tmp_1253_fu_21539_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_148_fu_13814_p2() {
    tmp_148_fu_13814_p2 = (!ap_phi_mux_p_31_phi_fu_3316_p4.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_31_phi_fu_3316_p4.read() != ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_148_mid1_fu_13976_p2() {
    tmp_148_mid1_fu_13976_p2 = (!index_tuple3_V_fu_13956_p2.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(index_tuple3_V_fu_13956_p2.read() != ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_1491_fu_21642_p1() {
    tmp_1491_fu_21642_p1 = msb_idx_15_fu_21636_p2.read().range(31-1, 0);
}

void mnist_fp16_opt::thread_tmp_1493_fu_21664_p4() {
    tmp_1493_fu_21664_p4 = msb_idx_16_fu_21654_p3.read().range(30, 5);
}

void mnist_fp16_opt::thread_tmp_1494_fu_21695_p1() {
    tmp_1494_fu_21695_p1 = msb_idx_16_fu_21654_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_1495_fu_21699_p2() {
    tmp_1495_fu_21699_p2 = (!ap_const_lv4_1.is_01() || !tmp_1494_fu_21695_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_1494_fu_21695_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1496_fu_21705_p1() {
    tmp_1496_fu_21705_p1 = esl_zext<16,4>(tmp_1495_fu_21699_p2.read());
}

void mnist_fp16_opt::thread_tmp_1497_fu_21709_p2() {
    tmp_1497_fu_21709_p2 = (!tmp_1496_fu_21705_p1.read().is_01())? sc_lv<16>(): tmp_V_8_reg_27776.read() >> (unsigned short)tmp_1496_fu_21705_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_1498_fu_21746_p1() {
    tmp_1498_fu_21746_p1 = msb_idx_15_reg_27782_pp23_iter10_reg.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1499_fu_21804_p1() {
    tmp_1499_fu_21804_p1 = p_03_i5_to_int_fu_21791_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_149_fu_13820_p2() {
    tmp_149_fu_13820_p2 = (!ap_phi_mux_p_31_phi_fu_3316_p4.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(ap_phi_mux_p_31_phi_fu_3316_p4.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp16_opt::thread_tmp_149_mid1_fu_13982_p2() {
    tmp_149_mid1_fu_13982_p2 = (!index_tuple3_V_fu_13956_p2.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(index_tuple3_V_fu_13956_p2.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp16_opt::thread_tmp_1500_fu_21822_p1() {
    tmp_1500_fu_21822_p1 = tmp_457_to_int_fu_21808_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1501_fu_22482_p1() {
    tmp_1501_fu_22482_p1 = p_a_assign_load_to_i_fu_22468_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1502_fu_22500_p1() {
    tmp_1502_fu_22500_p1 = compute14_to_int_fu_22486_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1504_fu_22297_p1() {
    tmp_1504_fu_22297_p1 = msb_idx_17_fu_22291_p2.read().range(31-1, 0);
}

void mnist_fp16_opt::thread_tmp_1506_fu_22319_p4() {
    tmp_1506_fu_22319_p4 = msb_idx_18_fu_22309_p3.read().range(30, 5);
}

void mnist_fp16_opt::thread_tmp_1507_fu_22350_p1() {
    tmp_1507_fu_22350_p1 = msb_idx_18_fu_22309_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_1508_fu_22354_p2() {
    tmp_1508_fu_22354_p2 = (!ap_const_lv4_1.is_01() || !tmp_1507_fu_22350_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_1507_fu_22350_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1509_fu_22360_p1() {
    tmp_1509_fu_22360_p1 = esl_zext<16,4>(tmp_1508_fu_22354_p2.read());
}

void mnist_fp16_opt::thread_tmp_150_fu_4866_p2() {
    tmp_150_fu_4866_p2 = (tmp_25_reg_23169.read() | tmp_147_fu_4861_p2.read());
}

void mnist_fp16_opt::thread_tmp_150_mid2_cast_fu_13415_p1() {
    tmp_150_mid2_cast_fu_13415_p1 = esl_zext<10,4>(tmp_150_mid2_fu_13407_p3.read());
}

void mnist_fp16_opt::thread_tmp_150_mid2_fu_13407_p3() {
    tmp_150_mid2_fu_13407_p3 = (!exitcond48_mid_fu_13381_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond48_mid_fu_13381_p2.read()[0].to_bool())? args12_V_fu_13387_p2.read(): p_41_mid_fu_13319_p3.read());
}

void mnist_fp16_opt::thread_tmp_1510_fu_22364_p2() {
    tmp_1510_fu_22364_p2 = (!tmp_1509_fu_22360_p1.read().is_01())? sc_lv<16>(): tmp_V_9_reg_27962.read() >> (unsigned short)tmp_1509_fu_22360_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_1511_fu_22405_p1() {
    tmp_1511_fu_22405_p1 = msb_idx_17_reg_27968.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1512_fu_22624_p1() {
    tmp_1512_fu_22624_p1 = pred_2_to_int_fu_22611_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1513_fu_22642_p1() {
    tmp_1513_fu_22642_p1 = pred_to_int_fu_22628_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_151_cast_fu_13264_p1() {
    tmp_151_cast_fu_13264_p1 = esl_zext<12,4>(p_29_mid2_reg_25433.read());
}

void mnist_fp16_opt::thread_tmp_151_fu_4626_p2() {
    tmp_151_fu_4626_p2 = (!tmp_s_reg_23097.read().is_01() || !tmp_6_cast_fu_4622_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_s_reg_23097.read()) + sc_biguint<11>(tmp_6_cast_fu_4622_p1.read()));
}

void mnist_fp16_opt::thread_tmp_152_cast_fu_4701_p1() {
    tmp_152_cast_fu_4701_p1 = esl_sext<64,11>(tmp_83_fu_4695_p2.read());
}

void mnist_fp16_opt::thread_tmp_152_fu_5837_p1() {
    tmp_152_fu_5837_p1 = mul1_fu_22812_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_152_mid2_cast_fu_13079_p1() {
    tmp_152_mid2_cast_fu_13079_p1 = esl_zext<7,3>(tmp_152_mid2_v_fu_13071_p3.read());
}

void mnist_fp16_opt::thread_tmp_152_mid2_v_fu_13071_p3() {
    tmp_152_mid2_v_fu_13071_p3 = (!exitcond_flatten22_fu_13057_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten22_fu_13057_p2.read()[0].to_bool())? rc1_V_fu_13051_p2.read(): ap_phi_mux_p_32_phi_fu_3171_p4.read());
}

void mnist_fp16_opt::thread_tmp_157_fu_5854_p4() {
    tmp_157_fu_5854_p4 = neg_mul1_fu_5849_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_159_fu_10280_p1() {
    tmp_159_fu_10280_p1 = esl_sext<32,16>(tmp_602_reg_24693.read());
}

void mnist_fp16_opt::thread_tmp_160_fu_10283_p2() {
    tmp_160_fu_10283_p2 = (!sh_amt_1_cast_fu_10252_p1.read().is_01())? sc_lv<32>(): tmp_159_fu_10280_p1.read() << (unsigned short)sh_amt_1_cast_fu_10252_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_161_fu_5864_p1() {
    tmp_161_fu_5864_p1 = esl_sext<12,8>(tmp_157_fu_5854_p4.read());
}

void mnist_fp16_opt::thread_tmp_162_fu_9306_p1() {
    tmp_162_fu_9306_p1 = esl_zext<54,32>(sh_amt_2_cast_fu_9298_p1.read());
}

void mnist_fp16_opt::thread_tmp_163_fu_9310_p2() {
    tmp_163_fu_9310_p2 = (!man_V_4_reg_24430.read().is_01() || !tmp_162_fu_9306_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_4_reg_24430.read()) >> (unsigned short)tmp_162_fu_9306_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_165_fu_9326_p1() {
    tmp_165_fu_9326_p1 = esl_sext<32,16>(tmp_687_reg_24453.read());
}

void mnist_fp16_opt::thread_tmp_166_fu_9329_p2() {
    tmp_166_fu_9329_p2 = (!sh_amt_2_cast_fu_9298_p1.read().is_01())? sc_lv<32>(): tmp_165_fu_9326_p1.read() << (unsigned short)sh_amt_2_cast_fu_9298_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_167_fu_5868_p1() {
    tmp_167_fu_5868_p1 = esl_sext<12,9>(tmp_164_reg_23568.read());
}

void mnist_fp16_opt::thread_tmp_169_cast_fu_9042_p1() {
    tmp_169_cast_fu_9042_p1 = esl_zext<9,2>(p_25_mid2_reg_24321.read());
}

void mnist_fp16_opt::thread_tmp_169_fu_5871_p3() {
    tmp_169_fu_5871_p3 = (!tmp_153_reg_23562.read()[0].is_01())? sc_lv<12>(): ((tmp_153_reg_23562.read()[0].to_bool())? tmp_161_fu_5864_p1.read(): tmp_167_fu_5868_p1.read());
}

void mnist_fp16_opt::thread_tmp_16_fu_4660_p3() {
    tmp_16_fu_4660_p3 = esl_concat<5,5>(ap_phi_mux_p_s_phi_fu_2277_p4.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_170_fu_11381_p3() {
    tmp_170_fu_11381_p3 = (!tmp_934_reg_25031.read()[0].is_01())? sc_lv<12>(): ((tmp_934_reg_25031.read()[0].to_bool())? neg_ti15_fu_11375_p2.read(): tmp_893_fu_11365_p1.read());
}

void mnist_fp16_opt::thread_tmp_171_fu_9213_p1() {
    tmp_171_fu_9213_p1 = esl_zext<12,11>(exp_tmp_V_1_reg_24414.read());
}

void mnist_fp16_opt::thread_tmp_172_fu_7623_p1() {
    tmp_172_fu_7623_p1 = esl_zext<54,32>(sh_amt_cast_fu_7615_p1.read());
}

void mnist_fp16_opt::thread_tmp_173_fu_9122_p2() {
    tmp_173_fu_9122_p2 = (!tmp_691_fu_9096_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_691_fu_9096_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_175_fu_7627_p2() {
    tmp_175_fu_7627_p2 = (!man_V_2_reg_24009.read().is_01() || !tmp_172_fu_7623_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_2_reg_24009.read()) >> (unsigned short)tmp_172_fu_7623_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_176_fu_9246_p2() {
    tmp_176_fu_9246_p2 = (!F2_3_fu_9240_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_3_fu_9240_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_177_fu_9252_p2() {
    tmp_177_fu_9252_p2 = (!ap_const_lv12_FF2.is_01() || !F2_3_fu_9240_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_3_fu_9240_p2.read()));
}

void mnist_fp16_opt::thread_tmp_178_fu_9258_p2() {
    tmp_178_fu_9258_p2 = (!ap_const_lv12_E.is_01() || !F2_3_fu_9240_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_3_fu_9240_p2.read()));
}

void mnist_fp16_opt::thread_tmp_179_fu_9272_p2() {
    tmp_179_fu_9272_p2 = (!F2_3_fu_9240_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_3_fu_9240_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_17_fu_4672_p3() {
    tmp_17_fu_4672_p3 = esl_concat<5,1>(ap_phi_mux_p_s_phi_fu_2277_p4.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_180_fu_6743_p1() {
    tmp_180_fu_6743_p1 = grp_fu_5891_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_181_fu_6755_p1() {
    tmp_181_fu_6755_p1 = grp_fu_5891_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_181_mid2_cast_fu_14751_p1() {
    tmp_181_mid2_cast_fu_14751_p1 = esl_zext<10,4>(tmp_181_mid2_fu_14743_p3.read());
}

void mnist_fp16_opt::thread_tmp_181_mid2_fu_14743_p3() {
    tmp_181_mid2_fu_14743_p3 = (!exitcond40_mid_fu_14717_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond40_mid_fu_14717_p2.read()[0].to_bool())? yy3_V_fu_14723_p2.read(): p_34_mid_fu_14643_p3.read());
}

void mnist_fp16_opt::thread_tmp_182_fu_14111_p2() {
    tmp_182_fu_14111_p2 = (!p_35_mid2_reg_25702.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_35_mid2_reg_25702.read() != ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_183_fu_14116_p2() {
    tmp_183_fu_14116_p2 = (!p_35_mid2_reg_25702.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_35_mid2_reg_25702.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp16_opt::thread_tmp_184_cast_fu_13473_p1() {
    tmp_184_cast_fu_13473_p1 = esl_zext<12,4>(p_47_mid2_reg_25549.read());
}

void mnist_fp16_opt::thread_tmp_184_fu_6767_p2() {
    tmp_184_fu_6767_p2 = (!p_shl19_cast_fu_6747_p3.read().is_01() || !p_shl20_cast_fu_6759_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl19_cast_fu_6747_p3.read()) - sc_biguint<11>(p_shl20_cast_fu_6759_p3.read()));
}

void mnist_fp16_opt::thread_tmp_185_fu_6773_p2() {
    tmp_185_fu_6773_p2 = (tmp_184_fu_6767_p2.read() | ap_const_lv11_2);
}

void mnist_fp16_opt::thread_tmp_186_fu_6784_p2() {
    tmp_186_fu_6784_p2 = (tmp_184_fu_6767_p2.read() | ap_const_lv11_3);
}

void mnist_fp16_opt::thread_tmp_186_mid2235_v_fu_13110_p3() {
    tmp_186_mid2235_v_fu_13110_p3 = (!exitcond_flatten22_fu_13057_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten22_fu_13057_p2.read()[0].to_bool())? tmp_95_mid2_reg_25440.read(): r_V_9_fu_13034_p2.read());
}

void mnist_fp16_opt::thread_tmp_186_mid2_v_fu_13164_p3() {
    tmp_186_mid2_v_fu_13164_p3 = (!exitcond41_mid_fu_13129_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond41_mid_fu_13129_p2.read()[0].to_bool())? r_V_11_mid1_fu_13159_p2.read(): tmp_186_mid2235_v_fu_13110_p3.read());
}

void mnist_fp16_opt::thread_tmp_187_fu_7261_p3() {
    tmp_187_fu_7261_p3 = esl_concat<3,5>(tmp_19_mid2_v_fu_7253_p3.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_187_mid2_cast_fu_13180_p1() {
    tmp_187_mid2_cast_fu_13180_p1 = esl_zext<64,2>(tmp_187_mid2_fu_13172_p3.read());
}

void mnist_fp16_opt::thread_tmp_187_mid2_fu_13172_p3() {
    tmp_187_mid2_fu_13172_p3 = (!exitcond41_mid_fu_13129_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond41_mid_fu_13129_p2.read()[0].to_bool())? ry2_V_fu_13135_p2.read(): p_37_mid_fu_13063_p3.read());
}

void mnist_fp16_opt::thread_tmp_188_fu_10729_p2() {
    tmp_188_fu_10729_p2 = (!ap_const_lv16_0.is_01() || !relu2_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu2_0_V_q0.read()));
}

void mnist_fp16_opt::thread_tmp_189_fu_7273_p3() {
    tmp_189_fu_7273_p3 = esl_concat<3,2>(tmp_19_mid2_v_fu_7253_p3.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_190_fu_14153_p3() {
    tmp_190_fu_14153_p3 = (!tmp_926_fu_14141_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_926_fu_14141_p2.read()[0].to_bool())? r_V_12_fu_14135_p2.read(): tmp_927_fu_14147_p2.read());
}

void mnist_fp16_opt::thread_tmp_191_cast_fu_14377_p1() {
    tmp_191_cast_fu_14377_p1 = esl_zext<12,4>(tmp_190_reg_25746_pp13_iter18_reg.read());
}

void mnist_fp16_opt::thread_tmp_191_fu_7285_p2() {
    tmp_191_fu_7285_p2 = (!p_shl21_cast_fu_7269_p1.read().is_01() || !p_shl22_cast_fu_7281_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl21_cast_fu_7269_p1.read()) - sc_biguint<9>(p_shl22_cast_fu_7281_p1.read()));
}

void mnist_fp16_opt::thread_tmp_192_fu_7643_p1() {
    tmp_192_fu_7643_p1 = esl_sext<32,16>(tmp_214_reg_24025.read());
}

void mnist_fp16_opt::thread_tmp_194_fu_7319_p2() {
    tmp_194_fu_7319_p2 = (exitcond17_mid_fu_7307_p2.read() | exitcond_flatten4_fu_7239_p2.read());
}

void mnist_fp16_opt::thread_tmp_196_fu_7646_p2() {
    tmp_196_fu_7646_p2 = (!sh_amt_cast_fu_7615_p1.read().is_01())? sc_lv<32>(): tmp_192_fu_7643_p1.read() << (unsigned short)sh_amt_cast_fu_7615_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_197_fu_7345_p2() {
    tmp_197_fu_7345_p2 = (!tmp_30_mid2_cast_fu_7341_p1.read().is_01() || !tmp_325_cast_fu_7291_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_30_mid2_cast_fu_7341_p1.read()) + sc_bigint<10>(tmp_325_cast_fu_7291_p1.read()));
}

void mnist_fp16_opt::thread_tmp_199_cast_fu_6707_p1() {
    tmp_199_cast_fu_6707_p1 = esl_sext<64,11>(tmp_113_fu_6701_p2.read());
}

void mnist_fp16_opt::thread_tmp_199_fu_7351_p1() {
    tmp_199_fu_7351_p1 = tmp_197_fu_7345_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_19_cast_fu_4690_p1() {
    tmp_19_cast_fu_4690_p1 = esl_sext<64,11>(tmp_19_fu_4684_p2.read());
}

void mnist_fp16_opt::thread_tmp_19_fu_4684_p2() {
    tmp_19_fu_4684_p2 = (!p_shl10_cast_fu_4668_p1.read().is_01() || !p_shl11_cast_fu_4680_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl10_cast_fu_4668_p1.read()) - sc_biguint<11>(p_shl11_cast_fu_4680_p1.read()));
}

void mnist_fp16_opt::thread_tmp_19_mid2_v_fu_7253_p3() {
    tmp_19_mid2_v_fu_7253_p3 = (!exitcond_flatten4_fu_7239_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten4_fu_7239_p2.read()[0].to_bool())? args0_V_fu_7233_p2.read(): ap_phi_mux_p_7_phi_fu_2583_p4.read());
}

void mnist_fp16_opt::thread_tmp_1_fu_4561_p3() {
    tmp_1_fu_4561_p3 = esl_concat<5,5>(p_0_reg_2156.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_200_fu_14415_p2() {
    tmp_200_fu_14415_p2 = (!relu3_0_V_q0.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu3_0_V_q0.read() == ap_const_lv16_0);
}

void mnist_fp16_opt::thread_tmp_201_fu_7382_p3() {
    tmp_201_fu_7382_p3 = esl_concat<10,2>(tmp_197_reg_23935.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_202_fu_7393_p2() {
    tmp_202_fu_7393_p2 = (!p_shl23_cast_fu_7375_p3.read().is_01() || !p_shl24_cast_fu_7389_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl23_cast_fu_7375_p3.read()) - sc_bigint<13>(p_shl24_cast_fu_7389_p1.read()));
}

void mnist_fp16_opt::thread_tmp_203_fu_9450_p2() {
    tmp_203_fu_9450_p2 = (!sh_amt_3_reg_24475.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_3_reg_24475.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_204_fu_7402_p2() {
    tmp_204_fu_7402_p2 = (!tmp_43_cast_fu_7399_p1.read().is_01() || !tmp_202_fu_7393_p2.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_43_cast_fu_7399_p1.read()) + sc_biguint<13>(tmp_202_fu_7393_p2.read()));
}

void mnist_fp16_opt::thread_tmp_205_fu_7426_p1() {
    tmp_205_fu_7426_p1 = conv1_0_load_to_int_fu_7413_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_206_fu_9216_p3() {
    tmp_206_fu_9216_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_693_reg_24419.read());
}

void mnist_fp16_opt::thread_tmp_207_cast_fu_6718_p1() {
    tmp_207_cast_fu_6718_p1 = esl_zext<64,11>(tmp_119_fu_6712_p2.read());
}

void mnist_fp16_opt::thread_tmp_207_fu_7463_p1() {
    tmp_207_fu_7463_p1 = ireg_V_fu_7455_p3.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_207_mid2_v_fu_15860_p3() {
    tmp_207_mid2_v_fu_15860_p3 = (!exitcond_flatten29_fu_15846_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten29_fu_15846_p2.read()[0].to_bool())? args03_V_fu_15840_p2.read(): ap_phi_mux_p_50_phi_fu_3487_p4.read());
}

void mnist_fp16_opt::thread_tmp_209_fu_13572_p3() {
    tmp_209_fu_13572_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_890_reg_25622.read());
}

void mnist_fp16_opt::thread_tmp_210_fu_13569_p1() {
    tmp_210_fu_13569_p1 = esl_zext<12,11>(p_Result_18_reg_25617.read());
}

void mnist_fp16_opt::thread_tmp_211_mid2_v_fu_9890_p3() {
    tmp_211_mid2_v_fu_9890_p3 = (!exitcond_flatten11_fu_9876_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten11_fu_9876_p2.read()[0].to_bool())? args01_V_fu_9870_p2.read(): ap_phi_mux_p_19_phi_fu_2835_p4.read());
}

void mnist_fp16_opt::thread_tmp_212_fu_13563_p2() {
    tmp_212_fu_13563_p2 = (!tmp_881_fu_13537_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_881_fu_13537_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_213_cast_fu_6986_p1() {
    tmp_213_cast_fu_6986_p1 = esl_sext<7,6>(tmp_121_fu_6980_p2.read());
}

void mnist_fp16_opt::thread_tmp_213_fu_7485_p1() {
    tmp_213_fu_7485_p1 = ireg_V_fu_7455_p3.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_214_cast_fu_13245_p1() {
    tmp_214_cast_fu_13245_p1 = esl_zext<10,2>(p_40_mid2_reg_25468.read());
}

void mnist_fp16_opt::thread_tmp_214_fu_7560_p1() {
    tmp_214_fu_7560_p1 = man_V_2_fu_7515_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_216_fu_10818_p2() {
    tmp_216_fu_10818_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_5_cast_fu_10795_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_5_cast_fu_10795_p1.read()));
}

void mnist_fp16_opt::thread_tmp_217_fu_7564_p4() {
    tmp_217_fu_7564_p4 = sh_amt_fu_7546_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_218_fu_9455_p1() {
    tmp_218_fu_9455_p1 = esl_zext<54,32>(sh_amt_3_cast_fu_9447_p1.read());
}

void mnist_fp16_opt::thread_tmp_219_fu_9459_p2() {
    tmp_219_fu_9459_p2 = (!man_V_9_reg_24464.read().is_01() || !tmp_218_fu_9455_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_9_reg_24464.read()) >> (unsigned short)tmp_218_fu_9455_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_21_cast_fu_6728_p1() {
    tmp_21_cast_fu_6728_p1 = esl_zext<64,11>(tmp_21_fu_6723_p2.read());
}

void mnist_fp16_opt::thread_tmp_21_fu_6723_p2() {
    tmp_21_fu_6723_p2 = (tmp_19_reg_23186_pp3_iter2_reg.read() | ap_const_lv11_1);
}

void mnist_fp16_opt::thread_tmp_21_mid2_v_fu_8663_p3() {
    tmp_21_mid2_v_fu_8663_p3 = (!exitcond_flatten8_reg_24270.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten8_reg_24270.read()[0].to_bool())? ff1_V_fu_8657_p2.read(): p_9_reg_2707.read());
}

void mnist_fp16_opt::thread_tmp_220_fu_7632_p1() {
    tmp_220_fu_7632_p1 = tmp_175_fu_7627_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_221_fu_9475_p1() {
    tmp_221_fu_9475_p1 = esl_sext<32,16>(tmp_694_reg_24487.read());
}

void mnist_fp16_opt::thread_tmp_222_fu_9478_p2() {
    tmp_222_fu_9478_p2 = (!sh_amt_3_cast_fu_9447_p1.read().is_01())? sc_lv<32>(): tmp_221_fu_9475_p1.read() << (unsigned short)sh_amt_3_cast_fu_9447_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_223_fu_7652_p1() {
    tmp_223_fu_7652_p1 = tmp_196_fu_7646_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_224_fu_11423_p3() {
    tmp_224_fu_11423_p3 = (!tmp_1077_reg_25054.read()[0].is_01())? sc_lv<12>(): ((tmp_1077_reg_25054.read()[0].to_bool())? neg_ti20_fu_11417_p2.read(): tmp_946_fu_11407_p1.read());
}

void mnist_fp16_opt::thread_tmp_225_fu_7193_p2() {
    tmp_225_fu_7193_p2 = (!tmp_141_reg_23839.read().is_01() || !tmp_25_cast_fu_7190_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_141_reg_23839.read()) + sc_biguint<13>(tmp_25_cast_fu_7190_p1.read()));
}

void mnist_fp16_opt::thread_tmp_225_mid2_cast_fu_9978_p1() {
    tmp_225_mid2_cast_fu_9978_p1 = esl_zext<10,5>(tmp_225_mid2_fu_9970_p3.read());
}

void mnist_fp16_opt::thread_tmp_225_mid2_fu_9970_p3() {
    tmp_225_mid2_fu_9970_p3 = (!exitcond29_mid_fu_9944_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond29_mid_fu_9944_p2.read()[0].to_bool())? args11_V_fu_9950_p2.read(): p_23_mid_fu_9882_p3.read());
}

void mnist_fp16_opt::thread_tmp_227_fu_7109_p3() {
    tmp_227_fu_7109_p3 = esl_concat<5,5>(tmp_26_mid2_v_reg_23865.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_228_fu_7120_p3() {
    tmp_228_fu_7120_p3 = esl_concat<5,1>(tmp_26_mid2_v_reg_23865.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_229_fu_7131_p2() {
    tmp_229_fu_7131_p2 = (!p_shl25_cast_fu_7116_p1.read().is_01() || !p_shl26_cast_fu_7127_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl25_cast_fu_7116_p1.read()) - sc_biguint<11>(p_shl26_cast_fu_7127_p1.read()));
}

void mnist_fp16_opt::thread_tmp_230_fu_14421_p2() {
    tmp_230_fu_14421_p2 = (!ap_const_lv16_0.is_01() || !relu3_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu3_0_V_q0.read()));
}

void mnist_fp16_opt::thread_tmp_231_cast_fu_10036_p1() {
    tmp_231_cast_fu_10036_p1 = esl_zext<13,5>(p_28_mid2_reg_24593.read());
}

void mnist_fp16_opt::thread_tmp_231_fu_7140_p2() {
    tmp_231_fu_7140_p2 = (!tmp_27_mid2_cast_fu_7137_p1.read().is_01() || !tmp_213_cast_reg_23834.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_27_mid2_cast_fu_7137_p1.read()) + sc_bigint<7>(tmp_213_cast_reg_23834.read()));
}

void mnist_fp16_opt::thread_tmp_232_fu_10053_p4() {
    tmp_232_fu_10053_p4 = conv2_0_load_to_int_fu_10050_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_233_fu_10897_p3() {
    tmp_233_fu_10897_p3 = esl_concat<1,8>(tmp_768_reg_24823_pp9_iter10_reg.read(), p_Repl2_7_trunc_fu_10891_p2.read());
}

void mnist_fp16_opt::thread_tmp_236_fu_10875_p2() {
    tmp_236_fu_10875_p2 = (!p_Result_16_fu_10865_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_16_fu_10865_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16_opt::thread_tmp_237_cast_fu_7008_p1() {
    tmp_237_cast_fu_7008_p1 = esl_sext<10,9>(tmp_134_fu_7002_p2.read());
}

void mnist_fp16_opt::thread_tmp_237_fu_7145_p2() {
    tmp_237_fu_7145_p2 = (!ap_const_lv7_2.is_01())? sc_lv<7>(): tmp_231_fu_7140_p2.read() << (unsigned short)ap_const_lv7_2.to_uint();
}

void mnist_fp16_opt::thread_tmp_238_fu_7151_p2() {
    tmp_238_fu_7151_p2 = (!tmp_237_fu_7145_p2.read().is_01() || !tmp_231_fu_7140_p2.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_237_fu_7145_p2.read()) - sc_biguint<7>(tmp_231_fu_7140_p2.read()));
}

void mnist_fp16_opt::thread_tmp_238_mid2_cast_fu_16377_p1() {
    tmp_238_mid2_cast_fu_16377_p1 = esl_zext<8,4>(tmp_238_mid2_v_reg_26334.read());
}

void mnist_fp16_opt::thread_tmp_238_mid2_v_fu_16369_p3() {
    tmp_238_mid2_v_fu_16369_p3 = (!exitcond_flatten32_fu_16355_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten32_fu_16355_p2.read()[0].to_bool())? c1_V_fu_16349_p2.read(): p_54_reg_3538.read());
}

void mnist_fp16_opt::thread_tmp_239_fu_7160_p2() {
    tmp_239_fu_7160_p2 = (!tmp_34_cast_fu_7157_p1.read().is_01() || !tmp_229_fu_7131_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_34_cast_fu_7157_p1.read()) + sc_biguint<11>(tmp_229_fu_7131_p2.read()));
}

void mnist_fp16_opt::thread_tmp_23_cast_fu_6738_p1() {
    tmp_23_cast_fu_6738_p1 = esl_sext<64,11>(tmp_23_fu_6733_p2.read());
}

void mnist_fp16_opt::thread_tmp_23_fu_6733_p2() {
    tmp_23_fu_6733_p2 = (!tmp_19_reg_23186_pp3_iter2_reg.read().is_01() || !ap_const_lv11_2.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter2_reg.read()) + sc_biguint<11>(ap_const_lv11_2));
}

void mnist_fp16_opt::thread_tmp_240_fu_15680_p2() {
    tmp_240_fu_15680_p2 = (!p_Val2_43_reg_3449.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(p_Val2_43_reg_3449.read() == ap_const_lv16_0);
}

void mnist_fp16_opt::thread_tmp_241_fu_7174_p2() {
    tmp_241_fu_7174_p2 = (!tmp_35_cast_fu_7171_p1.read().is_01() || !tmp_238_fu_7151_p2.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_35_cast_fu_7171_p1.read()) + sc_biguint<7>(tmp_238_fu_7151_p2.read()));
}

void mnist_fp16_opt::thread_tmp_241_mid2_cast_fu_14840_p1() {
    tmp_241_mid2_cast_fu_14840_p1 = esl_zext<8,4>(tmp_241_mid2_v_fu_14832_p3.read());
}

void mnist_fp16_opt::thread_tmp_241_mid2_v_fu_14832_p3() {
    tmp_241_mid2_v_fu_14832_p3 = (!exitcond_flatten30_fu_14818_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten30_fu_14818_p2.read()[0].to_bool())? rc2_V_fu_14812_p2.read(): ap_phi_mux_p_44_phi_fu_3420_p4.read());
}

void mnist_fp16_opt::thread_tmp_243_fu_4746_p2() {
    tmp_243_fu_4746_p2 = (grp_fu_4400_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_244_fu_4751_p2() {
    tmp_244_fu_4751_p2 = (tmp_11_reg_23152.read() | tmp_243_fu_4746_p2.read());
}

void mnist_fp16_opt::thread_tmp_245_fu_10079_p2() {
    tmp_245_fu_10079_p2 = (notrhs1_reg_24645.read() | notlhs1_reg_24640.read());
}

void mnist_fp16_opt::thread_tmp_246_cast_fu_14161_p1() {
    tmp_246_cast_fu_14161_p1 = esl_zext<5,4>(tmp_190_fu_14153_p3.read());
}

void mnist_fp16_opt::thread_tmp_247_fu_14513_p2() {
    tmp_247_fu_14513_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_7_cast_fu_14490_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_7_cast_fu_14490_p1.read()));
}

void mnist_fp16_opt::thread_tmp_248_fu_4756_p2() {
    tmp_248_fu_4756_p2 = (tmp_25_reg_23169.read() | tmp_244_fu_4751_p2.read());
}

void mnist_fp16_opt::thread_tmp_251_fu_4765_p1() {
    tmp_251_fu_4765_p1 = mul2_fu_22708_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_252_mid2_cast_fu_18035_p1() {
    tmp_252_mid2_cast_fu_18035_p1 = esl_zext<9,5>(tmp_252_mid2_v_fu_18027_p3.read());
}

void mnist_fp16_opt::thread_tmp_252_mid2_v_fu_18027_p3() {
    tmp_252_mid2_v_fu_18027_p3 = (!exitcond_flatten36_fu_18013_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten36_fu_18013_p2.read()[0].to_bool())? ff4_V_fu_18007_p2.read(): p_45_reg_3726.read());
}

void mnist_fp16_opt::thread_tmp_253_fu_10083_p2() {
    tmp_253_fu_10083_p2 = (tmp_245_fu_10079_p2.read() & tmp_246_reg_24650.read());
}

void mnist_fp16_opt::thread_tmp_256_fu_11514_p3() {
    tmp_256_fu_11514_p3 = (!tmp_1213_reg_25087.read()[0].is_01())? sc_lv<12>(): ((tmp_1213_reg_25087.read()[0].to_bool())? neg_ti23_fu_11508_p2.read(): tmp_983_fu_11498_p1.read());
}

void mnist_fp16_opt::thread_tmp_257_fu_15589_p2() {
    tmp_257_fu_15589_p2 = (!ap_const_lv16_0.is_01() || !p_Val2_43_reg_3449.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(p_Val2_43_reg_3449.read()));
}

void mnist_fp16_opt::thread_tmp_258_cast_fu_4631_p1() {
    tmp_258_cast_fu_4631_p1 = esl_sext<64,11>(tmp_151_fu_4626_p2.read());
}

void mnist_fp16_opt::thread_tmp_258_fu_4782_p4() {
    tmp_258_fu_4782_p4 = neg_mul2_fu_4777_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_258_mid2354_v_fu_14871_p3() {
    tmp_258_mid2354_v_fu_14871_p3 = (!exitcond_flatten30_fu_14818_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten30_fu_14818_p2.read()[0].to_bool())? tmp_181_mid2_reg_25902.read(): r_V_14_fu_14795_p2.read());
}

void mnist_fp16_opt::thread_tmp_258_mid2_v_fu_14925_p3() {
    tmp_258_mid2_v_fu_14925_p3 = (!exitcond52_mid_fu_14890_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond52_mid_fu_14890_p2.read()[0].to_bool())? r_V_17_mid1_fu_14920_p2.read(): tmp_258_mid2354_v_fu_14871_p3.read());
}

void mnist_fp16_opt::thread_tmp_259_fu_4792_p1() {
    tmp_259_fu_4792_p1 = esl_sext<12,8>(tmp_258_fu_4782_p4.read());
}

void mnist_fp16_opt::thread_tmp_259_mid2_cast_fu_14941_p1() {
    tmp_259_mid2_cast_fu_14941_p1 = esl_zext<64,2>(tmp_259_mid2_fu_14933_p3.read());
}

void mnist_fp16_opt::thread_tmp_259_mid2_fu_14933_p3() {
    tmp_259_mid2_fu_14933_p3 = (!exitcond52_mid_fu_14890_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond52_mid_fu_14890_p2.read()[0].to_bool())? ry3_V_fu_14896_p2.read(): p_48_mid_fu_14824_p3.read());
}

void mnist_fp16_opt::thread_tmp_25_cast_fu_7190_p1() {
    tmp_25_cast_fu_7190_p1 = esl_zext<13,5>(p_8_mid2_reg_23815.read());
}

void mnist_fp16_opt::thread_tmp_25_fu_4654_p2() {
    tmp_25_fu_4654_p2 = (!ap_phi_mux_p_s_phi_fu_2277_p4.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_s_phi_fu_2277_p4.read() == ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_260_fu_14570_p2() {
    tmp_260_fu_14570_p2 = (!p_Result_22_fu_14560_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_22_fu_14560_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16_opt::thread_tmp_265_fu_4796_p1() {
    tmp_265_fu_4796_p1 = esl_sext<12,9>(tmp_262_reg_23247.read());
}

void mnist_fp16_opt::thread_tmp_266_fu_4799_p3() {
    tmp_266_fu_4799_p3 = (!tmp_252_reg_23241.read()[0].is_01())? sc_lv<12>(): ((tmp_252_reg_23241.read()[0].to_bool())? tmp_259_fu_4792_p1.read(): tmp_265_fu_4796_p1.read());
}

void mnist_fp16_opt::thread_tmp_268_fu_5897_p1() {
    tmp_268_fu_5897_p1 = grp_fu_4819_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_269_fu_5909_p1() {
    tmp_269_fu_5909_p1 = grp_fu_4819_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_26_cast_fu_6800_p1() {
    tmp_26_cast_fu_6800_p1 = esl_sext<64,11>(tmp_26_fu_6795_p2.read());
}

void mnist_fp16_opt::thread_tmp_26_fu_6795_p2() {
    tmp_26_fu_6795_p2 = (!tmp_19_reg_23186_pp3_iter2_reg.read().is_01() || !ap_const_lv11_3.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter2_reg.read()) + sc_biguint<11>(ap_const_lv11_3));
}

void mnist_fp16_opt::thread_tmp_26_mid2_v_fu_7095_p2() {
    tmp_26_mid2_v_fu_7095_p2 = (!tmp_mid2_reg_23822.read().is_01() || !tmp_26_mid2_v_v_fu_7091_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_mid2_reg_23822.read()) + sc_biguint<5>(tmp_26_mid2_v_v_fu_7091_p1.read()));
}

void mnist_fp16_opt::thread_tmp_26_mid2_v_v_fu_7091_p1() {
    tmp_26_mid2_v_v_fu_7091_p1 = esl_zext<5,2>(tmp_26_mid2_v_v_v_fu_7083_p3.read());
}

void mnist_fp16_opt::thread_tmp_26_mid2_v_v_v_fu_7083_p3() {
    tmp_26_mid2_v_v_v_fu_7083_p3 = (!exitcond5_fu_7069_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond5_fu_7069_p2.read()[0].to_bool())? ry_V_fu_7063_p2.read(): ap_phi_mux_p_4_phi_fu_2537_p4.read());
}

void mnist_fp16_opt::thread_tmp_271_cast_fu_14180_p1() {
    tmp_271_cast_fu_14180_p1 = esl_zext<13,5>(p_35_mid2_reg_25702.read());
}

void mnist_fp16_opt::thread_tmp_271_fu_5921_p2() {
    tmp_271_fu_5921_p2 = (!p_shl28_cast_fu_5901_p3.read().is_01() || !p_shl29_cast_fu_5913_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl28_cast_fu_5901_p3.read()) - sc_biguint<11>(p_shl29_cast_fu_5913_p3.read()));
}

void mnist_fp16_opt::thread_tmp_272_fu_10088_p1() {
    tmp_272_fu_10088_p1 = grp_fu_4358_p1.read();
}

void mnist_fp16_opt::thread_tmp_273_fu_5927_p2() {
    tmp_273_fu_5927_p2 = (!ap_const_lv11_4.is_01() || !tmp_271_reg_23578.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_4) + sc_biguint<11>(tmp_271_reg_23578.read()));
}

void mnist_fp16_opt::thread_tmp_273_mid2_cast_fu_18111_p1() {
    tmp_273_mid2_cast_fu_18111_p1 = esl_zext<10,3>(lhs_V_22_cast_mid2_fu_18099_p3.read());
}

void mnist_fp16_opt::thread_tmp_274_fu_10132_p1() {
    tmp_274_fu_10132_p1 = esl_zext<12,11>(p_Result_6_reg_24661.read());
}

void mnist_fp16_opt::thread_tmp_276_fu_5937_p2() {
    tmp_276_fu_5937_p2 = (!ap_const_lv11_5.is_01() || !tmp_271_reg_23578.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_5) + sc_biguint<11>(tmp_271_reg_23578.read()));
}

void mnist_fp16_opt::thread_tmp_277_fu_7886_p3() {
    tmp_277_fu_7886_p3 = esl_concat<3,5>(lhs_V_2_mid2_v_reg_24073.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_278_fu_10255_p2() {
    tmp_278_fu_10255_p2 = (!sh_amt_1_reg_24682.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_1_reg_24682.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_279_fu_7897_p3() {
    tmp_279_fu_7897_p3 = esl_concat<3,1>(lhs_V_2_mid2_v_reg_24073.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_27_cast_fu_6810_p1() {
    tmp_27_cast_fu_6810_p1 = esl_sext<64,11>(tmp_27_fu_6805_p2.read());
}

void mnist_fp16_opt::thread_tmp_27_fu_6805_p2() {
    tmp_27_fu_6805_p2 = (!tmp_19_reg_23186_pp3_iter2_reg.read().is_01() || !ap_const_lv11_4.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter2_reg.read()) + sc_biguint<11>(ap_const_lv11_4));
}

void mnist_fp16_opt::thread_tmp_27_mid2_cast_fu_7137_p1() {
    tmp_27_mid2_cast_fu_7137_p1 = esl_zext<7,2>(tmp_26_mid2_v_v_v_reg_23859.read());
}

void mnist_fp16_opt::thread_tmp_280_fu_15715_p2() {
    tmp_280_fu_15715_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_9_cast_fu_15692_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_9_cast_fu_15692_p1.read()));
}

void mnist_fp16_opt::thread_tmp_281_fu_7908_p2() {
    tmp_281_fu_7908_p2 = (!p_shl30_cast_fu_7893_p1.read().is_01() || !p_shl31_cast_fu_7904_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl30_cast_fu_7893_p1.read()) - sc_biguint<9>(p_shl31_cast_fu_7904_p1.read()));
}

void mnist_fp16_opt::thread_tmp_282_fu_7844_p2() {
    tmp_282_fu_7844_p2 = (exitcond13_mid_fu_7832_p2.read() | exitcond_flatten6_fu_7798_p2.read());
}

void mnist_fp16_opt::thread_tmp_283_fu_14592_p3() {
    tmp_283_fu_14592_p3 = esl_concat<1,8>(is_neg_2_reg_25830_pp13_iter28_reg.read(), p_Repl2_10_trunc_fu_14586_p2.read());
}

void mnist_fp16_opt::thread_tmp_284_fu_15098_p1() {
    tmp_284_fu_15098_p1 = esl_zext<12,11>(exp_tmp_V_2_reg_25992.read());
}

void mnist_fp16_opt::thread_tmp_285_fu_10260_p1() {
    tmp_285_fu_10260_p1 = esl_zext<54,32>(sh_amt_1_cast_fu_10252_p1.read());
}

void mnist_fp16_opt::thread_tmp_286_fu_15056_p2() {
    tmp_286_fu_15056_p2 = (!tmp_1292_fu_15030_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1292_fu_15030_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_287_fu_11556_p3() {
    tmp_287_fu_11556_p3 = (!tmp_1311_reg_25110.read()[0].is_01())? sc_lv<12>(): ((tmp_1311_reg_25110.read()[0].to_bool())? neg_ti26_fu_11550_p2.read(): tmp_1029_fu_11540_p1.read());
}

void mnist_fp16_opt::thread_tmp_289_fu_7942_p2() {
    tmp_289_fu_7942_p2 = (!tmp_676_cast_fu_7914_p1.read().is_01() || !lhs_V_1_mid2_cast_fu_7939_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_676_cast_fu_7914_p1.read()) + sc_biguint<10>(lhs_V_1_mid2_cast_fu_7939_p1.read()));
}

void mnist_fp16_opt::thread_tmp_28_cast_fu_5982_p1() {
    tmp_28_cast_fu_5982_p1 = esl_sext<64,11>(tmp_28_fu_5977_p2.read());
}

void mnist_fp16_opt::thread_tmp_28_fu_5977_p2() {
    tmp_28_fu_5977_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_5.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_5));
}

void mnist_fp16_opt::thread_tmp_293_fu_10264_p2() {
    tmp_293_fu_10264_p2 = (!man_V_6_reg_24677.read().is_01() || !tmp_285_fu_10260_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_6_reg_24677.read()) >> (unsigned short)tmp_285_fu_10260_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_294_1_cast_fu_17541_p1() {
    tmp_294_1_cast_fu_17541_p1 = esl_zext<7,3>(tmp_1116_reg_26689.read());
}

void mnist_fp16_opt::thread_tmp_294_3_cast_fu_17711_p1() {
    tmp_294_3_cast_fu_17711_p1 = esl_zext<7,3>(tmp_1139_reg_26727.read());
}

void mnist_fp16_opt::thread_tmp_294_5_cast_fu_17807_p1() {
    tmp_294_5_cast_fu_17807_p1 = esl_zext<7,3>(tmp_1173_reg_26733.read());
}

void mnist_fp16_opt::thread_tmp_294_7_cast_fu_17903_p1() {
    tmp_294_7_cast_fu_17903_p1 = esl_zext<7,3>(tmp_1210_reg_26715.read());
}

void mnist_fp16_opt::thread_tmp_294_cast_fu_17111_p1() {
    tmp_294_cast_fu_17111_p1 = esl_zext<8,7>(tmp_294_fu_17104_p3.read());
}

void mnist_fp16_opt::thread_tmp_294_fu_17104_p3() {
    tmp_294_fu_17104_p3 = esl_concat<4,3>(p_46_mid2_reg_26501.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_295_fu_16105_p3() {
    tmp_295_fu_16105_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_1275_reg_26266.read());
}

void mnist_fp16_opt::thread_tmp_296_cast_fu_6779_p1() {
    tmp_296_cast_fu_6779_p1 = esl_sext<64,11>(tmp_185_fu_6773_p2.read());
}

void mnist_fp16_opt::thread_tmp_296_fu_7948_p1() {
    tmp_296_fu_7948_p1 = tmp_289_fu_7942_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_299_cast_fu_6790_p1() {
    tmp_299_cast_fu_6790_p1 = esl_sext<64,11>(tmp_186_fu_6784_p2.read());
}

void mnist_fp16_opt::thread_tmp_299_fu_7960_p3() {
    tmp_299_fu_7960_p3 = esl_concat<10,1>(tmp_289_fu_7942_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_29_fu_5987_p2() {
    tmp_29_fu_5987_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_6.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_6));
}

void mnist_fp16_opt::thread_tmp_29_mid2_cast_fu_8708_p1() {
    tmp_29_mid2_cast_fu_8708_p1 = esl_zext<10,5>(tmp_29_mid2_reg_24283.read());
}

void mnist_fp16_opt::thread_tmp_29_mid2_fu_8649_p3() {
    tmp_29_mid2_fu_8649_p3 = (!exitcond15_mid_fu_8623_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond15_mid_fu_8623_p2.read()[0].to_bool())? yy1_V_fu_8629_p2.read(): p_10_mid_fu_8603_p3.read());
}

void mnist_fp16_opt::thread_tmp_2_fu_4591_p2() {
    tmp_2_fu_4591_p2 = (!ap_phi_mux_p_1_phi_fu_2171_p4.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_1_phi_fu_2171_p4.read() == ap_const_lv5_1C);
}

void mnist_fp16_opt::thread_tmp_300_fu_7972_p2() {
    tmp_300_fu_7972_p2 = (!p_shl32_cast_fu_7952_p3.read().is_01() || !p_shl33_cast_fu_7968_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl32_cast_fu_7952_p3.read()) - sc_bigint<13>(p_shl33_cast_fu_7968_p1.read()));
}

void mnist_fp16_opt::thread_tmp_301_fu_16135_p2() {
    tmp_301_fu_16135_p2 = (!F2_5_fu_16129_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_5_fu_16129_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_302_fu_16141_p2() {
    tmp_302_fu_16141_p2 = (!ap_const_lv12_FF2.is_01() || !F2_5_fu_16129_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_5_fu_16129_p2.read()));
}

void mnist_fp16_opt::thread_tmp_303_fu_16147_p2() {
    tmp_303_fu_16147_p2 = (!ap_const_lv12_E.is_01() || !F2_5_fu_16129_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_5_fu_16129_p2.read()));
}

void mnist_fp16_opt::thread_tmp_304_fu_16161_p2() {
    tmp_304_fu_16161_p2 = (!F2_5_fu_16129_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_5_fu_16129_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_305_fu_15772_p2() {
    tmp_305_fu_15772_p2 = (!p_Result_28_fu_15762_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_28_fu_15762_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16_opt::thread_tmp_306_fu_8023_p2() {
    tmp_306_fu_8023_p2 = (!lhs_V_1_mid2_reg_24103.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(lhs_V_1_mid2_reg_24103.read() == ap_const_lv5_1F);
}

void mnist_fp16_opt::thread_tmp_307_fu_11179_p2() {
    tmp_307_fu_11179_p2 = (rhs_V_3_fu_11170_p1.read() | ap_const_lv12_1);
}

void mnist_fp16_opt::thread_tmp_308_fu_15131_p2() {
    tmp_308_fu_15131_p2 = (!F2_6_fu_15125_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_6_fu_15125_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_309_fu_15137_p2() {
    tmp_309_fu_15137_p2 = (!ap_const_lv12_FF2.is_01() || !F2_6_fu_15125_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_6_fu_15125_p2.read()));
}

void mnist_fp16_opt::thread_tmp_30_fu_6047_p2() {
    tmp_30_fu_6047_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_7.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_7));
}

void mnist_fp16_opt::thread_tmp_30_mid2_cast_fu_7341_p1() {
    tmp_30_mid2_cast_fu_7341_p1 = esl_zext<10,5>(tmp_30_mid2_fu_7333_p3.read());
}

void mnist_fp16_opt::thread_tmp_30_mid2_fu_7333_p3() {
    tmp_30_mid2_fu_7333_p3 = (!exitcond17_mid_fu_7307_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond17_mid_fu_7307_p2.read()[0].to_bool())? args1_V_fu_7313_p2.read(): p_11_mid_fu_7245_p3.read());
}

void mnist_fp16_opt::thread_tmp_310_fu_15143_p2() {
    tmp_310_fu_15143_p2 = (!ap_const_lv12_E.is_01() || !F2_6_fu_15125_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_6_fu_15125_p2.read()));
}

void mnist_fp16_opt::thread_tmp_311_fu_15157_p2() {
    tmp_311_fu_15157_p2 = (!F2_6_fu_15125_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_6_fu_15125_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_312_fu_8028_p2() {
    tmp_312_fu_8028_p2 = (!lhs_V_1_mid2_reg_24103.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(lhs_V_1_mid2_reg_24103.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_312_mid2_cast_fu_18497_p1() {
    tmp_312_mid2_cast_fu_18497_p1 = esl_zext<9,5>(tmp_312_mid2_v_fu_18489_p3.read());
}

void mnist_fp16_opt::thread_tmp_312_mid2_v_fu_18489_p3() {
    tmp_312_mid2_v_fu_18489_p3 = (!exitcond_flatten39_fu_18475_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten39_fu_18475_p2.read()[0].to_bool())? args04_V_fu_18469_p2.read(): ap_phi_mux_p_68_phi_fu_3854_p4.read());
}

void mnist_fp16_opt::thread_tmp_314_fu_10672_p2() {
    tmp_314_fu_10672_p2 = (!tmp_609_cast_fu_10668_p1.read().is_01() || !r_V_7_reg_24773.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_609_cast_fu_10668_p1.read()) + sc_biguint<5>(r_V_7_reg_24773.read()));
}

void mnist_fp16_opt::thread_tmp_315_fu_15794_p3() {
    tmp_315_fu_15794_p3 = esl_concat<1,8>(is_neg_3_reg_26133.read(), p_Repl2_13_trunc_fu_15788_p2.read());
}

void mnist_fp16_opt::thread_tmp_316_cast_fu_15595_p1() {
    tmp_316_cast_fu_15595_p1 = esl_zext<12,4>(p_39_mid2_reg_25895.read());
}

void mnist_fp16_opt::thread_tmp_316_fu_8033_p2() {
    tmp_316_fu_8033_p2 = (tmp_312_fu_8028_p2.read() | tmp_306_fu_8023_p2.read());
}

void mnist_fp16_opt::thread_tmp_317_fu_8039_p2() {
    tmp_317_fu_8039_p2 = (!lhs_V_1_mid2_reg_24103.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(lhs_V_1_mid2_reg_24103.read() == ap_const_lv5_1D);
}

void mnist_fp16_opt::thread_tmp_318_fu_11618_p3() {
    tmp_318_fu_11618_p3 = (!tmp_1334_reg_25128.read()[0].is_01())? sc_lv<12>(): ((tmp_1334_reg_25128.read()[0].to_bool())? neg_ti29_fu_11612_p2.read(): tmp_1065_fu_11602_p1.read());
}

void mnist_fp16_opt::thread_tmp_320_fu_8044_p2() {
    tmp_320_fu_8044_p2 = (tmp_317_fu_8039_p2.read() | tmp_316_fu_8033_p2.read());
}

void mnist_fp16_opt::thread_tmp_321_fu_10735_p2() {
    tmp_321_fu_10735_p2 = (!relu2_0_V_load_reg_24817.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu2_0_V_load_reg_24817.read() == ap_const_lv16_0);
}

void mnist_fp16_opt::thread_tmp_322_fu_15306_p2() {
    tmp_322_fu_15306_p2 = (!sh_amt_6_reg_26041.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_6_reg_26041.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_323_fu_10929_p4() {
    tmp_323_fu_10929_p4 = p_03_i1_to_int_fu_10926_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_324_fu_8050_p2() {
    tmp_324_fu_8050_p2 = (!lhs_V_1_mid2_reg_24103.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(lhs_V_1_mid2_reg_24103.read() == ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_325_cast_fu_7291_p1() {
    tmp_325_cast_fu_7291_p1 = esl_sext<10,9>(tmp_191_fu_7285_p2.read());
}

void mnist_fp16_opt::thread_tmp_325_fu_8055_p2() {
    tmp_325_fu_8055_p2 = (tmp_324_fu_8050_p2.read() | tmp_320_fu_8044_p2.read());
}

void mnist_fp16_opt::thread_tmp_325_mid2_cast_fu_19754_p1() {
    tmp_325_mid2_cast_fu_19754_p1 = esl_zext<9,5>(tmp_325_mid2_v_fu_19746_p3.read());
}

void mnist_fp16_opt::thread_tmp_325_mid2_v_fu_19746_p3() {
    tmp_325_mid2_v_fu_19746_p3 = (!exitcond_flatten44_fu_19732_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten44_fu_19732_p2.read()[0].to_bool())? ff5_V_fu_19726_p2.read(): p_56_reg_3975.read());
}

void mnist_fp16_opt::thread_tmp_327_fu_10947_p4() {
    tmp_327_fu_10947_p4 = tmp_608_to_int_fu_10943_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_329_cast_fu_18426_p1() {
    tmp_329_cast_fu_18426_p1 = esl_zext<11,3>(p_53_mid2_reg_26825.read());
}

void mnist_fp16_opt::thread_tmp_329_fu_4825_p2() {
    tmp_329_fu_4825_p2 = (grp_fu_4400_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_330_fu_4830_p2() {
    tmp_330_fu_4830_p2 = (tmp_11_reg_23152.read() | tmp_329_fu_4825_p2.read());
}

void mnist_fp16_opt::thread_tmp_330_mid2_cast1_fu_18289_p1() {
    tmp_330_mid2_cast1_fu_18289_p1 = esl_zext<8,4>(tmp_330_mid2_v_reg_26862.read());
}

void mnist_fp16_opt::thread_tmp_330_mid2_cast_fu_18196_p1() {
    tmp_330_mid2_cast_fu_18196_p1 = esl_zext<9,4>(tmp_330_mid2_v_fu_18188_p3.read());
}

void mnist_fp16_opt::thread_tmp_330_mid2_v_fu_18188_p3() {
    tmp_330_mid2_v_fu_18188_p3 = (!exitcond_flatten40_fu_18174_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten40_fu_18174_p2.read()[0].to_bool())? rc3_V_fu_18168_p2.read(): ap_phi_mux_p_58_phi_fu_3786_p4.read());
}

void mnist_fp16_opt::thread_tmp_331_fu_4835_p2() {
    tmp_331_fu_4835_p2 = (tmp_25_reg_23169.read() | tmp_330_fu_4830_p2.read());
}

void mnist_fp16_opt::thread_tmp_334_fu_15101_p3() {
    tmp_334_fu_15101_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_1294_reg_25997.read());
}

void mnist_fp16_opt::thread_tmp_335_fu_8061_p2() {
    tmp_335_fu_8061_p2 = (!p_12_mid2_reg_24091.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_12_mid2_reg_24091.read() == ap_const_lv5_1F);
}

void mnist_fp16_opt::thread_tmp_337_fu_8066_p2() {
    tmp_337_fu_8066_p2 = (!p_12_mid2_reg_24091.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_12_mid2_reg_24091.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_338_fu_17246_p3() {
    tmp_338_fu_17246_p3 = (!tmp_1377_reg_26560.read()[0].is_01())? sc_lv<10>(): ((tmp_1377_reg_26560.read()[0].to_bool())? neg_ti34_fu_17240_p2.read(): tmp_1132_fu_17230_p1.read());
}

void mnist_fp16_opt::thread_tmp_340_fu_15311_p1() {
    tmp_340_fu_15311_p1 = esl_zext<54,32>(sh_amt_6_cast_fu_15303_p1.read());
}

void mnist_fp16_opt::thread_tmp_341_fu_15315_p2() {
    tmp_341_fu_15315_p2 = (!man_V_12_reg_26030.read().is_01() || !tmp_340_fu_15311_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_12_reg_26030.read()) >> (unsigned short)tmp_340_fu_15311_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_342_fu_8071_p2() {
    tmp_342_fu_8071_p2 = (tmp_337_fu_8066_p2.read() | tmp_335_fu_8061_p2.read());
}

void mnist_fp16_opt::thread_tmp_343_fu_15331_p1() {
    tmp_343_fu_15331_p1 = esl_sext<32,16>(tmp_1295_reg_26053.read());
}

void mnist_fp16_opt::thread_tmp_344_fu_15334_p2() {
    tmp_344_fu_15334_p2 = (!sh_amt_6_cast_fu_15303_p1.read().is_01())? sc_lv<32>(): tmp_343_fu_15331_p1.read() << (unsigned short)sh_amt_6_cast_fu_15303_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_345_fu_8077_p2() {
    tmp_345_fu_8077_p2 = (!p_12_mid2_reg_24091.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_12_mid2_reg_24091.read() == ap_const_lv5_1D);
}

void mnist_fp16_opt::thread_tmp_346_fu_8082_p2() {
    tmp_346_fu_8082_p2 = (tmp_345_fu_8077_p2.read() | tmp_342_fu_8071_p2.read());
}

void mnist_fp16_opt::thread_tmp_347_cast_fu_15012_p1() {
    tmp_347_cast_fu_15012_p1 = esl_zext<11,2>(p_51_mid2_reg_25930.read());
}

void mnist_fp16_opt::thread_tmp_347_fu_8088_p2() {
    tmp_347_fu_8088_p2 = (!p_12_mid2_reg_24091.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_12_mid2_reg_24091.read() == ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_349_fu_15183_p1() {
    tmp_349_fu_15183_p1 = esl_zext<12,11>(exp_tmp_V_3_reg_26014.read());
}

void mnist_fp16_opt::thread_tmp_34_cast_fu_7157_p1() {
    tmp_34_cast_fu_7157_p1 = esl_zext<11,5>(r_V_1_reg_23871.read());
}

void mnist_fp16_opt::thread_tmp_34_fu_6057_p2() {
    tmp_34_fu_6057_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_8.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_8));
}

void mnist_fp16_opt::thread_tmp_350_fu_10973_p2() {
    tmp_350_fu_10973_p2 = (notrhs2_fu_10967_p2.read() | notlhs2_fu_10961_p2.read());
}

void mnist_fp16_opt::thread_tmp_351_fu_15092_p2() {
    tmp_351_fu_15092_p2 = (!tmp_1299_fu_15066_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1299_fu_15066_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_352_fu_8093_p2() {
    tmp_352_fu_8093_p2 = (tmp_347_fu_8088_p2.read() | tmp_346_fu_8082_p2.read());
}

void mnist_fp16_opt::thread_tmp_353_fu_10991_p2() {
    tmp_353_fu_10991_p2 = (notrhs3_fu_10985_p2.read() | notlhs3_fu_10979_p2.read());
}

void mnist_fp16_opt::thread_tmp_354_fu_15216_p2() {
    tmp_354_fu_15216_p2 = (!F2_7_fu_15210_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_7_fu_15210_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_355_fu_15222_p2() {
    tmp_355_fu_15222_p2 = (!ap_const_lv12_FF2.is_01() || !F2_7_fu_15210_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_7_fu_15210_p2.read()));
}

void mnist_fp16_opt::thread_tmp_356_fu_15228_p2() {
    tmp_356_fu_15228_p2 = (!ap_const_lv12_E.is_01() || !F2_7_fu_15210_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_7_fu_15210_p2.read()));
}

void mnist_fp16_opt::thread_tmp_357_fu_15242_p2() {
    tmp_357_fu_15242_p2 = (!F2_7_fu_15210_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_7_fu_15210_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_359_fu_4844_p1() {
    tmp_359_fu_4844_p1 = mul3_fu_22716_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_359_mid2_cast_fu_19842_p1() {
    tmp_359_mid2_cast_fu_19842_p1 = esl_zext<10,3>(lhs_V_26_cast_mid2_fu_19830_p3.read());
}

void mnist_fp16_opt::thread_tmp_35_cast_fu_7171_p1() {
    tmp_35_cast_fu_7171_p1 = esl_zext<7,2>(p_13_mid2_reg_23853.read());
}

void mnist_fp16_opt::thread_tmp_35_fu_6117_p2() {
    tmp_35_fu_6117_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_9.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_9));
}

void mnist_fp16_opt::thread_tmp_360_fu_10997_p2() {
    tmp_360_fu_10997_p2 = (tmp_350_fu_10973_p2.read() & tmp_353_fu_10991_p2.read());
}

void mnist_fp16_opt::thread_tmp_362_cast1_fu_18960_p1() {
    tmp_362_cast1_fu_18960_p1 = esl_zext<8,4>(ap_phi_mux_p_57_phi_fu_3931_p4.read());
}

void mnist_fp16_opt::thread_tmp_362_cast1_mid1_fu_19116_p1() {
    tmp_362_cast1_mid1_fu_19116_p1 = esl_zext<8,4>(index_tuple5_V_fu_19088_p2.read());
}

void mnist_fp16_opt::thread_tmp_362_fu_11003_p2() {
    tmp_362_fu_11003_p2 = (tmp_360_fu_10997_p2.read() & grp_fu_4367_p2.read());
}

void mnist_fp16_opt::thread_tmp_363_cast_fu_18972_p1() {
    tmp_363_cast_fu_18972_p1 = esl_zext<8,7>(tmp_363_fu_18964_p3.read());
}

void mnist_fp16_opt::thread_tmp_363_cast_mid1_fu_19128_p1() {
    tmp_363_cast_mid1_fu_19128_p1 = esl_zext<8,7>(tmp_363_mid1_fu_19120_p3.read());
}

void mnist_fp16_opt::thread_tmp_363_fu_18964_p3() {
    tmp_363_fu_18964_p3 = esl_concat<4,3>(ap_phi_mux_p_57_phi_fu_3931_p4.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_363_mid1_fu_19120_p3() {
    tmp_363_mid1_fu_19120_p3 = esl_concat<4,3>(index_tuple5_V_fu_19088_p2.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_364_cast_fu_7408_p1() {
    tmp_364_cast_fu_7408_p1 = esl_zext<64,13>(tmp_204_fu_7402_p2.read());
}

void mnist_fp16_opt::thread_tmp_364_mid2_cast_fu_18329_p1() {
    tmp_364_mid2_cast_fu_18329_p1 = esl_zext<8,4>(tmp_1398_reg_26882.read());
}

void mnist_fp16_opt::thread_tmp_365_fu_4876_p4() {
    tmp_365_fu_4876_p4 = neg_mul3_fu_4871_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_365_mid2_cast_fu_18338_p1() {
    tmp_365_mid2_cast_fu_18338_p1 = esl_zext<64,2>(tmp_365_mid2_reg_26887.read());
}

void mnist_fp16_opt::thread_tmp_365_mid2_fu_18267_p3() {
    tmp_365_mid2_fu_18267_p3 = (!exitcond67_mid_fu_18217_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond67_mid_fu_18217_p2.read()[0].to_bool())? ry4_V_fu_18223_p2.read(): p_63_mid_fu_18180_p3.read());
}

void mnist_fp16_opt::thread_tmp_367_fu_4886_p1() {
    tmp_367_fu_4886_p1 = esl_sext<12,8>(tmp_365_fu_4876_p4.read());
}

void mnist_fp16_opt::thread_tmp_368_fu_19264_p3() {
    tmp_368_fu_19264_p3 = (!tmp_1189_fu_19258_p2.read()[0].is_01())? sc_lv<3>(): ((tmp_1189_fu_19258_p2.read()[0].to_bool())? r_V_20_fu_19252_p2.read(): ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_369_cast_fu_19480_p1() {
    tmp_369_cast_fu_19480_p1 = esl_zext<11,3>(tmp_368_reg_27124_pp20_iter17_reg.read());
}

void mnist_fp16_opt::thread_tmp_370_fu_13490_p4() {
    tmp_370_fu_13490_p4 = conv3_0_load_to_int_fu_13487_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_371_fu_17325_p3() {
    tmp_371_fu_17325_p3 = (!tmp_1402_reg_26598.read()[0].is_01())? sc_lv<10>(): ((tmp_1402_reg_26598.read()[0].to_bool())? neg_ti36_fu_17319_p2.read(): tmp_1169_fu_17309_p1.read());
}

void mnist_fp16_opt::thread_tmp_372_fu_4890_p1() {
    tmp_372_fu_4890_p1 = esl_sext<12,9>(tmp_369_reg_23272.read());
}

void mnist_fp16_opt::thread_tmp_374_fu_4893_p3() {
    tmp_374_fu_4893_p3 = (!tmp_364_reg_23266.read()[0].is_01())? sc_lv<12>(): ((tmp_364_reg_23266.read()[0].to_bool())? tmp_367_fu_4886_p1.read(): tmp_372_fu_4890_p1.read());
}

void mnist_fp16_opt::thread_tmp_375_cast_fu_19446_p1() {
    tmp_375_cast_fu_19446_p1 = esl_zext<8,4>(r_V_21_reg_27172_pp20_iter16_reg.read());
}

void mnist_fp16_opt::thread_tmp_375_fu_5947_p1() {
    tmp_375_fu_5947_p1 = grp_fu_4913_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_376_cast_fu_7198_p1() {
    tmp_376_cast_fu_7198_p1 = esl_zext<64,13>(tmp_225_fu_7193_p2.read());
}

void mnist_fp16_opt::thread_tmp_376_fu_5959_p1() {
    tmp_376_fu_5959_p1 = grp_fu_4913_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_378_fu_19510_p2() {
    tmp_378_fu_19510_p2 = (!relu5_0_V_q0.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu5_0_V_q0.read() == ap_const_lv16_0);
}

void mnist_fp16_opt::thread_tmp_379_fu_5971_p2() {
    tmp_379_fu_5971_p2 = (!p_shl34_cast_fu_5951_p3.read().is_01() || !p_shl35_cast_fu_5963_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl34_cast_fu_5951_p3.read()) - sc_biguint<11>(p_shl35_cast_fu_5963_p3.read()));
}

void mnist_fp16_opt::thread_tmp_37_fu_8115_p3() {
    tmp_37_fu_8115_p3 = (!tmp_406_fu_8104_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_406_fu_8104_p2.read()[0].to_bool())? r_V_2_fu_8099_p2.read(): tmp_407_fu_8110_p2.read());
}

void mnist_fp16_opt::thread_tmp_380_fu_5997_p2() {
    tmp_380_fu_5997_p2 = (!ap_const_lv11_6.is_01() || !tmp_379_reg_23594.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_6) + sc_biguint<11>(tmp_379_reg_23594.read()));
}

void mnist_fp16_opt::thread_tmp_381_fu_15455_p2() {
    tmp_381_fu_15455_p2 = (!sh_amt_7_reg_26069.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_7_reg_26069.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_382_fu_13516_p2() {
    tmp_382_fu_13516_p2 = (notrhs4_reg_25601.read() | notlhs4_reg_25596.read());
}

void mnist_fp16_opt::thread_tmp_383_fu_6007_p2() {
    tmp_383_fu_6007_p2 = (!ap_const_lv11_7.is_01() || !tmp_379_reg_23594.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_7) + sc_biguint<11>(tmp_379_reg_23594.read()));
}

void mnist_fp16_opt::thread_tmp_384_fu_8670_p3() {
    tmp_384_fu_8670_p3 = esl_concat<3,2>(tmp_21_mid2_v_fu_8663_p3.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_385_fu_8686_p3() {
    tmp_385_fu_8686_p3 = esl_concat<3,5>(tmp_21_mid2_v_fu_8663_p3.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_385_mid2_cast_fu_21000_p1() {
    tmp_385_mid2_cast_fu_21000_p1 = esl_zext<9,5>(tmp_385_mid2_v_fu_20992_p3.read());
}

void mnist_fp16_opt::thread_tmp_385_mid2_v_fu_20992_p3() {
    tmp_385_mid2_v_fu_20992_p3 = (!exitcond_flatten47_fu_20978_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten47_fu_20978_p2.read()[0].to_bool())? args05_V_fu_20972_p2.read(): ap_phi_mux_p_79_phi_fu_4102_p4.read());
}

void mnist_fp16_opt::thread_tmp_386_fu_15186_p3() {
    tmp_386_fu_15186_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_1301_reg_26019.read());
}

void mnist_fp16_opt::thread_tmp_388_fu_18715_p1() {
    tmp_388_fu_18715_p1 = esl_zext<12,11>(p_Result_37_reg_27026.read());
}

void mnist_fp16_opt::thread_tmp_389_fu_18718_p3() {
    tmp_389_fu_18718_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_1391_reg_27031.read());
}

void mnist_fp16_opt::thread_tmp_38_cast_fu_8343_p1() {
    tmp_38_cast_fu_8343_p1 = esl_zext<13,5>(tmp_37_reg_24136_pp6_iter20_reg.read());
}

void mnist_fp16_opt::thread_tmp_38_fu_6127_p2() {
    tmp_38_fu_6127_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_A.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_A));
}

void mnist_fp16_opt::thread_tmp_390_fu_18709_p2() {
    tmp_390_fu_18709_p2 = (!tmp_1389_fu_18683_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1389_fu_18683_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_391_cast_fu_18402_p1() {
    tmp_391_cast_fu_18402_p1 = esl_zext<11,4>(r_V_19_fu_18397_p2.read());
}

void mnist_fp16_opt::thread_tmp_391_fu_8698_p2() {
    tmp_391_fu_8698_p2 = (!p_shl36_cast_fu_8694_p1.read().is_01() || !tmp_691_cast_fu_8682_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl36_cast_fu_8694_p1.read()) - sc_biguint<9>(tmp_691_cast_fu_8682_p1.read()));
}

void mnist_fp16_opt::thread_tmp_392_cast_fu_18369_p1() {
    tmp_392_cast_fu_18369_p1 = esl_zext<12,2>(p_66_mid2_reg_26875.read());
}

void mnist_fp16_opt::thread_tmp_392_fu_8635_p2() {
    tmp_392_fu_8635_p2 = (exitcond15_mid_fu_8623_p2.read() | exitcond_flatten8_fu_8597_p2.read());
}

void mnist_fp16_opt::thread_tmp_394_fu_16773_p2() {
    tmp_394_fu_16773_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_10_cast_fu_16750_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_10_cast_fu_16750_p1.read()));
}

void mnist_fp16_opt::thread_tmp_395_fu_8711_p2() {
    tmp_395_fu_8711_p2 = (!tmp_694_cast_fu_8704_p1.read().is_01() || !tmp_29_mid2_cast_fu_8708_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_694_cast_fu_8704_p1.read()) + sc_biguint<10>(tmp_29_mid2_cast_fu_8708_p1.read()));
}

void mnist_fp16_opt::thread_tmp_396_fu_15460_p1() {
    tmp_396_fu_15460_p1 = esl_zext<54,32>(sh_amt_7_cast_fu_15452_p1.read());
}

void mnist_fp16_opt::thread_tmp_397_fu_15464_p2() {
    tmp_397_fu_15464_p2 = (!man_V_15_reg_26064.read().is_01() || !tmp_396_fu_15460_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_15_reg_26064.read()) >> (unsigned short)tmp_396_fu_15460_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_398_fu_8717_p1() {
    tmp_398_fu_8717_p1 = tmp_395_fu_8711_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_399_fu_15480_p1() {
    tmp_399_fu_15480_p1 = esl_sext<32,16>(tmp_1302_reg_26080.read());
}

void mnist_fp16_opt::thread_tmp_3_fu_4739_p2() {
    tmp_3_fu_4739_p2 = (rhs_V_1_fu_4736_p1.read() | ap_const_lv12_1);
}

void mnist_fp16_opt::thread_tmp_400_fu_15483_p2() {
    tmp_400_fu_15483_p2 = (!sh_amt_7_cast_fu_15452_p1.read().is_01())? sc_lv<32>(): tmp_399_fu_15480_p1.read() << (unsigned short)sh_amt_7_cast_fu_15452_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_401_fu_8729_p3() {
    tmp_401_fu_8729_p3 = esl_concat<10,2>(tmp_395_fu_8711_p2.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_402_fu_17367_p3() {
    tmp_402_fu_17367_p3 = (!tmp_1425_reg_26606.read()[0].is_01())? sc_lv<10>(): ((tmp_1425_reg_26606.read()[0].to_bool())? neg_ti40_fu_17361_p2.read(): tmp_1206_fu_17351_p1.read());
}

void mnist_fp16_opt::thread_tmp_405_fu_8741_p2() {
    tmp_405_fu_8741_p2 = (!p_shl38_cast_fu_8721_p3.read().is_01() || !p_shl39_cast_fu_8737_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl38_cast_fu_8721_p3.read()) - sc_bigint<13>(p_shl39_cast_fu_8737_p1.read()));
}

void mnist_fp16_opt::thread_tmp_406_fu_8104_p2() {
    tmp_406_fu_8104_p2 = (!r_V_2_fu_8099_p2.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): (sc_biguint<5>(r_V_2_fu_8099_p2.read()) < sc_biguint<5>(ap_const_lv5_1C));
}

void mnist_fp16_opt::thread_tmp_407_fu_8110_p2() {
    tmp_407_fu_8110_p2 = (!ap_const_lv5_3.is_01() || !p_12_mid2_reg_24091.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_3) + sc_biguint<5>(p_12_mid2_reg_24091.read()));
}

void mnist_fp16_opt::thread_tmp_408_fu_19516_p2() {
    tmp_408_fu_19516_p2 = (!ap_const_lv16_0.is_01() || !relu5_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu5_0_V_q0.read()));
}

void mnist_fp16_opt::thread_tmp_409_fu_13520_p2() {
    tmp_409_fu_13520_p2 = (tmp_382_fu_13516_p2.read() & tmp_403_reg_25606.read());
}

void mnist_fp16_opt::thread_tmp_40_cast_fu_5992_p1() {
    tmp_40_cast_fu_5992_p1 = esl_sext<64,11>(tmp_29_fu_5987_p2.read());
}

void mnist_fp16_opt::thread_tmp_40_fu_6187_p2() {
    tmp_40_fu_6187_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_B.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_B));
}

void mnist_fp16_opt::thread_tmp_411_fu_16852_p3() {
    tmp_411_fu_16852_p3 = esl_concat<1,8>(tmp_1353_reg_26418_pp16_iter10_reg.read(), p_Repl2_16_trunc_fu_16846_p2.read());
}

void mnist_fp16_opt::thread_tmp_414_fu_16830_p2() {
    tmp_414_fu_16830_p2 = (!p_Result_35_fu_16820_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_35_fu_16820_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16_opt::thread_tmp_415_fu_8167_p1() {
    tmp_415_fu_8167_p1 = mul4_fu_22820_p2.read().range(27-1, 0);
}

void mnist_fp16_opt::thread_tmp_416_fu_13525_p1() {
    tmp_416_fu_13525_p1 = grp_fu_4358_p1.read();
}

void mnist_fp16_opt::thread_tmp_417_cast_fu_21469_p1() {
    tmp_417_cast_fu_21469_p1 = esl_zext<9,5>(p_80_reg_4142.read());
}

void mnist_fp16_opt::thread_tmp_417_fu_21465_p1() {
    tmp_417_fu_21465_p1 = esl_zext<64,5>(p_80_reg_4142.read());
}

void mnist_fp16_opt::thread_tmp_418_mid2_cast_fu_21072_p1() {
    tmp_418_mid2_cast_fu_21072_p1 = esl_zext<10,3>(tmp_418_mid2_fu_21064_p3.read());
}

void mnist_fp16_opt::thread_tmp_418_mid2_fu_21064_p3() {
    tmp_418_mid2_fu_21064_p3 = (!exitcond83_mid_fu_21038_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond83_mid_fu_21038_p2.read()[0].to_bool())? args15_V_fu_21044_p2.read(): p_81_mid_fu_20984_p3.read());
}

void mnist_fp16_opt::thread_tmp_419_fu_20812_p2() {
    tmp_419_fu_20812_p2 = (!p_Val2_54_reg_4064.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(p_Val2_54_reg_4064.read() == ap_const_lv16_0);
}

void mnist_fp16_opt::thread_tmp_41_fu_6197_p2() {
    tmp_41_fu_6197_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_C.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_C));
}

void mnist_fp16_opt::thread_tmp_420_fu_8204_p4() {
    tmp_420_fu_8204_p4 = neg_mul4_fu_8199_p2.read().range(26, 18);
}

void mnist_fp16_opt::thread_tmp_420_mid2_cast1_fu_20026_p1() {
    tmp_420_mid2_cast1_fu_20026_p1 = esl_zext<9,5>(tmp_420_mid2_v_reg_27310.read());
}

void mnist_fp16_opt::thread_tmp_420_mid2_cast_fu_19927_p1() {
    tmp_420_mid2_cast_fu_19927_p1 = esl_zext<10,5>(tmp_420_mid2_v_fu_19919_p3.read());
}

void mnist_fp16_opt::thread_tmp_420_mid2_v_fu_19919_p3() {
    tmp_420_mid2_v_fu_19919_p3 = (!exitcond_flatten48_fu_19905_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten48_fu_19905_p2.read()[0].to_bool())? rc4_V_fu_19899_p2.read(): ap_phi_mux_p_67_phi_fu_4035_p4.read());
}

void mnist_fp16_opt::thread_tmp_421_fu_8214_p1() {
    tmp_421_fu_8214_p1 = esl_sext<13,9>(tmp_420_fu_8204_p4.read());
}

void mnist_fp16_opt::thread_tmp_423_cast_fu_19272_p1() {
    tmp_423_cast_fu_19272_p1 = esl_zext<4,3>(tmp_368_fu_19264_p3.read());
}

void mnist_fp16_opt::thread_tmp_423_fu_13602_p2() {
    tmp_423_fu_13602_p2 = (!F2_4_fu_13596_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_4_fu_13596_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_426_cast_fu_7166_p1() {
    tmp_426_cast_fu_7166_p1 = esl_sext<64,11>(tmp_239_fu_7160_p2.read());
}

void mnist_fp16_opt::thread_tmp_426_fu_8218_p1() {
    tmp_426_fu_8218_p1 = esl_sext<13,10>(tmp_422_reg_24164.read());
}

void mnist_fp16_opt::thread_tmp_427_fu_19608_p2() {
    tmp_427_fu_19608_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_12_cast_fu_19585_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_12_cast_fu_19585_p1.read()));
}

void mnist_fp16_opt::thread_tmp_428_fu_8221_p3() {
    tmp_428_fu_8221_p3 = (!tmp_418_reg_24156.read()[0].is_01())? sc_lv<13>(): ((tmp_418_reg_24156.read()[0].to_bool())? tmp_421_fu_8214_p1.read(): tmp_426_fu_8218_p1.read());
}

void mnist_fp16_opt::thread_tmp_432_fu_21931_p3() {
    tmp_432_fu_21931_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_1484_reg_27859.read());
}

void mnist_fp16_opt::thread_tmp_433_fu_21893_p1() {
    tmp_433_fu_21893_p1 = esl_zext<64,5>(p_82_reg_4199.read());
}

void mnist_fp16_opt::thread_tmp_434_cast_fu_7180_p1() {
    tmp_434_cast_fu_7180_p1 = esl_zext<64,7>(tmp_241_fu_7174_p2.read());
}

void mnist_fp16_opt::thread_tmp_434_fu_8297_p1() {
    tmp_434_fu_8297_p1 = grp_fu_8241_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_435_fu_21928_p1() {
    tmp_435_fu_21928_p1 = esl_zext<12,11>(p_Result_53_reg_27854.read());
}

void mnist_fp16_opt::thread_tmp_436_fu_13608_p2() {
    tmp_436_fu_13608_p2 = (!ap_const_lv12_FF2.is_01() || !F2_4_fu_13596_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_4_fu_13596_p2.read()));
}

void mnist_fp16_opt::thread_tmp_437_fu_21955_p2() {
    tmp_437_fu_21955_p2 = (!tmp_1482_reg_27843.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1482_reg_27843.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_438_fu_8187_p1() {
    tmp_438_fu_8187_p1 = mul5_fu_22828_p2.read().range(27-1, 0);
}

void mnist_fp16_opt::thread_tmp_438_mid2_cast_fu_21535_p1() {
    tmp_438_mid2_cast_fu_21535_p1 = esl_zext<10,3>(tmp_438_mid2_v_fu_21527_p3.read());
}

void mnist_fp16_opt::thread_tmp_438_mid2_v_fu_21527_p3() {
    tmp_438_mid2_v_fu_21527_p3 = (!exitcond32_fu_21513_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond32_fu_21513_p2.read()[0].to_bool())? ra67_V_fu_21507_p2.read(): ap_phi_mux_p_83_phi_fu_4168_p4.read());
}

void mnist_fp16_opt::thread_tmp_43_cast_fu_7399_p1() {
    tmp_43_cast_fu_7399_p1 = esl_zext<13,5>(p_16_mid2_reg_23925.read());
}

void mnist_fp16_opt::thread_tmp_43_fu_6257_p2() {
    tmp_43_fu_6257_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_D.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_D));
}

void mnist_fp16_opt::thread_tmp_440_fu_13614_p2() {
    tmp_440_fu_13614_p2 = (!ap_const_lv12_E.is_01() || !F2_4_fu_13596_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_4_fu_13596_p2.read()));
}

void mnist_fp16_opt::thread_tmp_441_fu_20721_p2() {
    tmp_441_fu_20721_p2 = (!ap_const_lv16_0.is_01() || !p_Val2_54_reg_4064.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(p_Val2_54_reg_4064.read()));
}

void mnist_fp16_opt::thread_tmp_442_fu_8252_p4() {
    tmp_442_fu_8252_p4 = neg_mul5_fu_8247_p2.read().range(26, 23);
}

void mnist_fp16_opt::thread_tmp_442_mid2_cast_fu_20066_p1() {
    tmp_442_mid2_cast_fu_20066_p1 = esl_zext<9,4>(tmp_1465_reg_27329.read());
}

void mnist_fp16_opt::thread_tmp_443_fu_8262_p1() {
    tmp_443_fu_8262_p1 = esl_sext<14,4>(tmp_442_fu_8252_p4.read());
}

void mnist_fp16_opt::thread_tmp_443_mid2_cast_fu_20075_p1() {
    tmp_443_mid2_cast_fu_20075_p1 = esl_zext<64,2>(tmp_443_mid2_reg_27334.read());
}

void mnist_fp16_opt::thread_tmp_443_mid2_fu_19998_p3() {
    tmp_443_mid2_fu_19998_p3 = (!exitcond73_mid_fu_19948_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond73_mid_fu_19948_p2.read()[0].to_bool())? ry5_V_fu_19954_p2.read(): p_69_mid_fu_19911_p3.read());
}

void mnist_fp16_opt::thread_tmp_444_fu_19665_p2() {
    tmp_444_fu_19665_p2 = (!p_Result_42_fu_19655_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_42_fu_19655_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16_opt::thread_tmp_445_fu_13628_p2() {
    tmp_445_fu_13628_p2 = (!F2_4_fu_13596_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_4_fu_13596_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_447_fu_8266_p1() {
    tmp_447_fu_8266_p1 = esl_sext<14,5>(tmp_446_reg_24174.read());
}

void mnist_fp16_opt::thread_tmp_448_fu_8276_p1() {
    tmp_448_fu_8276_p1 = p_v_fu_8269_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_449_fu_8286_p1() {
    tmp_449_fu_8286_p1 = p_v_fu_8269_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_44_cast_fu_6052_p1() {
    tmp_44_cast_fu_6052_p1 = esl_sext<64,11>(tmp_30_fu_6047_p2.read());
}

void mnist_fp16_opt::thread_tmp_44_fu_6267_p2() {
    tmp_44_fu_6267_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_E.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_E));
}

void mnist_fp16_opt::thread_tmp_450_fu_13692_p2() {
    tmp_450_fu_13692_p2 = (!sh_amt_4_reg_25638.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_4_reg_25638.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_451_fu_8301_p3() {
    tmp_451_fu_8301_p3 = esl_concat<2,5>(r_V_4_reg_24184_pp6_iter19_reg.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_452_fu_13697_p1() {
    tmp_452_fu_13697_p1 = esl_zext<54,32>(sh_amt_4_cast_fu_13689_p1.read());
}

void mnist_fp16_opt::thread_tmp_453_fu_21966_p2() {
    tmp_453_fu_21966_p2 = (!F2_9_fu_21960_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_9_fu_21960_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_454_fu_21972_p2() {
    tmp_454_fu_21972_p2 = (!ap_const_lv12_FF2.is_01() || !F2_9_fu_21960_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_9_fu_21960_p2.read()));
}

void mnist_fp16_opt::thread_tmp_455_fu_21978_p2() {
    tmp_455_fu_21978_p2 = (!ap_const_lv12_E.is_01() || !F2_9_fu_21960_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_9_fu_21960_p2.read()));
}

void mnist_fp16_opt::thread_tmp_456_fu_21992_p2() {
    tmp_456_fu_21992_p2 = (!F2_9_fu_21960_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_9_fu_21960_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_457_to_int_fu_21808_p1() {
    tmp_457_to_int_fu_21808_p1 = tmp_457_reg_4175.read();
}

void mnist_fp16_opt::thread_tmp_458_fu_19687_p3() {
    tmp_458_fu_19687_p3 = esl_concat<1,8>(is_neg_4_reg_27208_pp20_iter27_reg.read(), p_Repl2_19_trunc_fu_19681_p2.read());
}

void mnist_fp16_opt::thread_tmp_45_fu_7416_p4() {
    tmp_45_fu_7416_p4 = conv1_0_load_to_int_fu_7413_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_461_cast_fu_19308_p1() {
    tmp_461_cast_fu_19308_p1 = esl_zext<12,4>(p_62_mid2_reg_27092.read());
}

void mnist_fp16_opt::thread_tmp_461_fu_8312_p3() {
    tmp_461_fu_8312_p3 = esl_concat<2,2>(r_V_4_reg_24184_pp6_iter19_reg.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_462_fu_22463_p1() {
    tmp_462_fu_22463_p1 = esl_zext<64,4>(p_71_reg_4257.read());
}

void mnist_fp16_opt::thread_tmp_463_fu_13701_p2() {
    tmp_463_fu_13701_p2 = (!man_V_7_reg_25633.read().is_01() || !tmp_452_fu_13697_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_7_reg_25633.read()) >> (unsigned short)tmp_452_fu_13697_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_464_cast_fu_22185_p1() {
    tmp_464_cast_fu_22185_p1 = esl_zext<9,4>(p_85_reg_4210.read());
}

void mnist_fp16_opt::thread_tmp_464_fu_22181_p1() {
    tmp_464_fu_22181_p1 = esl_zext<64,4>(p_85_reg_4210.read());
}

void mnist_fp16_opt::thread_tmp_465_fu_8323_p2() {
    tmp_465_fu_8323_p2 = (!p_shl40_cast_fu_8308_p1.read().is_01() || !p_shl41_cast_fu_8319_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl40_cast_fu_8308_p1.read()) - sc_biguint<8>(p_shl41_cast_fu_8319_p1.read()));
}

void mnist_fp16_opt::thread_tmp_467_fu_21600_p2() {
    tmp_467_fu_21600_p2 = (!relu6_0_V_load_reg_27754.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu6_0_V_load_reg_27754.read() == ap_const_lv16_0);
}

void mnist_fp16_opt::thread_tmp_468_fu_21221_p3() {
    tmp_468_fu_21221_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_1449_reg_27661.read());
}

void mnist_fp16_opt::thread_tmp_469_fu_21218_p1() {
    tmp_469_fu_21218_p1 = esl_zext<12,11>(p_Result_44_reg_27656.read());
}

void mnist_fp16_opt::thread_tmp_46_fu_6327_p2() {
    tmp_46_fu_6327_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_F.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_F));
}

void mnist_fp16_opt::thread_tmp_470_fu_8333_p2() {
    tmp_470_fu_8333_p2 = (!tmp_434_fu_8297_p1.read().is_01() || !tmp_713_cast_fu_8329_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_434_fu_8297_p1.read()) + sc_bigint<9>(tmp_713_cast_fu_8329_p1.read()));
}

void mnist_fp16_opt::thread_tmp_471_fu_21212_p2() {
    tmp_471_fu_21212_p2 = (!tmp_1447_fu_21186_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1447_fu_21186_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_472_fu_20847_p2() {
    tmp_472_fu_20847_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_14_cast_fu_20824_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_14_cast_fu_20824_p1.read()));
}

void mnist_fp16_opt::thread_tmp_473_fu_8339_p1() {
    tmp_473_fu_8339_p1 = tmp_470_fu_8333_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_474_cast_fu_20139_p1() {
    tmp_474_cast_fu_20139_p1 = esl_zext<12,4>(r_V_23_fu_20134_p2.read());
}

void mnist_fp16_opt::thread_tmp_474_fu_8353_p3() {
    tmp_474_fu_8353_p3 = esl_concat<9,2>(tmp_470_reg_24190.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_475_fu_20904_p2() {
    tmp_475_fu_20904_p2 = (!p_Result_49_fu_20894_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_49_fu_20894_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16_opt::thread_tmp_476_fu_20230_p1() {
    tmp_476_fu_20230_p1 = esl_zext<12,11>(exp_tmp_V_4_reg_27387.read());
}

void mnist_fp16_opt::thread_tmp_477_fu_13717_p1() {
    tmp_477_fu_13717_p1 = esl_sext<32,16>(tmp_891_reg_25649.read());
}

void mnist_fp16_opt::thread_tmp_478_fu_20188_p2() {
    tmp_478_fu_20188_p2 = (!tmp_1468_fu_20162_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1468_fu_20162_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_479_fu_22057_p2() {
    tmp_479_fu_22057_p2 = (!sh_amt_9_reg_27874.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_9_reg_27874.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_47_fu_8381_p2() {
    tmp_47_fu_8381_p2 = (!relu1_0_V_q0.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu1_0_V_q0.read() == ap_const_lv16_0);
}

void mnist_fp16_opt::thread_tmp_480_fu_13720_p2() {
    tmp_480_fu_13720_p2 = (!sh_amt_4_cast_fu_13689_p1.read().is_01())? sc_lv<32>(): tmp_477_fu_13717_p1.read() << (unsigned short)sh_amt_4_cast_fu_13689_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_481_fu_8364_p2() {
    tmp_481_fu_8364_p2 = (!p_shl42_cast_fu_8346_p3.read().is_01() || !p_shl43_cast_fu_8360_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl42_cast_fu_8346_p3.read()) - sc_bigint<13>(p_shl43_cast_fu_8360_p1.read()));
}

void mnist_fp16_opt::thread_tmp_482_fu_8370_p2() {
    tmp_482_fu_8370_p2 = (!tmp_38_cast_fu_8343_p1.read().is_01() || !tmp_481_fu_8364_p2.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_38_cast_fu_8343_p1.read()) + sc_biguint<13>(tmp_481_fu_8364_p2.read()));
}

void mnist_fp16_opt::thread_tmp_483_fu_4919_p2() {
    tmp_483_fu_4919_p2 = (!p_s_reg_2273.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_2273.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_484_fu_21251_p2() {
    tmp_484_fu_21251_p2 = (!F2_s_fu_21245_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_s_fu_21245_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_485_fu_21257_p2() {
    tmp_485_fu_21257_p2 = (!ap_const_lv12_FF2.is_01() || !F2_s_fu_21245_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_s_fu_21245_p2.read()));
}

void mnist_fp16_opt::thread_tmp_486_fu_21263_p2() {
    tmp_486_fu_21263_p2 = (!ap_const_lv12_E.is_01() || !F2_s_fu_21245_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_s_fu_21245_p2.read()));
}

void mnist_fp16_opt::thread_tmp_487_fu_21277_p2() {
    tmp_487_fu_21277_p2 = (!F2_s_fu_21245_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_s_fu_21245_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_488_fu_4925_p2() {
    tmp_488_fu_4925_p2 = (tmp_483_fu_4919_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_488_mid2_cast_fu_15948_p1() {
    tmp_488_mid2_cast_fu_15948_p1 = esl_zext<10,4>(tmp_488_mid2_fu_15940_p3.read());
}

void mnist_fp16_opt::thread_tmp_488_mid2_fu_15940_p3() {
    tmp_488_mid2_fu_15940_p3 = (!exitcond61_mid_fu_15914_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond61_mid_fu_15914_p2.read()[0].to_bool())? args13_V_fu_15920_p2.read(): p_55_mid_fu_15852_p3.read());
}

void mnist_fp16_opt::thread_tmp_489_cast_fu_16006_p1() {
    tmp_489_cast_fu_16006_p1 = esl_zext<12,4>(p_60_mid2_reg_26193.read());
}

void mnist_fp16_opt::thread_tmp_489_fu_4930_p2() {
    tmp_489_fu_4930_p2 = (tmp_11_reg_23152.read() | tmp_488_fu_4925_p2.read());
}

void mnist_fp16_opt::thread_tmp_48_fu_7442_p2() {
    tmp_48_fu_7442_p2 = (notrhs_reg_23977.read() | notlhs_reg_23972.read());
}

void mnist_fp16_opt::thread_tmp_490_fu_16023_p4() {
    tmp_490_fu_16023_p4 = conv4_0_load_to_int_fu_16020_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_491_fu_20263_p2() {
    tmp_491_fu_20263_p2 = (!F2_10_fu_20257_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_10_fu_20257_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_492_fu_20269_p2() {
    tmp_492_fu_20269_p2 = (!ap_const_lv12_FF2.is_01() || !F2_10_fu_20257_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_10_fu_20257_p2.read()));
}

void mnist_fp16_opt::thread_tmp_493_fu_20275_p2() {
    tmp_493_fu_20275_p2 = (!ap_const_lv12_E.is_01() || !F2_10_fu_20257_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_10_fu_20257_p2.read()));
}

void mnist_fp16_opt::thread_tmp_494_fu_20289_p2() {
    tmp_494_fu_20289_p2 = (!F2_10_fu_20257_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_10_fu_20257_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_496_fu_22572_p1() {
    tmp_496_fu_22572_p1 = esl_zext<64,4>(p_74_reg_4268.read());
}

void mnist_fp16_opt::thread_tmp_49_cast_fu_6062_p1() {
    tmp_49_cast_fu_6062_p1 = esl_sext<64,11>(tmp_34_fu_6057_p2.read());
}

void mnist_fp16_opt::thread_tmp_49_fu_6337_p2() {
    tmp_49_fu_6337_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_10.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_10));
}

void mnist_fp16_opt::thread_tmp_502_fu_20926_p3() {
    tmp_502_fu_20926_p3 = esl_concat<1,8>(is_neg_5_reg_27528.read(), p_Repl2_22_trunc_fu_20920_p2.read());
}

void mnist_fp16_opt::thread_tmp_504_fu_22062_p1() {
    tmp_504_fu_22062_p1 = esl_zext<54,32>(sh_amt_9_cast_fu_22054_p1.read());
}

void mnist_fp16_opt::thread_tmp_505_fu_22066_p2() {
    tmp_505_fu_22066_p2 = (!man_V_26_reg_27864.read().is_01() || !tmp_504_fu_22062_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_26_reg_27864.read()) >> (unsigned short)tmp_504_fu_22062_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_506_fu_4935_p2() {
    tmp_506_fu_4935_p2 = (tmp_25_reg_23169.read() | tmp_489_fu_4930_p2.read());
}

void mnist_fp16_opt::thread_tmp_507_fu_22082_p1() {
    tmp_507_fu_22082_p1 = esl_sext<32,16>(tmp_1485_reg_27885.read());
}

void mnist_fp16_opt::thread_tmp_508_fu_22085_p2() {
    tmp_508_fu_22085_p2 = (!sh_amt_9_cast_fu_22054_p1.read().is_01())? sc_lv<32>(): tmp_507_fu_22082_p1.read() << (unsigned short)sh_amt_9_cast_fu_22054_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_509_fu_5160_p1() {
    tmp_509_fu_5160_p1 = esl_sext<12,8>(tmp_540_fu_5150_p4.read());
}

void mnist_fp16_opt::thread_tmp_510_fu_21594_p2() {
    tmp_510_fu_21594_p2 = (!ap_const_lv16_0.is_01() || !relu6_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu6_0_V_q0.read()));
}

void mnist_fp16_opt::thread_tmp_511_fu_8438_p1() {
    tmp_511_fu_8438_p1 = msb_idx_fu_8432_p2.read().range(31-1, 0);
}

void mnist_fp16_opt::thread_tmp_512_cast_fu_20727_p1() {
    tmp_512_cast_fu_20727_p1 = esl_zext<11,3>(p_64_mid2_reg_27273.read());
}

void mnist_fp16_opt::thread_tmp_513_fu_8460_p4() {
    tmp_513_fu_8460_p4 = msb_idx_1_fu_8450_p3.read().range(30, 5);
}

void mnist_fp16_opt::thread_tmp_515_fu_8491_p1() {
    tmp_515_fu_8491_p1 = msb_idx_1_fu_8450_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_516_fu_8495_p2() {
    tmp_516_fu_8495_p2 = (!ap_const_lv4_1.is_01() || !tmp_515_fu_8491_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_515_fu_8491_p1.read()));
}

void mnist_fp16_opt::thread_tmp_517_fu_20233_p3() {
    tmp_517_fu_20233_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_1470_reg_27392.read());
}

void mnist_fp16_opt::thread_tmp_518_fu_8501_p1() {
    tmp_518_fu_8501_p1 = esl_zext<16,4>(tmp_516_fu_8495_p2.read());
}

void mnist_fp16_opt::thread_tmp_519_fu_5164_p1() {
    tmp_519_fu_5164_p1 = esl_sext<12,9>(tmp_543_reg_23301.read());
}

void mnist_fp16_opt::thread_tmp_51_fu_7446_p2() {
    tmp_51_fu_7446_p2 = (tmp_48_fu_7442_p2.read() & tmp_50_reg_23982.read());
}

void mnist_fp16_opt::thread_tmp_520_fu_20438_p2() {
    tmp_520_fu_20438_p2 = (!sh_amt_10_reg_27430.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_10_reg_27430.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_521_fu_16049_p2() {
    tmp_521_fu_16049_p2 = (notrhs5_reg_26245.read() | notlhs5_reg_26240.read());
}

void mnist_fp16_opt::thread_tmp_522_fu_8542_p1() {
    tmp_522_fu_8542_p1 = msb_idx_reg_24231_pp6_iter30_reg.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_523_fu_22589_p1() {
    tmp_523_fu_22589_p1 = esl_zext<64,4>(p_76_reg_4291.read());
}

void mnist_fp16_opt::thread_tmp_530_fu_21683_p2() {
    tmp_530_fu_21683_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_16_cast_fu_21660_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_16_cast_fu_21660_p1.read()));
}

void mnist_fp16_opt::thread_tmp_531_fu_16053_p2() {
    tmp_531_fu_16053_p2 = (tmp_521_fu_16049_p2.read() & tmp_529_reg_26250.read());
}

void mnist_fp16_opt::thread_tmp_533_fu_21762_p3() {
    tmp_533_fu_21762_p3 = esl_concat<1,8>(tmp_1490_reg_27760_pp23_iter10_reg.read(), p_Repl2_26_trunc_fu_21756_p2.read());
}

void mnist_fp16_opt::thread_tmp_534_fu_4944_p1() {
    tmp_534_fu_4944_p1 = mul6_fu_22724_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_535_fu_21366_p1() {
    tmp_535_fu_21366_p1 = esl_sext<32,16>(tmp_1450_reg_27688.read());
}

void mnist_fp16_opt::thread_tmp_536_fu_21369_p2() {
    tmp_536_fu_21369_p2 = (!sh_amt_cast_72_fu_21338_p1.read().is_01())? sc_lv<32>(): tmp_535_fu_21366_p1.read() << (unsigned short)sh_amt_cast_72_fu_21338_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_538_fu_20443_p1() {
    tmp_538_fu_20443_p1 = esl_zext<54,32>(sh_amt_10_cast_fu_20435_p1.read());
}

void mnist_fp16_opt::thread_tmp_539_fu_20447_p2() {
    tmp_539_fu_20447_p2 = (!man_V_20_reg_27425.read().is_01() || !tmp_538_fu_20443_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_20_reg_27425.read()) >> (unsigned short)tmp_538_fu_20443_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_53_cast_fu_6122_p1() {
    tmp_53_cast_fu_6122_p1 = esl_sext<64,11>(tmp_35_fu_6117_p2.read());
}

void mnist_fp16_opt::thread_tmp_53_fu_6397_p2() {
    tmp_53_fu_6397_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_11.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_11));
}

void mnist_fp16_opt::thread_tmp_540_fu_5150_p4() {
    tmp_540_fu_5150_p4 = neg_mul6_fu_5145_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_541_fu_20463_p1() {
    tmp_541_fu_20463_p1 = esl_sext<32,16>(tmp_1471_reg_27441.read());
}

void mnist_fp16_opt::thread_tmp_542_fu_20466_p2() {
    tmp_542_fu_20466_p2 = (!sh_amt_10_cast_fu_20435_p1.read().is_01())? sc_lv<32>(): tmp_541_fu_20463_p1.read() << (unsigned short)sh_amt_10_cast_fu_20435_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_544_fu_16058_p1() {
    tmp_544_fu_16058_p1 = grp_fu_4358_p1.read();
}

void mnist_fp16_opt::thread_tmp_545_cast_fu_20106_p1() {
    tmp_545_cast_fu_20106_p1 = esl_zext<13,2>(p_72_mid2_reg_27323.read());
}

void mnist_fp16_opt::thread_tmp_545_fu_5167_p3() {
    tmp_545_fu_5167_p3 = (!tmp_537_reg_23295.read()[0].is_01())? sc_lv<12>(): ((tmp_537_reg_23295.read()[0].to_bool())? tmp_509_fu_5160_p1.read(): tmp_519_fu_5164_p1.read());
}

void mnist_fp16_opt::thread_tmp_546_fu_22594_p2() {
    tmp_546_fu_22594_p2 = (!ap_phi_mux_index_V_phi_fu_4319_p4.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_index_V_phi_fu_4319_p4.read() == ap_const_lv4_A);
}

void mnist_fp16_opt::thread_tmp_547_fu_20350_p1() {
    tmp_547_fu_20350_p1 = esl_zext<12,11>(exp_tmp_V_5_reg_27409.read());
}

void mnist_fp16_opt::thread_tmp_548_fu_16102_p1() {
    tmp_548_fu_16102_p1 = esl_zext<12,11>(p_Result_24_reg_26261.read());
}

void mnist_fp16_opt::thread_tmp_549_fu_20224_p2() {
    tmp_549_fu_20224_p2 = (!tmp_1475_fu_20198_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1475_fu_20198_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_551_fu_6017_p1() {
    tmp_551_fu_6017_p1 = grp_fu_5187_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_552_fu_6029_p1() {
    tmp_552_fu_6029_p1 = grp_fu_5187_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_553_fu_21740_p2() {
    tmp_553_fu_21740_p2 = (!p_Result_57_fu_21730_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_57_fu_21730_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16_opt::thread_tmp_554_fu_6041_p2() {
    tmp_554_fu_6041_p2 = (!p_shl44_cast_fu_6021_p3.read().is_01() || !p_shl45_cast_fu_6033_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl44_cast_fu_6021_p3.read()) - sc_biguint<11>(p_shl45_cast_fu_6033_p3.read()));
}

void mnist_fp16_opt::thread_tmp_555_fu_16096_p2() {
    tmp_555_fu_16096_p2 = (!tmp_1273_fu_16070_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1273_fu_16070_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_556_fu_20383_p2() {
    tmp_556_fu_20383_p2 = (!F2_11_fu_20377_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_11_fu_20377_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_557_fu_20389_p2() {
    tmp_557_fu_20389_p2 = (!ap_const_lv12_FF2.is_01() || !F2_11_fu_20377_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_11_fu_20377_p2.read()));
}

void mnist_fp16_opt::thread_tmp_558_fu_20395_p2() {
    tmp_558_fu_20395_p2 = (!ap_const_lv12_E.is_01() || !F2_11_fu_20377_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_11_fu_20377_p2.read()));
}

void mnist_fp16_opt::thread_tmp_559_fu_20409_p2() {
    tmp_559_fu_20409_p2 = (!F2_11_fu_20377_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_11_fu_20377_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_55_fu_8387_p2() {
    tmp_55_fu_8387_p2 = (!ap_const_lv16_0.is_01() || !relu1_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu1_0_V_q0.read()));
}

void mnist_fp16_opt::thread_tmp_560_fu_22606_p1() {
    tmp_560_fu_22606_p1 = esl_zext<64,4>(ap_phi_mux_index_V_phi_fu_4319_p4.read());
}

void mnist_fp16_opt::thread_tmp_561_fu_6067_p2() {
    tmp_561_fu_6067_p2 = (!ap_const_lv11_8.is_01() || !tmp_554_reg_23610.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_8) + sc_biguint<11>(tmp_554_reg_23610.read()));
}

void mnist_fp16_opt::thread_tmp_562_fu_16225_p2() {
    tmp_562_fu_16225_p2 = (!sh_amt_5_reg_26282.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_5_reg_26282.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_563_fu_6077_p2() {
    tmp_563_fu_6077_p2 = (!ap_const_lv11_9.is_01() || !tmp_554_reg_23610.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_9) + sc_biguint<11>(tmp_554_reg_23610.read()));
}

void mnist_fp16_opt::thread_tmp_564_fu_9898_p3() {
    tmp_564_fu_9898_p3 = esl_concat<3,5>(tmp_211_mid2_v_fu_9890_p3.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_565_fu_9910_p3() {
    tmp_565_fu_9910_p3 = esl_concat<3,2>(tmp_211_mid2_v_fu_9890_p3.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_566_fu_22399_p2() {
    tmp_566_fu_22399_p2 = (!p_Result_61_fu_22389_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_61_fu_22389_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16_opt::thread_tmp_567_fu_16230_p1() {
    tmp_567_fu_16230_p1 = esl_zext<54,32>(sh_amt_5_cast_fu_16222_p1.read());
}

void mnist_fp16_opt::thread_tmp_568_fu_20552_p2() {
    tmp_568_fu_20552_p2 = (!sh_amt_11_reg_27470.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_11_reg_27470.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_569_fu_16234_p2() {
    tmp_569_fu_16234_p2 = (!man_V_16_reg_26277.read().is_01() || !tmp_567_fu_16230_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_16_reg_26277.read()) >> (unsigned short)tmp_567_fu_16230_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_56_fu_7498_p3() {
    tmp_56_fu_7498_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_213_reg_23998.read());
}

void mnist_fp16_opt::thread_tmp_570_fu_9922_p2() {
    tmp_570_fu_9922_p2 = (!p_shl46_cast_fu_9906_p1.read().is_01() || !p_shl47_cast_fu_9918_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl46_cast_fu_9906_p1.read()) - sc_biguint<9>(p_shl47_cast_fu_9918_p1.read()));
}

void mnist_fp16_opt::thread_tmp_571_fu_9956_p2() {
    tmp_571_fu_9956_p2 = (exitcond29_mid_fu_9944_p2.read() | exitcond_flatten11_fu_9876_p2.read());
}

void mnist_fp16_opt::thread_tmp_572_fu_20557_p1() {
    tmp_572_fu_20557_p1 = esl_zext<54,32>(sh_amt_11_cast_fu_20549_p1.read());
}

void mnist_fp16_opt::thread_tmp_573_fu_20561_p2() {
    tmp_573_fu_20561_p2 = (!man_V_25_reg_27459.read().is_01() || !tmp_572_fu_20557_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_25_reg_27459.read()) >> (unsigned short)tmp_572_fu_20557_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_574_fu_9982_p2() {
    tmp_574_fu_9982_p2 = (!tmp_225_mid2_cast_fu_9978_p1.read().is_01() || !tmp_729_cast_fu_9928_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_225_mid2_cast_fu_9978_p1.read()) + sc_bigint<10>(tmp_729_cast_fu_9928_p1.read()));
}

void mnist_fp16_opt::thread_tmp_575_fu_20577_p1() {
    tmp_575_fu_20577_p1 = esl_sext<32,16>(tmp_1478_reg_27482.read());
}

void mnist_fp16_opt::thread_tmp_576_fu_20580_p2() {
    tmp_576_fu_20580_p2 = (!sh_amt_11_cast_fu_20549_p1.read().is_01())? sc_lv<32>(): tmp_575_fu_20577_p1.read() << (unsigned short)sh_amt_11_cast_fu_20549_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_577_fu_9988_p1() {
    tmp_577_fu_9988_p1 = tmp_574_fu_9982_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_578_fu_16250_p1() {
    tmp_578_fu_16250_p1 = esl_sext<32,16>(tmp_1276_reg_26293.read());
}

void mnist_fp16_opt::thread_tmp_579_fu_16253_p2() {
    tmp_579_fu_16253_p2 = (!sh_amt_5_cast_fu_16222_p1.read().is_01())? sc_lv<32>(): tmp_578_fu_16250_p1.read() << (unsigned short)sh_amt_5_cast_fu_16222_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_580_fu_22421_p3() {
    tmp_580_fu_22421_p3 = esl_concat<1,8>(tmp_1503_reg_27946.read(), p_Repl2_28_trunc_fu_22415_p2.read());
}

void mnist_fp16_opt::thread_tmp_581_fu_10019_p3() {
    tmp_581_fu_10019_p3 = esl_concat<10,2>(tmp_574_reg_24603.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_582_fu_10030_p2() {
    tmp_582_fu_10030_p2 = (!p_shl48_cast_fu_10012_p3.read().is_01() || !p_shl49_cast_fu_10026_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl48_cast_fu_10012_p3.read()) - sc_bigint<13>(p_shl49_cast_fu_10026_p1.read()));
}

void mnist_fp16_opt::thread_tmp_584_fu_20353_p3() {
    tmp_584_fu_20353_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_1477_reg_27414.read());
}

void mnist_fp16_opt::thread_tmp_585_fu_17126_p2() {
    tmp_585_fu_17126_p2 = (!p_46_mid2_reg_26501.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): (sc_bigint<4>(p_46_mid2_reg_26501.read()) > sc_bigint<4>(ap_const_lv4_0));
}

void mnist_fp16_opt::thread_tmp_586_fu_10039_p2() {
    tmp_586_fu_10039_p2 = (!tmp_231_cast_fu_10036_p1.read().is_01() || !tmp_582_fu_10030_p2.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_231_cast_fu_10036_p1.read()) + sc_biguint<13>(tmp_582_fu_10030_p2.read()));
}

void mnist_fp16_opt::thread_tmp_587_fu_10063_p1() {
    tmp_587_fu_10063_p1 = conv2_0_load_to_int_fu_10050_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_589_fu_10100_p1() {
    tmp_589_fu_10100_p1 = ireg_V_3_fu_10092_p3.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_589_mid2_cast_fu_18569_p1() {
    tmp_589_mid2_cast_fu_18569_p1 = esl_zext<10,3>(tmp_589_mid2_fu_18561_p3.read());
}

void mnist_fp16_opt::thread_tmp_589_mid2_fu_18561_p3() {
    tmp_589_mid2_fu_18561_p3 = (!exitcond77_mid_fu_18535_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond77_mid_fu_18535_p2.read()[0].to_bool())? args14_V_fu_18541_p2.read(): p_73_mid_fu_18481_p3.read());
}

void mnist_fp16_opt::thread_tmp_590_fu_16627_p2() {
    tmp_590_fu_16627_p2 = (!tmp_649_cast_fu_16623_p1.read().is_01() || !r_V_16_reg_26368.read().is_01())? sc_lv<4>(): (sc_biguint<4>(tmp_649_cast_fu_16623_p1.read()) + sc_biguint<4>(r_V_16_reg_26368.read()));
}

void mnist_fp16_opt::thread_tmp_591_fu_16690_p2() {
    tmp_591_fu_16690_p2 = (!relu4_0_V_load_reg_26412.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu4_0_V_load_reg_26412.read() == ap_const_lv16_0);
}

void mnist_fp16_opt::thread_tmp_592_fu_16684_p2() {
    tmp_592_fu_16684_p2 = (!ap_const_lv16_0.is_01() || !relu4_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu4_0_V_q0.read()));
}

void mnist_fp16_opt::thread_tmp_593_fu_16884_p4() {
    tmp_593_fu_16884_p4 = p_03_i3_to_int_fu_16881_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_595_fu_16902_p4() {
    tmp_595_fu_16902_p4 = tmp_647_to_int_fu_16898_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_596_fu_10122_p1() {
    tmp_596_fu_10122_p1 = ireg_V_3_fu_10092_p3.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_597_fu_16928_p2() {
    tmp_597_fu_16928_p2 = (notrhs6_fu_16922_p2.read() | notlhs6_fu_16916_p2.read());
}

void mnist_fp16_opt::thread_tmp_598_fu_16946_p2() {
    tmp_598_fu_16946_p2 = (notrhs7_fu_16940_p2.read() | notlhs7_fu_16934_p2.read());
}

void mnist_fp16_opt::thread_tmp_599_fu_16952_p2() {
    tmp_599_fu_16952_p2 = (tmp_597_fu_16928_p2.read() & tmp_598_fu_16946_p2.read());
}

void mnist_fp16_opt::thread_tmp_601_fu_16958_p2() {
    tmp_601_fu_16958_p2 = (tmp_599_fu_16952_p2.read() & grp_fu_4367_p2.read());
}

void mnist_fp16_opt::thread_tmp_602_cast_fu_18619_p1() {
    tmp_602_cast_fu_18619_p1 = esl_zext<11,3>(p_78_mid2_reg_26958.read());
}

void mnist_fp16_opt::thread_tmp_602_fu_10197_p1() {
    tmp_602_fu_10197_p1 = man_V_6_fu_10152_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_603_fu_18636_p4() {
    tmp_603_fu_18636_p4 = conv5_0_load_to_int_fu_18633_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_604_fu_10201_p4() {
    tmp_604_fu_10201_p4 = sh_amt_1_fu_10183_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_605_fu_18662_p2() {
    tmp_605_fu_18662_p2 = (notrhs8_reg_27010.read() | notlhs8_reg_27005.read());
}

void mnist_fp16_opt::thread_tmp_607_fu_18666_p2() {
    tmp_607_fu_18666_p2 = (tmp_605_fu_18662_p2.read() & tmp_606_reg_27015.read());
}

void mnist_fp16_opt::thread_tmp_608_to_int_fu_10943_p1() {
    tmp_608_to_int_fu_10943_p1 = tmp_313_reg_2955.read();
}

void mnist_fp16_opt::thread_tmp_609_cast_fu_10668_p1() {
    tmp_609_cast_fu_10668_p1 = esl_zext<5,2>(p_43_mid2_fu_10630_p3.read());
}

void mnist_fp16_opt::thread_tmp_609_fu_18671_p1() {
    tmp_609_fu_18671_p1 = grp_fu_4358_p1.read();
}

void mnist_fp16_opt::thread_tmp_60_cast_fu_6132_p1() {
    tmp_60_cast_fu_6132_p1 = esl_sext<64,11>(tmp_38_fu_6127_p2.read());
}

void mnist_fp16_opt::thread_tmp_60_fu_6407_p2() {
    tmp_60_fu_6407_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_12.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_12));
}

void mnist_fp16_opt::thread_tmp_610_cast_cast_fu_10707_p1() {
    tmp_610_cast_cast_fu_10707_p1 = esl_zext<13,5>(tmp_314_reg_24802.read());
}

void mnist_fp16_opt::thread_tmp_610_fu_18748_p2() {
    tmp_610_fu_18748_p2 = (!F2_8_fu_18742_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_8_fu_18742_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_611_fu_18754_p2() {
    tmp_611_fu_18754_p2 = (!ap_const_lv12_FF2.is_01() || !F2_8_fu_18742_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_8_fu_18742_p2.read()));
}

void mnist_fp16_opt::thread_tmp_612_fu_18760_p2() {
    tmp_612_fu_18760_p2 = (!ap_const_lv12_E.is_01() || !F2_8_fu_18742_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_8_fu_18742_p2.read()));
}

void mnist_fp16_opt::thread_tmp_613_fu_18774_p2() {
    tmp_613_fu_18774_p2 = (!F2_8_fu_18742_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_8_fu_18742_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_614_fu_10269_p1() {
    tmp_614_fu_10269_p1 = tmp_293_fu_10264_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_615_fu_18838_p2() {
    tmp_615_fu_18838_p2 = (!sh_amt_8_reg_27047.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_8_reg_27047.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_616_fu_10289_p1() {
    tmp_616_fu_10289_p1 = tmp_160_fu_10283_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_617_fu_18843_p1() {
    tmp_617_fu_18843_p1 = esl_zext<54,32>(sh_amt_8_cast_fu_18835_p1.read());
}

void mnist_fp16_opt::thread_tmp_618_fu_18847_p2() {
    tmp_618_fu_18847_p2 = (!man_V_21_reg_27042.read().is_01() || !tmp_617_fu_18843_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_21_reg_27042.read()) >> (unsigned short)tmp_617_fu_18843_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_619_fu_9628_p2() {
    tmp_619_fu_9628_p2 = (!tmp_405_reg_24302.read().is_01() || !tmp_120_cast_fu_9625_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_405_reg_24302.read()) + sc_biguint<13>(tmp_120_cast_fu_9625_p1.read()));
}

void mnist_fp16_opt::thread_tmp_61_cast_fu_6192_p1() {
    tmp_61_cast_fu_6192_p1 = esl_sext<64,11>(tmp_40_fu_6187_p2.read());
}

void mnist_fp16_opt::thread_tmp_61_fu_6467_p2() {
    tmp_61_fu_6467_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_13.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_13));
}

void mnist_fp16_opt::thread_tmp_61_mid2_v_fu_10399_p3() {
    tmp_61_mid2_v_fu_10399_p3 = (!exitcond_flatten14_fu_10385_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten14_fu_10385_p2.read()[0].to_bool())? c_V_fu_10379_p2.read(): p_22_reg_2886.read());
}

void mnist_fp16_opt::thread_tmp_620_fu_18863_p1() {
    tmp_620_fu_18863_p1 = esl_sext<32,16>(tmp_1392_reg_27058.read());
}

void mnist_fp16_opt::thread_tmp_621_fu_18866_p2() {
    tmp_621_fu_18866_p2 = (!sh_amt_8_cast_fu_18835_p1.read().is_01())? sc_lv<32>(): tmp_620_fu_18863_p1.read() << (unsigned short)sh_amt_8_cast_fu_18835_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_622_fu_9698_p1() {
    tmp_622_fu_9698_p1 = msb_idx_2_fu_9692_p2.read().range(31-1, 0);
}

void mnist_fp16_opt::thread_tmp_623_fu_18986_p2() {
    tmp_623_fu_18986_p2 = (!ap_phi_mux_p_57_phi_fu_3931_p4.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): (sc_bigint<4>(ap_phi_mux_p_57_phi_fu_3931_p4.read()) > sc_bigint<4>(ap_const_lv4_0));
}

void mnist_fp16_opt::thread_tmp_623_mid1_fu_19142_p2() {
    tmp_623_mid1_fu_19142_p2 = (!index_tuple5_V_fu_19088_p2.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): (sc_bigint<4>(index_tuple5_V_fu_19088_p2.read()) > sc_bigint<4>(ap_const_lv4_0));
}

void mnist_fp16_opt::thread_tmp_623_mid2_fu_19148_p3() {
    tmp_623_mid2_fu_19148_p3 = (!exitcond63_mid_fu_19082_p2.read()[0].is_01())? sc_lv<1>(): ((exitcond63_mid_fu_19082_p2.read()[0].to_bool())? tmp_623_mid1_fu_19142_p2.read(): tmp_623_mid_fu_19062_p2.read());
}

void mnist_fp16_opt::thread_tmp_623_mid_fu_19062_p2() {
    tmp_623_mid_fu_19062_p2 = (tmp_623_fu_18986_p2.read() & not_exitcond_flatten_17_fu_19056_p2.read());
}

void mnist_fp16_opt::thread_tmp_624_fu_19170_p2() {
    tmp_624_fu_19170_p2 = (!p_62_mid2_fu_19100_p3.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): (sc_bigint<4>(p_62_mid2_fu_19100_p3.read()) > sc_bigint<4>(ap_const_lv4_0));
}

void mnist_fp16_opt::thread_tmp_626_cast_fu_21122_p1() {
    tmp_626_cast_fu_21122_p1 = esl_zext<11,3>(p_84_mid2_reg_27588.read());
}

void mnist_fp16_opt::thread_tmp_626_fu_9726_p4() {
    tmp_626_fu_9726_p4 = msb_idx_3_fu_9716_p3.read().range(30, 5);
}

void mnist_fp16_opt::thread_tmp_627_fu_21139_p4() {
    tmp_627_fu_21139_p4 = conv6_0_load_to_int_fu_21136_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_628_fu_9757_p1() {
    tmp_628_fu_9757_p1 = msb_idx_3_fu_9716_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_629_fu_21165_p2() {
    tmp_629_fu_21165_p2 = (notrhs9_reg_27640.read() | notlhs9_reg_27635.read());
}

void mnist_fp16_opt::thread_tmp_62_cast_fu_6202_p1() {
    tmp_62_cast_fu_6202_p1 = esl_sext<64,11>(tmp_41_fu_6197_p2.read());
}

void mnist_fp16_opt::thread_tmp_62_fu_6477_p2() {
    tmp_62_fu_6477_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_14.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_14));
}

void mnist_fp16_opt::thread_tmp_631_fu_21169_p2() {
    tmp_631_fu_21169_p2 = (tmp_629_fu_21165_p2.read() & tmp_630_reg_27645.read());
}

void mnist_fp16_opt::thread_tmp_633_fu_21174_p1() {
    tmp_633_fu_21174_p1 = grp_fu_4358_p1.read();
}

void mnist_fp16_opt::thread_tmp_634_fu_21341_p2() {
    tmp_634_fu_21341_p2 = (!sh_amt_s_reg_27677.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_s_reg_27677.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16_opt::thread_tmp_635_fu_9761_p2() {
    tmp_635_fu_9761_p2 = (!ap_const_lv4_1.is_01() || !tmp_628_fu_9757_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_628_fu_9757_p1.read()));
}

void mnist_fp16_opt::thread_tmp_636_fu_21346_p1() {
    tmp_636_fu_21346_p1 = esl_zext<54,32>(sh_amt_cast_72_fu_21338_p1.read());
}

void mnist_fp16_opt::thread_tmp_637_fu_21350_p2() {
    tmp_637_fu_21350_p2 = (!man_V_28_reg_27672.read().is_01() || !tmp_636_fu_21346_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_28_reg_27672.read()) >> (unsigned short)tmp_636_fu_21346_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_638_cast_fu_21566_p1() {
    tmp_638_cast_fu_21566_p1 = esl_zext<11,3>(p_86_mid2_fu_21519_p3.read());
}

void mnist_fp16_opt::thread_tmp_638_fu_9767_p1() {
    tmp_638_fu_9767_p1 = esl_zext<16,4>(tmp_635_fu_9761_p2.read());
}

void mnist_fp16_opt::thread_tmp_639_fu_8800_p2() {
    tmp_639_fu_8800_p2 = (!tmp_64_mid2_cast_fu_8796_p1.read().is_01() || !tmp_691_cast1_reg_24297.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_64_mid2_cast_fu_8796_p1.read()) + sc_biguint<6>(tmp_691_cast1_reg_24297.read()));
}

void mnist_fp16_opt::thread_tmp_63_fu_9710_p2() {
    tmp_63_fu_9710_p2 = (!p_Val2_18_reg_2797.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(p_Val2_18_reg_2797.read() == ap_const_lv16_0);
}

void mnist_fp16_opt::thread_tmp_640_cast_mid2_ca_fu_16610_p1() {
    tmp_640_cast_mid2_ca_fu_16610_p1 = esl_zext<10,4>(tmp_640_cast_mid2_v_fu_16605_p2.read());
}

void mnist_fp16_opt::thread_tmp_640_cast_mid2_v_1_fu_16601_p1() {
    tmp_640_cast_mid2_v_1_fu_16601_p1 = esl_zext<4,2>(tmp_640_cast_mid2_v_s_fu_16593_p3.read());
}

void mnist_fp16_opt::thread_tmp_640_cast_mid2_v_fu_16605_p2() {
    tmp_640_cast_mid2_v_fu_16605_p2 = (!tmp_640_cast_mid2_v_1_fu_16601_p1.read().is_01() || !r_V_15_mid2_reg_26358.read().is_01())? sc_lv<4>(): (sc_biguint<4>(tmp_640_cast_mid2_v_1_fu_16601_p1.read()) + sc_biguint<4>(r_V_15_mid2_reg_26358.read()));
}

void mnist_fp16_opt::thread_tmp_640_cast_mid2_v_s_fu_16593_p3() {
    tmp_640_cast_mid2_v_s_fu_16593_p3 = (!exitcond22_fu_16579_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond22_fu_16579_p2.read()[0].to_bool())? ra65_V_fu_16573_p2.read(): ap_phi_mux_p_70_phi_fu_3600_p4.read());
}

void mnist_fp16_opt::thread_tmp_640_fu_21794_p4() {
    tmp_640_fu_21794_p4 = p_03_i5_to_int_fu_21791_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_641_fu_9808_p1() {
    tmp_641_fu_9808_p1 = msb_idx_2_reg_24544.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_642_fu_21812_p4() {
    tmp_642_fu_21812_p4 = tmp_457_to_int_fu_21808_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_643_fu_8809_p3() {
    tmp_643_fu_8809_p3 = esl_concat<6,2>(tmp_639_fu_8800_p2.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_644_fu_21838_p2() {
    tmp_644_fu_21838_p2 = (notrhs10_fu_21832_p2.read() | notlhs10_fu_21826_p2.read());
}

void mnist_fp16_opt::thread_tmp_645_fu_21856_p2() {
    tmp_645_fu_21856_p2 = (notrhs11_fu_21850_p2.read() | notlhs11_fu_21844_p2.read());
}

void mnist_fp16_opt::thread_tmp_646_fu_21862_p2() {
    tmp_646_fu_21862_p2 = (tmp_644_fu_21838_p2.read() & tmp_645_fu_21856_p2.read());
}

void mnist_fp16_opt::thread_tmp_647_to_int_fu_16898_p1() {
    tmp_647_to_int_fu_16898_p1 = tmp_588_reg_3607.read();
}

void mnist_fp16_opt::thread_tmp_648_fu_21868_p2() {
    tmp_648_fu_21868_p2 = (tmp_646_fu_21862_p2.read() & grp_fu_4367_p2.read());
}

void mnist_fp16_opt::thread_tmp_649_cast_fu_16623_p1() {
    tmp_649_cast_fu_16623_p1 = esl_zext<4,2>(p_75_mid2_fu_16585_p3.read());
}

void mnist_fp16_opt::thread_tmp_649_fu_22472_p4() {
    tmp_649_fu_22472_p4 = p_a_assign_load_to_i_fu_22468_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_64_cast_fu_6262_p1() {
    tmp_64_cast_fu_6262_p1 = esl_sext<64,11>(tmp_43_fu_6257_p2.read());
}

void mnist_fp16_opt::thread_tmp_64_fu_6537_p2() {
    tmp_64_fu_6537_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_15.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_15));
}

void mnist_fp16_opt::thread_tmp_64_mid2_cast_fu_8796_p1() {
    tmp_64_mid2_cast_fu_8796_p1 = esl_zext<6,3>(tmp_64_mid2_v_fu_8788_p3.read());
}

void mnist_fp16_opt::thread_tmp_64_mid2_v_fu_8788_p3() {
    tmp_64_mid2_v_fu_8788_p3 = (!exitcond_flatten12_fu_8774_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten12_fu_8774_p2.read()[0].to_bool())? rc_V_fu_8768_p2.read(): ap_phi_mux_p_17_phi_fu_2768_p4.read());
}

void mnist_fp16_opt::thread_tmp_650_cast_cast_fu_16662_p1() {
    tmp_650_cast_cast_fu_16662_p1 = esl_zext<12,4>(tmp_590_reg_26397.read());
}

void mnist_fp16_opt::thread_tmp_650_fu_8821_p2() {
    tmp_650_fu_8821_p2 = (!p_shl_fu_8817_p1.read().is_01() || !tmp_737_cast_fu_8805_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl_fu_8817_p1.read()) - sc_biguint<64>(tmp_737_cast_fu_8805_p1.read()));
}

void mnist_fp16_opt::thread_tmp_651_fu_22490_p4() {
    tmp_651_fu_22490_p4 = compute14_to_int_fu_22486_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_652_fu_8827_p3() {
    tmp_652_fu_8827_p3 = esl_concat<3,5>(tmp_64_mid2_v_fu_8788_p3.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_653_fu_22516_p2() {
    tmp_653_fu_22516_p2 = (notrhs12_fu_22510_p2.read() | notlhs12_fu_22504_p2.read());
}

void mnist_fp16_opt::thread_tmp_654_fu_22534_p2() {
    tmp_654_fu_22534_p2 = (notrhs13_fu_22528_p2.read() | notlhs13_fu_22522_p2.read());
}

void mnist_fp16_opt::thread_tmp_655_fu_22540_p2() {
    tmp_655_fu_22540_p2 = (tmp_653_fu_22516_p2.read() & tmp_654_fu_22534_p2.read());
}

void mnist_fp16_opt::thread_tmp_657_fu_22546_p2() {
    tmp_657_fu_22546_p2 = (tmp_655_fu_22540_p2.read() & grp_fu_4367_p2.read());
}

void mnist_fp16_opt::thread_tmp_658_fu_22201_p1() {
    tmp_658_fu_22201_p1 = esl_zext<64,5>(p_87_reg_4221.read());
}

void mnist_fp16_opt::thread_tmp_659_fu_22255_p2() {
    tmp_659_fu_22255_p2 = (!pool3_0_V_load_reg_27940.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(pool3_0_V_load_reg_27940.read() == ap_const_lv16_0);
}

void mnist_fp16_opt::thread_tmp_65_fu_6547_p2() {
    tmp_65_fu_6547_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_16.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_16));
}

void mnist_fp16_opt::thread_tmp_660_fu_22249_p2() {
    tmp_660_fu_22249_p2 = (!ap_const_lv16_0.is_01() || !pool3_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(pool3_0_V_q0.read()));
}

void mnist_fp16_opt::thread_tmp_661_fu_22338_p2() {
    tmp_661_fu_22338_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_18_cast_fu_22315_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_18_cast_fu_22315_p1.read()));
}

void mnist_fp16_opt::thread_tmp_663_fu_22614_p4() {
    tmp_663_fu_22614_p4 = pred_2_to_int_fu_22611_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_664_fu_8839_p3() {
    tmp_664_fu_8839_p3 = esl_concat<3,1>(tmp_64_mid2_v_fu_8788_p3.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_665_fu_22632_p4() {
    tmp_665_fu_22632_p4 = pred_to_int_fu_22628_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_tmp_666_fu_8851_p2() {
    tmp_666_fu_8851_p2 = (!p_shl51_cast_fu_8835_p1.read().is_01() || !p_shl52_cast_fu_8847_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl51_cast_fu_8835_p1.read()) - sc_biguint<9>(p_shl52_cast_fu_8847_p1.read()));
}

void mnist_fp16_opt::thread_tmp_667_fu_22658_p2() {
    tmp_667_fu_22658_p2 = (notrhs14_fu_22652_p2.read() | notlhs14_fu_22646_p2.read());
}

void mnist_fp16_opt::thread_tmp_668_fu_22676_p2() {
    tmp_668_fu_22676_p2 = (notrhs15_fu_22670_p2.read() | notlhs15_fu_22664_p2.read());
}

void mnist_fp16_opt::thread_tmp_669_fu_22682_p2() {
    tmp_669_fu_22682_p2 = (tmp_667_fu_22658_p2.read() & tmp_668_fu_22676_p2.read());
}

void mnist_fp16_opt::thread_tmp_66_fu_6607_p2() {
    tmp_66_fu_6607_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_17.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_17));
}

void mnist_fp16_opt::thread_tmp_671_fu_22688_p2() {
    tmp_671_fu_22688_p2 = (tmp_669_fu_22682_p2.read() & grp_fu_4367_p2.read());
}

void mnist_fp16_opt::thread_tmp_672_cast_fu_5932_p1() {
    tmp_672_cast_fu_5932_p1 = esl_sext<64,11>(tmp_273_fu_5927_p2.read());
}

void mnist_fp16_opt::thread_tmp_672_fu_8885_p2() {
    tmp_672_fu_8885_p2 = (exitcond26_mid_fu_8873_p2.read() | exitcond_flatten12_fu_8774_p2.read());
}

void mnist_fp16_opt::thread_tmp_673_cast_fu_5942_p1() {
    tmp_673_cast_fu_5942_p1 = esl_sext<64,11>(tmp_276_fu_5937_p2.read());
}

void mnist_fp16_opt::thread_tmp_673_fu_8908_p3() {
    tmp_673_fu_8908_p3 = (!exitcond_flatten12_fu_8774_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten12_fu_8774_p2.read()[0].to_bool())? tmp_29_mid2_reg_24283.read(): r_V_8_fu_8751_p2.read());
}

void mnist_fp16_opt::thread_tmp_674_fu_8915_p3() {
    tmp_674_fu_8915_p3 = (!exitcond26_mid_fu_8873_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond26_mid_fu_8873_p2.read()[0].to_bool())? r_V_8_mid1_fu_8903_p2.read(): tmp_673_fu_8908_p3.read());
}

void mnist_fp16_opt::thread_tmp_675_fu_8927_p2() {
    tmp_675_fu_8927_p2 = (!tmp_80_mid2_cast_fu_8923_p1.read().is_01() || !tmp_742_cast_fu_8857_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_80_mid2_cast_fu_8923_p1.read()) + sc_bigint<10>(tmp_742_cast_fu_8857_p1.read()));
}

void mnist_fp16_opt::thread_tmp_676_cast_fu_7914_p1() {
    tmp_676_cast_fu_7914_p1 = esl_sext<10,9>(tmp_281_fu_7908_p2.read());
}

void mnist_fp16_opt::thread_tmp_676_fu_8933_p1() {
    tmp_676_fu_8933_p1 = tmp_675_fu_8927_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_677_fu_8999_p3() {
    tmp_677_fu_8999_p3 = esl_concat<10,1>(tmp_675_reg_24326.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_678_fu_9010_p2() {
    tmp_678_fu_9010_p2 = (!p_shl53_cast_fu_8992_p3.read().is_01() || !p_shl54_cast_fu_9006_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl53_cast_fu_8992_p3.read()) - sc_bigint<13>(p_shl54_cast_fu_9006_p1.read()));
}

void mnist_fp16_opt::thread_tmp_679_fu_8949_p2() {
    tmp_679_fu_8949_p2 = (!tmp_81_mid2_cast_fu_8945_p1.read().is_01() || !tmp_650_fu_8821_p2.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_81_mid2_cast_fu_8945_p1.read()) + sc_biguint<64>(tmp_650_fu_8821_p2.read()));
}

void mnist_fp16_opt::thread_tmp_67_cast_fu_8123_p1() {
    tmp_67_cast_fu_8123_p1 = esl_zext<6,5>(p_12_mid2_reg_24091.read());
}

void mnist_fp16_opt::thread_tmp_680_fu_8955_p1() {
    tmp_680_fu_8955_p1 = tmp_679_fu_8949_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_681_fu_8959_p1() {
    tmp_681_fu_8959_p1 = tmp_679_fu_8949_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_682_fu_9023_p2() {
    tmp_682_fu_9023_p2 = (!p_shl55_cast_fu_9016_p3.read().is_01() || !tmp_680_reg_24341.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl55_cast_fu_9016_p3.read()) - sc_biguint<9>(tmp_680_reg_24341.read()));
}

void mnist_fp16_opt::thread_tmp_683_fu_9031_p2() {
    tmp_683_fu_9031_p2 = (!tmp_104_cast_fu_9028_p1.read().is_01() || !tmp_678_fu_9010_p2.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_104_cast_fu_9028_p1.read()) + sc_biguint<13>(tmp_678_fu_9010_p2.read()));
}

void mnist_fp16_opt::thread_tmp_684_fu_9060_p1() {
    tmp_684_fu_9060_p1 = ireg_V_12_fu_9056_p1.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_685_fu_9045_p2() {
    tmp_685_fu_9045_p2 = (!tmp_169_cast_fu_9042_p1.read().is_01() || !tmp_682_fu_9023_p2.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_169_cast_fu_9042_p1.read()) + sc_biguint<9>(tmp_682_fu_9023_p2.read()));
}

void mnist_fp16_opt::thread_tmp_686_fu_9082_p1() {
    tmp_686_fu_9082_p1 = ireg_V_12_fu_9056_p1.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_687_fu_9193_p1() {
    tmp_687_fu_9193_p1 = man_V_4_fu_9148_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_688_cast_fu_6002_p1() {
    tmp_688_cast_fu_6002_p1 = esl_sext<64,11>(tmp_380_fu_5997_p2.read());
}

void mnist_fp16_opt::thread_tmp_688_fu_9197_p4() {
    tmp_688_fu_9197_p4 = sh_amt_2_fu_9179_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_689_cast_fu_6012_p1() {
    tmp_689_cast_fu_6012_p1 = esl_sext<64,11>(tmp_383_fu_6007_p2.read());
}

void mnist_fp16_opt::thread_tmp_689_fu_9315_p1() {
    tmp_689_fu_9315_p1 = tmp_163_fu_9310_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_68_cast_fu_8126_p1() {
    tmp_68_cast_fu_8126_p1 = esl_zext<6,5>(tmp_37_fu_8115_p3.read());
}

void mnist_fp16_opt::thread_tmp_68_fu_7451_p1() {
    tmp_68_fu_7451_p1 = grp_fu_4358_p1.read();
}

void mnist_fp16_opt::thread_tmp_690_fu_9335_p1() {
    tmp_690_fu_9335_p1 = tmp_166_fu_9329_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_691_cast1_fu_8678_p1() {
    tmp_691_cast1_fu_8678_p1 = esl_zext<6,5>(tmp_384_fu_8670_p3.read());
}

void mnist_fp16_opt::thread_tmp_691_cast_fu_8682_p1() {
    tmp_691_cast_fu_8682_p1 = esl_zext<9,5>(tmp_384_fu_8670_p3.read());
}

void mnist_fp16_opt::thread_tmp_691_fu_9096_p1() {
    tmp_691_fu_9096_p1 = ireg_V_13_fu_9092_p1.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_692_fu_8149_p2() {
    tmp_692_fu_8149_p2 = (!tmp_300_fu_7972_p2.read().is_01() || !tmp_93_cast_fu_8146_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_300_fu_7972_p2.read()) + sc_biguint<13>(tmp_93_cast_fu_8146_p1.read()));
}

void mnist_fp16_opt::thread_tmp_693_fu_9118_p1() {
    tmp_693_fu_9118_p1 = ireg_V_13_fu_9092_p1.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_694_cast_fu_8704_p1() {
    tmp_694_cast_fu_8704_p1 = esl_sext<10,9>(tmp_391_fu_8698_p2.read());
}

void mnist_fp16_opt::thread_tmp_694_fu_9278_p1() {
    tmp_694_fu_9278_p1 = man_V_9_fu_9233_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_695_fu_9282_p4() {
    tmp_695_fu_9282_p4 = sh_amt_3_fu_9264_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_696_fu_9464_p1() {
    tmp_696_fu_9464_p1 = tmp_219_fu_9459_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_697_fu_9484_p1() {
    tmp_697_fu_9484_p1 = tmp_222_fu_9478_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_698_fu_4956_p2() {
    tmp_698_fu_4956_p2 = (!p_s_reg_2273.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_2273.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_699_fu_4962_p2() {
    tmp_699_fu_4962_p2 = (tmp_698_fu_4956_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_69_fu_8479_p2() {
    tmp_69_fu_8479_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_1_cast_fu_8456_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_1_cast_fu_8456_p1.read()));
}

void mnist_fp16_opt::thread_tmp_6_cast_fu_4622_p1() {
    tmp_6_cast_fu_4622_p1 = esl_zext<11,5>(p_1_reg_2167.read());
}

void mnist_fp16_opt::thread_tmp_6_fu_4642_p2() {
    tmp_6_fu_4642_p2 = (!ap_phi_mux_p_s_phi_fu_2277_p4.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_s_phi_fu_2277_p4.read() == ap_const_lv5_1F);
}

void mnist_fp16_opt::thread_tmp_700_fu_4967_p2() {
    tmp_700_fu_4967_p2 = (tmp_11_reg_23152.read() | tmp_699_fu_4962_p2.read());
}

void mnist_fp16_opt::thread_tmp_701_fu_4972_p2() {
    tmp_701_fu_4972_p2 = (tmp_25_reg_23169.read() | tmp_700_fu_4967_p2.read());
}

void mnist_fp16_opt::thread_tmp_702_fu_5197_p1() {
    tmp_702_fu_5197_p1 = mul7_fu_22732_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_704_fu_5214_p4() {
    tmp_704_fu_5214_p4 = neg_mul7_fu_5209_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_705_fu_5224_p1() {
    tmp_705_fu_5224_p1 = esl_sext<12,8>(tmp_704_fu_5214_p4.read());
}

void mnist_fp16_opt::thread_tmp_707_fu_5228_p1() {
    tmp_707_fu_5228_p1 = esl_sext<12,9>(tmp_706_reg_23358.read());
}

void mnist_fp16_opt::thread_tmp_708_fu_5231_p3() {
    tmp_708_fu_5231_p3 = (!tmp_703_reg_23352.read()[0].is_01())? sc_lv<12>(): ((tmp_703_reg_23352.read()[0].to_bool())? tmp_705_fu_5224_p1.read(): tmp_707_fu_5228_p1.read());
}

void mnist_fp16_opt::thread_tmp_709_fu_6087_p1() {
    tmp_709_fu_6087_p1 = grp_fu_5251_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_70_fu_6617_p2() {
    tmp_70_fu_6617_p2 = (!tmp_19_reg_23186_pp3_iter1_reg.read().is_01() || !ap_const_lv11_18.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter1_reg.read()) + sc_biguint<11>(ap_const_lv11_18));
}

void mnist_fp16_opt::thread_tmp_710_fu_6099_p1() {
    tmp_710_fu_6099_p1 = grp_fu_5251_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_711_fu_6111_p2() {
    tmp_711_fu_6111_p2 = (!p_shl56_cast_fu_6091_p3.read().is_01() || !p_shl57_cast_fu_6103_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl56_cast_fu_6091_p3.read()) - sc_biguint<11>(p_shl57_cast_fu_6103_p3.read()));
}

void mnist_fp16_opt::thread_tmp_712_fu_6137_p2() {
    tmp_712_fu_6137_p2 = (!ap_const_lv11_A.is_01() || !tmp_711_reg_23626.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_A) + sc_biguint<11>(tmp_711_reg_23626.read()));
}

void mnist_fp16_opt::thread_tmp_713_cast_fu_8329_p1() {
    tmp_713_cast_fu_8329_p1 = esl_sext<9,8>(tmp_465_fu_8323_p2.read());
}

void mnist_fp16_opt::thread_tmp_713_fu_6147_p2() {
    tmp_713_fu_6147_p2 = (!ap_const_lv11_B.is_01() || !tmp_711_reg_23626.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_B) + sc_biguint<11>(tmp_711_reg_23626.read()));
}

void mnist_fp16_opt::thread_tmp_714_fu_10413_p3() {
    tmp_714_fu_10413_p3 = esl_concat<3,5>(tmp_61_mid2_v_reg_24734.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_715_fu_10424_p3() {
    tmp_715_fu_10424_p3 = esl_concat<3,2>(tmp_61_mid2_v_reg_24734.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_716_fu_10435_p2() {
    tmp_716_fu_10435_p2 = (!p_shl58_cast_fu_10420_p1.read().is_01() || !p_shl59_cast_fu_10431_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl58_cast_fu_10420_p1.read()) - sc_biguint<9>(p_shl59_cast_fu_10431_p1.read()));
}

void mnist_fp16_opt::thread_tmp_717_fu_10445_p3() {
    tmp_717_fu_10445_p3 = esl_concat<3,4>(tmp_61_mid2_v_reg_24734.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_718_cast_fu_8376_p1() {
    tmp_718_cast_fu_8376_p1 = esl_zext<64,13>(tmp_482_fu_8370_p2.read());
}

void mnist_fp16_opt::thread_tmp_718_fu_10456_p3() {
    tmp_718_fu_10456_p3 = esl_concat<3,1>(tmp_61_mid2_v_reg_24734.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_719_fu_10467_p2() {
    tmp_719_fu_10467_p2 = (!p_shl60_cast_fu_10452_p1.read().is_01() || !p_shl61_cast_fu_10463_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl60_cast_fu_10452_p1.read()) - sc_biguint<8>(p_shl61_cast_fu_10463_p1.read()));
}

void mnist_fp16_opt::thread_tmp_720_fu_10507_p2() {
    tmp_720_fu_10507_p2 = (exitcond34_mid_fu_10497_p2.read() | exitcond_flatten14_reg_24720.read());
}

void mnist_fp16_opt::thread_tmp_721_fu_10531_p2() {
    tmp_721_fu_10531_p2 = (!tmp_767_cast_fu_10473_p1.read().is_01() || !lhs_V_mid2_cast_fu_10527_p1.read().is_01())? sc_lv<9>(): (sc_bigint<9>(tmp_767_cast_fu_10473_p1.read()) + sc_biguint<9>(lhs_V_mid2_cast_fu_10527_p1.read()));
}

void mnist_fp16_opt::thread_tmp_722_fu_10537_p1() {
    tmp_722_fu_10537_p1 = tmp_721_fu_10531_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_723_fu_10549_p3() {
    tmp_723_fu_10549_p3 = esl_concat<9,1>(tmp_721_fu_10531_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_724_fu_10561_p2() {
    tmp_724_fu_10561_p2 = (!p_shl62_cast_fu_10541_p3.read().is_01() || !p_shl63_cast_fu_10557_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl62_cast_fu_10541_p3.read()) - sc_bigint<11>(p_shl63_cast_fu_10557_p1.read()));
}

void mnist_fp16_opt::thread_tmp_725_cast_fu_6072_p1() {
    tmp_725_cast_fu_6072_p1 = esl_sext<64,11>(tmp_561_fu_6067_p2.read());
}

void mnist_fp16_opt::thread_tmp_725_fu_10587_p2() {
    tmp_725_fu_10587_p2 = (!tmp_724_fu_10561_p2.read().is_01() || !lhs_V_3_cast_fu_10583_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_724_fu_10561_p2.read()) + sc_biguint<11>(lhs_V_3_cast_fu_10583_p1.read()));
}

void mnist_fp16_opt::thread_tmp_726_cast_fu_6082_p1() {
    tmp_726_cast_fu_6082_p1 = esl_sext<64,11>(tmp_563_fu_6077_p2.read());
}

void mnist_fp16_opt::thread_tmp_726_fu_4977_p2() {
    tmp_726_fu_4977_p2 = (!p_s_reg_2273.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_2273.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_727_fu_4983_p2() {
    tmp_727_fu_4983_p2 = (tmp_726_fu_4977_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_728_fu_4988_p2() {
    tmp_728_fu_4988_p2 = (tmp_11_reg_23152.read() | tmp_727_fu_4983_p2.read());
}

void mnist_fp16_opt::thread_tmp_729_cast_fu_9928_p1() {
    tmp_729_cast_fu_9928_p1 = esl_sext<10,9>(tmp_570_fu_9922_p2.read());
}

void mnist_fp16_opt::thread_tmp_729_fu_4993_p2() {
    tmp_729_fu_4993_p2 = (tmp_25_reg_23169.read() | tmp_728_fu_4988_p2.read());
}

void mnist_fp16_opt::thread_tmp_730_fu_5261_p1() {
    tmp_730_fu_5261_p1 = mul8_fu_22740_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_732_fu_5278_p4() {
    tmp_732_fu_5278_p4 = neg_mul8_fu_5273_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_733_fu_5288_p1() {
    tmp_733_fu_5288_p1 = esl_sext<12,8>(tmp_732_fu_5278_p4.read());
}

void mnist_fp16_opt::thread_tmp_735_cast_fu_10045_p1() {
    tmp_735_cast_fu_10045_p1 = esl_zext<64,13>(tmp_586_fu_10039_p2.read());
}

void mnist_fp16_opt::thread_tmp_735_fu_5292_p1() {
    tmp_735_fu_5292_p1 = esl_sext<12,9>(tmp_734_reg_23379.read());
}

void mnist_fp16_opt::thread_tmp_736_cast_fu_9854_p1() {
    tmp_736_cast_fu_9854_p1 = esl_zext<64,13>(tmp_619_reg_24518.read());
}

void mnist_fp16_opt::thread_tmp_736_fu_5295_p3() {
    tmp_736_fu_5295_p3 = (!tmp_731_reg_23373.read()[0].is_01())? sc_lv<12>(): ((tmp_731_reg_23373.read()[0].to_bool())? tmp_733_fu_5288_p1.read(): tmp_735_fu_5292_p1.read());
}

void mnist_fp16_opt::thread_tmp_737_cast_fu_8805_p1() {
    tmp_737_cast_fu_8805_p1 = esl_zext<64,6>(tmp_639_fu_8800_p2.read());
}

void mnist_fp16_opt::thread_tmp_737_fu_6157_p1() {
    tmp_737_fu_6157_p1 = grp_fu_5315_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_738_fu_6169_p1() {
    tmp_738_fu_6169_p1 = grp_fu_5315_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_739_fu_6181_p2() {
    tmp_739_fu_6181_p2 = (!p_shl64_cast_fu_6161_p3.read().is_01() || !p_shl65_cast_fu_6173_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl64_cast_fu_6161_p3.read()) - sc_biguint<11>(p_shl65_cast_fu_6173_p3.read()));
}

void mnist_fp16_opt::thread_tmp_73_cast_fu_6272_p1() {
    tmp_73_cast_fu_6272_p1 = esl_sext<64,11>(tmp_44_fu_6267_p2.read());
}

void mnist_fp16_opt::thread_tmp_73_fu_6835_p2() {
    tmp_73_fu_6835_p2 = (!tmp_19_reg_23186_pp3_iter2_reg.read().is_01() || !ap_const_lv11_19.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter2_reg.read()) + sc_biguint<11>(ap_const_lv11_19));
}

void mnist_fp16_opt::thread_tmp_740_fu_6207_p2() {
    tmp_740_fu_6207_p2 = (!ap_const_lv11_C.is_01() || !tmp_739_reg_23642.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_C) + sc_biguint<11>(tmp_739_reg_23642.read()));
}

void mnist_fp16_opt::thread_tmp_741_fu_6217_p2() {
    tmp_741_fu_6217_p2 = (!ap_const_lv11_D.is_01() || !tmp_739_reg_23642.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_D) + sc_biguint<11>(tmp_739_reg_23642.read()));
}

void mnist_fp16_opt::thread_tmp_742_cast_fu_8857_p1() {
    tmp_742_cast_fu_8857_p1 = esl_sext<10,9>(tmp_666_fu_8851_p2.read());
}

void mnist_fp16_opt::thread_tmp_742_fu_11074_p3() {
    tmp_742_fu_11074_p3 = esl_concat<3,4>(lhs_V_5_mid2_v_fu_11066_p3.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_743_fu_11090_p2() {
    tmp_743_fu_11090_p2 = (!tmp_783_cast_fu_11082_p1.read().is_01() || !lhs_V_1_cast_fu_11086_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_783_cast_fu_11082_p1.read()) + sc_biguint<8>(lhs_V_1_cast_fu_11086_p1.read()));
}

void mnist_fp16_opt::thread_tmp_744_fu_11096_p3() {
    tmp_744_fu_11096_p3 = esl_concat<8,4>(tmp_743_fu_11090_p2.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_745_fu_11104_p1() {
    tmp_745_fu_11104_p1 = esl_zext<64,12>(tmp_744_fu_11096_p3.read());
}

void mnist_fp16_opt::thread_tmp_746_fu_12238_p2() {
    tmp_746_fu_12238_p2 = (tmp_744_reg_24919_pp10_iter2_reg.read() | ap_const_lv12_1);
}

void mnist_fp16_opt::thread_tmp_747_fu_12248_p2() {
    tmp_747_fu_12248_p2 = (tmp_744_reg_24919_pp10_iter2_reg.read() | ap_const_lv12_2);
}

void mnist_fp16_opt::thread_tmp_748_fu_12350_p2() {
    tmp_748_fu_12350_p2 = (tmp_744_reg_24919_pp10_iter2_reg.read() | ap_const_lv12_3);
}

void mnist_fp16_opt::thread_tmp_749_fu_12360_p2() {
    tmp_749_fu_12360_p2 = (tmp_744_reg_24919_pp10_iter2_reg.read() | ap_const_lv12_4);
}

void mnist_fp16_opt::thread_tmp_74_cast_fu_6332_p1() {
    tmp_74_cast_fu_6332_p1 = esl_sext<64,11>(tmp_46_fu_6327_p2.read());
}

void mnist_fp16_opt::thread_tmp_74_fu_6845_p2() {
    tmp_74_fu_6845_p2 = (!tmp_19_reg_23186_pp3_iter2_reg.read().is_01() || !ap_const_lv11_1A.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter2_reg.read()) + sc_biguint<11>(ap_const_lv11_1A));
}

void mnist_fp16_opt::thread_tmp_74_mid2_v_fu_12890_p3() {
    tmp_74_mid2_v_fu_12890_p3 = (!exitcond_flatten18_fu_12876_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten18_fu_12876_p2.read()[0].to_bool())? ff2_V_fu_12870_p2.read(): p_18_reg_3111.read());
}

void mnist_fp16_opt::thread_tmp_750_fu_12462_p2() {
    tmp_750_fu_12462_p2 = (tmp_744_reg_24919_pp10_iter2_reg.read() | ap_const_lv12_5);
}

void mnist_fp16_opt::thread_tmp_751_cast_fu_9037_p1() {
    tmp_751_cast_fu_9037_p1 = esl_zext<64,13>(tmp_683_fu_9031_p2.read());
}

void mnist_fp16_opt::thread_tmp_751_fu_12472_p2() {
    tmp_751_fu_12472_p2 = (tmp_744_reg_24919_pp10_iter2_reg.read() | ap_const_lv12_6);
}

void mnist_fp16_opt::thread_tmp_752_cast_fu_9051_p1() {
    tmp_752_cast_fu_9051_p1 = esl_zext<64,9>(tmp_685_fu_9045_p2.read());
}

void mnist_fp16_opt::thread_tmp_752_fu_12574_p2() {
    tmp_752_fu_12574_p2 = (tmp_744_reg_24919_pp10_iter2_reg.read() | ap_const_lv12_7);
}

void mnist_fp16_opt::thread_tmp_753_cast_fu_8581_p1() {
    tmp_753_cast_fu_8581_p1 = esl_zext<64,13>(tmp_692_reg_24146_pp6_iter30_reg.read());
}

void mnist_fp16_opt::thread_tmp_753_fu_12584_p2() {
    tmp_753_fu_12584_p2 = (tmp_744_reg_24919_pp10_iter2_reg.read() | ap_const_lv12_8);
}

void mnist_fp16_opt::thread_tmp_754_fu_12778_p2() {
    tmp_754_fu_12778_p2 = (tmp_744_reg_24919_pp10_iter3_reg.read() | ap_const_lv12_9);
}

void mnist_fp16_opt::thread_tmp_755_fu_12788_p2() {
    tmp_755_fu_12788_p2 = (tmp_744_reg_24919_pp10_iter3_reg.read() | ap_const_lv12_A);
}

void mnist_fp16_opt::thread_tmp_756_fu_12818_p2() {
    tmp_756_fu_12818_p2 = (tmp_744_reg_24919_pp10_iter3_reg.read() | ap_const_lv12_B);
}

void mnist_fp16_opt::thread_tmp_757_fu_12828_p2() {
    tmp_757_fu_12828_p2 = (tmp_744_reg_24919_pp10_iter3_reg.read() | ap_const_lv12_C);
}

void mnist_fp16_opt::thread_tmp_758_fu_12838_p2() {
    tmp_758_fu_12838_p2 = (tmp_744_reg_24919_pp10_iter3_reg.read() | ap_const_lv12_D);
}

void mnist_fp16_opt::thread_tmp_759_fu_12848_p2() {
    tmp_759_fu_12848_p2 = (tmp_744_reg_24919_pp10_iter3_reg.read() | ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_75_fu_11160_p2() {
    tmp_75_fu_11160_p2 = (!p_20_mid2_reg_24906.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_20_mid2_reg_24906.read() != ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_760_cast_fu_6142_p1() {
    tmp_760_cast_fu_6142_p1 = esl_sext<64,11>(tmp_712_fu_6137_p2.read());
}

void mnist_fp16_opt::thread_tmp_760_fu_11109_p2() {
    tmp_760_fu_11109_p2 = (tmp_744_fu_11096_p3.read() | ap_const_lv12_F);
}

void mnist_fp16_opt::thread_tmp_761_cast_fu_6152_p1() {
    tmp_761_cast_fu_6152_p1 = esl_sext<64,11>(tmp_713_fu_6147_p2.read());
}

void mnist_fp16_opt::thread_tmp_761_fu_11120_p1() {
    tmp_761_fu_11120_p1 = p_20_mid2_fu_11058_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_762_fu_11136_p2() {
    tmp_762_fu_11136_p2 = (!ap_const_lv5_1.is_01())? sc_lv<5>(): p_20_mid2_fu_11058_p3.read() << (unsigned short)ap_const_lv5_1.to_uint();
}

void mnist_fp16_opt::thread_tmp_763_fu_10659_p2() {
    tmp_763_fu_10659_p2 = (!tmp_764_cast_reg_24748.read().is_01() || !tmp_136_cast_mid2_ca_fu_10655_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_764_cast_reg_24748.read()) + sc_biguint<10>(tmp_136_cast_mid2_ca_fu_10655_p1.read()));
}

void mnist_fp16_opt::thread_tmp_764_cast_fu_10441_p1() {
    tmp_764_cast_fu_10441_p1 = esl_sext<10,9>(tmp_716_fu_10435_p2.read());
}

void mnist_fp16_opt::thread_tmp_764_fu_10664_p1() {
    tmp_764_fu_10664_p1 = tmp_763_fu_10659_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_765_fu_10690_p3() {
    tmp_765_fu_10690_p3 = esl_concat<10,2>(tmp_763_reg_24792.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_766_fu_10701_p2() {
    tmp_766_fu_10701_p2 = (!p_shl66_cast_fu_10683_p3.read().is_01() || !p_shl67_cast_fu_10697_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl66_cast_fu_10683_p3.read()) - sc_bigint<13>(p_shl67_cast_fu_10697_p1.read()));
}

void mnist_fp16_opt::thread_tmp_767_cast_fu_10473_p1() {
    tmp_767_cast_fu_10473_p1 = esl_sext<9,8>(tmp_719_fu_10467_p2.read());
}

void mnist_fp16_opt::thread_tmp_767_fu_10710_p2() {
    tmp_767_fu_10710_p2 = (!tmp_766_fu_10701_p2.read().is_01() || !tmp_610_cast_cast_fu_10707_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_766_fu_10701_p2.read()) + sc_biguint<13>(tmp_610_cast_cast_fu_10707_p1.read()));
}

void mnist_fp16_opt::thread_tmp_769_fu_10777_p1() {
    tmp_769_fu_10777_p1 = msb_idx_4_fu_10771_p2.read().range(31-1, 0);
}

void mnist_fp16_opt::thread_tmp_76_fu_11165_p2() {
    tmp_76_fu_11165_p2 = (!p_20_mid2_reg_24906.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_20_mid2_reg_24906.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp16_opt::thread_tmp_771_fu_10799_p4() {
    tmp_771_fu_10799_p4 = msb_idx_5_fu_10789_p3.read().range(30, 5);
}

void mnist_fp16_opt::thread_tmp_772_fu_10830_p1() {
    tmp_772_fu_10830_p1 = msb_idx_5_fu_10789_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_773_cast_fu_10593_p1() {
    tmp_773_cast_fu_10593_p1 = esl_zext<64,11>(tmp_725_fu_10587_p2.read());
}

void mnist_fp16_opt::thread_tmp_773_fu_10834_p2() {
    tmp_773_fu_10834_p2 = (!ap_const_lv4_1.is_01() || !tmp_772_fu_10830_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_772_fu_10830_p1.read()));
}

void mnist_fp16_opt::thread_tmp_774_fu_10840_p1() {
    tmp_774_fu_10840_p1 = esl_zext<16,4>(tmp_773_fu_10834_p2.read());
}

void mnist_fp16_opt::thread_tmp_775_fu_10844_p2() {
    tmp_775_fu_10844_p2 = (!tmp_774_fu_10840_p1.read().is_01())? sc_lv<16>(): tmp_V_2_reg_24839.read() >> (unsigned short)tmp_774_fu_10840_p1.read().to_uint();
}

void mnist_fp16_opt::thread_tmp_776_fu_10881_p1() {
    tmp_776_fu_10881_p1 = msb_idx_4_reg_24845_pp9_iter10_reg.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_777_fu_10939_p1() {
    tmp_777_fu_10939_p1 = p_03_i1_to_int_fu_10926_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_778_fu_10957_p1() {
    tmp_778_fu_10957_p1 = tmp_608_to_int_fu_10943_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_779_fu_4998_p2() {
    tmp_779_fu_4998_p2 = (!p_s_reg_2273.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_2273.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_780_cast_fu_6212_p1() {
    tmp_780_cast_fu_6212_p1 = esl_sext<64,11>(tmp_740_fu_6207_p2.read());
}

void mnist_fp16_opt::thread_tmp_780_fu_5004_p2() {
    tmp_780_fu_5004_p2 = (tmp_779_fu_4998_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_781_cast_fu_6222_p1() {
    tmp_781_cast_fu_6222_p1 = esl_sext<64,11>(tmp_741_fu_6217_p2.read());
}

void mnist_fp16_opt::thread_tmp_781_fu_5009_p2() {
    tmp_781_fu_5009_p2 = (tmp_11_reg_23152.read() | tmp_780_fu_5004_p2.read());
}

void mnist_fp16_opt::thread_tmp_782_fu_5014_p2() {
    tmp_782_fu_5014_p2 = (tmp_25_reg_23169.read() | tmp_781_fu_5009_p2.read());
}

void mnist_fp16_opt::thread_tmp_783_cast_fu_11082_p1() {
    tmp_783_cast_fu_11082_p1 = esl_zext<8,7>(tmp_742_fu_11074_p3.read());
}

void mnist_fp16_opt::thread_tmp_783_fu_11191_p1() {
    tmp_783_fu_11191_p1 = mul9_fu_22853_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_785_fu_11224_p4() {
    tmp_785_fu_11224_p4 = neg_mul9_fu_11219_p2.read().range(24, 16);
}

void mnist_fp16_opt::thread_tmp_786_fu_11234_p1() {
    tmp_786_fu_11234_p1 = esl_sext<12,9>(tmp_785_fu_11224_p4.read());
}

void mnist_fp16_opt::thread_tmp_787_cast_fu_12243_p1() {
    tmp_787_cast_fu_12243_p1 = esl_zext<64,12>(tmp_746_fu_12238_p2.read());
}

void mnist_fp16_opt::thread_tmp_788_cast_fu_12253_p1() {
    tmp_788_cast_fu_12253_p1 = esl_zext<64,12>(tmp_747_fu_12248_p2.read());
}

void mnist_fp16_opt::thread_tmp_788_fu_11238_p1() {
    tmp_788_fu_11238_p1 = esl_sext<12,10>(tmp_787_reg_24983.read());
}

void mnist_fp16_opt::thread_tmp_789_cast_fu_12355_p1() {
    tmp_789_cast_fu_12355_p1 = esl_zext<64,12>(tmp_748_fu_12350_p2.read());
}

void mnist_fp16_opt::thread_tmp_789_fu_11241_p3() {
    tmp_789_fu_11241_p3 = (!tmp_784_reg_24975.read()[0].is_01())? sc_lv<12>(): ((tmp_784_reg_24975.read()[0].to_bool())? tmp_786_fu_11234_p1.read(): tmp_788_fu_11238_p1.read());
}

void mnist_fp16_opt::thread_tmp_790_cast_fu_12365_p1() {
    tmp_790_cast_fu_12365_p1 = esl_zext<64,12>(tmp_749_fu_12360_p2.read());
}

void mnist_fp16_opt::thread_tmp_790_fu_12079_p1() {
    tmp_790_fu_12079_p1 = tmp_110_reg_25283.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_791_cast_fu_12467_p1() {
    tmp_791_cast_fu_12467_p1 = esl_zext<64,12>(tmp_750_fu_12462_p2.read());
}

void mnist_fp16_opt::thread_tmp_791_fu_11645_p1() {
    tmp_791_fu_11645_p1 = mul10_fu_22909_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_792_cast_fu_12477_p1() {
    tmp_792_cast_fu_12477_p1 = esl_zext<64,12>(tmp_751_fu_12472_p2.read());
}

void mnist_fp16_opt::thread_tmp_792_fu_11662_p4() {
    tmp_792_fu_11662_p4 = neg_mul10_fu_11657_p2.read().range(24, 20);
}

void mnist_fp16_opt::thread_tmp_793_cast_fu_12579_p1() {
    tmp_793_cast_fu_12579_p1 = esl_zext<64,12>(tmp_752_fu_12574_p2.read());
}

void mnist_fp16_opt::thread_tmp_793_fu_11672_p1() {
    tmp_793_fu_11672_p1 = esl_sext<13,5>(tmp_792_fu_11662_p4.read());
}

void mnist_fp16_opt::thread_tmp_794_cast_fu_12589_p1() {
    tmp_794_cast_fu_12589_p1 = esl_zext<64,12>(tmp_753_fu_12584_p2.read());
}

void mnist_fp16_opt::thread_tmp_795_cast_fu_12783_p1() {
    tmp_795_cast_fu_12783_p1 = esl_zext<64,12>(tmp_754_fu_12778_p2.read());
}

void mnist_fp16_opt::thread_tmp_795_fu_11676_p1() {
    tmp_795_fu_11676_p1 = esl_sext<13,6>(tmp_794_reg_25176.read());
}

void mnist_fp16_opt::thread_tmp_796_cast_fu_12793_p1() {
    tmp_796_cast_fu_12793_p1 = esl_zext<64,12>(tmp_755_fu_12788_p2.read());
}

void mnist_fp16_opt::thread_tmp_796_fu_11686_p1() {
    tmp_796_fu_11686_p1 = p_v1_fu_11679_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_797_cast_fu_12823_p1() {
    tmp_797_cast_fu_12823_p1 = esl_zext<64,12>(tmp_756_fu_12818_p2.read());
}

void mnist_fp16_opt::thread_tmp_797_fu_11696_p1() {
    tmp_797_fu_11696_p1 = p_v1_fu_11679_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_798_cast_fu_12833_p1() {
    tmp_798_cast_fu_12833_p1 = esl_zext<64,12>(tmp_757_fu_12828_p2.read());
}

void mnist_fp16_opt::thread_tmp_798_fu_11700_p3() {
    tmp_798_fu_11700_p3 = (!tmp_784_reg_24975_pp10_iter1_reg.read()[0].is_01())? sc_lv<2>(): ((tmp_784_reg_24975_pp10_iter1_reg.read()[0].to_bool())? neg_ti10_fu_11690_p2.read(): tmp_797_fu_11696_p1.read());
}

void mnist_fp16_opt::thread_tmp_799_cast_fu_12843_p1() {
    tmp_799_cast_fu_12843_p1 = esl_zext<64,12>(tmp_758_fu_12838_p2.read());
}

void mnist_fp16_opt::thread_tmp_799_fu_12082_p3() {
    tmp_799_fu_12082_p3 = esl_concat<2,4>(tmp_798_reg_25181.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_79_fu_9619_p2() {
    tmp_79_fu_9619_p2 = (!ap_const_lv16_0.is_01() || !p_Val2_18_reg_2797.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(p_Val2_18_reg_2797.read()));
}

void mnist_fp16_opt::thread_tmp_7_fu_4573_p3() {
    tmp_7_fu_4573_p3 = esl_concat<5,2>(p_0_reg_2156.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_7_mid2_cast_fu_6960_p1() {
    tmp_7_mid2_cast_fu_6960_p1 = esl_zext<6,3>(tmp_7_mid2_v_fu_6953_p3.read());
}

void mnist_fp16_opt::thread_tmp_7_mid2_v_fu_6953_p3() {
    tmp_7_mid2_v_fu_6953_p3 = (!exitcond_flatten2_reg_23809.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten2_reg_23809.read()[0].to_bool())? ff_V_fu_6947_p2.read(): p_2_reg_2476.read());
}

void mnist_fp16_opt::thread_tmp_800_cast_fu_12853_p1() {
    tmp_800_cast_fu_12853_p1 = esl_zext<64,12>(tmp_759_fu_12848_p2.read());
}

void mnist_fp16_opt::thread_tmp_800_fu_12093_p3() {
    tmp_800_fu_12093_p3 = esl_concat<2,1>(tmp_798_reg_25181.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_801_cast_fu_11115_p1() {
    tmp_801_cast_fu_11115_p1 = esl_zext<64,12>(tmp_760_fu_11109_p2.read());
}

void mnist_fp16_opt::thread_tmp_801_fu_12104_p2() {
    tmp_801_fu_12104_p2 = (!p_shl68_cast_fu_12089_p1.read().is_01() || !p_shl69_cast_fu_12100_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl68_cast_fu_12089_p1.read()) - sc_biguint<7>(p_shl69_cast_fu_12100_p1.read()));
}

void mnist_fp16_opt::thread_tmp_802_fu_12114_p2() {
    tmp_802_fu_12114_p2 = (!tmp_790_fu_12079_p1.read().is_01() || !tmp_820_cast_fu_12110_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_790_fu_12079_p1.read()) + sc_bigint<8>(tmp_820_cast_fu_12110_p1.read()));
}

void mnist_fp16_opt::thread_tmp_803_fu_12120_p1() {
    tmp_803_fu_12120_p1 = tmp_802_fu_12114_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_804_fu_12132_p3() {
    tmp_804_fu_12132_p3 = esl_concat<8,1>(tmp_802_fu_12114_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_805_fu_12144_p2() {
    tmp_805_fu_12144_p2 = (!p_shl70_cast_fu_12124_p3.read().is_01() || !p_shl71_cast_fu_12140_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl70_cast_fu_12124_p3.read()) - sc_bigint<11>(p_shl71_cast_fu_12140_p1.read()));
}

void mnist_fp16_opt::thread_tmp_806_fu_12155_p2() {
    tmp_806_fu_12155_p2 = (tmp_805_fu_12144_p2.read() | ap_const_lv11_1);
}

void mnist_fp16_opt::thread_tmp_807_fu_5325_p1() {
    tmp_807_fu_5325_p1 = mul11_fu_22748_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_808_cast_fu_10716_p1() {
    tmp_808_cast_fu_10716_p1 = esl_zext<64,13>(tmp_767_fu_10710_p2.read());
}

void mnist_fp16_opt::thread_tmp_809_fu_5342_p4() {
    tmp_809_fu_5342_p4 = neg_mul11_fu_5337_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_80_cast_fu_6342_p1() {
    tmp_80_cast_fu_6342_p1 = esl_sext<64,11>(tmp_49_fu_6337_p2.read());
}

void mnist_fp16_opt::thread_tmp_80_fu_6855_p2() {
    tmp_80_fu_6855_p2 = (!tmp_19_reg_23186_pp3_iter2_reg.read().is_01() || !ap_const_lv11_1B.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter2_reg.read()) + sc_biguint<11>(ap_const_lv11_1B));
}

void mnist_fp16_opt::thread_tmp_80_mid2_cast_fu_8923_p1() {
    tmp_80_mid2_cast_fu_8923_p1 = esl_zext<10,5>(tmp_674_fu_8915_p3.read());
}

void mnist_fp16_opt::thread_tmp_810_fu_5352_p1() {
    tmp_810_fu_5352_p1 = esl_sext<12,8>(tmp_809_fu_5342_p4.read());
}

void mnist_fp16_opt::thread_tmp_812_fu_5356_p1() {
    tmp_812_fu_5356_p1 = esl_sext<12,9>(tmp_811_reg_23400.read());
}

void mnist_fp16_opt::thread_tmp_813_fu_5359_p3() {
    tmp_813_fu_5359_p3 = (!tmp_808_reg_23394.read()[0].is_01())? sc_lv<12>(): ((tmp_808_reg_23394.read()[0].to_bool())? tmp_810_fu_5352_p1.read(): tmp_812_fu_5356_p1.read());
}

void mnist_fp16_opt::thread_tmp_814_fu_6227_p1() {
    tmp_814_fu_6227_p1 = grp_fu_5379_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_815_fu_6239_p1() {
    tmp_815_fu_6239_p1 = grp_fu_5379_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_816_fu_6251_p2() {
    tmp_816_fu_6251_p2 = (!p_shl72_cast_fu_6231_p3.read().is_01() || !p_shl73_cast_fu_6243_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl72_cast_fu_6231_p3.read()) - sc_biguint<11>(p_shl73_cast_fu_6243_p3.read()));
}

void mnist_fp16_opt::thread_tmp_817_fu_6277_p2() {
    tmp_817_fu_6277_p2 = (!ap_const_lv11_E.is_01() || !tmp_816_reg_23658.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_E) + sc_biguint<11>(tmp_816_reg_23658.read()));
}

void mnist_fp16_opt::thread_tmp_818_fu_6287_p2() {
    tmp_818_fu_6287_p2 = (!ap_const_lv11_F.is_01() || !tmp_816_reg_23658.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_F) + sc_biguint<11>(tmp_816_reg_23658.read()));
}

void mnist_fp16_opt::thread_tmp_819_fu_12898_p3() {
    tmp_819_fu_12898_p3 = esl_concat<4,2>(tmp_74_mid2_v_fu_12890_p3.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_81_fu_6865_p2() {
    tmp_81_fu_6865_p2 = (!tmp_19_reg_23186_pp3_iter2_reg.read().is_01() || !ap_const_lv11_1C.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_reg_23186_pp3_iter2_reg.read()) + sc_biguint<11>(ap_const_lv11_1C));
}

void mnist_fp16_opt::thread_tmp_81_mid2_cast_fu_8945_p1() {
    tmp_81_mid2_cast_fu_8945_p1 = esl_zext<64,2>(tmp_81_mid2_fu_8937_p3.read());
}

void mnist_fp16_opt::thread_tmp_81_mid2_fu_8937_p3() {
    tmp_81_mid2_fu_8937_p3 = (!exitcond26_mid_fu_8873_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond26_mid_fu_8873_p2.read()[0].to_bool())? ry1_V_fu_8879_p2.read(): p_21_mid_fu_8780_p3.read());
}

void mnist_fp16_opt::thread_tmp_820_cast_fu_12110_p1() {
    tmp_820_cast_fu_12110_p1 = esl_sext<8,7>(tmp_801_fu_12104_p2.read());
}

}

