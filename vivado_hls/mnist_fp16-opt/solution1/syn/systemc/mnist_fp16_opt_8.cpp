#include "mnist_fp16_opt.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp16_opt::thread_tmp_820_fu_12910_p3() {
    tmp_820_fu_12910_p3 = esl_concat<4,4>(tmp_74_mid2_v_fu_12890_p3.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_821_fu_12922_p3() {
    tmp_821_fu_12922_p3 = esl_concat<4,1>(tmp_74_mid2_v_fu_12890_p3.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_822_fu_12934_p2() {
    tmp_822_fu_12934_p2 = (!p_shl74_cast_fu_12918_p1.read().is_01() || !p_shl75_cast_fu_12930_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl74_cast_fu_12918_p1.read()) - sc_biguint<9>(p_shl75_cast_fu_12930_p1.read()));
}

void mnist_fp16_opt::thread_tmp_823_fu_12968_p2() {
    tmp_823_fu_12968_p2 = (exitcond30_mid_fu_12956_p2.read() | exitcond_flatten18_fu_12876_p2.read());
}

void mnist_fp16_opt::thread_tmp_824_cast_fu_12150_p1() {
    tmp_824_cast_fu_12150_p1 = esl_zext<64,11>(tmp_805_fu_12144_p2.read());
}

void mnist_fp16_opt::thread_tmp_824_fu_12994_p2() {
    tmp_824_fu_12994_p2 = (!tmp_838_cast_fu_12940_p1.read().is_01() || !tmp_95_mid2_cast_fu_12990_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_838_cast_fu_12940_p1.read()) + sc_biguint<10>(tmp_95_mid2_cast_fu_12990_p1.read()));
}

void mnist_fp16_opt::thread_tmp_825_cast_fu_12161_p1() {
    tmp_825_cast_fu_12161_p1 = esl_zext<64,11>(tmp_806_fu_12155_p2.read());
}

void mnist_fp16_opt::thread_tmp_825_fu_13000_p1() {
    tmp_825_fu_13000_p1 = tmp_824_fu_12994_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_826_fu_13012_p3() {
    tmp_826_fu_13012_p3 = esl_concat<10,1>(tmp_824_fu_12994_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_827_fu_13024_p2() {
    tmp_827_fu_13024_p2 = (!p_shl76_cast_fu_13004_p3.read().is_01() || !p_shl77_cast_fu_13020_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl76_cast_fu_13004_p3.read()) - sc_bigint<12>(p_shl77_cast_fu_13020_p1.read()));
}

void mnist_fp16_opt::thread_tmp_828_fu_5019_p2() {
    tmp_828_fu_5019_p2 = (!p_s_reg_2273.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_2273.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_829_fu_5025_p2() {
    tmp_829_fu_5025_p2 = (tmp_828_fu_5019_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_82_fu_8536_p2() {
    tmp_82_fu_8536_p2 = (!p_Result_9_fu_8526_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_9_fu_8526_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16_opt::thread_tmp_830_fu_5030_p2() {
    tmp_830_fu_5030_p2 = (tmp_11_reg_23152.read() | tmp_829_fu_5025_p2.read());
}

void mnist_fp16_opt::thread_tmp_831_fu_5035_p2() {
    tmp_831_fu_5035_p2 = (tmp_25_reg_23169.read() | tmp_830_fu_5030_p2.read());
}

void mnist_fp16_opt::thread_tmp_832_cast_fu_6282_p1() {
    tmp_832_cast_fu_6282_p1 = esl_sext<64,11>(tmp_817_fu_6277_p2.read());
}

void mnist_fp16_opt::thread_tmp_832_fu_11207_p1() {
    tmp_832_fu_11207_p1 = mul12_fu_22861_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_833_cast_fu_6292_p1() {
    tmp_833_cast_fu_6292_p1 = esl_sext<64,11>(tmp_818_fu_6287_p2.read());
}

void mnist_fp16_opt::thread_tmp_834_fu_11272_p4() {
    tmp_834_fu_11272_p4 = neg_mul12_fu_11267_p2.read().range(24, 16);
}

void mnist_fp16_opt::thread_tmp_835_cast_fu_12906_p1() {
    tmp_835_cast_fu_12906_p1 = esl_zext<7,6>(tmp_819_fu_12898_p3.read());
}

void mnist_fp16_opt::thread_tmp_835_fu_11282_p1() {
    tmp_835_fu_11282_p1 = esl_sext<12,9>(tmp_834_fu_11272_p4.read());
}

void mnist_fp16_opt::thread_tmp_837_fu_11286_p1() {
    tmp_837_fu_11286_p1 = esl_sext<12,10>(tmp_836_reg_25006.read());
}

void mnist_fp16_opt::thread_tmp_838_cast_fu_12940_p1() {
    tmp_838_cast_fu_12940_p1 = esl_sext<10,9>(tmp_822_fu_12934_p2.read());
}

void mnist_fp16_opt::thread_tmp_838_fu_11289_p3() {
    tmp_838_fu_11289_p3 = (!tmp_833_reg_24998.read()[0].is_01())? sc_lv<12>(): ((tmp_833_reg_24998.read()[0].to_bool())? tmp_835_fu_11282_p1.read(): tmp_837_fu_11286_p1.read());
}

void mnist_fp16_opt::thread_tmp_839_fu_12166_p1() {
    tmp_839_fu_12166_p1 = grp_fu_11341_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_83_cast_fu_6402_p1() {
    tmp_83_cast_fu_6402_p1 = esl_sext<64,11>(tmp_53_fu_6397_p2.read());
}

void mnist_fp16_opt::thread_tmp_83_fu_4695_p2() {
    tmp_83_fu_4695_p2 = (!tmp_19_fu_4684_p2.read().is_01() || !ap_const_lv11_1D.is_01())? sc_lv<11>(): (sc_bigint<11>(tmp_19_fu_4684_p2.read()) + sc_biguint<11>(ap_const_lv11_1D));
}

void mnist_fp16_opt::thread_tmp_840_fu_11707_p1() {
    tmp_840_fu_11707_p1 = mul13_fu_22916_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_841_fu_11736_p4() {
    tmp_841_fu_11736_p4 = neg_mul13_fu_11731_p2.read().range(24, 20);
}

void mnist_fp16_opt::thread_tmp_842_fu_11746_p1() {
    tmp_842_fu_11746_p1 = esl_sext<13,5>(tmp_841_fu_11736_p4.read());
}

void mnist_fp16_opt::thread_tmp_844_fu_11750_p1() {
    tmp_844_fu_11750_p1 = esl_sext<13,6>(tmp_843_reg_25192.read());
}

void mnist_fp16_opt::thread_tmp_845_fu_11760_p1() {
    tmp_845_fu_11760_p1 = p_v2_fu_11753_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_846_fu_11770_p1() {
    tmp_846_fu_11770_p1 = p_v2_fu_11753_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_847_fu_11774_p3() {
    tmp_847_fu_11774_p3 = (!tmp_833_reg_24998_pp10_iter1_reg.read()[0].is_01())? sc_lv<2>(): ((tmp_833_reg_24998_pp10_iter1_reg.read()[0].to_bool())? neg_ti13_fu_11764_p2.read(): tmp_846_fu_11770_p1.read());
}

void mnist_fp16_opt::thread_tmp_848_fu_12170_p3() {
    tmp_848_fu_12170_p3 = esl_concat<2,4>(tmp_847_reg_25207.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_849_fu_12181_p3() {
    tmp_849_fu_12181_p3 = esl_concat<2,1>(tmp_847_reg_25207.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_84_cast_fu_6412_p1() {
    tmp_84_cast_fu_6412_p1 = esl_sext<64,11>(tmp_60_fu_6407_p2.read());
}

void mnist_fp16_opt::thread_tmp_84_fu_4724_p2() {
    tmp_84_fu_4724_p2 = (tmp_11_fu_4648_p2.read() | tmp_6_fu_4642_p2.read());
}

void mnist_fp16_opt::thread_tmp_850_fu_12192_p2() {
    tmp_850_fu_12192_p2 = (!p_shl78_cast_fu_12177_p1.read().is_01() || !p_shl79_cast_fu_12188_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl78_cast_fu_12177_p1.read()) - sc_biguint<7>(p_shl79_cast_fu_12188_p1.read()));
}

void mnist_fp16_opt::thread_tmp_851_fu_12202_p2() {
    tmp_851_fu_12202_p2 = (!tmp_839_fu_12166_p1.read().is_01() || !tmp_855_cast_fu_12198_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_839_fu_12166_p1.read()) + sc_bigint<8>(tmp_855_cast_fu_12198_p1.read()));
}

void mnist_fp16_opt::thread_tmp_852_fu_12208_p1() {
    tmp_852_fu_12208_p1 = tmp_851_fu_12202_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_853_fu_12220_p3() {
    tmp_853_fu_12220_p3 = esl_concat<8,1>(tmp_851_fu_12202_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_854_fu_12232_p2() {
    tmp_854_fu_12232_p2 = (!p_shl80_cast_fu_12212_p3.read().is_01() || !p_shl81_cast_fu_12228_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl80_cast_fu_12212_p3.read()) - sc_bigint<11>(p_shl81_cast_fu_12228_p1.read()));
}

void mnist_fp16_opt::thread_tmp_855_cast_fu_12198_p1() {
    tmp_855_cast_fu_12198_p1 = esl_sext<8,7>(tmp_850_fu_12192_p2.read());
}

void mnist_fp16_opt::thread_tmp_855_fu_12258_p2() {
    tmp_855_fu_12258_p2 = (!ap_const_lv11_2.is_01() || !tmp_854_reg_25298.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_2) + sc_biguint<11>(tmp_854_reg_25298.read()));
}

void mnist_fp16_opt::thread_tmp_856_fu_12268_p2() {
    tmp_856_fu_12268_p2 = (!ap_const_lv11_3.is_01() || !tmp_854_reg_25298.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_3) + sc_biguint<11>(tmp_854_reg_25298.read()));
}

void mnist_fp16_opt::thread_tmp_857_fu_5389_p1() {
    tmp_857_fu_5389_p1 = mul14_fu_22756_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_859_fu_5406_p4() {
    tmp_859_fu_5406_p4 = neg_mul14_fu_5401_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_860_cast_fu_12263_p1() {
    tmp_860_cast_fu_12263_p1 = esl_zext<64,11>(tmp_855_fu_12258_p2.read());
}

void mnist_fp16_opt::thread_tmp_860_fu_5416_p1() {
    tmp_860_fu_5416_p1 = esl_sext<12,8>(tmp_859_fu_5406_p4.read());
}

void mnist_fp16_opt::thread_tmp_861_cast_fu_12273_p1() {
    tmp_861_cast_fu_12273_p1 = esl_zext<64,11>(tmp_856_fu_12268_p2.read());
}

void mnist_fp16_opt::thread_tmp_862_fu_5420_p1() {
    tmp_862_fu_5420_p1 = esl_sext<12,9>(tmp_861_reg_23421.read());
}

void mnist_fp16_opt::thread_tmp_863_fu_5423_p3() {
    tmp_863_fu_5423_p3 = (!tmp_858_reg_23415.read()[0].is_01())? sc_lv<12>(): ((tmp_858_reg_23415.read()[0].to_bool())? tmp_860_fu_5416_p1.read(): tmp_862_fu_5420_p1.read());
}

void mnist_fp16_opt::thread_tmp_864_fu_6297_p1() {
    tmp_864_fu_6297_p1 = grp_fu_5443_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_865_fu_6309_p1() {
    tmp_865_fu_6309_p1 = grp_fu_5443_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_866_fu_6321_p2() {
    tmp_866_fu_6321_p2 = (!p_shl82_cast_fu_6301_p3.read().is_01() || !p_shl83_cast_fu_6313_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl82_cast_fu_6301_p3.read()) - sc_biguint<11>(p_shl83_cast_fu_6313_p3.read()));
}

void mnist_fp16_opt::thread_tmp_867_fu_6347_p2() {
    tmp_867_fu_6347_p2 = (!ap_const_lv11_10.is_01() || !tmp_866_reg_23674.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_10) + sc_biguint<11>(tmp_866_reg_23674.read()));
}

void mnist_fp16_opt::thread_tmp_868_cast_fu_6352_p1() {
    tmp_868_cast_fu_6352_p1 = esl_sext<64,11>(tmp_867_fu_6347_p2.read());
}

void mnist_fp16_opt::thread_tmp_868_fu_6357_p2() {
    tmp_868_fu_6357_p2 = (!ap_const_lv11_11.is_01() || !tmp_866_reg_23674.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_11) + sc_biguint<11>(tmp_866_reg_23674.read()));
}

void mnist_fp16_opt::thread_tmp_869_cast_fu_6362_p1() {
    tmp_869_cast_fu_6362_p1 = esl_sext<64,11>(tmp_868_fu_6357_p2.read());
}

void mnist_fp16_opt::thread_tmp_869_fu_13335_p3() {
    tmp_869_fu_13335_p3 = esl_concat<4,4>(tmp_134_mid2_v_fu_13327_p3.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_870_fu_13347_p3() {
    tmp_870_fu_13347_p3 = esl_concat<4,1>(tmp_134_mid2_v_fu_13327_p3.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_871_fu_13359_p2() {
    tmp_871_fu_13359_p2 = (!p_shl84_cast_fu_13343_p1.read().is_01() || !p_shl85_cast_fu_13355_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl84_cast_fu_13343_p1.read()) - sc_biguint<9>(p_shl85_cast_fu_13355_p1.read()));
}

void mnist_fp16_opt::thread_tmp_872_cast_fu_13365_p1() {
    tmp_872_cast_fu_13365_p1 = esl_sext<10,9>(tmp_871_fu_13359_p2.read());
}

void mnist_fp16_opt::thread_tmp_872_fu_13393_p2() {
    tmp_872_fu_13393_p2 = (exitcond48_mid_fu_13381_p2.read() | exitcond_flatten21_fu_13313_p2.read());
}

void mnist_fp16_opt::thread_tmp_873_fu_13419_p2() {
    tmp_873_fu_13419_p2 = (!tmp_150_mid2_cast_fu_13415_p1.read().is_01() || !tmp_872_cast_fu_13365_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_150_mid2_cast_fu_13415_p1.read()) + sc_bigint<10>(tmp_872_cast_fu_13365_p1.read()));
}

void mnist_fp16_opt::thread_tmp_874_fu_13425_p1() {
    tmp_874_fu_13425_p1 = tmp_873_fu_13419_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_875_fu_13456_p3() {
    tmp_875_fu_13456_p3 = esl_concat<10,1>(tmp_873_reg_25559.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_876_fu_13467_p2() {
    tmp_876_fu_13467_p2 = (!p_shl86_cast_fu_13449_p3.read().is_01() || !p_shl87_cast_fu_13463_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl86_cast_fu_13449_p3.read()) - sc_bigint<12>(p_shl87_cast_fu_13463_p1.read()));
}

void mnist_fp16_opt::thread_tmp_877_fu_13476_p2() {
    tmp_877_fu_13476_p2 = (!tmp_184_cast_fu_13473_p1.read().is_01() || !tmp_876_fu_13467_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_184_cast_fu_13473_p1.read()) + sc_biguint<12>(tmp_876_fu_13467_p2.read()));
}

void mnist_fp16_opt::thread_tmp_878_cast_fu_13482_p1() {
    tmp_878_cast_fu_13482_p1 = esl_zext<64,12>(tmp_877_fu_13476_p2.read());
}

void mnist_fp16_opt::thread_tmp_878_fu_13500_p1() {
    tmp_878_fu_13500_p1 = conv3_0_load_to_int_fu_13487_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_879_cast_fu_13272_p1() {
    tmp_879_cast_fu_13272_p1 = esl_zext<64,12>(tmp_879_fu_13267_p2.read());
}

void mnist_fp16_opt::thread_tmp_879_fu_13267_p2() {
    tmp_879_fu_13267_p2 = (!tmp_827_reg_25448.read().is_01() || !tmp_151_cast_fu_13264_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_827_reg_25448.read()) + sc_biguint<12>(tmp_151_cast_fu_13264_p1.read()));
}

void mnist_fp16_opt::thread_tmp_87_cast_fu_6472_p1() {
    tmp_87_cast_fu_6472_p1 = esl_sext<64,11>(tmp_61_fu_6467_p2.read());
}

void mnist_fp16_opt::thread_tmp_87_fu_4730_p2() {
    tmp_87_fu_4730_p2 = (tmp_25_fu_4654_p2.read() | tmp_84_fu_4724_p2.read());
}

void mnist_fp16_opt::thread_tmp_880_cast_fu_13088_p1() {
    tmp_880_cast_fu_13088_p1 = esl_zext<64,7>(tmp_880_fu_13083_p2.read());
}

void mnist_fp16_opt::thread_tmp_880_fu_13083_p2() {
    tmp_880_fu_13083_p2 = (!tmp_152_mid2_cast_fu_13079_p1.read().is_01() || !tmp_835_cast_reg_25428.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_152_mid2_cast_fu_13079_p1.read()) + sc_biguint<7>(tmp_835_cast_reg_25428.read()));
}

void mnist_fp16_opt::thread_tmp_881_fu_13537_p1() {
    tmp_881_fu_13537_p1 = ireg_V_4_fu_13529_p3.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_882_fu_13104_p2() {
    tmp_882_fu_13104_p2 = (!p_shl2_fu_13100_p1.read().is_01() || !tmp_880_cast_fu_13088_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl2_fu_13100_p1.read()) - sc_biguint<64>(tmp_880_cast_fu_13088_p1.read()));
}

void mnist_fp16_opt::thread_tmp_883_fu_13141_p2() {
    tmp_883_fu_13141_p2 = (exitcond41_mid_fu_13129_p2.read() | exitcond_flatten22_fu_13057_p2.read());
}

void mnist_fp16_opt::thread_tmp_884_fu_13184_p2() {
    tmp_884_fu_13184_p2 = (!tmp_187_mid2_cast_fu_13180_p1.read().is_01() || !tmp_882_fu_13104_p2.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_187_mid2_cast_fu_13180_p1.read()) + sc_biguint<64>(tmp_882_fu_13104_p2.read()));
}

void mnist_fp16_opt::thread_tmp_886_fu_13219_p2() {
    tmp_886_fu_13219_p2 = (!p_shl89_cast_fu_13212_p3.read().is_01() || !tmp_906_reg_25485.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl89_cast_fu_13212_p3.read()) - sc_biguint<10>(tmp_906_reg_25485.read()));
}

void mnist_fp16_opt::thread_tmp_887_fu_13232_p4() {
    tmp_887_fu_13232_p4 = esl_concat<7,4>(esl_concat<3,4>(tmp_152_mid2_v_reg_25462.read(), tmp_186_mid2_v_reg_25475.read()), r_V_11_fu_13227_p2.read());
}

void mnist_fp16_opt::thread_tmp_888_fu_13240_p1() {
    tmp_888_fu_13240_p1 = esl_zext<64,11>(tmp_887_fu_13232_p4.read());
}

void mnist_fp16_opt::thread_tmp_889_cast_fu_13254_p1() {
    tmp_889_cast_fu_13254_p1 = esl_zext<64,10>(tmp_889_fu_13248_p2.read());
}

void mnist_fp16_opt::thread_tmp_889_fu_13248_p2() {
    tmp_889_fu_13248_p2 = (!tmp_214_cast_fu_13245_p1.read().is_01() || !tmp_886_fu_13219_p2.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_214_cast_fu_13245_p1.read()) + sc_biguint<10>(tmp_886_fu_13219_p2.read()));
}

void mnist_fp16_opt::thread_tmp_88_fu_5725_p1() {
    tmp_88_fu_5725_p1 = mul_fu_22804_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_890_fu_13559_p1() {
    tmp_890_fu_13559_p1 = ireg_V_4_fu_13529_p3.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_891_fu_13634_p1() {
    tmp_891_fu_13634_p1 = man_V_7_fu_13589_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_892_fu_11361_p1() {
    tmp_892_fu_11361_p1 = esl_sext<12,9>(tmp_935_fu_11351_p4.read());
}

void mnist_fp16_opt::thread_tmp_893_fu_11365_p1() {
    tmp_893_fu_11365_p1 = esl_sext<12,10>(tmp_936_reg_25039.read());
}

void mnist_fp16_opt::thread_tmp_894_fu_11368_p3() {
    tmp_894_fu_11368_p3 = (!tmp_934_reg_25031.read()[0].is_01())? sc_lv<12>(): ((tmp_934_reg_25031.read()[0].to_bool())? tmp_892_fu_11361_p1.read(): tmp_893_fu_11365_p1.read());
}

void mnist_fp16_opt::thread_tmp_895_fu_11796_p1() {
    tmp_895_fu_11796_p1 = esl_sext<13,5>(tmp_950_fu_11786_p4.read());
}

void mnist_fp16_opt::thread_tmp_896_fu_11800_p1() {
    tmp_896_fu_11800_p1 = esl_sext<13,6>(tmp_951_reg_25202.read());
}

void mnist_fp16_opt::thread_tmp_897_fu_13638_p4() {
    tmp_897_fu_13638_p4 = sh_amt_4_fu_13620_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_898_fu_13706_p1() {
    tmp_898_fu_13706_p1 = tmp_463_fu_13701_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_899_fu_13726_p1() {
    tmp_899_fu_13726_p1 = tmp_480_fu_13720_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_900_fu_11824_p3() {
    tmp_900_fu_11824_p3 = (!tmp_934_reg_25031_pp10_iter1_reg.read()[0].is_01())? sc_lv<2>(): ((tmp_934_reg_25031_pp10_iter1_reg.read()[0].to_bool())? neg_ti16_fu_11814_p2.read(): tmp_958_fu_11820_p1.read());
}

void mnist_fp16_opt::thread_tmp_901_fu_12282_p3() {
    tmp_901_fu_12282_p3 = esl_concat<2,4>(tmp_900_reg_25213.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_902_fu_12293_p3() {
    tmp_902_fu_12293_p3 = esl_concat<2,1>(tmp_900_reg_25213.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_903_cast_fu_12310_p1() {
    tmp_903_cast_fu_12310_p1 = esl_sext<8,7>(tmp_903_fu_12304_p2.read());
}

void mnist_fp16_opt::thread_tmp_903_fu_12304_p2() {
    tmp_903_fu_12304_p2 = (!p_shl90_cast_fu_12289_p1.read().is_01() || !p_shl91_cast_fu_12300_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl90_cast_fu_12289_p1.read()) - sc_biguint<7>(p_shl91_cast_fu_12300_p1.read()));
}

void mnist_fp16_opt::thread_tmp_904_fu_12314_p2() {
    tmp_904_fu_12314_p2 = (!tmp_941_fu_12278_p1.read().is_01() || !tmp_903_cast_fu_12310_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_941_fu_12278_p1.read()) + sc_bigint<8>(tmp_903_cast_fu_12310_p1.read()));
}

void mnist_fp16_opt::thread_tmp_905_fu_13092_p3() {
    tmp_905_fu_13092_p3 = esl_concat<7,2>(tmp_880_fu_13083_p2.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_906_fu_13190_p1() {
    tmp_906_fu_13190_p1 = tmp_884_fu_13184_p2.read().range(10-1, 0);
}

void mnist_fp16_opt::thread_tmp_907_fu_12344_p2() {
    tmp_907_fu_12344_p2 = (!p_shl92_cast_fu_12324_p3.read().is_01() || !p_shl93_cast_fu_12340_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl92_cast_fu_12324_p3.read()) - sc_bigint<11>(p_shl93_cast_fu_12340_p1.read()));
}

void mnist_fp16_opt::thread_tmp_908_cast_fu_12375_p1() {
    tmp_908_cast_fu_12375_p1 = esl_zext<64,11>(tmp_908_fu_12370_p2.read());
}

void mnist_fp16_opt::thread_tmp_908_fu_12370_p2() {
    tmp_908_fu_12370_p2 = (!ap_const_lv11_4.is_01() || !tmp_907_reg_25314.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_4) + sc_biguint<11>(tmp_907_reg_25314.read()));
}

void mnist_fp16_opt::thread_tmp_909_cast_fu_12385_p1() {
    tmp_909_cast_fu_12385_p1 = esl_zext<64,11>(tmp_909_fu_12380_p2.read());
}

void mnist_fp16_opt::thread_tmp_909_fu_12380_p2() {
    tmp_909_fu_12380_p2 = (!ap_const_lv11_5.is_01() || !tmp_907_reg_25314.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_5) + sc_biguint<11>(tmp_907_reg_25314.read()));
}

void mnist_fp16_opt::thread_tmp_90_fu_5790_p4() {
    tmp_90_fu_5790_p4 = neg_mul_fu_5785_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_910_fu_5480_p1() {
    tmp_910_fu_5480_p1 = esl_sext<12,8>(tmp_978_fu_5470_p4.read());
}

void mnist_fp16_opt::thread_tmp_911_fu_5484_p1() {
    tmp_911_fu_5484_p1 = esl_sext<12,9>(tmp_979_reg_23442.read());
}

void mnist_fp16_opt::thread_tmp_912_fu_5487_p3() {
    tmp_912_fu_5487_p3 = (!tmp_972_reg_23436.read()[0].is_01())? sc_lv<12>(): ((tmp_972_reg_23436.read()[0].to_bool())? tmp_910_fu_5480_p1.read(): tmp_911_fu_5484_p1.read());
}

void mnist_fp16_opt::thread_tmp_913_fu_13194_p1() {
    tmp_913_fu_13194_p1 = tmp_884_fu_13184_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_914_fu_5040_p2() {
    tmp_914_fu_5040_p2 = (!p_s_reg_2273.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_2273.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_915_fu_6391_p2() {
    tmp_915_fu_6391_p2 = (!p_shl94_cast_fu_6371_p3.read().is_01() || !p_shl95_cast_fu_6383_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl94_cast_fu_6371_p3.read()) - sc_biguint<11>(p_shl95_cast_fu_6383_p3.read()));
}

void mnist_fp16_opt::thread_tmp_916_cast_fu_6422_p1() {
    tmp_916_cast_fu_6422_p1 = esl_sext<64,11>(tmp_916_fu_6417_p2.read());
}

void mnist_fp16_opt::thread_tmp_916_fu_6417_p2() {
    tmp_916_fu_6417_p2 = (!ap_const_lv11_12.is_01() || !tmp_915_reg_23690.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_12) + sc_biguint<11>(tmp_915_reg_23690.read()));
}

void mnist_fp16_opt::thread_tmp_917_cast_fu_6432_p1() {
    tmp_917_cast_fu_6432_p1 = esl_sext<64,11>(tmp_917_fu_6427_p2.read());
}

void mnist_fp16_opt::thread_tmp_917_fu_6427_p2() {
    tmp_917_fu_6427_p2 = (!ap_const_lv11_13.is_01() || !tmp_915_reg_23690.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_13) + sc_biguint<11>(tmp_915_reg_23690.read()));
}

void mnist_fp16_opt::thread_tmp_918_fu_14072_p3() {
    tmp_918_fu_14072_p3 = esl_concat<4,4>(lhs_V_7_mid2_v_reg_25686.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_919_cast_fu_14079_p1() {
    tmp_919_cast_fu_14079_p1 = esl_zext<9,8>(tmp_918_fu_14072_p3.read());
}

void mnist_fp16_opt::thread_tmp_919_fu_5046_p2() {
    tmp_919_fu_5046_p2 = (tmp_914_fu_5040_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_91_fu_7495_p1() {
    tmp_91_fu_7495_p1 = esl_zext<12,11>(p_Result_2_reg_23993.read());
}

void mnist_fp16_opt::thread_tmp_920_fu_13962_p2() {
    tmp_920_fu_13962_p2 = (exitcond36_mid_fu_13950_p2.read() | exitcond_flatten24_fu_13892_p2.read());
}

void mnist_fp16_opt::thread_tmp_921_fu_14091_p2() {
    tmp_921_fu_14091_p2 = (!tmp_919_cast_fu_14079_p1.read().is_01() || !lhs_V_8_mid2_cast_fu_14088_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_919_cast_fu_14079_p1.read()) + sc_biguint<9>(lhs_V_8_mid2_cast_fu_14088_p1.read()));
}

void mnist_fp16_opt::thread_tmp_922_fu_5051_p2() {
    tmp_922_fu_5051_p2 = (tmp_11_reg_23152.read() | tmp_919_fu_5046_p2.read());
}

void mnist_fp16_opt::thread_tmp_923_cast_fu_14097_p3() {
    tmp_923_cast_fu_14097_p3 = esl_concat<9,4>(tmp_921_fu_14091_p2.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_923_fu_5056_p2() {
    tmp_923_fu_5056_p2 = (tmp_25_reg_23169.read() | tmp_922_fu_5051_p2.read());
}

void mnist_fp16_opt::thread_tmp_924_fu_13826_p1() {
    tmp_924_fu_13826_p1 = ap_phi_mux_p_31_phi_fu_3316_p4.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_925_fu_13842_p2() {
    tmp_925_fu_13842_p2 = (!ap_const_lv5_1.is_01())? sc_lv<5>(): ap_phi_mux_p_31_phi_fu_3316_p4.read() << (unsigned short)ap_const_lv5_1.to_uint();
}

void mnist_fp16_opt::thread_tmp_926_fu_14141_p2() {
    tmp_926_fu_14141_p2 = (!r_V_12_fu_14135_p2.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): (sc_biguint<4>(r_V_12_fu_14135_p2.read()) < sc_biguint<4>(ap_const_lv4_E));
}

void mnist_fp16_opt::thread_tmp_927_fu_14147_p2() {
    tmp_927_fu_14147_p2 = (!ap_const_lv4_1.is_01() || !tmp_1014_fu_14132_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_1014_fu_14132_p1.read()));
}

void mnist_fp16_opt::thread_tmp_928_fu_14248_p1() {
    tmp_928_fu_14248_p1 = esl_sext<12,9>(tmp_1033_fu_14238_p4.read());
}

void mnist_fp16_opt::thread_tmp_929_fu_14252_p1() {
    tmp_929_fu_14252_p1 = esl_sext<12,10>(tmp_1034_reg_25774.read());
}

void mnist_fp16_opt::thread_tmp_930_fu_14255_p3() {
    tmp_930_fu_14255_p3 = (!tmp_1023_reg_25766.read()[0].is_01())? sc_lv<12>(): ((tmp_1023_reg_25766.read()[0].to_bool())? tmp_928_fu_14248_p1.read(): tmp_929_fu_14252_p1.read());
}

void mnist_fp16_opt::thread_tmp_931_fu_14296_p1() {
    tmp_931_fu_14296_p1 = esl_sext<13,5>(tmp_1042_fu_14286_p4.read());
}

void mnist_fp16_opt::thread_tmp_932_fu_14300_p1() {
    tmp_932_fu_14300_p1 = esl_sext<13,6>(tmp_1049_reg_25784.read());
}

void mnist_fp16_opt::thread_tmp_933_fu_11313_p1() {
    tmp_933_fu_11313_p1 = mul15_fu_22869_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_935_fu_11351_p4() {
    tmp_935_fu_11351_p4 = neg_mul15_fu_11346_p2.read().range(24, 16);
}

void mnist_fp16_opt::thread_tmp_937_fu_14335_p3() {
    tmp_937_fu_14335_p3 = esl_concat<3,4>(r_V_13_reg_25794_pp13_iter17_reg.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_938_fu_14346_p3() {
    tmp_938_fu_14346_p3 = esl_concat<3,1>(r_V_13_reg_25794_pp13_iter17_reg.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_939_cast_fu_14363_p1() {
    tmp_939_cast_fu_14363_p1 = esl_sext<9,8>(tmp_939_fu_14357_p2.read());
}

void mnist_fp16_opt::thread_tmp_939_fu_14357_p2() {
    tmp_939_fu_14357_p2 = (!p_shl96_cast_fu_14342_p1.read().is_01() || !p_shl97_cast_fu_14353_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl96_cast_fu_14342_p1.read()) - sc_biguint<8>(p_shl97_cast_fu_14353_p1.read()));
}

void mnist_fp16_opt::thread_tmp_93_cast_fu_8146_p1() {
    tmp_93_cast_fu_8146_p1 = esl_zext<13,5>(p_12_mid2_reg_24091.read());
}

void mnist_fp16_opt::thread_tmp_93_fu_5800_p1() {
    tmp_93_fu_5800_p1 = esl_sext<12,8>(tmp_90_fu_5790_p4.read());
}

void mnist_fp16_opt::thread_tmp_940_fu_14367_p2() {
    tmp_940_fu_14367_p2 = (!tmp_1035_fu_14331_p1.read().is_01() || !tmp_939_cast_fu_14363_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_1035_fu_14331_p1.read()) + sc_bigint<9>(tmp_939_cast_fu_14363_p1.read()));
}

void mnist_fp16_opt::thread_tmp_941_fu_12278_p1() {
    tmp_941_fu_12278_p1 = grp_fu_11474_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_942_fu_11719_p1() {
    tmp_942_fu_11719_p1 = mul16_fu_22923_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_943_fu_14398_p2() {
    tmp_943_fu_14398_p2 = (!p_shl98_cast_fu_14380_p3.read().is_01() || !p_shl99_cast_fu_14394_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl98_cast_fu_14380_p3.read()) - sc_bigint<12>(p_shl99_cast_fu_14394_p1.read()));
}

void mnist_fp16_opt::thread_tmp_944_cast_fu_14410_p1() {
    tmp_944_cast_fu_14410_p1 = esl_zext<64,12>(tmp_944_fu_14404_p2.read());
}

void mnist_fp16_opt::thread_tmp_944_fu_14404_p2() {
    tmp_944_fu_14404_p2 = (!tmp_191_cast_fu_14377_p1.read().is_01() || !tmp_943_fu_14398_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_191_cast_fu_14377_p1.read()) + sc_biguint<12>(tmp_943_fu_14398_p2.read()));
}

void mnist_fp16_opt::thread_tmp_945_fu_11403_p1() {
    tmp_945_fu_11403_p1 = esl_sext<12,9>(tmp_1078_fu_11393_p4.read());
}

void mnist_fp16_opt::thread_tmp_946_fu_11407_p1() {
    tmp_946_fu_11407_p1 = esl_sext<12,10>(tmp_1085_reg_25062.read());
}

void mnist_fp16_opt::thread_tmp_947_fu_11410_p3() {
    tmp_947_fu_11410_p3 = (!tmp_1077_reg_25054.read()[0].is_01())? sc_lv<12>(): ((tmp_1077_reg_25054.read()[0].to_bool())? tmp_945_fu_11403_p1.read(): tmp_946_fu_11407_p1.read());
}

void mnist_fp16_opt::thread_tmp_948_fu_11870_p1() {
    tmp_948_fu_11870_p1 = esl_sext<13,5>(tmp_1104_fu_11860_p4.read());
}

void mnist_fp16_opt::thread_tmp_949_fu_11874_p1() {
    tmp_949_fu_11874_p1 = esl_sext<13,6>(tmp_1105_reg_25224.read());
}

void mnist_fp16_opt::thread_tmp_94_fu_7489_p2() {
    tmp_94_fu_7489_p2 = (!tmp_207_fu_7463_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_207_fu_7463_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_950_fu_11786_p4() {
    tmp_950_fu_11786_p4 = neg_mul16_fu_11781_p2.read().range(24, 20);
}

void mnist_fp16_opt::thread_tmp_952_fu_11810_p1() {
    tmp_952_fu_11810_p1 = p_v3_fu_11803_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_953_fu_11898_p3() {
    tmp_953_fu_11898_p3 = (!tmp_1077_reg_25054_pp10_iter1_reg.read()[0].is_01())? sc_lv<2>(): ((tmp_1077_reg_25054_pp10_iter1_reg.read()[0].to_bool())? neg_ti21_fu_11888_p2.read(): tmp_1114_fu_11894_p1.read());
}

void mnist_fp16_opt::thread_tmp_954_fu_12394_p3() {
    tmp_954_fu_12394_p3 = esl_concat<2,4>(tmp_953_reg_25239.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_955_fu_12405_p3() {
    tmp_955_fu_12405_p3 = esl_concat<2,1>(tmp_953_reg_25239.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_956_cast_fu_12422_p1() {
    tmp_956_cast_fu_12422_p1 = esl_sext<8,7>(tmp_956_fu_12416_p2.read());
}

void mnist_fp16_opt::thread_tmp_956_fu_12416_p2() {
    tmp_956_fu_12416_p2 = (!p_shl100_cast_fu_12401_p1.read().is_01() || !p_shl101_cast_fu_12412_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl100_cast_fu_12401_p1.read()) - sc_biguint<7>(p_shl101_cast_fu_12412_p1.read()));
}

void mnist_fp16_opt::thread_tmp_957_fu_12426_p2() {
    tmp_957_fu_12426_p2 = (!tmp_1086_fu_12390_p1.read().is_01() || !tmp_956_cast_fu_12422_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_1086_fu_12390_p1.read()) + sc_bigint<8>(tmp_956_cast_fu_12422_p1.read()));
}

void mnist_fp16_opt::thread_tmp_958_fu_11820_p1() {
    tmp_958_fu_11820_p1 = p_v3_fu_11803_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_959_fu_12320_p1() {
    tmp_959_fu_12320_p1 = tmp_904_fu_12314_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_95_mid2_cast_fu_12990_p1() {
    tmp_95_mid2_cast_fu_12990_p1 = esl_zext<10,4>(tmp_95_mid2_fu_12982_p3.read());
}

void mnist_fp16_opt::thread_tmp_95_mid2_fu_12982_p3() {
    tmp_95_mid2_fu_12982_p3 = (!exitcond30_mid_fu_12956_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond30_mid_fu_12956_p2.read()[0].to_bool())? yy2_V_fu_12962_p2.read(): p_24_mid_fu_12882_p3.read());
}

void mnist_fp16_opt::thread_tmp_960_fu_12456_p2() {
    tmp_960_fu_12456_p2 = (!p_shl102_cast_fu_12436_p3.read().is_01() || !p_shl103_cast_fu_12452_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl102_cast_fu_12436_p3.read()) - sc_bigint<11>(p_shl103_cast_fu_12452_p1.read()));
}

void mnist_fp16_opt::thread_tmp_961_cast_fu_12487_p1() {
    tmp_961_cast_fu_12487_p1 = esl_zext<64,11>(tmp_961_fu_12482_p2.read());
}

void mnist_fp16_opt::thread_tmp_961_fu_12482_p2() {
    tmp_961_fu_12482_p2 = (!ap_const_lv11_6.is_01() || !tmp_960_reg_25330.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_6) + sc_biguint<11>(tmp_960_reg_25330.read()));
}

void mnist_fp16_opt::thread_tmp_962_cast_fu_12497_p1() {
    tmp_962_cast_fu_12497_p1 = esl_zext<64,11>(tmp_962_fu_12492_p2.read());
}

void mnist_fp16_opt::thread_tmp_962_fu_12492_p2() {
    tmp_962_fu_12492_p2 = (!ap_const_lv11_7.is_01() || !tmp_960_reg_25330.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_7) + sc_biguint<11>(tmp_960_reg_25330.read()));
}

void mnist_fp16_opt::thread_tmp_963_fu_5544_p1() {
    tmp_963_fu_5544_p1 = esl_sext<12,8>(tmp_1129_fu_5534_p4.read());
}

void mnist_fp16_opt::thread_tmp_964_fu_5548_p1() {
    tmp_964_fu_5548_p1 = esl_sext<12,9>(tmp_1136_reg_23463.read());
}

void mnist_fp16_opt::thread_tmp_965_fu_5551_p3() {
    tmp_965_fu_5551_p3 = (!tmp_1125_reg_23457.read()[0].is_01())? sc_lv<12>(): ((tmp_1125_reg_23457.read()[0].to_bool())? tmp_963_fu_5544_p1.read(): tmp_964_fu_5548_p1.read());
}

void mnist_fp16_opt::thread_tmp_966_fu_12332_p3() {
    tmp_966_fu_12332_p3 = esl_concat<8,1>(tmp_904_fu_12314_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_967_fu_5453_p1() {
    tmp_967_fu_5453_p1 = mul17_fu_22764_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_968_fu_6461_p2() {
    tmp_968_fu_6461_p2 = (!p_shl104_cast_fu_6441_p3.read().is_01() || !p_shl105_cast_fu_6453_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl104_cast_fu_6441_p3.read()) - sc_biguint<11>(p_shl105_cast_fu_6453_p3.read()));
}

void mnist_fp16_opt::thread_tmp_969_cast_fu_6492_p1() {
    tmp_969_cast_fu_6492_p1 = esl_sext<64,11>(tmp_969_fu_6487_p2.read());
}

void mnist_fp16_opt::thread_tmp_969_fu_6487_p2() {
    tmp_969_fu_6487_p2 = (!ap_const_lv11_14.is_01() || !tmp_968_reg_23706.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_14) + sc_biguint<11>(tmp_968_reg_23706.read()));
}

void mnist_fp16_opt::thread_tmp_96_fu_8558_p3() {
    tmp_96_fu_8558_p3 = esl_concat<1,8>(is_neg_reg_24220_pp6_iter30_reg.read(), p_Repl2_1_trunc_fu_8552_p2.read());
}

void mnist_fp16_opt::thread_tmp_970_cast_fu_6502_p1() {
    tmp_970_cast_fu_6502_p1 = esl_sext<64,11>(tmp_970_fu_6497_p2.read());
}

void mnist_fp16_opt::thread_tmp_970_fu_6497_p2() {
    tmp_970_fu_6497_p2 = (!ap_const_lv11_15.is_01() || !tmp_968_reg_23706.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_15) + sc_biguint<11>(tmp_968_reg_23706.read()));
}

void mnist_fp16_opt::thread_tmp_971_fu_14659_p3() {
    tmp_971_fu_14659_p3 = esl_concat<4,3>(tmp_147_mid2_v_fu_14651_p3.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_972_cast_fu_14667_p1() {
    tmp_972_cast_fu_14667_p1 = esl_zext<8,7>(tmp_971_fu_14659_p3.read());
}

void mnist_fp16_opt::thread_tmp_973_fu_14671_p3() {
    tmp_973_fu_14671_p3 = esl_concat<4,4>(tmp_147_mid2_v_fu_14651_p3.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_974_fu_14683_p3() {
    tmp_974_fu_14683_p3 = esl_concat<4,1>(tmp_147_mid2_v_fu_14651_p3.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_975_cast_fu_14701_p1() {
    tmp_975_cast_fu_14701_p1 = esl_sext<10,9>(tmp_975_fu_14695_p2.read());
}

void mnist_fp16_opt::thread_tmp_975_fu_14695_p2() {
    tmp_975_fu_14695_p2 = (!p_shl106_cast_fu_14679_p1.read().is_01() || !p_shl107_cast_fu_14691_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl106_cast_fu_14679_p1.read()) - sc_biguint<9>(p_shl107_cast_fu_14691_p1.read()));
}

void mnist_fp16_opt::thread_tmp_976_fu_14729_p2() {
    tmp_976_fu_14729_p2 = (exitcond40_mid_fu_14717_p2.read() | exitcond_flatten26_fu_14637_p2.read());
}

void mnist_fp16_opt::thread_tmp_977_fu_14755_p2() {
    tmp_977_fu_14755_p2 = (!tmp_975_cast_fu_14701_p1.read().is_01() || !tmp_181_mid2_cast_fu_14751_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_975_cast_fu_14701_p1.read()) + sc_biguint<10>(tmp_181_mid2_cast_fu_14751_p1.read()));
}

void mnist_fp16_opt::thread_tmp_978_fu_5470_p4() {
    tmp_978_fu_5470_p4 = neg_mul17_fu_5465_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_980_fu_14785_p2() {
    tmp_980_fu_14785_p2 = (!p_shl108_cast_fu_14765_p3.read().is_01() || !p_shl109_cast_fu_14781_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl108_cast_fu_14765_p3.read()) - sc_bigint<12>(p_shl109_cast_fu_14781_p1.read()));
}

void mnist_fp16_opt::thread_tmp_981_cast_fu_14615_p1() {
    tmp_981_cast_fu_14615_p1 = esl_zext<64,13>(tmp_981_reg_25756_pp13_iter28_reg.read());
}

void mnist_fp16_opt::thread_tmp_981_fu_14183_p2() {
    tmp_981_fu_14183_p2 = (!tmp_923_cast_fu_14097_p3.read().is_01() || !tmp_271_cast_fu_14180_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_923_cast_fu_14097_p3.read()) + sc_biguint<13>(tmp_271_cast_fu_14180_p1.read()));
}

void mnist_fp16_opt::thread_tmp_982_fu_11494_p1() {
    tmp_982_fu_11494_p1 = esl_sext<12,9>(tmp_1214_fu_11484_p4.read());
}

void mnist_fp16_opt::thread_tmp_983_fu_11498_p1() {
    tmp_983_fu_11498_p1 = esl_sext<12,10>(tmp_1219_reg_25095.read());
}

void mnist_fp16_opt::thread_tmp_984_fu_11501_p3() {
    tmp_984_fu_11501_p3 = (!tmp_1213_reg_25087.read()[0].is_01())? sc_lv<12>(): ((tmp_1213_reg_25087.read()[0].to_bool())? tmp_982_fu_11494_p1.read(): tmp_983_fu_11498_p1.read());
}

void mnist_fp16_opt::thread_tmp_985_fu_11920_p1() {
    tmp_985_fu_11920_p1 = esl_sext<13,5>(tmp_1235_fu_11910_p4.read());
}

void mnist_fp16_opt::thread_tmp_986_fu_11924_p1() {
    tmp_986_fu_11924_p1 = esl_sext<13,6>(tmp_1240_reg_25234.read());
}

void mnist_fp16_opt::thread_tmp_987_fu_6367_p1() {
    tmp_987_fu_6367_p1 = grp_fu_5507_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_988_fu_6379_p1() {
    tmp_988_fu_6379_p1 = grp_fu_5507_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_989_fu_13996_p1() {
    tmp_989_fu_13996_p1 = index_tuple3_V_fu_13956_p2.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_990_fu_11948_p3() {
    tmp_990_fu_11948_p3 = (!tmp_1213_reg_25087_pp10_iter1_reg.read()[0].is_01())? sc_lv<2>(): ((tmp_1213_reg_25087_pp10_iter1_reg.read()[0].to_bool())? neg_ti24_fu_11938_p2.read(): tmp_1249_fu_11944_p1.read());
}

void mnist_fp16_opt::thread_tmp_991_fu_12506_p3() {
    tmp_991_fu_12506_p3 = esl_concat<2,4>(tmp_990_reg_25245.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_992_fu_12517_p3() {
    tmp_992_fu_12517_p3 = esl_concat<2,1>(tmp_990_reg_25245.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_993_cast_fu_12534_p1() {
    tmp_993_cast_fu_12534_p1 = esl_sext<8,7>(tmp_993_fu_12528_p2.read());
}

void mnist_fp16_opt::thread_tmp_993_fu_12528_p2() {
    tmp_993_fu_12528_p2 = (!p_shl110_cast_fu_12513_p1.read().is_01() || !p_shl111_cast_fu_12524_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl110_cast_fu_12513_p1.read()) - sc_biguint<7>(p_shl111_cast_fu_12524_p1.read()));
}

void mnist_fp16_opt::thread_tmp_994_fu_12538_p2() {
    tmp_994_fu_12538_p2 = (!tmp_1223_fu_12502_p1.read().is_01() || !tmp_993_cast_fu_12534_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_1223_fu_12502_p1.read()) + sc_bigint<8>(tmp_993_cast_fu_12534_p1.read()));
}

void mnist_fp16_opt::thread_tmp_995_fu_14012_p2() {
    tmp_995_fu_14012_p2 = (!ap_const_lv5_1.is_01())? sc_lv<5>(): index_tuple3_V_fu_13956_p2.read() << (unsigned short)ap_const_lv5_1.to_uint();
}

void mnist_fp16_opt::thread_tmp_996_fu_5061_p2() {
    tmp_996_fu_5061_p2 = (!p_s_reg_2273.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_2273.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_997_fu_12568_p2() {
    tmp_997_fu_12568_p2 = (!p_shl112_cast_fu_12548_p3.read().is_01() || !p_shl113_cast_fu_12564_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl112_cast_fu_12548_p3.read()) - sc_bigint<11>(p_shl113_cast_fu_12564_p1.read()));
}

void mnist_fp16_opt::thread_tmp_998_cast_fu_12599_p1() {
    tmp_998_cast_fu_12599_p1 = esl_zext<64,11>(tmp_998_fu_12594_p2.read());
}

void mnist_fp16_opt::thread_tmp_998_fu_12594_p2() {
    tmp_998_fu_12594_p2 = (!ap_const_lv11_8.is_01() || !tmp_997_reg_25346.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_8) + sc_biguint<11>(tmp_997_reg_25346.read()));
}

void mnist_fp16_opt::thread_tmp_999_cast_fu_12609_p1() {
    tmp_999_cast_fu_12609_p1 = esl_zext<64,11>(tmp_999_fu_12604_p2.read());
}

void mnist_fp16_opt::thread_tmp_999_fu_12604_p2() {
    tmp_999_fu_12604_p2 = (!ap_const_lv11_9.is_01() || !tmp_997_reg_25346.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_9) + sc_biguint<11>(tmp_997_reg_25346.read()));
}

void mnist_fp16_opt::thread_tmp_V_2_fu_10740_p3() {
    tmp_V_2_fu_10740_p3 = (!tmp_768_reg_24823.read()[0].is_01())? sc_lv<16>(): ((tmp_768_reg_24823.read()[0].to_bool())? tmp_188_reg_24829.read(): relu2_0_V_load_reg_24817.read());
}

void mnist_fp16_opt::thread_tmp_V_5_fu_16695_p3() {
    tmp_V_5_fu_16695_p3 = (!tmp_1353_reg_26418.read()[0].is_01())? sc_lv<16>(): ((tmp_1353_reg_26418.read()[0].to_bool())? tmp_592_reg_26424.read(): relu4_0_V_load_reg_26412.read());
}

void mnist_fp16_opt::thread_tmp_V_8_fu_21605_p3() {
    tmp_V_8_fu_21605_p3 = (!tmp_1490_reg_27760.read()[0].is_01())? sc_lv<16>(): ((tmp_1490_reg_27760.read()[0].to_bool())? tmp_510_reg_27766.read(): relu6_0_V_load_reg_27754.read());
}

void mnist_fp16_opt::thread_tmp_V_9_fu_22260_p3() {
    tmp_V_9_fu_22260_p3 = (!tmp_1503_reg_27946.read()[0].is_01())? sc_lv<16>(): ((tmp_1503_reg_27946.read()[0].to_bool())? tmp_660_reg_27952.read(): pool3_0_V_load_reg_27940.read());
}

void mnist_fp16_opt::thread_tmp_mid2_cast_fu_7012_p1() {
    tmp_mid2_cast_fu_7012_p1 = esl_zext<10,5>(tmp_mid2_reg_23822.read());
}

void mnist_fp16_opt::thread_tmp_mid2_fu_6939_p3() {
    tmp_mid2_fu_6939_p3 = (!exitcond9_mid_fu_6913_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond9_mid_fu_6913_p2.read()[0].to_bool())? yy_V_fu_6919_p2.read(): p_5_mid_fu_6893_p3.read());
}

void mnist_fp16_opt::thread_tmp_s_fu_4585_p2() {
    tmp_s_fu_4585_p2 = (!p_shl5_cast_fu_4569_p1.read().is_01() || !p_shl6_cast_fu_4581_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl5_cast_fu_4569_p1.read()) - sc_biguint<11>(p_shl6_cast_fu_4581_p1.read()));
}

void mnist_fp16_opt::thread_tmp_user_V_fu_4540_p1() {
    tmp_user_V_fu_4540_p1 = stream_in_V_user_V_0_data_out.read();
}

void mnist_fp16_opt::thread_w1_V_fu_16971_p2() {
    w1_V_fu_16971_p2 = (!p_65_mid2_reg_26348.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_65_mid2_reg_26348.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16_opt::thread_w_V_fu_11016_p2() {
    w_V_fu_11016_p2 = (!p_33_mid2_reg_24753.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_33_mid2_reg_24753.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_w_conv1_0_address0() {
    w_conv1_0_address0 =  (sc_lv<6>) (tmp_434_cast_fu_7180_p1.read());
}

void mnist_fp16_opt::thread_w_conv1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp4_stage1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp4_stage1_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp4_iter0.read(), ap_const_logic_1))) {
        w_conv1_0_ce0 = ap_const_logic_1;
    } else {
        w_conv1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_w_conv2_address0() {
    w_conv2_address0 =  (sc_lv<8>) (tmp_752_cast_fu_9051_p1.read());
}

void mnist_fp16_opt::thread_w_conv2_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp7_stage0.read()) && 
         esl_seteq<1,1,1>(ap_block_pp7_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp7_iter1.read()))) {
        w_conv2_ce0 = ap_const_logic_1;
    } else {
        w_conv2_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_w_conv3_address0() {
    w_conv3_address0 =  (sc_lv<9>) (tmp_889_cast_fu_13254_p1.read());
}

void mnist_fp16_opt::thread_w_conv3_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp11_stage1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp11_stage1_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp11_iter0.read(), ap_const_logic_1))) {
        w_conv3_ce0 = ap_const_logic_1;
    } else {
        w_conv3_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_w_conv4_address0() {
    w_conv4_address0 =  (sc_lv<10>) (tmp_1027_cast_fu_15021_p1.read());
}

void mnist_fp16_opt::thread_w_conv4_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp14_stage0.read()) && 
         esl_seteq<1,1,1>(ap_block_pp14_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp14_iter1.read()))) {
        w_conv4_ce0 = ap_const_logic_1;
    } else {
        w_conv4_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_w_conv5_address0() {
    w_conv5_address0 =  (sc_lv<11>) (tmp_1168_cast_fu_18417_p1.read());
}

void mnist_fp16_opt::thread_w_conv5_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp18_stage2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp18_stage2_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp18_iter0.read(), ap_const_logic_1))) {
        w_conv5_ce0 = ap_const_logic_1;
    } else {
        w_conv5_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_w_conv6_address0() {
    w_conv6_address0 =  (sc_lv<12>) (tmp_1252_cast_fu_20154_p1.read());
}

void mnist_fp16_opt::thread_w_conv6_ce0() {
    if ((esl_seteq<1,1,1>(ap_block_pp21_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp21_iter2.read()))) {
        w_conv6_ce0 = ap_const_logic_1;
    } else {
        w_conv6_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_w_dense_1_address0() {
    w_dense_1_address0 =  (sc_lv<8>) (tmp_1262_cast_fu_22381_p1.read());
}

void mnist_fp16_opt::thread_w_dense_1_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state403.read())) {
        w_dense_1_ce0 = ap_const_logic_1;
    } else {
        w_dense_1_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_xx1_V_fu_9633_p2() {
    xx1_V_fu_9633_p2 = (!ap_const_lv5_1.is_01() || !p_14_mid2_reg_24276.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_1) + sc_biguint<5>(p_14_mid2_reg_24276.read()));
}

void mnist_fp16_opt::thread_xx2_V_fu_13277_p2() {
    xx2_V_fu_13277_p2 = (!p_29_mid2_reg_25433.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_29_mid2_reg_25433.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_xx3_V_fu_15603_p2() {
    xx3_V_fu_15603_p2 = (!ap_const_lv4_1.is_01() || !p_39_mid2_reg_25895.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(p_39_mid2_reg_25895.read()));
}

void mnist_fp16_opt::thread_xx4_V_fu_18439_p2() {
    xx4_V_fu_18439_p2 = (!p_53_mid2_reg_26825.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_53_mid2_reg_26825.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16_opt::thread_xx5_V_fu_20735_p2() {
    xx5_V_fu_20735_p2 = (!ap_const_lv3_1.is_01() || !p_64_mid2_reg_27273.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(p_64_mid2_reg_27273.read()));
}

void mnist_fp16_opt::thread_xx_V_fu_7203_p2() {
    xx_V_fu_7203_p2 = (!p_8_mid2_reg_23815.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_8_mid2_reg_23815.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16_opt::thread_yy1_V_fu_8629_p2() {
    yy1_V_fu_8629_p2 = (!ap_const_lv5_1.is_01() || !p_10_mid_fu_8603_p3.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_1) + sc_biguint<5>(p_10_mid_fu_8603_p3.read()));
}

void mnist_fp16_opt::thread_yy2_V_fu_12962_p2() {
    yy2_V_fu_12962_p2 = (!ap_const_lv4_1.is_01() || !p_24_mid_fu_12882_p3.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(p_24_mid_fu_12882_p3.read()));
}

void mnist_fp16_opt::thread_yy3_V_fu_14723_p2() {
    yy3_V_fu_14723_p2 = (!ap_const_lv4_1.is_01() || !p_34_mid_fu_14643_p3.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(p_34_mid_fu_14643_p3.read()));
}

void mnist_fp16_opt::thread_yy4_V_fu_18079_p2() {
    yy4_V_fu_18079_p2 = (!ap_const_lv3_1.is_01() || !p_49_mid_fu_18019_p3.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(p_49_mid_fu_18019_p3.read()));
}

void mnist_fp16_opt::thread_yy5_V_fu_19810_p2() {
    yy5_V_fu_19810_p2 = (!ap_const_lv3_1.is_01() || !p_61_mid_fu_19738_p3.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(p_61_mid_fu_19738_p3.read()));
}

void mnist_fp16_opt::thread_yy_V_fu_6919_p2() {
    yy_V_fu_6919_p2 = (!ap_const_lv5_1.is_01() || !p_5_mid_fu_6893_p3.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_1) + sc_biguint<5>(p_5_mid_fu_6893_p3.read()));
}

}

