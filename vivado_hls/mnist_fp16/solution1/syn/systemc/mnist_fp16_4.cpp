#include "mnist_fp16.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp16::thread_F2_10_fu_12901_p2() {
    F2_10_fu_12901_p2 = (!ap_const_lv12_433.is_01() || !tmp_476_fu_12874_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_476_fu_12874_p1.read()));
}

void mnist_fp16::thread_F2_11_fu_12986_p2() {
    F2_11_fu_12986_p2 = (!ap_const_lv12_433.is_01() || !tmp_527_fu_12959_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_527_fu_12959_p1.read()));
}

void mnist_fp16::thread_F2_1_fu_6108_p2() {
    F2_1_fu_6108_p2 = (!ap_const_lv12_433.is_01() || !tmp_99_fu_6081_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_99_fu_6081_p1.read()));
}

void mnist_fp16::thread_F2_2_fu_5196_p2() {
    F2_2_fu_5196_p2 = (!ap_const_lv12_433.is_01() || !tmp_106_fu_5169_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_106_fu_5169_p1.read()));
}

void mnist_fp16::thread_F2_3_fu_5281_p2() {
    F2_3_fu_5281_p2 = (!ap_const_lv12_433.is_01() || !tmp_170_fu_5254_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_170_fu_5254_p1.read()));
}

void mnist_fp16::thread_F2_4_fu_7839_p2() {
    F2_4_fu_7839_p2 = (!ap_const_lv12_433.is_01() || !tmp_210_fu_7812_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_210_fu_7812_p1.read()));
}

void mnist_fp16::thread_F2_5_fu_9977_p2() {
    F2_5_fu_9977_p2 = (!ap_const_lv12_433.is_01() || !tmp_277_fu_9950_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_277_fu_9950_p1.read()));
}

void mnist_fp16::thread_F2_6_fu_9065_p2() {
    F2_6_fu_9065_p2 = (!ap_const_lv12_433.is_01() || !tmp_284_fu_9038_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_284_fu_9038_p1.read()));
}

void mnist_fp16::thread_F2_7_fu_9150_p2() {
    F2_7_fu_9150_p2 = (!ap_const_lv12_433.is_01() || !tmp_336_fu_9123_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_336_fu_9123_p1.read()));
}

void mnist_fp16::thread_F2_8_fu_11663_p2() {
    F2_8_fu_11663_p2 = (!ap_const_lv12_433.is_01() || !tmp_388_fu_11636_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_388_fu_11636_p1.read()));
}

void mnist_fp16::thread_F2_9_fu_14481_p2() {
    F2_9_fu_14481_p2 = (!ap_const_lv12_433.is_01() || !tmp_435_fu_14449_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_435_fu_14449_p1.read()));
}

void mnist_fp16::thread_F2_fu_3832_p2() {
    F2_fu_3832_p2 = (!ap_const_lv12_433.is_01() || !tmp_32_fu_3805_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_32_fu_3805_p1.read()));
}

void mnist_fp16::thread_F2_s_fu_13788_p2() {
    F2_s_fu_13788_p2 = (!ap_const_lv12_433.is_01() || !tmp_469_fu_13761_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_469_fu_13761_p1.read()));
}

void mnist_fp16::thread_ap_CS_fsm_pp1_stage0() {
    ap_CS_fsm_pp1_stage0 = ap_CS_fsm.read()[4];
}

void mnist_fp16::thread_ap_CS_fsm_pp2_stage0() {
    ap_CS_fsm_pp2_stage0 = ap_CS_fsm.read()[6];
}

void mnist_fp16::thread_ap_CS_fsm_state1() {
    ap_CS_fsm_state1 = ap_CS_fsm.read()[0];
}

void mnist_fp16::thread_ap_CS_fsm_state10() {
    ap_CS_fsm_state10 = ap_CS_fsm.read()[7];
}

void mnist_fp16::thread_ap_CS_fsm_state100() {
    ap_CS_fsm_state100 = ap_CS_fsm.read()[97];
}

void mnist_fp16::thread_ap_CS_fsm_state101() {
    ap_CS_fsm_state101 = ap_CS_fsm.read()[98];
}

void mnist_fp16::thread_ap_CS_fsm_state102() {
    ap_CS_fsm_state102 = ap_CS_fsm.read()[99];
}

void mnist_fp16::thread_ap_CS_fsm_state103() {
    ap_CS_fsm_state103 = ap_CS_fsm.read()[100];
}

void mnist_fp16::thread_ap_CS_fsm_state108() {
    ap_CS_fsm_state108 = ap_CS_fsm.read()[105];
}

void mnist_fp16::thread_ap_CS_fsm_state109() {
    ap_CS_fsm_state109 = ap_CS_fsm.read()[106];
}

void mnist_fp16::thread_ap_CS_fsm_state11() {
    ap_CS_fsm_state11 = ap_CS_fsm.read()[8];
}

void mnist_fp16::thread_ap_CS_fsm_state110() {
    ap_CS_fsm_state110 = ap_CS_fsm.read()[107];
}

void mnist_fp16::thread_ap_CS_fsm_state111() {
    ap_CS_fsm_state111 = ap_CS_fsm.read()[108];
}

void mnist_fp16::thread_ap_CS_fsm_state112() {
    ap_CS_fsm_state112 = ap_CS_fsm.read()[109];
}

void mnist_fp16::thread_ap_CS_fsm_state113() {
    ap_CS_fsm_state113 = ap_CS_fsm.read()[110];
}

void mnist_fp16::thread_ap_CS_fsm_state114() {
    ap_CS_fsm_state114 = ap_CS_fsm.read()[111];
}

void mnist_fp16::thread_ap_CS_fsm_state115() {
    ap_CS_fsm_state115 = ap_CS_fsm.read()[112];
}

void mnist_fp16::thread_ap_CS_fsm_state116() {
    ap_CS_fsm_state116 = ap_CS_fsm.read()[113];
}

void mnist_fp16::thread_ap_CS_fsm_state117() {
    ap_CS_fsm_state117 = ap_CS_fsm.read()[114];
}

void mnist_fp16::thread_ap_CS_fsm_state118() {
    ap_CS_fsm_state118 = ap_CS_fsm.read()[115];
}

void mnist_fp16::thread_ap_CS_fsm_state119() {
    ap_CS_fsm_state119 = ap_CS_fsm.read()[116];
}

void mnist_fp16::thread_ap_CS_fsm_state12() {
    ap_CS_fsm_state12 = ap_CS_fsm.read()[9];
}

void mnist_fp16::thread_ap_CS_fsm_state120() {
    ap_CS_fsm_state120 = ap_CS_fsm.read()[117];
}

void mnist_fp16::thread_ap_CS_fsm_state121() {
    ap_CS_fsm_state121 = ap_CS_fsm.read()[118];
}

void mnist_fp16::thread_ap_CS_fsm_state122() {
    ap_CS_fsm_state122 = ap_CS_fsm.read()[119];
}

void mnist_fp16::thread_ap_CS_fsm_state123() {
    ap_CS_fsm_state123 = ap_CS_fsm.read()[120];
}

void mnist_fp16::thread_ap_CS_fsm_state124() {
    ap_CS_fsm_state124 = ap_CS_fsm.read()[121];
}

void mnist_fp16::thread_ap_CS_fsm_state125() {
    ap_CS_fsm_state125 = ap_CS_fsm.read()[122];
}

void mnist_fp16::thread_ap_CS_fsm_state126() {
    ap_CS_fsm_state126 = ap_CS_fsm.read()[123];
}

void mnist_fp16::thread_ap_CS_fsm_state13() {
    ap_CS_fsm_state13 = ap_CS_fsm.read()[10];
}

void mnist_fp16::thread_ap_CS_fsm_state131() {
    ap_CS_fsm_state131 = ap_CS_fsm.read()[128];
}

void mnist_fp16::thread_ap_CS_fsm_state132() {
    ap_CS_fsm_state132 = ap_CS_fsm.read()[129];
}

void mnist_fp16::thread_ap_CS_fsm_state133() {
    ap_CS_fsm_state133 = ap_CS_fsm.read()[130];
}

void mnist_fp16::thread_ap_CS_fsm_state134() {
    ap_CS_fsm_state134 = ap_CS_fsm.read()[131];
}

void mnist_fp16::thread_ap_CS_fsm_state135() {
    ap_CS_fsm_state135 = ap_CS_fsm.read()[132];
}

void mnist_fp16::thread_ap_CS_fsm_state136() {
    ap_CS_fsm_state136 = ap_CS_fsm.read()[133];
}

void mnist_fp16::thread_ap_CS_fsm_state137() {
    ap_CS_fsm_state137 = ap_CS_fsm.read()[134];
}

void mnist_fp16::thread_ap_CS_fsm_state138() {
    ap_CS_fsm_state138 = ap_CS_fsm.read()[135];
}

void mnist_fp16::thread_ap_CS_fsm_state14() {
    ap_CS_fsm_state14 = ap_CS_fsm.read()[11];
}

void mnist_fp16::thread_ap_CS_fsm_state152() {
    ap_CS_fsm_state152 = ap_CS_fsm.read()[149];
}

void mnist_fp16::thread_ap_CS_fsm_state153() {
    ap_CS_fsm_state153 = ap_CS_fsm.read()[150];
}

void mnist_fp16::thread_ap_CS_fsm_state154() {
    ap_CS_fsm_state154 = ap_CS_fsm.read()[151];
}

void mnist_fp16::thread_ap_CS_fsm_state155() {
    ap_CS_fsm_state155 = ap_CS_fsm.read()[152];
}

void mnist_fp16::thread_ap_CS_fsm_state156() {
    ap_CS_fsm_state156 = ap_CS_fsm.read()[153];
}

void mnist_fp16::thread_ap_CS_fsm_state157() {
    ap_CS_fsm_state157 = ap_CS_fsm.read()[154];
}

void mnist_fp16::thread_ap_CS_fsm_state158() {
    ap_CS_fsm_state158 = ap_CS_fsm.read()[155];
}

void mnist_fp16::thread_ap_CS_fsm_state159() {
    ap_CS_fsm_state159 = ap_CS_fsm.read()[156];
}

void mnist_fp16::thread_ap_CS_fsm_state160() {
    ap_CS_fsm_state160 = ap_CS_fsm.read()[157];
}

void mnist_fp16::thread_ap_CS_fsm_state161() {
    ap_CS_fsm_state161 = ap_CS_fsm.read()[158];
}

void mnist_fp16::thread_ap_CS_fsm_state162() {
    ap_CS_fsm_state162 = ap_CS_fsm.read()[159];
}

void mnist_fp16::thread_ap_CS_fsm_state163() {
    ap_CS_fsm_state163 = ap_CS_fsm.read()[160];
}

void mnist_fp16::thread_ap_CS_fsm_state166() {
    ap_CS_fsm_state166 = ap_CS_fsm.read()[163];
}

void mnist_fp16::thread_ap_CS_fsm_state167() {
    ap_CS_fsm_state167 = ap_CS_fsm.read()[164];
}

void mnist_fp16::thread_ap_CS_fsm_state171() {
    ap_CS_fsm_state171 = ap_CS_fsm.read()[168];
}

void mnist_fp16::thread_ap_CS_fsm_state172() {
    ap_CS_fsm_state172 = ap_CS_fsm.read()[169];
}

void mnist_fp16::thread_ap_CS_fsm_state173() {
    ap_CS_fsm_state173 = ap_CS_fsm.read()[170];
}

void mnist_fp16::thread_ap_CS_fsm_state174() {
    ap_CS_fsm_state174 = ap_CS_fsm.read()[171];
}

void mnist_fp16::thread_ap_CS_fsm_state175() {
    ap_CS_fsm_state175 = ap_CS_fsm.read()[172];
}

void mnist_fp16::thread_ap_CS_fsm_state176() {
    ap_CS_fsm_state176 = ap_CS_fsm.read()[173];
}

void mnist_fp16::thread_ap_CS_fsm_state177() {
    ap_CS_fsm_state177 = ap_CS_fsm.read()[174];
}

void mnist_fp16::thread_ap_CS_fsm_state178() {
    ap_CS_fsm_state178 = ap_CS_fsm.read()[175];
}

void mnist_fp16::thread_ap_CS_fsm_state179() {
    ap_CS_fsm_state179 = ap_CS_fsm.read()[176];
}

void mnist_fp16::thread_ap_CS_fsm_state180() {
    ap_CS_fsm_state180 = ap_CS_fsm.read()[177];
}

void mnist_fp16::thread_ap_CS_fsm_state181() {
    ap_CS_fsm_state181 = ap_CS_fsm.read()[178];
}

void mnist_fp16::thread_ap_CS_fsm_state182() {
    ap_CS_fsm_state182 = ap_CS_fsm.read()[179];
}

void mnist_fp16::thread_ap_CS_fsm_state183() {
    ap_CS_fsm_state183 = ap_CS_fsm.read()[180];
}

void mnist_fp16::thread_ap_CS_fsm_state184() {
    ap_CS_fsm_state184 = ap_CS_fsm.read()[181];
}

void mnist_fp16::thread_ap_CS_fsm_state199() {
    ap_CS_fsm_state199 = ap_CS_fsm.read()[196];
}

void mnist_fp16::thread_ap_CS_fsm_state2() {
    ap_CS_fsm_state2 = ap_CS_fsm.read()[1];
}

void mnist_fp16::thread_ap_CS_fsm_state200() {
    ap_CS_fsm_state200 = ap_CS_fsm.read()[197];
}

void mnist_fp16::thread_ap_CS_fsm_state201() {
    ap_CS_fsm_state201 = ap_CS_fsm.read()[198];
}

void mnist_fp16::thread_ap_CS_fsm_state202() {
    ap_CS_fsm_state202 = ap_CS_fsm.read()[199];
}

void mnist_fp16::thread_ap_CS_fsm_state203() {
    ap_CS_fsm_state203 = ap_CS_fsm.read()[200];
}

void mnist_fp16::thread_ap_CS_fsm_state204() {
    ap_CS_fsm_state204 = ap_CS_fsm.read()[201];
}

void mnist_fp16::thread_ap_CS_fsm_state209() {
    ap_CS_fsm_state209 = ap_CS_fsm.read()[206];
}

void mnist_fp16::thread_ap_CS_fsm_state210() {
    ap_CS_fsm_state210 = ap_CS_fsm.read()[207];
}

void mnist_fp16::thread_ap_CS_fsm_state211() {
    ap_CS_fsm_state211 = ap_CS_fsm.read()[208];
}

void mnist_fp16::thread_ap_CS_fsm_state212() {
    ap_CS_fsm_state212 = ap_CS_fsm.read()[209];
}

void mnist_fp16::thread_ap_CS_fsm_state213() {
    ap_CS_fsm_state213 = ap_CS_fsm.read()[210];
}

void mnist_fp16::thread_ap_CS_fsm_state214() {
    ap_CS_fsm_state214 = ap_CS_fsm.read()[211];
}

void mnist_fp16::thread_ap_CS_fsm_state215() {
    ap_CS_fsm_state215 = ap_CS_fsm.read()[212];
}

void mnist_fp16::thread_ap_CS_fsm_state216() {
    ap_CS_fsm_state216 = ap_CS_fsm.read()[213];
}

void mnist_fp16::thread_ap_CS_fsm_state217() {
    ap_CS_fsm_state217 = ap_CS_fsm.read()[214];
}

void mnist_fp16::thread_ap_CS_fsm_state218() {
    ap_CS_fsm_state218 = ap_CS_fsm.read()[215];
}

void mnist_fp16::thread_ap_CS_fsm_state219() {
    ap_CS_fsm_state219 = ap_CS_fsm.read()[216];
}

void mnist_fp16::thread_ap_CS_fsm_state220() {
    ap_CS_fsm_state220 = ap_CS_fsm.read()[217];
}

void mnist_fp16::thread_ap_CS_fsm_state221() {
    ap_CS_fsm_state221 = ap_CS_fsm.read()[218];
}

void mnist_fp16::thread_ap_CS_fsm_state222() {
    ap_CS_fsm_state222 = ap_CS_fsm.read()[219];
}

void mnist_fp16::thread_ap_CS_fsm_state223() {
    ap_CS_fsm_state223 = ap_CS_fsm.read()[220];
}

void mnist_fp16::thread_ap_CS_fsm_state224() {
    ap_CS_fsm_state224 = ap_CS_fsm.read()[221];
}

void mnist_fp16::thread_ap_CS_fsm_state225() {
    ap_CS_fsm_state225 = ap_CS_fsm.read()[222];
}

void mnist_fp16::thread_ap_CS_fsm_state230() {
    ap_CS_fsm_state230 = ap_CS_fsm.read()[227];
}

void mnist_fp16::thread_ap_CS_fsm_state231() {
    ap_CS_fsm_state231 = ap_CS_fsm.read()[228];
}

void mnist_fp16::thread_ap_CS_fsm_state232() {
    ap_CS_fsm_state232 = ap_CS_fsm.read()[229];
}

void mnist_fp16::thread_ap_CS_fsm_state233() {
    ap_CS_fsm_state233 = ap_CS_fsm.read()[230];
}

void mnist_fp16::thread_ap_CS_fsm_state234() {
    ap_CS_fsm_state234 = ap_CS_fsm.read()[231];
}

void mnist_fp16::thread_ap_CS_fsm_state235() {
    ap_CS_fsm_state235 = ap_CS_fsm.read()[232];
}

void mnist_fp16::thread_ap_CS_fsm_state236() {
    ap_CS_fsm_state236 = ap_CS_fsm.read()[233];
}

void mnist_fp16::thread_ap_CS_fsm_state237() {
    ap_CS_fsm_state237 = ap_CS_fsm.read()[234];
}

void mnist_fp16::thread_ap_CS_fsm_state238() {
    ap_CS_fsm_state238 = ap_CS_fsm.read()[235];
}

void mnist_fp16::thread_ap_CS_fsm_state239() {
    ap_CS_fsm_state239 = ap_CS_fsm.read()[236];
}

void mnist_fp16::thread_ap_CS_fsm_state240() {
    ap_CS_fsm_state240 = ap_CS_fsm.read()[237];
}

void mnist_fp16::thread_ap_CS_fsm_state241() {
    ap_CS_fsm_state241 = ap_CS_fsm.read()[238];
}

void mnist_fp16::thread_ap_CS_fsm_state242() {
    ap_CS_fsm_state242 = ap_CS_fsm.read()[239];
}

void mnist_fp16::thread_ap_CS_fsm_state243() {
    ap_CS_fsm_state243 = ap_CS_fsm.read()[240];
}

void mnist_fp16::thread_ap_CS_fsm_state244() {
    ap_CS_fsm_state244 = ap_CS_fsm.read()[241];
}

void mnist_fp16::thread_ap_CS_fsm_state245() {
    ap_CS_fsm_state245 = ap_CS_fsm.read()[242];
}

void mnist_fp16::thread_ap_CS_fsm_state246() {
    ap_CS_fsm_state246 = ap_CS_fsm.read()[243];
}

void mnist_fp16::thread_ap_CS_fsm_state247() {
    ap_CS_fsm_state247 = ap_CS_fsm.read()[244];
}

void mnist_fp16::thread_ap_CS_fsm_state248() {
    ap_CS_fsm_state248 = ap_CS_fsm.read()[245];
}

void mnist_fp16::thread_ap_CS_fsm_state253() {
    ap_CS_fsm_state253 = ap_CS_fsm.read()[250];
}

void mnist_fp16::thread_ap_CS_fsm_state254() {
    ap_CS_fsm_state254 = ap_CS_fsm.read()[251];
}

void mnist_fp16::thread_ap_CS_fsm_state255() {
    ap_CS_fsm_state255 = ap_CS_fsm.read()[252];
}

void mnist_fp16::thread_ap_CS_fsm_state256() {
    ap_CS_fsm_state256 = ap_CS_fsm.read()[253];
}

void mnist_fp16::thread_ap_CS_fsm_state257() {
    ap_CS_fsm_state257 = ap_CS_fsm.read()[254];
}

void mnist_fp16::thread_ap_CS_fsm_state258() {
    ap_CS_fsm_state258 = ap_CS_fsm.read()[255];
}

void mnist_fp16::thread_ap_CS_fsm_state259() {
    ap_CS_fsm_state259 = ap_CS_fsm.read()[256];
}

void mnist_fp16::thread_ap_CS_fsm_state260() {
    ap_CS_fsm_state260 = ap_CS_fsm.read()[257];
}

void mnist_fp16::thread_ap_CS_fsm_state273() {
    ap_CS_fsm_state273 = ap_CS_fsm.read()[270];
}

void mnist_fp16::thread_ap_CS_fsm_state274() {
    ap_CS_fsm_state274 = ap_CS_fsm.read()[271];
}

void mnist_fp16::thread_ap_CS_fsm_state275() {
    ap_CS_fsm_state275 = ap_CS_fsm.read()[272];
}

void mnist_fp16::thread_ap_CS_fsm_state276() {
    ap_CS_fsm_state276 = ap_CS_fsm.read()[273];
}

void mnist_fp16::thread_ap_CS_fsm_state277() {
    ap_CS_fsm_state277 = ap_CS_fsm.read()[274];
}

void mnist_fp16::thread_ap_CS_fsm_state278() {
    ap_CS_fsm_state278 = ap_CS_fsm.read()[275];
}

void mnist_fp16::thread_ap_CS_fsm_state279() {
    ap_CS_fsm_state279 = ap_CS_fsm.read()[276];
}

void mnist_fp16::thread_ap_CS_fsm_state280() {
    ap_CS_fsm_state280 = ap_CS_fsm.read()[277];
}

void mnist_fp16::thread_ap_CS_fsm_state281() {
    ap_CS_fsm_state281 = ap_CS_fsm.read()[278];
}

void mnist_fp16::thread_ap_CS_fsm_state282() {
    ap_CS_fsm_state282 = ap_CS_fsm.read()[279];
}

void mnist_fp16::thread_ap_CS_fsm_state283() {
    ap_CS_fsm_state283 = ap_CS_fsm.read()[280];
}

void mnist_fp16::thread_ap_CS_fsm_state284() {
    ap_CS_fsm_state284 = ap_CS_fsm.read()[281];
}

void mnist_fp16::thread_ap_CS_fsm_state287() {
    ap_CS_fsm_state287 = ap_CS_fsm.read()[284];
}

void mnist_fp16::thread_ap_CS_fsm_state288() {
    ap_CS_fsm_state288 = ap_CS_fsm.read()[285];
}

void mnist_fp16::thread_ap_CS_fsm_state29() {
    ap_CS_fsm_state29 = ap_CS_fsm.read()[26];
}

void mnist_fp16::thread_ap_CS_fsm_state292() {
    ap_CS_fsm_state292 = ap_CS_fsm.read()[289];
}

void mnist_fp16::thread_ap_CS_fsm_state293() {
    ap_CS_fsm_state293 = ap_CS_fsm.read()[290];
}

void mnist_fp16::thread_ap_CS_fsm_state294() {
    ap_CS_fsm_state294 = ap_CS_fsm.read()[291];
}

void mnist_fp16::thread_ap_CS_fsm_state295() {
    ap_CS_fsm_state295 = ap_CS_fsm.read()[292];
}

void mnist_fp16::thread_ap_CS_fsm_state296() {
    ap_CS_fsm_state296 = ap_CS_fsm.read()[293];
}

void mnist_fp16::thread_ap_CS_fsm_state297() {
    ap_CS_fsm_state297 = ap_CS_fsm.read()[294];
}

void mnist_fp16::thread_ap_CS_fsm_state298() {
    ap_CS_fsm_state298 = ap_CS_fsm.read()[295];
}

void mnist_fp16::thread_ap_CS_fsm_state299() {
    ap_CS_fsm_state299 = ap_CS_fsm.read()[296];
}

void mnist_fp16::thread_ap_CS_fsm_state3() {
    ap_CS_fsm_state3 = ap_CS_fsm.read()[2];
}

void mnist_fp16::thread_ap_CS_fsm_state30() {
    ap_CS_fsm_state30 = ap_CS_fsm.read()[27];
}

void mnist_fp16::thread_ap_CS_fsm_state300() {
    ap_CS_fsm_state300 = ap_CS_fsm.read()[297];
}

void mnist_fp16::thread_ap_CS_fsm_state301() {
    ap_CS_fsm_state301 = ap_CS_fsm.read()[298];
}

void mnist_fp16::thread_ap_CS_fsm_state302() {
    ap_CS_fsm_state302 = ap_CS_fsm.read()[299];
}

void mnist_fp16::thread_ap_CS_fsm_state303() {
    ap_CS_fsm_state303 = ap_CS_fsm.read()[300];
}

void mnist_fp16::thread_ap_CS_fsm_state304() {
    ap_CS_fsm_state304 = ap_CS_fsm.read()[301];
}

void mnist_fp16::thread_ap_CS_fsm_state305() {
    ap_CS_fsm_state305 = ap_CS_fsm.read()[302];
}

void mnist_fp16::thread_ap_CS_fsm_state31() {
    ap_CS_fsm_state31 = ap_CS_fsm.read()[28];
}

void mnist_fp16::thread_ap_CS_fsm_state319() {
    ap_CS_fsm_state319 = ap_CS_fsm.read()[316];
}

void mnist_fp16::thread_ap_CS_fsm_state32() {
    ap_CS_fsm_state32 = ap_CS_fsm.read()[29];
}

void mnist_fp16::thread_ap_CS_fsm_state320() {
    ap_CS_fsm_state320 = ap_CS_fsm.read()[317];
}

void mnist_fp16::thread_ap_CS_fsm_state321() {
    ap_CS_fsm_state321 = ap_CS_fsm.read()[318];
}

void mnist_fp16::thread_ap_CS_fsm_state322() {
    ap_CS_fsm_state322 = ap_CS_fsm.read()[319];
}

void mnist_fp16::thread_ap_CS_fsm_state323() {
    ap_CS_fsm_state323 = ap_CS_fsm.read()[320];
}

void mnist_fp16::thread_ap_CS_fsm_state324() {
    ap_CS_fsm_state324 = ap_CS_fsm.read()[321];
}

void mnist_fp16::thread_ap_CS_fsm_state329() {
    ap_CS_fsm_state329 = ap_CS_fsm.read()[326];
}

void mnist_fp16::thread_ap_CS_fsm_state33() {
    ap_CS_fsm_state33 = ap_CS_fsm.read()[30];
}

void mnist_fp16::thread_ap_CS_fsm_state330() {
    ap_CS_fsm_state330 = ap_CS_fsm.read()[327];
}

void mnist_fp16::thread_ap_CS_fsm_state331() {
    ap_CS_fsm_state331 = ap_CS_fsm.read()[328];
}

void mnist_fp16::thread_ap_CS_fsm_state332() {
    ap_CS_fsm_state332 = ap_CS_fsm.read()[329];
}

void mnist_fp16::thread_ap_CS_fsm_state333() {
    ap_CS_fsm_state333 = ap_CS_fsm.read()[330];
}

void mnist_fp16::thread_ap_CS_fsm_state334() {
    ap_CS_fsm_state334 = ap_CS_fsm.read()[331];
}

void mnist_fp16::thread_ap_CS_fsm_state335() {
    ap_CS_fsm_state335 = ap_CS_fsm.read()[332];
}

void mnist_fp16::thread_ap_CS_fsm_state336() {
    ap_CS_fsm_state336 = ap_CS_fsm.read()[333];
}

void mnist_fp16::thread_ap_CS_fsm_state337() {
    ap_CS_fsm_state337 = ap_CS_fsm.read()[334];
}

void mnist_fp16::thread_ap_CS_fsm_state338() {
    ap_CS_fsm_state338 = ap_CS_fsm.read()[335];
}

void mnist_fp16::thread_ap_CS_fsm_state339() {
    ap_CS_fsm_state339 = ap_CS_fsm.read()[336];
}

void mnist_fp16::thread_ap_CS_fsm_state34() {
    ap_CS_fsm_state34 = ap_CS_fsm.read()[31];
}

void mnist_fp16::thread_ap_CS_fsm_state340() {
    ap_CS_fsm_state340 = ap_CS_fsm.read()[337];
}

void mnist_fp16::thread_ap_CS_fsm_state341() {
    ap_CS_fsm_state341 = ap_CS_fsm.read()[338];
}

void mnist_fp16::thread_ap_CS_fsm_state342() {
    ap_CS_fsm_state342 = ap_CS_fsm.read()[339];
}

void mnist_fp16::thread_ap_CS_fsm_state343() {
    ap_CS_fsm_state343 = ap_CS_fsm.read()[340];
}

void mnist_fp16::thread_ap_CS_fsm_state344() {
    ap_CS_fsm_state344 = ap_CS_fsm.read()[341];
}

void mnist_fp16::thread_ap_CS_fsm_state345() {
    ap_CS_fsm_state345 = ap_CS_fsm.read()[342];
}

void mnist_fp16::thread_ap_CS_fsm_state35() {
    ap_CS_fsm_state35 = ap_CS_fsm.read()[32];
}

void mnist_fp16::thread_ap_CS_fsm_state350() {
    ap_CS_fsm_state350 = ap_CS_fsm.read()[347];
}

void mnist_fp16::thread_ap_CS_fsm_state351() {
    ap_CS_fsm_state351 = ap_CS_fsm.read()[348];
}

void mnist_fp16::thread_ap_CS_fsm_state352() {
    ap_CS_fsm_state352 = ap_CS_fsm.read()[349];
}

void mnist_fp16::thread_ap_CS_fsm_state353() {
    ap_CS_fsm_state353 = ap_CS_fsm.read()[350];
}

void mnist_fp16::thread_ap_CS_fsm_state354() {
    ap_CS_fsm_state354 = ap_CS_fsm.read()[351];
}

void mnist_fp16::thread_ap_CS_fsm_state355() {
    ap_CS_fsm_state355 = ap_CS_fsm.read()[352];
}

void mnist_fp16::thread_ap_CS_fsm_state356() {
    ap_CS_fsm_state356 = ap_CS_fsm.read()[353];
}

void mnist_fp16::thread_ap_CS_fsm_state357() {
    ap_CS_fsm_state357 = ap_CS_fsm.read()[354];
}

void mnist_fp16::thread_ap_CS_fsm_state358() {
    ap_CS_fsm_state358 = ap_CS_fsm.read()[355];
}

void mnist_fp16::thread_ap_CS_fsm_state359() {
    ap_CS_fsm_state359 = ap_CS_fsm.read()[356];
}

void mnist_fp16::thread_ap_CS_fsm_state36() {
    ap_CS_fsm_state36 = ap_CS_fsm.read()[33];
}

void mnist_fp16::thread_ap_CS_fsm_state360() {
    ap_CS_fsm_state360 = ap_CS_fsm.read()[357];
}

void mnist_fp16::thread_ap_CS_fsm_state361() {
    ap_CS_fsm_state361 = ap_CS_fsm.read()[358];
}

void mnist_fp16::thread_ap_CS_fsm_state362() {
    ap_CS_fsm_state362 = ap_CS_fsm.read()[359];
}

void mnist_fp16::thread_ap_CS_fsm_state363() {
    ap_CS_fsm_state363 = ap_CS_fsm.read()[360];
}

void mnist_fp16::thread_ap_CS_fsm_state364() {
    ap_CS_fsm_state364 = ap_CS_fsm.read()[361];
}

void mnist_fp16::thread_ap_CS_fsm_state365() {
    ap_CS_fsm_state365 = ap_CS_fsm.read()[362];
}

void mnist_fp16::thread_ap_CS_fsm_state366() {
    ap_CS_fsm_state366 = ap_CS_fsm.read()[363];
}

void mnist_fp16::thread_ap_CS_fsm_state37() {
    ap_CS_fsm_state37 = ap_CS_fsm.read()[34];
}

void mnist_fp16::thread_ap_CS_fsm_state371() {
    ap_CS_fsm_state371 = ap_CS_fsm.read()[368];
}

void mnist_fp16::thread_ap_CS_fsm_state372() {
    ap_CS_fsm_state372 = ap_CS_fsm.read()[369];
}

void mnist_fp16::thread_ap_CS_fsm_state373() {
    ap_CS_fsm_state373 = ap_CS_fsm.read()[370];
}

void mnist_fp16::thread_ap_CS_fsm_state374() {
    ap_CS_fsm_state374 = ap_CS_fsm.read()[371];
}

void mnist_fp16::thread_ap_CS_fsm_state375() {
    ap_CS_fsm_state375 = ap_CS_fsm.read()[372];
}

void mnist_fp16::thread_ap_CS_fsm_state376() {
    ap_CS_fsm_state376 = ap_CS_fsm.read()[373];
}

void mnist_fp16::thread_ap_CS_fsm_state377() {
    ap_CS_fsm_state377 = ap_CS_fsm.read()[374];
}

void mnist_fp16::thread_ap_CS_fsm_state378() {
    ap_CS_fsm_state378 = ap_CS_fsm.read()[375];
}

void mnist_fp16::thread_ap_CS_fsm_state379() {
    ap_CS_fsm_state379 = ap_CS_fsm.read()[376];
}

void mnist_fp16::thread_ap_CS_fsm_state38() {
    ap_CS_fsm_state38 = ap_CS_fsm.read()[35];
}

void mnist_fp16::thread_ap_CS_fsm_state380() {
    ap_CS_fsm_state380 = ap_CS_fsm.read()[377];
}

void mnist_fp16::thread_ap_CS_fsm_state381() {
    ap_CS_fsm_state381 = ap_CS_fsm.read()[378];
}

void mnist_fp16::thread_ap_CS_fsm_state382() {
    ap_CS_fsm_state382 = ap_CS_fsm.read()[379];
}

void mnist_fp16::thread_ap_CS_fsm_state383() {
    ap_CS_fsm_state383 = ap_CS_fsm.read()[380];
}

void mnist_fp16::thread_ap_CS_fsm_state387() {
    ap_CS_fsm_state387 = ap_CS_fsm.read()[384];
}

void mnist_fp16::thread_ap_CS_fsm_state388() {
    ap_CS_fsm_state388 = ap_CS_fsm.read()[385];
}

void mnist_fp16::thread_ap_CS_fsm_state389() {
    ap_CS_fsm_state389 = ap_CS_fsm.read()[386];
}

void mnist_fp16::thread_ap_CS_fsm_state39() {
    ap_CS_fsm_state39 = ap_CS_fsm.read()[36];
}

void mnist_fp16::thread_ap_CS_fsm_state392() {
    ap_CS_fsm_state392 = ap_CS_fsm.read()[389];
}

void mnist_fp16::thread_ap_CS_fsm_state393() {
    ap_CS_fsm_state393 = ap_CS_fsm.read()[390];
}

void mnist_fp16::thread_ap_CS_fsm_state397() {
    ap_CS_fsm_state397 = ap_CS_fsm.read()[394];
}

void mnist_fp16::thread_ap_CS_fsm_state398() {
    ap_CS_fsm_state398 = ap_CS_fsm.read()[395];
}

void mnist_fp16::thread_ap_CS_fsm_state399() {
    ap_CS_fsm_state399 = ap_CS_fsm.read()[396];
}

void mnist_fp16::thread_ap_CS_fsm_state4() {
    ap_CS_fsm_state4 = ap_CS_fsm.read()[3];
}

void mnist_fp16::thread_ap_CS_fsm_state400() {
    ap_CS_fsm_state400 = ap_CS_fsm.read()[397];
}

void mnist_fp16::thread_ap_CS_fsm_state401() {
    ap_CS_fsm_state401 = ap_CS_fsm.read()[398];
}

void mnist_fp16::thread_ap_CS_fsm_state402() {
    ap_CS_fsm_state402 = ap_CS_fsm.read()[399];
}

void mnist_fp16::thread_ap_CS_fsm_state403() {
    ap_CS_fsm_state403 = ap_CS_fsm.read()[400];
}

void mnist_fp16::thread_ap_CS_fsm_state407() {
    ap_CS_fsm_state407 = ap_CS_fsm.read()[404];
}

void mnist_fp16::thread_ap_CS_fsm_state408() {
    ap_CS_fsm_state408 = ap_CS_fsm.read()[405];
}

void mnist_fp16::thread_ap_CS_fsm_state409() {
    ap_CS_fsm_state409 = ap_CS_fsm.read()[406];
}

void mnist_fp16::thread_ap_CS_fsm_state42() {
    ap_CS_fsm_state42 = ap_CS_fsm.read()[39];
}

void mnist_fp16::thread_ap_CS_fsm_state426() {
    ap_CS_fsm_state426 = ap_CS_fsm.read()[423];
}

void mnist_fp16::thread_ap_CS_fsm_state427() {
    ap_CS_fsm_state427 = ap_CS_fsm.read()[424];
}

void mnist_fp16::thread_ap_CS_fsm_state43() {
    ap_CS_fsm_state43 = ap_CS_fsm.read()[40];
}

void mnist_fp16::thread_ap_CS_fsm_state431() {
    ap_CS_fsm_state431 = ap_CS_fsm.read()[428];
}

void mnist_fp16::thread_ap_CS_fsm_state432() {
    ap_CS_fsm_state432 = ap_CS_fsm.read()[429];
}

void mnist_fp16::thread_ap_CS_fsm_state433() {
    ap_CS_fsm_state433 = ap_CS_fsm.read()[430];
}

void mnist_fp16::thread_ap_CS_fsm_state434() {
    ap_CS_fsm_state434 = ap_CS_fsm.read()[431];
}

void mnist_fp16::thread_ap_CS_fsm_state435() {
    ap_CS_fsm_state435 = ap_CS_fsm.read()[432];
}

void mnist_fp16::thread_ap_CS_fsm_state439() {
    ap_CS_fsm_state439 = ap_CS_fsm.read()[436];
}

void mnist_fp16::thread_ap_CS_fsm_state440() {
    ap_CS_fsm_state440 = ap_CS_fsm.read()[437];
}

void mnist_fp16::thread_ap_CS_fsm_state441() {
    ap_CS_fsm_state441 = ap_CS_fsm.read()[438];
}

void mnist_fp16::thread_ap_CS_fsm_state458() {
    ap_CS_fsm_state458 = ap_CS_fsm.read()[455];
}

void mnist_fp16::thread_ap_CS_fsm_state459() {
    ap_CS_fsm_state459 = ap_CS_fsm.read()[456];
}

void mnist_fp16::thread_ap_CS_fsm_state47() {
    ap_CS_fsm_state47 = ap_CS_fsm.read()[44];
}

void mnist_fp16::thread_ap_CS_fsm_state48() {
    ap_CS_fsm_state48 = ap_CS_fsm.read()[45];
}

void mnist_fp16::thread_ap_CS_fsm_state489() {
    ap_CS_fsm_state489 = ap_CS_fsm.read()[486];
}

void mnist_fp16::thread_ap_CS_fsm_state49() {
    ap_CS_fsm_state49 = ap_CS_fsm.read()[46];
}

void mnist_fp16::thread_ap_CS_fsm_state490() {
    ap_CS_fsm_state490 = ap_CS_fsm.read()[487];
}

void mnist_fp16::thread_ap_CS_fsm_state491() {
    ap_CS_fsm_state491 = ap_CS_fsm.read()[488];
}

void mnist_fp16::thread_ap_CS_fsm_state492() {
    ap_CS_fsm_state492 = ap_CS_fsm.read()[489];
}

void mnist_fp16::thread_ap_CS_fsm_state493() {
    ap_CS_fsm_state493 = ap_CS_fsm.read()[490];
}

void mnist_fp16::thread_ap_CS_fsm_state494() {
    ap_CS_fsm_state494 = ap_CS_fsm.read()[491];
}

void mnist_fp16::thread_ap_CS_fsm_state495() {
    ap_CS_fsm_state495 = ap_CS_fsm.read()[492];
}

void mnist_fp16::thread_ap_CS_fsm_state50() {
    ap_CS_fsm_state50 = ap_CS_fsm.read()[47];
}

void mnist_fp16::thread_ap_CS_fsm_state51() {
    ap_CS_fsm_state51 = ap_CS_fsm.read()[48];
}

void mnist_fp16::thread_ap_CS_fsm_state52() {
    ap_CS_fsm_state52 = ap_CS_fsm.read()[49];
}

void mnist_fp16::thread_ap_CS_fsm_state53() {
    ap_CS_fsm_state53 = ap_CS_fsm.read()[50];
}

void mnist_fp16::thread_ap_CS_fsm_state54() {
    ap_CS_fsm_state54 = ap_CS_fsm.read()[51];
}

void mnist_fp16::thread_ap_CS_fsm_state55() {
    ap_CS_fsm_state55 = ap_CS_fsm.read()[52];
}

void mnist_fp16::thread_ap_CS_fsm_state56() {
    ap_CS_fsm_state56 = ap_CS_fsm.read()[53];
}

void mnist_fp16::thread_ap_CS_fsm_state57() {
    ap_CS_fsm_state57 = ap_CS_fsm.read()[54];
}

void mnist_fp16::thread_ap_CS_fsm_state58() {
    ap_CS_fsm_state58 = ap_CS_fsm.read()[55];
}

void mnist_fp16::thread_ap_CS_fsm_state59() {
    ap_CS_fsm_state59 = ap_CS_fsm.read()[56];
}

void mnist_fp16::thread_ap_CS_fsm_state60() {
    ap_CS_fsm_state60 = ap_CS_fsm.read()[57];
}

void mnist_fp16::thread_ap_CS_fsm_state7() {
    ap_CS_fsm_state7 = ap_CS_fsm.read()[5];
}

void mnist_fp16::thread_ap_CS_fsm_state77() {
    ap_CS_fsm_state77 = ap_CS_fsm.read()[74];
}

void mnist_fp16::thread_ap_CS_fsm_state78() {
    ap_CS_fsm_state78 = ap_CS_fsm.read()[75];
}

void mnist_fp16::thread_ap_CS_fsm_state79() {
    ap_CS_fsm_state79 = ap_CS_fsm.read()[76];
}

void mnist_fp16::thread_ap_CS_fsm_state80() {
    ap_CS_fsm_state80 = ap_CS_fsm.read()[77];
}

void mnist_fp16::thread_ap_CS_fsm_state81() {
    ap_CS_fsm_state81 = ap_CS_fsm.read()[78];
}

void mnist_fp16::thread_ap_CS_fsm_state82() {
    ap_CS_fsm_state82 = ap_CS_fsm.read()[79];
}

void mnist_fp16::thread_ap_CS_fsm_state87() {
    ap_CS_fsm_state87 = ap_CS_fsm.read()[84];
}

void mnist_fp16::thread_ap_CS_fsm_state88() {
    ap_CS_fsm_state88 = ap_CS_fsm.read()[85];
}

void mnist_fp16::thread_ap_CS_fsm_state89() {
    ap_CS_fsm_state89 = ap_CS_fsm.read()[86];
}

void mnist_fp16::thread_ap_CS_fsm_state90() {
    ap_CS_fsm_state90 = ap_CS_fsm.read()[87];
}

void mnist_fp16::thread_ap_CS_fsm_state91() {
    ap_CS_fsm_state91 = ap_CS_fsm.read()[88];
}

void mnist_fp16::thread_ap_CS_fsm_state92() {
    ap_CS_fsm_state92 = ap_CS_fsm.read()[89];
}

void mnist_fp16::thread_ap_CS_fsm_state93() {
    ap_CS_fsm_state93 = ap_CS_fsm.read()[90];
}

void mnist_fp16::thread_ap_CS_fsm_state94() {
    ap_CS_fsm_state94 = ap_CS_fsm.read()[91];
}

void mnist_fp16::thread_ap_CS_fsm_state95() {
    ap_CS_fsm_state95 = ap_CS_fsm.read()[92];
}

void mnist_fp16::thread_ap_CS_fsm_state96() {
    ap_CS_fsm_state96 = ap_CS_fsm.read()[93];
}

void mnist_fp16::thread_ap_CS_fsm_state97() {
    ap_CS_fsm_state97 = ap_CS_fsm.read()[94];
}

void mnist_fp16::thread_ap_CS_fsm_state98() {
    ap_CS_fsm_state98 = ap_CS_fsm.read()[95];
}

void mnist_fp16::thread_ap_CS_fsm_state99() {
    ap_CS_fsm_state99 = ap_CS_fsm.read()[96];
}

void mnist_fp16::thread_ap_block_pp1_stage0() {
    ap_block_pp1_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp16::thread_ap_block_pp1_stage0_11001() {
    ap_block_pp1_stage0_11001 = (esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op586_read_state6.read()));
}

void mnist_fp16::thread_ap_block_pp1_stage0_subdone() {
    ap_block_pp1_stage0_subdone = (esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op586_read_state6.read()));
}

void mnist_fp16::thread_ap_block_pp2_stage0() {
    ap_block_pp2_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp16::thread_ap_block_pp2_stage0_11001() {
    ap_block_pp2_stage0_11001 = (esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_1279.read()) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()));
}

void mnist_fp16::thread_ap_block_pp2_stage0_subdone() {
    ap_block_pp2_stage0_subdone = (esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_1279.read()) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()));
}

void mnist_fp16::thread_ap_block_state5_pp1_stage0_iter0() {
    ap_block_state5_pp1_stage0_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp16::thread_ap_block_state6_pp1_stage0_iter1() {
    ap_block_state6_pp1_stage0_iter1 = (esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op586_read_state6.read()));
}

void mnist_fp16::thread_ap_block_state8_pp2_stage0_iter0() {
    ap_block_state8_pp2_stage0_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp16::thread_ap_block_state9_pp2_stage0_iter1() {
    ap_block_state9_pp2_stage0_iter1 = (esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_1279.read()) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()));
}

void mnist_fp16::thread_ap_condition_4054() {
    ap_condition_4054 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_15378.read()));
}

void mnist_fp16::thread_ap_done() {
    if ((esl_seteq<1,1,1>(tmp_422_fu_15115_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state492.read()))) {
        ap_done = ap_const_logic_1;
    } else {
        ap_done = ap_const_logic_0;
    }
}

void mnist_fp16::thread_ap_enable_pp1() {
    ap_enable_pp1 = (ap_idle_pp1.read() ^ ap_const_logic_1);
}

void mnist_fp16::thread_ap_enable_pp2() {
    ap_enable_pp2 = (ap_idle_pp2.read() ^ ap_const_logic_1);
}

void mnist_fp16::thread_ap_idle() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state1.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void mnist_fp16::thread_ap_idle_pp1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp1_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp1_iter1.read()))) {
        ap_idle_pp1 = ap_const_logic_1;
    } else {
        ap_idle_pp1 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_ap_idle_pp2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp2_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp2_iter1.read()))) {
        ap_idle_pp2 = ap_const_logic_1;
    } else {
        ap_idle_pp2 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_ap_phi_mux_compute15_phi_fu_2755_p4() {
    ap_phi_mux_compute15_phi_fu_2755_p4 = compute15_reg_2751.read();
}

void mnist_fp16::thread_ap_phi_mux_eol_2_phi_fu_1282_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp2_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_1279.read()))) {
        ap_phi_mux_eol_2_phi_fu_1282_p4 = stream_in_V_last_V_0_data_out.read();
    } else {
        ap_phi_mux_eol_2_phi_fu_1282_p4 = eol_2_reg_1279.read();
    }
}

void mnist_fp16::thread_ap_phi_mux_eol_phi_fu_1224_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_15378.read()))) {
        ap_phi_mux_eol_phi_fu_1224_p4 = ap_phi_mux_pixel_last_V_2_phi_fu_1259_p4.read();
    } else {
        ap_phi_mux_eol_phi_fu_1224_p4 = eol_reg_1220.read();
    }
}

void mnist_fp16::thread_ap_phi_mux_p_1_phi_fu_1212_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_15378.read()))) {
        ap_phi_mux_p_1_phi_fu_1212_p4 = j_V_reg_15382.read();
    } else {
        ap_phi_mux_p_1_phi_fu_1212_p4 = p_1_reg_1208.read();
    }
}

void mnist_fp16::thread_ap_phi_mux_p_4_phi_fu_1494_p4() {
    ap_phi_mux_p_4_phi_fu_1494_p4 = p_4_reg_1490.read();
}

void mnist_fp16::thread_ap_phi_mux_p_s_phi_fu_1318_p4() {
    ap_phi_mux_p_s_phi_fu_1318_p4 = p_s_reg_1314.read();
}

void mnist_fp16::thread_ap_phi_mux_pixel_data_V_2_phi_fu_1271_p4() {
    if (esl_seteq<1,1,1>(ap_condition_4054.read(), ap_const_boolean_1)) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, brmerge_reg_15387.read())) {
            ap_phi_mux_pixel_data_V_2_phi_fu_1271_p4 = pixel_data_V_1_reg_1243.read();
        } else if (esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_15387.read())) {
            ap_phi_mux_pixel_data_V_2_phi_fu_1271_p4 = stream_in_V_data_V_0_data_out.read();
        } else {
            ap_phi_mux_pixel_data_V_2_phi_fu_1271_p4 = ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_1267.read();
        }
    } else {
        ap_phi_mux_pixel_data_V_2_phi_fu_1271_p4 = ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_1267.read();
    }
}

void mnist_fp16::thread_ap_phi_mux_pixel_last_V_2_phi_fu_1259_p4() {
    if (esl_seteq<1,1,1>(ap_condition_4054.read(), ap_const_boolean_1)) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, brmerge_reg_15387.read())) {
            ap_phi_mux_pixel_last_V_2_phi_fu_1259_p4 = eol_1_reg_1232.read();
        } else if (esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_15387.read())) {
            ap_phi_mux_pixel_last_V_2_phi_fu_1259_p4 = stream_in_V_last_V_0_data_out.read();
        } else {
            ap_phi_mux_pixel_last_V_2_phi_fu_1259_p4 = ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_1254.read();
        }
    } else {
        ap_phi_mux_pixel_last_V_2_phi_fu_1259_p4 = ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_1254.read();
    }
}

void mnist_fp16::thread_ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_1267() {
    ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_1267 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
}

void mnist_fp16::thread_ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_1254() {
    ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_1254 =  (sc_lv<1>) ("X");
}

void mnist_fp16::thread_ap_predicate_op586_read_state6() {
    ap_predicate_op586_read_state6 = (esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_15378.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_15387.read()));
}

void mnist_fp16::thread_ap_ready() {
    if ((esl_seteq<1,1,1>(tmp_422_fu_15115_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state492.read()))) {
        ap_ready = ap_const_logic_1;
    } else {
        ap_ready = ap_const_logic_0;
    }
}

void mnist_fp16::thread_ap_rst_n_inv() {
    ap_rst_n_inv =  (sc_logic) (~ap_rst_n.read());
}

void mnist_fp16::thread_args01_V_fu_5882_p2() {
    args01_V_fu_5882_p2 = (!p_19_reg_1636.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_19_reg_1636.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_args02_V_fu_7613_p2() {
    args02_V_fu_7613_p2 = (!p_36_reg_1912.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_36_reg_1912.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_args03_V_fu_9751_p2() {
    args03_V_fu_9751_p2 = (!p_50_reg_2110.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_50_reg_2110.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_args04_V_fu_11453_p2() {
    args04_V_fu_11453_p2 = (!p_68_reg_2384.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_68_reg_2384.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_args05_V_fu_13578_p2() {
    args05_V_fu_13578_p2 = (!p_82_reg_2580.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_82_reg_2580.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_args0_V_fu_3606_p2() {
    args0_V_fu_3606_p2 = (!p_7_reg_1435.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_7_reg_1435.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_args11_V_fu_5928_p2() {
    args11_V_fu_5928_p2 = (!p_23_reg_1647.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_23_reg_1647.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_args12_V_fu_7659_p2() {
    args12_V_fu_7659_p2 = (!p_41_reg_1923.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_41_reg_1923.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_args13_V_fu_9797_p2() {
    args13_V_fu_9797_p2 = (!p_55_reg_2121.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_55_reg_2121.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_args14_V_fu_11491_p2() {
    args14_V_fu_11491_p2 = (!p_73_reg_2395.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_73_reg_2395.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_args15_V_fu_13616_p2() {
    args15_V_fu_13616_p2 = (!p_84_reg_2591.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_84_reg_2591.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_args1_V_fu_3652_p2() {
    args1_V_fu_3652_p2 = (!p_11_reg_1446.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_11_reg_1446.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_args21_V_fu_5979_p2() {
    args21_V_fu_5979_p2 = (!p_28_reg_1658.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_28_reg_1658.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_args22_V_fu_7710_p2() {
    args22_V_fu_7710_p2 = (!p_47_reg_1934.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_47_reg_1934.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_args23_V_fu_9848_p2() {
    args23_V_fu_9848_p2 = (!p_60_reg_2132.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_60_reg_2132.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_args24_V_fu_11534_p2() {
    args24_V_fu_11534_p2 = (!p_79_reg_2406.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_79_reg_2406.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_args25_V_fu_13659_p2() {
    args25_V_fu_13659_p2 = (!p_87_reg_2602.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_87_reg_2602.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_args2_V_fu_3703_p2() {
    args2_V_fu_3703_p2 = (!p_16_reg_1457.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_16_reg_1457.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_brmerge_fu_2991_p2() {
    brmerge_fu_2991_p2 = (sof_1_fu_570.read() | ap_phi_mux_eol_phi_fu_1224_p4.read());
}

void mnist_fp16::thread_c1_V_fu_10191_p2() {
    c1_V_fu_10191_p2 = (!p_54_reg_2143.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_54_reg_2143.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_c2_V_fu_14002_p2() {
    c2_V_fu_14002_p2 = (!p_83_reg_2613.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_83_reg_2613.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_c3_V_fu_14408_p2() {
    c3_V_fu_14408_p2 = (!p_85_reg_2671.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_85_reg_2671.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_c_V_fu_6322_p2() {
    c_V_fu_6322_p2 = (!p_22_reg_1669.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_22_reg_1669.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_compute14_to_int_fu_15007_p1() {
    compute14_to_int_fu_15007_p1 = compute14_reg_2717.read();
}

void mnist_fp16::thread_conv1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state50.read())) {
        conv1_0_address0 =  (sc_lv<12>) (tmp_588_cast_fu_3718_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state36.read())) {
        conv1_0_address0 =  (sc_lv<12>) (tmp_589_cast_fu_3545_p1.read());
    } else {
        conv1_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16::thread_conv1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state36.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state50.read()))) {
        conv1_0_ce0 = ap_const_logic_1;
    } else {
        conv1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv1_0_load_to_int_fu_3723_p1() {
    conv1_0_load_to_int_fu_3723_p1 = conv1_0_load_reg_15609.read();
}

void mnist_fp16::thread_conv1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond13_fu_3463_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state36.read()))) {
        conv1_0_we0 = ap_const_logic_1;
    } else {
        conv1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv2_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state112.read())) {
        conv2_0_address0 =  (sc_lv<12>) (tmp_640_cast_fu_5994_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state109.read())) {
        conv2_0_address0 =  (sc_lv<12>) (tmp_641_cast_fu_5872_p1.read());
    } else {
        conv2_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16::thread_conv2_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state112.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state109.read()))) {
        conv2_0_ce0 = ap_const_logic_1;
    } else {
        conv2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv2_0_d0() {
    conv2_0_d0 = (!tmp_63_reg_16101.read()[0].is_01())? sc_lv<32>(): ((tmp_63_reg_16101.read()[0].to_bool())? ap_const_lv32_0: f_2_fu_5860_p1.read());
}

void mnist_fp16::thread_conv2_0_load_to_int_fu_5999_p1() {
    conv2_0_load_to_int_fu_5999_p1 = conv2_0_load_reg_16196.read();
}

void mnist_fp16::thread_conv2_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state109.read())) {
        conv2_0_we0 = ap_const_logic_1;
    } else {
        conv2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv3_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state174.read())) {
        conv3_0_address0 =  (sc_lv<11>) (tmp_717_cast_fu_7725_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state159.read())) {
        conv3_0_address0 =  (sc_lv<11>) (tmp_718_cast_fu_7499_p1.read());
    } else {
        conv3_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp16::thread_conv3_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state159.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state174.read()))) {
        conv3_0_ce0 = ap_const_logic_1;
    } else {
        conv3_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv3_0_load_to_int_fu_7730_p1() {
    conv3_0_load_to_int_fu_7730_p1 = conv3_0_load_reg_16692.read();
}

void mnist_fp16::thread_conv3_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond41_fu_7447_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state159.read()))) {
        conv3_0_we0 = ap_const_logic_1;
    } else {
        conv3_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv4_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state234.read())) {
        conv4_0_address0 =  (sc_lv<11>) (tmp_769_cast_fu_9863_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state231.read())) {
        conv4_0_address0 =  (sc_lv<11>) (tmp_770_cast_fu_9741_p1.read());
    } else {
        conv4_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp16::thread_conv4_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state234.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state231.read()))) {
        conv4_0_ce0 = ap_const_logic_1;
    } else {
        conv4_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv4_0_d0() {
    conv4_0_d0 = (!tmp_193_reg_17181.read()[0].is_01())? sc_lv<32>(): ((tmp_193_reg_17181.read()[0].to_bool())? ap_const_lv32_0: f_6_fu_9729_p1.read());
}

void mnist_fp16::thread_conv4_0_load_to_int_fu_9868_p1() {
    conv4_0_load_to_int_fu_9868_p1 = conv4_0_load_reg_17276.read();
}

void mnist_fp16::thread_conv4_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state231.read())) {
        conv4_0_we0 = ap_const_logic_1;
    } else {
        conv4_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv5_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state295.read())) {
        conv5_0_address0 =  (sc_lv<10>) (tmp_827_cast_fu_11549_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state280.read())) {
        conv5_0_address0 =  (sc_lv<10>) (tmp_828_cast_fu_11314_p1.read());
    } else {
        conv5_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16::thread_conv5_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state280.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state295.read()))) {
        conv5_0_ce0 = ap_const_logic_1;
    } else {
        conv5_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv5_0_load_to_int_fu_11554_p1() {
    conv5_0_load_to_int_fu_11554_p1 = conv5_0_load_reg_17787.read();
}

void mnist_fp16::thread_conv5_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond65_fu_11240_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state280.read()))) {
        conv5_0_we0 = ap_const_logic_1;
    } else {
        conv5_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv6_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state354.read())) {
        conv6_0_address0 =  (sc_lv<10>) (tmp_872_cast_fu_13674_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state351.read())) {
        conv6_0_address0 =  (sc_lv<10>) (tmp_873_cast_fu_13568_p1.read());
    } else {
        conv6_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16::thread_conv6_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state354.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state351.read()))) {
        conv6_0_ce0 = ap_const_logic_1;
    } else {
        conv6_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_conv6_0_d0() {
    conv6_0_d0 = (!tmp_374_reg_18296.read()[0].is_01())? sc_lv<32>(): ((tmp_374_reg_18296.read()[0].to_bool())? ap_const_lv32_0: f_12_fu_13556_p1.read());
}

void mnist_fp16::thread_conv6_0_load_to_int_fu_13679_p1() {
    conv6_0_load_to_int_fu_13679_p1 = conv6_0_load_reg_18386.read();
}

void mnist_fp16::thread_conv6_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state351.read())) {
        conv6_0_we0 = ap_const_logic_1;
    } else {
        conv6_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_dense1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state433.read())) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_426_fu_15110_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state401.read())) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_368_fu_15093_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state398.read())) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_348_fu_14984_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state379.read())) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_464_reg_18664.read());
    } else {
        dense1_0_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void mnist_fp16::thread_dense1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state401.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state379.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state398.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state433.read()))) {
        dense1_0_ce0 = ap_const_logic_1;
    } else {
        dense1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_dense1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond89_fu_14710_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state379.read()))) {
        dense1_0_we0 = ap_const_logic_1;
    } else {
        dense1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_exitcond11_fu_4809_p2() {
    exitcond11_fu_4809_p2 = (!p_10_reg_1543.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_10_reg_1543.read() == ap_const_lv5_1C);
}

void mnist_fp16::thread_exitcond12_fu_3646_p2() {
    exitcond12_fu_3646_p2 = (!p_11_reg_1446.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_11_reg_1446.read() == ap_const_lv5_1C);
}

void mnist_fp16::thread_exitcond13_fu_3463_p2() {
    exitcond13_fu_3463_p2 = (!p_12_reg_1388.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_12_reg_1388.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond14_fu_6889_p2() {
    exitcond14_fu_6889_p2 = (!p_13_reg_1749.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_13_reg_1749.read() == ap_const_lv3_4);
}

void mnist_fp16::thread_exitcond15_fu_4180_p2() {
    exitcond15_fu_4180_p2 = (!p_14_reg_1502.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1502.read() == ap_const_lv5_1E);
}

void mnist_fp16::thread_exitcond16_fu_7326_p2() {
    exitcond16_fu_7326_p2 = (!p_15_reg_1806.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_15_reg_1806.read() == ap_const_lv4_8);
}

void mnist_fp16::thread_exitcond17_fu_3697_p2() {
    exitcond17_fu_3697_p2 = (!p_16_reg_1457.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_16_reg_1457.read() == ap_const_lv5_1C);
}

void mnist_fp16::thread_exitcond18_fu_3550_p2() {
    exitcond18_fu_3550_p2 = (!p_17_reg_1412.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_17_reg_1412.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond19_fu_4860_p2() {
    exitcond19_fu_4860_p2 = (!p_18_reg_1555.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_18_reg_1555.read() == ap_const_lv5_1C);
}

void mnist_fp16::thread_exitcond1_fu_2934_p2() {
    exitcond1_fu_2934_p2 = (!p_0_reg_1197.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_0_reg_1197.read() == ap_const_lv5_1C);
}

void mnist_fp16::thread_exitcond20_fu_5876_p2() {
    exitcond20_fu_5876_p2 = (!p_19_reg_1636.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_19_reg_1636.read() == ap_const_lv3_4);
}

void mnist_fp16::thread_exitcond21_fu_6913_p2() {
    exitcond21_fu_6913_p2 = (!p_20_reg_1771.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_20_reg_1771.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond22_fu_8057_p2() {
    exitcond22_fu_8057_p2 = (!p_21_reg_1945.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_21_reg_1945.read() == ap_const_lv4_8);
}

void mnist_fp16::thread_exitcond23_fu_6316_p2() {
    exitcond23_fu_6316_p2 = (!p_22_reg_1669.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_22_reg_1669.read() == ap_const_lv3_4);
}

void mnist_fp16::thread_exitcond24_fu_5922_p2() {
    exitcond24_fu_5922_p2 = (!p_23_reg_1647.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_23_reg_1647.read() == ap_const_lv5_1C);
}

void mnist_fp16::thread_exitcond25_fu_4872_p2() {
    exitcond25_fu_4872_p2 = (!p_24_reg_1579.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_24_reg_1579.read() == ap_const_lv3_4);
}

void mnist_fp16::thread_exitcond26_fu_7384_p2() {
    exitcond26_fu_7384_p2 = (!p_25_reg_1817.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_25_reg_1817.read() == ap_const_lv4_E);
}

void mnist_fp16::thread_exitcond27_fu_8693_p2() {
    exitcond27_fu_8693_p2 = (!p_26_reg_2005.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_26_reg_2005.read() == ap_const_lv4_8);
}

void mnist_fp16::thread_exitcond28_fu_6396_p2() {
    exitcond28_fu_6396_p2 = (!p_27_reg_1680.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_27_reg_1680.read() == ap_const_lv4_E);
}

void mnist_fp16::thread_exitcond29_fu_5973_p2() {
    exitcond29_fu_5973_p2 = (!p_28_reg_1658.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_28_reg_1658.read() == ap_const_lv5_1C);
}

void mnist_fp16::thread_exitcond30_fu_4955_p2() {
    exitcond30_fu_4955_p2 = (!p_29_reg_1602.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_29_reg_1602.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond31_fu_7001_p2() {
    exitcond31_fu_7001_p2 = (!p_30_reg_1782.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_30_reg_1782.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond32_fu_8081_p2() {
    exitcond32_fu_8081_p2 = (!p_31_reg_1967.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_31_reg_1967.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond33_fu_7435_p2() {
    exitcond33_fu_7435_p2 = (!p_32_reg_1829.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_32_reg_1829.read() == ap_const_lv4_E);
}

void mnist_fp16::thread_exitcond34_fu_6455_p2() {
    exitcond34_fu_6455_p2 = (!p_33_reg_1691.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_33_reg_1691.read() == ap_const_lv4_E);
}

void mnist_fp16::thread_exitcond35_fu_5047_p2() {
    exitcond35_fu_5047_p2 = (!p_34_reg_1625.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_34_reg_1625.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond36_fu_8751_p2() {
    exitcond36_fu_8751_p2 = (!p_35_reg_2016.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_35_reg_2016.read() == ap_const_lv4_E);
}

void mnist_fp16::thread_exitcond37_fu_7607_p2() {
    exitcond37_fu_7607_p2 = (!p_36_reg_1912.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_36_reg_1912.read() == ap_const_lv4_8);
}

void mnist_fp16::thread_exitcond38_fu_10742_p2() {
    exitcond38_fu_10742_p2 = (!p_37_reg_2223.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_37_reg_2223.read() == ap_const_lv4_8);
}

void mnist_fp16::thread_exitcond39_fu_6489_p2() {
    exitcond39_fu_6489_p2 = (!p_38_reg_1715.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_38_reg_1715.read() == ap_const_lv2_2);
}

void mnist_fp16::thread_exitcond3_fu_3336_p2() {
    exitcond3_fu_3336_p2 = (!p_2_reg_1353.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_2_reg_1353.read() == ap_const_lv3_4);
}

void mnist_fp16::thread_exitcond40_fu_8169_p2() {
    exitcond40_fu_8169_p2 = (!p_39_reg_1978.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_39_reg_1978.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond41_fu_7447_p2() {
    exitcond41_fu_7447_p2 = (!p_40_reg_1841.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_40_reg_1841.read() == ap_const_lv3_4);
}

void mnist_fp16::thread_exitcond42_fu_7653_p2() {
    exitcond42_fu_7653_p2 = (!p_41_reg_1923.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_41_reg_1923.read() == ap_const_lv4_E);
}

void mnist_fp16::thread_exitcond43_fu_11139_p2() {
    exitcond43_fu_11139_p2 = (!p_42_reg_2280.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_42_reg_2280.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond44_fu_6549_p2() {
    exitcond44_fu_6549_p2 = (!p_43_reg_1738.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_43_reg_1738.read() == ap_const_lv2_2);
}

void mnist_fp16::thread_exitcond45_fu_8802_p2() {
    exitcond45_fu_8802_p2 = (!p_44_reg_2028.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_44_reg_2028.read() == ap_const_lv4_E);
}

void mnist_fp16::thread_exitcond46_fu_10776_p2() {
    exitcond46_fu_10776_p2 = (!p_45_reg_2245.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_45_reg_2245.read() == ap_const_lv4_9);
}

void mnist_fp16::thread_exitcond47_fu_7504_p2() {
    exitcond47_fu_7504_p2 = (!p_46_reg_1866.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_46_reg_1866.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond48_fu_7704_p2() {
    exitcond48_fu_7704_p2 = (!p_47_reg_1934.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_47_reg_1934.read() == ap_const_lv4_E);
}

void mnist_fp16::thread_exitcond49_fu_11881_p2() {
    exitcond49_fu_11881_p2 = (!p_48_reg_2417.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_48_reg_2417.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond4_fu_3081_p2() {
    exitcond4_fu_3081_p2 = (!p_3_reg_1326.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_1326.read() == ap_const_lv5_1E);
}

void mnist_fp16::thread_exitcond50_fu_11177_p2() {
    exitcond50_fu_11177_p2 = (!p_49_reg_2291.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_49_reg_2291.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond51_fu_9745_p2() {
    exitcond51_fu_9745_p2 = (!p_50_reg_2110.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_50_reg_2110.read() == ap_const_lv4_8);
}

void mnist_fp16::thread_exitcond52_fu_8814_p2() {
    exitcond52_fu_8814_p2 = (!p_51_reg_2052.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_51_reg_2052.read() == ap_const_lv4_8);
}

void mnist_fp16::thread_exitcond53_fu_7557_p2() {
    exitcond53_fu_7557_p2 = (!p_52_reg_1889.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_52_reg_1889.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond54_fu_12481_p2() {
    exitcond54_fu_12481_p2 = (!p_53_reg_2477.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_53_reg_2477.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond55_fu_10185_p2() {
    exitcond55_fu_10185_p2 = (!p_54_reg_2143.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_54_reg_2143.read() == ap_const_lv4_8);
}

void mnist_fp16::thread_exitcond56_fu_9791_p2() {
    exitcond56_fu_9791_p2 = (!p_55_reg_2121.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_55_reg_2121.read() == ap_const_lv4_E);
}

void mnist_fp16::thread_exitcond57_fu_10848_p2() {
    exitcond57_fu_10848_p2 = (!p_56_reg_2256.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_56_reg_2256.read() == ap_const_lv4_9);
}

void mnist_fp16::thread_exitcond58_fu_11915_p2() {
    exitcond58_fu_11915_p2 = (!p_57_reg_2439.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_57_reg_2439.read() == ap_const_lv4_9);
}

void mnist_fp16::thread_exitcond59_fu_11224_p2() {
    exitcond59_fu_11224_p2 = (!p_58_reg_2302.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_58_reg_2302.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond5_fu_4759_p2() {
    exitcond5_fu_4759_p2 = (!p_9_reg_1532.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_9_reg_1532.read() == ap_const_lv3_4);
}

void mnist_fp16::thread_exitcond60_fu_10257_p2() {
    exitcond60_fu_10257_p2 = (!p_59_reg_2154.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_59_reg_2154.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond61_fu_9842_p2() {
    exitcond61_fu_9842_p2 = (!p_60_reg_2132.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_60_reg_2132.read() == ap_const_lv4_E);
}

void mnist_fp16::thread_exitcond62_fu_8863_p2() {
    exitcond62_fu_8863_p2 = (!p_61_reg_2076.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_61_reg_2076.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond63_fu_12531_p2() {
    exitcond63_fu_12531_p2 = (!p_62_reg_2488.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_62_reg_2488.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond64_fu_11991_p2() {
    exitcond64_fu_11991_p2 = (!p_63_reg_2450.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_63_reg_2450.read() == ap_const_lv4_9);
}

void mnist_fp16::thread_exitcond65_fu_11240_p2() {
    exitcond65_fu_11240_p2 = (!p_64_reg_2314.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_64_reg_2314.read() == ap_const_lv4_8);
}

void mnist_fp16::thread_exitcond66_fu_10308_p2() {
    exitcond66_fu_10308_p2 = (!p_65_reg_2165.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_65_reg_2165.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond67_fu_8916_p2() {
    exitcond67_fu_8916_p2 = (!p_66_reg_2099.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_66_reg_2099.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond68_fu_12578_p2() {
    exitcond68_fu_12578_p2 = (!p_67_reg_2499.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_67_reg_2499.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond69_fu_11447_p2() {
    exitcond69_fu_11447_p2 = (!p_68_reg_2384.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_68_reg_2384.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond6_fu_3400_p2() {
    exitcond6_fu_3400_p2 = (!p_5_reg_1364.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_5_reg_1364.read() == ap_const_lv5_1C);
}

void mnist_fp16::thread_exitcond70_fu_14972_p2() {
    exitcond70_fu_14972_p2 = (!p_69_reg_2729.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_69_reg_2729.read() == ap_const_lv4_A);
}

void mnist_fp16::thread_exitcond71_fu_10342_p2() {
    exitcond71_fu_10342_p2 = (!p_70_reg_2189.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_70_reg_2189.read() == ap_const_lv2_2);
}

void mnist_fp16::thread_exitcond72_fu_11319_p2() {
    exitcond72_fu_11319_p2 = (!p_71_reg_2338.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_71_reg_2338.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond73_fu_15081_p2() {
    exitcond73_fu_15081_p2 = (!p_72_reg_2740.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_72_reg_2740.read() == ap_const_lv4_A);
}

void mnist_fp16::thread_exitcond74_fu_11485_p2() {
    exitcond74_fu_11485_p2 = (!p_73_reg_2395.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_73_reg_2395.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond75_fu_12594_p2() {
    exitcond75_fu_12594_p2 = (!p_74_reg_2523.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_74_reg_2523.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond76_fu_10402_p2() {
    exitcond76_fu_10402_p2 = (!p_75_reg_2212.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_75_reg_2212.read() == ap_const_lv2_2);
}

void mnist_fp16::thread_exitcond77_fu_11398_p2() {
    exitcond77_fu_11398_p2 = (!p_76_reg_2361.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_76_reg_2361.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond78_fu_12674_p2() {
    exitcond78_fu_12674_p2 = (!p_78_reg_2546.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_78_reg_2546.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond79_fu_12753_p2() {
    exitcond79_fu_12753_p2 = (!p_81_reg_2569.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_81_reg_2569.read() == ap_const_lv2_3);
}

void mnist_fp16::thread_exitcond7_fu_4050_p2() {
    exitcond7_fu_4050_p2 = (!p_6_reg_1468.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_6_reg_1468.read() == ap_const_lv3_4);
}

void mnist_fp16::thread_exitcond80_fu_11528_p2() {
    exitcond80_fu_11528_p2 = (!p_79_reg_2406.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_79_reg_2406.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond81_fu_13572_p2() {
    exitcond81_fu_13572_p2 = (!p_82_reg_2580.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_82_reg_2580.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond82_fu_13996_p2() {
    exitcond82_fu_13996_p2 = (!p_83_reg_2613.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_83_reg_2613.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond83_fu_13610_p2() {
    exitcond83_fu_13610_p2 = (!p_84_reg_2591.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_84_reg_2591.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond84_fu_14402_p2() {
    exitcond84_fu_14402_p2 = (!p_85_reg_2671.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_85_reg_2671.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond85_fu_14038_p2() {
    exitcond85_fu_14038_p2 = (!p_86_reg_2637.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_86_reg_2637.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond86_fu_13653_p2() {
    exitcond86_fu_13653_p2 = (!p_87_reg_2602.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_87_reg_2602.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond87_fu_14690_p2() {
    exitcond87_fu_14690_p2 = (!p_88_reg_2682.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_88_reg_2682.read() == ap_const_lv4_A);
}

void mnist_fp16::thread_exitcond88_fu_14081_p2() {
    exitcond88_fu_14081_p2 = (!p_89_reg_2660.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_89_reg_2660.read() == ap_const_lv3_7);
}

void mnist_fp16::thread_exitcond89_fu_14710_p2() {
    exitcond89_fu_14710_p2 = (!p_90_reg_2693.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_90_reg_2693.read() == ap_const_lv5_10);
}

void mnist_fp16::thread_exitcond8_fu_3600_p2() {
    exitcond8_fu_3600_p2 = (!p_7_reg_1435.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_7_reg_1435.read() == ap_const_lv3_4);
}

void mnist_fp16::thread_exitcond9_fu_3451_p2() {
    exitcond9_fu_3451_p2 = (!p_8_reg_1376.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_8_reg_1376.read() == ap_const_lv5_1C);
}

void mnist_fp16::thread_exitcond_fu_15098_p2() {
    exitcond_fu_15098_p2 = (!p_77_reg_2763.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_77_reg_2763.read() == ap_const_lv4_A);
}

void mnist_fp16::thread_f_10_fu_12463_p1() {
    f_10_fu_12463_p1 = p_Result_81_fu_12452_p5.read();
}

void mnist_fp16::thread_f_12_fu_13556_p1() {
    f_12_fu_13556_p1 = p_Result_86_fu_13545_p5.read();
}

void mnist_fp16::thread_f_14_fu_14301_p1() {
    f_14_fu_14301_p1 = p_Result_56_fu_14290_p5.read();
}

void mnist_fp16::thread_f_16_fu_14960_p1() {
    f_16_fu_14960_p1 = p_Result_60_fu_14949_p5.read();
}

void mnist_fp16::thread_f_2_fu_5860_p1() {
    f_2_fu_5860_p1 = p_Result_70_fu_5849_p5.read();
}

void mnist_fp16::thread_f_4_fu_6778_p1() {
    f_4_fu_6778_p1 = p_Result_17_fu_6767_p5.read();
}

void mnist_fp16::thread_f_5_fu_8675_p1() {
    f_5_fu_8675_p1 = p_Result_73_fu_8664_p5.read();
}

void mnist_fp16::thread_f_6_fu_9729_p1() {
    f_6_fu_9729_p1 = p_Result_78_fu_9718_p5.read();
}

void mnist_fp16::thread_f_8_fu_10631_p1() {
    f_8_fu_10631_p1 = p_Result_36_fu_10620_p5.read();
}

void mnist_fp16::thread_f_fu_4741_p1() {
    f_fu_4741_p1 = p_Result_65_fu_4730_p5.read();
}

void mnist_fp16::thread_ff1_V_fu_4765_p2() {
    ff1_V_fu_4765_p2 = (!p_9_reg_1532.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_9_reg_1532.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_ff2_V_fu_7332_p2() {
    ff2_V_fu_7332_p2 = (!p_15_reg_1806.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_15_reg_1806.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_ff3_V_fu_8699_p2() {
    ff3_V_fu_8699_p2 = (!p_26_reg_2005.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_26_reg_2005.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_ff4_V_fu_11145_p2() {
    ff4_V_fu_11145_p2 = (!p_42_reg_2280.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_42_reg_2280.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_ff5_V_fu_12487_p2() {
    ff5_V_fu_12487_p2 = (!p_53_reg_2477.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_53_reg_2477.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_ff_V_fu_3342_p2() {
    ff_V_fu_3342_p2 = (!p_2_reg_1353.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_2_reg_1353.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_grp_fu_11001_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state260.read())) {
        grp_fu_11001_ap_start = ap_const_logic_1;
    } else {
        grp_fu_11001_ap_start = ap_const_logic_0;
    }
}

void mnist_fp16::thread_grp_fu_11001_p0() {
    grp_fu_11001_p0 = (!tmp_752_reg_17571.read()[0].is_01())? sc_lv<10>(): ((tmp_752_reg_17571.read()[0].to_bool())? neg_ti7_fu_10988_p2.read(): tmp_756_fu_10978_p1.read());
}

void mnist_fp16::thread_grp_fu_11001_p1() {
    grp_fu_11001_p1 =  (sc_lv<4>) (ap_const_lv10_7);
}

void mnist_fp16::thread_grp_fu_12144_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state305.read())) {
        grp_fu_12144_ap_start = ap_const_logic_1;
    } else {
        grp_fu_12144_ap_start = ap_const_logic_0;
    }
}

void mnist_fp16::thread_grp_fu_12144_p0() {
    grp_fu_12144_p0 = (!tmp_824_reg_17932.read()[0].is_01())? sc_lv<11>(): ((tmp_824_reg_17932.read()[0].to_bool())? neg_ti9_fu_12131_p2.read(): tmp_828_fu_12121_p1.read());
}

void mnist_fp16::thread_grp_fu_12144_p1() {
    grp_fu_12144_p1 =  (sc_lv<4>) (ap_const_lv11_7);
}

void mnist_fp16::thread_grp_fu_15253_p2() {
    grp_fu_15253_p2 = esl_concat<16,14>(p_Val2_24_reg_1613.read(), ap_const_lv14_0);
}

void mnist_fp16::thread_grp_fu_15294_p2() {
    grp_fu_15294_p2 = esl_concat<16,14>(p_Val2_37_reg_2087.read(), ap_const_lv14_0);
}

void mnist_fp16::thread_grp_fu_15335_p2() {
    grp_fu_15335_p2 = esl_concat<16,14>(p_Val2_50_reg_2557.read(), ap_const_lv14_0);
}

void mnist_fp16::thread_grp_fu_2811_opcode() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state403.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state435.read()))) {
        grp_fu_2811_opcode = ap_const_lv2_1;
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state43.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state167.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state288.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state393.read()))) {
        grp_fu_2811_opcode = ap_const_lv2_0;
    } else {
        grp_fu_2811_opcode =  (sc_lv<2>) ("XX");
    }
}

void mnist_fp16::thread_grp_fu_2811_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state403.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state435.read()))) {
        grp_fu_2811_p0 = reg_2900.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state43.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state167.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state288.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state393.read()))) {
        grp_fu_2811_p0 = reg_2886.read();
    } else {
        grp_fu_2811_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2811_p1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state403.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state435.read()))) {
        grp_fu_2811_p1 = compute14_reg_2717.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state393.read())) {
        grp_fu_2811_p1 = reducer6_reg_2704.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state288.read())) {
        grp_fu_2811_p1 = reducer90_2_reg_2372.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state167.read())) {
        grp_fu_2811_p1 = reducer87_2_reg_1900.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state43.read())) {
        grp_fu_2811_p1 = reducer84_1_reg_1423.read();
    } else {
        grp_fu_2811_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2820_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state389.read())) {
        grp_fu_2820_p0 = p_03_i6_fu_14964_p3.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state284.read())) {
        grp_fu_2820_p0 = pad_temp4_0_load_reg_17733.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state163.read())) {
        grp_fu_2820_p0 = pad_temp2_0_load_reg_16638.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state39.read())) {
        grp_fu_2820_p0 = pad_temp_0_0_load_reg_15555.read();
    } else {
        grp_fu_2820_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2820_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state389.read())) {
        grp_fu_2820_p1 = w_dense_1_load_reg_18755.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state284.read())) {
        grp_fu_2820_p1 = w_conv5_load_reg_17738.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state163.read())) {
        grp_fu_2820_p1 = w_conv3_load_reg_16643.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state39.read())) {
        grp_fu_2820_p1 = w_conv1_load_reg_15560.read();
    } else {
        grp_fu_2820_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2824_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state383.read())) {
        grp_fu_2824_p0 = p_012_0_i9_reg_18735.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state366.read())) {
        grp_fu_2824_p0 = p_012_0_i8_reg_18551.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state345.read())) {
        grp_fu_2824_p0 = tmp32_V_47_reg_18327.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state324.read())) {
        grp_fu_2824_p0 = tmp32_V_45_reg_18026.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state248.read())) {
        grp_fu_2824_p0 = p_012_0_i5_reg_17477.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state225.read())) {
        grp_fu_2824_p0 = tmp32_V_42_reg_17212.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state204.read())) {
        grp_fu_2824_p0 = tmp32_V_38_reg_16931.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state126.read())) {
        grp_fu_2824_p0 = p_012_0_i2_reg_16397.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state103.read())) {
        grp_fu_2824_p0 = tmp32_V_28_reg_16132.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state82.read())) {
        grp_fu_2824_p0 = tmp32_V_12_reg_15846.read();
    } else {
        grp_fu_2824_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2827_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state490.read())) {
        grp_fu_2827_p0 = tmp_434_reg_18824.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state432.read())) {
        grp_fu_2827_p0 = tmp_373_reg_18796.read();
    } else {
        grp_fu_2827_p0 =  (sc_lv<64>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2830_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state426.read())) {
        grp_fu_2830_p0 = compute15_reg_2751.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state408.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state440.read()))) {
        grp_fu_2830_p0 = reg_2891.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state401.read())) {
        grp_fu_2830_p0 = ap_phi_mux_compute15_phi_fu_2755_p4.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state375.read())) {
        grp_fu_2830_p0 = max_pool_0_0_0_q0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state357.read())) {
        grp_fu_2830_p0 = conv6_0_load_reg_18386.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state339.read())) {
        grp_fu_2830_p0 = pad_temp5_0_load_reg_18159.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state298.read())) {
        grp_fu_2830_p0 = conv5_0_load_reg_17787.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state237.read())) {
        grp_fu_2830_p0 = conv4_0_load_reg_17276.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state219.read())) {
        grp_fu_2830_p0 = pad_temp3_0_load_reg_17044.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state177.read())) {
        grp_fu_2830_p0 = conv3_0_load_reg_16692.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state115.read())) {
        grp_fu_2830_p0 = conv2_0_load_reg_16196.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state97.read())) {
        grp_fu_2830_p0 = pad_temp1_0_load_reg_15964.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state53.read())) {
        grp_fu_2830_p0 = conv1_0_load_reg_15609.read();
    } else {
        grp_fu_2830_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2833_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state339.read())) {
        grp_fu_2833_p0 = w_conv6_load_reg_18164.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state219.read())) {
        grp_fu_2833_p0 = w_conv4_load_reg_17049.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state97.read())) {
        grp_fu_2833_p0 = w_conv2_load_reg_15969.read();
    } else {
        grp_fu_2833_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2839_opcode() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state494.read())) {
        grp_fu_2839_opcode = ap_const_lv5_2;
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state52.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state114.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state133.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state176.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state236.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state255.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state297.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state356.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state373.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state400.read()))) {
        grp_fu_2839_opcode = ap_const_lv5_4;
    } else {
        grp_fu_2839_opcode =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2839_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state494.read())) {
        grp_fu_2839_p0 = pred_2_reg_18842.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state400.read())) {
        grp_fu_2839_p0 = reg_2900.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state373.read())) {
        grp_fu_2839_p0 = p_03_i5_reg_18566.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state356.read())) {
        grp_fu_2839_p0 = conv6_0_load_reg_18386.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state297.read())) {
        grp_fu_2839_p0 = conv5_0_load_reg_17787.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state255.read())) {
        grp_fu_2839_p0 = p_03_i3_reg_17492.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state236.read())) {
        grp_fu_2839_p0 = conv4_0_load_reg_17276.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state176.read())) {
        grp_fu_2839_p0 = conv3_0_load_reg_16692.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state133.read())) {
        grp_fu_2839_p0 = p_03_i1_reg_16412.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state114.read())) {
        grp_fu_2839_p0 = conv2_0_load_reg_16196.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state52.read())) {
        grp_fu_2839_p0 = conv1_0_load_reg_15609.read();
    } else {
        grp_fu_2839_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2839_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state494.read())) {
        grp_fu_2839_p1 = pred_reg_2799.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state400.read())) {
        grp_fu_2839_p1 = compute14_reg_2717.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state373.read())) {
        grp_fu_2839_p1 = tmp_457_reg_2648.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state255.read())) {
        grp_fu_2839_p1 = tmp_318_reg_2200.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state133.read())) {
        grp_fu_2839_p1 = tmp_140_reg_1726.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state52.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state114.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state176.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state236.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state297.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state356.read()))) {
        grp_fu_2839_p1 = ap_const_lv32_0;
    } else {
        grp_fu_2839_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2857_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state441.read())) {
        grp_fu_2857_p1 = tmp_428_reg_18819.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state409.read())) {
        grp_fu_2857_p1 = reg_2906.read();
    } else {
        grp_fu_2857_p1 =  (sc_lv<64>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2872_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state12.read())) {
        grp_fu_2872_p0 = p_s_reg_1314.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state11.read())) {
        grp_fu_2872_p0 = ap_phi_mux_p_s_phi_fu_1318_p4.read();
    } else {
        grp_fu_2872_p0 =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2872_p2() {
    grp_fu_2872_p2 = (!grp_fu_2872_p0.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(grp_fu_2872_p0.read() == ap_const_lv5_1E);
}

void mnist_fp16::thread_grp_fu_2879_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state58.read())) {
        grp_fu_2879_p0 = p_4_reg_1490.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state57.read())) {
        grp_fu_2879_p0 = ap_phi_mux_p_4_phi_fu_1494_p4.read();
    } else {
        grp_fu_2879_p0 =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp16::thread_grp_fu_2879_p2() {
    grp_fu_2879_p2 = (!grp_fu_2879_p0.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(grp_fu_2879_p0.read() == ap_const_lv5_1E);
}

void mnist_fp16::thread_grp_fu_3285_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state14.read())) {
        grp_fu_3285_ap_start = ap_const_logic_1;
    } else {
        grp_fu_3285_ap_start = ap_const_logic_0;
    }
}

void mnist_fp16::thread_grp_fu_3285_p0() {
    grp_fu_3285_p0 =  (sc_lv<12>) (grp_fu_3285_p00.read());
}

void mnist_fp16::thread_grp_fu_3285_p00() {
    grp_fu_3285_p00 = (!tmp_68_reg_15444.read()[0].is_01())? sc_lv<11>(): ((tmp_68_reg_15444.read()[0].to_bool())? neg_ti_fu_3272_p2.read(): tmp_77_fu_3262_p1.read());
}

void mnist_fp16::thread_grp_fu_3285_p1() {
    grp_fu_3285_p1 =  (sc_lv<6>) (ap_const_lv11_1C);
}

void mnist_fp16::thread_grp_fu_4406_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state60.read())) {
        grp_fu_4406_ap_start = ap_const_logic_1;
    } else {
        grp_fu_4406_ap_start = ap_const_logic_0;
    }
}

void mnist_fp16::thread_grp_fu_4406_p0() {
    grp_fu_4406_p0 =  (sc_lv<14>) (grp_fu_4406_p00.read());
}

void mnist_fp16::thread_grp_fu_4406_p00() {
    grp_fu_4406_p00 = (!tmp_264_reg_15757.read()[0].is_01())? sc_lv<13>(): ((tmp_264_reg_15757.read()[0].to_bool())? neg_ti1_fu_4393_p2.read(): tmp_269_fu_4383_p1.read());
}

void mnist_fp16::thread_grp_fu_4406_p1() {
    grp_fu_4406_p1 =  (sc_lv<6>) (ap_const_lv13_1C);
}

void mnist_fp16::thread_grp_fu_7172_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state138.read())) {
        grp_fu_7172_ap_start = ap_const_logic_1;
    } else {
        grp_fu_7172_ap_start = ap_const_logic_0;
    }
}

void mnist_fp16::thread_grp_fu_7172_p0() {
    grp_fu_7172_p0 = (!tmp_574_reg_16496.read()[0].is_01())? sc_lv<11>(): ((tmp_574_reg_16496.read()[0].to_bool())? neg_ti3_fu_7159_p2.read(): tmp_578_fu_7149_p1.read());
}

void mnist_fp16::thread_grp_fu_7172_p1() {
    grp_fu_7172_p1 =  (sc_lv<5>) (ap_const_lv11_E);
}

void mnist_fp16::thread_grp_fu_8340_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state184.read())) {
        grp_fu_8340_ap_start = ap_const_logic_1;
    } else {
        grp_fu_8340_ap_start = ap_const_logic_0;
    }
}

void mnist_fp16::thread_grp_fu_8340_p0() {
    grp_fu_8340_p0 = (!tmp_649_reg_16842.read()[0].is_01())? sc_lv<12>(): ((tmp_649_reg_16842.read()[0].to_bool())? neg_ti5_fu_8327_p2.read(): tmp_653_fu_8317_p1.read());
}

void mnist_fp16::thread_grp_fu_8340_p1() {
    grp_fu_8340_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp16::thread_h1_V_fu_10263_p2() {
    h1_V_fu_10263_p2 = (!p_59_reg_2154.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_59_reg_2154.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_h_V_fu_6402_p2() {
    h_V_fu_6402_p2 = (!p_27_reg_1680.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_27_reg_1680.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_i1_V_fu_4186_p2() {
    i1_V_fu_4186_p2 = (!p_14_reg_1502.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_14_reg_1502.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_i3_V_fu_7007_p2() {
    i3_V_fu_7007_p2 = (!p_30_reg_1782.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_30_reg_1782.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_i4_V_fu_8175_p2() {
    i4_V_fu_8175_p2 = (!p_39_reg_1978.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_39_reg_1978.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_i6_V_fu_10854_p2() {
    i6_V_fu_10854_p2 = (!p_56_reg_2256.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_56_reg_2256.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_i7_V_fu_11997_p2() {
    i7_V_fu_11997_p2 = (!p_63_reg_2450.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_63_reg_2450.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_i_V_1_fu_3087_p2() {
    i_V_1_fu_3087_p2 = (!p_3_reg_1326.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_3_reg_1326.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_i_V_2_fu_15121_p2() {
    i_V_2_fu_15121_p2 = (!index_V_reg_2787.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(index_V_reg_2787.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_i_V_fu_2940_p2() {
    i_V_fu_2940_p2 = (!p_0_reg_1197.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_0_reg_1197.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_icmp10_fu_9614_p2() {
    icmp10_fu_9614_p2 = (!tmp_708_fu_9604_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_708_fu_9604_p4.read() == ap_const_lv26_0);
}

void mnist_fp16::thread_icmp11_fu_9117_p2() {
    icmp11_fu_9117_p2 = (!tmp_740_fu_9107_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_740_fu_9107_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp12_fu_9202_p2() {
    icmp12_fu_9202_p2 = (!tmp_747_fu_9192_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_747_fu_9192_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp13_fu_10525_p2() {
    icmp13_fu_10525_p2 = (!tmp_782_fu_10515_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_782_fu_10515_p4.read() == ap_const_lv26_0);
}

void mnist_fp16::thread_icmp14_fu_11715_p2() {
    icmp14_fu_11715_p2 = (!tmp_801_fu_11705_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_801_fu_11705_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp15_fu_12357_p2() {
    icmp15_fu_12357_p2 = (!tmp_852_fu_12347_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_852_fu_12347_p4.read() == ap_const_lv26_0);
}

void mnist_fp16::thread_icmp16_fu_14533_p2() {
    icmp16_fu_14533_p2 = (!tmp_863_fu_14523_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_863_fu_14523_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp17_fu_13840_p2() {
    icmp17_fu_13840_p2 = (!tmp_884_fu_13830_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_884_fu_13830_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp18_fu_13450_p2() {
    icmp18_fu_13450_p2 = (!tmp_897_fu_13440_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_897_fu_13440_p4.read() == ap_const_lv26_0);
}

void mnist_fp16::thread_icmp19_fu_14195_p2() {
    icmp19_fu_14195_p2 = (!tmp_907_fu_14185_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_907_fu_14185_p4.read() == ap_const_lv26_0);
}

void mnist_fp16::thread_icmp1_fu_4635_p2() {
    icmp1_fu_4635_p2 = (!tmp_334_fu_4625_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_334_fu_4625_p4.read() == ap_const_lv26_0);
}

void mnist_fp16::thread_icmp20_fu_14850_p2() {
    icmp20_fu_14850_p2 = (!tmp_922_fu_14840_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_922_fu_14840_p4.read() == ap_const_lv26_0);
}

void mnist_fp16::thread_icmp21_fu_12953_p2() {
    icmp21_fu_12953_p2 = (!tmp_932_fu_12943_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_932_fu_12943_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp22_fu_13038_p2() {
    icmp22_fu_13038_p2 = (!tmp_939_fu_13028_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_939_fu_13028_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp2_fu_6160_p2() {
    icmp2_fu_6160_p2 = (!tmp_375_fu_6150_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_375_fu_6150_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp3_fu_5745_p2() {
    icmp3_fu_5745_p2 = (!tmp_392_fu_5735_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_392_fu_5735_p4.read() == ap_const_lv26_0);
}

void mnist_fp16::thread_icmp4_fu_5248_p2() {
    icmp4_fu_5248_p2 = (!tmp_547_fu_5238_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_547_fu_5238_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp5_fu_5333_p2() {
    icmp5_fu_5333_p2 = (!tmp_563_fu_5323_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_563_fu_5323_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp6_fu_6672_p2() {
    icmp6_fu_6672_p2 = (!tmp_606_fu_6662_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_606_fu_6662_p4.read() == ap_const_lv26_0);
}

void mnist_fp16::thread_icmp7_fu_7891_p2() {
    icmp7_fu_7891_p2 = (!tmp_627_fu_7881_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_627_fu_7881_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp8_fu_8569_p2() {
    icmp8_fu_8569_p2 = (!tmp_683_fu_8559_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_683_fu_8559_p4.read() == ap_const_lv26_0);
}

void mnist_fp16::thread_icmp9_fu_10029_p2() {
    icmp9_fu_10029_p2 = (!tmp_702_fu_10019_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_702_fu_10019_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_icmp_fu_3884_p2() {
    icmp_fu_3884_p2 = (!tmp_149_fu_3874_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_149_fu_3874_p4.read() == ap_const_lv8_0);
}

void mnist_fp16::thread_image_0_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state30.read())) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_570_cast_fu_3318_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_97_cast_fu_3016_p1.read());
    } else {
        image_0_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16::thread_image_0_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state30.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        image_0_0_ce0 = ap_const_logic_1;
    } else {
        image_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_image_0_0_d0() {
    image_0_0_d0 = ap_phi_mux_pixel_data_V_2_phi_fu_1271_p4.read();
}

void mnist_fp16::thread_image_0_0_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_15378.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0))) {
        image_0_0_we0 = ap_const_logic_1;
    } else {
        image_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_index_V_1_fu_15222_p3() {
    index_V_1_fu_15222_p3 = (!tmp_568_reg_18849.read()[0].is_01())? sc_lv<4>(): ((tmp_568_reg_18849.read()[0].to_bool())? index_V_reg_2787.read(): p_80_reg_2774.read());
}

void mnist_fp16::thread_index_tuple1_V_fu_4096_p2() {
    index_tuple1_V_fu_4096_p2 = (!p_4_reg_1490.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_4_reg_1490.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_index_tuple2_V_fu_6919_p2() {
    index_tuple2_V_fu_6919_p2 = (!p_20_reg_1771.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_20_reg_1771.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_index_tuple3_V_fu_8087_p2() {
    index_tuple3_V_fu_8087_p2 = (!p_31_reg_1967.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_31_reg_1967.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_index_tuple4_V_fu_10782_p2() {
    index_tuple4_V_fu_10782_p2 = (!p_45_reg_2245.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_45_reg_2245.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_index_tuple5_V_fu_11921_p2() {
    index_tuple5_V_fu_11921_p2 = (!p_57_reg_2439.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_57_reg_2439.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_index_tuple_V_fu_3021_p2() {
    index_tuple_V_fu_3021_p2 = (!p_s_reg_1314.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_s_reg_1314.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_ireg_V_12_fu_5097_p1() {
    ireg_V_12_fu_5097_p1 = grp_fu_2830_p1.read();
}

void mnist_fp16::thread_ireg_V_13_fu_5133_p1() {
    ireg_V_13_fu_5133_p1 = grp_fu_2833_p1.read();
}

void mnist_fp16::thread_ireg_V_14_fu_8966_p1() {
    ireg_V_14_fu_8966_p1 = grp_fu_2830_p1.read();
}

void mnist_fp16::thread_ireg_V_15_fu_9002_p1() {
    ireg_V_15_fu_9002_p1 = grp_fu_2833_p1.read();
}

void mnist_fp16::thread_ireg_V_16_fu_12802_p1() {
    ireg_V_16_fu_12802_p1 = grp_fu_2830_p1.read();
}

void mnist_fp16::thread_ireg_V_17_fu_12838_p1() {
    ireg_V_17_fu_12838_p1 = grp_fu_2833_p1.read();
}

void mnist_fp16::thread_ireg_V_1_fu_13721_p3() {
    ireg_V_1_fu_13721_p3 = (!tmp_470_fu_13712_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_470_fu_13712_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_473_fu_13717_p1.read());
}

void mnist_fp16::thread_ireg_V_3_fu_6041_p3() {
    ireg_V_3_fu_6041_p3 = (!tmp_122_fu_6032_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_122_fu_6032_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_135_fu_6037_p1.read());
}

void mnist_fp16::thread_ireg_V_4_fu_7772_p3() {
    ireg_V_4_fu_7772_p3 = (!tmp_240_fu_7763_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_240_fu_7763_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_243_fu_7768_p1.read());
}

void mnist_fp16::thread_ireg_V_7_fu_9910_p3() {
    ireg_V_7_fu_9910_p3 = (!tmp_278_fu_9901_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_278_fu_9901_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_281_fu_9906_p1.read());
}

void mnist_fp16::thread_ireg_V_8_fu_11596_p3() {
    ireg_V_8_fu_11596_p3 = (!tmp_398_fu_11587_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_398_fu_11587_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_400_fu_11592_p1.read());
}

void mnist_fp16::thread_ireg_V_fu_3765_p3() {
    ireg_V_fu_3765_p3 = (!tmp_28_fu_3756_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_28_fu_3756_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_31_fu_3761_p1.read());
}

void mnist_fp16::thread_ireg_V_s_fu_14419_p1() {
    ireg_V_s_fu_14419_p1 = grp_fu_2830_p1.read();
}

void mnist_fp16::thread_is_neg_1_fu_5666_p3() {
    is_neg_1_fu_5666_p3 = p_Val2_3_reg_1567.read().range(15, 15);
}

void mnist_fp16::thread_is_neg_2_fu_8492_p3() {
    is_neg_2_fu_8492_p3 = p_Val2_28_reg_16891.read().range(15, 15);
}

void mnist_fp16::thread_is_neg_3_fu_9535_p3() {
    is_neg_3_fu_9535_p3 = p_Val2_1_reg_2040.read().range(15, 15);
}

void mnist_fp16::thread_is_neg_4_fu_12280_p3() {
    is_neg_4_fu_12280_p3 = p_Val2_40_reg_17986.read().range(15, 15);
}

void mnist_fp16::thread_is_neg_5_fu_13371_p3() {
    is_neg_5_fu_13371_p3 = p_Val2_44_reg_2511.read().range(15, 15);
}

void mnist_fp16::thread_is_neg_fu_4558_p3() {
    is_neg_fu_4558_p3 = p_Val2_s_reg_15806.read().range(15, 15);
}

void mnist_fp16::thread_j1_V_fu_15104_p2() {
    j1_V_fu_15104_p2 = (!p_77_reg_2763.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_77_reg_2763.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_j_V_1_fu_14696_p2() {
    j_V_1_fu_14696_p2 = (!p_88_reg_2682.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_88_reg_2682.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_j_V_fu_2982_p2() {
    j_V_fu_2982_p2 = (!ap_phi_mux_p_1_phi_fu_1212_p4.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(ap_phi_mux_p_1_phi_fu_1212_p4.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_lhs_V_10_cast1_fu_10788_p1() {
    lhs_V_10_cast1_fu_10788_p1 = esl_zext<8,4>(p_45_reg_2245.read());
}

void mnist_fp16::thread_lhs_V_10_cast_fu_4310_p1() {
    lhs_V_10_cast_fu_4310_p1 = esl_sext<7,6>(lhs_V_2_fu_4304_p2.read());
}

void mnist_fp16::thread_lhs_V_11_cast_fu_10320_p1() {
    lhs_V_11_cast_fu_10320_p1 = esl_zext<10,3>(p_65_reg_2165.read());
}

void mnist_fp16::thread_lhs_V_13_cast_fu_11893_p1() {
    lhs_V_13_cast_fu_11893_p1 = esl_zext<9,5>(p_48_reg_2417.read());
}

void mnist_fp16::thread_lhs_V_14_cast_fu_11927_p1() {
    lhs_V_14_cast_fu_11927_p1 = esl_zext<9,4>(p_57_reg_2439.read());
}

void mnist_fp16::thread_lhs_V_18_cast_fu_7076_p1() {
    lhs_V_18_cast_fu_7076_p1 = esl_sext<6,5>(lhs_V_8_fu_7070_p2.read());
}

void mnist_fp16::thread_lhs_V_1_cast_fu_6937_p1() {
    lhs_V_1_cast_fu_6937_p1 = esl_zext<8,5>(p_20_reg_1771.read());
}

void mnist_fp16::thread_lhs_V_1_fu_10899_p2() {
    lhs_V_1_fu_10899_p2 = (!p_56_reg_2256.read().is_01() || !tmp_260_cast_fu_10895_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(p_56_reg_2256.read()) - sc_biguint<4>(tmp_260_cast_fu_10895_p1.read()));
}

void mnist_fp16::thread_lhs_V_24_cast_fu_8244_p1() {
    lhs_V_24_cast_fu_8244_p1 = esl_sext<6,5>(lhs_V_4_fu_8238_p2.read());
}

void mnist_fp16::thread_lhs_V_27_cast_fu_11189_p1() {
    lhs_V_27_cast_fu_11189_p1 = esl_zext<4,3>(p_49_reg_2291.read());
}

void mnist_fp16::thread_lhs_V_29_cast_fu_11236_p1() {
    lhs_V_29_cast_fu_11236_p1 = esl_zext<4,3>(p_58_reg_2302.read());
}

void mnist_fp16::thread_lhs_V_2_fu_4304_p2() {
    lhs_V_2_fu_4304_p2 = (!tmp_45_cast_fu_4296_p1.read().is_01() || !tmp_46_cast_fu_4300_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_45_cast_fu_4296_p1.read()) - sc_biguint<6>(tmp_46_cast_fu_4300_p1.read()));
}

void mnist_fp16::thread_lhs_V_31_cast_fu_10905_p1() {
    lhs_V_31_cast_fu_10905_p1 = esl_sext<5,4>(lhs_V_1_fu_10899_p2.read());
}

void mnist_fp16::thread_lhs_V_32_cast_fu_12543_p1() {
    lhs_V_32_cast_fu_12543_p1 = esl_zext<4,3>(p_62_reg_2488.read());
}

void mnist_fp16::thread_lhs_V_33_cast_fu_12590_p1() {
    lhs_V_33_cast_fu_12590_p1 = esl_zext<4,3>(p_67_reg_2499.read());
}

void mnist_fp16::thread_lhs_V_36_cast_fu_12048_p1() {
    lhs_V_36_cast_fu_12048_p1 = esl_sext<5,4>(lhs_V_3_fu_12042_p2.read());
}

void mnist_fp16::thread_lhs_V_3_cast_fu_8105_p1() {
    lhs_V_3_cast_fu_8105_p1 = esl_zext<9,5>(p_31_reg_1967.read());
}

void mnist_fp16::thread_lhs_V_3_fu_12042_p2() {
    lhs_V_3_fu_12042_p2 = (!p_63_reg_2450.read().is_01() || !tmp_297_cast_fu_12038_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(p_63_reg_2450.read()) - sc_biguint<4>(tmp_297_cast_fu_12038_p1.read()));
}

void mnist_fp16::thread_lhs_V_4_fu_8238_p2() {
    lhs_V_4_fu_8238_p2 = (!p_39_reg_1978.read().is_01() || !tmp_151_cast_fu_8234_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_39_reg_1978.read()) - sc_biguint<5>(tmp_151_cast_fu_8234_p1.read()));
}

void mnist_fp16::thread_lhs_V_5_cast_fu_6408_p1() {
    lhs_V_5_cast_fu_6408_p1 = esl_zext<9,4>(p_27_reg_1680.read());
}

void mnist_fp16::thread_lhs_V_6_cast_fu_10754_p1() {
    lhs_V_6_cast_fu_10754_p1 = esl_zext<8,4>(p_37_reg_2223.read());
}

void mnist_fp16::thread_lhs_V_7_cast_fu_6467_p1() {
    lhs_V_7_cast_fu_6467_p1 = esl_zext<11,4>(p_33_reg_1691.read());
}

void mnist_fp16::thread_lhs_V_8_fu_7070_p2() {
    lhs_V_8_fu_7070_p2 = (!p_30_reg_1782.read().is_01() || !tmp_90_cast_fu_7066_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_30_reg_1782.read()) - sc_biguint<5>(tmp_90_cast_fu_7066_p1.read()));
}

void mnist_fp16::thread_lhs_V_9_cast_fu_10269_p1() {
    lhs_V_9_cast_fu_10269_p1 = esl_zext<9,3>(p_59_reg_2154.read());
}

void mnist_fp16::thread_lhs_V_cast_41_fu_4102_p1() {
    lhs_V_cast_41_fu_4102_p1 = esl_zext<10,5>(p_4_reg_1490.read());
}

void mnist_fp16::thread_lhs_V_cast_fu_3211_p1() {
    lhs_V_cast_fu_3211_p1 = esl_sext<11,6>(lhs_V_s_fu_3205_p2.read());
}

void mnist_fp16::thread_lhs_V_s_fu_3205_p2() {
    lhs_V_s_fu_3205_p2 = (!tmp_24_cast_fu_3197_p1.read().is_01() || !tmp_28_cast_fu_3201_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_24_cast_fu_3197_p1.read()) - sc_biguint<6>(tmp_28_cast_fu_3201_p1.read()));
}

void mnist_fp16::thread_man_V_11_fu_9052_p2() {
    man_V_11_fu_9052_p2 = (!ap_const_lv54_0.is_01() || !p_Result_74_fu_9048_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_74_fu_9048_p1.read()));
}

void mnist_fp16::thread_man_V_12_fu_9058_p3() {
    man_V_12_fu_9058_p3 = (!isneg_2_reg_17054.read()[0].is_01())? sc_lv<54>(): ((isneg_2_reg_17054.read()[0].to_bool())? man_V_11_fu_9052_p2.read(): p_Result_74_fu_9048_p1.read());
}

void mnist_fp16::thread_man_V_13_fu_9964_p2() {
    man_V_13_fu_9964_p2 = (!ap_const_lv54_0.is_01() || !p_Result_25_fu_9960_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_25_fu_9960_p1.read()));
}

void mnist_fp16::thread_man_V_14_fu_9137_p2() {
    man_V_14_fu_9137_p2 = (!ap_const_lv54_0.is_01() || !p_Result_75_fu_9133_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_75_fu_9133_p1.read()));
}

void mnist_fp16::thread_man_V_15_fu_9143_p3() {
    man_V_15_fu_9143_p3 = (!isneg_3_reg_17076.read()[0].is_01())? sc_lv<54>(): ((isneg_3_reg_17076.read()[0].to_bool())? man_V_14_fu_9137_p2.read(): p_Result_75_fu_9133_p1.read());
}

void mnist_fp16::thread_man_V_16_fu_9970_p3() {
    man_V_16_fu_9970_p3 = (!tmp_699_reg_17298.read()[0].is_01())? sc_lv<54>(): ((tmp_699_reg_17298.read()[0].to_bool())? man_V_13_fu_9964_p2.read(): p_Result_25_fu_9960_p1.read());
}

void mnist_fp16::thread_man_V_18_fu_11650_p2() {
    man_V_18_fu_11650_p2 = (!ap_const_lv54_0.is_01() || !p_Result_38_fu_11646_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_38_fu_11646_p1.read()));
}

void mnist_fp16::thread_man_V_19_fu_12888_p2() {
    man_V_19_fu_12888_p2 = (!ap_const_lv54_0.is_01() || !p_Result_82_fu_12884_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_82_fu_12884_p1.read()));
}

void mnist_fp16::thread_man_V_1_fu_3819_p2() {
    man_V_1_fu_3819_p2 = (!ap_const_lv54_0.is_01() || !p_Result_4_fu_3815_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_4_fu_3815_p1.read()));
}

void mnist_fp16::thread_man_V_20_fu_12894_p3() {
    man_V_20_fu_12894_p3 = (!isneg_4_reg_18169.read()[0].is_01())? sc_lv<54>(): ((isneg_4_reg_18169.read()[0].to_bool())? man_V_19_fu_12888_p2.read(): p_Result_82_fu_12884_p1.read());
}

void mnist_fp16::thread_man_V_21_fu_11656_p3() {
    man_V_21_fu_11656_p3 = (!tmp_798_reg_17809.read()[0].is_01())? sc_lv<54>(): ((tmp_798_reg_17809.read()[0].to_bool())? man_V_18_fu_11650_p2.read(): p_Result_38_fu_11646_p1.read());
}

void mnist_fp16::thread_man_V_23_fu_14463_p2() {
    man_V_23_fu_14463_p2 = (!ap_const_lv54_0.is_01() || !p_Result_45_fu_14459_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_45_fu_14459_p1.read()));
}

void mnist_fp16::thread_man_V_24_fu_12973_p2() {
    man_V_24_fu_12973_p2 = (!ap_const_lv54_0.is_01() || !p_Result_83_fu_12969_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_83_fu_12969_p1.read()));
}

void mnist_fp16::thread_man_V_25_fu_12979_p3() {
    man_V_25_fu_12979_p3 = (!isneg_5_reg_18191.read()[0].is_01())? sc_lv<54>(): ((isneg_5_reg_18191.read()[0].to_bool())? man_V_24_fu_12973_p2.read(): p_Result_83_fu_12969_p1.read());
}

void mnist_fp16::thread_man_V_26_fu_14469_p3() {
    man_V_26_fu_14469_p3 = (!tmp_860_reg_18601.read()[0].is_01())? sc_lv<54>(): ((tmp_860_reg_18601.read()[0].to_bool())? man_V_23_fu_14463_p2.read(): p_Result_45_fu_14459_p1.read());
}

void mnist_fp16::thread_man_V_27_fu_13775_p2() {
    man_V_27_fu_13775_p2 = (!ap_const_lv54_0.is_01() || !p_Result_47_fu_13771_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_47_fu_13771_p1.read()));
}

void mnist_fp16::thread_man_V_28_fu_13781_p3() {
    man_V_28_fu_13781_p3 = (!tmp_872_reg_18408.read()[0].is_01())? sc_lv<54>(): ((tmp_872_reg_18408.read()[0].to_bool())? man_V_27_fu_13775_p2.read(): p_Result_47_fu_13771_p1.read());
}

void mnist_fp16::thread_man_V_2_fu_3825_p3() {
    man_V_2_fu_3825_p3 = (!tmp_142_reg_15631.read()[0].is_01())? sc_lv<54>(): ((tmp_142_reg_15631.read()[0].to_bool())? man_V_1_fu_3819_p2.read(): p_Result_4_fu_3815_p1.read());
}

void mnist_fp16::thread_man_V_3_fu_5183_p2() {
    man_V_3_fu_5183_p2 = (!ap_const_lv54_0.is_01() || !p_Result_66_fu_5179_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_66_fu_5179_p1.read()));
}

void mnist_fp16::thread_man_V_4_fu_5189_p3() {
    man_V_4_fu_5189_p3 = (!isneg_reg_15974.read()[0].is_01())? sc_lv<54>(): ((isneg_reg_15974.read()[0].to_bool())? man_V_3_fu_5183_p2.read(): p_Result_66_fu_5179_p1.read());
}

void mnist_fp16::thread_man_V_5_fu_6095_p2() {
    man_V_5_fu_6095_p2 = (!ap_const_lv54_0.is_01() || !p_Result_7_fu_6091_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_7_fu_6091_p1.read()));
}

void mnist_fp16::thread_man_V_6_fu_6101_p3() {
    man_V_6_fu_6101_p3 = (!tmp_362_reg_16218.read()[0].is_01())? sc_lv<54>(): ((tmp_362_reg_16218.read()[0].to_bool())? man_V_5_fu_6095_p2.read(): p_Result_7_fu_6091_p1.read());
}

void mnist_fp16::thread_man_V_7_fu_7832_p3() {
    man_V_7_fu_7832_p3 = (!tmp_624_reg_16714.read()[0].is_01())? sc_lv<54>(): ((tmp_624_reg_16714.read()[0].to_bool())? man_V_s_fu_7826_p2.read(): p_Result_19_fu_7822_p1.read());
}

void mnist_fp16::thread_man_V_8_fu_5268_p2() {
    man_V_8_fu_5268_p2 = (!ap_const_lv54_0.is_01() || !p_Result_67_fu_5264_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_67_fu_5264_p1.read()));
}

void mnist_fp16::thread_man_V_9_fu_5274_p3() {
    man_V_9_fu_5274_p3 = (!isneg_1_reg_15996.read()[0].is_01())? sc_lv<54>(): ((isneg_1_reg_15996.read()[0].to_bool())? man_V_8_fu_5268_p2.read(): p_Result_67_fu_5264_p1.read());
}

void mnist_fp16::thread_man_V_s_fu_7826_p2() {
    man_V_s_fu_7826_p2 = (!ap_const_lv54_0.is_01() || !p_Result_19_fu_7822_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_19_fu_7822_p1.read()));
}

void mnist_fp16::thread_max_pool_0_0_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state374.read())) {
        max_pool_0_0_0_address0 =  (sc_lv<4>) (tmp_433_fu_14414_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state361.read())) {
        max_pool_0_0_0_address0 =  (sc_lv<4>) (tmp_417_reg_18472.read());
    } else {
        max_pool_0_0_0_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void mnist_fp16::thread_max_pool_0_0_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state361.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state374.read()))) {
        max_pool_0_0_0_ce0 = ap_const_logic_1;
    } else {
        max_pool_0_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_max_pool_0_0_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond85_fu_14038_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state361.read()))) {
        max_pool_0_0_0_we0 = ap_const_logic_1;
    } else {
        max_pool_0_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_msb_idx_10_cast_fu_10511_p1() {
    msb_idx_10_cast_fu_10511_p1 = esl_zext<32,31>(msb_idx_10_fu_10505_p3.read());
}

void mnist_fp16::thread_msb_idx_10_fu_10505_p3() {
    msb_idx_10_fu_10505_p3 = (!tmp_781_reg_17472.read()[0].is_01())? sc_lv<31>(): ((tmp_781_reg_17472.read()[0].to_bool())? ap_const_lv31_0: tmp_780_reg_17467.read());
}

void mnist_fp16::thread_msb_idx_11_fu_12319_p2() {
    msb_idx_11_fu_12319_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_6_fu_12311_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_6_fu_12311_p3.read()));
}

void mnist_fp16::thread_msb_idx_12_cast_fu_12343_p1() {
    msb_idx_12_cast_fu_12343_p1 = esl_zext<32,31>(msb_idx_12_fu_12337_p3.read());
}

void mnist_fp16::thread_msb_idx_12_fu_12337_p3() {
    msb_idx_12_fu_12337_p3 = (!tmp_851_reg_18021.read()[0].is_01())? sc_lv<31>(): ((tmp_851_reg_18021.read()[0].to_bool())? ap_const_lv31_0: tmp_850_reg_18016.read());
}

void mnist_fp16::thread_msb_idx_13_fu_13412_p2() {
    msb_idx_13_fu_13412_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_7_fu_13404_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_7_fu_13404_p3.read()));
}

void mnist_fp16::thread_msb_idx_14_cast_fu_13436_p1() {
    msb_idx_14_cast_fu_13436_p1 = esl_zext<32,31>(msb_idx_14_fu_13430_p3.read());
}

void mnist_fp16::thread_msb_idx_14_fu_13430_p3() {
    msb_idx_14_fu_13430_p3 = (!tmp_896_reg_18322.read()[0].is_01())? sc_lv<31>(): ((tmp_896_reg_18322.read()[0].to_bool())? ap_const_lv31_0: tmp_895_reg_18317.read());
}

void mnist_fp16::thread_msb_idx_15_fu_14157_p2() {
    msb_idx_15_fu_14157_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_8_fu_14149_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_8_fu_14149_p3.read()));
}

void mnist_fp16::thread_msb_idx_16_cast_fu_14181_p1() {
    msb_idx_16_cast_fu_14181_p1 = esl_zext<32,31>(msb_idx_16_fu_14175_p3.read());
}

void mnist_fp16::thread_msb_idx_16_fu_14175_p3() {
    msb_idx_16_fu_14175_p3 = (!tmp_906_reg_18546.read()[0].is_01())? sc_lv<31>(): ((tmp_906_reg_18546.read()[0].to_bool())? ap_const_lv31_0: tmp_905_reg_18541.read());
}

void mnist_fp16::thread_msb_idx_17_fu_14812_p2() {
    msb_idx_17_fu_14812_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_9_fu_14804_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_9_fu_14804_p3.read()));
}

void mnist_fp16::thread_msb_idx_18_cast_fu_14836_p1() {
    msb_idx_18_cast_fu_14836_p1 = esl_zext<32,31>(msb_idx_18_fu_14830_p3.read());
}

void mnist_fp16::thread_msb_idx_18_fu_14830_p3() {
    msb_idx_18_fu_14830_p3 = (!tmp_921_reg_18730.read()[0].is_01())? sc_lv<31>(): ((tmp_921_reg_18730.read()[0].to_bool())? ap_const_lv31_0: tmp_920_reg_18725.read());
}

void mnist_fp16::thread_msb_idx_1_cast_fu_4621_p1() {
    msb_idx_1_cast_fu_4621_p1 = esl_zext<32,31>(msb_idx_1_fu_4615_p3.read());
}

void mnist_fp16::thread_msb_idx_1_fu_4615_p3() {
    msb_idx_1_fu_4615_p3 = (!tmp_328_reg_15841.read()[0].is_01())? sc_lv<31>(): ((tmp_328_reg_15841.read()[0].to_bool())? ap_const_lv31_0: tmp_327_reg_15836.read());
}

void mnist_fp16::thread_msb_idx_2_fu_5707_p2() {
    msb_idx_2_fu_5707_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_1_fu_5699_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_1_fu_5699_p3.read()));
}

void mnist_fp16::thread_msb_idx_3_cast_fu_5731_p1() {
    msb_idx_3_cast_fu_5731_p1 = esl_zext<32,31>(msb_idx_3_fu_5725_p3.read());
}

void mnist_fp16::thread_msb_idx_3_fu_5725_p3() {
    msb_idx_3_fu_5725_p3 = (!tmp_391_reg_16127.read()[0].is_01())? sc_lv<31>(): ((tmp_391_reg_16127.read()[0].to_bool())? ap_const_lv31_0: tmp_385_reg_16122.read());
}

void mnist_fp16::thread_msb_idx_4_fu_6634_p2() {
    msb_idx_4_fu_6634_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_2_fu_6626_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_2_fu_6626_p3.read()));
}

void mnist_fp16::thread_msb_idx_5_cast_fu_6658_p1() {
    msb_idx_5_cast_fu_6658_p1 = esl_zext<32,31>(msb_idx_5_fu_6652_p3.read());
}

void mnist_fp16::thread_msb_idx_5_fu_6652_p3() {
    msb_idx_5_fu_6652_p3 = (!tmp_605_reg_16392.read()[0].is_01())? sc_lv<31>(): ((tmp_605_reg_16392.read()[0].to_bool())? ap_const_lv31_0: tmp_604_reg_16387.read());
}

void mnist_fp16::thread_msb_idx_6_fu_8531_p2() {
    msb_idx_6_fu_8531_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_3_fu_8523_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_3_fu_8523_p3.read()));
}

void mnist_fp16::thread_msb_idx_7_cast_fu_8555_p1() {
    msb_idx_7_cast_fu_8555_p1 = esl_zext<32,31>(msb_idx_7_fu_8549_p3.read());
}

void mnist_fp16::thread_msb_idx_7_fu_8549_p3() {
    msb_idx_7_fu_8549_p3 = (!tmp_682_reg_16926.read()[0].is_01())? sc_lv<31>(): ((tmp_682_reg_16926.read()[0].to_bool())? ap_const_lv31_0: tmp_681_reg_16921.read());
}

void mnist_fp16::thread_msb_idx_8_fu_9576_p2() {
    msb_idx_8_fu_9576_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_4_fu_9568_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_4_fu_9568_p3.read()));
}

void mnist_fp16::thread_msb_idx_9_cast_fu_9600_p1() {
    msb_idx_9_cast_fu_9600_p1 = esl_zext<32,31>(msb_idx_9_fu_9594_p3.read());
}

void mnist_fp16::thread_msb_idx_9_fu_9594_p3() {
    msb_idx_9_fu_9594_p3 = (!tmp_707_reg_17207.read()[0].is_01())? sc_lv<31>(): ((tmp_707_reg_17207.read()[0].to_bool())? ap_const_lv31_0: tmp_706_reg_17202.read());
}

void mnist_fp16::thread_msb_idx_fu_4597_p2() {
    msb_idx_fu_4597_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_fu_4589_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_fu_4589_p3.read()));
}

void mnist_fp16::thread_msb_idx_s_fu_10487_p2() {
    msb_idx_s_fu_10487_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_5_fu_10479_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_5_fu_10479_p3.read()));
}

void mnist_fp16::thread_mul10_fu_15327_p0() {
    mul10_fu_15327_p0 =  (sc_lv<13>) (ap_const_lv24_A73);
}

void mnist_fp16::thread_mul10_fu_15327_p1() {
    mul10_fu_15327_p1 =  (sc_lv<11>) (sext9_cast_fu_12075_p1.read());
}

void mnist_fp16::thread_mul1_fu_15237_p0() {
    mul1_fu_15237_p0 =  (sc_lv<15>) (ap_const_lv28_2493);
}

void mnist_fp16::thread_mul1_fu_15237_p1() {
    mul1_fu_15237_p1 =  (sc_lv<13>) (sext1_cast_fu_4328_p1.read());
}

void mnist_fp16::thread_mul2_fu_15245_p0() {
    mul2_fu_15245_p0 =  (sc_lv<15>) (ap_const_lv28_29CC);
}

void mnist_fp16::thread_mul2_fu_15245_p1() {
    mul2_fu_15245_p1 =  (sc_lv<13>) (sext1_cast_fu_4328_p1.read());
}

void mnist_fp16::thread_mul3_fu_15262_p0() {
    mul3_fu_15262_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp16::thread_mul3_fu_15262_p1() {
    mul3_fu_15262_p1 =  (sc_lv<11>) (sext3_cast_fu_7094_p1.read());
}

void mnist_fp16::thread_mul4_fu_15270_p0() {
    mul4_fu_15270_p0 =  (sc_lv<13>) (ap_const_lv24_A73);
}

void mnist_fp16::thread_mul4_fu_15270_p1() {
    mul4_fu_15270_p1 =  (sc_lv<11>) (sext3_cast_fu_7094_p1.read());
}

void mnist_fp16::thread_mul5_fu_15278_p0() {
    mul5_fu_15278_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16::thread_mul5_fu_15278_p1() {
    mul5_fu_15278_p1 =  (sc_lv<12>) (sext5_cast_fu_8262_p1.read());
}

void mnist_fp16::thread_mul6_fu_15286_p0() {
    mul6_fu_15286_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp16::thread_mul6_fu_15286_p1() {
    mul6_fu_15286_p1 =  (sc_lv<12>) (sext5_cast_fu_8262_p1.read());
}

void mnist_fp16::thread_mul7_fu_15303_p0() {
    mul7_fu_15303_p0 =  (sc_lv<12>) (ap_const_lv22_493);
}

void mnist_fp16::thread_mul7_fu_15303_p1() {
    mul7_fu_15303_p1 =  (sc_lv<10>) (sext7_cast_fu_10932_p1.read());
}

void mnist_fp16::thread_mul8_fu_15311_p0() {
    mul8_fu_15311_p0 =  (sc_lv<12>) (ap_const_lv22_53A);
}

void mnist_fp16::thread_mul8_fu_15311_p1() {
    mul8_fu_15311_p1 =  (sc_lv<10>) (sext7_cast_fu_10932_p1.read());
}

void mnist_fp16::thread_mul9_fu_15319_p0() {
    mul9_fu_15319_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp16::thread_mul9_fu_15319_p1() {
    mul9_fu_15319_p1 =  (sc_lv<11>) (sext9_cast_fu_12075_p1.read());
}

void mnist_fp16::thread_mul_fu_15229_p0() {
    mul_fu_15229_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp16::thread_neg_mul10_fu_12150_p2() {
    neg_mul10_fu_12150_p2 = (!ap_const_lv23_0.is_01() || !tmp_831_reg_17950.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_831_reg_17950.read()));
}

void mnist_fp16::thread_neg_mul1_fu_4364_p2() {
    neg_mul1_fu_4364_p2 = (!ap_const_lv26_0.is_01() || !tmp_260_reg_15752.read().is_01())? sc_lv<26>(): (sc_biguint<26>(ap_const_lv26_0) - sc_biguint<26>(tmp_260_reg_15752.read()));
}

void mnist_fp16::thread_neg_mul2_fu_4412_p2() {
    neg_mul2_fu_4412_p2 = (!ap_const_lv27_0.is_01() || !tmp_276_reg_15770.read().is_01())? sc_lv<27>(): (sc_biguint<27>(ap_const_lv27_0) - sc_biguint<27>(tmp_276_reg_15770.read()));
}

void mnist_fp16::thread_neg_mul3_fu_7130_p2() {
    neg_mul3_fu_7130_p2 = (!ap_const_lv22_0.is_01() || !tmp_573_reg_16491.read().is_01())? sc_lv<22>(): (sc_biguint<22>(ap_const_lv22_0) - sc_biguint<22>(tmp_573_reg_16491.read()));
}

void mnist_fp16::thread_neg_mul4_fu_7178_p2() {
    neg_mul4_fu_7178_p2 = (!ap_const_lv23_0.is_01() || !tmp_581_reg_16509.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_581_reg_16509.read()));
}

void mnist_fp16::thread_neg_mul5_fu_8298_p2() {
    neg_mul5_fu_8298_p2 = (!ap_const_lv24_0.is_01() || !tmp_648_reg_16837.read().is_01())? sc_lv<24>(): (sc_biguint<24>(ap_const_lv24_0) - sc_biguint<24>(tmp_648_reg_16837.read()));
}

void mnist_fp16::thread_neg_mul6_fu_8346_p2() {
    neg_mul6_fu_8346_p2 = (!ap_const_lv25_0.is_01() || !tmp_656_reg_16855.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_656_reg_16855.read()));
}

void mnist_fp16::thread_neg_mul7_fu_10959_p2() {
    neg_mul7_fu_10959_p2 = (!ap_const_lv21_0.is_01() || !tmp_751_reg_17579.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_751_reg_17579.read()));
}

void mnist_fp16::thread_neg_mul8_fu_11007_p2() {
    neg_mul8_fu_11007_p2 = (!ap_const_lv21_0.is_01() || !tmp_759_reg_17589.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_759_reg_17589.read()));
}

void mnist_fp16::thread_neg_mul9_fu_12102_p2() {
    neg_mul9_fu_12102_p2 = (!ap_const_lv22_0.is_01() || !tmp_823_reg_17940.read().is_01())? sc_lv<22>(): (sc_biguint<22>(ap_const_lv22_0) - sc_biguint<22>(tmp_823_reg_17940.read()));
}

void mnist_fp16::thread_neg_mul_fu_3243_p2() {
    neg_mul_fu_3243_p2 = (!ap_const_lv23_0.is_01() || !tmp_66_reg_15450.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_66_reg_15450.read()));
}

void mnist_fp16::thread_neg_ti10_fu_12183_p2() {
    neg_ti10_fu_12183_p2 = (!ap_const_lv4_0.is_01() || !tmp_836_fu_12179_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_0) - sc_biguint<4>(tmp_836_fu_12179_p1.read()));
}

void mnist_fp16::thread_neg_ti1_fu_4393_p2() {
    neg_ti1_fu_4393_p2 = (!ap_const_lv13_0.is_01() || !tmp_273_fu_4386_p3.read().is_01())? sc_lv<13>(): (sc_biguint<13>(ap_const_lv13_0) - sc_biguint<13>(tmp_273_fu_4386_p3.read()));
}

void mnist_fp16::thread_neg_ti2_fu_4445_p2() {
    neg_ti2_fu_4445_p2 = (!ap_const_lv2_0.is_01() || !tmp_295_fu_4441_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_295_fu_4441_p1.read()));
}

void mnist_fp16::thread_neg_ti3_fu_7159_p2() {
    neg_ti3_fu_7159_p2 = (!ap_const_lv11_0.is_01() || !tmp_579_fu_7152_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_579_fu_7152_p3.read()));
}

void mnist_fp16::thread_neg_ti4_fu_7211_p2() {
    neg_ti4_fu_7211_p2 = (!ap_const_lv2_0.is_01() || !tmp_586_fu_7207_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_586_fu_7207_p1.read()));
}

void mnist_fp16::thread_neg_ti5_fu_8327_p2() {
    neg_ti5_fu_8327_p2 = (!ap_const_lv12_0.is_01() || !tmp_654_fu_8320_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_654_fu_8320_p3.read()));
}

void mnist_fp16::thread_neg_ti6_fu_8379_p2() {
    neg_ti6_fu_8379_p2 = (!ap_const_lv3_0.is_01() || !tmp_661_fu_8375_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_661_fu_8375_p1.read()));
}

void mnist_fp16::thread_neg_ti7_fu_10988_p2() {
    neg_ti7_fu_10988_p2 = (!ap_const_lv10_0.is_01() || !tmp_757_fu_10981_p3.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_0) - sc_biguint<10>(tmp_757_fu_10981_p3.read()));
}

void mnist_fp16::thread_neg_ti8_fu_11040_p2() {
    neg_ti8_fu_11040_p2 = (!ap_const_lv3_0.is_01() || !tmp_764_fu_11036_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_764_fu_11036_p1.read()));
}

void mnist_fp16::thread_neg_ti9_fu_12131_p2() {
    neg_ti9_fu_12131_p2 = (!ap_const_lv11_0.is_01() || !tmp_829_fu_12124_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_829_fu_12124_p3.read()));
}

void mnist_fp16::thread_neg_ti_fu_3272_p2() {
    neg_ti_fu_3272_p2 = (!ap_const_lv11_0.is_01() || !tmp_79_fu_3265_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_79_fu_3265_p3.read()));
}

void mnist_fp16::thread_newSel10_fu_5466_p3() {
    newSel10_fu_5466_p3 = (!or_cond6_fu_5447_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond6_fu_5447_p2.read()[0].to_bool())? newSel8_fu_5439_p3.read(): newSel9_fu_5453_p3.read());
}

void mnist_fp16::thread_newSel11_fu_5588_p3() {
    newSel11_fu_5588_p3 = (!sel_tmp35_fu_5583_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp35_fu_5583_p2.read()[0].to_bool())? tmp_570_fu_5525_p1.read(): tmp_569_fu_5505_p1.read());
}

void mnist_fp16::thread_newSel12_fu_5602_p3() {
    newSel12_fu_5602_p3 = (!sel_tmp32_fu_5560_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp32_fu_5560_p2.read()[0].to_bool())? storemerge6_fu_5509_p3.read(): tmp_561_reg_16075.read());
}

void mnist_fp16::thread_newSel13_fu_5615_p3() {
    newSel13_fu_5615_p3 = (!or_cond9_fu_5596_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond9_fu_5596_p2.read()[0].to_bool())? newSel11_fu_5588_p3.read(): newSel12_fu_5602_p3.read());
}

void mnist_fp16::thread_newSel14_fu_7999_p3() {
    newSel14_fu_7999_p3 = (!sel_tmp44_reg_16764.read()[0].is_01())? sc_lv<16>(): ((sel_tmp44_reg_16764.read()[0].to_bool())? tmp_629_fu_7969_p1.read(): tmp_628_fu_7949_p1.read());
}

void mnist_fp16::thread_newSel15_fu_8011_p3() {
    newSel15_fu_8011_p3 = (!sel_tmp41_fu_7989_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp41_fu_7989_p2.read()[0].to_bool())? storemerge8_fu_7953_p3.read(): tmp_626_reg_16752.read());
}

void mnist_fp16::thread_newSel16_fu_8024_p3() {
    newSel16_fu_8024_p3 = (!or_cond12_fu_8006_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond12_fu_8006_p2.read()[0].to_bool())? newSel14_fu_7999_p3.read(): newSel15_fu_8011_p3.read());
}

void mnist_fp16::thread_newSel18_fu_10137_p3() {
    newSel18_fu_10137_p3 = (!sel_tmp53_reg_17348.read()[0].is_01())? sc_lv<16>(): ((sel_tmp53_reg_17348.read()[0].to_bool())? tmp_704_fu_10107_p1.read(): tmp_703_fu_10087_p1.read());
}

void mnist_fp16::thread_newSel19_fu_10149_p3() {
    newSel19_fu_10149_p3 = (!sel_tmp50_fu_10127_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp50_fu_10127_p2.read()[0].to_bool())? storemerge5_fu_10091_p3.read(): tmp_701_reg_17336.read());
}

void mnist_fp16::thread_newSel1_fu_4004_p3() {
    newSel1_fu_4004_p3 = (!sel_tmp9_fu_3982_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp9_fu_3982_p2.read()[0].to_bool())? storemerge_fu_3946_p3.read(): tmp_148_reg_15669.read());
}

void mnist_fp16::thread_newSel20_fu_10162_p3() {
    newSel20_fu_10162_p3 = (!or_cond15_fu_10144_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond15_fu_10144_p2.read()[0].to_bool())? newSel18_fu_10137_p3.read(): newSel19_fu_10149_p3.read());
}

void mnist_fp16::thread_newSel22_fu_9308_p3() {
    newSel22_fu_9308_p3 = (!sel_tmp62_fu_9303_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp62_fu_9303_p2.read()[0].to_bool())? tmp_742_fu_9245_p1.read(): tmp_741_fu_9225_p1.read());
}

void mnist_fp16::thread_newSel23_fu_9322_p3() {
    newSel23_fu_9322_p3 = (!sel_tmp59_fu_9280_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp59_fu_9280_p2.read()[0].to_bool())? storemerge1_fu_9229_p3.read(): tmp_739_reg_17121.read());
}

void mnist_fp16::thread_newSel24_fu_9335_p3() {
    newSel24_fu_9335_p3 = (!or_cond18_fu_9316_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond18_fu_9316_p2.read()[0].to_bool())? newSel22_fu_9308_p3.read(): newSel23_fu_9322_p3.read());
}

void mnist_fp16::thread_newSel25_fu_9457_p3() {
    newSel25_fu_9457_p3 = (!sel_tmp71_fu_9452_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp71_fu_9452_p2.read()[0].to_bool())? tmp_749_fu_9394_p1.read(): tmp_748_fu_9374_p1.read());
}

void mnist_fp16::thread_newSel26_fu_9471_p3() {
    newSel26_fu_9471_p3 = (!sel_tmp68_fu_9429_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp68_fu_9429_p2.read()[0].to_bool())? storemerge3_fu_9378_p3.read(): tmp_746_reg_17155.read());
}

void mnist_fp16::thread_newSel27_fu_9484_p3() {
    newSel27_fu_9484_p3 = (!or_cond21_fu_9465_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond21_fu_9465_p2.read()[0].to_bool())? newSel25_fu_9457_p3.read(): newSel26_fu_9471_p3.read());
}

void mnist_fp16::thread_newSel28_fu_11823_p3() {
    newSel28_fu_11823_p3 = (!sel_tmp80_reg_17859.read()[0].is_01())? sc_lv<16>(): ((sel_tmp80_reg_17859.read()[0].to_bool())? tmp_803_fu_11793_p1.read(): tmp_802_fu_11773_p1.read());
}

void mnist_fp16::thread_newSel29_fu_11835_p3() {
    newSel29_fu_11835_p3 = (!sel_tmp77_fu_11813_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp77_fu_11813_p2.read()[0].to_bool())? storemerge7_fu_11777_p3.read(): tmp_800_reg_17847.read());
}

void mnist_fp16::thread_newSel2_fu_4017_p3() {
    newSel2_fu_4017_p3 = (!or_cond_fu_3999_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond_fu_3999_p2.read()[0].to_bool())? newSel_fu_3992_p3.read(): newSel1_fu_4004_p3.read());
}

void mnist_fp16::thread_newSel30_fu_11848_p3() {
    newSel30_fu_11848_p3 = (!or_cond24_fu_11830_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond24_fu_11830_p2.read()[0].to_bool())? newSel28_fu_11823_p3.read(): newSel29_fu_11835_p3.read());
}

void mnist_fp16::thread_newSel32_fu_14642_p3() {
    newSel32_fu_14642_p3 = (!sel_tmp89_reg_18650.read()[0].is_01())? sc_lv<16>(): ((sel_tmp89_reg_18650.read()[0].to_bool())? tmp_865_fu_14612_p1.read(): tmp_864_fu_14592_p1.read());
}

void mnist_fp16::thread_newSel33_fu_14654_p3() {
    newSel33_fu_14654_p3 = (!sel_tmp86_fu_14632_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp86_fu_14632_p2.read()[0].to_bool())? storemerge9_fu_14596_p3.read(): tmp_862_reg_18638.read());
}

void mnist_fp16::thread_newSel34_fu_14667_p3() {
    newSel34_fu_14667_p3 = (!or_cond27_fu_14649_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond27_fu_14649_p2.read()[0].to_bool())? newSel32_fu_14642_p3.read(): newSel33_fu_14654_p3.read());
}

void mnist_fp16::thread_newSel36_fu_13948_p3() {
    newSel36_fu_13948_p3 = (!sel_tmp98_reg_18458.read()[0].is_01())? sc_lv<16>(): ((sel_tmp98_reg_18458.read()[0].to_bool())? tmp_893_fu_13918_p1.read(): tmp_892_fu_13898_p1.read());
}

void mnist_fp16::thread_newSel37_fu_13960_p3() {
    newSel37_fu_13960_p3 = (!sel_tmp95_fu_13938_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp95_fu_13938_p2.read()[0].to_bool())? storemerge10_fu_13902_p3.read(): tmp_881_reg_18446.read());
}

void mnist_fp16::thread_newSel38_fu_13973_p3() {
    newSel38_fu_13973_p3 = (!or_cond30_fu_13955_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond30_fu_13955_p2.read()[0].to_bool())? newSel36_fu_13948_p3.read(): newSel37_fu_13960_p3.read());
}

void mnist_fp16::thread_newSel40_fu_13144_p3() {
    newSel40_fu_13144_p3 = (!sel_tmp107_fu_13139_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp107_fu_13139_p2.read()[0].to_bool())? tmp_934_fu_13081_p1.read(): tmp_933_fu_13061_p1.read());
}

void mnist_fp16::thread_newSel41_fu_13158_p3() {
    newSel41_fu_13158_p3 = (!sel_tmp104_fu_13116_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp104_fu_13116_p2.read()[0].to_bool())? storemerge11_fu_13065_p3.read(): tmp_931_reg_18236.read());
}

void mnist_fp16::thread_newSel42_fu_13171_p3() {
    newSel42_fu_13171_p3 = (!or_cond33_fu_13152_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond33_fu_13152_p2.read()[0].to_bool())? newSel40_fu_13144_p3.read(): newSel41_fu_13158_p3.read());
}

void mnist_fp16::thread_newSel43_fu_13293_p3() {
    newSel43_fu_13293_p3 = (!sel_tmp116_fu_13288_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp116_fu_13288_p2.read()[0].to_bool())? tmp_941_fu_13230_p1.read(): tmp_940_fu_13210_p1.read());
}

void mnist_fp16::thread_newSel44_fu_13307_p3() {
    newSel44_fu_13307_p3 = (!sel_tmp113_fu_13265_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp113_fu_13265_p2.read()[0].to_bool())? storemerge12_fu_13214_p3.read(): tmp_938_reg_18270.read());
}

void mnist_fp16::thread_newSel45_fu_13320_p3() {
    newSel45_fu_13320_p3 = (!or_cond36_fu_13301_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond36_fu_13301_p2.read()[0].to_bool())? newSel43_fu_13293_p3.read(): newSel44_fu_13307_p3.read());
}

void mnist_fp16::thread_newSel4_fu_6268_p3() {
    newSel4_fu_6268_p3 = (!sel_tmp17_reg_16268.read()[0].is_01())? sc_lv<16>(): ((sel_tmp17_reg_16268.read()[0].to_bool())? tmp_380_fu_6238_p1.read(): tmp_377_fu_6218_p1.read());
}

void mnist_fp16::thread_newSel5_fu_6280_p3() {
    newSel5_fu_6280_p3 = (!sel_tmp14_fu_6258_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp14_fu_6258_p2.read()[0].to_bool())? storemerge2_fu_6222_p3.read(): tmp_367_reg_16256.read());
}

void mnist_fp16::thread_newSel6_fu_6293_p3() {
    newSel6_fu_6293_p3 = (!or_cond3_fu_6275_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond3_fu_6275_p2.read()[0].to_bool())? newSel4_fu_6268_p3.read(): newSel5_fu_6280_p3.read());
}

void mnist_fp16::thread_newSel8_fu_5439_p3() {
    newSel8_fu_5439_p3 = (!sel_tmp26_fu_5434_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp26_fu_5434_p2.read()[0].to_bool())? tmp_552_fu_5376_p1.read(): tmp_548_fu_5356_p1.read());
}

void mnist_fp16::thread_newSel9_fu_5453_p3() {
    newSel9_fu_5453_p3 = (!sel_tmp23_fu_5411_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp23_fu_5411_p2.read()[0].to_bool())? storemerge4_fu_5360_p3.read(): tmp_545_reg_16041.read());
}

void mnist_fp16::thread_newSel_fu_3992_p3() {
    newSel_fu_3992_p3 = (!sel_tmp4_reg_15681.read()[0].is_01())? sc_lv<16>(): ((sel_tmp4_reg_15681.read()[0].to_bool())? tmp_156_fu_3962_p1.read(): tmp_150_fu_3942_p1.read());
}

void mnist_fp16::thread_next_mul1_fu_6883_p2() {
    next_mul1_fu_6883_p2 = (!phi_mul1_reg_1760.read().is_01() || !ap_const_lv10_C4.is_01())? sc_lv<10>(): (sc_biguint<10>(phi_mul1_reg_1760.read()) + sc_biguint<10>(ap_const_lv10_C4));
}

void mnist_fp16::thread_next_mul2_fu_8051_p2() {
    next_mul2_fu_8051_p2 = (!phi_mul2_reg_1956.read().is_01() || !ap_const_lv11_C4.is_01())? sc_lv<11>(): (sc_biguint<11>(phi_mul2_reg_1956.read()) + sc_biguint<11>(ap_const_lv11_C4));
}

void mnist_fp16::thread_next_mul3_fu_10736_p2() {
    next_mul3_fu_10736_p2 = (!phi_mul3_reg_2234.read().is_01() || !ap_const_lv9_31.is_01())? sc_lv<9>(): (sc_biguint<9>(phi_mul3_reg_2234.read()) + sc_biguint<9>(ap_const_lv9_31));
}

void mnist_fp16::thread_next_mul4_fu_11875_p2() {
    next_mul4_fu_11875_p2 = (!phi_mul4_reg_2428.read().is_01() || !ap_const_lv10_31.is_01())? sc_lv<10>(): (sc_biguint<10>(phi_mul4_reg_2428.read()) + sc_biguint<10>(ap_const_lv10_31));
}

void mnist_fp16::thread_next_mul_fu_4044_p2() {
    next_mul_fu_4044_p2 = (!phi_mul_reg_1479.read().is_01() || !ap_const_lv12_310.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul_reg_1479.read()) + sc_biguint<12>(ap_const_lv12_310));
}

void mnist_fp16::thread_not_zero1_V_fu_6895_p2() {
    not_zero1_V_fu_6895_p2 = (!p_13_reg_1749.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_13_reg_1749.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_not_zero2_V_fu_8063_p2() {
    not_zero2_V_fu_8063_p2 = (!p_21_reg_1945.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_21_reg_1945.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_not_zero3_V_fu_10748_p2() {
    not_zero3_V_fu_10748_p2 = (!p_37_reg_2223.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_37_reg_2223.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_not_zero4_V_fu_11887_p2() {
    not_zero4_V_fu_11887_p2 = (!p_48_reg_2417.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_48_reg_2417.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_not_zero_V_fu_4056_p2() {
    not_zero_V_fu_4056_p2 = (!p_6_reg_1468.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_6_reg_1468.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_notlhs10_fu_14347_p2() {
    notlhs10_fu_14347_p2 = (!tmp_509_fu_14315_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_509_fu_14315_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs11_fu_14365_p2() {
    notlhs11_fu_14365_p2 = (!tmp_511_fu_14333_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_511_fu_14333_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs12_fu_15025_p2() {
    notlhs12_fu_15025_p2 = (!tmp_530_fu_14993_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_530_fu_14993_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs13_fu_15043_p2() {
    notlhs13_fu_15043_p2 = (!tmp_532_fu_15011_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_532_fu_15011_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs14_fu_15167_p2() {
    notlhs14_fu_15167_p2 = (!tmp_560_fu_15135_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_560_fu_15135_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs15_fu_15185_p2() {
    notlhs15_fu_15185_p2 = (!tmp_562_fu_15153_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_562_fu_15153_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs1_fu_6016_p2() {
    notlhs1_fu_6016_p2 = (!tmp_100_fu_6002_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_100_fu_6002_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs2_fu_6824_p2() {
    notlhs2_fu_6824_p2 = (!tmp_211_fu_6792_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_211_fu_6792_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs3_fu_6842_p2() {
    notlhs3_fu_6842_p2 = (!tmp_217_fu_6810_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_217_fu_6810_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs4_fu_7747_p2() {
    notlhs4_fu_7747_p2 = (!tmp_225_fu_7733_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_225_fu_7733_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs5_fu_9885_p2() {
    notlhs5_fu_9885_p2 = (!tmp_262_fu_9871_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_262_fu_9871_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs6_fu_10677_p2() {
    notlhs6_fu_10677_p2 = (!tmp_379_fu_10645_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_379_fu_10645_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs7_fu_10695_p2() {
    notlhs7_fu_10695_p2 = (!tmp_381_fu_10663_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_381_fu_10663_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs8_fu_11571_p2() {
    notlhs8_fu_11571_p2 = (!tmp_394_fu_11557_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_394_fu_11557_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs9_fu_13696_p2() {
    notlhs9_fu_13696_p2 = (!tmp_459_fu_13682_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_459_fu_13682_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notlhs_fu_3740_p2() {
    notlhs_fu_3740_p2 = (!tmp_21_fu_3726_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_21_fu_3726_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16::thread_notrhs10_fu_14353_p2() {
    notrhs10_fu_14353_p2 = (!tmp_913_fu_14325_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_913_fu_14325_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs11_fu_14371_p2() {
    notrhs11_fu_14371_p2 = (!tmp_914_fu_14343_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_914_fu_14343_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs12_fu_15031_p2() {
    notrhs12_fu_15031_p2 = (!tmp_917_fu_15003_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_917_fu_15003_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs13_fu_15049_p2() {
    notrhs13_fu_15049_p2 = (!tmp_918_fu_15021_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_918_fu_15021_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs14_fu_15173_p2() {
    notrhs14_fu_15173_p2 = (!tmp_942_fu_15145_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_942_fu_15145_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs15_fu_15191_p2() {
    notrhs15_fu_15191_p2 = (!tmp_943_fu_15163_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_943_fu_15163_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs1_fu_6022_p2() {
    notrhs1_fu_6022_p2 = (!tmp_360_fu_6012_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_360_fu_6012_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs2_fu_6830_p2() {
    notrhs2_fu_6830_p2 = (!tmp_612_fu_6802_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_612_fu_6802_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs3_fu_6848_p2() {
    notrhs3_fu_6848_p2 = (!tmp_613_fu_6820_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_613_fu_6820_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs4_fu_7753_p2() {
    notrhs4_fu_7753_p2 = (!tmp_622_fu_7743_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_622_fu_7743_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs5_fu_9891_p2() {
    notrhs5_fu_9891_p2 = (!tmp_697_fu_9881_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_697_fu_9881_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs6_fu_10683_p2() {
    notrhs6_fu_10683_p2 = (!tmp_788_fu_10655_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_788_fu_10655_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs7_fu_10701_p2() {
    notrhs7_fu_10701_p2 = (!tmp_789_fu_10673_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_789_fu_10673_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs8_fu_11577_p2() {
    notrhs8_fu_11577_p2 = (!tmp_796_fu_11567_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_796_fu_11567_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs9_fu_13702_p2() {
    notrhs9_fu_13702_p2 = (!tmp_870_fu_13692_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_870_fu_13692_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_notrhs_fu_3746_p2() {
    notrhs_fu_3746_p2 = (!tmp_138_fu_3736_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_138_fu_3736_p1.read() == ap_const_lv23_0);
}

void mnist_fp16::thread_num_zeros_1_fu_5699_p3() {
    num_zeros_1_fu_5699_p3 = esl_cttz<32,32>(p_Result_69_fu_5691_p3.read());
}

void mnist_fp16::thread_num_zeros_2_fu_6626_p3() {
    num_zeros_2_fu_6626_p3 = esl_cttz<32,32>(p_Result_15_fu_6618_p3.read());
}

void mnist_fp16::thread_num_zeros_3_fu_8523_p3() {
    num_zeros_3_fu_8523_p3 = esl_cttz<32,32>(p_Result_72_fu_8515_p3.read());
}

void mnist_fp16::thread_num_zeros_4_fu_9568_p3() {
    num_zeros_4_fu_9568_p3 = esl_cttz<32,32>(p_Result_77_fu_9560_p3.read());
}

void mnist_fp16::thread_num_zeros_5_fu_10479_p3() {
    num_zeros_5_fu_10479_p3 = esl_cttz<32,32>(p_Result_34_fu_10471_p3.read());
}

void mnist_fp16::thread_num_zeros_6_fu_12311_p3() {
    num_zeros_6_fu_12311_p3 = esl_cttz<32,32>(p_Result_80_fu_12303_p3.read());
}

void mnist_fp16::thread_num_zeros_7_fu_13404_p3() {
    num_zeros_7_fu_13404_p3 = esl_cttz<32,32>(p_Result_85_fu_13396_p3.read());
}

void mnist_fp16::thread_num_zeros_8_fu_14149_p3() {
    num_zeros_8_fu_14149_p3 = esl_cttz<32,32>(p_Result_54_fu_14141_p3.read());
}

void mnist_fp16::thread_num_zeros_9_fu_14804_p3() {
    num_zeros_9_fu_14804_p3 = esl_cttz<32,32>(p_Result_58_fu_14796_p3.read());
}

void mnist_fp16::thread_num_zeros_fu_4589_p3() {
    num_zeros_fu_4589_p3 = esl_cttz<32,32>(p_Result_64_fu_4581_p3.read());
}

void mnist_fp16::thread_or_cond10_fu_5609_p2() {
    or_cond10_fu_5609_p2 = (sel_tmp32_fu_5560_p2.read() | sel_tmp28_fu_5534_p2.read());
}

void mnist_fp16::thread_or_cond11_fu_5623_p2() {
    or_cond11_fu_5623_p2 = (or_cond9_fu_5596_p2.read() | or_cond10_fu_5609_p2.read());
}

void mnist_fp16::thread_or_cond12_fu_8006_p2() {
    or_cond12_fu_8006_p2 = (sel_tmp44_reg_16764.read() | sel_tmp42_fu_7994_p2.read());
}

void mnist_fp16::thread_or_cond13_fu_8018_p2() {
    or_cond13_fu_8018_p2 = (sel_tmp41_fu_7989_p2.read() | sel_tmp37_fu_7978_p2.read());
}

void mnist_fp16::thread_or_cond14_fu_8032_p2() {
    or_cond14_fu_8032_p2 = (or_cond12_fu_8006_p2.read() | or_cond13_fu_8018_p2.read());
}

void mnist_fp16::thread_or_cond15_fu_10144_p2() {
    or_cond15_fu_10144_p2 = (sel_tmp53_reg_17348.read() | sel_tmp51_fu_10132_p2.read());
}

void mnist_fp16::thread_or_cond16_fu_10156_p2() {
    or_cond16_fu_10156_p2 = (sel_tmp50_fu_10127_p2.read() | sel_tmp46_fu_10116_p2.read());
}

void mnist_fp16::thread_or_cond17_fu_10170_p2() {
    or_cond17_fu_10170_p2 = (or_cond15_fu_10144_p2.read() | or_cond16_fu_10156_p2.read());
}

void mnist_fp16::thread_or_cond18_fu_9316_p2() {
    or_cond18_fu_9316_p2 = (sel_tmp62_fu_9303_p2.read() | sel_tmp60_fu_9286_p2.read());
}

void mnist_fp16::thread_or_cond19_fu_9329_p2() {
    or_cond19_fu_9329_p2 = (sel_tmp59_fu_9280_p2.read() | sel_tmp55_fu_9254_p2.read());
}

void mnist_fp16::thread_or_cond1_45_fu_10866_p2() {
    or_cond1_45_fu_10866_p2 = (tmp_319_fu_10860_p2.read() & tmp_315_reg_17540.read());
}

void mnist_fp16::thread_or_cond1_fu_4011_p2() {
    or_cond1_fu_4011_p2 = (sel_tmp9_fu_3982_p2.read() | sel_tmp2_fu_3971_p2.read());
}

void mnist_fp16::thread_or_cond20_fu_9343_p2() {
    or_cond20_fu_9343_p2 = (or_cond18_fu_9316_p2.read() | or_cond19_fu_9329_p2.read());
}

void mnist_fp16::thread_or_cond21_fu_9465_p2() {
    or_cond21_fu_9465_p2 = (sel_tmp71_fu_9452_p2.read() | sel_tmp69_fu_9435_p2.read());
}

void mnist_fp16::thread_or_cond22_fu_9478_p2() {
    or_cond22_fu_9478_p2 = (sel_tmp68_fu_9429_p2.read() | sel_tmp64_fu_9403_p2.read());
}

void mnist_fp16::thread_or_cond23_fu_9492_p2() {
    or_cond23_fu_9492_p2 = (or_cond21_fu_9465_p2.read() | or_cond22_fu_9478_p2.read());
}

void mnist_fp16::thread_or_cond24_fu_11830_p2() {
    or_cond24_fu_11830_p2 = (sel_tmp80_reg_17859.read() | sel_tmp78_fu_11818_p2.read());
}

void mnist_fp16::thread_or_cond25_fu_11842_p2() {
    or_cond25_fu_11842_p2 = (sel_tmp77_fu_11813_p2.read() | sel_tmp73_fu_11802_p2.read());
}

void mnist_fp16::thread_or_cond26_fu_11856_p2() {
    or_cond26_fu_11856_p2 = (or_cond24_fu_11830_p2.read() | or_cond25_fu_11842_p2.read());
}

void mnist_fp16::thread_or_cond27_fu_14649_p2() {
    or_cond27_fu_14649_p2 = (sel_tmp89_reg_18650.read() | sel_tmp87_fu_14637_p2.read());
}

void mnist_fp16::thread_or_cond28_fu_14661_p2() {
    or_cond28_fu_14661_p2 = (sel_tmp86_fu_14632_p2.read() | sel_tmp82_fu_14621_p2.read());
}

void mnist_fp16::thread_or_cond29_fu_14675_p2() {
    or_cond29_fu_14675_p2 = (or_cond27_fu_14649_p2.read() | or_cond28_fu_14661_p2.read());
}

void mnist_fp16::thread_or_cond2_46_fu_12009_p2() {
    or_cond2_46_fu_12009_p2 = (tmp_429_fu_12003_p2.read() & tmp_424_reg_17901.read());
}

void mnist_fp16::thread_or_cond2_fu_4025_p2() {
    or_cond2_fu_4025_p2 = (or_cond_fu_3999_p2.read() | or_cond1_fu_4011_p2.read());
}

void mnist_fp16::thread_or_cond30_fu_13955_p2() {
    or_cond30_fu_13955_p2 = (sel_tmp98_reg_18458.read() | sel_tmp96_fu_13943_p2.read());
}

void mnist_fp16::thread_or_cond31_fu_13967_p2() {
    or_cond31_fu_13967_p2 = (sel_tmp95_fu_13938_p2.read() | sel_tmp91_fu_13927_p2.read());
}

void mnist_fp16::thread_or_cond32_fu_13981_p2() {
    or_cond32_fu_13981_p2 = (or_cond30_fu_13955_p2.read() | or_cond31_fu_13967_p2.read());
}

void mnist_fp16::thread_or_cond33_fu_13152_p2() {
    or_cond33_fu_13152_p2 = (sel_tmp107_fu_13139_p2.read() | sel_tmp105_fu_13122_p2.read());
}

void mnist_fp16::thread_or_cond34_fu_13165_p2() {
    or_cond34_fu_13165_p2 = (sel_tmp104_fu_13116_p2.read() | sel_tmp100_fu_13090_p2.read());
}

void mnist_fp16::thread_or_cond35_fu_13179_p2() {
    or_cond35_fu_13179_p2 = (or_cond33_fu_13152_p2.read() | or_cond34_fu_13165_p2.read());
}

void mnist_fp16::thread_or_cond36_fu_13301_p2() {
    or_cond36_fu_13301_p2 = (sel_tmp116_fu_13288_p2.read() | sel_tmp114_fu_13271_p2.read());
}

void mnist_fp16::thread_or_cond37_fu_13314_p2() {
    or_cond37_fu_13314_p2 = (sel_tmp113_fu_13265_p2.read() | sel_tmp109_fu_13239_p2.read());
}

void mnist_fp16::thread_or_cond38_fu_13328_p2() {
    or_cond38_fu_13328_p2 = (or_cond36_fu_13301_p2.read() | or_cond37_fu_13314_p2.read());
}

void mnist_fp16::thread_or_cond3_fu_6275_p2() {
    or_cond3_fu_6275_p2 = (sel_tmp17_reg_16268.read() | sel_tmp15_fu_6263_p2.read());
}

void mnist_fp16::thread_or_cond4_fu_6287_p2() {
    or_cond4_fu_6287_p2 = (sel_tmp14_fu_6258_p2.read() | sel_tmp10_fu_6247_p2.read());
}

void mnist_fp16::thread_or_cond5_fu_6301_p2() {
    or_cond5_fu_6301_p2 = (or_cond3_fu_6275_p2.read() | or_cond4_fu_6287_p2.read());
}

void mnist_fp16::thread_or_cond6_fu_5447_p2() {
    or_cond6_fu_5447_p2 = (sel_tmp26_fu_5434_p2.read() | sel_tmp24_fu_5417_p2.read());
}

void mnist_fp16::thread_or_cond7_fu_5460_p2() {
    or_cond7_fu_5460_p2 = (sel_tmp23_fu_5411_p2.read() | sel_tmp19_fu_5385_p2.read());
}

void mnist_fp16::thread_or_cond8_43_fu_7031_p2() {
    or_cond8_43_fu_7031_p2 = (tmp137_fu_7025_p2.read() & tmp136_reg_16460.read());
}

void mnist_fp16::thread_or_cond8_fu_5474_p2() {
    or_cond8_fu_5474_p2 = (or_cond6_fu_5447_p2.read() | or_cond7_fu_5460_p2.read());
}

void mnist_fp16::thread_or_cond9_fu_5596_p2() {
    or_cond9_fu_5596_p2 = (sel_tmp35_fu_5583_p2.read() | sel_tmp33_fu_5566_p2.read());
}

void mnist_fp16::thread_or_cond_44_fu_8199_p2() {
    or_cond_44_fu_8199_p2 = (tmp245_fu_8193_p2.read() & tmp244_reg_16806.read());
}

void mnist_fp16::thread_or_cond_fu_3999_p2() {
    or_cond_fu_3999_p2 = (sel_tmp4_reg_15681.read() | sel_tmp_fu_3987_p2.read());
}

void mnist_fp16::thread_p_012_0_i2_fu_6716_p3() {
    p_012_0_i2_fu_6716_p3 = (!icmp6_fu_6672_p2.read()[0].is_01())? sc_lv<32>(): ((icmp6_fu_6672_p2.read()[0].to_bool())? tmp32_V_3_fu_6687_p2.read(): tmp32_V_4_fu_6712_p1.read());
}

void mnist_fp16::thread_p_012_0_i5_fu_10569_p3() {
    p_012_0_i5_fu_10569_p3 = (!icmp13_fu_10525_p2.read()[0].is_01())? sc_lv<32>(): ((icmp13_fu_10525_p2.read()[0].to_bool())? tmp32_V_18_fu_10540_p2.read(): tmp32_V_19_fu_10565_p1.read());
}

void mnist_fp16::thread_p_012_0_i8_fu_14239_p3() {
    p_012_0_i8_fu_14239_p3 = (!icmp19_fu_14195_p2.read()[0].is_01())? sc_lv<32>(): ((icmp19_fu_14195_p2.read()[0].to_bool())? tmp32_V_30_fu_14210_p2.read(): tmp32_V_31_fu_14235_p1.read());
}

void mnist_fp16::thread_p_012_0_i9_fu_14894_p3() {
    p_012_0_i9_fu_14894_p3 = (!icmp20_fu_14850_p2.read()[0].is_01())? sc_lv<32>(): ((icmp20_fu_14850_p2.read()[0].to_bool())? tmp32_V_34_fu_14865_p2.read(): tmp32_V_35_fu_14890_p1.read());
}

void mnist_fp16::thread_p_03_i1_fu_6782_p3() {
    p_03_i1_fu_6782_p3 = (!tmp_155_reg_16371.read()[0].is_01())? sc_lv<32>(): ((tmp_155_reg_16371.read()[0].to_bool())? ap_const_lv32_0: f_4_fu_6778_p1.read());
}

void mnist_fp16::thread_p_03_i1_to_int_fu_6789_p1() {
    p_03_i1_to_int_fu_6789_p1 = p_03_i1_reg_16412.read();
}

void mnist_fp16::thread_p_03_i3_fu_10635_p3() {
    p_03_i3_fu_10635_p3 = (!tmp_333_reg_17451.read()[0].is_01())? sc_lv<32>(): ((tmp_333_reg_17451.read()[0].to_bool())? ap_const_lv32_0: f_8_fu_10631_p1.read());
}

void mnist_fp16::thread_p_03_i3_to_int_fu_10642_p1() {
    p_03_i3_to_int_fu_10642_p1 = p_03_i3_reg_17492.read();
}

void mnist_fp16::thread_p_03_i5_fu_14305_p3() {
    p_03_i5_fu_14305_p3 = (!tmp_467_reg_18525.read()[0].is_01())? sc_lv<32>(): ((tmp_467_reg_18525.read()[0].to_bool())? ap_const_lv32_0: f_14_fu_14301_p1.read());
}

void mnist_fp16::thread_p_03_i5_to_int_fu_14312_p1() {
    p_03_i5_to_int_fu_14312_p1 = p_03_i5_reg_18566.read();
}

void mnist_fp16::thread_p_03_i6_fu_14964_p3() {
    p_03_i6_fu_14964_p3 = (!tmp_503_reg_18709.read()[0].is_01())? sc_lv<32>(): ((tmp_503_reg_18709.read()[0].to_bool())? ap_const_lv32_0: f_16_fu_14960_p1.read());
}

void mnist_fp16::thread_p_Repl2_10_trunc_fu_8651_p2() {
    p_Repl2_10_trunc_fu_8651_p2 = (!tmp363_cast_cast_fu_8644_p3.read().is_01() || !tmp_688_fu_8641_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp363_cast_cast_fu_8644_p3.read()) + sc_biguint<8>(tmp_688_fu_8641_p1.read()));
}

void mnist_fp16::thread_p_Repl2_13_trunc_fu_9705_p2() {
    p_Repl2_13_trunc_fu_9705_p2 = (!tmp_713_fu_9695_p1.read().is_01() || !tmp364_cast_cast_fu_9698_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_713_fu_9695_p1.read()) + sc_biguint<8>(tmp364_cast_cast_fu_9698_p3.read()));
}

void mnist_fp16::thread_p_Repl2_16_trunc_fu_10607_p2() {
    p_Repl2_16_trunc_fu_10607_p2 = (!tmp_787_fu_10597_p1.read().is_01() || !tmp365_cast_cast_fu_10600_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_787_fu_10597_p1.read()) + sc_biguint<8>(tmp365_cast_cast_fu_10600_p3.read()));
}

void mnist_fp16::thread_p_Repl2_19_trunc_fu_12439_p2() {
    p_Repl2_19_trunc_fu_12439_p2 = (!tmp376_cast_cast_fu_12432_p3.read().is_01() || !tmp_857_fu_12429_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp376_cast_cast_fu_12432_p3.read()) + sc_biguint<8>(tmp_857_fu_12429_p1.read()));
}

void mnist_fp16::thread_p_Repl2_1_trunc_fu_4717_p2() {
    p_Repl2_1_trunc_fu_4717_p2 = (!tmp350_cast_cast_fu_4710_p3.read().is_01() || !tmp_352_fu_4707_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp350_cast_cast_fu_4710_p3.read()) + sc_biguint<8>(tmp_352_fu_4707_p1.read()));
}

void mnist_fp16::thread_p_Repl2_22_trunc_fu_13532_p2() {
    p_Repl2_22_trunc_fu_13532_p2 = (!tmp_902_fu_13522_p1.read().is_01() || !tmp377_cast_cast_fu_13525_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_902_fu_13522_p1.read()) + sc_biguint<8>(tmp377_cast_cast_fu_13525_p3.read()));
}

void mnist_fp16::thread_p_Repl2_26_trunc_fu_14277_p2() {
    p_Repl2_26_trunc_fu_14277_p2 = (!tmp_912_fu_14267_p1.read().is_01() || !tmp378_cast_cast_fu_14270_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_912_fu_14267_p1.read()) + sc_biguint<8>(tmp378_cast_cast_fu_14270_p3.read()));
}

void mnist_fp16::thread_p_Repl2_28_trunc_fu_14936_p2() {
    p_Repl2_28_trunc_fu_14936_p2 = (!tmp379_cast_cast_fu_14929_p3.read().is_01() || !tmp_927_fu_14926_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp379_cast_cast_fu_14929_p3.read()) + sc_biguint<8>(tmp_927_fu_14926_p1.read()));
}

void mnist_fp16::thread_p_Repl2_4_trunc_fu_5836_p2() {
    p_Repl2_4_trunc_fu_5836_p2 = (!tmp_412_fu_5826_p1.read().is_01() || !tmp351_cast_cast_fu_5829_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_412_fu_5826_p1.read()) + sc_biguint<8>(tmp351_cast_cast_fu_5829_p3.read()));
}

void mnist_fp16::thread_p_Repl2_7_trunc_fu_6754_p2() {
    p_Repl2_7_trunc_fu_6754_p2 = (!tmp_611_fu_6744_p1.read().is_01() || !tmp352_cast_cast_fu_6747_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_611_fu_6744_p1.read()) + sc_biguint<8>(tmp352_cast_cast_fu_6747_p3.read()));
}

void mnist_fp16::thread_p_Result_14_fu_6608_p4() {
    p_Result_14_fu_6608_p4 = tmp_V_2_fu_6603_p3.read().range(0, 15);
}

void mnist_fp16::thread_p_Result_15_fu_6618_p3() {
    p_Result_15_fu_6618_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_14_fu_6608_p4.read());
}

void mnist_fp16::thread_p_Result_16_fu_6728_p4() {
    p_Result_16_fu_6728_p4 = tmp32_V_8_fu_6724_p1.read().range(30, 23);
}

void mnist_fp16::thread_p_Result_17_fu_6767_p5() {
    p_Result_17_fu_6767_p5 = esl_partset<32,32,9,32,32>(tmp32_V_8_reg_16402.read(), tmp_200_fu_6760_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16::thread_p_Result_19_fu_7822_p1() {
    p_Result_19_fu_7822_p1 = esl_zext<54,53>(tmp_160_fu_7815_p3.read());
}

void mnist_fp16::thread_p_Result_21_fu_8625_p4() {
    p_Result_21_fu_8625_p4 = tmp32_V_53_fu_8621_p1.read().range(30, 23);
}

void mnist_fp16::thread_p_Result_22_fu_8604_p2() {
    p_Result_22_fu_8604_p2 = (!tmp_686_fu_8600_p1.read().is_01())? sc_lv<16>(): p_Val2_30_reg_16910.read() >> (unsigned short)tmp_686_fu_8600_p1.read().to_uint();
}

void mnist_fp16::thread_p_Result_25_fu_9960_p1() {
    p_Result_25_fu_9960_p1 = esl_zext<54,53>(tmp_224_fu_9953_p3.read());
}

void mnist_fp16::thread_p_Result_28_fu_9679_p4() {
    p_Result_28_fu_9679_p4 = tmp32_V_54_fu_9675_p1.read().range(30, 23);
}

void mnist_fp16::thread_p_Result_29_fu_9649_p2() {
    p_Result_29_fu_9649_p2 = (!tmp_711_fu_9645_p1.read().is_01())? sc_lv<16>(): p_Val2_33_reg_17191.read() >> (unsigned short)tmp_711_fu_9645_p1.read().to_uint();
}

void mnist_fp16::thread_p_Result_33_fu_10461_p4() {
    p_Result_33_fu_10461_p4 = tmp_V_5_fu_10456_p3.read().range(0, 15);
}

void mnist_fp16::thread_p_Result_34_fu_10471_p3() {
    p_Result_34_fu_10471_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_33_fu_10461_p4.read());
}

void mnist_fp16::thread_p_Result_35_fu_10581_p4() {
    p_Result_35_fu_10581_p4 = tmp32_V_20_fu_10577_p1.read().range(30, 23);
}

void mnist_fp16::thread_p_Result_36_fu_10620_p5() {
    p_Result_36_fu_10620_p5 = esl_partset<32,32,9,32,32>(tmp32_V_20_reg_17482.read(), tmp_378_fu_10613_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16::thread_p_Result_38_fu_11646_p1() {
    p_Result_38_fu_11646_p1 = esl_zext<54,53>(tmp_326_fu_11639_p3.read());
}

void mnist_fp16::thread_p_Result_41_fu_12392_p2() {
    p_Result_41_fu_12392_p2 = (!tmp_855_fu_12388_p1.read().is_01())? sc_lv<16>(): p_Val2_42_reg_18005.read() >> (unsigned short)tmp_855_fu_12388_p1.read().to_uint();
}

void mnist_fp16::thread_p_Result_42_fu_12413_p4() {
    p_Result_42_fu_12413_p4 = tmp32_V_55_fu_12409_p1.read().range(30, 23);
}

void mnist_fp16::thread_p_Result_45_fu_14459_p1() {
    p_Result_45_fu_14459_p1 = esl_zext<54,53>(tmp_287_fu_14452_p3.read());
}

void mnist_fp16::thread_p_Result_47_fu_13771_p1() {
    p_Result_47_fu_13771_p1 = esl_zext<54,53>(tmp_402_fu_13764_p3.read());
}

void mnist_fp16::thread_p_Result_4_fu_3815_p1() {
    p_Result_4_fu_3815_p1 = esl_zext<54,53>(tmp_33_fu_3808_p3.read());
}

void mnist_fp16::thread_p_Result_50_fu_13485_p2() {
    p_Result_50_fu_13485_p2 = (!tmp_900_fu_13481_p1.read().is_01())? sc_lv<16>(): p_Val2_46_reg_18306.read() >> (unsigned short)tmp_900_fu_13481_p1.read().to_uint();
}

void mnist_fp16::thread_p_Result_51_fu_13506_p4() {
    p_Result_51_fu_13506_p4 = tmp32_V_56_fu_13502_p1.read().range(30, 23);
}

void mnist_fp16::thread_p_Result_53_fu_14131_p4() {
    p_Result_53_fu_14131_p4 = tmp_V_8_fu_14126_p3.read().range(0, 15);
}

void mnist_fp16::thread_p_Result_54_fu_14141_p3() {
    p_Result_54_fu_14141_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_53_fu_14131_p4.read());
}

void mnist_fp16::thread_p_Result_55_fu_14251_p4() {
    p_Result_55_fu_14251_p4 = tmp32_V_32_fu_14247_p1.read().range(30, 23);
}

void mnist_fp16::thread_p_Result_56_fu_14290_p5() {
    p_Result_56_fu_14290_p5 = esl_partset<32,32,9,32,32>(tmp32_V_32_reg_18556.read(), tmp_475_fu_14283_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16::thread_p_Result_57_fu_14786_p4() {
    p_Result_57_fu_14786_p4 = tmp_V_9_fu_14781_p3.read().range(0, 15);
}

void mnist_fp16::thread_p_Result_58_fu_14796_p3() {
    p_Result_58_fu_14796_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_57_fu_14786_p4.read());
}

void mnist_fp16::thread_p_Result_59_fu_14910_p4() {
    p_Result_59_fu_14910_p4 = tmp32_V_36_fu_14906_p1.read().range(30, 23);
}

void mnist_fp16::thread_p_Result_60_fu_14949_p5() {
    p_Result_60_fu_14949_p5 = esl_partset<32,32,9,32,32>(tmp32_V_36_reg_18745.read(), tmp_463_fu_14942_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16::thread_p_Result_63_fu_4571_p4() {
    p_Result_63_fu_4571_p4 = p_Val2_8_fu_4565_p3.read().range(0, 15);
}

void mnist_fp16::thread_p_Result_64_fu_4581_p3() {
    p_Result_64_fu_4581_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_63_fu_4571_p4.read());
}

void mnist_fp16::thread_p_Result_65_fu_4730_p5() {
    p_Result_65_fu_4730_p5 = esl_partset<32,32,9,32,32>(tmp32_V_51_reg_15851.read(), tmp_84_fu_4723_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16::thread_p_Result_66_fu_5179_p1() {
    p_Result_66_fu_5179_p1 = esl_zext<54,53>(tmp_128_fu_5172_p3.read());
}

void mnist_fp16::thread_p_Result_67_fu_5264_p1() {
    p_Result_67_fu_5264_p1 = esl_zext<54,53>(tmp_171_fu_5257_p3.read());
}

void mnist_fp16::thread_p_Result_68_fu_5681_p4() {
    p_Result_68_fu_5681_p4 = p_Val2_15_fu_5674_p3.read().range(0, 15);
}

void mnist_fp16::thread_p_Result_69_fu_5691_p3() {
    p_Result_69_fu_5691_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_68_fu_5681_p4.read());
}

void mnist_fp16::thread_p_Result_70_fu_5849_p5() {
    p_Result_70_fu_5849_p5 = esl_partset<32,32,9,32,32>(tmp32_V_52_reg_16142.read(), tmp_115_fu_5842_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16::thread_p_Result_71_fu_8505_p4() {
    p_Result_71_fu_8505_p4 = p_Val2_30_fu_8499_p3.read().range(0, 15);
}

void mnist_fp16::thread_p_Result_72_fu_8515_p3() {
    p_Result_72_fu_8515_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_71_fu_8505_p4.read());
}

void mnist_fp16::thread_p_Result_73_fu_8664_p5() {
    p_Result_73_fu_8664_p5 = esl_partset<32,32,9,32,32>(tmp32_V_53_reg_16936.read(), tmp_242_fu_8657_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16::thread_p_Result_74_fu_9048_p1() {
    p_Result_74_fu_9048_p1 = esl_zext<54,53>(tmp_293_fu_9041_p3.read());
}

void mnist_fp16::thread_p_Result_75_fu_9133_p1() {
    p_Result_75_fu_9133_p1 = esl_zext<54,53>(tmp_337_fu_9126_p3.read());
}

void mnist_fp16::thread_p_Result_76_fu_9550_p4() {
    p_Result_76_fu_9550_p4 = p_Val2_33_fu_9543_p3.read().range(0, 15);
}

void mnist_fp16::thread_p_Result_77_fu_9560_p3() {
    p_Result_77_fu_9560_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_76_fu_9550_p4.read());
}

void mnist_fp16::thread_p_Result_78_fu_9718_p5() {
    p_Result_78_fu_9718_p5 = esl_partset<32,32,9,32,32>(tmp32_V_54_reg_17222.read(), tmp_283_fu_9711_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16::thread_p_Result_79_fu_12293_p4() {
    p_Result_79_fu_12293_p4 = p_Val2_42_fu_12287_p3.read().range(0, 15);
}

void mnist_fp16::thread_p_Result_7_fu_6091_p1() {
    p_Result_7_fu_6091_p1 = esl_zext<54,53>(tmp_72_fu_6084_p3.read());
}

void mnist_fp16::thread_p_Result_80_fu_12303_p3() {
    p_Result_80_fu_12303_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_79_fu_12293_p4.read());
}

void mnist_fp16::thread_p_Result_81_fu_12452_p5() {
    p_Result_81_fu_12452_p5 = esl_partset<32,32,9,32,32>(tmp32_V_55_reg_18031.read(), tmp_420_fu_12445_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16::thread_p_Result_82_fu_12884_p1() {
    p_Result_82_fu_12884_p1 = esl_zext<54,53>(tmp_498_fu_12877_p3.read());
}

void mnist_fp16::thread_p_Result_83_fu_12969_p1() {
    p_Result_83_fu_12969_p1 = esl_zext<54,53>(tmp_528_fu_12962_p3.read());
}

void mnist_fp16::thread_p_Result_84_fu_13386_p4() {
    p_Result_84_fu_13386_p4 = p_Val2_46_fu_13379_p3.read().range(0, 15);
}

void mnist_fp16::thread_p_Result_85_fu_13396_p3() {
    p_Result_85_fu_13396_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_84_fu_13386_p4.read());
}

void mnist_fp16::thread_p_Result_86_fu_13545_p5() {
    p_Result_86_fu_13545_p5 = esl_partset<32,32,9,32,32>(tmp32_V_56_reg_18332.read(), tmp_496_fu_13538_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16::thread_p_Result_8_fu_5780_p2() {
    p_Result_8_fu_5780_p2 = (!tmp_407_fu_5776_p1.read().is_01())? sc_lv<16>(): p_Val2_15_reg_16111.read() >> (unsigned short)tmp_407_fu_5776_p1.read().to_uint();
}

void mnist_fp16::thread_p_Result_9_fu_4691_p4() {
    p_Result_9_fu_4691_p4 = tmp32_V_51_fu_4687_p1.read().range(30, 23);
}

void mnist_fp16::thread_p_Result_s_42_fu_5810_p4() {
    p_Result_s_42_fu_5810_p4 = tmp32_V_52_fu_5806_p1.read().range(30, 23);
}

void mnist_fp16::thread_p_Result_s_fu_4670_p2() {
    p_Result_s_fu_4670_p2 = (!tmp_345_fu_4666_p1.read().is_01())? sc_lv<16>(): p_Val2_8_reg_15825.read() >> (unsigned short)tmp_345_fu_4666_p1.read().to_uint();
}

void mnist_fp16::thread_p_Val2_10_fu_9498_p3() {
    p_Val2_10_fu_9498_p3 = (!or_cond23_fu_9492_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond23_fu_9492_p2.read()[0].to_bool())? newSel27_fu_9484_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_p_Val2_11_fu_13185_p3() {
    p_Val2_11_fu_13185_p3 = (!or_cond35_fu_13179_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond35_fu_13179_p2.read()[0].to_bool())? newSel42_fu_13171_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_p_Val2_13_fu_13334_p3() {
    p_Val2_13_fu_13334_p3 = (!or_cond38_fu_13328_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond38_fu_13328_p2.read()[0].to_bool())? newSel45_fu_13320_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_p_Val2_15_fu_5674_p3() {
    p_Val2_15_fu_5674_p3 = (!is_neg_1_fu_5666_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_1_fu_5666_p3.read()[0].to_bool())? tmp_73_reg_15923.read(): p_Val2_3_reg_1567.read());
}

void mnist_fp16::thread_p_Val2_30_fu_8499_p3() {
    p_Val2_30_fu_8499_p3 = (!is_neg_2_fu_8492_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_2_fu_8492_p3.read()[0].to_bool())? tmp_182_reg_16900.read(): p_Val2_28_reg_16891.read());
}

void mnist_fp16::thread_p_Val2_33_fu_9543_p3() {
    p_Val2_33_fu_9543_p3 = (!is_neg_3_fu_9535_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_3_fu_9535_p3.read()[0].to_bool())? tmp_226_reg_17003.read(): p_Val2_1_reg_2040.read());
}

void mnist_fp16::thread_p_Val2_42_fu_12287_p3() {
    p_Val2_42_fu_12287_p3 = (!is_neg_4_fu_12280_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_4_fu_12280_p3.read()[0].to_bool())? tmp_358_reg_17995.read(): p_Val2_40_reg_17986.read());
}

void mnist_fp16::thread_p_Val2_46_fu_13379_p3() {
    p_Val2_46_fu_13379_p3 = (!is_neg_5_fu_13371_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_5_fu_13371_p3.read()[0].to_bool())? tmp_404_reg_18113.read(): p_Val2_44_reg_2511.read());
}

void mnist_fp16::thread_p_Val2_4_fu_5480_p3() {
    p_Val2_4_fu_5480_p3 = (!or_cond8_fu_5474_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond8_fu_5474_p2.read()[0].to_bool())? newSel10_fu_5466_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_p_Val2_5_fu_9349_p3() {
    p_Val2_5_fu_9349_p3 = (!or_cond20_fu_9343_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond20_fu_9343_p2.read()[0].to_bool())? newSel24_fu_9335_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_p_Val2_7_fu_5629_p3() {
    p_Val2_7_fu_5629_p3 = (!or_cond11_fu_5623_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond11_fu_5623_p2.read()[0].to_bool())? newSel13_fu_5615_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_p_Val2_8_fu_4565_p3() {
    p_Val2_8_fu_4565_p3 = (!is_neg_fu_4558_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_fu_4558_p3.read()[0].to_bool())? tmp_55_reg_15815.read(): p_Val2_s_reg_15806.read());
}

void mnist_fp16::thread_p_a_assign_load_to_i_fu_14989_p1() {
    p_a_assign_load_to_i_fu_14989_p1 = reg_2900.read();
}

void mnist_fp16::thread_p_shl100_cast_fu_11940_p3() {
    p_shl100_cast_fu_11940_p3 = esl_concat<9,3>(tmp_793_fu_11931_p2.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl102_cast_fu_11295_p1() {
    p_shl102_cast_fu_11295_p1 = esl_zext<8,7>(tmp_808_fu_11287_p3.read());
}

void mnist_fp16::thread_p_shl103_cast_fu_13596_p1() {
    p_shl103_cast_fu_13596_p1 = esl_zext<9,8>(tmp_810_fu_13588_p3.read());
}

void mnist_fp16::thread_p_shl104_cast_fu_12564_p3() {
    p_shl104_cast_fu_12564_p3 = esl_concat<8,3>(tmp_813_fu_12560_p1.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl105_cast_fu_11353_p3() {
    p_shl105_cast_fu_11353_p3 = esl_concat<8,3>(tmp_815_fu_11344_p2.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl106_cast_fu_11384_p3() {
    p_shl106_cast_fu_11384_p3 = esl_concat<10,2>(tmp_819_fu_11380_p1.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_p_shl107_cast_fu_12214_p1() {
    p_shl107_cast_fu_12214_p1 = esl_zext<8,7>(tmp_838_fu_12207_p3.read());
}

void mnist_fp16::thread_p_shl108_cast_fu_12244_p3() {
    p_shl108_cast_fu_12244_p3 = esl_concat<8,3>(tmp_841_reg_17976.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl109_cast_fu_14024_p1() {
    p_shl109_cast_fu_14024_p1 = esl_zext<9,8>(tmp_844_fu_14016_p3.read());
}

void mnist_fp16::thread_p_shl10_cast_fu_3035_p1() {
    p_shl10_cast_fu_3035_p1 = esl_zext<11,10>(tmp_7_fu_3027_p3.read());
}

void mnist_fp16::thread_p_shl110_cast_fu_13639_p3() {
    p_shl110_cast_fu_13639_p3 = esl_concat<8,3>(tmp_847_fu_13635_p1.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl111_cast_fu_14067_p3() {
    p_shl111_cast_fu_14067_p3 = esl_concat<8,3>(tmp_867_fu_14063_p1.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl113_cast_fu_12649_p1() {
    p_shl113_cast_fu_12649_p1 = esl_zext<9,8>(tmp_877_fu_12641_p3.read());
}

void mnist_fp16::thread_p_shl114_cast_fu_12708_p3() {
    p_shl114_cast_fu_12708_p3 = esl_concat<9,3>(tmp_880_fu_12699_p2.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl115_cast_fu_12739_p3() {
    p_shl115_cast_fu_12739_p3 = esl_concat<11,2>(tmp_916_fu_12735_p1.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_p_shl116_cast_fu_14735_p1() {
    p_shl116_cast_fu_14735_p1 = esl_zext<9,8>(tmp_886_fu_14727_p3.read());
}

void mnist_fp16::thread_p_shl117_cast_fu_14747_p1() {
    p_shl117_cast_fu_14747_p1 = esl_zext<9,6>(tmp_887_fu_14739_p3.read());
}

void mnist_fp16::thread_p_shl11_cast_fu_3047_p1() {
    p_shl11_cast_fu_3047_p1 = esl_zext<11,6>(tmp_10_fu_3039_p3.read());
}

void mnist_fp16::thread_p_shl14_cast_fu_3386_p1() {
    p_shl14_cast_fu_3386_p1 = esl_zext<9,8>(tmp_17_fu_3378_p3.read());
}

void mnist_fp16::thread_p_shl16_cast1_fu_3360_p1() {
    p_shl16_cast1_fu_3360_p1 = esl_zext<9,5>(tmp_13_fu_3352_p3.read());
}

void mnist_fp16::thread_p_shl16_cast_fu_3364_p1() {
    p_shl16_cast_fu_3364_p1 = esl_zext<6,5>(tmp_13_fu_3352_p3.read());
}

void mnist_fp16::thread_p_shl17_cast_fu_3425_p3() {
    p_shl17_cast_fu_3425_p3 = esl_concat<8,5>(tmp_60_fu_3421_p1.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_p_shl18_cast_fu_3441_p1() {
    p_shl18_cast_fu_3441_p1 = esl_sext<13,12>(tmp_61_fu_3433_p3.read());
}

void mnist_fp16::thread_p_shl1_cast_fu_3065_p1() {
    p_shl1_cast_fu_3065_p1 = esl_zext<11,7>(p_shl1_fu_3057_p3.read());
}

void mnist_fp16::thread_p_shl1_fu_3057_p3() {
    p_shl1_fu_3057_p3 = esl_concat<5,2>(p_s_reg_1314.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_p_shl20_cast_fu_3493_p1() {
    p_shl20_cast_fu_3493_p1 = esl_zext<11,10>(tmp_159_fu_3485_p3.read());
}

void mnist_fp16::thread_p_shl21_cast_fu_3505_p1() {
    p_shl21_cast_fu_3505_p1 = esl_zext<11,6>(tmp_161_fu_3497_p3.read());
}

void mnist_fp16::thread_p_shl22_cast_fu_3620_p1() {
    p_shl22_cast_fu_3620_p1 = esl_zext<9,8>(tmp_51_fu_3612_p3.read());
}

void mnist_fp16::thread_p_shl23_cast_fu_3632_p1() {
    p_shl23_cast_fu_3632_p1 = esl_zext<9,5>(tmp_53_fu_3624_p3.read());
}

void mnist_fp16::thread_p_shl24_cast_fu_3671_p3() {
    p_shl24_cast_fu_3671_p3 = esl_concat<8,5>(tmp_104_fu_3667_p1.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_p_shl25_cast_fu_3687_p1() {
    p_shl25_cast_fu_3687_p1 = esl_sext<13,12>(tmp_105_fu_3679_p3.read());
}

void mnist_fp16::thread_p_shl26_cast_fu_4070_p1() {
    p_shl26_cast_fu_4070_p1 = esl_zext<9,8>(tmp_96_fu_4062_p3.read());
}

void mnist_fp16::thread_p_shl27_cast_fu_4082_p1() {
    p_shl27_cast_fu_4082_p1 = esl_zext<9,4>(tmp_97_fu_4074_p3.read());
}

void mnist_fp16::thread_p_shl28_cast_fu_4115_p3() {
    p_shl28_cast_fu_4115_p3 = esl_concat<8,5>(tmp_120_fu_4111_p1.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_p_shl29_cast_fu_4131_p1() {
    p_shl29_cast_fu_4131_p1 = esl_sext<13,11>(tmp_127_fu_4123_p3.read());
}

void mnist_fp16::thread_p_shl2_cast_fu_4149_p1() {
    p_shl2_cast_fu_4149_p1 = esl_zext<11,10>(p_shl2_fu_4141_p3.read());
}

void mnist_fp16::thread_p_shl2_fu_4141_p3() {
    p_shl2_fu_4141_p3 = esl_concat<5,5>(p_4_reg_1490.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_p_shl30_cast_fu_4511_p3() {
    p_shl30_cast_fu_4511_p3 = esl_concat<8,5>(tmp_316_reg_15796.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_p_shl31_cast_fu_4525_p1() {
    p_shl31_cast_fu_4525_p1 = esl_sext<13,11>(tmp_317_fu_4518_p3.read());
}

void mnist_fp16::thread_p_shl32_cast_fu_4473_p1() {
    p_shl32_cast_fu_4473_p1 = esl_zext<8,7>(tmp_301_fu_4466_p3.read());
}

void mnist_fp16::thread_p_shl33_cast_fu_4484_p1() {
    p_shl33_cast_fu_4484_p1 = esl_zext<8,4>(tmp_304_fu_4477_p3.read());
}

void mnist_fp16::thread_p_shl34_cast_fu_4795_p1() {
    p_shl34_cast_fu_4795_p1 = esl_zext<9,8>(tmp_111_fu_4787_p3.read());
}

void mnist_fp16::thread_p_shl36_cast_fu_5896_p1() {
    p_shl36_cast_fu_5896_p1 = esl_zext<9,8>(tmp_179_fu_5888_p3.read());
}

void mnist_fp16::thread_p_shl37_cast_fu_5908_p1() {
    p_shl37_cast_fu_5908_p1 = esl_zext<9,5>(tmp_183_fu_5900_p3.read());
}

void mnist_fp16::thread_p_shl38_cast_fu_4834_p3() {
    p_shl38_cast_fu_4834_p3 = esl_concat<8,5>(tmp_189_fu_4830_p1.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_p_shl39_cast_fu_4850_p1() {
    p_shl39_cast_fu_4850_p1 = esl_sext<13,12>(tmp_190_fu_4842_p3.read());
}

void mnist_fp16::thread_p_shl3_cast_fu_4161_p1() {
    p_shl3_cast_fu_4161_p1 = esl_zext<11,7>(p_shl3_fu_4153_p3.read());
}

void mnist_fp16::thread_p_shl3_fu_4153_p3() {
    p_shl3_fu_4153_p3 = esl_concat<5,2>(p_4_reg_1490.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_p_shl40_cast_fu_6336_p1() {
    p_shl40_cast_fu_6336_p1 = esl_zext<9,8>(tmp_228_fu_6328_p3.read());
}

void mnist_fp16::thread_p_shl41_cast_fu_6348_p1() {
    p_shl41_cast_fu_6348_p1 = esl_zext<9,5>(tmp_229_fu_6340_p3.read());
}

void mnist_fp16::thread_p_shl42_cast_fu_6370_p1() {
    p_shl42_cast_fu_6370_p1 = esl_zext<8,7>(tmp_238_fu_6362_p3.read());
}

void mnist_fp16::thread_p_shl43_cast_fu_6382_p1() {
    p_shl43_cast_fu_6382_p1 = esl_zext<8,4>(tmp_239_fu_6374_p3.read());
}

void mnist_fp16::thread_p_shl44_cast_fu_5947_p3() {
    p_shl44_cast_fu_5947_p3 = esl_concat<8,5>(tmp_249_fu_5943_p1.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_p_shl45_cast_fu_5963_p1() {
    p_shl45_cast_fu_5963_p1 = esl_sext<13,12>(tmp_254_fu_5955_p3.read());
}

void mnist_fp16::thread_p_shl46_cast_fu_6421_p3() {
    p_shl46_cast_fu_6421_p3 = esl_concat<7,4>(tmp_353_fu_6417_p1.read(), ap_const_lv4_0);
}

}

