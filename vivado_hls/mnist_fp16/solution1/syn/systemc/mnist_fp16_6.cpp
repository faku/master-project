#include "mnist_fp16.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp16::thread_tmp_521_fu_13056_p2() {
    tmp_521_fu_13056_p2 = (!man_V_20_reg_18213.read().is_01() || !tmp_520_fu_13052_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_20_reg_18213.read()) >> (unsigned short)tmp_520_fu_13052_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_522_fu_6531_p3() {
    tmp_522_fu_6531_p3 = esl_concat<10,2>(tmp_513_fu_6514_p2.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_523_fu_13072_p1() {
    tmp_523_fu_13072_p1 = esl_sext<32,16>(tmp_931_reg_18236.read());
}

void mnist_fp16::thread_tmp_524_fu_13075_p2() {
    tmp_524_fu_13075_p2 = (!sh_amt_10_cast_fu_13044_p1.read().is_01())? sc_lv<32>(): tmp_523_fu_13072_p1.read() << (unsigned short)sh_amt_10_cast_fu_13044_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_525_fu_6543_p2() {
    tmp_525_fu_6543_p2 = (!p_shl60_cast_fu_6523_p3.read().is_01() || !p_shl61_cast_fu_6539_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl60_cast_fu_6523_p3.read()) - sc_bigint<13>(p_shl61_cast_fu_6539_p1.read()));
}

void mnist_fp16::thread_tmp_526_cast_fu_12788_p1() {
    tmp_526_cast_fu_12788_p1 = esl_zext<13,2>(p_81_reg_2569.read());
}

void mnist_fp16::thread_tmp_526_fu_5073_p2() {
    tmp_526_fu_5073_p2 = (!tmp_104_cast_fu_5069_p1.read().is_01() || !tmp_474_reg_15936.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_104_cast_fu_5069_p1.read()) + sc_biguint<13>(tmp_474_reg_15936.read()));
}

void mnist_fp16::thread_tmp_527_fu_12959_p1() {
    tmp_527_fu_12959_p1 = esl_zext<12,11>(exp_tmp_V_5_reg_18197.read());
}

void mnist_fp16::thread_tmp_528_fu_12962_p3() {
    tmp_528_fu_12962_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_937_reg_18202.read());
}

void mnist_fp16::thread_tmp_529_fu_14770_p2() {
    tmp_529_fu_14770_p2 = (!ap_const_lv16_0.is_01() || !pool3_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(pool3_0_V_q0.read()));
}

void mnist_fp16::thread_tmp_52_fu_3864_p2() {
    tmp_52_fu_3864_p2 = (!F2_fu_3832_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_fu_3832_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_530_fu_14993_p4() {
    tmp_530_fu_14993_p4 = p_a_assign_load_to_i_fu_14989_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_531_fu_5101_p1() {
    tmp_531_fu_5101_p1 = ireg_V_12_fu_5097_p1.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_532_fu_15011_p4() {
    tmp_532_fu_15011_p4 = compute14_to_int_fu_15007_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_533_fu_5087_p2() {
    tmp_533_fu_5087_p2 = (!tmp_169_cast_fu_5083_p1.read().is_01() || !tmp_489_reg_15941.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_169_cast_fu_5083_p1.read()) + sc_biguint<9>(tmp_489_reg_15941.read()));
}

void mnist_fp16::thread_tmp_534_fu_15037_p2() {
    tmp_534_fu_15037_p2 = (notrhs12_fu_15031_p2.read() | notlhs12_fu_15025_p2.read());
}

void mnist_fp16::thread_tmp_535_fu_15055_p2() {
    tmp_535_fu_15055_p2 = (notrhs13_fu_15049_p2.read() | notlhs13_fu_15043_p2.read());
}

void mnist_fp16::thread_tmp_536_fu_15061_p2() {
    tmp_536_fu_15061_p2 = (tmp_534_fu_15037_p2.read() & tmp_535_fu_15055_p2.read());
}

void mnist_fp16::thread_tmp_538_fu_15067_p2() {
    tmp_538_fu_15067_p2 = (tmp_536_fu_15061_p2.read() & grp_fu_2839_p2.read());
}

void mnist_fp16::thread_tmp_539_fu_12868_p2() {
    tmp_539_fu_12868_p2 = (!tmp_935_fu_12842_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_935_fu_12842_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_53_fu_3624_p3() {
    tmp_53_fu_3624_p3 = esl_concat<3,2>(p_7_reg_1435.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_540_fu_5123_p1() {
    tmp_540_fu_5123_p1 = ireg_V_12_fu_5097_p1.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_541_fu_12992_p2() {
    tmp_541_fu_12992_p2 = (!F2_11_fu_12986_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_11_fu_12986_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_542_fu_12998_p2() {
    tmp_542_fu_12998_p2 = (!ap_const_lv12_FF2.is_01() || !F2_11_fu_12986_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_11_fu_12986_p2.read()));
}

void mnist_fp16::thread_tmp_543_fu_13004_p2() {
    tmp_543_fu_13004_p2 = (!ap_const_lv12_E.is_01() || !F2_11_fu_12986_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_11_fu_12986_p2.read()));
}

void mnist_fp16::thread_tmp_544_fu_13018_p2() {
    tmp_544_fu_13018_p2 = (!F2_11_fu_12986_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_11_fu_12986_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_545_fu_5234_p1() {
    tmp_545_fu_5234_p1 = man_V_4_fu_5189_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_546_fu_13196_p2() {
    tmp_546_fu_13196_p2 = (!sh_amt_11_reg_18258.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_11_reg_18258.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_547_fu_5238_p4() {
    tmp_547_fu_5238_p4 = sh_amt_2_fu_5220_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_548_fu_5356_p1() {
    tmp_548_fu_5356_p1 = tmp_163_fu_5351_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_549_fu_13201_p1() {
    tmp_549_fu_13201_p1 = esl_zext<54,32>(sh_amt_11_cast_fu_13193_p1.read());
}

void mnist_fp16::thread_tmp_54_fu_3928_p2() {
    tmp_54_fu_3928_p2 = (!sh_amt_reg_15658.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_reg_15658.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_550_fu_13205_p2() {
    tmp_550_fu_13205_p2 = (!man_V_25_reg_18247.read().is_01() || !tmp_549_fu_13201_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_25_reg_18247.read()) >> (unsigned short)tmp_549_fu_13201_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_551_fu_14859_p2() {
    tmp_551_fu_14859_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_18_cast_fu_14836_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_18_cast_fu_14836_p1.read()));
}

void mnist_fp16::thread_tmp_552_fu_5376_p1() {
    tmp_552_fu_5376_p1 = tmp_166_fu_5370_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_553_fu_14920_p2() {
    tmp_553_fu_14920_p2 = (!p_Result_59_fu_14910_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_59_fu_14910_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16::thread_tmp_554_fu_5137_p1() {
    tmp_554_fu_5137_p1 = ireg_V_13_fu_5133_p1.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_555_fu_13221_p1() {
    tmp_555_fu_13221_p1 = esl_sext<32,16>(tmp_938_reg_18270.read());
}

void mnist_fp16::thread_tmp_556_fu_13224_p2() {
    tmp_556_fu_13224_p2 = (!sh_amt_11_cast_fu_13193_p1.read().is_01())? sc_lv<32>(): tmp_555_fu_13221_p1.read() << (unsigned short)sh_amt_11_cast_fu_13193_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_557_fu_7046_p2() {
    tmp_557_fu_7046_p2 = (!r_V_11_fu_7040_p2.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): (sc_biguint<4>(r_V_11_fu_7040_p2.read()) < sc_biguint<4>(ap_const_lv4_E));
}

void mnist_fp16::thread_tmp_559_fu_5159_p1() {
    tmp_559_fu_5159_p1 = ireg_V_13_fu_5133_p1.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_55_fu_4552_p2() {
    tmp_55_fu_4552_p2 = (!ap_const_lv16_0.is_01() || !relu1_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu1_0_V_q0.read()));
}

void mnist_fp16::thread_tmp_560_fu_15135_p4() {
    tmp_560_fu_15135_p4 = pred_2_to_int_fu_15132_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_561_fu_5319_p1() {
    tmp_561_fu_5319_p1 = man_V_9_fu_5274_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_562_fu_15153_p4() {
    tmp_562_fu_15153_p4 = pred_to_int_fu_15149_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_563_fu_5323_p4() {
    tmp_563_fu_5323_p4 = sh_amt_3_fu_5305_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_564_fu_15179_p2() {
    tmp_564_fu_15179_p2 = (notrhs14_fu_15173_p2.read() | notlhs14_fu_15167_p2.read());
}

void mnist_fp16::thread_tmp_565_fu_15197_p2() {
    tmp_565_fu_15197_p2 = (notrhs15_fu_15191_p2.read() | notlhs15_fu_15185_p2.read());
}

void mnist_fp16::thread_tmp_566_fu_15203_p2() {
    tmp_566_fu_15203_p2 = (tmp_564_fu_15179_p2.read() & tmp_565_fu_15197_p2.read());
}

void mnist_fp16::thread_tmp_568_fu_15209_p2() {
    tmp_568_fu_15209_p2 = (tmp_566_fu_15203_p2.read() & grp_fu_2839_p2.read());
}

void mnist_fp16::thread_tmp_569_fu_5505_p1() {
    tmp_569_fu_5505_p1 = tmp_185_fu_5500_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_56_fu_6925_p2() {
    tmp_56_fu_6925_p2 = (!p_20_reg_1771.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_20_reg_1771.read() != ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_570_cast_fu_3318_p1() {
    tmp_570_cast_fu_3318_p1 = esl_sext<64,11>(tmp_93_reg_15465.read());
}

void mnist_fp16::thread_tmp_570_fu_5525_p1() {
    tmp_570_fu_5525_p1 = tmp_199_fu_5519_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_571_cast_fu_3331_p1() {
    tmp_571_cast_fu_3331_p1 = esl_sext<64,11>(tmp_94_fu_3326_p2.read());
}

void mnist_fp16::thread_tmp_571_fu_7036_p1() {
    tmp_571_fu_7036_p1 = p_30_reg_1782.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_572_fu_7052_p2() {
    tmp_572_fu_7052_p2 = (!ap_const_lv4_1.is_01() || !tmp_571_fu_7036_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_571_fu_7036_p1.read()));
}

void mnist_fp16::thread_tmp_573_fu_7098_p1() {
    tmp_573_fu_7098_p1 = mul3_fu_15262_p2.read().range(22-1, 0);
}

void mnist_fp16::thread_tmp_574_cast_fu_4092_p1() {
    tmp_574_cast_fu_4092_p1 = esl_sext<10,9>(tmp_98_fu_4086_p2.read());
}

void mnist_fp16::thread_tmp_575_fu_7135_p4() {
    tmp_575_fu_7135_p4 = neg_mul3_fu_7130_p2.read().range(21, 15);
}

void mnist_fp16::thread_tmp_576_fu_7145_p1() {
    tmp_576_fu_7145_p1 = esl_sext<11,7>(tmp_575_fu_7135_p4.read());
}

void mnist_fp16::thread_tmp_578_fu_7149_p1() {
    tmp_578_fu_7149_p1 = esl_sext<11,9>(tmp_577_reg_16504.read());
}

void mnist_fp16::thread_tmp_579_fu_7152_p3() {
    tmp_579_fu_7152_p3 = (!tmp_574_reg_16496.read()[0].is_01())? sc_lv<11>(): ((tmp_574_reg_16496.read()[0].to_bool())? tmp_576_fu_7145_p1.read(): tmp_578_fu_7149_p1.read());
}

void mnist_fp16::thread_tmp_57_fu_6931_p2() {
    tmp_57_fu_6931_p2 = (!p_20_reg_1771.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_20_reg_1771.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp16::thread_tmp_580_cast1_fu_4779_p1() {
    tmp_580_cast1_fu_4779_p1 = esl_zext<6,5>(tmp_110_fu_4771_p3.read());
}

void mnist_fp16::thread_tmp_580_cast_fu_4783_p1() {
    tmp_580_cast_fu_4783_p1 = esl_zext<9,5>(tmp_110_fu_4771_p3.read());
}

void mnist_fp16::thread_tmp_580_fu_7228_p1() {
    tmp_580_fu_7228_p1 = grp_fu_7172_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_581_fu_7118_p1() {
    tmp_581_fu_7118_p1 = mul4_fu_15270_p2.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_582_fu_7183_p4() {
    tmp_582_fu_7183_p4 = neg_mul4_fu_7178_p2.read().range(22, 19);
}

void mnist_fp16::thread_tmp_583_cast_fu_4805_p1() {
    tmp_583_cast_fu_4805_p1 = esl_sext<10,9>(tmp_116_fu_4799_p2.read());
}

void mnist_fp16::thread_tmp_583_fu_7193_p1() {
    tmp_583_fu_7193_p1 = esl_sext<12,4>(tmp_582_fu_7183_p4.read());
}

void mnist_fp16::thread_tmp_585_fu_7197_p1() {
    tmp_585_fu_7197_p1 = esl_sext<12,5>(tmp_584_reg_16514.read());
}

void mnist_fp16::thread_tmp_586_fu_7207_p1() {
    tmp_586_fu_7207_p1 = p_v1_fu_7200_p3.read().range(2-1, 0);
}

void mnist_fp16::thread_tmp_587_fu_7217_p1() {
    tmp_587_fu_7217_p1 = p_v1_fu_7200_p3.read().range(2-1, 0);
}

void mnist_fp16::thread_tmp_588_cast_fu_3718_p1() {
    tmp_588_cast_fu_3718_p1 = esl_zext<64,13>(tmp_137_fu_3713_p2.read());
}

void mnist_fp16::thread_tmp_588_fu_7232_p3() {
    tmp_588_fu_7232_p3 = esl_concat<2,4>(r_V_12_reg_16524.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_589_cast_fu_3545_p1() {
    tmp_589_cast_fu_3545_p1 = esl_zext<64,13>(tmp_158_fu_3540_p2.read());
}

void mnist_fp16::thread_tmp_589_fu_7243_p3() {
    tmp_589_fu_7243_p3 = esl_concat<2,1>(r_V_12_reg_16524.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_58_fu_3636_p2() {
    tmp_58_fu_3636_p2 = (!p_shl22_cast_fu_3620_p1.read().is_01() || !p_shl23_cast_fu_3632_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl22_cast_fu_3620_p1.read()) - sc_biguint<9>(p_shl23_cast_fu_3632_p1.read()));
}

void mnist_fp16::thread_tmp_590_fu_7254_p2() {
    tmp_590_fu_7254_p2 = (!p_shl62_cast_fu_7239_p1.read().is_01() || !p_shl63_cast_fu_7250_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl62_cast_fu_7239_p1.read()) - sc_biguint<7>(p_shl63_cast_fu_7250_p1.read()));
}

void mnist_fp16::thread_tmp_591_cast_fu_10414_p1() {
    tmp_591_cast_fu_10414_p1 = esl_zext<4,2>(p_75_reg_2212.read());
}

void mnist_fp16::thread_tmp_591_fu_7264_p2() {
    tmp_591_fu_7264_p2 = (!tmp_580_fu_7228_p1.read().is_01() || !tmp_693_cast_fu_7260_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_580_fu_7228_p1.read()) + sc_bigint<8>(tmp_693_cast_fu_7260_p1.read()));
}

void mnist_fp16::thread_tmp_592_fu_7270_p1() {
    tmp_592_fu_7270_p1 = tmp_591_fu_7264_p2.read().range(7-1, 0);
}

void mnist_fp16::thread_tmp_593_fu_7284_p3() {
    tmp_593_fu_7284_p3 = esl_concat<8,1>(tmp_591_reg_16530.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_594_fu_7295_p2() {
    tmp_594_fu_7295_p2 = (!p_shl64_cast_fu_7277_p3.read().is_01() || !p_shl65_cast_fu_7291_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl64_cast_fu_7277_p3.read()) - sc_bigint<11>(p_shl65_cast_fu_7291_p1.read()));
}

void mnist_fp16::thread_tmp_595_fu_7301_p2() {
    tmp_595_fu_7301_p2 = (!tmp_88_cast_fu_7274_p1.read().is_01() || !tmp_594_fu_7295_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_88_cast_fu_7274_p1.read()) + sc_biguint<11>(tmp_594_fu_7295_p2.read()));
}

void mnist_fp16::thread_tmp_596_fu_7316_p2() {
    tmp_596_fu_7316_p2 = (!tmp_656_cast_reg_16455.read().is_01() || !tmp_110_cast_fu_7312_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_656_cast_reg_16455.read()) + sc_biguint<12>(tmp_110_cast_fu_7312_p1.read()));
}

void mnist_fp16::thread_tmp_597_fu_8069_p3() {
    tmp_597_fu_8069_p3 = esl_concat<4,4>(p_21_reg_1945.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_598_cast_fu_5918_p1() {
    tmp_598_cast_fu_5918_p1 = esl_sext<10,9>(tmp_184_fu_5912_p2.read());
}

void mnist_fp16::thread_tmp_598_fu_7669_p2() {
    tmp_598_fu_7669_p2 = (!tmp_669_cast_reg_16656.read().is_01() || !tmp_150_cast_fu_7665_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_669_cast_reg_16656.read()) + sc_biguint<10>(tmp_150_cast_fu_7665_p1.read()));
}

void mnist_fp16::thread_tmp_599_fu_7674_p1() {
    tmp_599_fu_7674_p1 = tmp_598_fu_7669_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_59_fu_3416_p2() {
    tmp_59_fu_3416_p2 = (!tmp_69_cast_reg_15493.read().is_01() || !tmp_11_cast_fu_3412_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_69_cast_reg_15493.read()) + sc_biguint<10>(tmp_11_cast_fu_3412_p1.read()));
}

void mnist_fp16::thread_tmp_600_fu_7686_p3() {
    tmp_600_fu_7686_p3 = esl_concat<10,1>(tmp_598_fu_7669_p2.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_601_fu_7698_p2() {
    tmp_601_fu_7698_p2 = (!p_shl66_cast_fu_7678_p3.read().is_01() || !p_shl67_cast_fu_7694_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl66_cast_fu_7678_p3.read()) - sc_bigint<12>(p_shl67_cast_fu_7694_p1.read()));
}

void mnist_fp16::thread_tmp_602_fu_6574_p2() {
    tmp_602_fu_6574_p2 = (!tmp_525_reg_16336.read().is_01() || !tmp_154_cast_cast_fu_6570_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_525_reg_16336.read()) + sc_biguint<13>(tmp_154_cast_cast_fu_6570_p1.read()));
}

void mnist_fp16::thread_tmp_603_cast_fu_3581_p1() {
    tmp_603_cast_fu_3581_p1 = esl_sext<64,11>(tmp_204_fu_3576_p2.read());
}

void mnist_fp16::thread_tmp_604_cast_fu_3595_p1() {
    tmp_604_cast_fu_3595_p1 = esl_zext<64,7>(tmp_206_fu_3590_p2.read());
}

void mnist_fp16::thread_tmp_604_fu_6640_p1() {
    tmp_604_fu_6640_p1 = msb_idx_4_fu_6634_p2.read().range(31-1, 0);
}

void mnist_fp16::thread_tmp_606_fu_6662_p4() {
    tmp_606_fu_6662_p4 = msb_idx_5_fu_6652_p3.read().range(30, 5);
}

void mnist_fp16::thread_tmp_607_cast_fu_6358_p1() {
    tmp_607_cast_fu_6358_p1 = esl_sext<10,9>(tmp_237_fu_6352_p2.read());
}

void mnist_fp16::thread_tmp_607_fu_6693_p1() {
    tmp_607_fu_6693_p1 = msb_idx_5_fu_6652_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_608_fu_6697_p2() {
    tmp_608_fu_6697_p2 = (!ap_const_lv4_1.is_01() || !tmp_607_fu_6693_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_607_fu_6693_p1.read()));
}

void mnist_fp16::thread_tmp_609_fu_6703_p1() {
    tmp_609_fu_6703_p1 = esl_zext<16,4>(tmp_608_fu_6697_p2.read());
}

void mnist_fp16::thread_tmp_60_fu_3421_p1() {
    tmp_60_fu_3421_p1 = tmp_59_fu_3416_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_610_cast_fu_6392_p1() {
    tmp_610_cast_fu_6392_p1 = esl_sext<9,8>(tmp_244_fu_6386_p2.read());
}

void mnist_fp16::thread_tmp_610_fu_6707_p2() {
    tmp_610_fu_6707_p2 = (!tmp_609_fu_6703_p1.read().is_01())? sc_lv<16>(): tmp_V_2_reg_16376.read() >> (unsigned short)tmp_609_fu_6703_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_611_fu_6744_p1() {
    tmp_611_fu_6744_p1 = msb_idx_4_reg_16382.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_612_fu_6802_p1() {
    tmp_612_fu_6802_p1 = p_03_i1_to_int_fu_6789_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_613_fu_6820_p1() {
    tmp_613_fu_6820_p1 = tmp_140_to_int_fu_6806_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_614_fu_8705_p3() {
    tmp_614_fu_8705_p3 = esl_concat<4,3>(p_26_reg_2005.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_615_fu_8717_p3() {
    tmp_615_fu_8717_p3 = esl_concat<4,4>(p_26_reg_2005.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_616_fu_8729_p3() {
    tmp_616_fu_8729_p3 = esl_concat<4,1>(p_26_reg_2005.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_617_fu_8741_p2() {
    tmp_617_fu_8741_p2 = (!p_shl68_cast_fu_8725_p1.read().is_01() || !p_shl69_cast_fu_8737_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl68_cast_fu_8725_p1.read()) - sc_biguint<9>(p_shl69_cast_fu_8737_p1.read()));
}

void mnist_fp16::thread_tmp_618_fu_8109_p2() {
    tmp_618_fu_8109_p2 = (!lhs_V_3_cast_fu_8105_p1.read().is_01() || !tmp_701_cast_reg_16788.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_3_cast_fu_8105_p1.read()) + sc_biguint<9>(tmp_701_cast_reg_16788.read()));
}

void mnist_fp16::thread_tmp_619_fu_8122_p1() {
    tmp_619_fu_8122_p1 = p_31_reg_1967.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_61_fu_3433_p3() {
    tmp_61_fu_3433_p3 = esl_concat<10,2>(tmp_59_fu_3416_p2.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_620_fu_8138_p2() {
    tmp_620_fu_8138_p2 = (!ap_const_lv5_1.is_01())? sc_lv<5>(): p_31_reg_1967.read() << (unsigned short)ap_const_lv5_1.to_uint();
}

void mnist_fp16::thread_tmp_621_fu_7720_p2() {
    tmp_621_fu_7720_p2 = (!tmp_184_cast_fu_7716_p1.read().is_01() || !tmp_601_reg_16669.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_184_cast_fu_7716_p1.read()) + sc_biguint<12>(tmp_601_reg_16669.read()));
}

void mnist_fp16::thread_tmp_622_fu_7743_p1() {
    tmp_622_fu_7743_p1 = conv3_0_load_to_int_fu_7730_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_623_fu_7780_p1() {
    tmp_623_fu_7780_p1 = ireg_V_4_fu_7772_p3.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_625_fu_7802_p1() {
    tmp_625_fu_7802_p1 = ireg_V_4_fu_7772_p3.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_626_fu_7877_p1() {
    tmp_626_fu_7877_p1 = man_V_7_fu_7832_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_627_fu_7881_p4() {
    tmp_627_fu_7881_p4 = sh_amt_4_fu_7863_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_628_cast_fu_4494_p1() {
    tmp_628_cast_fu_4494_p1 = esl_sext<9,8>(tmp_312_fu_4488_p2.read());
}

void mnist_fp16::thread_tmp_628_fu_7949_p1() {
    tmp_628_fu_7949_p1 = tmp_248_fu_7944_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_629_fu_7969_p1() {
    tmp_629_fu_7969_p1 = tmp_251_fu_7963_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_62_cast_fu_5934_p1() {
    tmp_62_cast_fu_5934_p1 = esl_zext<10,5>(p_23_reg_1647.read());
}

void mnist_fp16::thread_tmp_62_fu_3445_p2() {
    tmp_62_fu_3445_p2 = (!p_shl17_cast_fu_3425_p3.read().is_01() || !p_shl18_cast_fu_3441_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl17_cast_fu_3425_p3.read()) - sc_bigint<13>(p_shl18_cast_fu_3441_p1.read()));
}

void mnist_fp16::thread_tmp_630_fu_7494_p2() {
    tmp_630_fu_7494_p2 = (!tmp_510_reg_16576.read().is_01() || !tmp_119_cast_fu_7490_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_510_reg_16576.read()) + sc_biguint<12>(tmp_119_cast_fu_7490_p1.read()));
}

void mnist_fp16::thread_tmp_631_fu_7463_p2() {
    tmp_631_fu_7463_p2 = (!tmp_120_cast_fu_7459_p1.read().is_01() || !tmp_650_cast_reg_16558.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_120_cast_fu_7459_p1.read()) + sc_biguint<7>(tmp_650_cast_reg_16558.read()));
}

void mnist_fp16::thread_tmp_632_fu_7472_p3() {
    tmp_632_fu_7472_p3 = esl_concat<7,2>(tmp_631_fu_7463_p2.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_633_cast_fu_4541_p1() {
    tmp_633_cast_fu_4541_p1 = esl_zext<64,13>(tmp_324_fu_4535_p2.read());
}

void mnist_fp16::thread_tmp_633_fu_7484_p2() {
    tmp_633_fu_7484_p2 = (!p_shl5_fu_7480_p1.read().is_01() || !tmp_719_cast_fu_7468_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl5_fu_7480_p1.read()) - sc_biguint<64>(tmp_719_cast_fu_7468_p1.read()));
}

void mnist_fp16::thread_tmp_634_fu_9757_p3() {
    tmp_634_fu_9757_p3 = esl_concat<4,4>(p_50_reg_2110.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_635_cast_fu_6909_p1() {
    tmp_635_cast_fu_6909_p1 = esl_zext<8,7>(tmp_325_fu_6901_p3.read());
}

void mnist_fp16::thread_tmp_635_fu_9769_p3() {
    tmp_635_fu_9769_p3 = esl_concat<4,1>(p_50_reg_2110.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_636_fu_9781_p2() {
    tmp_636_fu_9781_p2 = (!p_shl71_cast_fu_9765_p1.read().is_01() || !p_shl72_cast_fu_9777_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl71_cast_fu_9765_p1.read()) - sc_biguint<9>(p_shl72_cast_fu_9777_p1.read()));
}

void mnist_fp16::thread_tmp_637_fu_8767_p2() {
    tmp_637_fu_8767_p2 = (!tmp_711_cast_reg_16964.read().is_01() || !tmp_111_cast_fu_8763_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_711_cast_reg_16964.read()) + sc_biguint<10>(tmp_111_cast_fu_8763_p1.read()));
}

void mnist_fp16::thread_tmp_638_fu_8772_p1() {
    tmp_638_fu_8772_p1 = tmp_637_fu_8767_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_639_fu_8784_p3() {
    tmp_639_fu_8784_p3 = esl_concat<10,1>(tmp_637_fu_8767_p2.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_63_fu_5660_p2() {
    tmp_63_fu_5660_p2 = (!p_Val2_3_reg_1567.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(p_Val2_3_reg_1567.read() == ap_const_lv16_0);
}

void mnist_fp16::thread_tmp_640_cast_fu_5994_p1() {
    tmp_640_cast_fu_5994_p1 = esl_zext<64,13>(tmp_359_fu_5989_p2.read());
}

void mnist_fp16::thread_tmp_640_fu_8796_p2() {
    tmp_640_fu_8796_p2 = (!p_shl73_cast_fu_8776_p3.read().is_01() || !p_shl74_cast_fu_8792_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl73_cast_fu_8776_p3.read()) - sc_bigint<12>(p_shl74_cast_fu_8792_p1.read()));
}

void mnist_fp16::thread_tmp_641_cast_fu_5872_p1() {
    tmp_641_cast_fu_5872_p1 = esl_zext<64,13>(tmp_382_reg_16137.read());
}

void mnist_fp16::thread_tmp_641_fu_7530_p2() {
    tmp_641_fu_7530_p2 = (!tmp_633_reg_16597.read().is_01() || !tmp_187_fu_7526_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_633_reg_16597.read()) + sc_biguint<64>(tmp_187_fu_7526_p1.read()));
}

void mnist_fp16::thread_tmp_642_cast_fu_4893_p1() {
    tmp_642_cast_fu_4893_p1 = esl_zext<64,6>(tmp_410_fu_4888_p2.read());
}

void mnist_fp16::thread_tmp_642_fu_7535_p1() {
    tmp_642_fu_7535_p1 = tmp_641_fu_7530_p2.read().range(10-1, 0);
}

void mnist_fp16::thread_tmp_643_fu_7539_p1() {
    tmp_643_fu_7539_p1 = tmp_641_fu_7530_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_644_fu_7551_p2() {
    tmp_644_fu_7551_p2 = (!p_shl75_cast_fu_7543_p3.read().is_01() || !tmp_642_fu_7535_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl75_cast_fu_7543_p3.read()) - sc_biguint<10>(tmp_642_fu_7535_p1.read()));
}

void mnist_fp16::thread_tmp_645_fu_8204_p1() {
    tmp_645_fu_8204_p1 = p_39_reg_1978.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_646_fu_8214_p2() {
    tmp_646_fu_8214_p2 = (!r_V_15_fu_8208_p2.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): (sc_biguint<4>(r_V_15_fu_8208_p2.read()) < sc_biguint<4>(ap_const_lv4_E));
}

void mnist_fp16::thread_tmp_647_cast_fu_4945_p1() {
    tmp_647_cast_fu_4945_p1 = esl_sext<10,9>(tmp_431_fu_4939_p2.read());
}

void mnist_fp16::thread_tmp_647_fu_8220_p2() {
    tmp_647_fu_8220_p2 = (!ap_const_lv4_1.is_01() || !tmp_645_fu_8204_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_645_fu_8204_p1.read()));
}

void mnist_fp16::thread_tmp_648_cast_fu_4754_p1() {
    tmp_648_cast_fu_4754_p1 = esl_zext<64,13>(tmp_438_fu_4749_p2.read());
}

void mnist_fp16::thread_tmp_648_fu_8266_p1() {
    tmp_648_fu_8266_p1 = mul5_fu_15278_p2.read().range(24-1, 0);
}

void mnist_fp16::thread_tmp_64_cast_fu_4884_p1() {
    tmp_64_cast_fu_4884_p1 = esl_zext<6,3>(p_24_reg_1579.read());
}

void mnist_fp16::thread_tmp_64_fu_3177_p2() {
    tmp_64_fu_3177_p2 = (!r_V_fu_3171_p2.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): (sc_biguint<5>(r_V_fu_3171_p2.read()) < sc_biguint<5>(ap_const_lv5_1C));
}

void mnist_fp16::thread_tmp_650_cast_fu_7346_p1() {
    tmp_650_cast_fu_7346_p1 = esl_zext<7,6>(tmp_439_fu_7338_p3.read());
}

void mnist_fp16::thread_tmp_650_fu_8303_p4() {
    tmp_650_fu_8303_p4 = neg_mul5_fu_8298_p2.read().range(23, 16);
}

void mnist_fp16::thread_tmp_651_fu_8313_p1() {
    tmp_651_fu_8313_p1 = esl_sext<12,8>(tmp_650_fu_8303_p4.read());
}

void mnist_fp16::thread_tmp_653_cast_fu_7380_p1() {
    tmp_653_cast_fu_7380_p1 = esl_sext<10,9>(tmp_448_fu_7374_p2.read());
}

void mnist_fp16::thread_tmp_653_fu_8317_p1() {
    tmp_653_fu_8317_p1 = esl_sext<12,10>(tmp_652_reg_16850.read());
}

void mnist_fp16::thread_tmp_654_fu_8320_p3() {
    tmp_654_fu_8320_p3 = (!tmp_649_reg_16842.read()[0].is_01())? sc_lv<12>(): ((tmp_649_reg_16842.read()[0].to_bool())? tmp_651_fu_8313_p1.read(): tmp_653_fu_8317_p1.read());
}

void mnist_fp16::thread_tmp_655_fu_8396_p1() {
    tmp_655_fu_8396_p1 = grp_fu_8340_p2.read().range(9-1, 0);
}

void mnist_fp16::thread_tmp_656_cast_fu_6946_p3() {
    tmp_656_cast_fu_6946_p3 = esl_concat<8,4>(tmp_450_fu_6941_p2.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_656_fu_8286_p1() {
    tmp_656_fu_8286_p1 = mul6_fu_15286_p2.read().range(25-1, 0);
}

void mnist_fp16::thread_tmp_657_fu_8351_p4() {
    tmp_657_fu_8351_p4 = neg_mul6_fu_8346_p2.read().range(24, 20);
}

void mnist_fp16::thread_tmp_658_fu_8361_p1() {
    tmp_658_fu_8361_p1 = esl_sext<13,5>(tmp_657_fu_8351_p4.read());
}

void mnist_fp16::thread_tmp_659_cast_fu_6476_p1() {
    tmp_659_cast_fu_6476_p1 = esl_zext<64,11>(tmp_460_fu_6471_p2.read());
}

void mnist_fp16::thread_tmp_65_fu_3183_p2() {
    tmp_65_fu_3183_p2 = (!ap_const_lv5_3.is_01() || !p_3_reg_1326.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_3) + sc_biguint<5>(p_3_reg_1326.read()));
}

void mnist_fp16::thread_tmp_660_fu_8365_p1() {
    tmp_660_fu_8365_p1 = esl_sext<13,6>(tmp_659_reg_16860.read());
}

void mnist_fp16::thread_tmp_661_fu_8375_p1() {
    tmp_661_fu_8375_p1 = p_v2_fu_8368_p3.read().range(3-1, 0);
}

void mnist_fp16::thread_tmp_662_fu_8385_p1() {
    tmp_662_fu_8385_p1 = p_v2_fu_8368_p3.read().range(3-1, 0);
}

void mnist_fp16::thread_tmp_663_fu_8400_p3() {
    tmp_663_fu_8400_p3 = esl_concat<3,4>(r_V_16_reg_16870.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_664_fu_8411_p3() {
    tmp_664_fu_8411_p3 = esl_concat<3,1>(r_V_16_reg_16870.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_665_fu_8422_p2() {
    tmp_665_fu_8422_p2 = (!p_shl76_cast_fu_8407_p1.read().is_01() || !p_shl77_cast_fu_8418_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl76_cast_fu_8407_p1.read()) - sc_biguint<8>(p_shl77_cast_fu_8418_p1.read()));
}

void mnist_fp16::thread_tmp_666_fu_8432_p2() {
    tmp_666_fu_8432_p2 = (!tmp_655_fu_8396_p1.read().is_01() || !tmp_745_cast_fu_8428_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_655_fu_8396_p1.read()) + sc_bigint<9>(tmp_745_cast_fu_8428_p1.read()));
}

void mnist_fp16::thread_tmp_667_fu_8438_p1() {
    tmp_667_fu_8438_p1 = tmp_666_fu_8432_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_668_fu_8452_p3() {
    tmp_668_fu_8452_p3 = esl_concat<9,1>(tmp_666_reg_16876.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_669_cast_fu_7649_p1() {
    tmp_669_cast_fu_7649_p1 = esl_sext<10,9>(tmp_501_fu_7643_p2.read());
}

void mnist_fp16::thread_tmp_669_fu_8463_p2() {
    tmp_669_fu_8463_p2 = (!p_shl78_cast_fu_8445_p3.read().is_01() || !p_shl79_cast_fu_8459_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl78_cast_fu_8445_p3.read()) - sc_bigint<12>(p_shl79_cast_fu_8459_p1.read()));
}

void mnist_fp16::thread_tmp_66_cast_fu_7396_p1() {
    tmp_66_cast_fu_7396_p1 = esl_zext<10,4>(p_25_reg_1817.read());
}

void mnist_fp16::thread_tmp_66_fu_3231_p1() {
    tmp_66_fu_3231_p1 = mul_fu_15229_p2.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_670_fu_8469_p2() {
    tmp_670_fu_8469_p2 = (!tmp_148_cast_fu_8442_p1.read().is_01() || !tmp_669_fu_8463_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_148_cast_fu_8442_p1.read()) + sc_biguint<12>(tmp_669_fu_8463_p2.read()));
}

void mnist_fp16::thread_tmp_671_fu_10201_p3() {
    tmp_671_fu_10201_p3 = esl_concat<4,4>(p_54_reg_2143.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_672_fu_10213_p3() {
    tmp_672_fu_10213_p3 = esl_concat<4,1>(p_54_reg_2143.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_673_fu_10225_p2() {
    tmp_673_fu_10225_p2 = (!p_shl80_cast_fu_10209_p1.read().is_01() || !p_shl81_cast_fu_10221_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl80_cast_fu_10209_p1.read()) - sc_biguint<9>(p_shl81_cast_fu_10221_p1.read()));
}

void mnist_fp16::thread_tmp_674_fu_10235_p3() {
    tmp_674_fu_10235_p3 = esl_concat<4,3>(p_54_reg_2143.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_675_fu_10247_p2() {
    tmp_675_fu_10247_p2 = (!p_shl82_cast_fu_10243_p1.read().is_01() || !tmp_238_cast_fu_10197_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl82_cast_fu_10243_p1.read()) - sc_biguint<8>(tmp_238_cast_fu_10197_p1.read()));
}

void mnist_fp16::thread_tmp_676_fu_9807_p2() {
    tmp_676_fu_9807_p2 = (!tmp_724_cast_reg_17240.read().is_01() || !tmp_239_cast_fu_9803_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_724_cast_reg_17240.read()) + sc_biguint<10>(tmp_239_cast_fu_9803_p1.read()));
}

void mnist_fp16::thread_tmp_677_fu_9812_p1() {
    tmp_677_fu_9812_p1 = tmp_676_fu_9807_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_678_cast_fu_5078_p1() {
    tmp_678_cast_fu_5078_p1 = esl_zext<64,13>(tmp_526_fu_5073_p2.read());
}

void mnist_fp16::thread_tmp_678_fu_9824_p3() {
    tmp_678_fu_9824_p3 = esl_concat<10,1>(tmp_676_fu_9807_p2.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_679_cast_fu_5092_p1() {
    tmp_679_cast_fu_5092_p1 = esl_zext<64,9>(tmp_533_fu_5087_p2.read());
}

void mnist_fp16::thread_tmp_679_fu_9836_p2() {
    tmp_679_fu_9836_p2 = (!p_shl83_cast_fu_9816_p3.read().is_01() || !p_shl84_cast_fu_9832_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl83_cast_fu_9816_p3.read()) - sc_bigint<12>(p_shl84_cast_fu_9832_p1.read()));
}

void mnist_fp16::thread_tmp_67_fu_4644_p2() {
    tmp_67_fu_4644_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_1_cast_fu_4621_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_1_cast_fu_4621_p1.read()));
}

void mnist_fp16::thread_tmp_680_fu_7579_p4() {
    tmp_680_fu_7579_p4 = esl_concat<7,4>(esl_concat<3,4>(p_40_reg_1841.read(), r_V_14_reg_16610.read()), r_V_17_fu_7573_p2.read());
}

void mnist_fp16::thread_tmp_681_fu_8537_p1() {
    tmp_681_fu_8537_p1 = msb_idx_6_fu_8531_p2.read().range(31-1, 0);
}

void mnist_fp16::thread_tmp_683_fu_8559_p4() {
    tmp_683_fu_8559_p4 = msb_idx_7_fu_8549_p3.read().range(30, 5);
}

void mnist_fp16::thread_tmp_684_fu_8590_p1() {
    tmp_684_fu_8590_p1 = msb_idx_7_fu_8549_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_685_fu_8594_p2() {
    tmp_685_fu_8594_p2 = (!ap_const_lv4_1.is_01() || !tmp_684_fu_8590_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_684_fu_8590_p1.read()));
}

void mnist_fp16::thread_tmp_686_fu_8600_p1() {
    tmp_686_fu_8600_p1 = esl_zext<16,4>(tmp_685_fu_8594_p2.read());
}

void mnist_fp16::thread_tmp_687_fu_7588_p1() {
    tmp_687_fu_7588_p1 = esl_zext<64,11>(tmp_680_fu_7579_p4.read());
}

void mnist_fp16::thread_tmp_688_fu_8641_p1() {
    tmp_688_fu_8641_p1 = msb_idx_6_reg_16916.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_689_fu_7597_p2() {
    tmp_689_fu_7597_p2 = (!tmp_644_reg_16615.read().is_01() || !tmp_214_cast_fu_7593_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_644_reg_16615.read()) + sc_biguint<10>(tmp_214_cast_fu_7593_p1.read()));
}

void mnist_fp16::thread_tmp_690_fu_8683_p2() {
    tmp_690_fu_8683_p2 = (!tmp_714_cast_reg_16801.read().is_01() || !tmp_260_cast1_fu_8679_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_714_cast_reg_16801.read()) + sc_biguint<13>(tmp_260_cast1_fu_8679_p1.read()));
}

void mnist_fp16::thread_tmp_691_fu_10758_p3() {
    tmp_691_fu_10758_p3 = esl_concat<4,3>(p_37_reg_2223.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_692_fu_10770_p2() {
    tmp_692_fu_10770_p2 = (!lhs_V_6_cast_fu_10754_p1.read().is_01() || !p_shl85_cast_fu_10766_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(lhs_V_6_cast_fu_10754_p1.read()) + sc_biguint<8>(p_shl85_cast_fu_10766_p1.read()));
}

void mnist_fp16::thread_tmp_693_cast_fu_7260_p1() {
    tmp_693_cast_fu_7260_p1 = esl_sext<8,7>(tmp_590_fu_7254_p2.read());
}

void mnist_fp16::thread_tmp_693_fu_10273_p2() {
    tmp_693_fu_10273_p2 = (!tmp_755_cast_reg_17367.read().is_01() || !lhs_V_9_cast_fu_10269_p1.read().is_01())? sc_lv<9>(): (sc_bigint<9>(tmp_755_cast_reg_17367.read()) + sc_biguint<9>(lhs_V_9_cast_fu_10269_p1.read()));
}

void mnist_fp16::thread_tmp_694_fu_10282_p1() {
    tmp_694_fu_10282_p1 = tmp_693_fu_10273_p2.read().range(7-1, 0);
}

void mnist_fp16::thread_tmp_695_fu_10294_p2() {
    tmp_695_fu_10294_p2 = (!p_shl86_cast_fu_10286_p3.read().is_01() || !tmp_766_cast_fu_10278_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl86_cast_fu_10286_p3.read()) - sc_bigint<10>(tmp_766_cast_fu_10278_p1.read()));
}

void mnist_fp16::thread_tmp_696_fu_9858_p2() {
    tmp_696_fu_9858_p2 = (!tmp_255_cast_fu_9854_p1.read().is_01() || !tmp_679_reg_17253.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_255_cast_fu_9854_p1.read()) + sc_biguint<12>(tmp_679_reg_17253.read()));
}

void mnist_fp16::thread_tmp_697_fu_9881_p1() {
    tmp_697_fu_9881_p1 = conv4_0_load_to_int_fu_9868_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_698_cast_fu_7307_p1() {
    tmp_698_cast_fu_7307_p1 = esl_zext<64,11>(tmp_595_fu_7301_p2.read());
}

void mnist_fp16::thread_tmp_698_fu_9918_p1() {
    tmp_698_fu_9918_p1 = ireg_V_7_fu_9910_p3.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_699_cast_fu_7321_p1() {
    tmp_699_cast_fu_7321_p1 = esl_zext<64,12>(tmp_596_fu_7316_p2.read());
}

void mnist_fp16::thread_tmp_69_cast_fu_3396_p1() {
    tmp_69_cast_fu_3396_p1 = esl_sext<10,9>(tmp_18_fu_3390_p2.read());
}

void mnist_fp16::thread_tmp_69_fu_3248_p4() {
    tmp_69_fu_3248_p4 = neg_mul_fu_3243_p2.read().range(22, 16);
}

void mnist_fp16::thread_tmp_6_cast_fu_3007_p1() {
    tmp_6_cast_fu_3007_p1 = esl_zext<11,5>(p_1_reg_1208.read());
}

void mnist_fp16::thread_tmp_6_fu_2970_p2() {
    tmp_6_fu_2970_p2 = (!p_shl8_cast_fu_2954_p1.read().is_01() || !p_shl9_cast_fu_2966_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl8_cast_fu_2954_p1.read()) - sc_biguint<11>(p_shl9_cast_fu_2966_p1.read()));
}

void mnist_fp16::thread_tmp_700_fu_9940_p1() {
    tmp_700_fu_9940_p1 = ireg_V_7_fu_9910_p3.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_701_cast_fu_8077_p1() {
    tmp_701_cast_fu_8077_p1 = esl_zext<9,8>(tmp_597_fu_8069_p3.read());
}

void mnist_fp16::thread_tmp_701_fu_10015_p1() {
    tmp_701_fu_10015_p1 = man_V_16_fu_9970_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_702_fu_10019_p4() {
    tmp_702_fu_10019_p4 = sh_amt_5_fu_10001_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_703_fu_10087_p1() {
    tmp_703_fu_10087_p1 = tmp_303_fu_10082_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_704_fu_10107_p1() {
    tmp_704_fu_10107_p1 = tmp_307_fu_10101_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_705_fu_9670_p2() {
    tmp_705_fu_9670_p2 = (!tmp_640_reg_16977.read().is_01() || !tmp_292_cast_fu_9666_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_640_reg_16977.read()) + sc_biguint<12>(tmp_292_cast_fu_9666_p1.read()));
}

void mnist_fp16::thread_tmp_706_cast_fu_6579_p1() {
    tmp_706_cast_fu_6579_p1 = esl_zext<64,13>(tmp_602_fu_6574_p2.read());
}

void mnist_fp16::thread_tmp_706_fu_9582_p1() {
    tmp_706_fu_9582_p1 = msb_idx_8_fu_9576_p2.read().range(31-1, 0);
}

void mnist_fp16::thread_tmp_708_cast_fu_8713_p1() {
    tmp_708_cast_fu_8713_p1 = esl_zext<8,7>(tmp_614_fu_8705_p3.read());
}

void mnist_fp16::thread_tmp_708_fu_9604_p4() {
    tmp_708_fu_9604_p4 = msb_idx_9_fu_9594_p3.read().range(30, 5);
}

void mnist_fp16::thread_tmp_709_fu_9635_p1() {
    tmp_709_fu_9635_p1 = msb_idx_9_fu_9594_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_70_fu_7013_p2() {
    tmp_70_fu_7013_p2 = (!p_30_reg_1782.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_30_reg_1782.read() != ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_710_fu_9639_p2() {
    tmp_710_fu_9639_p2 = (!ap_const_lv4_1.is_01() || !tmp_709_fu_9635_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_709_fu_9635_p1.read()));
}

void mnist_fp16::thread_tmp_711_cast_fu_8747_p1() {
    tmp_711_cast_fu_8747_p1 = esl_sext<10,9>(tmp_617_fu_8741_p2.read());
}

void mnist_fp16::thread_tmp_711_fu_9645_p1() {
    tmp_711_fu_9645_p1 = esl_zext<16,4>(tmp_710_fu_9639_p2.read());
}

void mnist_fp16::thread_tmp_712_fu_8830_p2() {
    tmp_712_fu_8830_p2 = (!tmp_194_cast_fu_8826_p1.read().is_01() || !tmp_708_cast_reg_16959.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_194_cast_fu_8826_p1.read()) + sc_biguint<8>(tmp_708_cast_reg_16959.read()));
}

void mnist_fp16::thread_tmp_713_fu_9695_p1() {
    tmp_713_fu_9695_p1 = msb_idx_8_reg_17197.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_714_cast_fu_8114_p3() {
    tmp_714_cast_fu_8114_p3 = esl_concat<9,4>(tmp_618_fu_8109_p2.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_714_fu_8839_p3() {
    tmp_714_fu_8839_p3 = esl_concat<8,2>(tmp_712_fu_8830_p2.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_715_fu_8851_p2() {
    tmp_715_fu_8851_p2 = (!p_shl7_fu_8847_p1.read().is_01() || !tmp_771_cast_fu_8835_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl7_fu_8847_p1.read()) - sc_biguint<64>(tmp_771_cast_fu_8835_p1.read()));
}

void mnist_fp16::thread_tmp_716_fu_11155_p3() {
    tmp_716_fu_11155_p3 = esl_concat<5,3>(p_42_reg_2280.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_717_cast_fu_7725_p1() {
    tmp_717_cast_fu_7725_p1 = esl_zext<64,12>(tmp_621_fu_7720_p2.read());
}

void mnist_fp16::thread_tmp_717_fu_11167_p2() {
    tmp_717_fu_11167_p2 = (!tmp_775_cast_fu_11163_p1.read().is_01() || !tmp_138_cast_fu_11151_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_775_cast_fu_11163_p1.read()) - sc_biguint<9>(tmp_138_cast_fu_11151_p1.read()));
}

void mnist_fp16::thread_tmp_718_cast_fu_7499_p1() {
    tmp_718_cast_fu_7499_p1 = esl_zext<64,12>(tmp_630_fu_7494_p2.read());
}

void mnist_fp16::thread_tmp_718_fu_10792_p2() {
    tmp_718_fu_10792_p2 = (!lhs_V_10_cast1_fu_10788_p1.read().is_01() || !tmp_692_reg_17522.read().is_01())? sc_lv<8>(): (sc_biguint<8>(lhs_V_10_cast1_fu_10788_p1.read()) + sc_biguint<8>(tmp_692_reg_17522.read()));
}

void mnist_fp16::thread_tmp_719_cast_fu_7468_p1() {
    tmp_719_cast_fu_7468_p1 = esl_zext<64,7>(tmp_631_fu_7463_p2.read());
}

void mnist_fp16::thread_tmp_719_fu_10809_p2() {
    tmp_719_fu_10809_p2 = (!p_shl89_cast_fu_10801_p3.read().is_01() || !tmp_778_cast_fu_10797_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl89_cast_fu_10801_p3.read()) + sc_biguint<11>(tmp_778_cast_fu_10797_p1.read()));
}

void mnist_fp16::thread_tmp_71_fu_7019_p2() {
    tmp_71_fu_7019_p2 = (!p_30_reg_1782.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_30_reg_1782.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp16::thread_tmp_720_fu_10324_p2() {
    tmp_720_fu_10324_p2 = (!lhs_V_11_cast_fu_10320_p1.read().is_01() || !tmp_695_reg_17380.read().is_01())? sc_lv<10>(): (sc_biguint<10>(lhs_V_11_cast_fu_10320_p1.read()) + sc_biguint<10>(tmp_695_reg_17380.read()));
}

void mnist_fp16::thread_tmp_721_fu_8889_p2() {
    tmp_721_fu_8889_p2 = (!tmp_715_reg_16998.read().is_01() || !tmp_259_fu_8885_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_715_reg_16998.read()) + sc_biguint<64>(tmp_259_fu_8885_p1.read()));
}

void mnist_fp16::thread_tmp_722_fu_8894_p1() {
    tmp_722_fu_8894_p1 = tmp_721_fu_8889_p2.read().range(11-1, 0);
}

void mnist_fp16::thread_tmp_723_fu_8898_p1() {
    tmp_723_fu_8898_p1 = tmp_721_fu_8889_p2.read().range(9-1, 0);
}

void mnist_fp16::thread_tmp_724_cast_fu_9787_p1() {
    tmp_724_cast_fu_9787_p1 = esl_sext<10,9>(tmp_636_fu_9781_p2.read());
}

void mnist_fp16::thread_tmp_724_fu_8910_p2() {
    tmp_724_fu_8910_p2 = (!p_shl90_cast_fu_8902_p3.read().is_01() || !tmp_722_fu_8894_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl90_cast_fu_8902_p3.read()) - sc_biguint<11>(tmp_722_fu_8894_p1.read()));
}

void mnist_fp16::thread_tmp_725_fu_11463_p3() {
    tmp_725_fu_11463_p3 = esl_concat<5,3>(p_68_reg_2384.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_726_fu_11475_p2() {
    tmp_726_fu_11475_p2 = (!p_shl91_cast_fu_11471_p1.read().is_01() || !tmp_312_cast_fu_11459_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl91_cast_fu_11471_p1.read()) - sc_biguint<9>(tmp_312_cast_fu_11459_p1.read()));
}

void mnist_fp16::thread_tmp_727_fu_11197_p2() {
    tmp_727_fu_11197_p2 = (!tmp_192_cast_fu_11193_p1.read().is_01() || !tmp_777_cast_reg_17643.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_192_cast_fu_11193_p1.read()) + sc_bigint<10>(tmp_777_cast_reg_17643.read()));
}

void mnist_fp16::thread_tmp_728_fu_11206_p1() {
    tmp_728_fu_11206_p1 = tmp_727_fu_11197_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_729_fu_11218_p2() {
    tmp_729_fu_11218_p2 = (!p_shl92_cast_fu_11210_p3.read().is_01() || !tmp_787_cast_fu_11202_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl92_cast_fu_11210_p3.read()) - sc_bigint<11>(tmp_787_cast_fu_11202_p1.read()));
}

void mnist_fp16::thread_tmp_72_fu_6084_p3() {
    tmp_72_fu_6084_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_363_reg_16229.read());
}

void mnist_fp16::thread_tmp_730_fu_10367_p2() {
    tmp_730_fu_10367_p2 = (!tmp_753_cast_reg_17362.read().is_01() || !tmp_314_cast_cast_fu_10363_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_753_cast_reg_17362.read()) + sc_biguint<10>(tmp_314_cast_cast_fu_10363_p1.read()));
}

void mnist_fp16::thread_tmp_731_fu_10372_p1() {
    tmp_731_fu_10372_p1 = tmp_730_fu_10367_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_732_fu_10384_p3() {
    tmp_732_fu_10384_p3 = esl_concat<10,1>(tmp_730_fu_10367_p2.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_733_fu_10396_p2() {
    tmp_733_fu_10396_p2 = (!p_shl93_cast_fu_10376_p3.read().is_01() || !p_shl94_cast_fu_10392_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl93_cast_fu_10376_p3.read()) - sc_bigint<12>(p_shl94_cast_fu_10392_p1.read()));
}

void mnist_fp16::thread_tmp_734_fu_8938_p4() {
    tmp_734_fu_8938_p4 = esl_concat<8,4>(esl_concat<4,4>(p_51_reg_2052.read(), r_V_20_reg_17016.read()), r_V_21_fu_8932_p2.read());
}

void mnist_fp16::thread_tmp_735_fu_8947_p1() {
    tmp_735_fu_8947_p1 = esl_zext<64,12>(tmp_734_fu_8938_p4.read());
}

void mnist_fp16::thread_tmp_736_fu_8970_p1() {
    tmp_736_fu_8970_p1 = ireg_V_14_fu_8966_p1.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_737_fu_8956_p2() {
    tmp_737_fu_8956_p2 = (!tmp_335_cast_fu_8952_p1.read().is_01() || !tmp_724_reg_17021.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_335_cast_fu_8952_p1.read()) + sc_biguint<11>(tmp_724_reg_17021.read()));
}

void mnist_fp16::thread_tmp_738_fu_8992_p1() {
    tmp_738_fu_8992_p1 = ireg_V_14_fu_8966_p1.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_739_fu_9103_p1() {
    tmp_739_fu_9103_p1 = man_V_12_fu_9058_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_73_fu_4949_p2() {
    tmp_73_fu_4949_p2 = (!ap_const_lv16_0.is_01() || !p_Val2_3_reg_1567.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(p_Val2_3_reg_1567.read()));
}

void mnist_fp16::thread_tmp_740_fu_9107_p4() {
    tmp_740_fu_9107_p4 = sh_amt_6_fu_9089_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_741_fu_9225_p1() {
    tmp_741_fu_9225_p1 = tmp_323_fu_9220_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_742_fu_9245_p1() {
    tmp_742_fu_9245_p1 = tmp_331_fu_9239_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_743_fu_9006_p1() {
    tmp_743_fu_9006_p1 = ireg_V_15_fu_9002_p1.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_744_fu_10881_p2() {
    tmp_744_fu_10881_p2 = (!tmp_750_fu_10871_p1.read().is_01() || !ap_const_lv3_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_750_fu_10871_p1.read() != ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_745_cast_fu_8428_p1() {
    tmp_745_cast_fu_8428_p1 = esl_sext<9,8>(tmp_665_fu_8422_p2.read());
}

void mnist_fp16::thread_tmp_745_fu_9028_p1() {
    tmp_745_fu_9028_p1 = ireg_V_15_fu_9002_p1.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_746_fu_9188_p1() {
    tmp_746_fu_9188_p1 = man_V_15_fu_9143_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_747_fu_9192_p4() {
    tmp_747_fu_9192_p4 = sh_amt_7_fu_9174_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_748_fu_9374_p1() {
    tmp_748_fu_9374_p1 = tmp_347_fu_9369_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_749_fu_9394_p1() {
    tmp_749_fu_9394_p1 = tmp_351_fu_9388_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_74_fu_3258_p1() {
    tmp_74_fu_3258_p1 = esl_sext<11,7>(tmp_69_fu_3248_p4.read());
}

void mnist_fp16::thread_tmp_750_cast_fu_8475_p1() {
    tmp_750_cast_fu_8475_p1 = esl_zext<64,12>(tmp_670_fu_8469_p2.read());
}

void mnist_fp16::thread_tmp_750_fu_10871_p1() {
    tmp_750_fu_10871_p1 = p_56_reg_2256.read().range(3-1, 0);
}

void mnist_fp16::thread_tmp_751_fu_10935_p1() {
    tmp_751_fu_10935_p1 = mul7_fu_15303_p2.read().range(21-1, 0);
}

void mnist_fp16::thread_tmp_753_cast_fu_10231_p1() {
    tmp_753_cast_fu_10231_p1 = esl_sext<10,9>(tmp_673_fu_10225_p2.read());
}

void mnist_fp16::thread_tmp_753_fu_10964_p4() {
    tmp_753_fu_10964_p4 = neg_mul7_fu_10959_p2.read().range(20, 13);
}

void mnist_fp16::thread_tmp_754_fu_10974_p1() {
    tmp_754_fu_10974_p1 = esl_sext<10,8>(tmp_753_fu_10964_p4.read());
}

void mnist_fp16::thread_tmp_755_cast_fu_10253_p1() {
    tmp_755_cast_fu_10253_p1 = esl_sext<9,8>(tmp_675_fu_10247_p2.read());
}

void mnist_fp16::thread_tmp_756_fu_10978_p1() {
    tmp_756_fu_10978_p1 = esl_sext<10,9>(tmp_755_reg_17584.read());
}

void mnist_fp16::thread_tmp_757_fu_10981_p3() {
    tmp_757_fu_10981_p3 = (!tmp_752_reg_17571.read()[0].is_01())? sc_lv<10>(): ((tmp_752_reg_17571.read()[0].to_bool())? tmp_754_fu_10974_p1.read(): tmp_756_fu_10978_p1.read());
}

void mnist_fp16::thread_tmp_758_fu_11057_p1() {
    tmp_758_fu_11057_p1 = grp_fu_11001_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_759_fu_10947_p1() {
    tmp_759_fu_10947_p1 = mul8_fu_15311_p2.read().range(21-1, 0);
}

void mnist_fp16::thread_tmp_760_fu_11012_p4() {
    tmp_760_fu_11012_p4 = neg_mul8_fu_11007_p2.read().range(20, 16);
}

void mnist_fp16::thread_tmp_761_fu_11022_p1() {
    tmp_761_fu_11022_p1 = esl_sext<11,5>(tmp_760_fu_11012_p4.read());
}

void mnist_fp16::thread_tmp_762_cast_fu_7602_p1() {
    tmp_762_cast_fu_7602_p1 = esl_zext<64,10>(tmp_689_fu_7597_p2.read());
}

void mnist_fp16::thread_tmp_763_cast_fu_8688_p1() {
    tmp_763_cast_fu_8688_p1 = esl_zext<64,13>(tmp_690_fu_8683_p2.read());
}

void mnist_fp16::thread_tmp_763_fu_11026_p1() {
    tmp_763_fu_11026_p1 = esl_sext<11,6>(tmp_762_reg_17594.read());
}

void mnist_fp16::thread_tmp_764_fu_11036_p1() {
    tmp_764_fu_11036_p1 = p_v3_fu_11029_p3.read().range(3-1, 0);
}

void mnist_fp16::thread_tmp_765_fu_11046_p1() {
    tmp_765_fu_11046_p1 = p_v3_fu_11029_p3.read().range(3-1, 0);
}

void mnist_fp16::thread_tmp_766_cast_fu_10278_p1() {
    tmp_766_cast_fu_10278_p1 = esl_sext<10,9>(tmp_693_fu_10273_p2.read());
}

void mnist_fp16::thread_tmp_766_fu_11064_p3() {
    tmp_766_fu_11064_p3 = esl_concat<3,3>(r_V_23_reg_17604.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_767_fu_11075_p2() {
    tmp_767_fu_11075_p2 = (!p_shl95_cast_fu_11071_p1.read().is_01() || !tmp_267_cast_fu_11061_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl95_cast_fu_11071_p1.read()) - sc_biguint<7>(tmp_267_cast_fu_11061_p1.read()));
}

void mnist_fp16::thread_tmp_768_fu_11085_p2() {
    tmp_768_fu_11085_p2 = (!tmp_808_cast_fu_11081_p1.read().is_01() || !tmp_758_fu_11057_p1.read().is_01())? sc_lv<8>(): (sc_bigint<8>(tmp_808_cast_fu_11081_p1.read()) + sc_biguint<8>(tmp_758_fu_11057_p1.read()));
}

void mnist_fp16::thread_tmp_769_cast_fu_9863_p1() {
    tmp_769_cast_fu_9863_p1 = esl_zext<64,12>(tmp_696_fu_9858_p2.read());
}

void mnist_fp16::thread_tmp_769_fu_11091_p1() {
    tmp_769_fu_11091_p1 = tmp_768_fu_11085_p2.read().range(7-1, 0);
}

void mnist_fp16::thread_tmp_76_fu_4701_p2() {
    tmp_76_fu_4701_p2 = (!p_Result_9_fu_4691_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_9_fu_4691_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16::thread_tmp_770_cast_fu_9741_p1() {
    tmp_770_cast_fu_9741_p1 = esl_zext<64,12>(tmp_705_reg_17217.read());
}

void mnist_fp16::thread_tmp_770_fu_11108_p2() {
    tmp_770_fu_11108_p2 = (!p_shl96_cast_fu_11101_p3.read().is_01() || !tmp_809_cast_fu_11098_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl96_cast_fu_11101_p3.read()) - sc_bigint<10>(tmp_809_cast_fu_11098_p1.read()));
}

void mnist_fp16::thread_tmp_771_cast_fu_8835_p1() {
    tmp_771_cast_fu_8835_p1 = esl_zext<64,8>(tmp_712_fu_8830_p2.read());
}

void mnist_fp16::thread_tmp_771_fu_11114_p2() {
    tmp_771_fu_11114_p2 = (!tmp_770_fu_11108_p2.read().is_01() || !tmp_254_cast_fu_11095_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_770_fu_11108_p2.read()) + sc_biguint<10>(tmp_254_cast_fu_11095_p1.read()));
}

void mnist_fp16::thread_tmp_772_fu_11129_p2() {
    tmp_772_fu_11129_p2 = (!tmp_719_reg_17535.read().is_01() || !tmp_269_cast_fu_11125_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_719_reg_17535.read()) + sc_biguint<11>(tmp_269_cast_fu_11125_p1.read()));
}

void mnist_fp16::thread_tmp_773_fu_11897_p3() {
    tmp_773_fu_11897_p3 = esl_concat<5,3>(p_48_reg_2417.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_774_fu_11909_p2() {
    tmp_774_fu_11909_p2 = (!lhs_V_13_cast_fu_11893_p1.read().is_01() || !p_shl97_cast_fu_11905_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_13_cast_fu_11893_p1.read()) + sc_biguint<9>(p_shl97_cast_fu_11905_p1.read()));
}

void mnist_fp16::thread_tmp_775_cast_fu_11163_p1() {
    tmp_775_cast_fu_11163_p1 = esl_zext<9,8>(tmp_716_fu_11155_p3.read());
}

void mnist_fp16::thread_tmp_775_fu_11501_p2() {
    tmp_775_fu_11501_p2 = (!tmp_328_cast_fu_11497_p1.read().is_01() || !tmp_786_cast_reg_17751.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_328_cast_fu_11497_p1.read()) + sc_bigint<10>(tmp_786_cast_reg_17751.read()));
}

void mnist_fp16::thread_tmp_776_fu_11510_p1() {
    tmp_776_fu_11510_p1 = tmp_775_fu_11501_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_777_cast_fu_11173_p1() {
    tmp_777_cast_fu_11173_p1 = esl_sext<10,9>(tmp_717_fu_11167_p2.read());
}

void mnist_fp16::thread_tmp_777_fu_11522_p2() {
    tmp_777_fu_11522_p2 = (!p_shl98_cast_fu_11514_p3.read().is_01() || !tmp_816_cast_fu_11506_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl98_cast_fu_11514_p3.read()) - sc_bigint<11>(tmp_816_cast_fu_11506_p1.read()));
}

void mnist_fp16::thread_tmp_778_cast_fu_10797_p1() {
    tmp_778_cast_fu_10797_p1 = esl_zext<11,8>(tmp_718_fu_10792_p2.read());
}

void mnist_fp16::thread_tmp_778_fu_10427_p2() {
    tmp_778_fu_10427_p2 = (!tmp_733_reg_17416.read().is_01() || !tmp_332_cast_cast_fu_10423_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_733_reg_17416.read()) + sc_biguint<12>(tmp_332_cast_cast_fu_10423_p1.read()));
}

void mnist_fp16::thread_tmp_77_cast_fu_5985_p1() {
    tmp_77_cast_fu_5985_p1 = esl_zext<13,5>(p_28_reg_1658.read());
}

void mnist_fp16::thread_tmp_77_fu_3262_p1() {
    tmp_77_fu_3262_p1 = esl_sext<11,8>(tmp_75_reg_15455.read());
}

void mnist_fp16::thread_tmp_780_fu_10493_p1() {
    tmp_780_fu_10493_p1 = msb_idx_s_fu_10487_p2.read().range(31-1, 0);
}

void mnist_fp16::thread_tmp_781_cast_fu_10329_p1() {
    tmp_781_cast_fu_10329_p1 = esl_zext<64,10>(tmp_720_fu_10324_p2.read());
}

void mnist_fp16::thread_tmp_782_fu_10515_p4() {
    tmp_782_fu_10515_p4 = msb_idx_10_fu_10505_p3.read().range(30, 5);
}

void mnist_fp16::thread_tmp_783_fu_10546_p1() {
    tmp_783_fu_10546_p1 = msb_idx_10_fu_10505_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_784_fu_10550_p2() {
    tmp_784_fu_10550_p2 = (!ap_const_lv4_1.is_01() || !tmp_783_fu_10546_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_783_fu_10546_p1.read()));
}

void mnist_fp16::thread_tmp_785_fu_10556_p1() {
    tmp_785_fu_10556_p1 = esl_zext<16,4>(tmp_784_fu_10550_p2.read());
}

void mnist_fp16::thread_tmp_786_cast_fu_11481_p1() {
    tmp_786_cast_fu_11481_p1 = esl_sext<10,9>(tmp_726_fu_11475_p2.read());
}

void mnist_fp16::thread_tmp_786_fu_10560_p2() {
    tmp_786_fu_10560_p2 = (!tmp_785_fu_10556_p1.read().is_01())? sc_lv<16>(): tmp_V_5_reg_17456.read() >> (unsigned short)tmp_785_fu_10556_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_787_cast_fu_11202_p1() {
    tmp_787_cast_fu_11202_p1 = esl_sext<11,10>(tmp_727_fu_11197_p2.read());
}

void mnist_fp16::thread_tmp_787_fu_10597_p1() {
    tmp_787_fu_10597_p1 = msb_idx_s_reg_17462.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_788_fu_10655_p1() {
    tmp_788_fu_10655_p1 = p_03_i3_to_int_fu_10642_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_789_fu_10673_p1() {
    tmp_789_fu_10673_p1 = tmp_318_to_int_fu_10659_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_78_fu_3933_p1() {
    tmp_78_fu_3933_p1 = esl_zext<54,32>(sh_amt_cast_fu_3925_p1.read());
}

void mnist_fp16::thread_tmp_790_fu_12497_p3() {
    tmp_790_fu_12497_p3 = esl_concat<5,4>(p_53_reg_2477.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_791_fu_12509_p3() {
    tmp_791_fu_12509_p3 = esl_concat<5,3>(p_53_reg_2477.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_792_fu_12521_p2() {
    tmp_792_fu_12521_p2 = (!p_shl99_cast_fu_12517_p1.read().is_01() || !tmp_206_cast_fu_12493_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl99_cast_fu_12517_p1.read()) - sc_biguint<9>(tmp_206_cast_fu_12493_p1.read()));
}

void mnist_fp16::thread_tmp_793_fu_11931_p2() {
    tmp_793_fu_11931_p2 = (!lhs_V_14_cast_fu_11927_p1.read().is_01() || !tmp_774_reg_17883.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_14_cast_fu_11927_p1.read()) + sc_biguint<9>(tmp_774_reg_17883.read()));
}

void mnist_fp16::thread_tmp_794_fu_11948_p2() {
    tmp_794_fu_11948_p2 = (!p_shl100_cast_fu_11940_p3.read().is_01() || !tmp_824_cast_fu_11936_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl100_cast_fu_11940_p3.read()) + sc_biguint<12>(tmp_824_cast_fu_11936_p1.read()));
}

void mnist_fp16::thread_tmp_795_fu_11544_p2() {
    tmp_795_fu_11544_p2 = (!tmp_362_cast_fu_11540_p1.read().is_01() || !tmp_777_reg_17764.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_362_cast_fu_11540_p1.read()) + sc_biguint<11>(tmp_777_reg_17764.read()));
}

void mnist_fp16::thread_tmp_796_cast_fu_8961_p1() {
    tmp_796_cast_fu_8961_p1 = esl_zext<64,11>(tmp_737_fu_8956_p2.read());
}

void mnist_fp16::thread_tmp_796_fu_11567_p1() {
    tmp_796_fu_11567_p1 = conv5_0_load_to_int_fu_11554_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_797_fu_11604_p1() {
    tmp_797_fu_11604_p1 = ireg_V_8_fu_11596_p3.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_799_fu_11626_p1() {
    tmp_799_fu_11626_p1 = ireg_V_8_fu_11596_p3.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_79_fu_3265_p3() {
    tmp_79_fu_3265_p3 = (!tmp_68_reg_15444.read()[0].is_01())? sc_lv<11>(): ((tmp_68_reg_15444.read()[0].to_bool())? tmp_74_fu_3258_p1.read(): tmp_77_fu_3262_p1.read());
}

void mnist_fp16::thread_tmp_7_cast_fu_3348_p1() {
    tmp_7_cast_fu_3348_p1 = esl_zext<6,3>(p_2_reg_1353.read());
}

void mnist_fp16::thread_tmp_7_fu_3027_p3() {
    tmp_7_fu_3027_p3 = esl_concat<5,5>(p_s_reg_1314.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_800_fu_11701_p1() {
    tmp_800_fu_11701_p1 = man_V_21_fu_11656_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_801_fu_11705_p4() {
    tmp_801_fu_11705_p4 = sh_amt_8_fu_11687_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_802_fu_11773_p1() {
    tmp_802_fu_11773_p1 = tmp_414_fu_11768_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_803_fu_11793_p1() {
    tmp_803_fu_11793_p1 = tmp_421_fu_11787_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_804_fu_11309_p2() {
    tmp_804_fu_11309_p2 = (!tmp_729_reg_17661.read().is_01() || !tmp_290_cast_fu_11305_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_729_reg_17661.read()) + sc_biguint<11>(tmp_290_cast_fu_11305_p1.read()));
}

void mnist_fp16::thread_tmp_805_fu_11260_p2() {
    tmp_805_fu_11260_p2 = (!tmp_775_cast_reg_17638.read().is_01() || !tmp_291_cast_fu_11256_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_775_cast_reg_17638.read()) + sc_biguint<9>(tmp_291_cast_fu_11256_p1.read()));
}

void mnist_fp16::thread_tmp_806_fu_11269_p3() {
    tmp_806_fu_11269_p3 = esl_concat<9,2>(tmp_805_fu_11260_p2.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_807_fu_11281_p2() {
    tmp_807_fu_11281_p2 = (!p_shl8_fu_11277_p1.read().is_01() || !tmp_829_cast_fu_11265_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl8_fu_11277_p1.read()) - sc_biguint<64>(tmp_829_cast_fu_11265_p1.read()));
}

void mnist_fp16::thread_tmp_808_cast_fu_11081_p1() {
    tmp_808_cast_fu_11081_p1 = esl_sext<8,7>(tmp_767_fu_11075_p2.read());
}

void mnist_fp16::thread_tmp_808_fu_11287_p3() {
    tmp_808_fu_11287_p3 = esl_concat<4,3>(p_64_reg_2314.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_809_cast_fu_11098_p1() {
    tmp_809_cast_fu_11098_p1 = esl_sext<10,8>(tmp_768_reg_17610.read());
}

void mnist_fp16::thread_tmp_809_fu_11299_p2() {
    tmp_809_fu_11299_p2 = (!tmp_291_cast1_fu_11252_p1.read().is_01() || !p_shl102_cast_fu_11295_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_291_cast1_fu_11252_p1.read()) + sc_biguint<8>(p_shl102_cast_fu_11295_p1.read()));
}

void mnist_fp16::thread_tmp_80_cast_fu_4977_p1() {
    tmp_80_cast_fu_4977_p1 = esl_zext<10,5>(r_V_9_fu_4971_p2.read());
}

void mnist_fp16::thread_tmp_80_fu_3294_p2() {
    tmp_80_fu_3294_p2 = (!ap_const_lv11_5.is_01())? sc_lv<11>(): grp_fu_3285_p2.read() << (unsigned short)ap_const_lv11_5.to_uint();
}

void mnist_fp16::thread_tmp_810_fu_13588_p3() {
    tmp_810_fu_13588_p3 = esl_concat<5,3>(p_82_reg_2580.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_811_fu_13600_p2() {
    tmp_811_fu_13600_p2 = (!p_shl103_cast_fu_13596_p1.read().is_01() || !tmp_385_cast_fu_13584_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl103_cast_fu_13596_p1.read()) - sc_biguint<9>(tmp_385_cast_fu_13584_p1.read()));
}

void mnist_fp16::thread_tmp_812_cast_fu_11120_p1() {
    tmp_812_cast_fu_11120_p1 = esl_zext<64,10>(tmp_771_fu_11114_p2.read());
}

void mnist_fp16::thread_tmp_812_fu_12551_p2() {
    tmp_812_fu_12551_p2 = (!tmp_274_cast_fu_12547_p1.read().is_01() || !tmp_823_cast_reg_18059.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_274_cast_fu_12547_p1.read()) + sc_bigint<10>(tmp_823_cast_reg_18059.read()));
}

void mnist_fp16::thread_tmp_813_cast_fu_11134_p1() {
    tmp_813_cast_fu_11134_p1 = esl_zext<64,11>(tmp_772_fu_11129_p2.read());
}

void mnist_fp16::thread_tmp_813_fu_12560_p1() {
    tmp_813_fu_12560_p1 = tmp_812_fu_12551_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_814_fu_12572_p2() {
    tmp_814_fu_12572_p2 = (!p_shl104_cast_fu_12564_p3.read().is_01() || !tmp_836_cast_fu_12556_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl104_cast_fu_12564_p3.read()) - sc_bigint<11>(tmp_836_cast_fu_12556_p1.read()));
}

void mnist_fp16::thread_tmp_815_fu_11344_p2() {
    tmp_815_fu_11344_p2 = (!tmp_361_cast_fu_11340_p1.read().is_01() || !tmp_809_reg_17692.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_361_cast_fu_11340_p1.read()) + sc_biguint<8>(tmp_809_reg_17692.read()));
}

void mnist_fp16::thread_tmp_816_cast_fu_11506_p1() {
    tmp_816_cast_fu_11506_p1 = esl_sext<11,10>(tmp_775_fu_11501_p2.read());
}

void mnist_fp16::thread_tmp_816_fu_11361_p2() {
    tmp_816_fu_11361_p2 = (!p_shl105_cast_fu_11353_p3.read().is_01() || !tmp_839_cast_fu_11349_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl105_cast_fu_11353_p3.read()) + sc_biguint<11>(tmp_839_cast_fu_11349_p1.read()));
}

void mnist_fp16::thread_tmp_817_fu_11371_p2() {
    tmp_817_fu_11371_p2 = (!tmp_364_fu_11367_p1.read().is_01() || !tmp_807_reg_17687.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_364_fu_11367_p1.read()) + sc_biguint<64>(tmp_807_reg_17687.read()));
}

void mnist_fp16::thread_tmp_818_fu_11376_p1() {
    tmp_818_fu_11376_p1 = tmp_817_fu_11371_p2.read().range(12-1, 0);
}

void mnist_fp16::thread_tmp_819_cast_fu_10432_p1() {
    tmp_819_cast_fu_10432_p1 = esl_zext<64,12>(tmp_778_fu_10427_p2.read());
}

void mnist_fp16::thread_tmp_819_fu_11380_p1() {
    tmp_819_fu_11380_p1 = tmp_817_fu_11371_p2.read().range(10-1, 0);
}

void mnist_fp16::thread_tmp_81_fu_5016_p1() {
    tmp_81_fu_5016_p1 = esl_zext<64,2>(p_29_reg_1602.read());
}

void mnist_fp16::thread_tmp_820_fu_11392_p2() {
    tmp_820_fu_11392_p2 = (!p_shl106_cast_fu_11384_p3.read().is_01() || !tmp_818_fu_11376_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl106_cast_fu_11384_p3.read()) - sc_biguint<12>(tmp_818_fu_11376_p1.read()));
}

void mnist_fp16::thread_tmp_821_cast_fu_12505_p1() {
    tmp_821_cast_fu_12505_p1 = esl_zext<10,9>(tmp_790_fu_12497_p3.read());
}

void mnist_fp16::thread_tmp_821_fu_12014_p1() {
    tmp_821_fu_12014_p1 = p_63_reg_2450.read().range(3-1, 0);
}

void mnist_fp16::thread_tmp_822_fu_12024_p2() {
    tmp_822_fu_12024_p2 = (!tmp_821_fu_12014_p1.read().is_01() || !ap_const_lv3_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_821_fu_12014_p1.read() != ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_823_cast_fu_12527_p1() {
    tmp_823_cast_fu_12527_p1 = esl_sext<10,9>(tmp_792_fu_12521_p2.read());
}

void mnist_fp16::thread_tmp_823_fu_12078_p1() {
    tmp_823_fu_12078_p1 = mul9_fu_15319_p2.read().range(22-1, 0);
}

void mnist_fp16::thread_tmp_824_cast_fu_11936_p1() {
    tmp_824_cast_fu_11936_p1 = esl_zext<12,9>(tmp_793_fu_11931_p2.read());
}

void mnist_fp16::thread_tmp_825_fu_12107_p4() {
    tmp_825_fu_12107_p4 = neg_mul9_fu_12102_p2.read().range(21, 14);
}

void mnist_fp16::thread_tmp_826_fu_12117_p1() {
    tmp_826_fu_12117_p1 = esl_sext<11,8>(tmp_825_fu_12107_p4.read());
}

void mnist_fp16::thread_tmp_827_cast_fu_11549_p1() {
    tmp_827_cast_fu_11549_p1 = esl_zext<64,11>(tmp_795_fu_11544_p2.read());
}

void mnist_fp16::thread_tmp_828_cast_fu_11314_p1() {
    tmp_828_cast_fu_11314_p1 = esl_zext<64,11>(tmp_804_fu_11309_p2.read());
}

void mnist_fp16::thread_tmp_828_fu_12121_p1() {
    tmp_828_fu_12121_p1 = esl_sext<11,10>(tmp_827_reg_17945.read());
}

void mnist_fp16::thread_tmp_829_cast_fu_11265_p1() {
    tmp_829_cast_fu_11265_p1 = esl_zext<64,9>(tmp_805_fu_11260_p2.read());
}

void mnist_fp16::thread_tmp_829_fu_12124_p3() {
    tmp_829_fu_12124_p3 = (!tmp_824_reg_17932.read()[0].is_01())? sc_lv<11>(): ((tmp_824_reg_17932.read()[0].to_bool())? tmp_826_fu_12117_p1.read(): tmp_828_fu_12121_p1.read());
}

void mnist_fp16::thread_tmp_82_fu_3937_p2() {
    tmp_82_fu_3937_p2 = (!man_V_2_reg_15653.read().is_01() || !tmp_78_fu_3933_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_2_reg_15653.read()) >> (unsigned short)tmp_78_fu_3933_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_830_fu_12200_p1() {
    tmp_830_fu_12200_p1 = grp_fu_12144_p2.read().range(9-1, 0);
}

void mnist_fp16::thread_tmp_831_fu_12090_p1() {
    tmp_831_fu_12090_p1 = mul10_fu_15327_p2.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_832_fu_12155_p4() {
    tmp_832_fu_12155_p4 = neg_mul10_fu_12150_p2.read().range(22, 17);
}

void mnist_fp16::thread_tmp_833_fu_12165_p1() {
    tmp_833_fu_12165_p1 = esl_sext<12,6>(tmp_832_fu_12155_p4.read());
}

void mnist_fp16::thread_tmp_835_cast_fu_13606_p1() {
    tmp_835_cast_fu_13606_p1 = esl_sext<10,9>(tmp_811_fu_13600_p2.read());
}

void mnist_fp16::thread_tmp_835_fu_12169_p1() {
    tmp_835_fu_12169_p1 = esl_sext<12,7>(tmp_834_reg_17955.read());
}

void mnist_fp16::thread_tmp_836_cast_fu_12556_p1() {
    tmp_836_cast_fu_12556_p1 = esl_sext<11,10>(tmp_812_fu_12551_p2.read());
}

void mnist_fp16::thread_tmp_836_fu_12179_p1() {
    tmp_836_fu_12179_p1 = p_v4_fu_12172_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_837_fu_12189_p1() {
    tmp_837_fu_12189_p1 = p_v4_fu_12172_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_838_fu_12207_p3() {
    tmp_838_fu_12207_p3 = esl_concat<4,3>(r_V_26_reg_17965.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_839_cast_fu_11349_p1() {
    tmp_839_cast_fu_11349_p1 = esl_zext<11,8>(tmp_815_fu_11344_p2.read());
}

void mnist_fp16::thread_tmp_839_fu_12218_p2() {
    tmp_839_fu_12218_p2 = (!p_shl107_cast_fu_12214_p1.read().is_01() || !tmp_327_cast_fu_12204_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl107_cast_fu_12214_p1.read()) - sc_biguint<8>(tmp_327_cast_fu_12204_p1.read()));
}

void mnist_fp16::thread_tmp_83_fu_3300_p2() {
    tmp_83_fu_3300_p2 = (!ap_const_lv11_2.is_01())? sc_lv<11>(): grp_fu_3285_p2.read() << (unsigned short)ap_const_lv11_2.to_uint();
}

void mnist_fp16::thread_tmp_840_fu_12228_p2() {
    tmp_840_fu_12228_p2 = (!tmp_856_cast_fu_12224_p1.read().is_01() || !tmp_830_fu_12200_p1.read().is_01())? sc_lv<9>(): (sc_bigint<9>(tmp_856_cast_fu_12224_p1.read()) + sc_biguint<9>(tmp_830_fu_12200_p1.read()));
}

void mnist_fp16::thread_tmp_841_fu_12234_p1() {
    tmp_841_fu_12234_p1 = tmp_840_fu_12228_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_842_fu_12251_p2() {
    tmp_842_fu_12251_p2 = (!p_shl108_cast_fu_12244_p3.read().is_01() || !tmp_857_cast_fu_12241_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl108_cast_fu_12244_p3.read()) - sc_bigint<11>(tmp_857_cast_fu_12241_p1.read()));
}

void mnist_fp16::thread_tmp_843_fu_12257_p2() {
    tmp_843_fu_12257_p2 = (!tmp_842_fu_12251_p2.read().is_01() || !tmp_295_cast_fu_12238_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_842_fu_12251_p2.read()) + sc_biguint<11>(tmp_295_cast_fu_12238_p1.read()));
}

void mnist_fp16::thread_tmp_844_fu_14016_p3() {
    tmp_844_fu_14016_p3 = esl_concat<5,3>(p_83_reg_2613.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_845_fu_14028_p2() {
    tmp_845_fu_14028_p2 = (!p_shl109_cast_fu_14024_p1.read().is_01() || !tmp_417_cast_fu_14012_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl109_cast_fu_14024_p1.read()) - sc_biguint<9>(tmp_417_cast_fu_14012_p1.read()));
}

void mnist_fp16::thread_tmp_846_fu_13626_p2() {
    tmp_846_fu_13626_p2 = (!tmp_418_cast_fu_13622_p1.read().is_01() || !tmp_835_cast_reg_18350.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_418_cast_fu_13622_p1.read()) + sc_bigint<10>(tmp_835_cast_reg_18350.read()));
}

void mnist_fp16::thread_tmp_847_fu_13635_p1() {
    tmp_847_fu_13635_p1 = tmp_846_fu_13626_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_848_fu_13647_p2() {
    tmp_848_fu_13647_p2 = (!p_shl110_cast_fu_13639_p3.read().is_01() || !tmp_863_cast_fu_13631_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl110_cast_fu_13639_p3.read()) - sc_bigint<11>(tmp_863_cast_fu_13631_p1.read()));
}

void mnist_fp16::thread_tmp_849_fu_11423_p2() {
    tmp_849_fu_11423_p2 = (!tmp_816_reg_17705.read().is_01() || !tmp_391_cast_fu_11419_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_816_reg_17705.read()) + sc_biguint<11>(tmp_391_cast_fu_11419_p1.read()));
}

void mnist_fp16::thread_tmp_84_fu_4723_p3() {
    tmp_84_fu_4723_p3 = esl_concat<1,8>(is_neg_reg_15820.read(), p_Repl2_1_trunc_fu_4717_p2.read());
}

void mnist_fp16::thread_tmp_850_fu_12325_p1() {
    tmp_850_fu_12325_p1 = msb_idx_11_fu_12319_p2.read().range(31-1, 0);
}

void mnist_fp16::thread_tmp_852_fu_12347_p4() {
    tmp_852_fu_12347_p4 = msb_idx_12_fu_12337_p3.read().range(30, 5);
}

void mnist_fp16::thread_tmp_853_fu_12378_p1() {
    tmp_853_fu_12378_p1 = msb_idx_12_fu_12337_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_854_fu_12382_p2() {
    tmp_854_fu_12382_p2 = (!ap_const_lv4_1.is_01() || !tmp_853_fu_12378_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_853_fu_12378_p1.read()));
}

void mnist_fp16::thread_tmp_855_fu_12388_p1() {
    tmp_855_fu_12388_p1 = esl_zext<16,4>(tmp_854_fu_12382_p2.read());
}

void mnist_fp16::thread_tmp_856_cast_fu_12224_p1() {
    tmp_856_cast_fu_12224_p1 = esl_sext<9,8>(tmp_839_fu_12218_p2.read());
}

void mnist_fp16::thread_tmp_856_fu_11437_p2() {
    tmp_856_fu_11437_p2 = (!tmp_820_reg_17710.read().is_01() || !tmp_392_cast_fu_11433_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_820_reg_17710.read()) + sc_biguint<12>(tmp_392_cast_fu_11433_p1.read()));
}

void mnist_fp16::thread_tmp_857_cast_fu_12241_p1() {
    tmp_857_cast_fu_12241_p1 = esl_sext<11,9>(tmp_840_reg_17971.read());
}

void mnist_fp16::thread_tmp_857_fu_12429_p1() {
    tmp_857_fu_12429_p1 = msb_idx_11_reg_18011.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_858_fu_12471_p2() {
    tmp_858_fu_12471_p2 = (!tmp_794_reg_17896.read().is_01() || !tmp_431_cast_fu_12467_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_794_reg_17896.read()) + sc_biguint<12>(tmp_431_cast_fu_12467_p1.read()));
}

void mnist_fp16::thread_tmp_859_fu_14423_p1() {
    tmp_859_fu_14423_p1 = ireg_V_s_fu_14419_p1.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_85_fu_8093_p2() {
    tmp_85_fu_8093_p2 = (!p_31_reg_1967.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_31_reg_1967.read() != ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_860_cast_fu_12263_p1() {
    tmp_860_cast_fu_12263_p1 = esl_zext<64,11>(tmp_843_fu_12257_p2.read());
}

void mnist_fp16::thread_tmp_861_fu_14445_p1() {
    tmp_861_fu_14445_p1 = ireg_V_s_fu_14419_p1.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_862_cast_fu_14034_p1() {
    tmp_862_cast_fu_14034_p1 = esl_sext<10,9>(tmp_845_fu_14028_p2.read());
}

void mnist_fp16::thread_tmp_862_fu_14519_p1() {
    tmp_862_fu_14519_p1 = man_V_26_fu_14469_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_863_cast_fu_13631_p1() {
    tmp_863_cast_fu_13631_p1 = esl_sext<11,10>(tmp_846_fu_13626_p2.read());
}

void mnist_fp16::thread_tmp_863_fu_14523_p4() {
    tmp_863_fu_14523_p4 = sh_amt_9_fu_14505_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_864_fu_14592_p1() {
    tmp_864_fu_14592_p1 = tmp_452_fu_14587_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_865_fu_14612_p1() {
    tmp_865_fu_14612_p1 = tmp_455_fu_14606_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_866_cast_fu_11428_p1() {
    tmp_866_cast_fu_11428_p1 = esl_zext<64,11>(tmp_849_fu_11423_p2.read());
}

void mnist_fp16::thread_tmp_866_fu_14054_p2() {
    tmp_866_fu_14054_p2 = (!tmp_438_cast_fu_14050_p1.read().is_01() || !tmp_862_cast_reg_18477.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_438_cast_fu_14050_p1.read()) + sc_bigint<10>(tmp_862_cast_reg_18477.read()));
}

void mnist_fp16::thread_tmp_867_cast_fu_11442_p1() {
    tmp_867_cast_fu_11442_p1 = esl_zext<64,12>(tmp_856_fu_11437_p2.read());
}

void mnist_fp16::thread_tmp_867_fu_14063_p1() {
    tmp_867_fu_14063_p1 = tmp_866_fu_14054_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_868_cast_fu_12476_p1() {
    tmp_868_cast_fu_12476_p1 = esl_zext<64,12>(tmp_858_fu_12471_p2.read());
}

void mnist_fp16::thread_tmp_868_fu_14075_p2() {
    tmp_868_fu_14075_p2 = (!p_shl111_cast_fu_14067_p3.read().is_01() || !tmp_869_cast_fu_14059_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl111_cast_fu_14067_p3.read()) - sc_bigint<11>(tmp_869_cast_fu_14059_p1.read()));
}

void mnist_fp16::thread_tmp_869_cast_fu_14059_p1() {
    tmp_869_cast_fu_14059_p1 = esl_sext<11,10>(tmp_866_fu_14054_p2.read());
}

void mnist_fp16::thread_tmp_869_fu_13669_p2() {
    tmp_869_fu_13669_p2 = (!tmp_439_cast_fu_13665_p1.read().is_01() || !tmp_848_reg_18363.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_439_cast_fu_13665_p1.read()) + sc_biguint<11>(tmp_848_reg_18363.read()));
}

void mnist_fp16::thread_tmp_86_fu_8099_p2() {
    tmp_86_fu_8099_p2 = (!p_31_reg_1967.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_31_reg_1967.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp16::thread_tmp_870_fu_13692_p1() {
    tmp_870_fu_13692_p1 = conv6_0_load_to_int_fu_13679_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_871_fu_13729_p1() {
    tmp_871_fu_13729_p1 = ireg_V_1_fu_13721_p3.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_872_cast_fu_13674_p1() {
    tmp_872_cast_fu_13674_p1 = esl_zext<64,11>(tmp_869_fu_13669_p2.read());
}

void mnist_fp16::thread_tmp_873_cast_fu_13568_p1() {
    tmp_873_cast_fu_13568_p1 = esl_zext<64,11>(tmp_873_reg_18118.read());
}

void mnist_fp16::thread_tmp_873_fu_12669_p2() {
    tmp_873_fu_12669_p2 = (!tmp_814_reg_18077.read().is_01() || !tmp_497_cast_fu_12665_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_814_reg_18077.read()) + sc_biguint<11>(tmp_497_cast_fu_12665_p1.read()));
}

void mnist_fp16::thread_tmp_874_cast_fu_12619_p1() {
    tmp_874_cast_fu_12619_p1 = esl_zext<64,10>(tmp_874_fu_12614_p2.read());
}

void mnist_fp16::thread_tmp_874_fu_12614_p2() {
    tmp_874_fu_12614_p2 = (!tmp_821_cast_reg_18054.read().is_01() || !tmp_375_cast_fu_12610_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_821_cast_reg_18054.read()) + sc_biguint<10>(tmp_375_cast_fu_12610_p1.read()));
}

void mnist_fp16::thread_tmp_875_fu_13751_p1() {
    tmp_875_fu_13751_p1 = ireg_V_1_fu_13721_p3.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_876_fu_12635_p2() {
    tmp_876_fu_12635_p2 = (!p_shl9_fu_12631_p1.read().is_01() || !tmp_874_cast_fu_12619_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl9_fu_12631_p1.read()) - sc_biguint<64>(tmp_874_cast_fu_12619_p1.read()));
}

void mnist_fp16::thread_tmp_877_fu_12641_p3() {
    tmp_877_fu_12641_p3 = esl_concat<5,3>(p_74_reg_2523.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_878_fu_12653_p2() {
    tmp_878_fu_12653_p2 = (!tmp_375_cast1_fu_12606_p1.read().is_01() || !p_shl113_cast_fu_12649_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_375_cast1_fu_12606_p1.read()) + sc_biguint<9>(p_shl113_cast_fu_12649_p1.read()));
}

void mnist_fp16::thread_tmp_879_cast_fu_14102_p1() {
    tmp_879_cast_fu_14102_p1 = esl_zext<64,11>(tmp_879_fu_14097_p2.read());
}

void mnist_fp16::thread_tmp_879_fu_14097_p2() {
    tmp_879_fu_14097_p2 = (!tmp_868_reg_18490.read().is_01() || !tmp_466_cast_fu_14093_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_868_reg_18490.read()) + sc_biguint<11>(tmp_466_cast_fu_14093_p1.read()));
}

void mnist_fp16::thread_tmp_87_fu_7058_p3() {
    tmp_87_fu_7058_p3 = (!tmp_557_fu_7046_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_557_fu_7046_p2.read()[0].to_bool())? r_V_11_fu_7040_p2.read(): tmp_572_fu_7052_p2.read());
}

void mnist_fp16::thread_tmp_880_cast_fu_12704_p1() {
    tmp_880_cast_fu_12704_p1 = esl_zext<12,9>(tmp_880_fu_12699_p2.read());
}

void mnist_fp16::thread_tmp_880_fu_12699_p2() {
    tmp_880_fu_12699_p2 = (!tmp_442_cast_fu_12695_p1.read().is_01() || !tmp_878_reg_18108.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_442_cast_fu_12695_p1.read()) + sc_biguint<9>(tmp_878_reg_18108.read()));
}

void mnist_fp16::thread_tmp_881_fu_13826_p1() {
    tmp_881_fu_13826_p1 = man_V_28_fu_13781_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_882_fu_12716_p2() {
    tmp_882_fu_12716_p2 = (!p_shl114_cast_fu_12708_p3.read().is_01() || !tmp_880_cast_fu_12704_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl114_cast_fu_12708_p3.read()) + sc_biguint<12>(tmp_880_cast_fu_12704_p1.read()));
}

void mnist_fp16::thread_tmp_883_fu_12726_p2() {
    tmp_883_fu_12726_p2 = (!tmp_443_fu_12722_p1.read().is_01() || !tmp_876_reg_18103.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_443_fu_12722_p1.read()) + sc_biguint<64>(tmp_876_reg_18103.read()));
}

void mnist_fp16::thread_tmp_884_fu_13830_p4() {
    tmp_884_fu_13830_p4 = sh_amt_s_fu_13812_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_885_fu_12747_p2() {
    tmp_885_fu_12747_p2 = (!p_shl115_cast_fu_12739_p3.read().is_01() || !tmp_915_fu_12731_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl115_cast_fu_12739_p3.read()) - sc_biguint<13>(tmp_915_fu_12731_p1.read()));
}

void mnist_fp16::thread_tmp_886_fu_14727_p3() {
    tmp_886_fu_14727_p3 = esl_concat<5,3>(p_90_reg_2693.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_887_fu_14739_p3() {
    tmp_887_fu_14739_p3 = esl_concat<5,1>(p_90_reg_2693.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_888_fu_14751_p2() {
    tmp_888_fu_14751_p2 = (!p_shl116_cast_fu_14735_p1.read().is_01() || !p_shl117_cast_fu_14747_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl116_cast_fu_14735_p1.read()) + sc_biguint<9>(p_shl117_cast_fu_14747_p1.read()));
}

void mnist_fp16::thread_tmp_889_cast_fu_14902_p1() {
    tmp_889_cast_fu_14902_p1 = esl_zext<64,9>(tmp_889_reg_18682.read());
}

void mnist_fp16::thread_tmp_889_fu_14757_p2() {
    tmp_889_fu_14757_p2 = (!tmp_464_cast_reg_18669.read().is_01() || !tmp_888_fu_14751_p2.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_464_cast_reg_18669.read()) + sc_biguint<9>(tmp_888_fu_14751_p2.read()));
}

void mnist_fp16::thread_tmp_88_cast_fu_7274_p1() {
    tmp_88_cast_fu_7274_p1 = esl_zext<11,4>(tmp_87_reg_16481.read());
}

void mnist_fp16::thread_tmp_88_fu_3306_p2() {
    tmp_88_fu_3306_p2 = (!tmp_80_fu_3294_p2.read().is_01() || !tmp_83_fu_3300_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_80_fu_3294_p2.read()) - sc_biguint<11>(tmp_83_fu_3300_p2.read()));
}

void mnist_fp16::thread_tmp_890_cast_fu_12783_p1() {
    tmp_890_cast_fu_12783_p1 = esl_zext<64,12>(tmp_890_fu_12778_p2.read());
}

void mnist_fp16::thread_tmp_890_fu_12778_p2() {
    tmp_890_fu_12778_p2 = (!tmp_474_cast_fu_12774_p1.read().is_01() || !tmp_882_reg_18131.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_474_cast_fu_12774_p1.read()) + sc_biguint<12>(tmp_882_reg_18131.read()));
}

void mnist_fp16::thread_tmp_891_cast_fu_12797_p1() {
    tmp_891_cast_fu_12797_p1 = esl_zext<64,13>(tmp_891_fu_12792_p2.read());
}

void mnist_fp16::thread_tmp_891_fu_12792_p2() {
    tmp_891_fu_12792_p2 = (!tmp_526_cast_fu_12788_p1.read().is_01() || !tmp_885_reg_18136.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_526_cast_fu_12788_p1.read()) + sc_biguint<13>(tmp_885_reg_18136.read()));
}

void mnist_fp16::thread_tmp_892_fu_13898_p1() {
    tmp_892_fu_13898_p1 = tmp_487_fu_13893_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_893_fu_13918_p1() {
    tmp_893_fu_13918_p1 = tmp_495_fu_13912_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_895_fu_13418_p1() {
    tmp_895_fu_13418_p1 = msb_idx_13_fu_13412_p2.read().range(31-1, 0);
}

void mnist_fp16::thread_tmp_897_fu_13440_p4() {
    tmp_897_fu_13440_p4 = msb_idx_14_fu_13430_p3.read().range(30, 5);
}

void mnist_fp16::thread_tmp_898_fu_13471_p1() {
    tmp_898_fu_13471_p1 = msb_idx_14_fu_13430_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_899_fu_13475_p2() {
    tmp_899_fu_13475_p2 = (!ap_const_lv4_1.is_01() || !tmp_898_fu_13471_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_898_fu_13471_p1.read()));
}

void mnist_fp16::thread_tmp_89_fu_3953_p1() {
    tmp_89_fu_3953_p1 = esl_sext<32,16>(tmp_148_reg_15669.read());
}

void mnist_fp16::thread_tmp_8_fu_3189_p3() {
    tmp_8_fu_3189_p3 = (!tmp_64_fu_3177_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_64_fu_3177_p2.read()[0].to_bool())? r_V_fu_3171_p2.read(): tmp_65_fu_3183_p2.read());
}

void mnist_fp16::thread_tmp_900_fu_13481_p1() {
    tmp_900_fu_13481_p1 = esl_zext<16,4>(tmp_899_fu_13475_p2.read());
}

void mnist_fp16::thread_tmp_902_fu_13522_p1() {
    tmp_902_fu_13522_p1 = msb_idx_13_reg_18312.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_903_fu_12623_p3() {
    tmp_903_fu_12623_p3 = esl_concat<10,2>(tmp_874_fu_12614_p2.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_905_fu_14163_p1() {
    tmp_905_fu_14163_p1 = msb_idx_15_fu_14157_p2.read().range(31-1, 0);
}

void mnist_fp16::thread_tmp_907_fu_14185_p4() {
    tmp_907_fu_14185_p4 = msb_idx_16_fu_14175_p3.read().range(30, 5);
}

void mnist_fp16::thread_tmp_908_fu_14216_p1() {
    tmp_908_fu_14216_p1 = msb_idx_16_fu_14175_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_909_fu_14220_p2() {
    tmp_909_fu_14220_p2 = (!ap_const_lv4_1.is_01() || !tmp_908_fu_14216_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_908_fu_14216_p1.read()));
}

void mnist_fp16::thread_tmp_90_cast_fu_7066_p1() {
    tmp_90_cast_fu_7066_p1 = esl_zext<5,4>(tmp_87_fu_7058_p3.read());
}

void mnist_fp16::thread_tmp_90_fu_3956_p2() {
    tmp_90_fu_3956_p2 = (!sh_amt_cast_fu_3925_p1.read().is_01())? sc_lv<32>(): tmp_89_fu_3953_p1.read() << (unsigned short)sh_amt_cast_fu_3925_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_910_fu_14226_p1() {
    tmp_910_fu_14226_p1 = esl_zext<16,4>(tmp_909_fu_14220_p2.read());
}

void mnist_fp16::thread_tmp_911_fu_14230_p2() {
    tmp_911_fu_14230_p2 = (!tmp_910_fu_14226_p1.read().is_01())? sc_lv<16>(): tmp_V_8_reg_18530.read() >> (unsigned short)tmp_910_fu_14226_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_912_fu_14267_p1() {
    tmp_912_fu_14267_p1 = msb_idx_15_reg_18536.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_913_fu_14325_p1() {
    tmp_913_fu_14325_p1 = p_03_i5_to_int_fu_14312_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_914_fu_14343_p1() {
    tmp_914_fu_14343_p1 = tmp_457_to_int_fu_14329_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_915_fu_12731_p1() {
    tmp_915_fu_12731_p1 = tmp_883_fu_12726_p2.read().range(13-1, 0);
}

void mnist_fp16::thread_tmp_916_fu_12735_p1() {
    tmp_916_fu_12735_p1 = tmp_883_fu_12726_p2.read().range(11-1, 0);
}

void mnist_fp16::thread_tmp_917_fu_15003_p1() {
    tmp_917_fu_15003_p1 = p_a_assign_load_to_i_fu_14989_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_918_fu_15021_p1() {
    tmp_918_fu_15021_p1 = compute14_to_int_fu_15007_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_920_fu_14818_p1() {
    tmp_920_fu_14818_p1 = msb_idx_17_fu_14812_p2.read().range(31-1, 0);
}

void mnist_fp16::thread_tmp_922_fu_14840_p4() {
    tmp_922_fu_14840_p4 = msb_idx_18_fu_14830_p3.read().range(30, 5);
}

void mnist_fp16::thread_tmp_923_fu_14871_p1() {
    tmp_923_fu_14871_p1 = msb_idx_18_fu_14830_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_924_fu_14875_p2() {
    tmp_924_fu_14875_p2 = (!ap_const_lv4_1.is_01() || !tmp_923_fu_14871_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_923_fu_14871_p1.read()));
}

void mnist_fp16::thread_tmp_925_fu_14881_p1() {
    tmp_925_fu_14881_p1 = esl_zext<16,4>(tmp_924_fu_14875_p2.read());
}

void mnist_fp16::thread_tmp_926_fu_14885_p2() {
    tmp_926_fu_14885_p2 = (!tmp_925_fu_14881_p1.read().is_01())? sc_lv<16>(): tmp_V_9_reg_18714.read() >> (unsigned short)tmp_925_fu_14881_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_927_fu_14926_p1() {
    tmp_927_fu_14926_p1 = msb_idx_17_reg_18720.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_928_fu_12806_p1() {
    tmp_928_fu_12806_p1 = ireg_V_16_fu_12802_p1.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_930_fu_12828_p1() {
    tmp_930_fu_12828_p1 = ireg_V_16_fu_12802_p1.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_931_fu_12939_p1() {
    tmp_931_fu_12939_p1 = man_V_20_fu_12894_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_932_fu_12943_p4() {
    tmp_932_fu_12943_p4 = sh_amt_10_fu_12925_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_933_fu_13061_p1() {
    tmp_933_fu_13061_p1 = tmp_521_fu_13056_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_934_fu_13081_p1() {
    tmp_934_fu_13081_p1 = tmp_524_fu_13075_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_935_fu_12842_p1() {
    tmp_935_fu_12842_p1 = ireg_V_17_fu_12838_p1.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_937_fu_12864_p1() {
    tmp_937_fu_12864_p1 = ireg_V_17_fu_12838_p1.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_938_fu_13024_p1() {
    tmp_938_fu_13024_p1 = man_V_25_fu_12979_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_939_fu_13028_p4() {
    tmp_939_fu_13028_p4 = sh_amt_11_fu_13010_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_93_cast_fu_4745_p1() {
    tmp_93_cast_fu_4745_p1 = esl_zext<13,5>(p_14_reg_1502.read());
}

void mnist_fp16::thread_tmp_93_fu_3312_p2() {
    tmp_93_fu_3312_p2 = (!tmp_10_cast_fu_3291_p1.read().is_01() || !tmp_88_fu_3306_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_10_cast_fu_3291_p1.read()) + sc_biguint<11>(tmp_88_fu_3306_p2.read()));
}

void mnist_fp16::thread_tmp_940_fu_13210_p1() {
    tmp_940_fu_13210_p1 = tmp_550_fu_13205_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_941_fu_13230_p1() {
    tmp_941_fu_13230_p1 = tmp_556_fu_13224_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_942_fu_15145_p1() {
    tmp_942_fu_15145_p1 = pred_2_to_int_fu_15132_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_943_fu_15163_p1() {
    tmp_943_fu_15163_p1 = pred_to_int_fu_15149_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_94_fu_3326_p2() {
    tmp_94_fu_3326_p2 = (!tmp_11_reg_15410.read().is_01() || !tmp_16_cast_fu_3322_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_11_reg_15410.read()) + sc_biguint<11>(tmp_16_cast_fu_3322_p1.read()));
}

void mnist_fp16::thread_tmp_96_fu_4062_p3() {
    tmp_96_fu_4062_p3 = esl_concat<3,5>(p_6_reg_1468.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_97_cast_fu_3016_p1() {
    tmp_97_cast_fu_3016_p1 = esl_sext<64,11>(tmp_34_fu_3011_p2.read());
}

void mnist_fp16::thread_tmp_97_fu_4074_p3() {
    tmp_97_fu_4074_p3 = esl_concat<3,1>(p_6_reg_1468.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_98_fu_4086_p2() {
    tmp_98_fu_4086_p2 = (!p_shl26_cast_fu_4070_p1.read().is_01() || !p_shl27_cast_fu_4082_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl26_cast_fu_4070_p1.read()) - sc_biguint<9>(p_shl27_cast_fu_4082_p1.read()));
}

void mnist_fp16::thread_tmp_99_fu_6081_p1() {
    tmp_99_fu_6081_p1 = esl_zext<12,11>(p_Result_6_reg_16224.read());
}

void mnist_fp16::thread_tmp_9_fu_2958_p3() {
    tmp_9_fu_2958_p3 = esl_concat<5,2>(p_0_reg_1197.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_V_2_fu_6603_p3() {
    tmp_V_2_fu_6603_p3 = (!tmp_603_reg_16360.read()[0].is_01())? sc_lv<16>(): ((tmp_603_reg_16360.read()[0].to_bool())? tmp_188_reg_16366.read(): relu2_0_V_load_reg_16354.read());
}

void mnist_fp16::thread_tmp_V_5_fu_10456_p3() {
    tmp_V_5_fu_10456_p3 = (!tmp_779_reg_17440.read()[0].is_01())? sc_lv<16>(): ((tmp_779_reg_17440.read()[0].to_bool())? tmp_354_reg_17446.read(): relu4_0_V_load_reg_17434.read());
}

void mnist_fp16::thread_tmp_V_8_fu_14126_p3() {
    tmp_V_8_fu_14126_p3 = (!tmp_904_reg_18514.read()[0].is_01())? sc_lv<16>(): ((tmp_904_reg_18514.read()[0].to_bool())? tmp_500_reg_18520.read(): relu6_0_V_load_reg_18508.read());
}

void mnist_fp16::thread_tmp_V_9_fu_14781_p3() {
    tmp_V_9_fu_14781_p3 = (!tmp_919_reg_18698.read()[0].is_01())? sc_lv<16>(): ((tmp_919_reg_18698.read()[0].to_bool())? tmp_529_reg_18704.read(): pool3_0_V_load_reg_18692.read());
}

void mnist_fp16::thread_tmp_user_V_fu_2925_p1() {
    tmp_user_V_fu_2925_p1 = stream_in_V_user_V_0_data_out.read();
}

void mnist_fp16::thread_w1_V_fu_10314_p2() {
    w1_V_fu_10314_p2 = (!p_65_reg_2165.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_65_reg_2165.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_w_V_fu_6461_p2() {
    w_V_fu_6461_p2 = (!p_33_reg_1691.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_33_reg_1691.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_w_conv1_address0() {
    w_conv1_address0 =  (sc_lv<6>) (tmp_604_cast_fu_3595_p1.read());
}

void mnist_fp16::thread_w_conv1_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state37.read())) {
        w_conv1_ce0 = ap_const_logic_1;
    } else {
        w_conv1_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_w_conv2_address0() {
    w_conv2_address0 =  (sc_lv<8>) (tmp_679_cast_fu_5092_p1.read());
}

void mnist_fp16::thread_w_conv2_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state95.read())) {
        w_conv2_ce0 = ap_const_logic_1;
    } else {
        w_conv2_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_w_conv3_address0() {
    w_conv3_address0 =  (sc_lv<9>) (tmp_762_cast_fu_7602_p1.read());
}

void mnist_fp16::thread_w_conv3_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state161.read())) {
        w_conv3_ce0 = ap_const_logic_1;
    } else {
        w_conv3_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_w_conv4_address0() {
    w_conv4_address0 =  (sc_lv<10>) (tmp_796_cast_fu_8961_p1.read());
}

void mnist_fp16::thread_w_conv4_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state217.read())) {
        w_conv4_ce0 = ap_const_logic_1;
    } else {
        w_conv4_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_w_conv5_address0() {
    w_conv5_address0 =  (sc_lv<11>) (tmp_867_cast_fu_11442_p1.read());
}

void mnist_fp16::thread_w_conv5_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state282.read())) {
        w_conv5_ce0 = ap_const_logic_1;
    } else {
        w_conv5_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_w_conv6_address0() {
    w_conv6_address0 =  (sc_lv<12>) (tmp_891_cast_fu_12797_p1.read());
}

void mnist_fp16::thread_w_conv6_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state337.read())) {
        w_conv6_ce0 = ap_const_logic_1;
    } else {
        w_conv6_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_w_dense_1_address0() {
    w_dense_1_address0 =  (sc_lv<8>) (tmp_889_cast_fu_14902_p1.read());
}

void mnist_fp16::thread_w_dense_1_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state387.read())) {
        w_dense_1_ce0 = ap_const_logic_1;
    } else {
        w_dense_1_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_xx1_V_fu_4866_p2() {
    xx1_V_fu_4866_p2 = (!p_18_reg_1555.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_18_reg_1555.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_xx2_V_fu_7441_p2() {
    xx2_V_fu_7441_p2 = (!p_32_reg_1829.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_32_reg_1829.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_xx3_V_fu_8808_p2() {
    xx3_V_fu_8808_p2 = (!p_44_reg_2028.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_44_reg_2028.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_xx4_V_fu_11230_p2() {
    xx4_V_fu_11230_p2 = (!p_58_reg_2302.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_58_reg_2302.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_xx5_V_fu_12584_p2() {
    xx5_V_fu_12584_p2 = (!p_67_reg_2499.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_67_reg_2499.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_xx_V_fu_3457_p2() {
    xx_V_fu_3457_p2 = (!p_8_reg_1376.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_8_reg_1376.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_yy1_V_fu_4815_p2() {
    yy1_V_fu_4815_p2 = (!p_10_reg_1543.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_10_reg_1543.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_yy2_V_fu_7390_p2() {
    yy2_V_fu_7390_p2 = (!p_25_reg_1817.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_25_reg_1817.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_yy3_V_fu_8757_p2() {
    yy3_V_fu_8757_p2 = (!p_35_reg_2016.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_35_reg_2016.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_yy4_V_fu_11183_p2() {
    yy4_V_fu_11183_p2 = (!p_49_reg_2291.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_49_reg_2291.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_yy5_V_fu_12537_p2() {
    yy5_V_fu_12537_p2 = (!p_62_reg_2488.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_62_reg_2488.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_yy_V_fu_3406_p2() {
    yy_V_fu_3406_p2 = (!p_5_reg_1364.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_5_reg_1364.read()) + sc_biguint<5>(ap_const_lv5_1));
}

}

