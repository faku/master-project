#include "mnist_fp32.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp32::thread_F2_10_fu_11900_p2() {
    F2_10_fu_11900_p2 = (!ap_const_lv12_433.is_01() || !tmp_422_fu_11873_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_422_fu_11873_p1.read()));
}

void mnist_fp32::thread_F2_11_fu_11985_p2() {
    F2_11_fu_11985_p2 = (!ap_const_lv12_433.is_01() || !tmp_459_fu_11958_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_459_fu_11958_p1.read()));
}

void mnist_fp32::thread_F2_1_fu_5593_p2() {
    F2_1_fu_5593_p2 = (!ap_const_lv12_433.is_01() || !tmp_88_fu_5566_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_88_fu_5566_p1.read()));
}

void mnist_fp32::thread_F2_2_fu_4775_p2() {
    F2_2_fu_4775_p2 = (!ap_const_lv12_433.is_01() || !tmp_94_fu_4748_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_94_fu_4748_p1.read()));
}

void mnist_fp32::thread_F2_3_fu_4860_p2() {
    F2_3_fu_4860_p2 = (!ap_const_lv12_433.is_01() || !tmp_150_fu_4833_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_150_fu_4833_p1.read()));
}

void mnist_fp32::thread_F2_4_fu_7226_p2() {
    F2_4_fu_7226_p2 = (!ap_const_lv12_433.is_01() || !tmp_228_fu_7199_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_228_fu_7199_p1.read()));
}

void mnist_fp32::thread_F2_5_fu_9172_p2() {
    F2_5_fu_9172_p2 = (!ap_const_lv12_433.is_01() || !tmp_246_fu_9145_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_246_fu_9145_p1.read()));
}

void mnist_fp32::thread_F2_6_fu_8354_p2() {
    F2_6_fu_8354_p2 = (!ap_const_lv12_433.is_01() || !tmp_252_fu_8327_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_252_fu_8327_p1.read()));
}

void mnist_fp32::thread_F2_7_fu_8439_p2() {
    F2_7_fu_8439_p2 = (!ap_const_lv12_433.is_01() || !tmp_306_fu_8412_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_306_fu_8412_p1.read()));
}

void mnist_fp32::thread_F2_8_fu_10760_p2() {
    F2_8_fu_10760_p2 = (!ap_const_lv12_433.is_01() || !tmp_349_fu_10733_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_349_fu_10733_p1.read()));
}

void mnist_fp32::thread_F2_9_fu_13288_p2() {
    F2_9_fu_13288_p2 = (!ap_const_lv12_433.is_01() || !tmp_390_fu_13256_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_390_fu_13256_p1.read()));
}

void mnist_fp32::thread_F2_fu_3509_p2() {
    F2_fu_3509_p2 = (!ap_const_lv12_433.is_01() || !tmp_26_fu_3482_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_26_fu_3482_p1.read()));
}

void mnist_fp32::thread_F2_s_fu_12693_p2() {
    F2_s_fu_12693_p2 = (!ap_const_lv12_433.is_01() || !tmp_418_fu_12666_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_418_fu_12666_p1.read()));
}

void mnist_fp32::thread_ap_CS_fsm_state1() {
    ap_CS_fsm_state1 = ap_CS_fsm.read()[0];
}

void mnist_fp32::thread_ap_CS_fsm_state100() {
    ap_CS_fsm_state100 = ap_CS_fsm.read()[99];
}

void mnist_fp32::thread_ap_CS_fsm_state101() {
    ap_CS_fsm_state101 = ap_CS_fsm.read()[100];
}

void mnist_fp32::thread_ap_CS_fsm_state102() {
    ap_CS_fsm_state102 = ap_CS_fsm.read()[101];
}

void mnist_fp32::thread_ap_CS_fsm_state103() {
    ap_CS_fsm_state103 = ap_CS_fsm.read()[102];
}

void mnist_fp32::thread_ap_CS_fsm_state104() {
    ap_CS_fsm_state104 = ap_CS_fsm.read()[103];
}

void mnist_fp32::thread_ap_CS_fsm_state105() {
    ap_CS_fsm_state105 = ap_CS_fsm.read()[104];
}

void mnist_fp32::thread_ap_CS_fsm_state106() {
    ap_CS_fsm_state106 = ap_CS_fsm.read()[105];
}

void mnist_fp32::thread_ap_CS_fsm_state107() {
    ap_CS_fsm_state107 = ap_CS_fsm.read()[106];
}

void mnist_fp32::thread_ap_CS_fsm_state108() {
    ap_CS_fsm_state108 = ap_CS_fsm.read()[107];
}

void mnist_fp32::thread_ap_CS_fsm_state109() {
    ap_CS_fsm_state109 = ap_CS_fsm.read()[108];
}

void mnist_fp32::thread_ap_CS_fsm_state110() {
    ap_CS_fsm_state110 = ap_CS_fsm.read()[109];
}

void mnist_fp32::thread_ap_CS_fsm_state111() {
    ap_CS_fsm_state111 = ap_CS_fsm.read()[110];
}

void mnist_fp32::thread_ap_CS_fsm_state112() {
    ap_CS_fsm_state112 = ap_CS_fsm.read()[111];
}

void mnist_fp32::thread_ap_CS_fsm_state113() {
    ap_CS_fsm_state113 = ap_CS_fsm.read()[112];
}

void mnist_fp32::thread_ap_CS_fsm_state114() {
    ap_CS_fsm_state114 = ap_CS_fsm.read()[113];
}

void mnist_fp32::thread_ap_CS_fsm_state115() {
    ap_CS_fsm_state115 = ap_CS_fsm.read()[114];
}

void mnist_fp32::thread_ap_CS_fsm_state120() {
    ap_CS_fsm_state120 = ap_CS_fsm.read()[119];
}

void mnist_fp32::thread_ap_CS_fsm_state121() {
    ap_CS_fsm_state121 = ap_CS_fsm.read()[120];
}

void mnist_fp32::thread_ap_CS_fsm_state122() {
    ap_CS_fsm_state122 = ap_CS_fsm.read()[121];
}

void mnist_fp32::thread_ap_CS_fsm_state123() {
    ap_CS_fsm_state123 = ap_CS_fsm.read()[122];
}

void mnist_fp32::thread_ap_CS_fsm_state124() {
    ap_CS_fsm_state124 = ap_CS_fsm.read()[123];
}

void mnist_fp32::thread_ap_CS_fsm_state125() {
    ap_CS_fsm_state125 = ap_CS_fsm.read()[124];
}

void mnist_fp32::thread_ap_CS_fsm_state126() {
    ap_CS_fsm_state126 = ap_CS_fsm.read()[125];
}

void mnist_fp32::thread_ap_CS_fsm_state127() {
    ap_CS_fsm_state127 = ap_CS_fsm.read()[126];
}

void mnist_fp32::thread_ap_CS_fsm_state141() {
    ap_CS_fsm_state141 = ap_CS_fsm.read()[140];
}

void mnist_fp32::thread_ap_CS_fsm_state142() {
    ap_CS_fsm_state142 = ap_CS_fsm.read()[141];
}

void mnist_fp32::thread_ap_CS_fsm_state143() {
    ap_CS_fsm_state143 = ap_CS_fsm.read()[142];
}

void mnist_fp32::thread_ap_CS_fsm_state144() {
    ap_CS_fsm_state144 = ap_CS_fsm.read()[143];
}

void mnist_fp32::thread_ap_CS_fsm_state145() {
    ap_CS_fsm_state145 = ap_CS_fsm.read()[144];
}

void mnist_fp32::thread_ap_CS_fsm_state146() {
    ap_CS_fsm_state146 = ap_CS_fsm.read()[145];
}

void mnist_fp32::thread_ap_CS_fsm_state147() {
    ap_CS_fsm_state147 = ap_CS_fsm.read()[146];
}

void mnist_fp32::thread_ap_CS_fsm_state148() {
    ap_CS_fsm_state148 = ap_CS_fsm.read()[147];
}

void mnist_fp32::thread_ap_CS_fsm_state149() {
    ap_CS_fsm_state149 = ap_CS_fsm.read()[148];
}

void mnist_fp32::thread_ap_CS_fsm_state150() {
    ap_CS_fsm_state150 = ap_CS_fsm.read()[149];
}

void mnist_fp32::thread_ap_CS_fsm_state151() {
    ap_CS_fsm_state151 = ap_CS_fsm.read()[150];
}

void mnist_fp32::thread_ap_CS_fsm_state152() {
    ap_CS_fsm_state152 = ap_CS_fsm.read()[151];
}

void mnist_fp32::thread_ap_CS_fsm_state155() {
    ap_CS_fsm_state155 = ap_CS_fsm.read()[154];
}

void mnist_fp32::thread_ap_CS_fsm_state156() {
    ap_CS_fsm_state156 = ap_CS_fsm.read()[155];
}

void mnist_fp32::thread_ap_CS_fsm_state160() {
    ap_CS_fsm_state160 = ap_CS_fsm.read()[159];
}

void mnist_fp32::thread_ap_CS_fsm_state161() {
    ap_CS_fsm_state161 = ap_CS_fsm.read()[160];
}

void mnist_fp32::thread_ap_CS_fsm_state162() {
    ap_CS_fsm_state162 = ap_CS_fsm.read()[161];
}

void mnist_fp32::thread_ap_CS_fsm_state163() {
    ap_CS_fsm_state163 = ap_CS_fsm.read()[162];
}

void mnist_fp32::thread_ap_CS_fsm_state164() {
    ap_CS_fsm_state164 = ap_CS_fsm.read()[163];
}

void mnist_fp32::thread_ap_CS_fsm_state165() {
    ap_CS_fsm_state165 = ap_CS_fsm.read()[164];
}

void mnist_fp32::thread_ap_CS_fsm_state166() {
    ap_CS_fsm_state166 = ap_CS_fsm.read()[165];
}

void mnist_fp32::thread_ap_CS_fsm_state167() {
    ap_CS_fsm_state167 = ap_CS_fsm.read()[166];
}

void mnist_fp32::thread_ap_CS_fsm_state168() {
    ap_CS_fsm_state168 = ap_CS_fsm.read()[167];
}

void mnist_fp32::thread_ap_CS_fsm_state169() {
    ap_CS_fsm_state169 = ap_CS_fsm.read()[168];
}

void mnist_fp32::thread_ap_CS_fsm_state170() {
    ap_CS_fsm_state170 = ap_CS_fsm.read()[169];
}

void mnist_fp32::thread_ap_CS_fsm_state171() {
    ap_CS_fsm_state171 = ap_CS_fsm.read()[170];
}

void mnist_fp32::thread_ap_CS_fsm_state172() {
    ap_CS_fsm_state172 = ap_CS_fsm.read()[171];
}

void mnist_fp32::thread_ap_CS_fsm_state173() {
    ap_CS_fsm_state173 = ap_CS_fsm.read()[172];
}

void mnist_fp32::thread_ap_CS_fsm_state188() {
    ap_CS_fsm_state188 = ap_CS_fsm.read()[187];
}

void mnist_fp32::thread_ap_CS_fsm_state189() {
    ap_CS_fsm_state189 = ap_CS_fsm.read()[188];
}

void mnist_fp32::thread_ap_CS_fsm_state190() {
    ap_CS_fsm_state190 = ap_CS_fsm.read()[189];
}

void mnist_fp32::thread_ap_CS_fsm_state191() {
    ap_CS_fsm_state191 = ap_CS_fsm.read()[190];
}

void mnist_fp32::thread_ap_CS_fsm_state192() {
    ap_CS_fsm_state192 = ap_CS_fsm.read()[191];
}

void mnist_fp32::thread_ap_CS_fsm_state197() {
    ap_CS_fsm_state197 = ap_CS_fsm.read()[196];
}

void mnist_fp32::thread_ap_CS_fsm_state198() {
    ap_CS_fsm_state198 = ap_CS_fsm.read()[197];
}

void mnist_fp32::thread_ap_CS_fsm_state199() {
    ap_CS_fsm_state199 = ap_CS_fsm.read()[198];
}

void mnist_fp32::thread_ap_CS_fsm_state2() {
    ap_CS_fsm_state2 = ap_CS_fsm.read()[1];
}

void mnist_fp32::thread_ap_CS_fsm_state20() {
    ap_CS_fsm_state20 = ap_CS_fsm.read()[19];
}

void mnist_fp32::thread_ap_CS_fsm_state200() {
    ap_CS_fsm_state200 = ap_CS_fsm.read()[199];
}

void mnist_fp32::thread_ap_CS_fsm_state201() {
    ap_CS_fsm_state201 = ap_CS_fsm.read()[200];
}

void mnist_fp32::thread_ap_CS_fsm_state202() {
    ap_CS_fsm_state202 = ap_CS_fsm.read()[201];
}

void mnist_fp32::thread_ap_CS_fsm_state203() {
    ap_CS_fsm_state203 = ap_CS_fsm.read()[202];
}

void mnist_fp32::thread_ap_CS_fsm_state204() {
    ap_CS_fsm_state204 = ap_CS_fsm.read()[203];
}

void mnist_fp32::thread_ap_CS_fsm_state205() {
    ap_CS_fsm_state205 = ap_CS_fsm.read()[204];
}

void mnist_fp32::thread_ap_CS_fsm_state206() {
    ap_CS_fsm_state206 = ap_CS_fsm.read()[205];
}

void mnist_fp32::thread_ap_CS_fsm_state207() {
    ap_CS_fsm_state207 = ap_CS_fsm.read()[206];
}

void mnist_fp32::thread_ap_CS_fsm_state208() {
    ap_CS_fsm_state208 = ap_CS_fsm.read()[207];
}

void mnist_fp32::thread_ap_CS_fsm_state209() {
    ap_CS_fsm_state209 = ap_CS_fsm.read()[208];
}

void mnist_fp32::thread_ap_CS_fsm_state21() {
    ap_CS_fsm_state21 = ap_CS_fsm.read()[20];
}

void mnist_fp32::thread_ap_CS_fsm_state210() {
    ap_CS_fsm_state210 = ap_CS_fsm.read()[209];
}

void mnist_fp32::thread_ap_CS_fsm_state211() {
    ap_CS_fsm_state211 = ap_CS_fsm.read()[210];
}

void mnist_fp32::thread_ap_CS_fsm_state212() {
    ap_CS_fsm_state212 = ap_CS_fsm.read()[211];
}

void mnist_fp32::thread_ap_CS_fsm_state213() {
    ap_CS_fsm_state213 = ap_CS_fsm.read()[212];
}

void mnist_fp32::thread_ap_CS_fsm_state218() {
    ap_CS_fsm_state218 = ap_CS_fsm.read()[217];
}

void mnist_fp32::thread_ap_CS_fsm_state219() {
    ap_CS_fsm_state219 = ap_CS_fsm.read()[218];
}

void mnist_fp32::thread_ap_CS_fsm_state22() {
    ap_CS_fsm_state22 = ap_CS_fsm.read()[21];
}

void mnist_fp32::thread_ap_CS_fsm_state220() {
    ap_CS_fsm_state220 = ap_CS_fsm.read()[219];
}

void mnist_fp32::thread_ap_CS_fsm_state221() {
    ap_CS_fsm_state221 = ap_CS_fsm.read()[220];
}

void mnist_fp32::thread_ap_CS_fsm_state222() {
    ap_CS_fsm_state222 = ap_CS_fsm.read()[221];
}

void mnist_fp32::thread_ap_CS_fsm_state223() {
    ap_CS_fsm_state223 = ap_CS_fsm.read()[222];
}

void mnist_fp32::thread_ap_CS_fsm_state224() {
    ap_CS_fsm_state224 = ap_CS_fsm.read()[223];
}

void mnist_fp32::thread_ap_CS_fsm_state225() {
    ap_CS_fsm_state225 = ap_CS_fsm.read()[224];
}

void mnist_fp32::thread_ap_CS_fsm_state226() {
    ap_CS_fsm_state226 = ap_CS_fsm.read()[225];
}

void mnist_fp32::thread_ap_CS_fsm_state227() {
    ap_CS_fsm_state227 = ap_CS_fsm.read()[226];
}

void mnist_fp32::thread_ap_CS_fsm_state228() {
    ap_CS_fsm_state228 = ap_CS_fsm.read()[227];
}

void mnist_fp32::thread_ap_CS_fsm_state229() {
    ap_CS_fsm_state229 = ap_CS_fsm.read()[228];
}

void mnist_fp32::thread_ap_CS_fsm_state23() {
    ap_CS_fsm_state23 = ap_CS_fsm.read()[22];
}

void mnist_fp32::thread_ap_CS_fsm_state230() {
    ap_CS_fsm_state230 = ap_CS_fsm.read()[229];
}

void mnist_fp32::thread_ap_CS_fsm_state231() {
    ap_CS_fsm_state231 = ap_CS_fsm.read()[230];
}

void mnist_fp32::thread_ap_CS_fsm_state232() {
    ap_CS_fsm_state232 = ap_CS_fsm.read()[231];
}

void mnist_fp32::thread_ap_CS_fsm_state233() {
    ap_CS_fsm_state233 = ap_CS_fsm.read()[232];
}

void mnist_fp32::thread_ap_CS_fsm_state234() {
    ap_CS_fsm_state234 = ap_CS_fsm.read()[233];
}

void mnist_fp32::thread_ap_CS_fsm_state235() {
    ap_CS_fsm_state235 = ap_CS_fsm.read()[234];
}

void mnist_fp32::thread_ap_CS_fsm_state24() {
    ap_CS_fsm_state24 = ap_CS_fsm.read()[23];
}

void mnist_fp32::thread_ap_CS_fsm_state240() {
    ap_CS_fsm_state240 = ap_CS_fsm.read()[239];
}

void mnist_fp32::thread_ap_CS_fsm_state241() {
    ap_CS_fsm_state241 = ap_CS_fsm.read()[240];
}

void mnist_fp32::thread_ap_CS_fsm_state242() {
    ap_CS_fsm_state242 = ap_CS_fsm.read()[241];
}

void mnist_fp32::thread_ap_CS_fsm_state243() {
    ap_CS_fsm_state243 = ap_CS_fsm.read()[242];
}

void mnist_fp32::thread_ap_CS_fsm_state244() {
    ap_CS_fsm_state244 = ap_CS_fsm.read()[243];
}

void mnist_fp32::thread_ap_CS_fsm_state245() {
    ap_CS_fsm_state245 = ap_CS_fsm.read()[244];
}

void mnist_fp32::thread_ap_CS_fsm_state246() {
    ap_CS_fsm_state246 = ap_CS_fsm.read()[245];
}

void mnist_fp32::thread_ap_CS_fsm_state247() {
    ap_CS_fsm_state247 = ap_CS_fsm.read()[246];
}

void mnist_fp32::thread_ap_CS_fsm_state25() {
    ap_CS_fsm_state25 = ap_CS_fsm.read()[24];
}

void mnist_fp32::thread_ap_CS_fsm_state26() {
    ap_CS_fsm_state26 = ap_CS_fsm.read()[25];
}

void mnist_fp32::thread_ap_CS_fsm_state260() {
    ap_CS_fsm_state260 = ap_CS_fsm.read()[259];
}

void mnist_fp32::thread_ap_CS_fsm_state261() {
    ap_CS_fsm_state261 = ap_CS_fsm.read()[260];
}

void mnist_fp32::thread_ap_CS_fsm_state262() {
    ap_CS_fsm_state262 = ap_CS_fsm.read()[261];
}

void mnist_fp32::thread_ap_CS_fsm_state263() {
    ap_CS_fsm_state263 = ap_CS_fsm.read()[262];
}

void mnist_fp32::thread_ap_CS_fsm_state264() {
    ap_CS_fsm_state264 = ap_CS_fsm.read()[263];
}

void mnist_fp32::thread_ap_CS_fsm_state265() {
    ap_CS_fsm_state265 = ap_CS_fsm.read()[264];
}

void mnist_fp32::thread_ap_CS_fsm_state266() {
    ap_CS_fsm_state266 = ap_CS_fsm.read()[265];
}

void mnist_fp32::thread_ap_CS_fsm_state267() {
    ap_CS_fsm_state267 = ap_CS_fsm.read()[266];
}

void mnist_fp32::thread_ap_CS_fsm_state268() {
    ap_CS_fsm_state268 = ap_CS_fsm.read()[267];
}

void mnist_fp32::thread_ap_CS_fsm_state269() {
    ap_CS_fsm_state269 = ap_CS_fsm.read()[268];
}

void mnist_fp32::thread_ap_CS_fsm_state27() {
    ap_CS_fsm_state27 = ap_CS_fsm.read()[26];
}

void mnist_fp32::thread_ap_CS_fsm_state270() {
    ap_CS_fsm_state270 = ap_CS_fsm.read()[269];
}

void mnist_fp32::thread_ap_CS_fsm_state271() {
    ap_CS_fsm_state271 = ap_CS_fsm.read()[270];
}

void mnist_fp32::thread_ap_CS_fsm_state274() {
    ap_CS_fsm_state274 = ap_CS_fsm.read()[273];
}

void mnist_fp32::thread_ap_CS_fsm_state275() {
    ap_CS_fsm_state275 = ap_CS_fsm.read()[274];
}

void mnist_fp32::thread_ap_CS_fsm_state279() {
    ap_CS_fsm_state279 = ap_CS_fsm.read()[278];
}

void mnist_fp32::thread_ap_CS_fsm_state28() {
    ap_CS_fsm_state28 = ap_CS_fsm.read()[27];
}

void mnist_fp32::thread_ap_CS_fsm_state280() {
    ap_CS_fsm_state280 = ap_CS_fsm.read()[279];
}

void mnist_fp32::thread_ap_CS_fsm_state281() {
    ap_CS_fsm_state281 = ap_CS_fsm.read()[280];
}

void mnist_fp32::thread_ap_CS_fsm_state282() {
    ap_CS_fsm_state282 = ap_CS_fsm.read()[281];
}

void mnist_fp32::thread_ap_CS_fsm_state283() {
    ap_CS_fsm_state283 = ap_CS_fsm.read()[282];
}

void mnist_fp32::thread_ap_CS_fsm_state284() {
    ap_CS_fsm_state284 = ap_CS_fsm.read()[283];
}

void mnist_fp32::thread_ap_CS_fsm_state285() {
    ap_CS_fsm_state285 = ap_CS_fsm.read()[284];
}

void mnist_fp32::thread_ap_CS_fsm_state286() {
    ap_CS_fsm_state286 = ap_CS_fsm.read()[285];
}

void mnist_fp32::thread_ap_CS_fsm_state287() {
    ap_CS_fsm_state287 = ap_CS_fsm.read()[286];
}

void mnist_fp32::thread_ap_CS_fsm_state288() {
    ap_CS_fsm_state288 = ap_CS_fsm.read()[287];
}

void mnist_fp32::thread_ap_CS_fsm_state289() {
    ap_CS_fsm_state289 = ap_CS_fsm.read()[288];
}

void mnist_fp32::thread_ap_CS_fsm_state29() {
    ap_CS_fsm_state29 = ap_CS_fsm.read()[28];
}

void mnist_fp32::thread_ap_CS_fsm_state290() {
    ap_CS_fsm_state290 = ap_CS_fsm.read()[289];
}

void mnist_fp32::thread_ap_CS_fsm_state291() {
    ap_CS_fsm_state291 = ap_CS_fsm.read()[290];
}

void mnist_fp32::thread_ap_CS_fsm_state292() {
    ap_CS_fsm_state292 = ap_CS_fsm.read()[291];
}

void mnist_fp32::thread_ap_CS_fsm_state3() {
    ap_CS_fsm_state3 = ap_CS_fsm.read()[2];
}

void mnist_fp32::thread_ap_CS_fsm_state30() {
    ap_CS_fsm_state30 = ap_CS_fsm.read()[29];
}

void mnist_fp32::thread_ap_CS_fsm_state306() {
    ap_CS_fsm_state306 = ap_CS_fsm.read()[305];
}

void mnist_fp32::thread_ap_CS_fsm_state307() {
    ap_CS_fsm_state307 = ap_CS_fsm.read()[306];
}

void mnist_fp32::thread_ap_CS_fsm_state308() {
    ap_CS_fsm_state308 = ap_CS_fsm.read()[307];
}

void mnist_fp32::thread_ap_CS_fsm_state309() {
    ap_CS_fsm_state309 = ap_CS_fsm.read()[308];
}

void mnist_fp32::thread_ap_CS_fsm_state310() {
    ap_CS_fsm_state310 = ap_CS_fsm.read()[309];
}

void mnist_fp32::thread_ap_CS_fsm_state315() {
    ap_CS_fsm_state315 = ap_CS_fsm.read()[314];
}

void mnist_fp32::thread_ap_CS_fsm_state316() {
    ap_CS_fsm_state316 = ap_CS_fsm.read()[315];
}

void mnist_fp32::thread_ap_CS_fsm_state317() {
    ap_CS_fsm_state317 = ap_CS_fsm.read()[316];
}

void mnist_fp32::thread_ap_CS_fsm_state318() {
    ap_CS_fsm_state318 = ap_CS_fsm.read()[317];
}

void mnist_fp32::thread_ap_CS_fsm_state319() {
    ap_CS_fsm_state319 = ap_CS_fsm.read()[318];
}

void mnist_fp32::thread_ap_CS_fsm_state320() {
    ap_CS_fsm_state320 = ap_CS_fsm.read()[319];
}

void mnist_fp32::thread_ap_CS_fsm_state321() {
    ap_CS_fsm_state321 = ap_CS_fsm.read()[320];
}

void mnist_fp32::thread_ap_CS_fsm_state322() {
    ap_CS_fsm_state322 = ap_CS_fsm.read()[321];
}

void mnist_fp32::thread_ap_CS_fsm_state323() {
    ap_CS_fsm_state323 = ap_CS_fsm.read()[322];
}

void mnist_fp32::thread_ap_CS_fsm_state324() {
    ap_CS_fsm_state324 = ap_CS_fsm.read()[323];
}

void mnist_fp32::thread_ap_CS_fsm_state325() {
    ap_CS_fsm_state325 = ap_CS_fsm.read()[324];
}

void mnist_fp32::thread_ap_CS_fsm_state326() {
    ap_CS_fsm_state326 = ap_CS_fsm.read()[325];
}

void mnist_fp32::thread_ap_CS_fsm_state327() {
    ap_CS_fsm_state327 = ap_CS_fsm.read()[326];
}

void mnist_fp32::thread_ap_CS_fsm_state328() {
    ap_CS_fsm_state328 = ap_CS_fsm.read()[327];
}

void mnist_fp32::thread_ap_CS_fsm_state329() {
    ap_CS_fsm_state329 = ap_CS_fsm.read()[328];
}

void mnist_fp32::thread_ap_CS_fsm_state33() {
    ap_CS_fsm_state33 = ap_CS_fsm.read()[32];
}

void mnist_fp32::thread_ap_CS_fsm_state330() {
    ap_CS_fsm_state330 = ap_CS_fsm.read()[329];
}

void mnist_fp32::thread_ap_CS_fsm_state331() {
    ap_CS_fsm_state331 = ap_CS_fsm.read()[330];
}

void mnist_fp32::thread_ap_CS_fsm_state336() {
    ap_CS_fsm_state336 = ap_CS_fsm.read()[335];
}

void mnist_fp32::thread_ap_CS_fsm_state337() {
    ap_CS_fsm_state337 = ap_CS_fsm.read()[336];
}

void mnist_fp32::thread_ap_CS_fsm_state338() {
    ap_CS_fsm_state338 = ap_CS_fsm.read()[337];
}

void mnist_fp32::thread_ap_CS_fsm_state339() {
    ap_CS_fsm_state339 = ap_CS_fsm.read()[338];
}

void mnist_fp32::thread_ap_CS_fsm_state34() {
    ap_CS_fsm_state34 = ap_CS_fsm.read()[33];
}

void mnist_fp32::thread_ap_CS_fsm_state340() {
    ap_CS_fsm_state340 = ap_CS_fsm.read()[339];
}

void mnist_fp32::thread_ap_CS_fsm_state341() {
    ap_CS_fsm_state341 = ap_CS_fsm.read()[340];
}

void mnist_fp32::thread_ap_CS_fsm_state342() {
    ap_CS_fsm_state342 = ap_CS_fsm.read()[341];
}

void mnist_fp32::thread_ap_CS_fsm_state343() {
    ap_CS_fsm_state343 = ap_CS_fsm.read()[342];
}

void mnist_fp32::thread_ap_CS_fsm_state344() {
    ap_CS_fsm_state344 = ap_CS_fsm.read()[343];
}

void mnist_fp32::thread_ap_CS_fsm_state345() {
    ap_CS_fsm_state345 = ap_CS_fsm.read()[344];
}

void mnist_fp32::thread_ap_CS_fsm_state346() {
    ap_CS_fsm_state346 = ap_CS_fsm.read()[345];
}

void mnist_fp32::thread_ap_CS_fsm_state347() {
    ap_CS_fsm_state347 = ap_CS_fsm.read()[346];
}

void mnist_fp32::thread_ap_CS_fsm_state348() {
    ap_CS_fsm_state348 = ap_CS_fsm.read()[347];
}

void mnist_fp32::thread_ap_CS_fsm_state349() {
    ap_CS_fsm_state349 = ap_CS_fsm.read()[348];
}

void mnist_fp32::thread_ap_CS_fsm_state350() {
    ap_CS_fsm_state350 = ap_CS_fsm.read()[349];
}

void mnist_fp32::thread_ap_CS_fsm_state351() {
    ap_CS_fsm_state351 = ap_CS_fsm.read()[350];
}

void mnist_fp32::thread_ap_CS_fsm_state356() {
    ap_CS_fsm_state356 = ap_CS_fsm.read()[355];
}

void mnist_fp32::thread_ap_CS_fsm_state357() {
    ap_CS_fsm_state357 = ap_CS_fsm.read()[356];
}

void mnist_fp32::thread_ap_CS_fsm_state358() {
    ap_CS_fsm_state358 = ap_CS_fsm.read()[357];
}

void mnist_fp32::thread_ap_CS_fsm_state359() {
    ap_CS_fsm_state359 = ap_CS_fsm.read()[358];
}

void mnist_fp32::thread_ap_CS_fsm_state360() {
    ap_CS_fsm_state360 = ap_CS_fsm.read()[359];
}

void mnist_fp32::thread_ap_CS_fsm_state361() {
    ap_CS_fsm_state361 = ap_CS_fsm.read()[360];
}

void mnist_fp32::thread_ap_CS_fsm_state362() {
    ap_CS_fsm_state362 = ap_CS_fsm.read()[361];
}

void mnist_fp32::thread_ap_CS_fsm_state363() {
    ap_CS_fsm_state363 = ap_CS_fsm.read()[362];
}

void mnist_fp32::thread_ap_CS_fsm_state364() {
    ap_CS_fsm_state364 = ap_CS_fsm.read()[363];
}

void mnist_fp32::thread_ap_CS_fsm_state365() {
    ap_CS_fsm_state365 = ap_CS_fsm.read()[364];
}

void mnist_fp32::thread_ap_CS_fsm_state366() {
    ap_CS_fsm_state366 = ap_CS_fsm.read()[365];
}

void mnist_fp32::thread_ap_CS_fsm_state367() {
    ap_CS_fsm_state367 = ap_CS_fsm.read()[366];
}

void mnist_fp32::thread_ap_CS_fsm_state372() {
    ap_CS_fsm_state372 = ap_CS_fsm.read()[371];
}

void mnist_fp32::thread_ap_CS_fsm_state373() {
    ap_CS_fsm_state373 = ap_CS_fsm.read()[372];
}

void mnist_fp32::thread_ap_CS_fsm_state374() {
    ap_CS_fsm_state374 = ap_CS_fsm.read()[373];
}

void mnist_fp32::thread_ap_CS_fsm_state377() {
    ap_CS_fsm_state377 = ap_CS_fsm.read()[376];
}

void mnist_fp32::thread_ap_CS_fsm_state378() {
    ap_CS_fsm_state378 = ap_CS_fsm.read()[377];
}

void mnist_fp32::thread_ap_CS_fsm_state38() {
    ap_CS_fsm_state38 = ap_CS_fsm.read()[37];
}

void mnist_fp32::thread_ap_CS_fsm_state382() {
    ap_CS_fsm_state382 = ap_CS_fsm.read()[381];
}

void mnist_fp32::thread_ap_CS_fsm_state383() {
    ap_CS_fsm_state383 = ap_CS_fsm.read()[382];
}

void mnist_fp32::thread_ap_CS_fsm_state384() {
    ap_CS_fsm_state384 = ap_CS_fsm.read()[383];
}

void mnist_fp32::thread_ap_CS_fsm_state385() {
    ap_CS_fsm_state385 = ap_CS_fsm.read()[384];
}

void mnist_fp32::thread_ap_CS_fsm_state386() {
    ap_CS_fsm_state386 = ap_CS_fsm.read()[385];
}

void mnist_fp32::thread_ap_CS_fsm_state387() {
    ap_CS_fsm_state387 = ap_CS_fsm.read()[386];
}

void mnist_fp32::thread_ap_CS_fsm_state388() {
    ap_CS_fsm_state388 = ap_CS_fsm.read()[387];
}

void mnist_fp32::thread_ap_CS_fsm_state39() {
    ap_CS_fsm_state39 = ap_CS_fsm.read()[38];
}

void mnist_fp32::thread_ap_CS_fsm_state392() {
    ap_CS_fsm_state392 = ap_CS_fsm.read()[391];
}

void mnist_fp32::thread_ap_CS_fsm_state393() {
    ap_CS_fsm_state393 = ap_CS_fsm.read()[392];
}

void mnist_fp32::thread_ap_CS_fsm_state394() {
    ap_CS_fsm_state394 = ap_CS_fsm.read()[393];
}

void mnist_fp32::thread_ap_CS_fsm_state4() {
    ap_CS_fsm_state4 = ap_CS_fsm.read()[3];
}

void mnist_fp32::thread_ap_CS_fsm_state40() {
    ap_CS_fsm_state40 = ap_CS_fsm.read()[39];
}

void mnist_fp32::thread_ap_CS_fsm_state41() {
    ap_CS_fsm_state41 = ap_CS_fsm.read()[40];
}

void mnist_fp32::thread_ap_CS_fsm_state411() {
    ap_CS_fsm_state411 = ap_CS_fsm.read()[410];
}

void mnist_fp32::thread_ap_CS_fsm_state412() {
    ap_CS_fsm_state412 = ap_CS_fsm.read()[411];
}

void mnist_fp32::thread_ap_CS_fsm_state416() {
    ap_CS_fsm_state416 = ap_CS_fsm.read()[415];
}

void mnist_fp32::thread_ap_CS_fsm_state417() {
    ap_CS_fsm_state417 = ap_CS_fsm.read()[416];
}

void mnist_fp32::thread_ap_CS_fsm_state418() {
    ap_CS_fsm_state418 = ap_CS_fsm.read()[417];
}

void mnist_fp32::thread_ap_CS_fsm_state419() {
    ap_CS_fsm_state419 = ap_CS_fsm.read()[418];
}

void mnist_fp32::thread_ap_CS_fsm_state42() {
    ap_CS_fsm_state42 = ap_CS_fsm.read()[41];
}

void mnist_fp32::thread_ap_CS_fsm_state420() {
    ap_CS_fsm_state420 = ap_CS_fsm.read()[419];
}

void mnist_fp32::thread_ap_CS_fsm_state424() {
    ap_CS_fsm_state424 = ap_CS_fsm.read()[423];
}

void mnist_fp32::thread_ap_CS_fsm_state425() {
    ap_CS_fsm_state425 = ap_CS_fsm.read()[424];
}

void mnist_fp32::thread_ap_CS_fsm_state426() {
    ap_CS_fsm_state426 = ap_CS_fsm.read()[425];
}

void mnist_fp32::thread_ap_CS_fsm_state43() {
    ap_CS_fsm_state43 = ap_CS_fsm.read()[42];
}

void mnist_fp32::thread_ap_CS_fsm_state44() {
    ap_CS_fsm_state44 = ap_CS_fsm.read()[43];
}

void mnist_fp32::thread_ap_CS_fsm_state443() {
    ap_CS_fsm_state443 = ap_CS_fsm.read()[442];
}

void mnist_fp32::thread_ap_CS_fsm_state444() {
    ap_CS_fsm_state444 = ap_CS_fsm.read()[443];
}

void mnist_fp32::thread_ap_CS_fsm_state45() {
    ap_CS_fsm_state45 = ap_CS_fsm.read()[44];
}

void mnist_fp32::thread_ap_CS_fsm_state46() {
    ap_CS_fsm_state46 = ap_CS_fsm.read()[45];
}

void mnist_fp32::thread_ap_CS_fsm_state47() {
    ap_CS_fsm_state47 = ap_CS_fsm.read()[46];
}

void mnist_fp32::thread_ap_CS_fsm_state474() {
    ap_CS_fsm_state474 = ap_CS_fsm.read()[473];
}

void mnist_fp32::thread_ap_CS_fsm_state475() {
    ap_CS_fsm_state475 = ap_CS_fsm.read()[474];
}

void mnist_fp32::thread_ap_CS_fsm_state476() {
    ap_CS_fsm_state476 = ap_CS_fsm.read()[475];
}

void mnist_fp32::thread_ap_CS_fsm_state477() {
    ap_CS_fsm_state477 = ap_CS_fsm.read()[476];
}

void mnist_fp32::thread_ap_CS_fsm_state478() {
    ap_CS_fsm_state478 = ap_CS_fsm.read()[477];
}

void mnist_fp32::thread_ap_CS_fsm_state479() {
    ap_CS_fsm_state479 = ap_CS_fsm.read()[478];
}

void mnist_fp32::thread_ap_CS_fsm_state48() {
    ap_CS_fsm_state48 = ap_CS_fsm.read()[47];
}

void mnist_fp32::thread_ap_CS_fsm_state480() {
    ap_CS_fsm_state480 = ap_CS_fsm.read()[479];
}

void mnist_fp32::thread_ap_CS_fsm_state49() {
    ap_CS_fsm_state49 = ap_CS_fsm.read()[48];
}

void mnist_fp32::thread_ap_CS_fsm_state5() {
    ap_CS_fsm_state5 = ap_CS_fsm.read()[4];
}

void mnist_fp32::thread_ap_CS_fsm_state50() {
    ap_CS_fsm_state50 = ap_CS_fsm.read()[49];
}

void mnist_fp32::thread_ap_CS_fsm_state51() {
    ap_CS_fsm_state51 = ap_CS_fsm.read()[50];
}

void mnist_fp32::thread_ap_CS_fsm_state68() {
    ap_CS_fsm_state68 = ap_CS_fsm.read()[67];
}

void mnist_fp32::thread_ap_CS_fsm_state69() {
    ap_CS_fsm_state69 = ap_CS_fsm.read()[68];
}

void mnist_fp32::thread_ap_CS_fsm_state70() {
    ap_CS_fsm_state70 = ap_CS_fsm.read()[69];
}

void mnist_fp32::thread_ap_CS_fsm_state71() {
    ap_CS_fsm_state71 = ap_CS_fsm.read()[70];
}

void mnist_fp32::thread_ap_CS_fsm_state72() {
    ap_CS_fsm_state72 = ap_CS_fsm.read()[71];
}

void mnist_fp32::thread_ap_CS_fsm_state77() {
    ap_CS_fsm_state77 = ap_CS_fsm.read()[76];
}

void mnist_fp32::thread_ap_CS_fsm_state78() {
    ap_CS_fsm_state78 = ap_CS_fsm.read()[77];
}

void mnist_fp32::thread_ap_CS_fsm_state79() {
    ap_CS_fsm_state79 = ap_CS_fsm.read()[78];
}

void mnist_fp32::thread_ap_CS_fsm_state80() {
    ap_CS_fsm_state80 = ap_CS_fsm.read()[79];
}

void mnist_fp32::thread_ap_CS_fsm_state81() {
    ap_CS_fsm_state81 = ap_CS_fsm.read()[80];
}

void mnist_fp32::thread_ap_CS_fsm_state82() {
    ap_CS_fsm_state82 = ap_CS_fsm.read()[81];
}

void mnist_fp32::thread_ap_CS_fsm_state83() {
    ap_CS_fsm_state83 = ap_CS_fsm.read()[82];
}

void mnist_fp32::thread_ap_CS_fsm_state84() {
    ap_CS_fsm_state84 = ap_CS_fsm.read()[83];
}

void mnist_fp32::thread_ap_CS_fsm_state85() {
    ap_CS_fsm_state85 = ap_CS_fsm.read()[84];
}

void mnist_fp32::thread_ap_CS_fsm_state86() {
    ap_CS_fsm_state86 = ap_CS_fsm.read()[85];
}

void mnist_fp32::thread_ap_CS_fsm_state87() {
    ap_CS_fsm_state87 = ap_CS_fsm.read()[86];
}

void mnist_fp32::thread_ap_CS_fsm_state88() {
    ap_CS_fsm_state88 = ap_CS_fsm.read()[87];
}

void mnist_fp32::thread_ap_CS_fsm_state89() {
    ap_CS_fsm_state89 = ap_CS_fsm.read()[88];
}

void mnist_fp32::thread_ap_CS_fsm_state90() {
    ap_CS_fsm_state90 = ap_CS_fsm.read()[89];
}

void mnist_fp32::thread_ap_CS_fsm_state91() {
    ap_CS_fsm_state91 = ap_CS_fsm.read()[90];
}

void mnist_fp32::thread_ap_CS_fsm_state92() {
    ap_CS_fsm_state92 = ap_CS_fsm.read()[91];
}

void mnist_fp32::thread_ap_CS_fsm_state93() {
    ap_CS_fsm_state93 = ap_CS_fsm.read()[92];
}

void mnist_fp32::thread_ap_CS_fsm_state98() {
    ap_CS_fsm_state98 = ap_CS_fsm.read()[97];
}

void mnist_fp32::thread_ap_CS_fsm_state99() {
    ap_CS_fsm_state99 = ap_CS_fsm.read()[98];
}

void mnist_fp32::thread_ap_done() {
    if ((esl_seteq<1,1,1>(tmp_396_fu_13823_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state477.read()))) {
        ap_done = ap_const_logic_1;
    } else {
        ap_done = ap_const_logic_0;
    }
}

void mnist_fp32::thread_ap_idle() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state1.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void mnist_fp32::thread_ap_phi_mux_compute7_phi_fu_2538_p4() {
    ap_phi_mux_compute7_phi_fu_2538_p4 = compute7_reg_2534.read();
}

void mnist_fp32::thread_ap_phi_mux_p_9_phi_fu_1277_p4() {
    ap_phi_mux_p_9_phi_fu_1277_p4 = p_9_reg_1273.read();
}

void mnist_fp32::thread_ap_phi_mux_p_s_phi_fu_1101_p4() {
    ap_phi_mux_p_s_phi_fu_1101_p4 = p_s_reg_1097.read();
}

void mnist_fp32::thread_ap_ready() {
    if ((esl_seteq<1,1,1>(tmp_396_fu_13823_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state477.read()))) {
        ap_ready = ap_const_logic_1;
    } else {
        ap_ready = ap_const_logic_0;
    }
}

void mnist_fp32::thread_args01_V_fu_5367_p2() {
    args01_V_fu_5367_p2 = (!p_18_reg_1419.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_18_reg_1419.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_args02_V_fu_7000_p2() {
    args02_V_fu_7000_p2 = (!p_35_reg_1695.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_35_reg_1695.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_args03_V_fu_8946_p2() {
    args03_V_fu_8946_p2 = (!p_49_reg_1893.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_49_reg_1893.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_args04_V_fu_10550_p2() {
    args04_V_fu_10550_p2 = (!p_67_reg_2167.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_67_reg_2167.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_args05_V_fu_12483_p2() {
    args05_V_fu_12483_p2 = (!p_81_reg_2363.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_81_reg_2363.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_args0_V_fu_3283_p2() {
    args0_V_fu_3283_p2 = (!p_6_reg_1218.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_6_reg_1218.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_args11_V_fu_5413_p2() {
    args11_V_fu_5413_p2 = (!p_22_reg_1430.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_22_reg_1430.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_args12_V_fu_7046_p2() {
    args12_V_fu_7046_p2 = (!p_40_reg_1706.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_40_reg_1706.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_args13_V_fu_8992_p2() {
    args13_V_fu_8992_p2 = (!p_54_reg_1904.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_54_reg_1904.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_args14_V_fu_10588_p2() {
    args14_V_fu_10588_p2 = (!p_72_reg_2178.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_72_reg_2178.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_args15_V_fu_12521_p2() {
    args15_V_fu_12521_p2 = (!p_83_reg_2374.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_83_reg_2374.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_args1_V_fu_3329_p2() {
    args1_V_fu_3329_p2 = (!p_10_reg_1229.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_10_reg_1229.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_args21_V_fu_5464_p2() {
    args21_V_fu_5464_p2 = (!p_27_reg_1441.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_27_reg_1441.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_args22_V_fu_7097_p2() {
    args22_V_fu_7097_p2 = (!p_46_reg_1717.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_46_reg_1717.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_args23_V_fu_9043_p2() {
    args23_V_fu_9043_p2 = (!p_59_reg_1915.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_59_reg_1915.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_args24_V_fu_10631_p2() {
    args24_V_fu_10631_p2 = (!p_78_reg_2189.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_78_reg_2189.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_args25_V_fu_12564_p2() {
    args25_V_fu_12564_p2 = (!p_86_reg_2385.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_86_reg_2385.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_args2_V_fu_3380_p2() {
    args2_V_fu_3380_p2 = (!p_15_reg_1240.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_15_reg_1240.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_c1_V_fu_9378_p2() {
    c1_V_fu_9378_p2 = (!p_53_reg_1926.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_53_reg_1926.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_c2_V_fu_12899_p2() {
    c2_V_fu_12899_p2 = (!p_82_reg_2396.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_82_reg_2396.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_c3_V_fu_13215_p2() {
    c3_V_fu_13215_p2 = (!p_84_reg_2454.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_84_reg_2454.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_c_V_fu_5799_p2() {
    c_V_fu_5799_p2 = (!p_21_reg_1452.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_21_reg_1452.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_compute6_to_int_fu_13715_p1() {
    compute6_to_int_fu_13715_p1 = compute6_reg_2500.read();
}

void mnist_fp32::thread_conv1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state41.read())) {
        conv1_0_address0 =  (sc_lv<12>) (tmp_532_cast_fu_3395_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state27.read())) {
        conv1_0_address0 =  (sc_lv<12>) (tmp_533_cast_fu_3222_p1.read());
    } else {
        conv1_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp32::thread_conv1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state27.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state41.read()))) {
        conv1_0_ce0 = ap_const_logic_1;
    } else {
        conv1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv1_0_load_to_int_fu_3400_p1() {
    conv1_0_load_to_int_fu_3400_p1 = conv1_0_load_reg_14232.read();
}

void mnist_fp32::thread_conv1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond12_fu_3140_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state27.read()))) {
        conv1_0_we0 = ap_const_logic_1;
    } else {
        conv1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv2_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state102.read())) {
        conv2_0_address0 =  (sc_lv<12>) (tmp_584_cast_fu_5479_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state99.read())) {
        conv2_0_address0 =  (sc_lv<12>) (tmp_585_cast_fu_5357_p1.read());
    } else {
        conv2_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp32::thread_conv2_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state102.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state99.read()))) {
        conv2_0_ce0 = ap_const_logic_1;
    } else {
        conv2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv2_0_d0() {
    conv2_0_d0 = (!tmp_58_reg_14713.read()[0].is_01())? sc_lv<32>(): ((tmp_58_reg_14713.read()[0].to_bool())? ap_const_lv32_0: f_2_fu_5345_p1.read());
}

void mnist_fp32::thread_conv2_0_load_to_int_fu_5484_p1() {
    conv2_0_load_to_int_fu_5484_p1 = conv2_0_load_reg_14792.read();
}

void mnist_fp32::thread_conv2_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state99.read())) {
        conv2_0_we0 = ap_const_logic_1;
    } else {
        conv2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv3_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state163.read())) {
        conv3_0_address0 =  (sc_lv<11>) (tmp_661_cast_fu_7112_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state148.read())) {
        conv3_0_address0 =  (sc_lv<11>) (tmp_662_cast_fu_6886_p1.read());
    } else {
        conv3_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp32::thread_conv3_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state148.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state163.read()))) {
        conv3_0_ce0 = ap_const_logic_1;
    } else {
        conv3_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv3_0_load_to_int_fu_7117_p1() {
    conv3_0_load_to_int_fu_7117_p1 = conv3_0_load_reg_15272.read();
}

void mnist_fp32::thread_conv3_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond40_fu_6834_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state148.read()))) {
        conv3_0_we0 = ap_const_logic_1;
    } else {
        conv3_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv4_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state222.read())) {
        conv4_0_address0 =  (sc_lv<11>) (tmp_713_cast_fu_9058_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state219.read())) {
        conv4_0_address0 =  (sc_lv<11>) (tmp_714_cast_fu_8936_p1.read());
    } else {
        conv4_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp32::thread_conv4_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state222.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state219.read()))) {
        conv4_0_ce0 = ap_const_logic_1;
    } else {
        conv4_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv4_0_d0() {
    conv4_0_d0 = (!tmp_189_reg_15750.read()[0].is_01())? sc_lv<32>(): ((tmp_189_reg_15750.read()[0].to_bool())? ap_const_lv32_0: f_6_fu_8924_p1.read());
}

void mnist_fp32::thread_conv4_0_load_to_int_fu_9063_p1() {
    conv4_0_load_to_int_fu_9063_p1 = conv4_0_load_reg_15829.read();
}

void mnist_fp32::thread_conv4_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state219.read())) {
        conv4_0_we0 = ap_const_logic_1;
    } else {
        conv4_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv5_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state282.read())) {
        conv5_0_address0 =  (sc_lv<10>) (tmp_771_cast_fu_10646_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state267.read())) {
        conv5_0_address0 =  (sc_lv<10>) (tmp_772_cast_fu_10411_p1.read());
    } else {
        conv5_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp32::thread_conv5_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state267.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state282.read()))) {
        conv5_0_ce0 = ap_const_logic_1;
    } else {
        conv5_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv5_0_load_to_int_fu_10651_p1() {
    conv5_0_load_to_int_fu_10651_p1 = conv5_0_load_reg_16324.read();
}

void mnist_fp32::thread_conv5_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond64_fu_10337_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state267.read()))) {
        conv5_0_we0 = ap_const_logic_1;
    } else {
        conv5_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv6_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state340.read())) {
        conv6_0_address0 =  (sc_lv<10>) (tmp_816_cast_fu_12579_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state337.read())) {
        conv6_0_address0 =  (sc_lv<10>) (tmp_817_cast_fu_12473_p1.read());
    } else {
        conv6_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp32::thread_conv6_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state340.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state337.read()))) {
        conv6_0_ce0 = ap_const_logic_1;
    } else {
        conv6_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_conv6_0_d0() {
    conv6_0_d0 = (!tmp_347_reg_16822.read()[0].is_01())? sc_lv<32>(): ((tmp_347_reg_16822.read()[0].to_bool())? ap_const_lv32_0: f_12_fu_12461_p1.read());
}

void mnist_fp32::thread_conv6_0_load_to_int_fu_12584_p1() {
    conv6_0_load_to_int_fu_12584_p1 = conv6_0_load_reg_16896.read();
}

void mnist_fp32::thread_conv6_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state337.read())) {
        conv6_0_we0 = ap_const_logic_1;
    } else {
        conv6_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_dense1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state418.read())) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_399_fu_13818_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state386.read())) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_336_fu_13801_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state383.read())) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_321_fu_13692_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state364.read())) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_413_reg_17158.read());
    } else {
        dense1_0_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void mnist_fp32::thread_dense1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state386.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state364.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state383.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state418.read()))) {
        dense1_0_ce0 = ap_const_logic_1;
    } else {
        dense1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_dense1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond88_fu_13509_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state364.read()))) {
        dense1_0_we0 = ap_const_logic_1;
    } else {
        dense1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_exitcond10_fu_4388_p2() {
    exitcond10_fu_4388_p2 = (!p_3_reg_1326.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_1326.read() == ap_const_lv5_1C);
}

void mnist_fp32::thread_exitcond11_fu_3323_p2() {
    exitcond11_fu_3323_p2 = (!p_10_reg_1229.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_10_reg_1229.read() == ap_const_lv5_1C);
}

void mnist_fp32::thread_exitcond12_fu_3140_p2() {
    exitcond12_fu_3140_p2 = (!p_11_reg_1171.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_11_reg_1171.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond13_fu_6276_p2() {
    exitcond13_fu_6276_p2 = (!p_12_reg_1532.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_12_reg_1532.read() == ap_const_lv3_4);
}

void mnist_fp32::thread_exitcond14_fu_3849_p2() {
    exitcond14_fu_3849_p2 = (!p_13_reg_1285.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_13_reg_1285.read() == ap_const_lv5_1E);
}

void mnist_fp32::thread_exitcond15_fu_6713_p2() {
    exitcond15_fu_6713_p2 = (!p_14_reg_1589.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1589.read() == ap_const_lv4_8);
}

void mnist_fp32::thread_exitcond16_fu_3374_p2() {
    exitcond16_fu_3374_p2 = (!p_15_reg_1240.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_15_reg_1240.read() == ap_const_lv5_1C);
}

void mnist_fp32::thread_exitcond17_fu_3227_p2() {
    exitcond17_fu_3227_p2 = (!p_16_reg_1195.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_16_reg_1195.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond18_fu_4439_p2() {
    exitcond18_fu_4439_p2 = (!p_17_reg_1338.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_17_reg_1338.read() == ap_const_lv5_1C);
}

void mnist_fp32::thread_exitcond19_fu_5361_p2() {
    exitcond19_fu_5361_p2 = (!p_18_reg_1419.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_18_reg_1419.read() == ap_const_lv3_4);
}

void mnist_fp32::thread_exitcond20_fu_6300_p2() {
    exitcond20_fu_6300_p2 = (!p_19_reg_1554.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_19_reg_1554.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond21_fu_7436_p2() {
    exitcond21_fu_7436_p2 = (!p_20_reg_1728.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_20_reg_1728.read() == ap_const_lv4_8);
}

void mnist_fp32::thread_exitcond22_fu_5793_p2() {
    exitcond22_fu_5793_p2 = (!p_21_reg_1452.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_21_reg_1452.read() == ap_const_lv3_4);
}

void mnist_fp32::thread_exitcond23_fu_5407_p2() {
    exitcond23_fu_5407_p2 = (!p_22_reg_1430.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_22_reg_1430.read() == ap_const_lv5_1C);
}

void mnist_fp32::thread_exitcond24_fu_4451_p2() {
    exitcond24_fu_4451_p2 = (!p_23_reg_1362.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_23_reg_1362.read() == ap_const_lv3_4);
}

void mnist_fp32::thread_exitcond25_fu_6771_p2() {
    exitcond25_fu_6771_p2 = (!p_24_reg_1600.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_24_reg_1600.read() == ap_const_lv4_E);
}

void mnist_fp32::thread_exitcond26_fu_7982_p2() {
    exitcond26_fu_7982_p2 = (!p_25_reg_1788.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_25_reg_1788.read() == ap_const_lv4_8);
}

void mnist_fp32::thread_exitcond27_fu_5873_p2() {
    exitcond27_fu_5873_p2 = (!p_26_reg_1463.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_26_reg_1463.read() == ap_const_lv4_E);
}

void mnist_fp32::thread_exitcond28_fu_5458_p2() {
    exitcond28_fu_5458_p2 = (!p_27_reg_1441.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_27_reg_1441.read() == ap_const_lv5_1C);
}

void mnist_fp32::thread_exitcond29_fu_4534_p2() {
    exitcond29_fu_4534_p2 = (!p_28_reg_1385.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_28_reg_1385.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond2_fu_3013_p2() {
    exitcond2_fu_3013_p2 = (!p_1_reg_1136.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_1_reg_1136.read() == ap_const_lv3_4);
}

void mnist_fp32::thread_exitcond30_fu_6388_p2() {
    exitcond30_fu_6388_p2 = (!p_29_reg_1565.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_29_reg_1565.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond31_fu_7460_p2() {
    exitcond31_fu_7460_p2 = (!p_30_reg_1750.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_30_reg_1750.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond32_fu_6822_p2() {
    exitcond32_fu_6822_p2 = (!p_31_reg_1612.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_31_reg_1612.read() == ap_const_lv4_E);
}

void mnist_fp32::thread_exitcond33_fu_5932_p2() {
    exitcond33_fu_5932_p2 = (!p_32_reg_1474.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_32_reg_1474.read() == ap_const_lv4_E);
}

void mnist_fp32::thread_exitcond34_fu_4626_p2() {
    exitcond34_fu_4626_p2 = (!p_33_reg_1408.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_33_reg_1408.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond35_fu_8040_p2() {
    exitcond35_fu_8040_p2 = (!p_34_reg_1799.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_34_reg_1799.read() == ap_const_lv4_E);
}

void mnist_fp32::thread_exitcond36_fu_6994_p2() {
    exitcond36_fu_6994_p2 = (!p_35_reg_1695.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_35_reg_1695.read() == ap_const_lv4_8);
}

void mnist_fp32::thread_exitcond37_fu_9839_p2() {
    exitcond37_fu_9839_p2 = (!p_36_reg_2006.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_36_reg_2006.read() == ap_const_lv4_8);
}

void mnist_fp32::thread_exitcond38_fu_5966_p2() {
    exitcond38_fu_5966_p2 = (!p_37_reg_1498.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_37_reg_1498.read() == ap_const_lv2_2);
}

void mnist_fp32::thread_exitcond39_fu_7548_p2() {
    exitcond39_fu_7548_p2 = (!p_38_reg_1761.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_38_reg_1761.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond3_fu_2758_p2() {
    exitcond3_fu_2758_p2 = (!p_2_reg_1109.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_2_reg_1109.read() == ap_const_lv5_1E);
}

void mnist_fp32::thread_exitcond40_fu_6834_p2() {
    exitcond40_fu_6834_p2 = (!p_39_reg_1624.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_39_reg_1624.read() == ap_const_lv3_4);
}

void mnist_fp32::thread_exitcond41_fu_7040_p2() {
    exitcond41_fu_7040_p2 = (!p_40_reg_1706.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_40_reg_1706.read() == ap_const_lv4_E);
}

void mnist_fp32::thread_exitcond42_fu_10236_p2() {
    exitcond42_fu_10236_p2 = (!p_41_reg_2063.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_41_reg_2063.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond43_fu_6026_p2() {
    exitcond43_fu_6026_p2 = (!p_42_reg_1521.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_42_reg_1521.read() == ap_const_lv2_2);
}

void mnist_fp32::thread_exitcond44_fu_8091_p2() {
    exitcond44_fu_8091_p2 = (!p_43_reg_1811.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_43_reg_1811.read() == ap_const_lv4_E);
}

void mnist_fp32::thread_exitcond45_fu_9873_p2() {
    exitcond45_fu_9873_p2 = (!p_44_reg_2028.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_44_reg_2028.read() == ap_const_lv4_9);
}

void mnist_fp32::thread_exitcond46_fu_6891_p2() {
    exitcond46_fu_6891_p2 = (!p_45_reg_1649.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_45_reg_1649.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond47_fu_7091_p2() {
    exitcond47_fu_7091_p2 = (!p_46_reg_1717.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_46_reg_1717.read() == ap_const_lv4_E);
}

void mnist_fp32::thread_exitcond48_fu_10970_p2() {
    exitcond48_fu_10970_p2 = (!p_47_reg_2200.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_47_reg_2200.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond49_fu_10274_p2() {
    exitcond49_fu_10274_p2 = (!p_48_reg_2074.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_48_reg_2074.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond50_fu_8940_p2() {
    exitcond50_fu_8940_p2 = (!p_49_reg_1893.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_49_reg_1893.read() == ap_const_lv4_8);
}

void mnist_fp32::thread_exitcond51_fu_8103_p2() {
    exitcond51_fu_8103_p2 = (!p_50_reg_1835.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_50_reg_1835.read() == ap_const_lv4_8);
}

void mnist_fp32::thread_exitcond52_fu_6944_p2() {
    exitcond52_fu_6944_p2 = (!p_51_reg_1672.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_51_reg_1672.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond53_fu_11480_p2() {
    exitcond53_fu_11480_p2 = (!p_52_reg_2260.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_52_reg_2260.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond54_fu_9372_p2() {
    exitcond54_fu_9372_p2 = (!p_53_reg_1926.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_53_reg_1926.read() == ap_const_lv4_8);
}

void mnist_fp32::thread_exitcond55_fu_8986_p2() {
    exitcond55_fu_8986_p2 = (!p_54_reg_1904.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_54_reg_1904.read() == ap_const_lv4_E);
}

void mnist_fp32::thread_exitcond56_fu_9945_p2() {
    exitcond56_fu_9945_p2 = (!p_55_reg_2039.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_55_reg_2039.read() == ap_const_lv4_9);
}

void mnist_fp32::thread_exitcond57_fu_11004_p2() {
    exitcond57_fu_11004_p2 = (!p_56_reg_2222.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_56_reg_2222.read() == ap_const_lv4_9);
}

void mnist_fp32::thread_exitcond58_fu_10321_p2() {
    exitcond58_fu_10321_p2 = (!p_57_reg_2085.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_57_reg_2085.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond59_fu_9444_p2() {
    exitcond59_fu_9444_p2 = (!p_58_reg_1937.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_58_reg_1937.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond5_fu_3077_p2() {
    exitcond5_fu_3077_p2 = (!p_4_reg_1147.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_4_reg_1147.read() == ap_const_lv5_1C);
}

void mnist_fp32::thread_exitcond60_fu_9037_p2() {
    exitcond60_fu_9037_p2 = (!p_59_reg_1915.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_59_reg_1915.read() == ap_const_lv4_E);
}

void mnist_fp32::thread_exitcond61_fu_8152_p2() {
    exitcond61_fu_8152_p2 = (!p_60_reg_1859.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_60_reg_1859.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond62_fu_11530_p2() {
    exitcond62_fu_11530_p2 = (!p_61_reg_2271.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_61_reg_2271.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond63_fu_11080_p2() {
    exitcond63_fu_11080_p2 = (!p_62_reg_2233.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_62_reg_2233.read() == ap_const_lv4_9);
}

void mnist_fp32::thread_exitcond64_fu_10337_p2() {
    exitcond64_fu_10337_p2 = (!p_63_reg_2097.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_63_reg_2097.read() == ap_const_lv4_8);
}

void mnist_fp32::thread_exitcond65_fu_9495_p2() {
    exitcond65_fu_9495_p2 = (!p_64_reg_1948.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_64_reg_1948.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond66_fu_8205_p2() {
    exitcond66_fu_8205_p2 = (!p_65_reg_1882.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_65_reg_1882.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond67_fu_11577_p2() {
    exitcond67_fu_11577_p2 = (!p_66_reg_2282.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_66_reg_2282.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond68_fu_10544_p2() {
    exitcond68_fu_10544_p2 = (!p_67_reg_2167.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_67_reg_2167.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond69_fu_13680_p2() {
    exitcond69_fu_13680_p2 = (!p_68_reg_2512.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_68_reg_2512.read() == ap_const_lv4_A);
}

void mnist_fp32::thread_exitcond6_fu_3719_p2() {
    exitcond6_fu_3719_p2 = (!p_5_reg_1251.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_5_reg_1251.read() == ap_const_lv3_4);
}

void mnist_fp32::thread_exitcond70_fu_9529_p2() {
    exitcond70_fu_9529_p2 = (!p_69_reg_1972.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_69_reg_1972.read() == ap_const_lv2_2);
}

void mnist_fp32::thread_exitcond71_fu_10416_p2() {
    exitcond71_fu_10416_p2 = (!p_70_reg_2121.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_70_reg_2121.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond72_fu_13789_p2() {
    exitcond72_fu_13789_p2 = (!p_71_reg_2523.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_71_reg_2523.read() == ap_const_lv4_A);
}

void mnist_fp32::thread_exitcond73_fu_10582_p2() {
    exitcond73_fu_10582_p2 = (!p_72_reg_2178.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_72_reg_2178.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond74_fu_11593_p2() {
    exitcond74_fu_11593_p2 = (!p_73_reg_2306.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_73_reg_2306.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond75_fu_9589_p2() {
    exitcond75_fu_9589_p2 = (!p_74_reg_1995.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_74_reg_1995.read() == ap_const_lv2_2);
}

void mnist_fp32::thread_exitcond76_fu_10495_p2() {
    exitcond76_fu_10495_p2 = (!p_75_reg_2144.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_75_reg_2144.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond77_fu_11673_p2() {
    exitcond77_fu_11673_p2 = (!p_77_reg_2329.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_77_reg_2329.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond78_fu_11752_p2() {
    exitcond78_fu_11752_p2 = (!p_80_reg_2352.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_80_reg_2352.read() == ap_const_lv2_3);
}

void mnist_fp32::thread_exitcond79_fu_10625_p2() {
    exitcond79_fu_10625_p2 = (!p_78_reg_2189.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_78_reg_2189.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond7_fu_3277_p2() {
    exitcond7_fu_3277_p2 = (!p_6_reg_1218.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_6_reg_1218.read() == ap_const_lv3_4);
}

void mnist_fp32::thread_exitcond80_fu_12477_p2() {
    exitcond80_fu_12477_p2 = (!p_81_reg_2363.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_81_reg_2363.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond81_fu_12893_p2() {
    exitcond81_fu_12893_p2 = (!p_82_reg_2396.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_82_reg_2396.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond82_fu_12515_p2() {
    exitcond82_fu_12515_p2 = (!p_83_reg_2374.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_83_reg_2374.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond83_fu_13209_p2() {
    exitcond83_fu_13209_p2 = (!p_84_reg_2454.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_84_reg_2454.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond84_fu_12935_p2() {
    exitcond84_fu_12935_p2 = (!p_85_reg_2420.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_85_reg_2420.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond85_fu_12558_p2() {
    exitcond85_fu_12558_p2 = (!p_86_reg_2385.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_86_reg_2385.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond86_fu_13489_p2() {
    exitcond86_fu_13489_p2 = (!p_87_reg_2465.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_87_reg_2465.read() == ap_const_lv4_A);
}

void mnist_fp32::thread_exitcond87_fu_12978_p2() {
    exitcond87_fu_12978_p2 = (!p_88_reg_2443.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_88_reg_2443.read() == ap_const_lv3_7);
}

void mnist_fp32::thread_exitcond88_fu_13509_p2() {
    exitcond88_fu_13509_p2 = (!p_89_reg_2476.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_89_reg_2476.read() == ap_const_lv5_10);
}

void mnist_fp32::thread_exitcond8_fu_3128_p2() {
    exitcond8_fu_3128_p2 = (!p_7_reg_1159.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_7_reg_1159.read() == ap_const_lv5_1C);
}

void mnist_fp32::thread_exitcond9_fu_4338_p2() {
    exitcond9_fu_4338_p2 = (!p_8_reg_1315.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_8_reg_1315.read() == ap_const_lv3_4);
}

void mnist_fp32::thread_exitcond_fu_13806_p2() {
    exitcond_fu_13806_p2 = (!p_76_reg_2546.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_76_reg_2546.read() == ap_const_lv4_A);
}

void mnist_fp32::thread_f_10_fu_11462_p1() {
    f_10_fu_11462_p1 = p_Result_26_fu_11451_p5.read();
}

void mnist_fp32::thread_f_12_fu_12461_p1() {
    f_12_fu_12461_p1 = p_Result_48_fu_12450_p5.read();
}

void mnist_fp32::thread_f_14_fu_13108_p1() {
    f_14_fu_13108_p1 = p_Result_43_fu_13097_p5.read();
}

void mnist_fp32::thread_f_16_fu_13669_p1() {
    f_16_fu_13669_p1 = p_Result_46_fu_13658_p5.read();
}

void mnist_fp32::thread_f_2_fu_5345_p1() {
    f_2_fu_5345_p1 = p_Result_13_fu_5334_p5.read();
}

void mnist_fp32::thread_f_4_fu_6165_p1() {
    f_4_fu_6165_p1 = p_Result_16_fu_6154_p5.read();
}

void mnist_fp32::thread_f_5_fu_7964_p1() {
    f_5_fu_7964_p1 = p_Result_15_fu_7953_p5.read();
}

void mnist_fp32::thread_f_6_fu_8924_p1() {
    f_6_fu_8924_p1 = p_Result_24_fu_8913_p5.read();
}

void mnist_fp32::thread_f_8_fu_9728_p1() {
    f_8_fu_9728_p1 = p_Result_32_fu_9717_p5.read();
}

void mnist_fp32::thread_f_fu_4320_p1() {
    f_fu_4320_p1 = p_Result_5_fu_4309_p5.read();
}

void mnist_fp32::thread_ff1_V_fu_4344_p2() {
    ff1_V_fu_4344_p2 = (!p_8_reg_1315.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_8_reg_1315.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_ff2_V_fu_6719_p2() {
    ff2_V_fu_6719_p2 = (!p_14_reg_1589.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_14_reg_1589.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_ff3_V_fu_7988_p2() {
    ff3_V_fu_7988_p2 = (!p_25_reg_1788.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_25_reg_1788.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_ff4_V_fu_10242_p2() {
    ff4_V_fu_10242_p2 = (!p_41_reg_2063.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_41_reg_2063.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_ff5_V_fu_11486_p2() {
    ff5_V_fu_11486_p2 = (!p_52_reg_2260.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_52_reg_2260.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_ff_V_fu_3019_p2() {
    ff_V_fu_3019_p2 = (!p_1_reg_1136.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_1_reg_1136.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_grp_fu_10098_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state247.read())) {
        grp_fu_10098_ap_start = ap_const_logic_1;
    } else {
        grp_fu_10098_ap_start = ap_const_logic_0;
    }
}

void mnist_fp32::thread_grp_fu_10098_p0() {
    grp_fu_10098_p0 = (!tmp_701_reg_16108.read()[0].is_01())? sc_lv<10>(): ((tmp_701_reg_16108.read()[0].to_bool())? neg_ti7_fu_10085_p2.read(): tmp_705_fu_10075_p1.read());
}

void mnist_fp32::thread_grp_fu_10098_p1() {
    grp_fu_10098_p1 =  (sc_lv<4>) (ap_const_lv10_7);
}

void mnist_fp32::thread_grp_fu_11233_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state292.read())) {
        grp_fu_11233_ap_start = ap_const_logic_1;
    } else {
        grp_fu_11233_ap_start = ap_const_logic_0;
    }
}

void mnist_fp32::thread_grp_fu_11233_p0() {
    grp_fu_11233_p0 = (!tmp_765_reg_16469.read()[0].is_01())? sc_lv<11>(): ((tmp_765_reg_16469.read()[0].to_bool())? neg_ti9_fu_11220_p2.read(): tmp_769_fu_11210_p1.read());
}

void mnist_fp32::thread_grp_fu_11233_p1() {
    grp_fu_11233_p1 =  (sc_lv<4>) (ap_const_lv11_7);
}

void mnist_fp32::thread_grp_fu_2594_opcode() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state388.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state420.read()))) {
        grp_fu_2594_opcode = ap_const_lv2_1;
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state34.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state156.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state275.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state378.read()))) {
        grp_fu_2594_opcode = ap_const_lv2_0;
    } else {
        grp_fu_2594_opcode =  (sc_lv<2>) ("XX");
    }
}

void mnist_fp32::thread_grp_fu_2594_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state388.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state420.read()))) {
        grp_fu_2594_p0 = reg_2673.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state34.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state156.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state275.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state378.read()))) {
        grp_fu_2594_p0 = reg_2659.read();
    } else {
        grp_fu_2594_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2594_p1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state388.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state420.read()))) {
        grp_fu_2594_p1 = compute6_reg_2500.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state378.read())) {
        grp_fu_2594_p1 = reducer6_reg_2487.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state275.read())) {
        grp_fu_2594_p1 = reducer42_2_reg_2155.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state156.read())) {
        grp_fu_2594_p1 = reducer39_2_reg_1683.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state34.read())) {
        grp_fu_2594_p1 = reducer36_1_reg_1206.read();
    } else {
        grp_fu_2594_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2603_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state374.read())) {
        grp_fu_2603_p0 = p_03_i6_reg_17233.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state271.read())) {
        grp_fu_2603_p0 = pad_temp4_0_load_reg_16270.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state152.read())) {
        grp_fu_2603_p0 = pad_temp2_0_load_reg_15218.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state30.read())) {
        grp_fu_2603_p0 = pad_temp_0_0_load_reg_14178.read();
    } else {
        grp_fu_2603_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2603_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state374.read())) {
        grp_fu_2603_p1 = w_dense_1_load_reg_17238.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state271.read())) {
        grp_fu_2603_p1 = w_conv5_load_reg_16275.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state152.read())) {
        grp_fu_2603_p1 = w_conv3_load_reg_15223.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state30.read())) {
        grp_fu_2603_p1 = w_conv1_0_load_reg_14183.read();
    } else {
        grp_fu_2603_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2607_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state367.read())) {
        grp_fu_2607_p0 = tmp32_V_20_reg_17208.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state351.read())) {
        grp_fu_2607_p0 = tmp32_V_18_reg_17040.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state331.read())) {
        grp_fu_2607_p0 = tmp32_V_15_reg_16832.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state310.read())) {
        grp_fu_2607_p0 = tmp32_V_12_reg_16542.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state235.read())) {
        grp_fu_2607_p0 = tmp32_V_9_reg_16009.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state213.read())) {
        grp_fu_2607_p0 = tmp32_V_4_reg_15760.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state192.read())) {
        grp_fu_2607_p0 = tmp32_V_7_reg_15490.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state115.read())) {
        grp_fu_2607_p0 = tmp32_V_8_reg_14972.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state93.read())) {
        grp_fu_2607_p0 = tmp32_V_3_reg_14723.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state72.read())) {
        grp_fu_2607_p0 = tmp32_V_1_reg_14448.read();
    } else {
        grp_fu_2607_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2610_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state475.read())) {
        grp_fu_2610_p0 = tmp_409_reg_17302.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state417.read())) {
        grp_fu_2610_p0 = tmp_345_reg_17274.read();
    } else {
        grp_fu_2610_p0 =  (sc_lv<64>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2613_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state411.read())) {
        grp_fu_2613_p0 = compute7_reg_2534.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state393.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state425.read()))) {
        grp_fu_2613_p0 = reg_2664.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state386.read())) {
        grp_fu_2613_p0 = ap_phi_mux_compute7_phi_fu_2538_p4.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state360.read())) {
        grp_fu_2613_p0 = max_pool_0_0_0_q0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state343.read())) {
        grp_fu_2613_p0 = conv6_0_load_reg_16896.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state325.read())) {
        grp_fu_2613_p0 = pad_temp5_0_load_reg_16680.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state285.read())) {
        grp_fu_2613_p0 = conv5_0_load_reg_16324.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state225.read())) {
        grp_fu_2613_p0 = conv4_0_load_reg_15829.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state207.read())) {
        grp_fu_2613_p0 = pad_temp3_0_load_reg_15608.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state166.read())) {
        grp_fu_2613_p0 = conv3_0_load_reg_15272.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state105.read())) {
        grp_fu_2613_p0 = conv2_0_load_reg_14792.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state87.read())) {
        grp_fu_2613_p0 = pad_temp1_0_load_reg_14571.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state44.read())) {
        grp_fu_2613_p0 = conv1_0_load_reg_14232.read();
    } else {
        grp_fu_2613_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2616_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state325.read())) {
        grp_fu_2616_p0 = w_conv6_load_reg_16685.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state207.read())) {
        grp_fu_2616_p0 = w_conv4_load_reg_15613.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state87.read())) {
        grp_fu_2616_p0 = w_conv2_load_reg_14576.read();
    } else {
        grp_fu_2616_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2622_opcode() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state479.read())) {
        grp_fu_2622_opcode = ap_const_lv5_2;
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state43.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state104.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state122.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state165.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state224.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state242.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state284.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state342.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state358.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state385.read()))) {
        grp_fu_2622_opcode = ap_const_lv5_4;
    } else {
        grp_fu_2622_opcode =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2622_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state479.read())) {
        grp_fu_2622_p0 = pred_2_reg_17320.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state385.read())) {
        grp_fu_2622_p0 = reg_2673.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state358.read())) {
        grp_fu_2622_p0 = p_03_i5_reg_17060.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state342.read())) {
        grp_fu_2622_p0 = conv6_0_load_reg_16896.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state284.read())) {
        grp_fu_2622_p0 = conv5_0_load_reg_16324.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state242.read())) {
        grp_fu_2622_p0 = p_03_i3_reg_16029.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state224.read())) {
        grp_fu_2622_p0 = conv4_0_load_reg_15829.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state165.read())) {
        grp_fu_2622_p0 = conv3_0_load_reg_15272.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state122.read())) {
        grp_fu_2622_p0 = p_03_i1_reg_14992.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state104.read())) {
        grp_fu_2622_p0 = conv2_0_load_reg_14792.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state43.read())) {
        grp_fu_2622_p0 = conv1_0_load_reg_14232.read();
    } else {
        grp_fu_2622_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2622_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state479.read())) {
        grp_fu_2622_p1 = pred_reg_2582.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state385.read())) {
        grp_fu_2622_p1 = compute6_reg_2500.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state358.read())) {
        grp_fu_2622_p1 = tmp_408_reg_2431.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state242.read())) {
        grp_fu_2622_p1 = tmp_283_reg_1983.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state122.read())) {
        grp_fu_2622_p1 = tmp_125_reg_1509.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state43.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state104.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state165.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state224.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state284.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state342.read()))) {
        grp_fu_2622_p1 = ap_const_lv32_0;
    } else {
        grp_fu_2622_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2640_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state426.read())) {
        grp_fu_2640_p1 = tmp_401_reg_17297.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state394.read())) {
        grp_fu_2640_p1 = reg_2679.read();
    } else {
        grp_fu_2640_p1 =  (sc_lv<64>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2645_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state3.read())) {
        grp_fu_2645_p0 = p_s_reg_1097.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        grp_fu_2645_p0 = ap_phi_mux_p_s_phi_fu_1101_p4.read();
    } else {
        grp_fu_2645_p0 =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2645_p2() {
    grp_fu_2645_p2 = (!grp_fu_2645_p0.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(grp_fu_2645_p0.read() == ap_const_lv5_1E);
}

void mnist_fp32::thread_grp_fu_2652_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state49.read())) {
        grp_fu_2652_p0 = p_9_reg_1273.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state48.read())) {
        grp_fu_2652_p0 = ap_phi_mux_p_9_phi_fu_1277_p4.read();
    } else {
        grp_fu_2652_p0 =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp32::thread_grp_fu_2652_p2() {
    grp_fu_2652_p2 = (!grp_fu_2652_p0.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(grp_fu_2652_p0.read() == ap_const_lv5_1E);
}

void mnist_fp32::thread_grp_fu_2962_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read())) {
        grp_fu_2962_ap_start = ap_const_logic_1;
    } else {
        grp_fu_2962_ap_start = ap_const_logic_0;
    }
}

void mnist_fp32::thread_grp_fu_2962_p0() {
    grp_fu_2962_p0 =  (sc_lv<12>) (grp_fu_2962_p00.read());
}

void mnist_fp32::thread_grp_fu_2962_p00() {
    grp_fu_2962_p00 = (!tmp_63_reg_14067.read()[0].is_01())? sc_lv<11>(): ((tmp_63_reg_14067.read()[0].to_bool())? neg_ti_fu_2949_p2.read(): tmp_74_fu_2939_p1.read());
}

void mnist_fp32::thread_grp_fu_2962_p1() {
    grp_fu_2962_p1 =  (sc_lv<6>) (ap_const_lv11_1C);
}

void mnist_fp32::thread_grp_fu_4075_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state51.read())) {
        grp_fu_4075_ap_start = ap_const_logic_1;
    } else {
        grp_fu_4075_ap_start = ap_const_logic_0;
    }
}

void mnist_fp32::thread_grp_fu_4075_p0() {
    grp_fu_4075_p0 =  (sc_lv<14>) (grp_fu_4075_p00.read());
}

void mnist_fp32::thread_grp_fu_4075_p00() {
    grp_fu_4075_p00 = (!tmp_284_reg_14380.read()[0].is_01())? sc_lv<13>(): ((tmp_284_reg_14380.read()[0].to_bool())? neg_ti1_fu_4062_p2.read(): tmp_295_fu_4052_p1.read());
}

void mnist_fp32::thread_grp_fu_4075_p1() {
    grp_fu_4075_p1 =  (sc_lv<6>) (ap_const_lv13_1C);
}

void mnist_fp32::thread_grp_fu_6559_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state127.read())) {
        grp_fu_6559_ap_start = ap_const_logic_1;
    } else {
        grp_fu_6559_ap_start = ap_const_logic_0;
    }
}

void mnist_fp32::thread_grp_fu_6559_p0() {
    grp_fu_6559_p0 = (!tmp_546_reg_15076.read()[0].is_01())? sc_lv<11>(): ((tmp_546_reg_15076.read()[0].to_bool())? neg_ti3_fu_6546_p2.read(): tmp_550_fu_6536_p1.read());
}

void mnist_fp32::thread_grp_fu_6559_p1() {
    grp_fu_6559_p1 =  (sc_lv<5>) (ap_const_lv11_E);
}

void mnist_fp32::thread_grp_fu_7719_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state173.read())) {
        grp_fu_7719_ap_start = ap_const_logic_1;
    } else {
        grp_fu_7719_ap_start = ap_const_logic_0;
    }
}

void mnist_fp32::thread_grp_fu_7719_p0() {
    grp_fu_7719_p0 = (!tmp_613_reg_15422.read()[0].is_01())? sc_lv<12>(): ((tmp_613_reg_15422.read()[0].to_bool())? neg_ti5_fu_7706_p2.read(): tmp_617_fu_7696_p1.read());
}

void mnist_fp32::thread_grp_fu_7719_p1() {
    grp_fu_7719_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp32::thread_h1_V_fu_9450_p2() {
    h1_V_fu_9450_p2 = (!p_58_reg_1937.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_58_reg_1937.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_h_V_fu_5879_p2() {
    h_V_fu_5879_p2 = (!p_26_reg_1463.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_26_reg_1463.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_i1_V_fu_3855_p2() {
    i1_V_fu_3855_p2 = (!p_13_reg_1285.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_13_reg_1285.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_i3_V_fu_6394_p2() {
    i3_V_fu_6394_p2 = (!p_29_reg_1565.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_29_reg_1565.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_i4_V_fu_7554_p2() {
    i4_V_fu_7554_p2 = (!p_38_reg_1761.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_38_reg_1761.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_i6_V_fu_9951_p2() {
    i6_V_fu_9951_p2 = (!p_55_reg_2039.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_55_reg_2039.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_i7_V_fu_11086_p2() {
    i7_V_fu_11086_p2 = (!p_62_reg_2233.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_62_reg_2233.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_i_V_1_fu_13829_p2() {
    i_V_1_fu_13829_p2 = (!index_V_reg_2570.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(index_V_reg_2570.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_i_V_fu_2764_p2() {
    i_V_fu_2764_p2 = (!p_2_reg_1109.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_2_reg_1109.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_icmp10_fu_12745_p2() {
    icmp10_fu_12745_p2 = (!tmp_809_fu_12735_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_809_fu_12735_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp11_fu_11952_p2() {
    icmp11_fu_11952_p2 = (!tmp_843_fu_11942_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_843_fu_11942_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp12_fu_12037_p2() {
    icmp12_fu_12037_p2 = (!tmp_849_fu_12027_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_849_fu_12027_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp1_fu_5645_p2() {
    icmp1_fu_5645_p2 = (!tmp_415_fu_5635_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_415_fu_5635_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp2_fu_4827_p2() {
    icmp2_fu_4827_p2 = (!tmp_535_fu_4817_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_535_fu_4817_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp3_fu_4912_p2() {
    icmp3_fu_4912_p2 = (!tmp_541_fu_4902_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_541_fu_4902_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp4_fu_7278_p2() {
    icmp4_fu_7278_p2 = (!tmp_592_fu_7268_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_592_fu_7268_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp5_fu_9224_p2() {
    icmp5_fu_9224_p2 = (!tmp_660_fu_9214_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_660_fu_9214_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp6_fu_8406_p2() {
    icmp6_fu_8406_p2 = (!tmp_691_fu_8396_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_691_fu_8396_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp7_fu_8491_p2() {
    icmp7_fu_8491_p2 = (!tmp_697_fu_8481_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_697_fu_8481_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp8_fu_10812_p2() {
    icmp8_fu_10812_p2 = (!tmp_743_fu_10802_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_743_fu_10802_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp9_fu_13340_p2() {
    icmp9_fu_13340_p2 = (!tmp_798_fu_13330_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_798_fu_13330_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_icmp_fu_3561_p2() {
    icmp_fu_3561_p2 = (!tmp_149_fu_3551_p4.read().is_01() || !ap_const_lv7_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_149_fu_3551_p4.read() == ap_const_lv7_0);
}

void mnist_fp32::thread_image_r_address0() {
    image_r_address0 =  (sc_lv<10>) (tmp_328_cast_fu_2995_p1.read());
}

void mnist_fp32::thread_image_r_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state21.read())) {
        image_r_ce0 = ap_const_logic_1;
    } else {
        image_r_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_index_V_1_fu_13930_p3() {
    index_V_1_fu_13930_p3 = (!tmp_515_reg_17327.read()[0].is_01())? sc_lv<4>(): ((tmp_515_reg_17327.read()[0].to_bool())? index_V_reg_2570.read(): p_79_reg_2557.read());
}

void mnist_fp32::thread_index_tuple1_V_fu_3765_p2() {
    index_tuple1_V_fu_3765_p2 = (!p_9_reg_1273.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_9_reg_1273.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_index_tuple2_V_fu_6306_p2() {
    index_tuple2_V_fu_6306_p2 = (!p_19_reg_1554.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_19_reg_1554.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_index_tuple3_V_fu_7466_p2() {
    index_tuple3_V_fu_7466_p2 = (!p_30_reg_1750.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_30_reg_1750.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_index_tuple4_V_fu_9879_p2() {
    index_tuple4_V_fu_9879_p2 = (!p_44_reg_2028.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_44_reg_2028.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_index_tuple5_V_fu_11010_p2() {
    index_tuple5_V_fu_11010_p2 = (!p_56_reg_2222.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_56_reg_2222.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_index_tuple_V_fu_2698_p2() {
    index_tuple_V_fu_2698_p2 = (!p_s_reg_1097.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_s_reg_1097.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_ireg_V_12_fu_4676_p1() {
    ireg_V_12_fu_4676_p1 = grp_fu_2613_p1.read();
}

void mnist_fp32::thread_ireg_V_13_fu_4712_p1() {
    ireg_V_13_fu_4712_p1 = grp_fu_2616_p1.read();
}

void mnist_fp32::thread_ireg_V_14_fu_8255_p1() {
    ireg_V_14_fu_8255_p1 = grp_fu_2613_p1.read();
}

void mnist_fp32::thread_ireg_V_15_fu_8291_p1() {
    ireg_V_15_fu_8291_p1 = grp_fu_2616_p1.read();
}

void mnist_fp32::thread_ireg_V_16_fu_11801_p1() {
    ireg_V_16_fu_11801_p1 = grp_fu_2613_p1.read();
}

void mnist_fp32::thread_ireg_V_17_fu_11837_p1() {
    ireg_V_17_fu_11837_p1 = grp_fu_2616_p1.read();
}

void mnist_fp32::thread_ireg_V_1_fu_12626_p3() {
    ireg_V_1_fu_12626_p3 = (!tmp_473_fu_12617_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_473_fu_12617_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_485_fu_12622_p1.read());
}

void mnist_fp32::thread_ireg_V_3_fu_5526_p3() {
    ireg_V_3_fu_5526_p3 = (!tmp_115_fu_5517_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_115_fu_5517_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_127_fu_5522_p1.read());
}

void mnist_fp32::thread_ireg_V_4_fu_7159_p3() {
    ireg_V_4_fu_7159_p3 = (!tmp_209_fu_7150_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_209_fu_7150_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_227_fu_7155_p1.read());
}

void mnist_fp32::thread_ireg_V_7_fu_9105_p3() {
    ireg_V_7_fu_9105_p3 = (!tmp_258_fu_9096_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_258_fu_9096_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_265_fu_9101_p1.read());
}

void mnist_fp32::thread_ireg_V_8_fu_10693_p3() {
    ireg_V_8_fu_10693_p3 = (!tmp_391_fu_10684_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_391_fu_10684_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_403_fu_10689_p1.read());
}

void mnist_fp32::thread_ireg_V_fu_3442_p3() {
    ireg_V_fu_3442_p3 = (!tmp_18_fu_3433_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_18_fu_3433_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_25_fu_3438_p1.read());
}

void mnist_fp32::thread_ireg_V_s_fu_13226_p1() {
    ireg_V_s_fu_13226_p1 = grp_fu_2613_p1.read();
}

void mnist_fp32::thread_is_neg_1_fu_5241_p3() {
    is_neg_1_fu_5241_p3 = p_Val2_5_reg_1350.read().range(31, 31);
}

void mnist_fp32::thread_is_neg_2_fu_7871_p3() {
    is_neg_2_fu_7871_p3 = p_Val2_25_reg_15471.read().range(31, 31);
}

void mnist_fp32::thread_is_neg_3_fu_8820_p3() {
    is_neg_3_fu_8820_p3 = p_Val2_1_reg_1823.read().range(31, 31);
}

void mnist_fp32::thread_is_neg_4_fu_11369_p3() {
    is_neg_4_fu_11369_p3 = p_Val2_34_reg_16523.read().range(31, 31);
}

void mnist_fp32::thread_is_neg_5_fu_12366_p3() {
    is_neg_5_fu_12366_p3 = p_Val2_10_reg_2294.read().range(31, 31);
}

void mnist_fp32::thread_is_neg_fu_4227_p3() {
    is_neg_fu_4227_p3 = p_Val2_s_reg_14429.read().range(31, 31);
}

void mnist_fp32::thread_j1_V_fu_13812_p2() {
    j1_V_fu_13812_p2 = (!p_76_reg_2546.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_76_reg_2546.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_j_V_fu_13495_p2() {
    j_V_fu_13495_p2 = (!p_87_reg_2465.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_87_reg_2465.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_lhs_V_10_cast1_fu_9885_p1() {
    lhs_V_10_cast1_fu_9885_p1 = esl_zext<8,4>(p_44_reg_2028.read());
}

void mnist_fp32::thread_lhs_V_10_cast_fu_3979_p1() {
    lhs_V_10_cast_fu_3979_p1 = esl_sext<7,6>(lhs_V_2_fu_3973_p2.read());
}

void mnist_fp32::thread_lhs_V_11_cast_fu_9507_p1() {
    lhs_V_11_cast_fu_9507_p1 = esl_zext<10,3>(p_64_reg_1948.read());
}

void mnist_fp32::thread_lhs_V_13_cast_fu_10982_p1() {
    lhs_V_13_cast_fu_10982_p1 = esl_zext<9,5>(p_47_reg_2200.read());
}

void mnist_fp32::thread_lhs_V_14_cast_fu_11016_p1() {
    lhs_V_14_cast_fu_11016_p1 = esl_zext<9,4>(p_56_reg_2222.read());
}

void mnist_fp32::thread_lhs_V_18_cast_fu_6463_p1() {
    lhs_V_18_cast_fu_6463_p1 = esl_sext<6,5>(lhs_V_8_fu_6457_p2.read());
}

void mnist_fp32::thread_lhs_V_1_cast_fu_6324_p1() {
    lhs_V_1_cast_fu_6324_p1 = esl_zext<8,5>(p_19_reg_1554.read());
}

void mnist_fp32::thread_lhs_V_1_fu_9996_p2() {
    lhs_V_1_fu_9996_p2 = (!p_55_reg_2039.read().is_01() || !tmp_239_cast_fu_9992_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(p_55_reg_2039.read()) - sc_biguint<4>(tmp_239_cast_fu_9992_p1.read()));
}

void mnist_fp32::thread_lhs_V_24_cast_fu_7623_p1() {
    lhs_V_24_cast_fu_7623_p1 = esl_sext<6,5>(lhs_V_4_fu_7617_p2.read());
}

void mnist_fp32::thread_lhs_V_27_cast_fu_10286_p1() {
    lhs_V_27_cast_fu_10286_p1 = esl_zext<4,3>(p_48_reg_2074.read());
}

void mnist_fp32::thread_lhs_V_29_cast_fu_10333_p1() {
    lhs_V_29_cast_fu_10333_p1 = esl_zext<4,3>(p_57_reg_2085.read());
}

void mnist_fp32::thread_lhs_V_2_fu_3973_p2() {
    lhs_V_2_fu_3973_p2 = (!tmp_37_cast_fu_3965_p1.read().is_01() || !tmp_40_cast_fu_3969_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_37_cast_fu_3965_p1.read()) - sc_biguint<6>(tmp_40_cast_fu_3969_p1.read()));
}

void mnist_fp32::thread_lhs_V_31_cast_fu_10002_p1() {
    lhs_V_31_cast_fu_10002_p1 = esl_sext<5,4>(lhs_V_1_fu_9996_p2.read());
}

void mnist_fp32::thread_lhs_V_32_cast_fu_11542_p1() {
    lhs_V_32_cast_fu_11542_p1 = esl_zext<4,3>(p_61_reg_2271.read());
}

void mnist_fp32::thread_lhs_V_33_cast_fu_11589_p1() {
    lhs_V_33_cast_fu_11589_p1 = esl_zext<4,3>(p_66_reg_2282.read());
}

void mnist_fp32::thread_lhs_V_36_cast_fu_11137_p1() {
    lhs_V_36_cast_fu_11137_p1 = esl_sext<5,4>(lhs_V_3_fu_11131_p2.read());
}

void mnist_fp32::thread_lhs_V_3_cast_fu_7484_p1() {
    lhs_V_3_cast_fu_7484_p1 = esl_zext<9,5>(p_30_reg_1750.read());
}

void mnist_fp32::thread_lhs_V_3_fu_11131_p2() {
    lhs_V_3_fu_11131_p2 = (!p_62_reg_2233.read().is_01() || !tmp_266_cast_fu_11127_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(p_62_reg_2233.read()) - sc_biguint<4>(tmp_266_cast_fu_11127_p1.read()));
}

void mnist_fp32::thread_lhs_V_4_fu_7617_p2() {
    lhs_V_4_fu_7617_p2 = (!p_38_reg_1761.read().is_01() || !tmp_137_cast_fu_7613_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_38_reg_1761.read()) - sc_biguint<5>(tmp_137_cast_fu_7613_p1.read()));
}

void mnist_fp32::thread_lhs_V_5_cast_fu_5885_p1() {
    lhs_V_5_cast_fu_5885_p1 = esl_zext<9,4>(p_26_reg_1463.read());
}

void mnist_fp32::thread_lhs_V_6_cast_fu_9851_p1() {
    lhs_V_6_cast_fu_9851_p1 = esl_zext<8,4>(p_36_reg_2006.read());
}

void mnist_fp32::thread_lhs_V_7_cast_fu_5944_p1() {
    lhs_V_7_cast_fu_5944_p1 = esl_zext<11,4>(p_32_reg_1474.read());
}

void mnist_fp32::thread_lhs_V_8_fu_6457_p2() {
    lhs_V_8_fu_6457_p2 = (!p_29_reg_1565.read().is_01() || !tmp_85_cast_fu_6453_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_29_reg_1565.read()) - sc_biguint<5>(tmp_85_cast_fu_6453_p1.read()));
}

void mnist_fp32::thread_lhs_V_9_cast_fu_9456_p1() {
    lhs_V_9_cast_fu_9456_p1 = esl_zext<9,3>(p_58_reg_1937.read());
}

void mnist_fp32::thread_lhs_V_cast_42_fu_3771_p1() {
    lhs_V_cast_42_fu_3771_p1 = esl_zext<10,5>(p_9_reg_1273.read());
}

void mnist_fp32::thread_lhs_V_cast_fu_2888_p1() {
    lhs_V_cast_fu_2888_p1 = esl_sext<11,6>(lhs_V_s_fu_2882_p2.read());
}

void mnist_fp32::thread_lhs_V_s_fu_2882_p2() {
    lhs_V_s_fu_2882_p2 = (!tmp_10_cast_fu_2874_p1.read().is_01() || !tmp_13_cast_fu_2878_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_10_cast_fu_2874_p1.read()) - sc_biguint<6>(tmp_13_cast_fu_2878_p1.read()));
}

void mnist_fp32::thread_man_V_11_fu_8341_p2() {
    man_V_11_fu_8341_p2 = (!ap_const_lv54_0.is_01() || !p_Result_20_fu_8337_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_20_fu_8337_p1.read()));
}

void mnist_fp32::thread_man_V_12_fu_8347_p3() {
    man_V_12_fu_8347_p3 = (!isneg_2_reg_15618.read()[0].is_01())? sc_lv<54>(): ((isneg_2_reg_15618.read()[0].to_bool())? man_V_11_fu_8341_p2.read(): p_Result_20_fu_8337_p1.read());
}

void mnist_fp32::thread_man_V_13_fu_9159_p2() {
    man_V_13_fu_9159_p2 = (!ap_const_lv54_0.is_01() || !p_Result_27_fu_9155_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_27_fu_9155_p1.read()));
}

void mnist_fp32::thread_man_V_14_fu_8426_p2() {
    man_V_14_fu_8426_p2 = (!ap_const_lv54_0.is_01() || !p_Result_21_fu_8422_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_21_fu_8422_p1.read()));
}

void mnist_fp32::thread_man_V_15_fu_8432_p3() {
    man_V_15_fu_8432_p3 = (!isneg_3_reg_15640.read()[0].is_01())? sc_lv<54>(): ((isneg_3_reg_15640.read()[0].to_bool())? man_V_14_fu_8426_p2.read(): p_Result_21_fu_8422_p1.read());
}

void mnist_fp32::thread_man_V_16_fu_9165_p3() {
    man_V_16_fu_9165_p3 = (!tmp_657_reg_15851.read()[0].is_01())? sc_lv<54>(): ((tmp_657_reg_15851.read()[0].to_bool())? man_V_13_fu_9159_p2.read(): p_Result_27_fu_9155_p1.read());
}

void mnist_fp32::thread_man_V_18_fu_10747_p2() {
    man_V_18_fu_10747_p2 = (!ap_const_lv54_0.is_01() || !p_Result_35_fu_10743_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_35_fu_10743_p1.read()));
}

void mnist_fp32::thread_man_V_19_fu_11887_p2() {
    man_V_19_fu_11887_p2 = (!ap_const_lv54_0.is_01() || !p_Result_29_fu_11883_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_29_fu_11883_p1.read()));
}

void mnist_fp32::thread_man_V_1_fu_3496_p2() {
    man_V_1_fu_3496_p2 = (!ap_const_lv54_0.is_01() || !p_Result_1_fu_3492_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_1_fu_3492_p1.read()));
}

void mnist_fp32::thread_man_V_20_fu_11893_p3() {
    man_V_20_fu_11893_p3 = (!isneg_4_reg_16690.read()[0].is_01())? sc_lv<54>(): ((isneg_4_reg_16690.read()[0].to_bool())? man_V_19_fu_11887_p2.read(): p_Result_29_fu_11883_p1.read());
}

void mnist_fp32::thread_man_V_21_fu_10753_p3() {
    man_V_21_fu_10753_p3 = (!tmp_740_reg_16346.read()[0].is_01())? sc_lv<54>(): ((tmp_740_reg_16346.read()[0].to_bool())? man_V_18_fu_10747_p2.read(): p_Result_35_fu_10743_p1.read());
}

void mnist_fp32::thread_man_V_23_fu_13270_p2() {
    man_V_23_fu_13270_p2 = (!ap_const_lv54_0.is_01() || !p_Result_37_fu_13266_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_37_fu_13266_p1.read()));
}

void mnist_fp32::thread_man_V_24_fu_11972_p2() {
    man_V_24_fu_11972_p2 = (!ap_const_lv54_0.is_01() || !p_Result_30_fu_11968_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_30_fu_11968_p1.read()));
}

void mnist_fp32::thread_man_V_25_fu_11978_p3() {
    man_V_25_fu_11978_p3 = (!isneg_5_reg_16712.read()[0].is_01())? sc_lv<54>(): ((isneg_5_reg_16712.read()[0].to_bool())? man_V_24_fu_11972_p2.read(): p_Result_30_fu_11968_p1.read());
}

void mnist_fp32::thread_man_V_26_fu_13276_p3() {
    man_V_26_fu_13276_p3 = (!tmp_795_reg_17095.read()[0].is_01())? sc_lv<54>(): ((tmp_795_reg_17095.read()[0].to_bool())? man_V_23_fu_13270_p2.read(): p_Result_37_fu_13266_p1.read());
}

void mnist_fp32::thread_man_V_27_fu_12680_p2() {
    man_V_27_fu_12680_p2 = (!ap_const_lv54_0.is_01() || !p_Result_39_fu_12676_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_39_fu_12676_p1.read()));
}

void mnist_fp32::thread_man_V_28_fu_12686_p3() {
    man_V_28_fu_12686_p3 = (!tmp_806_reg_16918.read()[0].is_01())? sc_lv<54>(): ((tmp_806_reg_16918.read()[0].to_bool())? man_V_27_fu_12680_p2.read(): p_Result_39_fu_12676_p1.read());
}

void mnist_fp32::thread_man_V_2_fu_3502_p3() {
    man_V_2_fu_3502_p3 = (!tmp_144_reg_14254.read()[0].is_01())? sc_lv<54>(): ((tmp_144_reg_14254.read()[0].to_bool())? man_V_1_fu_3496_p2.read(): p_Result_1_fu_3492_p1.read());
}

void mnist_fp32::thread_man_V_3_fu_4762_p2() {
    man_V_3_fu_4762_p2 = (!ap_const_lv54_0.is_01() || !p_Result_6_fu_4758_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_6_fu_4758_p1.read()));
}

void mnist_fp32::thread_man_V_4_fu_4768_p3() {
    man_V_4_fu_4768_p3 = (!isneg_reg_14581.read()[0].is_01())? sc_lv<54>(): ((isneg_reg_14581.read()[0].to_bool())? man_V_3_fu_4762_p2.read(): p_Result_6_fu_4758_p1.read());
}

void mnist_fp32::thread_man_V_5_fu_5580_p2() {
    man_V_5_fu_5580_p2 = (!ap_const_lv54_0.is_01() || !p_Result_4_fu_5576_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_4_fu_5576_p1.read()));
}

void mnist_fp32::thread_man_V_6_fu_5586_p3() {
    man_V_6_fu_5586_p3 = (!tmp_397_reg_14814.read()[0].is_01())? sc_lv<54>(): ((tmp_397_reg_14814.read()[0].to_bool())? man_V_5_fu_5580_p2.read(): p_Result_4_fu_5576_p1.read());
}

void mnist_fp32::thread_man_V_7_fu_7219_p3() {
    man_V_7_fu_7219_p3 = (!tmp_589_reg_15294.read()[0].is_01())? sc_lv<54>(): ((tmp_589_reg_15294.read()[0].to_bool())? man_V_s_fu_7213_p2.read(): p_Result_18_fu_7209_p1.read());
}

void mnist_fp32::thread_man_V_8_fu_4847_p2() {
    man_V_8_fu_4847_p2 = (!ap_const_lv54_0.is_01() || !p_Result_8_fu_4843_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_8_fu_4843_p1.read()));
}

void mnist_fp32::thread_man_V_9_fu_4853_p3() {
    man_V_9_fu_4853_p3 = (!isneg_1_reg_14603.read()[0].is_01())? sc_lv<54>(): ((isneg_1_reg_14603.read()[0].to_bool())? man_V_8_fu_4847_p2.read(): p_Result_8_fu_4843_p1.read());
}

void mnist_fp32::thread_man_V_s_fu_7213_p2() {
    man_V_s_fu_7213_p2 = (!ap_const_lv54_0.is_01() || !p_Result_18_fu_7209_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_18_fu_7209_p1.read()));
}

void mnist_fp32::thread_max_pool_0_0_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state359.read())) {
        max_pool_0_0_0_address0 =  (sc_lv<4>) (tmp_388_fu_13221_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state347.read())) {
        max_pool_0_0_0_address0 =  (sc_lv<4>) (tmp_373_reg_16982.read());
    } else {
        max_pool_0_0_0_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void mnist_fp32::thread_max_pool_0_0_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state347.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state359.read()))) {
        max_pool_0_0_0_ce0 = ap_const_logic_1;
    } else {
        max_pool_0_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_max_pool_0_0_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond84_fu_12935_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state347.read()))) {
        max_pool_0_0_0_we0 = ap_const_logic_1;
    } else {
        max_pool_0_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_mul10_fu_14017_p0() {
    mul10_fu_14017_p0 =  (sc_lv<13>) (ap_const_lv24_A73);
}

void mnist_fp32::thread_mul10_fu_14017_p1() {
    mul10_fu_14017_p1 =  (sc_lv<11>) (sext9_cast_fu_11164_p1.read());
}

void mnist_fp32::thread_mul1_fu_13945_p0() {
    mul1_fu_13945_p0 =  (sc_lv<15>) (ap_const_lv28_2493);
}

void mnist_fp32::thread_mul1_fu_13945_p1() {
    mul1_fu_13945_p1 =  (sc_lv<13>) (sext1_cast_fu_3997_p1.read());
}

void mnist_fp32::thread_mul2_fu_13953_p0() {
    mul2_fu_13953_p0 =  (sc_lv<15>) (ap_const_lv28_29CC);
}

void mnist_fp32::thread_mul2_fu_13953_p1() {
    mul2_fu_13953_p1 =  (sc_lv<13>) (sext1_cast_fu_3997_p1.read());
}

void mnist_fp32::thread_mul3_fu_13961_p0() {
    mul3_fu_13961_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp32::thread_mul3_fu_13961_p1() {
    mul3_fu_13961_p1 =  (sc_lv<11>) (sext3_cast_fu_6481_p1.read());
}

void mnist_fp32::thread_mul4_fu_13969_p0() {
    mul4_fu_13969_p0 =  (sc_lv<13>) (ap_const_lv24_A73);
}

void mnist_fp32::thread_mul4_fu_13969_p1() {
    mul4_fu_13969_p1 =  (sc_lv<11>) (sext3_cast_fu_6481_p1.read());
}

void mnist_fp32::thread_mul5_fu_13977_p0() {
    mul5_fu_13977_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp32::thread_mul5_fu_13977_p1() {
    mul5_fu_13977_p1 =  (sc_lv<12>) (sext5_cast_fu_7641_p1.read());
}

void mnist_fp32::thread_mul6_fu_13985_p0() {
    mul6_fu_13985_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp32::thread_mul6_fu_13985_p1() {
    mul6_fu_13985_p1 =  (sc_lv<12>) (sext5_cast_fu_7641_p1.read());
}

void mnist_fp32::thread_mul7_fu_13993_p0() {
    mul7_fu_13993_p0 =  (sc_lv<12>) (ap_const_lv22_493);
}

void mnist_fp32::thread_mul7_fu_13993_p1() {
    mul7_fu_13993_p1 =  (sc_lv<10>) (sext7_cast_fu_10029_p1.read());
}

void mnist_fp32::thread_mul8_fu_14001_p0() {
    mul8_fu_14001_p0 =  (sc_lv<12>) (ap_const_lv22_53A);
}

void mnist_fp32::thread_mul8_fu_14001_p1() {
    mul8_fu_14001_p1 =  (sc_lv<10>) (sext7_cast_fu_10029_p1.read());
}

void mnist_fp32::thread_mul9_fu_14009_p0() {
    mul9_fu_14009_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp32::thread_mul9_fu_14009_p1() {
    mul9_fu_14009_p1 =  (sc_lv<11>) (sext9_cast_fu_11164_p1.read());
}

void mnist_fp32::thread_mul_fu_13937_p0() {
    mul_fu_13937_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp32::thread_neg_mul10_fu_11239_p2() {
    neg_mul10_fu_11239_p2 = (!ap_const_lv23_0.is_01() || !tmp_772_reg_16487.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_772_reg_16487.read()));
}

void mnist_fp32::thread_neg_mul1_fu_4033_p2() {
    neg_mul1_fu_4033_p2 = (!ap_const_lv26_0.is_01() || !tmp_282_reg_14375.read().is_01())? sc_lv<26>(): (sc_biguint<26>(ap_const_lv26_0) - sc_biguint<26>(tmp_282_reg_14375.read()));
}

void mnist_fp32::thread_neg_mul2_fu_4081_p2() {
    neg_mul2_fu_4081_p2 = (!ap_const_lv27_0.is_01() || !tmp_303_reg_14393.read().is_01())? sc_lv<27>(): (sc_biguint<27>(ap_const_lv27_0) - sc_biguint<27>(tmp_303_reg_14393.read()));
}

void mnist_fp32::thread_neg_mul3_fu_6517_p2() {
    neg_mul3_fu_6517_p2 = (!ap_const_lv22_0.is_01() || !tmp_545_reg_15071.read().is_01())? sc_lv<22>(): (sc_biguint<22>(ap_const_lv22_0) - sc_biguint<22>(tmp_545_reg_15071.read()));
}

void mnist_fp32::thread_neg_mul4_fu_6565_p2() {
    neg_mul4_fu_6565_p2 = (!ap_const_lv23_0.is_01() || !tmp_553_reg_15089.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_553_reg_15089.read()));
}

void mnist_fp32::thread_neg_mul5_fu_7677_p2() {
    neg_mul5_fu_7677_p2 = (!ap_const_lv24_0.is_01() || !tmp_612_reg_15417.read().is_01())? sc_lv<24>(): (sc_biguint<24>(ap_const_lv24_0) - sc_biguint<24>(tmp_612_reg_15417.read()));
}

void mnist_fp32::thread_neg_mul6_fu_7725_p2() {
    neg_mul6_fu_7725_p2 = (!ap_const_lv25_0.is_01() || !tmp_620_reg_15435.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_620_reg_15435.read()));
}

void mnist_fp32::thread_neg_mul7_fu_10056_p2() {
    neg_mul7_fu_10056_p2 = (!ap_const_lv21_0.is_01() || !tmp_700_reg_16116.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_700_reg_16116.read()));
}

void mnist_fp32::thread_neg_mul8_fu_10104_p2() {
    neg_mul8_fu_10104_p2 = (!ap_const_lv21_0.is_01() || !tmp_708_reg_16126.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_708_reg_16126.read()));
}

void mnist_fp32::thread_neg_mul9_fu_11191_p2() {
    neg_mul9_fu_11191_p2 = (!ap_const_lv22_0.is_01() || !tmp_764_reg_16477.read().is_01())? sc_lv<22>(): (sc_biguint<22>(ap_const_lv22_0) - sc_biguint<22>(tmp_764_reg_16477.read()));
}

void mnist_fp32::thread_neg_mul_fu_2920_p2() {
    neg_mul_fu_2920_p2 = (!ap_const_lv23_0.is_01() || !tmp_62_reg_14073.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_62_reg_14073.read()));
}

void mnist_fp32::thread_neg_ti10_fu_11272_p2() {
    neg_ti10_fu_11272_p2 = (!ap_const_lv4_0.is_01() || !tmp_777_fu_11268_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_0) - sc_biguint<4>(tmp_777_fu_11268_p1.read()));
}

void mnist_fp32::thread_neg_ti1_fu_4062_p2() {
    neg_ti1_fu_4062_p2 = (!ap_const_lv13_0.is_01() || !tmp_300_fu_4055_p3.read().is_01())? sc_lv<13>(): (sc_biguint<13>(ap_const_lv13_0) - sc_biguint<13>(tmp_300_fu_4055_p3.read()));
}

void mnist_fp32::thread_neg_ti2_fu_4114_p2() {
    neg_ti2_fu_4114_p2 = (!ap_const_lv2_0.is_01() || !tmp_323_fu_4110_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_323_fu_4110_p1.read()));
}

void mnist_fp32::thread_neg_ti3_fu_6546_p2() {
    neg_ti3_fu_6546_p2 = (!ap_const_lv11_0.is_01() || !tmp_551_fu_6539_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_551_fu_6539_p3.read()));
}

void mnist_fp32::thread_neg_ti4_fu_6598_p2() {
    neg_ti4_fu_6598_p2 = (!ap_const_lv2_0.is_01() || !tmp_558_fu_6594_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_558_fu_6594_p1.read()));
}

void mnist_fp32::thread_neg_ti5_fu_7706_p2() {
    neg_ti5_fu_7706_p2 = (!ap_const_lv12_0.is_01() || !tmp_618_fu_7699_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_618_fu_7699_p3.read()));
}

void mnist_fp32::thread_neg_ti6_fu_7758_p2() {
    neg_ti6_fu_7758_p2 = (!ap_const_lv3_0.is_01() || !tmp_625_fu_7754_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_625_fu_7754_p1.read()));
}

void mnist_fp32::thread_neg_ti7_fu_10085_p2() {
    neg_ti7_fu_10085_p2 = (!ap_const_lv10_0.is_01() || !tmp_706_fu_10078_p3.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_0) - sc_biguint<10>(tmp_706_fu_10078_p3.read()));
}

void mnist_fp32::thread_neg_ti8_fu_10137_p2() {
    neg_ti8_fu_10137_p2 = (!ap_const_lv3_0.is_01() || !tmp_713_fu_10133_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_713_fu_10133_p1.read()));
}

void mnist_fp32::thread_neg_ti9_fu_11220_p2() {
    neg_ti9_fu_11220_p2 = (!ap_const_lv11_0.is_01() || !tmp_770_fu_11213_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_770_fu_11213_p3.read()));
}

void mnist_fp32::thread_neg_ti_fu_2949_p2() {
    neg_ti_fu_2949_p2 = (!ap_const_lv11_0.is_01() || !tmp_75_fu_2942_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_75_fu_2942_p3.read()));
}

void mnist_fp32::thread_newSel10_fu_5037_p3() {
    newSel10_fu_5037_p3 = (!or_cond6_fu_5018_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond6_fu_5018_p2.read()[0].to_bool())? newSel8_fu_5010_p3.read(): newSel9_fu_5024_p3.read());
}

void mnist_fp32::thread_newSel11_fu_5151_p3() {
    newSel11_fu_5151_p3 = (!sel_tmp35_fu_5146_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp35_fu_5146_p2.read()[0].to_bool())? tmp_174_fu_5087_p2.read(): tmp_542_fu_5076_p1.read());
}

void mnist_fp32::thread_newSel12_fu_5165_p3() {
    newSel12_fu_5165_p3 = (!sel_tmp32_fu_5123_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp32_fu_5123_p2.read()[0].to_bool())? storemerge6_fu_5080_p3.read(): tmp_540_reg_14682.read());
}

void mnist_fp32::thread_newSel13_fu_5178_p3() {
    newSel13_fu_5178_p3 = (!or_cond9_fu_5159_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond9_fu_5159_p2.read()[0].to_bool())? newSel11_fu_5151_p3.read(): newSel12_fu_5165_p3.read());
}

void mnist_fp32::thread_newSel14_fu_7378_p3() {
    newSel14_fu_7378_p3 = (!sel_tmp44_reg_15344.read()[0].is_01())? sc_lv<32>(): ((sel_tmp44_reg_15344.read()[0].to_bool())? tmp_238_fu_7347_p2.read(): tmp_593_fu_7336_p1.read());
}

void mnist_fp32::thread_newSel15_fu_7390_p3() {
    newSel15_fu_7390_p3 = (!sel_tmp41_fu_7368_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp41_fu_7368_p2.read()[0].to_bool())? storemerge8_fu_7340_p3.read(): tmp_591_reg_15332.read());
}

void mnist_fp32::thread_newSel16_fu_7403_p3() {
    newSel16_fu_7403_p3 = (!or_cond12_fu_7385_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond12_fu_7385_p2.read()[0].to_bool())? newSel14_fu_7378_p3.read(): newSel15_fu_7390_p3.read());
}

void mnist_fp32::thread_newSel18_fu_9324_p3() {
    newSel18_fu_9324_p3 = (!sel_tmp53_reg_15901.read()[0].is_01())? sc_lv<32>(): ((sel_tmp53_reg_15901.read()[0].to_bool())? tmp_285_fu_9293_p2.read(): tmp_661_fu_9282_p1.read());
}

void mnist_fp32::thread_newSel19_fu_9336_p3() {
    newSel19_fu_9336_p3 = (!sel_tmp50_fu_9314_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp50_fu_9314_p2.read()[0].to_bool())? storemerge5_fu_9286_p3.read(): tmp_659_reg_15889.read());
}

void mnist_fp32::thread_newSel1_fu_3673_p3() {
    newSel1_fu_3673_p3 = (!sel_tmp9_fu_3651_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp9_fu_3651_p2.read()[0].to_bool())? storemerge_fu_3623_p3.read(): tmp_148_reg_14292.read());
}

void mnist_fp32::thread_newSel20_fu_9349_p3() {
    newSel20_fu_9349_p3 = (!or_cond15_fu_9331_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond15_fu_9331_p2.read()[0].to_bool())? newSel18_fu_9324_p3.read(): newSel19_fu_9336_p3.read());
}

void mnist_fp32::thread_newSel22_fu_8589_p3() {
    newSel22_fu_8589_p3 = (!sel_tmp62_fu_8584_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp62_fu_8584_p2.read()[0].to_bool())? tmp_304_fu_8525_p2.read(): tmp_692_fu_8514_p1.read());
}

void mnist_fp32::thread_newSel23_fu_8603_p3() {
    newSel23_fu_8603_p3 = (!sel_tmp59_fu_8561_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp59_fu_8561_p2.read()[0].to_bool())? storemerge1_fu_8518_p3.read(): tmp_690_reg_15685.read());
}

void mnist_fp32::thread_newSel24_fu_8616_p3() {
    newSel24_fu_8616_p3 = (!or_cond18_fu_8597_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond18_fu_8597_p2.read()[0].to_bool())? newSel22_fu_8589_p3.read(): newSel23_fu_8603_p3.read());
}

void mnist_fp32::thread_newSel25_fu_8730_p3() {
    newSel25_fu_8730_p3 = (!sel_tmp71_fu_8725_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp71_fu_8725_p2.read()[0].to_bool())? tmp_324_fu_8666_p2.read(): tmp_698_fu_8655_p1.read());
}

void mnist_fp32::thread_newSel26_fu_8744_p3() {
    newSel26_fu_8744_p3 = (!sel_tmp68_fu_8702_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp68_fu_8702_p2.read()[0].to_bool())? storemerge3_fu_8659_p3.read(): tmp_696_reg_15719.read());
}

void mnist_fp32::thread_newSel27_fu_8757_p3() {
    newSel27_fu_8757_p3 = (!or_cond21_fu_8738_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond21_fu_8738_p2.read()[0].to_bool())? newSel25_fu_8730_p3.read(): newSel26_fu_8744_p3.read());
}

void mnist_fp32::thread_newSel28_fu_10912_p3() {
    newSel28_fu_10912_p3 = (!sel_tmp80_reg_16396.read()[0].is_01())? sc_lv<32>(): ((sel_tmp80_reg_16396.read()[0].to_bool())? tmp_431_fu_10881_p2.read(): tmp_744_fu_10870_p1.read());
}

void mnist_fp32::thread_newSel29_fu_10924_p3() {
    newSel29_fu_10924_p3 = (!sel_tmp77_fu_10902_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp77_fu_10902_p2.read()[0].to_bool())? storemerge7_fu_10874_p3.read(): tmp_742_reg_16384.read());
}

void mnist_fp32::thread_newSel2_fu_3686_p3() {
    newSel2_fu_3686_p3 = (!or_cond_fu_3668_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond_fu_3668_p2.read()[0].to_bool())? newSel_fu_3661_p3.read(): newSel1_fu_3673_p3.read());
}

void mnist_fp32::thread_newSel30_fu_10937_p3() {
    newSel30_fu_10937_p3 = (!or_cond24_fu_10919_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond24_fu_10919_p2.read()[0].to_bool())? newSel28_fu_10912_p3.read(): newSel29_fu_10924_p3.read());
}

void mnist_fp32::thread_newSel32_fu_13441_p3() {
    newSel32_fu_13441_p3 = (!sel_tmp89_reg_17144.read()[0].is_01())? sc_lv<32>(): ((sel_tmp89_reg_17144.read()[0].to_bool())? tmp_452_fu_13410_p2.read(): tmp_799_fu_13399_p1.read());
}

void mnist_fp32::thread_newSel33_fu_13453_p3() {
    newSel33_fu_13453_p3 = (!sel_tmp86_fu_13431_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp86_fu_13431_p2.read()[0].to_bool())? storemerge9_fu_13403_p3.read(): tmp_797_reg_17132.read());
}

void mnist_fp32::thread_newSel34_fu_13466_p3() {
    newSel34_fu_13466_p3 = (!or_cond27_fu_13448_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond27_fu_13448_p2.read()[0].to_bool())? newSel32_fu_13441_p3.read(): newSel33_fu_13453_p3.read());
}

void mnist_fp32::thread_newSel36_fu_12845_p3() {
    newSel36_fu_12845_p3 = (!sel_tmp98_reg_16968.read()[0].is_01())? sc_lv<32>(): ((sel_tmp98_reg_16968.read()[0].to_bool())? tmp_477_fu_12814_p2.read(): tmp_810_fu_12803_p1.read());
}

void mnist_fp32::thread_newSel37_fu_12857_p3() {
    newSel37_fu_12857_p3 = (!sel_tmp95_fu_12835_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp95_fu_12835_p2.read()[0].to_bool())? storemerge10_fu_12807_p3.read(): tmp_808_reg_16956.read());
}

void mnist_fp32::thread_newSel38_fu_12870_p3() {
    newSel38_fu_12870_p3 = (!or_cond30_fu_12852_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond30_fu_12852_p2.read()[0].to_bool())? newSel36_fu_12845_p3.read(): newSel37_fu_12857_p3.read());
}

void mnist_fp32::thread_newSel40_fu_12135_p3() {
    newSel40_fu_12135_p3 = (!sel_tmp107_fu_12130_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp107_fu_12130_p2.read()[0].to_bool())? tmp_456_fu_12071_p2.read(): tmp_844_fu_12060_p1.read());
}

void mnist_fp32::thread_newSel41_fu_12149_p3() {
    newSel41_fu_12149_p3 = (!sel_tmp104_fu_12107_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp104_fu_12107_p2.read()[0].to_bool())? storemerge11_fu_12064_p3.read(): tmp_842_reg_16757.read());
}

void mnist_fp32::thread_newSel42_fu_12162_p3() {
    newSel42_fu_12162_p3 = (!or_cond33_fu_12143_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond33_fu_12143_p2.read()[0].to_bool())? newSel40_fu_12135_p3.read(): newSel41_fu_12149_p3.read());
}

void mnist_fp32::thread_newSel43_fu_12276_p3() {
    newSel43_fu_12276_p3 = (!sel_tmp116_fu_12271_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp116_fu_12271_p2.read()[0].to_bool())? tmp_484_fu_12212_p2.read(): tmp_850_fu_12201_p1.read());
}

void mnist_fp32::thread_newSel44_fu_12290_p3() {
    newSel44_fu_12290_p3 = (!sel_tmp113_fu_12248_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp113_fu_12248_p2.read()[0].to_bool())? storemerge12_fu_12205_p3.read(): tmp_848_reg_16791.read());
}

void mnist_fp32::thread_newSel45_fu_12303_p3() {
    newSel45_fu_12303_p3 = (!or_cond36_fu_12284_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond36_fu_12284_p2.read()[0].to_bool())? newSel43_fu_12276_p3.read(): newSel44_fu_12290_p3.read());
}

void mnist_fp32::thread_newSel4_fu_5745_p3() {
    newSel4_fu_5745_p3 = (!sel_tmp17_reg_14864.read()[0].is_01())? sc_lv<32>(): ((sel_tmp17_reg_14864.read()[0].to_bool())? tmp_138_fu_5714_p2.read(): tmp_419_fu_5703_p1.read());
}

void mnist_fp32::thread_newSel5_fu_5757_p3() {
    newSel5_fu_5757_p3 = (!sel_tmp14_fu_5735_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp14_fu_5735_p2.read()[0].to_bool())? storemerge2_fu_5707_p3.read(): tmp_414_reg_14852.read());
}

void mnist_fp32::thread_newSel6_fu_5770_p3() {
    newSel6_fu_5770_p3 = (!or_cond3_fu_5752_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond3_fu_5752_p2.read()[0].to_bool())? newSel4_fu_5745_p3.read(): newSel5_fu_5757_p3.read());
}

void mnist_fp32::thread_newSel8_fu_5010_p3() {
    newSel8_fu_5010_p3 = (!sel_tmp26_fu_5005_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp26_fu_5005_p2.read()[0].to_bool())? tmp_147_fu_4946_p2.read(): tmp_536_fu_4935_p1.read());
}

void mnist_fp32::thread_newSel9_fu_5024_p3() {
    newSel9_fu_5024_p3 = (!sel_tmp23_fu_4982_p2.read()[0].is_01())? sc_lv<32>(): ((sel_tmp23_fu_4982_p2.read()[0].to_bool())? storemerge4_fu_4939_p3.read(): tmp_534_reg_14648.read());
}

void mnist_fp32::thread_newSel_fu_3661_p3() {
    newSel_fu_3661_p3 = (!sel_tmp4_reg_14304.read()[0].is_01())? sc_lv<32>(): ((sel_tmp4_reg_14304.read()[0].to_bool())? tmp_85_fu_3630_p2.read(): tmp_155_fu_3619_p1.read());
}

void mnist_fp32::thread_next_mul1_fu_6270_p2() {
    next_mul1_fu_6270_p2 = (!phi_mul1_reg_1543.read().is_01() || !ap_const_lv10_C4.is_01())? sc_lv<10>(): (sc_biguint<10>(phi_mul1_reg_1543.read()) + sc_biguint<10>(ap_const_lv10_C4));
}

void mnist_fp32::thread_next_mul2_fu_7430_p2() {
    next_mul2_fu_7430_p2 = (!phi_mul2_reg_1739.read().is_01() || !ap_const_lv11_C4.is_01())? sc_lv<11>(): (sc_biguint<11>(phi_mul2_reg_1739.read()) + sc_biguint<11>(ap_const_lv11_C4));
}

void mnist_fp32::thread_next_mul3_fu_9833_p2() {
    next_mul3_fu_9833_p2 = (!phi_mul3_reg_2017.read().is_01() || !ap_const_lv9_31.is_01())? sc_lv<9>(): (sc_biguint<9>(phi_mul3_reg_2017.read()) + sc_biguint<9>(ap_const_lv9_31));
}

void mnist_fp32::thread_next_mul4_fu_10964_p2() {
    next_mul4_fu_10964_p2 = (!phi_mul4_reg_2211.read().is_01() || !ap_const_lv10_31.is_01())? sc_lv<10>(): (sc_biguint<10>(phi_mul4_reg_2211.read()) + sc_biguint<10>(ap_const_lv10_31));
}

void mnist_fp32::thread_next_mul_fu_3713_p2() {
    next_mul_fu_3713_p2 = (!phi_mul_reg_1262.read().is_01() || !ap_const_lv12_310.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul_reg_1262.read()) + sc_biguint<12>(ap_const_lv12_310));
}

void mnist_fp32::thread_not_zero1_V_fu_6282_p2() {
    not_zero1_V_fu_6282_p2 = (!p_12_reg_1532.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_12_reg_1532.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_not_zero2_V_fu_7442_p2() {
    not_zero2_V_fu_7442_p2 = (!p_20_reg_1728.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_20_reg_1728.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_not_zero3_V_fu_9845_p2() {
    not_zero3_V_fu_9845_p2 = (!p_36_reg_2006.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_36_reg_2006.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_not_zero4_V_fu_10976_p2() {
    not_zero4_V_fu_10976_p2 = (!p_47_reg_2200.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_47_reg_2200.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_not_zero_V_fu_3725_p2() {
    not_zero_V_fu_3725_p2 = (!p_5_reg_1251.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_5_reg_1251.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_notlhs10_fu_13154_p2() {
    notlhs10_fu_13154_p2 = (!tmp_486_fu_13122_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_486_fu_13122_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs11_fu_13172_p2() {
    notlhs11_fu_13172_p2 = (!tmp_490_fu_13140_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_490_fu_13140_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs12_fu_13733_p2() {
    notlhs12_fu_13733_p2 = (!tmp_497_fu_13701_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_497_fu_13701_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs13_fu_13751_p2() {
    notlhs13_fu_13751_p2 = (!tmp_500_fu_13719_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_500_fu_13719_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs14_fu_13875_p2() {
    notlhs14_fu_13875_p2 = (!tmp_507_fu_13843_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_507_fu_13843_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs15_fu_13893_p2() {
    notlhs15_fu_13893_p2 = (!tmp_509_fu_13861_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_509_fu_13861_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs1_fu_5501_p2() {
    notlhs1_fu_5501_p2 = (!tmp_87_fu_5487_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_87_fu_5487_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs2_fu_6211_p2() {
    notlhs2_fu_6211_p2 = (!tmp_185_fu_6179_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_185_fu_6179_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs3_fu_6229_p2() {
    notlhs3_fu_6229_p2 = (!tmp_191_fu_6197_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_191_fu_6197_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs4_fu_7134_p2() {
    notlhs4_fu_7134_p2 = (!tmp_203_fu_7120_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_203_fu_7120_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs5_fu_9080_p2() {
    notlhs5_fu_9080_p2 = (!tmp_239_fu_9066_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_239_fu_9066_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs6_fu_9774_p2() {
    notlhs6_fu_9774_p2 = (!tmp_342_fu_9742_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_342_fu_9742_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs7_fu_9792_p2() {
    notlhs7_fu_9792_p2 = (!tmp_344_fu_9760_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_344_fu_9760_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs8_fu_10668_p2() {
    notlhs8_fu_10668_p2 = (!tmp_361_fu_10654_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_361_fu_10654_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs9_fu_12601_p2() {
    notlhs9_fu_12601_p2 = (!tmp_453_fu_12587_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_453_fu_12587_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notlhs_fu_3417_p2() {
    notlhs_fu_3417_p2 = (!tmp_14_fu_3403_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_14_fu_3403_p4.read() != ap_const_lv8_FF);
}

void mnist_fp32::thread_notrhs10_fu_13160_p2() {
    notrhs10_fu_13160_p2 = (!tmp_821_fu_13132_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_821_fu_13132_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs11_fu_13178_p2() {
    notrhs11_fu_13178_p2 = (!tmp_822_fu_13150_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_822_fu_13150_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs12_fu_13739_p2() {
    notrhs12_fu_13739_p2 = (!tmp_829_fu_13711_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_829_fu_13711_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs13_fu_13757_p2() {
    notrhs13_fu_13757_p2 = (!tmp_836_fu_13729_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_836_fu_13729_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs14_fu_13881_p2() {
    notrhs14_fu_13881_p2 = (!tmp_851_fu_13853_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_851_fu_13853_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs15_fu_13899_p2() {
    notrhs15_fu_13899_p2 = (!tmp_852_fu_13871_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_852_fu_13871_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs1_fu_5507_p2() {
    notrhs1_fu_5507_p2 = (!tmp_393_fu_5497_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_393_fu_5497_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs2_fu_6217_p2() {
    notrhs2_fu_6217_p2 = (!tmp_577_fu_6189_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_577_fu_6189_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs3_fu_6235_p2() {
    notrhs3_fu_6235_p2 = (!tmp_578_fu_6207_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_578_fu_6207_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs4_fu_7140_p2() {
    notrhs4_fu_7140_p2 = (!tmp_587_fu_7130_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_587_fu_7130_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs5_fu_9086_p2() {
    notrhs5_fu_9086_p2 = (!tmp_655_fu_9076_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_655_fu_9076_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs6_fu_9780_p2() {
    notrhs6_fu_9780_p2 = (!tmp_730_fu_9752_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_730_fu_9752_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs7_fu_9798_p2() {
    notrhs7_fu_9798_p2 = (!tmp_731_fu_9770_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_731_fu_9770_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs8_fu_10674_p2() {
    notrhs8_fu_10674_p2 = (!tmp_738_fu_10664_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_738_fu_10664_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs9_fu_12607_p2() {
    notrhs9_fu_12607_p2 = (!tmp_804_fu_12597_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_804_fu_12597_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_notrhs_fu_3423_p2() {
    notrhs_fu_3423_p2 = (!tmp_135_fu_3413_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_135_fu_3413_p1.read() == ap_const_lv23_0);
}

void mnist_fp32::thread_num_zeros_1_fu_5266_p3() {
    num_zeros_1_fu_5266_p3 = esl_cttz<32,32>(p_Result_11_fu_5256_p4.read());
}

void mnist_fp32::thread_num_zeros_2_fu_6095_p3() {
    num_zeros_2_fu_6095_p3 = esl_cttz<32,32>(p_Result_9_fu_6085_p4.read());
}

void mnist_fp32::thread_num_zeros_3_fu_7894_p3() {
    num_zeros_3_fu_7894_p3 = esl_cttz<32,32>(p_Result_14_fu_7884_p4.read());
}

void mnist_fp32::thread_num_zeros_4_fu_8845_p3() {
    num_zeros_4_fu_8845_p3 = esl_cttz<32,32>(p_Result_22_fu_8835_p4.read());
}

void mnist_fp32::thread_num_zeros_5_fu_9658_p3() {
    num_zeros_5_fu_9658_p3 = esl_cttz<32,32>(p_Result_28_fu_9648_p4.read());
}

void mnist_fp32::thread_num_zeros_6_fu_11392_p3() {
    num_zeros_6_fu_11392_p3 = esl_cttz<32,32>(p_Result_25_fu_11382_p4.read());
}

void mnist_fp32::thread_num_zeros_7_fu_12391_p3() {
    num_zeros_7_fu_12391_p3 = esl_cttz<32,32>(p_Result_47_fu_12381_p4.read());
}

void mnist_fp32::thread_num_zeros_8_fu_13038_p3() {
    num_zeros_8_fu_13038_p3 = esl_cttz<32,32>(p_Result_41_fu_13028_p4.read());
}

void mnist_fp32::thread_num_zeros_9_fu_13595_p3() {
    num_zeros_9_fu_13595_p3 = esl_cttz<32,32>(p_Result_44_fu_13585_p4.read());
}

void mnist_fp32::thread_num_zeros_fu_4250_p3() {
    num_zeros_fu_4250_p3 = esl_cttz<32,32>(p_Result_s_fu_4240_p4.read());
}

void mnist_fp32::thread_or_cond10_fu_5172_p2() {
    or_cond10_fu_5172_p2 = (sel_tmp32_fu_5123_p2.read() | sel_tmp28_fu_5097_p2.read());
}

void mnist_fp32::thread_or_cond11_fu_5186_p2() {
    or_cond11_fu_5186_p2 = (or_cond9_fu_5159_p2.read() | or_cond10_fu_5172_p2.read());
}

void mnist_fp32::thread_or_cond12_fu_7385_p2() {
    or_cond12_fu_7385_p2 = (sel_tmp44_reg_15344.read() | sel_tmp42_fu_7373_p2.read());
}

void mnist_fp32::thread_or_cond13_fu_7397_p2() {
    or_cond13_fu_7397_p2 = (sel_tmp41_fu_7368_p2.read() | sel_tmp37_fu_7357_p2.read());
}

void mnist_fp32::thread_or_cond14_fu_7411_p2() {
    or_cond14_fu_7411_p2 = (or_cond12_fu_7385_p2.read() | or_cond13_fu_7397_p2.read());
}

void mnist_fp32::thread_or_cond15_fu_9331_p2() {
    or_cond15_fu_9331_p2 = (sel_tmp53_reg_15901.read() | sel_tmp51_fu_9319_p2.read());
}

void mnist_fp32::thread_or_cond16_fu_9343_p2() {
    or_cond16_fu_9343_p2 = (sel_tmp50_fu_9314_p2.read() | sel_tmp46_fu_9303_p2.read());
}

void mnist_fp32::thread_or_cond17_fu_9357_p2() {
    or_cond17_fu_9357_p2 = (or_cond15_fu_9331_p2.read() | or_cond16_fu_9343_p2.read());
}

void mnist_fp32::thread_or_cond18_fu_8597_p2() {
    or_cond18_fu_8597_p2 = (sel_tmp62_fu_8584_p2.read() | sel_tmp60_fu_8567_p2.read());
}

void mnist_fp32::thread_or_cond19_fu_8610_p2() {
    or_cond19_fu_8610_p2 = (sel_tmp59_fu_8561_p2.read() | sel_tmp55_fu_8535_p2.read());
}

void mnist_fp32::thread_or_cond1_94_fu_9963_p2() {
    or_cond1_94_fu_9963_p2 = (tmp_288_fu_9957_p2.read() & tmp_286_reg_16077.read());
}

void mnist_fp32::thread_or_cond1_fu_3680_p2() {
    or_cond1_fu_3680_p2 = (sel_tmp9_fu_3651_p2.read() | sel_tmp2_fu_3640_p2.read());
}

void mnist_fp32::thread_or_cond20_fu_8624_p2() {
    or_cond20_fu_8624_p2 = (or_cond18_fu_8597_p2.read() | or_cond19_fu_8610_p2.read());
}

void mnist_fp32::thread_or_cond21_fu_8738_p2() {
    or_cond21_fu_8738_p2 = (sel_tmp71_fu_8725_p2.read() | sel_tmp69_fu_8708_p2.read());
}

void mnist_fp32::thread_or_cond22_fu_8751_p2() {
    or_cond22_fu_8751_p2 = (sel_tmp68_fu_8702_p2.read() | sel_tmp64_fu_8676_p2.read());
}

void mnist_fp32::thread_or_cond23_fu_8765_p2() {
    or_cond23_fu_8765_p2 = (or_cond21_fu_8738_p2.read() | or_cond22_fu_8751_p2.read());
}

void mnist_fp32::thread_or_cond24_fu_10919_p2() {
    or_cond24_fu_10919_p2 = (sel_tmp80_reg_16396.read() | sel_tmp78_fu_10907_p2.read());
}

void mnist_fp32::thread_or_cond25_fu_10931_p2() {
    or_cond25_fu_10931_p2 = (sel_tmp77_fu_10902_p2.read() | sel_tmp73_fu_10891_p2.read());
}

void mnist_fp32::thread_or_cond26_fu_10945_p2() {
    or_cond26_fu_10945_p2 = (or_cond24_fu_10919_p2.read() | or_cond25_fu_10931_p2.read());
}

void mnist_fp32::thread_or_cond27_fu_13448_p2() {
    or_cond27_fu_13448_p2 = (sel_tmp89_reg_17144.read() | sel_tmp87_fu_13436_p2.read());
}

void mnist_fp32::thread_or_cond28_fu_13460_p2() {
    or_cond28_fu_13460_p2 = (sel_tmp86_fu_13431_p2.read() | sel_tmp82_fu_13420_p2.read());
}

void mnist_fp32::thread_or_cond29_fu_13474_p2() {
    or_cond29_fu_13474_p2 = (or_cond27_fu_13448_p2.read() | or_cond28_fu_13460_p2.read());
}

void mnist_fp32::thread_or_cond2_107_fu_11098_p2() {
    or_cond2_107_fu_11098_p2 = (tmp_440_fu_11092_p2.read() & tmp_439_reg_16438.read());
}

void mnist_fp32::thread_or_cond2_fu_3694_p2() {
    or_cond2_fu_3694_p2 = (or_cond_fu_3668_p2.read() | or_cond1_fu_3680_p2.read());
}

void mnist_fp32::thread_or_cond30_fu_12852_p2() {
    or_cond30_fu_12852_p2 = (sel_tmp98_reg_16968.read() | sel_tmp96_fu_12840_p2.read());
}

void mnist_fp32::thread_or_cond31_fu_12864_p2() {
    or_cond31_fu_12864_p2 = (sel_tmp95_fu_12835_p2.read() | sel_tmp91_fu_12824_p2.read());
}

void mnist_fp32::thread_or_cond32_fu_12878_p2() {
    or_cond32_fu_12878_p2 = (or_cond30_fu_12852_p2.read() | or_cond31_fu_12864_p2.read());
}

void mnist_fp32::thread_or_cond33_fu_12143_p2() {
    or_cond33_fu_12143_p2 = (sel_tmp107_fu_12130_p2.read() | sel_tmp105_fu_12113_p2.read());
}

void mnist_fp32::thread_or_cond34_fu_12156_p2() {
    or_cond34_fu_12156_p2 = (sel_tmp104_fu_12107_p2.read() | sel_tmp100_fu_12081_p2.read());
}

void mnist_fp32::thread_or_cond35_fu_12170_p2() {
    or_cond35_fu_12170_p2 = (or_cond33_fu_12143_p2.read() | or_cond34_fu_12156_p2.read());
}

void mnist_fp32::thread_or_cond36_fu_12284_p2() {
    or_cond36_fu_12284_p2 = (sel_tmp116_fu_12271_p2.read() | sel_tmp114_fu_12254_p2.read());
}

void mnist_fp32::thread_or_cond37_fu_12297_p2() {
    or_cond37_fu_12297_p2 = (sel_tmp113_fu_12248_p2.read() | sel_tmp109_fu_12222_p2.read());
}

void mnist_fp32::thread_or_cond38_fu_12311_p2() {
    or_cond38_fu_12311_p2 = (or_cond36_fu_12284_p2.read() | or_cond37_fu_12297_p2.read());
}

void mnist_fp32::thread_or_cond3_fu_5752_p2() {
    or_cond3_fu_5752_p2 = (sel_tmp17_reg_14864.read() | sel_tmp15_fu_5740_p2.read());
}

void mnist_fp32::thread_or_cond4_fu_5764_p2() {
    or_cond4_fu_5764_p2 = (sel_tmp14_fu_5735_p2.read() | sel_tmp10_fu_5724_p2.read());
}

void mnist_fp32::thread_or_cond5_fu_5778_p2() {
    or_cond5_fu_5778_p2 = (or_cond3_fu_5752_p2.read() | or_cond4_fu_5764_p2.read());
}

void mnist_fp32::thread_or_cond6_fu_5018_p2() {
    or_cond6_fu_5018_p2 = (sel_tmp26_fu_5005_p2.read() | sel_tmp24_fu_4988_p2.read());
}

void mnist_fp32::thread_or_cond7_fu_5031_p2() {
    or_cond7_fu_5031_p2 = (sel_tmp23_fu_4982_p2.read() | sel_tmp19_fu_4956_p2.read());
}

void mnist_fp32::thread_or_cond8_62_fu_6418_p2() {
    or_cond8_62_fu_6418_p2 = (tmp129_fu_6412_p2.read() & tmp128_reg_15040.read());
}

void mnist_fp32::thread_or_cond8_fu_5045_p2() {
    or_cond8_fu_5045_p2 = (or_cond6_fu_5018_p2.read() | or_cond7_fu_5031_p2.read());
}

void mnist_fp32::thread_or_cond9_fu_5159_p2() {
    or_cond9_fu_5159_p2 = (sel_tmp35_fu_5146_p2.read() | sel_tmp33_fu_5129_p2.read());
}

void mnist_fp32::thread_or_cond_75_fu_7578_p2() {
    or_cond_75_fu_7578_p2 = (tmp229_fu_7572_p2.read() & tmp228_reg_15386.read());
}

void mnist_fp32::thread_or_cond_fu_3668_p2() {
    or_cond_fu_3668_p2 = (sel_tmp4_reg_14304.read() | sel_tmp_fu_3656_p2.read());
}

void mnist_fp32::thread_p_03_i1_fu_6169_p3() {
    p_03_i1_fu_6169_p3 = (!tmp_140_reg_14967.read()[0].is_01())? sc_lv<32>(): ((tmp_140_reg_14967.read()[0].to_bool())? ap_const_lv32_0: f_4_fu_6165_p1.read());
}

void mnist_fp32::thread_p_03_i1_to_int_fu_6176_p1() {
    p_03_i1_to_int_fu_6176_p1 = p_03_i1_reg_14992.read();
}

void mnist_fp32::thread_p_03_i3_fu_9732_p3() {
    p_03_i3_fu_9732_p3 = (!tmp_298_reg_16004.read()[0].is_01())? sc_lv<32>(): ((tmp_298_reg_16004.read()[0].to_bool())? ap_const_lv32_0: f_8_fu_9728_p1.read());
}

void mnist_fp32::thread_p_03_i3_to_int_fu_9739_p1() {
    p_03_i3_to_int_fu_9739_p1 = p_03_i3_reg_16029.read();
}

void mnist_fp32::thread_p_03_i5_fu_13112_p3() {
    p_03_i5_fu_13112_p3 = (!tmp_416_reg_17035.read()[0].is_01())? sc_lv<32>(): ((tmp_416_reg_17035.read()[0].to_bool())? ap_const_lv32_0: f_14_fu_13108_p1.read());
}

void mnist_fp32::thread_p_03_i5_to_int_fu_13119_p1() {
    p_03_i5_to_int_fu_13119_p1 = p_03_i5_reg_17060.read();
}

void mnist_fp32::thread_p_03_i6_fu_13673_p3() {
    p_03_i6_fu_13673_p3 = (!tmp_449_reg_17203.read()[0].is_01())? sc_lv<32>(): ((tmp_449_reg_17203.read()[0].to_bool())? ap_const_lv32_0: f_16_fu_13669_p1.read());
}

void mnist_fp32::thread_p_Repl2_10_trunc_fu_7940_p2() {
    p_Repl2_10_trunc_fu_7940_p2 = (!tmp_179_fu_7932_p2.read().is_01() || !tmp_180_fu_7937_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_179_fu_7932_p2.read()) + sc_biguint<8>(tmp_180_fu_7937_p1.read()));
}

void mnist_fp32::thread_p_Repl2_13_trunc_fu_8900_p2() {
    p_Repl2_13_trunc_fu_8900_p2 = (!tmp_222_fu_8897_p1.read().is_01() || !tmp_221_fu_8892_p2.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_222_fu_8897_p1.read()) + sc_biguint<8>(tmp_221_fu_8892_p2.read()));
}

void mnist_fp32::thread_p_Repl2_16_trunc_fu_9704_p2() {
    p_Repl2_16_trunc_fu_9704_p2 = (!tmp_309_fu_9701_p1.read().is_01() || !tmp_307_fu_9696_p2.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_309_fu_9701_p1.read()) + sc_biguint<8>(tmp_307_fu_9696_p2.read()));
}

void mnist_fp32::thread_p_Repl2_19_trunc_fu_11438_p2() {
    p_Repl2_19_trunc_fu_11438_p2 = (!tmp_332_fu_11430_p2.read().is_01() || !tmp_333_fu_11435_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_332_fu_11430_p2.read()) + sc_biguint<8>(tmp_333_fu_11435_p1.read()));
}

void mnist_fp32::thread_p_Repl2_1_trunc_fu_4296_p2() {
    p_Repl2_1_trunc_fu_4296_p2 = (!tmp_55_fu_4288_p2.read().is_01() || !tmp_60_fu_4293_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_55_fu_4288_p2.read()) + sc_biguint<8>(tmp_60_fu_4293_p1.read()));
}

void mnist_fp32::thread_p_Repl2_22_trunc_fu_12437_p2() {
    p_Repl2_22_trunc_fu_12437_p2 = (!tmp_386_fu_12434_p1.read().is_01() || !tmp_385_fu_12429_p2.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_386_fu_12434_p1.read()) + sc_biguint<8>(tmp_385_fu_12429_p2.read()));
}

void mnist_fp32::thread_p_Repl2_25_trunc_fu_13084_p2() {
    p_Repl2_25_trunc_fu_13084_p2 = (!tmp_377_fu_13081_p1.read().is_01() || !tmp_376_fu_13076_p2.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_377_fu_13081_p1.read()) + sc_biguint<8>(tmp_376_fu_13076_p2.read()));
}

void mnist_fp32::thread_p_Repl2_28_trunc_fu_13645_p2() {
    p_Repl2_28_trunc_fu_13645_p2 = (!tmp_365_fu_13637_p2.read().is_01() || !tmp_366_fu_13642_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_365_fu_13637_p2.read()) + sc_biguint<8>(tmp_366_fu_13642_p1.read()));
}

void mnist_fp32::thread_p_Repl2_4_trunc_fu_5321_p2() {
    p_Repl2_4_trunc_fu_5321_p2 = (!tmp_78_fu_5318_p1.read().is_01() || !tmp_77_fu_5313_p2.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_78_fu_5318_p1.read()) + sc_biguint<8>(tmp_77_fu_5313_p2.read()));
}

void mnist_fp32::thread_p_Repl2_7_trunc_fu_6141_p2() {
    p_Repl2_7_trunc_fu_6141_p2 = (!tmp_164_fu_6138_p1.read().is_01() || !tmp_163_fu_6133_p2.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_164_fu_6138_p1.read()) + sc_biguint<8>(tmp_163_fu_6133_p2.read()));
}

void mnist_fp32::thread_p_Result_10_fu_6117_p4() {
    p_Result_10_fu_6117_p4 = tmp32_V_s_fu_6113_p1.read().range(30, 23);
}

void mnist_fp32::thread_p_Result_11_fu_5256_p4() {
    p_Result_11_fu_5256_p4 = p_Val2_44_fu_5249_p3.read().range(0, 31);
}

void mnist_fp32::thread_p_Result_12_fu_7916_p4() {
    p_Result_12_fu_7916_p4 = tmp32_V_32_fu_7912_p1.read().range(30, 23);
}

void mnist_fp32::thread_p_Result_13_fu_5334_p5() {
    p_Result_13_fu_5334_p5 = esl_partset<32,32,9,32,32>(tmp32_V_31_reg_14738.read(), tmp_79_fu_5327_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp32::thread_p_Result_14_fu_7884_p4() {
    p_Result_14_fu_7884_p4 = p_Val2_45_fu_7878_p3.read().range(0, 31);
}

void mnist_fp32::thread_p_Result_15_fu_7953_p5() {
    p_Result_15_fu_7953_p5 = esl_partset<32,32,9,32,32>(tmp32_V_32_reg_15500.read(), tmp_181_fu_7946_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp32::thread_p_Result_16_fu_6154_p5() {
    p_Result_16_fu_6154_p5 = esl_partset<32,32,9,32,32>(tmp32_V_s_reg_14982.read(), tmp_171_fu_6147_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp32::thread_p_Result_18_fu_7209_p1() {
    p_Result_18_fu_7209_p1 = esl_zext<54,53>(tmp_151_fu_7202_p3.read());
}

void mnist_fp32::thread_p_Result_19_fu_8876_p4() {
    p_Result_19_fu_8876_p4 = tmp32_V_33_fu_8872_p1.read().range(30, 23);
}

void mnist_fp32::thread_p_Result_1_fu_3492_p1() {
    p_Result_1_fu_3492_p1 = esl_zext<54,53>(tmp_27_fu_3485_p3.read());
}

void mnist_fp32::thread_p_Result_20_fu_8337_p1() {
    p_Result_20_fu_8337_p1 = esl_zext<54,53>(tmp_262_fu_8330_p3.read());
}

void mnist_fp32::thread_p_Result_21_fu_8422_p1() {
    p_Result_21_fu_8422_p1 = esl_zext<54,53>(tmp_308_fu_8415_p3.read());
}

void mnist_fp32::thread_p_Result_22_fu_8835_p4() {
    p_Result_22_fu_8835_p4 = p_Val2_46_fu_8828_p3.read().range(0, 31);
}

void mnist_fp32::thread_p_Result_24_fu_8913_p5() {
    p_Result_24_fu_8913_p5 = esl_partset<32,32,9,32,32>(tmp32_V_33_reg_15775.read(), tmp_223_fu_8906_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp32::thread_p_Result_25_fu_11382_p4() {
    p_Result_25_fu_11382_p4 = p_Val2_47_fu_11376_p3.read().range(0, 31);
}

void mnist_fp32::thread_p_Result_26_fu_11451_p5() {
    p_Result_26_fu_11451_p5 = esl_partset<32,32,9,32,32>(tmp32_V_34_reg_16552.read(), tmp_334_fu_11444_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp32::thread_p_Result_27_fu_9155_p1() {
    p_Result_27_fu_9155_p1 = esl_zext<54,53>(tmp_216_fu_9148_p3.read());
}

void mnist_fp32::thread_p_Result_28_fu_9648_p4() {
    p_Result_28_fu_9648_p4 = tmp_V_5_fu_9643_p3.read().range(0, 31);
}

void mnist_fp32::thread_p_Result_29_fu_11883_p1() {
    p_Result_29_fu_11883_p1 = esl_zext<54,53>(tmp_423_fu_11876_p3.read());
}

void mnist_fp32::thread_p_Result_30_fu_11968_p1() {
    p_Result_30_fu_11968_p1 = esl_zext<54,53>(tmp_463_fu_11961_p3.read());
}

void mnist_fp32::thread_p_Result_31_fu_9680_p4() {
    p_Result_31_fu_9680_p4 = tmp32_V_10_fu_9676_p1.read().range(30, 23);
}

void mnist_fp32::thread_p_Result_32_fu_9717_p5() {
    p_Result_32_fu_9717_p5 = esl_partset<32,32,9,32,32>(tmp32_V_10_reg_16019.read(), tmp_313_fu_9710_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp32::thread_p_Result_33_fu_11414_p4() {
    p_Result_33_fu_11414_p4 = tmp32_V_34_fu_11410_p1.read().range(30, 23);
}

void mnist_fp32::thread_p_Result_35_fu_10743_p1() {
    p_Result_35_fu_10743_p1 = esl_zext<54,53>(tmp_299_fu_10736_p3.read());
}

void mnist_fp32::thread_p_Result_37_fu_13266_p1() {
    p_Result_37_fu_13266_p1 = esl_zext<54,53>(tmp_245_fu_13259_p3.read());
}

void mnist_fp32::thread_p_Result_39_fu_12676_p1() {
    p_Result_39_fu_12676_p1 = esl_zext<54,53>(tmp_381_fu_12669_p3.read());
}

void mnist_fp32::thread_p_Result_40_fu_12413_p4() {
    p_Result_40_fu_12413_p4 = tmp32_V_35_fu_12409_p1.read().range(30, 23);
}

void mnist_fp32::thread_p_Result_41_fu_13028_p4() {
    p_Result_41_fu_13028_p4 = tmp_V_8_fu_13023_p3.read().range(0, 31);
}

void mnist_fp32::thread_p_Result_42_fu_13060_p4() {
    p_Result_42_fu_13060_p4 = tmp32_V_19_fu_13056_p1.read().range(30, 23);
}

void mnist_fp32::thread_p_Result_43_fu_13097_p5() {
    p_Result_43_fu_13097_p5 = esl_partset<32,32,9,32,32>(tmp32_V_19_reg_17050.read(), tmp_378_fu_13090_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp32::thread_p_Result_44_fu_13585_p4() {
    p_Result_44_fu_13585_p4 = tmp_V_9_fu_13580_p3.read().range(0, 31);
}

void mnist_fp32::thread_p_Result_45_fu_13621_p4() {
    p_Result_45_fu_13621_p4 = tmp32_V_21_fu_13617_p1.read().range(30, 23);
}

void mnist_fp32::thread_p_Result_46_fu_13658_p5() {
    p_Result_46_fu_13658_p5 = esl_partset<32,32,9,32,32>(tmp32_V_21_reg_17223.read(), tmp_372_fu_13651_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp32::thread_p_Result_47_fu_12381_p4() {
    p_Result_47_fu_12381_p4 = p_Val2_48_fu_12374_p3.read().range(0, 31);
}

void mnist_fp32::thread_p_Result_48_fu_12450_p5() {
    p_Result_48_fu_12450_p5 = esl_partset<32,32,9,32,32>(tmp32_V_35_reg_16842.read(), tmp_387_fu_12443_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp32::thread_p_Result_4_fu_5576_p1() {
    p_Result_4_fu_5576_p1 = esl_zext<54,53>(tmp_69_fu_5569_p3.read());
}

void mnist_fp32::thread_p_Result_5_fu_4309_p5() {
    p_Result_5_fu_4309_p5 = esl_partset<32,32,9,32,32>(tmp32_V_30_reg_14458.read(), tmp_61_fu_4302_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp32::thread_p_Result_6_fu_4758_p1() {
    p_Result_6_fu_4758_p1 = esl_zext<54,53>(tmp_108_fu_4751_p3.read());
}

void mnist_fp32::thread_p_Result_7_fu_4272_p4() {
    p_Result_7_fu_4272_p4 = tmp32_V_30_fu_4268_p1.read().range(30, 23);
}

void mnist_fp32::thread_p_Result_8_fu_4843_p1() {
    p_Result_8_fu_4843_p1 = esl_zext<54,53>(tmp_152_fu_4836_p3.read());
}

void mnist_fp32::thread_p_Result_9_fu_6085_p4() {
    p_Result_9_fu_6085_p4 = tmp_V_2_fu_6080_p3.read().range(0, 31);
}

void mnist_fp32::thread_p_Result_s_50_fu_5297_p4() {
    p_Result_s_50_fu_5297_p4 = tmp32_V_31_fu_5293_p1.read().range(30, 23);
}

void mnist_fp32::thread_p_Result_s_fu_4240_p4() {
    p_Result_s_fu_4240_p4 = p_Val2_43_fu_4234_p3.read().range(0, 31);
}

void mnist_fp32::thread_p_Val2_13_fu_12331_p0() {
    p_Val2_13_fu_12331_p0 = p_Val2_35_reg_16802.read();
}

void mnist_fp32::thread_p_Val2_13_fu_12331_p1() {
    p_Val2_13_fu_12331_p1 = p_Val2_36_reg_16807.read();
}

void mnist_fp32::thread_p_Val2_13_fu_12331_p2() {
    p_Val2_13_fu_12331_p2 = (!p_Val2_13_fu_12331_p0.read().is_01() || !p_Val2_13_fu_12331_p1.read().is_01())? sc_lv<62>(): sc_bigint<32>(p_Val2_13_fu_12331_p0.read()) * sc_bigint<32>(p_Val2_13_fu_12331_p1.read());
}

void mnist_fp32::thread_p_Val2_14_fu_5051_p3() {
    p_Val2_14_fu_5051_p3 = (!or_cond8_fu_5045_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond8_fu_5045_p2.read()[0].to_bool())? newSel10_fu_5037_p3.read(): ap_const_lv32_0);
}

void mnist_fp32::thread_p_Val2_15_fu_5192_p3() {
    p_Val2_15_fu_5192_p3 = (!or_cond11_fu_5186_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond11_fu_5186_p2.read()[0].to_bool())? newSel13_fu_5178_p3.read(): ap_const_lv32_0);
}

void mnist_fp32::thread_p_Val2_17_fu_5220_p2() {
    p_Val2_17_fu_5220_p2 = (!p_Val2_9_reg_14703.read().is_01() || !p_Val2_3_fu_5212_p3.read().is_01())? sc_lv<62>(): (sc_biguint<62>(p_Val2_9_reg_14703.read()) + sc_biguint<62>(p_Val2_3_fu_5212_p3.read()));
}

void mnist_fp32::thread_p_Val2_22_fu_12337_p3() {
    p_Val2_22_fu_12337_p3 = esl_concat<32,30>(p_Val2_21_reg_2340.read(), ap_const_lv30_0);
}

void mnist_fp32::thread_p_Val2_24_fu_12345_p2() {
    p_Val2_24_fu_12345_p2 = (!p_Val2_13_reg_16812.read().is_01() || !p_Val2_22_fu_12337_p3.read().is_01())? sc_lv<62>(): (sc_biguint<62>(p_Val2_13_reg_16812.read()) + sc_biguint<62>(p_Val2_22_fu_12337_p3.read()));
}

void mnist_fp32::thread_p_Val2_26_fu_8630_p3() {
    p_Val2_26_fu_8630_p3 = (!or_cond20_fu_8624_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond20_fu_8624_p2.read()[0].to_bool())? newSel24_fu_8616_p3.read(): ap_const_lv32_0);
}

void mnist_fp32::thread_p_Val2_27_fu_8771_p3() {
    p_Val2_27_fu_8771_p3 = (!or_cond23_fu_8765_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond23_fu_8765_p2.read()[0].to_bool())? newSel27_fu_8757_p3.read(): ap_const_lv32_0);
}

void mnist_fp32::thread_p_Val2_2_fu_8799_p2() {
    p_Val2_2_fu_8799_p2 = (!p_Val2_s_82_reg_15740.read().is_01() || !p_Val2_7_fu_8791_p3.read().is_01())? sc_lv<62>(): (sc_biguint<62>(p_Val2_s_82_reg_15740.read()) + sc_biguint<62>(p_Val2_7_fu_8791_p3.read()));
}

void mnist_fp32::thread_p_Val2_35_fu_12176_p3() {
    p_Val2_35_fu_12176_p3 = (!or_cond35_fu_12170_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond35_fu_12170_p2.read()[0].to_bool())? newSel42_fu_12162_p3.read(): ap_const_lv32_0);
}

void mnist_fp32::thread_p_Val2_36_fu_12317_p3() {
    p_Val2_36_fu_12317_p3 = (!or_cond38_fu_12311_p2.read()[0].is_01())? sc_lv<32>(): ((or_cond38_fu_12311_p2.read()[0].to_bool())? newSel45_fu_12303_p3.read(): ap_const_lv32_0);
}

void mnist_fp32::thread_p_Val2_3_fu_5212_p3() {
    p_Val2_3_fu_5212_p3 = esl_concat<32,30>(p_Val2_12_reg_1396.read(), ap_const_lv30_0);
}

void mnist_fp32::thread_p_Val2_43_fu_4234_p3() {
    p_Val2_43_fu_4234_p3 = (!is_neg_fu_4227_p3.read()[0].is_01())? sc_lv<32>(): ((is_neg_fu_4227_p3.read()[0].to_bool())? tmp_50_reg_14438.read(): p_Val2_s_reg_14429.read());
}

void mnist_fp32::thread_p_Val2_44_fu_5249_p3() {
    p_Val2_44_fu_5249_p3 = (!is_neg_1_fu_5241_p3.read()[0].is_01())? sc_lv<32>(): ((is_neg_1_fu_5241_p3.read()[0].to_bool())? tmp_70_reg_14530.read(): p_Val2_5_reg_1350.read());
}

void mnist_fp32::thread_p_Val2_45_fu_7878_p3() {
    p_Val2_45_fu_7878_p3 = (!is_neg_2_fu_7871_p3.read()[0].is_01())? sc_lv<32>(): ((is_neg_2_fu_7871_p3.read()[0].to_bool())? tmp_176_reg_15480.read(): p_Val2_25_reg_15471.read());
}

void mnist_fp32::thread_p_Val2_46_fu_8828_p3() {
    p_Val2_46_fu_8828_p3 = (!is_neg_3_fu_8820_p3.read()[0].is_01())? sc_lv<32>(): ((is_neg_3_fu_8820_p3.read()[0].to_bool())? tmp_217_reg_15567.read(): p_Val2_1_reg_1823.read());
}

void mnist_fp32::thread_p_Val2_47_fu_11376_p3() {
    p_Val2_47_fu_11376_p3 = (!is_neg_4_fu_11369_p3.read()[0].is_01())? sc_lv<32>(): ((is_neg_4_fu_11369_p3.read()[0].to_bool())? tmp_329_reg_16532.read(): p_Val2_34_reg_16523.read());
}

void mnist_fp32::thread_p_Val2_48_fu_12374_p3() {
    p_Val2_48_fu_12374_p3 = (!is_neg_5_fu_12366_p3.read()[0].is_01())? sc_lv<32>(): ((is_neg_5_fu_12366_p3.read()[0].to_bool())? tmp_382_reg_16634.read(): p_Val2_10_reg_2294.read());
}

void mnist_fp32::thread_p_Val2_7_fu_8791_p3() {
    p_Val2_7_fu_8791_p3 = esl_concat<32,30>(p_Val2_6_reg_1870.read(), ap_const_lv30_0);
}

void mnist_fp32::thread_p_Val2_9_fu_5206_p0() {
    p_Val2_9_fu_5206_p0 = p_Val2_14_reg_14693.read();
}

void mnist_fp32::thread_p_Val2_9_fu_5206_p1() {
    p_Val2_9_fu_5206_p1 = p_Val2_15_reg_14698.read();
}

void mnist_fp32::thread_p_Val2_9_fu_5206_p2() {
    p_Val2_9_fu_5206_p2 = (!p_Val2_9_fu_5206_p0.read().is_01() || !p_Val2_9_fu_5206_p1.read().is_01())? sc_lv<62>(): sc_bigint<32>(p_Val2_9_fu_5206_p0.read()) * sc_bigint<32>(p_Val2_9_fu_5206_p1.read());
}

void mnist_fp32::thread_p_Val2_s_82_fu_8785_p0() {
    p_Val2_s_82_fu_8785_p0 = p_Val2_26_reg_15730.read();
}

void mnist_fp32::thread_p_Val2_s_82_fu_8785_p1() {
    p_Val2_s_82_fu_8785_p1 = p_Val2_27_reg_15735.read();
}

void mnist_fp32::thread_p_Val2_s_82_fu_8785_p2() {
    p_Val2_s_82_fu_8785_p2 = (!p_Val2_s_82_fu_8785_p0.read().is_01() || !p_Val2_s_82_fu_8785_p1.read().is_01())? sc_lv<62>(): sc_bigint<32>(p_Val2_s_82_fu_8785_p0.read()) * sc_bigint<32>(p_Val2_s_82_fu_8785_p1.read());
}

void mnist_fp32::thread_p_a_assign_load_to_i_fu_13697_p1() {
    p_a_assign_load_to_i_fu_13697_p1 = reg_2673.read();
}

void mnist_fp32::thread_p_shl100_cast_fu_10392_p1() {
    p_shl100_cast_fu_10392_p1 = esl_zext<8,7>(tmp_749_fu_10384_p3.read());
}

void mnist_fp32::thread_p_shl101_cast_fu_12501_p1() {
    p_shl101_cast_fu_12501_p1 = esl_zext<9,8>(tmp_751_fu_12493_p3.read());
}

void mnist_fp32::thread_p_shl102_cast_fu_11563_p3() {
    p_shl102_cast_fu_11563_p3 = esl_concat<8,3>(tmp_754_fu_11559_p1.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl103_cast_fu_10450_p3() {
    p_shl103_cast_fu_10450_p3 = esl_concat<8,3>(tmp_756_fu_10441_p2.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl104_cast_fu_10481_p3() {
    p_shl104_cast_fu_10481_p3 = esl_concat<10,2>(tmp_760_fu_10477_p1.read(), ap_const_lv2_0);
}

void mnist_fp32::thread_p_shl105_cast_fu_11303_p1() {
    p_shl105_cast_fu_11303_p1 = esl_zext<8,7>(tmp_779_fu_11296_p3.read());
}

void mnist_fp32::thread_p_shl106_cast_fu_11333_p3() {
    p_shl106_cast_fu_11333_p3 = esl_concat<8,3>(tmp_782_reg_16513.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl107_cast_fu_12921_p1() {
    p_shl107_cast_fu_12921_p1 = esl_zext<9,8>(tmp_785_fu_12913_p3.read());
}

void mnist_fp32::thread_p_shl108_cast_fu_12544_p3() {
    p_shl108_cast_fu_12544_p3 = esl_concat<8,3>(tmp_788_fu_12540_p1.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl109_cast_fu_12964_p3() {
    p_shl109_cast_fu_12964_p3 = esl_concat<8,3>(tmp_801_fu_12960_p1.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl111_cast_fu_11648_p1() {
    p_shl111_cast_fu_11648_p1 = esl_zext<9,8>(tmp_816_fu_11640_p3.read());
}

void mnist_fp32::thread_p_shl112_cast_fu_11707_p3() {
    p_shl112_cast_fu_11707_p3 = esl_concat<9,3>(tmp_823_fu_11698_p2.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl113_cast_fu_11738_p3() {
    p_shl113_cast_fu_11738_p3 = esl_concat<11,2>(tmp_827_fu_11734_p1.read(), ap_const_lv2_0);
}

void mnist_fp32::thread_p_shl114_cast_fu_13534_p1() {
    p_shl114_cast_fu_13534_p1 = esl_zext<9,8>(tmp_830_fu_13526_p3.read());
}

void mnist_fp32::thread_p_shl115_cast_fu_13546_p1() {
    p_shl115_cast_fu_13546_p1 = esl_zext<9,6>(tmp_831_fu_13538_p3.read());
}

void mnist_fp32::thread_p_shl12_cast_fu_3063_p1() {
    p_shl12_cast_fu_3063_p1 = esl_zext<9,8>(tmp_8_fu_3055_p3.read());
}

void mnist_fp32::thread_p_shl14_cast1_fu_3037_p1() {
    p_shl14_cast1_fu_3037_p1 = esl_zext<9,5>(tmp_3_fu_3029_p3.read());
}

void mnist_fp32::thread_p_shl14_cast_fu_3041_p1() {
    p_shl14_cast_fu_3041_p1 = esl_zext<6,5>(tmp_3_fu_3029_p3.read());
}

void mnist_fp32::thread_p_shl15_cast_fu_3102_p3() {
    p_shl15_cast_fu_3102_p3 = esl_concat<8,5>(tmp_48_fu_3098_p1.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_p_shl16_cast_fu_3118_p1() {
    p_shl16_cast_fu_3118_p1 = esl_sext<13,12>(tmp_54_fu_3110_p3.read());
}

void mnist_fp32::thread_p_shl18_cast_fu_3170_p1() {
    p_shl18_cast_fu_3170_p1 = esl_zext<11,10>(tmp_162_fu_3162_p3.read());
}

void mnist_fp32::thread_p_shl19_cast_fu_3182_p1() {
    p_shl19_cast_fu_3182_p1 = esl_zext<11,6>(tmp_165_fu_3174_p3.read());
}

void mnist_fp32::thread_p_shl1_cast_fu_3830_p1() {
    p_shl1_cast_fu_3830_p1 = esl_zext<11,7>(p_shl1_fu_3822_p3.read());
}

void mnist_fp32::thread_p_shl1_fu_3822_p3() {
    p_shl1_fu_3822_p3 = esl_concat<5,2>(p_9_reg_1273.read(), ap_const_lv2_0);
}

void mnist_fp32::thread_p_shl20_cast_fu_3297_p1() {
    p_shl20_cast_fu_3297_p1 = esl_zext<9,8>(tmp_39_fu_3289_p3.read());
}

void mnist_fp32::thread_p_shl21_cast_fu_3309_p1() {
    p_shl21_cast_fu_3309_p1 = esl_zext<9,5>(tmp_44_fu_3301_p3.read());
}

void mnist_fp32::thread_p_shl22_cast_fu_3348_p3() {
    p_shl22_cast_fu_3348_p3 = esl_concat<8,5>(tmp_102_fu_3344_p1.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_p_shl23_cast_fu_3364_p1() {
    p_shl23_cast_fu_3364_p1 = esl_sext<13,12>(tmp_103_fu_3356_p3.read());
}

void mnist_fp32::thread_p_shl24_cast_fu_3739_p1() {
    p_shl24_cast_fu_3739_p1 = esl_zext<9,8>(tmp_97_fu_3731_p3.read());
}

void mnist_fp32::thread_p_shl25_cast_fu_3751_p1() {
    p_shl25_cast_fu_3751_p1 = esl_zext<9,4>(tmp_98_fu_3743_p3.read());
}

void mnist_fp32::thread_p_shl26_cast_fu_3784_p3() {
    p_shl26_cast_fu_3784_p3 = esl_concat<8,5>(tmp_128_fu_3780_p1.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_p_shl27_cast_fu_3800_p1() {
    p_shl27_cast_fu_3800_p1 = esl_sext<13,11>(tmp_131_fu_3792_p3.read());
}

void mnist_fp32::thread_p_shl28_cast_fu_4180_p3() {
    p_shl28_cast_fu_4180_p3 = esl_concat<8,5>(tmp_348_reg_14419.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_p_shl29_cast_fu_4194_p1() {
    p_shl29_cast_fu_4194_p1 = esl_sext<13,11>(tmp_350_fu_4187_p3.read());
}

void mnist_fp32::thread_p_shl2_cast_fu_6353_p1() {
    p_shl2_cast_fu_6353_p1 = esl_zext<9,8>(p_shl2_fu_6345_p3.read());
}

void mnist_fp32::thread_p_shl2_fu_6345_p3() {
    p_shl2_fu_6345_p3 = esl_concat<4,4>(tmp_483_fu_6341_p1.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_p_shl30_cast_fu_4142_p1() {
    p_shl30_cast_fu_4142_p1 = esl_zext<8,7>(tmp_328_fu_4135_p3.read());
}

void mnist_fp32::thread_p_shl31_cast_fu_4153_p1() {
    p_shl31_cast_fu_4153_p1 = esl_zext<8,4>(tmp_331_fu_4146_p3.read());
}

void mnist_fp32::thread_p_shl32_cast_fu_4374_p1() {
    p_shl32_cast_fu_4374_p1 = esl_zext<9,8>(tmp_120_fu_4366_p3.read());
}

void mnist_fp32::thread_p_shl34_cast_fu_5381_p1() {
    p_shl34_cast_fu_5381_p1 = esl_zext<9,8>(tmp_183_fu_5373_p3.read());
}

void mnist_fp32::thread_p_shl35_cast_fu_5393_p1() {
    p_shl35_cast_fu_5393_p1 = esl_zext<9,5>(tmp_184_fu_5385_p3.read());
}

void mnist_fp32::thread_p_shl36_cast_fu_4413_p3() {
    p_shl36_cast_fu_4413_p3 = esl_concat<8,5>(tmp_188_fu_4409_p1.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_p_shl37_cast_fu_4429_p1() {
    p_shl37_cast_fu_4429_p1 = esl_sext<13,12>(tmp_190_fu_4421_p3.read());
}

void mnist_fp32::thread_p_shl38_cast_fu_5813_p1() {
    p_shl38_cast_fu_5813_p1 = esl_zext<9,8>(tmp_244_fu_5805_p3.read());
}

void mnist_fp32::thread_p_shl39_cast_fu_5825_p1() {
    p_shl39_cast_fu_5825_p1 = esl_zext<9,5>(tmp_249_fu_5817_p3.read());
}

void mnist_fp32::thread_p_shl3_cast_fu_6363_p1() {
    p_shl3_cast_fu_6363_p1 = esl_zext<9,5>(tmp_487_fu_6357_p2.read());
}

void mnist_fp32::thread_p_shl3_fu_6867_p1() {
    p_shl3_fu_6867_p1 = esl_zext<64,9>(tmp_596_fu_6859_p3.read());
}

void mnist_fp32::thread_p_shl40_cast_fu_5847_p1() {
    p_shl40_cast_fu_5847_p1 = esl_zext<8,7>(tmp_251_fu_5839_p3.read());
}

void mnist_fp32::thread_p_shl41_cast_fu_5859_p1() {
    p_shl41_cast_fu_5859_p1 = esl_zext<8,4>(tmp_256_fu_5851_p3.read());
}

void mnist_fp32::thread_p_shl42_cast_fu_5432_p3() {
    p_shl42_cast_fu_5432_p3 = esl_concat<8,5>(tmp_261_fu_5428_p1.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_p_shl43_cast_fu_5448_p1() {
    p_shl43_cast_fu_5448_p1 = esl_sext<13,12>(tmp_264_fu_5440_p3.read());
}

void mnist_fp32::thread_p_shl44_cast_fu_5898_p3() {
    p_shl44_cast_fu_5898_p3 = esl_concat<7,4>(tmp_374_fu_5894_p1.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_p_shl45_cast_fu_5914_p1() {
    p_shl45_cast_fu_5914_p1 = esl_sext<11,10>(tmp_375_fu_5906_p3.read());
}

void mnist_fp32::thread_p_shl47_cast_fu_4502_p1() {
    p_shl47_cast_fu_4502_p1 = esl_zext<9,8>(tmp_447_fu_4494_p3.read());
}

void mnist_fp32::thread_p_shl48_cast_fu_4514_p1() {
    p_shl48_cast_fu_4514_p1 = esl_zext<9,4>(tmp_451_fu_4506_p3.read());
}

void mnist_fp32::thread_p_shl49_cast_fu_6745_p1() {
    p_shl49_cast_fu_6745_p1 = esl_zext<9,8>(tmp_462_fu_6737_p3.read());
}

void mnist_fp32::thread_p_shl4_cast_fu_7513_p1() {
    p_shl4_cast_fu_7513_p1 = esl_zext<9,8>(p_shl4_fu_7505_p3.read());
}

void mnist_fp32::thread_p_shl4_fu_7505_p3() {
    p_shl4_fu_7505_p3 = esl_concat<4,4>(tmp_584_fu_7501_p1.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_p_shl50_cast_fu_6757_p1() {
    p_shl50_cast_fu_6757_p1 = esl_zext<9,5>(tmp_471_fu_6749_p3.read());
}

void mnist_fp32::thread_p_shl51_cast_fu_4569_p3() {
    p_shl51_cast_fu_4569_p3 = esl_concat<8,5>(tmp_499_fu_4565_p1.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_p_shl52_cast_fu_4612_p3() {
    p_shl52_cast_fu_4612_p3 = esl_concat<7,2>(tmp_517_fu_4608_p1.read(), ap_const_lv2_0);
}

void mnist_fp32::thread_p_shl53_cast_fu_4585_p1() {
    p_shl53_cast_fu_4585_p1 = esl_sext<13,11>(tmp_501_fu_4577_p3.read());
}

void mnist_fp32::thread_p_shl54_cast_fu_7014_p1() {
    p_shl54_cast_fu_7014_p1 = esl_zext<9,8>(tmp_519_fu_7006_p3.read());
}

void mnist_fp32::thread_p_shl55_cast_fu_7026_p1() {
    p_shl55_cast_fu_7026_p1 = esl_zext<9,5>(tmp_520_fu_7018_p3.read());
}

void mnist_fp32::thread_p_shl56_cast_fu_6796_p3() {
    p_shl56_cast_fu_6796_p3 = esl_concat<8,4>(tmp_523_fu_6792_p1.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_p_shl57_cast_fu_6812_p1() {
    p_shl57_cast_fu_6812_p1 = esl_sext<12,11>(tmp_524_fu_6804_p3.read());
}

void mnist_fp32::thread_p_shl58_cast_fu_6000_p3() {
    p_shl58_cast_fu_6000_p3 = esl_concat<8,5>(tmp_527_fu_5996_p1.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_p_shl59_cast_fu_6016_p1() {
    p_shl59_cast_fu_6016_p1 = esl_sext<13,12>(tmp_528_fu_6008_p3.read());
}

void mnist_fp32::thread_p_shl5_cast_fu_7523_p1() {
    p_shl5_cast_fu_7523_p1 = esl_zext<9,5>(tmp_585_fu_7517_p2.read());
}

void mnist_fp32::thread_p_shl5_fu_8136_p1() {
    p_shl5_fu_8136_p1 = esl_zext<64,10>(tmp_665_fu_8128_p3.read());
}

void mnist_fp32::thread_p_shl60_cast_fu_6626_p1() {
    p_shl60_cast_fu_6626_p1 = esl_zext<7,6>(tmp_560_fu_6619_p3.read());
}

void mnist_fp32::thread_p_shl61_cast_fu_6637_p1() {
    p_shl61_cast_fu_6637_p1 = esl_zext<7,3>(tmp_561_fu_6630_p3.read());
}

void mnist_fp32::thread_p_shl62_cast_fu_6664_p3() {
    p_shl62_cast_fu_6664_p3 = esl_concat<7,4>(tmp_564_reg_15115.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_p_shl63_cast_fu_6678_p1() {
    p_shl63_cast_fu_6678_p1 = esl_sext<11,9>(tmp_565_fu_6671_p3.read());
}

void mnist_fp32::thread_p_shl64_cast_fu_7065_p3() {
    p_shl64_cast_fu_7065_p3 = esl_concat<8,4>(tmp_571_fu_7061_p1.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_p_shl65_cast_fu_7081_p1() {
    p_shl65_cast_fu_7081_p1 = esl_sext<12,11>(tmp_572_fu_7073_p3.read());
}

void mnist_fp32::thread_p_shl66_cast_fu_8014_p1() {
    p_shl66_cast_fu_8014_p1 = esl_zext<9,8>(tmp_580_fu_8006_p3.read());
}

void mnist_fp32::thread_p_shl67_cast_fu_8026_p1() {
    p_shl67_cast_fu_8026_p1 = esl_zext<9,5>(tmp_581_fu_8018_p3.read());
}

void mnist_fp32::thread_p_shl69_cast_fu_8960_p1() {
    p_shl69_cast_fu_8960_p1 = esl_zext<9,8>(tmp_598_fu_8952_p3.read());
}

void mnist_fp32::thread_p_shl6_cast_fu_2712_p1() {
    p_shl6_cast_fu_2712_p1 = esl_zext<11,10>(tmp_1_fu_2704_p3.read());
}

void mnist_fp32::thread_p_shl6_fu_10374_p1() {
    p_shl6_fu_10374_p1 = esl_zext<64,11>(tmp_747_fu_10366_p3.read());
}

void mnist_fp32::thread_p_shl70_cast_fu_8972_p1() {
    p_shl70_cast_fu_8972_p1 = esl_zext<9,5>(tmp_599_fu_8964_p3.read());
}

void mnist_fp32::thread_p_shl71_cast_fu_8065_p3() {
    p_shl71_cast_fu_8065_p3 = esl_concat<8,4>(tmp_602_fu_8061_p1.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_p_shl72_cast_fu_8081_p1() {
    p_shl72_cast_fu_8081_p1 = esl_sext<12,11>(tmp_603_fu_8073_p3.read());
}

void mnist_fp32::thread_p_shl73_cast_fu_6930_p3() {
    p_shl73_cast_fu_6930_p3 = esl_concat<8,2>(tmp_607_fu_6926_p1.read(), ap_const_lv2_0);
}

void mnist_fp32::thread_p_shl74_cast_fu_7786_p1() {
    p_shl74_cast_fu_7786_p1 = esl_zext<8,7>(tmp_627_fu_7779_p3.read());
}

void mnist_fp32::thread_p_shl75_cast_fu_7797_p1() {
    p_shl75_cast_fu_7797_p1 = esl_zext<8,4>(tmp_628_fu_7790_p3.read());
}

void mnist_fp32::thread_p_shl76_cast_fu_7824_p3() {
    p_shl76_cast_fu_7824_p3 = esl_concat<8,4>(tmp_631_reg_15461.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_p_shl77_cast_fu_7838_p1() {
    p_shl77_cast_fu_7838_p1 = esl_sext<12,10>(tmp_632_fu_7831_p3.read());
}

void mnist_fp32::thread_p_shl78_cast_fu_9396_p1() {
    p_shl78_cast_fu_9396_p1 = esl_zext<9,8>(tmp_635_fu_9388_p3.read());
}

void mnist_fp32::thread_p_shl79_cast_fu_9408_p1() {
    p_shl79_cast_fu_9408_p1 = esl_zext<9,5>(tmp_636_fu_9400_p3.read());
}

void mnist_fp32::thread_p_shl7_cast_fu_2724_p1() {
    p_shl7_cast_fu_2724_p1 = esl_zext<11,6>(tmp_7_fu_2716_p3.read());
}

void mnist_fp32::thread_p_shl7_fu_11630_p1() {
    p_shl7_fu_11630_p1 = esl_zext<64,12>(tmp_814_fu_11622_p3.read());
}

void mnist_fp32::thread_p_shl80_cast_fu_9430_p1() {
    p_shl80_cast_fu_9430_p1 = esl_zext<8,7>(tmp_638_fu_9422_p3.read());
}

void mnist_fp32::thread_p_shl81_cast_fu_9011_p3() {
    p_shl81_cast_fu_9011_p3 = esl_concat<8,4>(tmp_641_fu_9007_p1.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_p_shl82_cast_fu_9027_p1() {
    p_shl82_cast_fu_9027_p1 = esl_sext<12,11>(tmp_642_fu_9019_p3.read());
}

void mnist_fp32::thread_p_shl83_cast_fu_9863_p1() {
    p_shl83_cast_fu_9863_p1 = esl_zext<8,7>(tmp_649_fu_9855_p3.read());
}

void mnist_fp32::thread_p_shl84_cast_fu_9473_p3() {
    p_shl84_cast_fu_9473_p3 = esl_concat<7,3>(tmp_652_fu_9469_p1.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl87_cast_fu_9898_p3() {
    p_shl87_cast_fu_9898_p3 = esl_concat<8,3>(tmp_669_fu_9889_p2.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl88_cast_fu_8191_p3() {
    p_shl88_cast_fu_8191_p3 = esl_concat<9,2>(tmp_674_fu_8187_p1.read(), ap_const_lv2_0);
}

void mnist_fp32::thread_p_shl89_cast_fu_10568_p1() {
    p_shl89_cast_fu_10568_p1 = esl_zext<9,8>(tmp_676_fu_10560_p3.read());
}

void mnist_fp32::thread_p_shl8_cast_fu_2742_p1() {
    p_shl8_cast_fu_2742_p1 = esl_zext<11,7>(p_shl8_fu_2734_p3.read());
}

void mnist_fp32::thread_p_shl8_fu_2734_p3() {
    p_shl8_fu_2734_p3 = esl_concat<5,2>(p_s_reg_1097.read(), ap_const_lv2_0);
}

void mnist_fp32::thread_p_shl90_cast_fu_10307_p3() {
    p_shl90_cast_fu_10307_p3 = esl_concat<8,3>(tmp_679_fu_10303_p1.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl91_cast_fu_9563_p3() {
    p_shl91_cast_fu_9563_p3 = esl_concat<8,4>(tmp_682_fu_9559_p1.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_p_shl92_cast_fu_9579_p1() {
    p_shl92_cast_fu_9579_p1 = esl_sext<12,11>(tmp_683_fu_9571_p3.read());
}

void mnist_fp32::thread_p_shl93_cast_fu_10168_p1() {
    p_shl93_cast_fu_10168_p1 = esl_zext<7,6>(tmp_715_fu_10161_p3.read());
}

void mnist_fp32::thread_p_shl94_cast_fu_10198_p3() {
    p_shl94_cast_fu_10198_p3 = esl_concat<7,3>(tmp_718_reg_16152.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl95_cast_fu_10994_p1() {
    p_shl95_cast_fu_10994_p1 = esl_zext<9,8>(tmp_722_fu_10986_p3.read());
}

void mnist_fp32::thread_p_shl96_cast_fu_10611_p3() {
    p_shl96_cast_fu_10611_p3 = esl_concat<8,3>(tmp_725_fu_10607_p1.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl97_cast_fu_11516_p1() {
    p_shl97_cast_fu_11516_p1 = esl_zext<9,8>(tmp_733_fu_11508_p3.read());
}

void mnist_fp32::thread_p_shl98_cast_fu_11029_p3() {
    p_shl98_cast_fu_11029_p3 = esl_concat<9,3>(tmp_735_fu_11020_p2.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_p_shl9_cast_fu_3818_p1() {
    p_shl9_cast_fu_3818_p1 = esl_zext<11,10>(p_shl9_fu_3810_p3.read());
}

void mnist_fp32::thread_p_shl9_fu_3810_p3() {
    p_shl9_fu_3810_p3 = esl_concat<5,5>(p_9_reg_1273.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_p_shl_fu_4484_p1() {
    p_shl_fu_4484_p1 = esl_zext<64,8>(tmp_441_fu_4476_p3.read());
}

void mnist_fp32::thread_p_v1_fu_6587_p3() {
    p_v1_fu_6587_p3 = (!tmp_546_reg_15076.read()[0].is_01())? sc_lv<12>(): ((tmp_546_reg_15076.read()[0].to_bool())? tmp_555_fu_6580_p1.read(): tmp_557_fu_6584_p1.read());
}

void mnist_fp32::thread_p_v2_fu_7747_p3() {
    p_v2_fu_7747_p3 = (!tmp_613_reg_15422.read()[0].is_01())? sc_lv<13>(): ((tmp_613_reg_15422.read()[0].to_bool())? tmp_622_fu_7740_p1.read(): tmp_624_fu_7744_p1.read());
}

void mnist_fp32::thread_p_v3_fu_10126_p3() {
    p_v3_fu_10126_p3 = (!tmp_701_reg_16108.read()[0].is_01())? sc_lv<11>(): ((tmp_701_reg_16108.read()[0].to_bool())? tmp_710_fu_10119_p1.read(): tmp_712_fu_10123_p1.read());
}

void mnist_fp32::thread_p_v4_fu_11261_p3() {
    p_v4_fu_11261_p3 = (!tmp_765_reg_16469.read()[0].is_01())? sc_lv<12>(): ((tmp_765_reg_16469.read()[0].to_bool())? tmp_774_fu_11254_p1.read(): tmp_776_fu_11258_p1.read());
}

void mnist_fp32::thread_p_v_fu_4103_p3() {
    p_v_fu_4103_p3 = (!tmp_284_reg_14380.read()[0].is_01())? sc_lv<14>(): ((tmp_284_reg_14380.read()[0].to_bool())? tmp_316_fu_4096_p1.read(): tmp_322_fu_4100_p1.read());
}

void mnist_fp32::thread_pad_temp1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state85.read())) {
        pad_temp1_0_address0 =  (sc_lv<12>) (tmp_622_cast_fu_4657_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state79.read())) {
        pad_temp1_0_address0 =  (sc_lv<12>) (tmp_592_cast_fu_4333_p1.read());
    } else {
        pad_temp1_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp32::thread_pad_temp1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state85.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state79.read()))) {
        pad_temp1_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp1_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state79.read())) {
        pad_temp1_0_we0 = ap_const_logic_1;
    } else {
        pad_temp1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp2_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state150.read())) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_646_fu_6975_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state144.read())) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_643_cast_fu_6708_p1.read());
    } else {
        pad_temp2_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp32::thread_pad_temp2_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state150.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state144.read()))) {
        pad_temp2_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp2_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state144.read())) {
        pad_temp2_0_we0 = ap_const_logic_1;
    } else {
        pad_temp2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp3_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state205.read())) {
        pad_temp3_0_address0 =  (sc_lv<11>) (tmp_686_fu_8236_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state199.read())) {
        pad_temp3_0_address0 =  (sc_lv<11>) (tmp_707_cast_fu_7977_p1.read());
    } else {
        pad_temp3_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp32::thread_pad_temp3_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state205.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state199.read()))) {
        pad_temp3_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp3_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp3_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state199.read())) {
        pad_temp3_0_we0 = ap_const_logic_1;
    } else {
        pad_temp3_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp4_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state269.read())) {
        pad_temp4_0_address0 =  (sc_lv<10>) (tmp_810_cast_fu_10525_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state263.read())) {
        pad_temp4_0_address0 =  (sc_lv<10>) (tmp_757_cast_fu_10231_p1.read());
    } else {
        pad_temp4_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp32::thread_pad_temp4_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state269.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state263.read()))) {
        pad_temp4_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp4_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp4_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state263.read())) {
        pad_temp4_0_we0 = ap_const_logic_1;
    } else {
        pad_temp4_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp5_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state323.read())) {
        pad_temp5_0_address0 =  (sc_lv<11>) (tmp_834_cast_fu_11782_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state317.read())) {
        pad_temp5_0_address0 =  (sc_lv<11>) (tmp_812_cast_fu_11475_p1.read());
    } else {
        pad_temp5_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp32::thread_pad_temp5_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state323.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state317.read()))) {
        pad_temp5_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp5_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp5_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state317.read())) {
        pad_temp5_0_we0 = ap_const_logic_1;
    } else {
        pad_temp5_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp_0_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state28.read())) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_547_cast_fu_3258_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state23.read())) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_488_cast_fu_3008_p1.read());
    } else {
        pad_temp_0_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp32::thread_pad_temp_0_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state28.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state23.read()))) {
        pad_temp_0_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pad_temp_0_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state23.read())) {
        pad_temp_0_0_we0 = ap_const_logic_1;
    } else {
        pad_temp_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_phi_mul189_cast_fu_6266_p1() {
    phi_mul189_cast_fu_6266_p1 = esl_zext<11,10>(phi_mul1_reg_1543.read());
}

void mnist_fp32::thread_phi_mul191_cast_fu_7426_p1() {
    phi_mul191_cast_fu_7426_p1 = esl_zext<12,11>(phi_mul2_reg_1739.read());
}

void mnist_fp32::thread_phi_mul193_cast_fu_9829_p1() {
    phi_mul193_cast_fu_9829_p1 = esl_zext<10,9>(phi_mul3_reg_2017.read());
}

void mnist_fp32::thread_phi_mul195_cast_fu_10960_p1() {
    phi_mul195_cast_fu_10960_p1 = esl_zext<11,10>(phi_mul4_reg_2211.read());
}

void mnist_fp32::thread_phi_mul_cast_fu_3709_p1() {
    phi_mul_cast_fu_3709_p1 = esl_zext<13,12>(phi_mul_reg_1262.read());
}

void mnist_fp32::thread_pool1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state142.read())) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_642_cast_fu_6694_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state111.read())) {
        pool1_0_address0 = pool1_0_addr_1_reg_14914.read();
    } else {
        pool1_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp32::thread_pool1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state111.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state142.read()))) {
        pool1_0_ce0 = ap_const_logic_1;
    } else {
        pool1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pool1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond38_fu_5966_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state111.read()))) {
        pool1_0_we0 = ap_const_logic_1;
    } else {
        pool1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_pool2_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state261.read())) {
        pool2_0_address0 =  (sc_lv<9>) (tmp_756_cast_fu_10217_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state231.read())) {
        pool2_0_address0 = pool2_0_addr_1_reg_15951.read();
    } else {
        pool2_0_address0 =  (sc_lv<9>) ("XXXXXXXXX");
    }
}

}

