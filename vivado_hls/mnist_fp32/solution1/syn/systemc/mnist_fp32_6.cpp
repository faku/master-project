#include "mnist_fp32.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp32::thread_tmp_644_fu_6966_p4() {
    tmp_644_fu_6966_p4 = esl_concat<7,4>(esl_concat<3,4>(p_39_reg_1624.read(), r_V_14_reg_15190.read()), r_V_17_fu_6960_p2.read());
}

void mnist_fp32::thread_tmp_645_cast_fu_7456_p1() {
    tmp_645_cast_fu_7456_p1 = esl_zext<9,8>(tmp_569_fu_7448_p3.read());
}

void mnist_fp32::thread_tmp_645_fu_7908_p1() {
    tmp_645_fu_7908_p1 = num_zeros_3_fu_7894_p3.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_646_fu_6975_p1() {
    tmp_646_fu_6975_p1 = esl_zext<64,11>(tmp_644_fu_6966_p4.read());
}

void mnist_fp32::thread_tmp_647_fu_6984_p2() {
    tmp_647_fu_6984_p2 = (!tmp_608_reg_15195.read().is_01() || !tmp_195_cast_fu_6980_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_608_reg_15195.read()) + sc_biguint<10>(tmp_195_cast_fu_6980_p1.read()));
}

void mnist_fp32::thread_tmp_648_fu_7972_p2() {
    tmp_648_fu_7972_p2 = (!tmp_658_cast_reg_15381.read().is_01() || !tmp_204_cast_fu_7968_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_658_cast_reg_15381.read()) + sc_biguint<13>(tmp_204_cast_fu_7968_p1.read()));
}

void mnist_fp32::thread_tmp_649_fu_9855_p3() {
    tmp_649_fu_9855_p3 = esl_concat<4,3>(p_36_reg_2006.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_650_cast_fu_6056_p1() {
    tmp_650_cast_fu_6056_p1 = esl_zext<64,13>(tmp_574_fu_6051_p2.read());
}

void mnist_fp32::thread_tmp_650_fu_9867_p2() {
    tmp_650_fu_9867_p2 = (!lhs_V_6_cast_fu_9851_p1.read().is_01() || !p_shl83_cast_fu_9863_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(lhs_V_6_cast_fu_9851_p1.read()) + sc_biguint<8>(p_shl83_cast_fu_9863_p1.read()));
}

void mnist_fp32::thread_tmp_651_fu_9460_p2() {
    tmp_651_fu_9460_p2 = (!tmp_699_cast_reg_15920.read().is_01() || !lhs_V_9_cast_fu_9456_p1.read().is_01())? sc_lv<9>(): (sc_bigint<9>(tmp_699_cast_reg_15920.read()) + sc_biguint<9>(lhs_V_9_cast_fu_9456_p1.read()));
}

void mnist_fp32::thread_tmp_652_cast_fu_8002_p1() {
    tmp_652_cast_fu_8002_p1 = esl_zext<8,7>(tmp_579_fu_7994_p3.read());
}

void mnist_fp32::thread_tmp_652_fu_9469_p1() {
    tmp_652_fu_9469_p1 = tmp_651_fu_9460_p2.read().range(7-1, 0);
}

void mnist_fp32::thread_tmp_653_fu_9481_p2() {
    tmp_653_fu_9481_p2 = (!p_shl84_cast_fu_9473_p3.read().is_01() || !tmp_710_cast_fu_9465_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl84_cast_fu_9473_p3.read()) - sc_bigint<10>(tmp_710_cast_fu_9465_p1.read()));
}

void mnist_fp32::thread_tmp_654_fu_9053_p2() {
    tmp_654_fu_9053_p2 = (!tmp_230_cast_fu_9049_p1.read().is_01() || !tmp_643_reg_15806.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_230_cast_fu_9049_p1.read()) + sc_biguint<12>(tmp_643_reg_15806.read()));
}

void mnist_fp32::thread_tmp_655_cast_fu_8036_p1() {
    tmp_655_cast_fu_8036_p1 = esl_sext<10,9>(tmp_582_fu_8030_p2.read());
}

void mnist_fp32::thread_tmp_655_fu_9076_p1() {
    tmp_655_fu_9076_p1 = conv4_0_load_to_int_fu_9063_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_656_fu_9113_p1() {
    tmp_656_fu_9113_p1 = ireg_V_7_fu_9105_p3.read().range(63-1, 0);
}

void mnist_fp32::thread_tmp_658_cast_fu_7493_p3() {
    tmp_658_cast_fu_7493_p3 = esl_concat<9,4>(tmp_583_fu_7488_p2.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_tmp_658_fu_9135_p1() {
    tmp_658_fu_9135_p1 = ireg_V_7_fu_9105_p3.read().range(52-1, 0);
}

void mnist_fp32::thread_tmp_659_fu_9210_p1() {
    tmp_659_fu_9210_p1 = man_V_16_fu_9165_p3.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_65_cast_fu_4324_p1() {
    tmp_65_cast_fu_4324_p1 = esl_zext<13,5>(p_13_reg_1285.read());
}

void mnist_fp32::thread_tmp_65_fu_2925_p4() {
    tmp_65_fu_2925_p4 = neg_mul_fu_2920_p2.read().range(22, 16);
}

void mnist_fp32::thread_tmp_660_fu_9214_p4() {
    tmp_660_fu_9214_p4 = sh_amt_5_fu_9196_p3.read().range(11, 5);
}

void mnist_fp32::thread_tmp_661_cast_fu_7112_p1() {
    tmp_661_cast_fu_7112_p1 = esl_zext<64,12>(tmp_586_fu_7107_p2.read());
}

void mnist_fp32::thread_tmp_661_fu_9282_p1() {
    tmp_661_fu_9282_p1 = tmp_281_fu_9277_p2.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_662_cast_fu_6886_p1() {
    tmp_662_cast_fu_6886_p1 = esl_zext<64,12>(tmp_594_fu_6881_p2.read());
}

void mnist_fp32::thread_tmp_662_fu_8867_p2() {
    tmp_662_fu_8867_p2 = (!tmp_604_reg_15541.read().is_01() || !tmp_249_cast_fu_8863_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_604_reg_15541.read()) + sc_biguint<12>(tmp_249_cast_fu_8863_p1.read()));
}

void mnist_fp32::thread_tmp_663_cast_fu_6855_p1() {
    tmp_663_cast_fu_6855_p1 = esl_zext<64,7>(tmp_595_fu_6850_p2.read());
}

void mnist_fp32::thread_tmp_663_fu_8859_p1() {
    tmp_663_fu_8859_p1 = num_zeros_4_fu_8845_p3.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_664_fu_8119_p2() {
    tmp_664_fu_8119_p2 = (!tmp_190_cast_fu_8115_p1.read().is_01() || !tmp_652_cast_reg_15523.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_190_cast_fu_8115_p1.read()) + sc_biguint<8>(tmp_652_cast_reg_15523.read()));
}

void mnist_fp32::thread_tmp_665_fu_8128_p3() {
    tmp_665_fu_8128_p3 = esl_concat<8,2>(tmp_664_fu_8119_p2.read(), ap_const_lv2_0);
}

void mnist_fp32::thread_tmp_666_fu_8140_p2() {
    tmp_666_fu_8140_p2 = (!p_shl5_fu_8136_p1.read().is_01() || !tmp_715_cast_fu_8124_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl5_fu_8136_p1.read()) - sc_biguint<64>(tmp_715_cast_fu_8124_p1.read()));
}

void mnist_fp32::thread_tmp_667_fu_10252_p3() {
    tmp_667_fu_10252_p3 = esl_concat<5,3>(p_41_reg_2063.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_668_cast_fu_8982_p1() {
    tmp_668_cast_fu_8982_p1 = esl_sext<10,9>(tmp_600_fu_8976_p2.read());
}

void mnist_fp32::thread_tmp_668_fu_10264_p2() {
    tmp_668_fu_10264_p2 = (!tmp_719_cast_fu_10260_p1.read().is_01() || !tmp_132_cast_fu_10248_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_719_cast_fu_10260_p1.read()) - sc_biguint<9>(tmp_132_cast_fu_10248_p1.read()));
}

void mnist_fp32::thread_tmp_669_fu_9889_p2() {
    tmp_669_fu_9889_p2 = (!lhs_V_10_cast1_fu_9885_p1.read().is_01() || !tmp_650_reg_16059.read().is_01())? sc_lv<8>(): (sc_biguint<8>(lhs_V_10_cast1_fu_9885_p1.read()) + sc_biguint<8>(tmp_650_reg_16059.read()));
}

void mnist_fp32::thread_tmp_66_fu_2935_p1() {
    tmp_66_fu_2935_p1 = esl_sext<11,7>(tmp_65_fu_2925_p4.read());
}

void mnist_fp32::thread_tmp_670_fu_9906_p2() {
    tmp_670_fu_9906_p2 = (!p_shl87_cast_fu_9898_p3.read().is_01() || !tmp_722_cast_fu_9894_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl87_cast_fu_9898_p3.read()) + sc_biguint<11>(tmp_722_cast_fu_9894_p1.read()));
}

void mnist_fp32::thread_tmp_671_fu_9511_p2() {
    tmp_671_fu_9511_p2 = (!lhs_V_11_cast_fu_9507_p1.read().is_01() || !tmp_653_reg_15933.read().is_01())? sc_lv<10>(): (sc_biguint<10>(lhs_V_11_cast_fu_9507_p1.read()) + sc_biguint<10>(tmp_653_reg_15933.read()));
}

void mnist_fp32::thread_tmp_672_fu_8178_p2() {
    tmp_672_fu_8178_p2 = (!tmp_666_reg_15562.read().is_01() || !tmp_234_fu_8174_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_666_reg_15562.read()) + sc_biguint<64>(tmp_234_fu_8174_p1.read()));
}

void mnist_fp32::thread_tmp_673_fu_8183_p1() {
    tmp_673_fu_8183_p1 = tmp_672_fu_8178_p2.read().range(11-1, 0);
}

void mnist_fp32::thread_tmp_674_fu_8187_p1() {
    tmp_674_fu_8187_p1 = tmp_672_fu_8178_p2.read().range(9-1, 0);
}

void mnist_fp32::thread_tmp_675_fu_8199_p2() {
    tmp_675_fu_8199_p2 = (!p_shl88_cast_fu_8191_p3.read().is_01() || !tmp_673_fu_8183_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl88_cast_fu_8191_p3.read()) - sc_biguint<11>(tmp_673_fu_8183_p1.read()));
}

void mnist_fp32::thread_tmp_676_fu_10560_p3() {
    tmp_676_fu_10560_p3 = esl_concat<5,3>(p_67_reg_2167.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_677_fu_10572_p2() {
    tmp_677_fu_10572_p2 = (!p_shl89_cast_fu_10568_p1.read().is_01() || !tmp_278_cast_fu_10556_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl89_cast_fu_10568_p1.read()) - sc_biguint<9>(tmp_278_cast_fu_10556_p1.read()));
}

void mnist_fp32::thread_tmp_678_fu_10294_p2() {
    tmp_678_fu_10294_p2 = (!tmp_187_cast_fu_10290_p1.read().is_01() || !tmp_721_cast_reg_16180.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_187_cast_fu_10290_p1.read()) + sc_bigint<10>(tmp_721_cast_reg_16180.read()));
}

void mnist_fp32::thread_tmp_679_fu_10303_p1() {
    tmp_679_fu_10303_p1 = tmp_678_fu_10294_p2.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_67_fu_6400_p2() {
    tmp_67_fu_6400_p2 = (!p_29_reg_1565.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_29_reg_1565.read() != ap_const_lv5_0);
}

void mnist_fp32::thread_tmp_680_fu_10315_p2() {
    tmp_680_fu_10315_p2 = (!p_shl90_cast_fu_10307_p3.read().is_01() || !tmp_731_cast_fu_10299_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl90_cast_fu_10307_p3.read()) - sc_bigint<11>(tmp_731_cast_fu_10299_p1.read()));
}

void mnist_fp32::thread_tmp_681_fu_9554_p2() {
    tmp_681_fu_9554_p2 = (!tmp_697_cast_reg_15915.read().is_01() || !tmp_280_cast_cast_fu_9550_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_697_cast_reg_15915.read()) + sc_biguint<10>(tmp_280_cast_cast_fu_9550_p1.read()));
}

void mnist_fp32::thread_tmp_682_fu_9559_p1() {
    tmp_682_fu_9559_p1 = tmp_681_fu_9554_p2.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_683_fu_9571_p3() {
    tmp_683_fu_9571_p3 = esl_concat<10,1>(tmp_681_fu_9554_p2.read(), ap_const_lv1_0);
}

void mnist_fp32::thread_tmp_684_fu_9583_p2() {
    tmp_684_fu_9583_p2 = (!p_shl91_cast_fu_9563_p3.read().is_01() || !p_shl92_cast_fu_9579_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl91_cast_fu_9563_p3.read()) - sc_bigint<12>(p_shl92_cast_fu_9579_p1.read()));
}

void mnist_fp32::thread_tmp_685_fu_8227_p4() {
    tmp_685_fu_8227_p4 = esl_concat<8,4>(esl_concat<4,4>(p_50_reg_1835.read(), r_V_20_reg_15580.read()), r_V_21_fu_8221_p2.read());
}

void mnist_fp32::thread_tmp_686_fu_8236_p1() {
    tmp_686_fu_8236_p1 = esl_zext<64,12>(tmp_685_fu_8227_p4.read());
}

void mnist_fp32::thread_tmp_687_fu_8259_p1() {
    tmp_687_fu_8259_p1 = ireg_V_14_fu_8255_p1.read().range(63-1, 0);
}

void mnist_fp32::thread_tmp_688_fu_8245_p2() {
    tmp_688_fu_8245_p2 = (!tmp_305_cast_fu_8241_p1.read().is_01() || !tmp_675_reg_15585.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_305_cast_fu_8241_p1.read()) + sc_biguint<11>(tmp_675_reg_15585.read()));
}

void mnist_fp32::thread_tmp_689_cast_fu_7807_p1() {
    tmp_689_cast_fu_7807_p1 = esl_sext<9,8>(tmp_629_fu_7801_p2.read());
}

void mnist_fp32::thread_tmp_689_fu_8281_p1() {
    tmp_689_fu_8281_p1 = ireg_V_14_fu_8255_p1.read().range(52-1, 0);
}

void mnist_fp32::thread_tmp_68_fu_6406_p2() {
    tmp_68_fu_6406_p2 = (!p_29_reg_1565.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_29_reg_1565.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp32::thread_tmp_690_fu_8392_p1() {
    tmp_690_fu_8392_p1 = man_V_12_fu_8347_p3.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_691_fu_8396_p4() {
    tmp_691_fu_8396_p4 = sh_amt_6_fu_8378_p3.read().range(11, 5);
}

void mnist_fp32::thread_tmp_692_fu_8514_p1() {
    tmp_692_fu_8514_p1 = tmp_296_fu_8509_p2.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_693_fu_8295_p1() {
    tmp_693_fu_8295_p1 = ireg_V_15_fu_8291_p1.read().range(63-1, 0);
}

void mnist_fp32::thread_tmp_694_cast_fu_7854_p1() {
    tmp_694_cast_fu_7854_p1 = esl_zext<64,12>(tmp_634_fu_7848_p2.read());
}

void mnist_fp32::thread_tmp_694_fu_9978_p2() {
    tmp_694_fu_9978_p2 = (!tmp_699_fu_9968_p1.read().is_01() || !ap_const_lv3_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_699_fu_9968_p1.read() != ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_695_fu_8317_p1() {
    tmp_695_fu_8317_p1 = ireg_V_15_fu_8291_p1.read().range(52-1, 0);
}

void mnist_fp32::thread_tmp_696_fu_8477_p1() {
    tmp_696_fu_8477_p1 = man_V_15_fu_8432_p3.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_697_cast_fu_9418_p1() {
    tmp_697_cast_fu_9418_p1 = esl_sext<10,9>(tmp_637_fu_9412_p2.read());
}

void mnist_fp32::thread_tmp_697_fu_8481_p4() {
    tmp_697_fu_8481_p4 = sh_amt_7_fu_8463_p3.read().range(11, 5);
}

void mnist_fp32::thread_tmp_698_fu_8655_p1() {
    tmp_698_fu_8655_p1 = tmp_320_fu_8650_p2.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_699_cast_fu_9440_p1() {
    tmp_699_cast_fu_9440_p1 = esl_sext<9,8>(tmp_639_fu_9434_p2.read());
}

void mnist_fp32::thread_tmp_699_fu_9968_p1() {
    tmp_699_fu_9968_p1 = p_55_reg_2039.read().range(3-1, 0);
}

void mnist_fp32::thread_tmp_69_fu_5569_p3() {
    tmp_69_fu_5569_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_411_reg_14825.read());
}

void mnist_fp32::thread_tmp_700_fu_10032_p1() {
    tmp_700_fu_10032_p1 = mul7_fu_13993_p2.read().range(21-1, 0);
}

void mnist_fp32::thread_tmp_702_fu_10061_p4() {
    tmp_702_fu_10061_p4 = neg_mul7_fu_10056_p2.read().range(20, 13);
}

void mnist_fp32::thread_tmp_703_fu_10071_p1() {
    tmp_703_fu_10071_p1 = esl_sext<10,8>(tmp_702_fu_10061_p4.read());
}

void mnist_fp32::thread_tmp_705_fu_10075_p1() {
    tmp_705_fu_10075_p1 = esl_sext<10,9>(tmp_704_reg_16121.read());
}

void mnist_fp32::thread_tmp_706_cast_fu_6989_p1() {
    tmp_706_cast_fu_6989_p1 = esl_zext<64,10>(tmp_647_fu_6984_p2.read());
}

void mnist_fp32::thread_tmp_706_fu_10078_p3() {
    tmp_706_fu_10078_p3 = (!tmp_701_reg_16108.read()[0].is_01())? sc_lv<10>(): ((tmp_701_reg_16108.read()[0].to_bool())? tmp_703_fu_10071_p1.read(): tmp_705_fu_10075_p1.read());
}

void mnist_fp32::thread_tmp_707_cast_fu_7977_p1() {
    tmp_707_cast_fu_7977_p1 = esl_zext<64,13>(tmp_648_fu_7972_p2.read());
}

void mnist_fp32::thread_tmp_707_fu_10154_p1() {
    tmp_707_fu_10154_p1 = grp_fu_10098_p2.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_708_fu_10044_p1() {
    tmp_708_fu_10044_p1 = mul8_fu_14001_p2.read().range(21-1, 0);
}

void mnist_fp32::thread_tmp_709_fu_10109_p4() {
    tmp_709_fu_10109_p4 = neg_mul8_fu_10104_p2.read().range(20, 16);
}

void mnist_fp32::thread_tmp_70_fu_4528_p2() {
    tmp_70_fu_4528_p2 = (!ap_const_lv32_0.is_01() || !p_Val2_5_reg_1350.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_0) - sc_biguint<32>(p_Val2_5_reg_1350.read()));
}

void mnist_fp32::thread_tmp_710_cast_fu_9465_p1() {
    tmp_710_cast_fu_9465_p1 = esl_sext<10,9>(tmp_651_fu_9460_p2.read());
}

void mnist_fp32::thread_tmp_710_fu_10119_p1() {
    tmp_710_fu_10119_p1 = esl_sext<11,5>(tmp_709_fu_10109_p4.read());
}

void mnist_fp32::thread_tmp_712_fu_10123_p1() {
    tmp_712_fu_10123_p1 = esl_sext<11,6>(tmp_711_reg_16131.read());
}

void mnist_fp32::thread_tmp_713_cast_fu_9058_p1() {
    tmp_713_cast_fu_9058_p1 = esl_zext<64,12>(tmp_654_fu_9053_p2.read());
}

void mnist_fp32::thread_tmp_713_fu_10133_p1() {
    tmp_713_fu_10133_p1 = p_v3_fu_10126_p3.read().range(3-1, 0);
}

void mnist_fp32::thread_tmp_714_cast_fu_8936_p1() {
    tmp_714_cast_fu_8936_p1 = esl_zext<64,12>(tmp_662_reg_15770.read());
}

void mnist_fp32::thread_tmp_714_fu_10143_p1() {
    tmp_714_fu_10143_p1 = p_v3_fu_10126_p3.read().range(3-1, 0);
}

void mnist_fp32::thread_tmp_715_cast_fu_8124_p1() {
    tmp_715_cast_fu_8124_p1 = esl_zext<64,8>(tmp_664_fu_8119_p2.read());
}

void mnist_fp32::thread_tmp_715_fu_10161_p3() {
    tmp_715_fu_10161_p3 = esl_concat<3,3>(r_V_23_reg_16141.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_716_fu_10172_p2() {
    tmp_716_fu_10172_p2 = (!p_shl93_cast_fu_10168_p1.read().is_01() || !tmp_251_cast_fu_10158_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl93_cast_fu_10168_p1.read()) - sc_biguint<7>(tmp_251_cast_fu_10158_p1.read()));
}

void mnist_fp32::thread_tmp_717_fu_10182_p2() {
    tmp_717_fu_10182_p2 = (!tmp_752_cast_fu_10178_p1.read().is_01() || !tmp_707_fu_10154_p1.read().is_01())? sc_lv<8>(): (sc_bigint<8>(tmp_752_cast_fu_10178_p1.read()) + sc_biguint<8>(tmp_707_fu_10154_p1.read()));
}

void mnist_fp32::thread_tmp_718_fu_10188_p1() {
    tmp_718_fu_10188_p1 = tmp_717_fu_10182_p2.read().range(7-1, 0);
}

void mnist_fp32::thread_tmp_719_cast_fu_10260_p1() {
    tmp_719_cast_fu_10260_p1 = esl_zext<9,8>(tmp_667_fu_10252_p3.read());
}

void mnist_fp32::thread_tmp_719_fu_10205_p2() {
    tmp_719_fu_10205_p2 = (!p_shl94_cast_fu_10198_p3.read().is_01() || !tmp_753_cast_fu_10195_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl94_cast_fu_10198_p3.read()) - sc_bigint<10>(tmp_753_cast_fu_10195_p1.read()));
}

void mnist_fp32::thread_tmp_71_fu_5307_p2() {
    tmp_71_fu_5307_p2 = (!p_Result_s_50_fu_5297_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_s_50_fu_5297_p4.read() != ap_const_lv8_9E);
}

void mnist_fp32::thread_tmp_720_fu_10211_p2() {
    tmp_720_fu_10211_p2 = (!tmp_719_fu_10205_p2.read().is_01() || !tmp_237_cast_fu_10192_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_719_fu_10205_p2.read()) + sc_biguint<10>(tmp_237_cast_fu_10192_p1.read()));
}

void mnist_fp32::thread_tmp_721_cast_fu_10270_p1() {
    tmp_721_cast_fu_10270_p1 = esl_sext<10,9>(tmp_668_fu_10264_p2.read());
}

void mnist_fp32::thread_tmp_721_fu_10226_p2() {
    tmp_721_fu_10226_p2 = (!tmp_670_reg_16072.read().is_01() || !tmp_256_cast_fu_10222_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_670_reg_16072.read()) + sc_biguint<11>(tmp_256_cast_fu_10222_p1.read()));
}

void mnist_fp32::thread_tmp_722_cast_fu_9894_p1() {
    tmp_722_cast_fu_9894_p1 = esl_zext<11,8>(tmp_669_fu_9889_p2.read());
}

void mnist_fp32::thread_tmp_722_fu_10986_p3() {
    tmp_722_fu_10986_p3 = esl_concat<5,3>(p_47_reg_2200.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_723_fu_10998_p2() {
    tmp_723_fu_10998_p2 = (!lhs_V_13_cast_fu_10982_p1.read().is_01() || !p_shl95_cast_fu_10994_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_13_cast_fu_10982_p1.read()) + sc_biguint<9>(p_shl95_cast_fu_10994_p1.read()));
}

void mnist_fp32::thread_tmp_724_fu_10598_p2() {
    tmp_724_fu_10598_p2 = (!tmp_293_cast_fu_10594_p1.read().is_01() || !tmp_730_cast_reg_16288.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_293_cast_fu_10594_p1.read()) + sc_bigint<10>(tmp_730_cast_reg_16288.read()));
}

void mnist_fp32::thread_tmp_725_cast_fu_9516_p1() {
    tmp_725_cast_fu_9516_p1 = esl_zext<64,10>(tmp_671_fu_9511_p2.read());
}

void mnist_fp32::thread_tmp_725_fu_10607_p1() {
    tmp_725_fu_10607_p1 = tmp_724_fu_10598_p2.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_726_fu_10619_p2() {
    tmp_726_fu_10619_p2 = (!p_shl96_cast_fu_10611_p3.read().is_01() || !tmp_760_cast_fu_10603_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl96_cast_fu_10611_p3.read()) - sc_bigint<11>(tmp_760_cast_fu_10603_p1.read()));
}

void mnist_fp32::thread_tmp_727_fu_9614_p2() {
    tmp_727_fu_9614_p2 = (!tmp_684_reg_15969.read().is_01() || !tmp_297_cast_cast_fu_9610_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_684_reg_15969.read()) + sc_biguint<12>(tmp_297_cast_cast_fu_9610_p1.read()));
}

void mnist_fp32::thread_tmp_729_fu_9672_p1() {
    tmp_729_fu_9672_p1 = num_zeros_5_fu_9658_p3.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_72_cast_fu_5470_p1() {
    tmp_72_cast_fu_5470_p1 = esl_zext<13,5>(p_27_reg_1441.read());
}

void mnist_fp32::thread_tmp_730_cast_fu_10578_p1() {
    tmp_730_cast_fu_10578_p1 = esl_sext<10,9>(tmp_677_fu_10572_p2.read());
}

void mnist_fp32::thread_tmp_730_fu_9752_p1() {
    tmp_730_fu_9752_p1 = p_03_i3_to_int_fu_9739_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_731_cast_fu_10299_p1() {
    tmp_731_cast_fu_10299_p1 = esl_sext<11,10>(tmp_678_fu_10294_p2.read());
}

void mnist_fp32::thread_tmp_731_fu_9770_p1() {
    tmp_731_fu_9770_p1 = tmp_283_to_int_fu_9756_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_732_fu_11496_p3() {
    tmp_732_fu_11496_p3 = esl_concat<5,4>(p_52_reg_2260.read(), ap_const_lv4_0);
}

void mnist_fp32::thread_tmp_733_fu_11508_p3() {
    tmp_733_fu_11508_p3 = esl_concat<5,3>(p_52_reg_2260.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_734_fu_11520_p2() {
    tmp_734_fu_11520_p2 = (!p_shl97_cast_fu_11516_p1.read().is_01() || !tmp_205_cast_fu_11492_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl97_cast_fu_11516_p1.read()) - sc_biguint<9>(tmp_205_cast_fu_11492_p1.read()));
}

void mnist_fp32::thread_tmp_735_fu_11020_p2() {
    tmp_735_fu_11020_p2 = (!lhs_V_14_cast_fu_11016_p1.read().is_01() || !tmp_723_reg_16420.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_14_cast_fu_11016_p1.read()) + sc_biguint<9>(tmp_723_reg_16420.read()));
}

void mnist_fp32::thread_tmp_736_fu_11037_p2() {
    tmp_736_fu_11037_p2 = (!p_shl98_cast_fu_11029_p3.read().is_01() || !tmp_768_cast_fu_11025_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl98_cast_fu_11029_p3.read()) + sc_biguint<12>(tmp_768_cast_fu_11025_p1.read()));
}

void mnist_fp32::thread_tmp_737_fu_10641_p2() {
    tmp_737_fu_10641_p2 = (!tmp_323_cast_fu_10637_p1.read().is_01() || !tmp_726_reg_16301.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_323_cast_fu_10637_p1.read()) + sc_biguint<11>(tmp_726_reg_16301.read()));
}

void mnist_fp32::thread_tmp_738_fu_10664_p1() {
    tmp_738_fu_10664_p1 = conv5_0_load_to_int_fu_10651_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_739_fu_10701_p1() {
    tmp_739_fu_10701_p1 = ireg_V_8_fu_10693_p3.read().range(63-1, 0);
}

void mnist_fp32::thread_tmp_73_fu_3614_p2() {
    tmp_73_fu_3614_p2 = (!man_V_2_reg_14276.read().is_01() || !tmp_49_fu_3610_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_2_reg_14276.read()) >> (unsigned short)tmp_49_fu_3610_p1.read().to_uint();
}

void mnist_fp32::thread_tmp_740_cast_fu_8250_p1() {
    tmp_740_cast_fu_8250_p1 = esl_zext<64,11>(tmp_688_fu_8245_p2.read());
}

void mnist_fp32::thread_tmp_741_fu_10723_p1() {
    tmp_741_fu_10723_p1 = ireg_V_8_fu_10693_p3.read().range(52-1, 0);
}

void mnist_fp32::thread_tmp_742_fu_10798_p1() {
    tmp_742_fu_10798_p1 = man_V_21_fu_10753_p3.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_743_fu_10802_p4() {
    tmp_743_fu_10802_p4 = sh_amt_8_fu_10784_p3.read().range(11, 5);
}

void mnist_fp32::thread_tmp_744_fu_10870_p1() {
    tmp_744_fu_10870_p1 = tmp_426_fu_10865_p2.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_745_fu_10406_p2() {
    tmp_745_fu_10406_p2 = (!tmp_680_reg_16198.read().is_01() || !tmp_260_cast_fu_10402_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_680_reg_16198.read()) + sc_biguint<11>(tmp_260_cast_fu_10402_p1.read()));
}

void mnist_fp32::thread_tmp_746_fu_10357_p2() {
    tmp_746_fu_10357_p2 = (!tmp_719_cast_reg_16175.read().is_01() || !tmp_261_cast_fu_10353_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_719_cast_reg_16175.read()) + sc_biguint<9>(tmp_261_cast_fu_10353_p1.read()));
}

void mnist_fp32::thread_tmp_747_fu_10366_p3() {
    tmp_747_fu_10366_p3 = esl_concat<9,2>(tmp_746_fu_10357_p2.read(), ap_const_lv2_0);
}

void mnist_fp32::thread_tmp_748_fu_10378_p2() {
    tmp_748_fu_10378_p2 = (!p_shl6_fu_10374_p1.read().is_01() || !tmp_773_cast_fu_10362_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl6_fu_10374_p1.read()) - sc_biguint<64>(tmp_773_cast_fu_10362_p1.read()));
}

void mnist_fp32::thread_tmp_749_fu_10384_p3() {
    tmp_749_fu_10384_p3 = esl_concat<4,3>(p_63_reg_2097.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_74_fu_2939_p1() {
    tmp_74_fu_2939_p1 = esl_sext<11,8>(tmp_72_reg_14078.read());
}

void mnist_fp32::thread_tmp_750_fu_10396_p2() {
    tmp_750_fu_10396_p2 = (!tmp_261_cast1_fu_10349_p1.read().is_01() || !p_shl100_cast_fu_10392_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_261_cast1_fu_10349_p1.read()) + sc_biguint<8>(p_shl100_cast_fu_10392_p1.read()));
}

void mnist_fp32::thread_tmp_751_fu_12493_p3() {
    tmp_751_fu_12493_p3 = esl_concat<5,3>(p_81_reg_2363.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_752_cast_fu_10178_p1() {
    tmp_752_cast_fu_10178_p1 = esl_sext<8,7>(tmp_716_fu_10172_p2.read());
}

void mnist_fp32::thread_tmp_752_fu_12505_p2() {
    tmp_752_fu_12505_p2 = (!p_shl101_cast_fu_12501_p1.read().is_01() || !tmp_346_cast_fu_12489_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl101_cast_fu_12501_p1.read()) - sc_biguint<9>(tmp_346_cast_fu_12489_p1.read()));
}

void mnist_fp32::thread_tmp_753_cast_fu_10195_p1() {
    tmp_753_cast_fu_10195_p1 = esl_sext<10,8>(tmp_717_reg_16147.read());
}

void mnist_fp32::thread_tmp_753_fu_11550_p2() {
    tmp_753_fu_11550_p2 = (!tmp_257_cast_fu_11546_p1.read().is_01() || !tmp_767_cast_reg_16580.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_257_cast_fu_11546_p1.read()) + sc_bigint<10>(tmp_767_cast_reg_16580.read()));
}

void mnist_fp32::thread_tmp_754_fu_11559_p1() {
    tmp_754_fu_11559_p1 = tmp_753_fu_11550_p2.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_755_fu_11571_p2() {
    tmp_755_fu_11571_p2 = (!p_shl102_cast_fu_11563_p3.read().is_01() || !tmp_780_cast_fu_11555_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl102_cast_fu_11563_p3.read()) - sc_bigint<11>(tmp_780_cast_fu_11555_p1.read()));
}

void mnist_fp32::thread_tmp_756_cast_fu_10217_p1() {
    tmp_756_cast_fu_10217_p1 = esl_zext<64,10>(tmp_720_fu_10211_p2.read());
}

void mnist_fp32::thread_tmp_756_fu_10441_p2() {
    tmp_756_fu_10441_p2 = (!tmp_325_cast_fu_10437_p1.read().is_01() || !tmp_750_reg_16229.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_325_cast_fu_10437_p1.read()) + sc_biguint<8>(tmp_750_reg_16229.read()));
}

void mnist_fp32::thread_tmp_757_cast_fu_10231_p1() {
    tmp_757_cast_fu_10231_p1 = esl_zext<64,11>(tmp_721_fu_10226_p2.read());
}

void mnist_fp32::thread_tmp_757_fu_10458_p2() {
    tmp_757_fu_10458_p2 = (!p_shl103_cast_fu_10450_p3.read().is_01() || !tmp_783_cast_fu_10446_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl103_cast_fu_10450_p3.read()) + sc_biguint<11>(tmp_783_cast_fu_10446_p1.read()));
}

void mnist_fp32::thread_tmp_758_fu_10468_p2() {
    tmp_758_fu_10468_p2 = (!tmp_326_fu_10464_p1.read().is_01() || !tmp_748_reg_16224.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_326_fu_10464_p1.read()) + sc_biguint<64>(tmp_748_reg_16224.read()));
}

void mnist_fp32::thread_tmp_759_fu_10473_p1() {
    tmp_759_fu_10473_p1 = tmp_758_fu_10468_p2.read().range(12-1, 0);
}

void mnist_fp32::thread_tmp_75_cast_fu_4556_p1() {
    tmp_75_cast_fu_4556_p1 = esl_zext<10,5>(r_V_9_fu_4550_p2.read());
}

void mnist_fp32::thread_tmp_75_fu_2942_p3() {
    tmp_75_fu_2942_p3 = (!tmp_63_reg_14067.read()[0].is_01())? sc_lv<11>(): ((tmp_63_reg_14067.read()[0].to_bool())? tmp_66_fu_2935_p1.read(): tmp_74_fu_2939_p1.read());
}

void mnist_fp32::thread_tmp_760_cast_fu_10603_p1() {
    tmp_760_cast_fu_10603_p1 = esl_sext<11,10>(tmp_724_fu_10598_p2.read());
}

void mnist_fp32::thread_tmp_760_fu_10477_p1() {
    tmp_760_fu_10477_p1 = tmp_758_fu_10468_p2.read().range(10-1, 0);
}

void mnist_fp32::thread_tmp_761_fu_10489_p2() {
    tmp_761_fu_10489_p2 = (!p_shl104_cast_fu_10481_p3.read().is_01() || !tmp_759_fu_10473_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl104_cast_fu_10481_p3.read()) - sc_biguint<12>(tmp_759_fu_10473_p1.read()));
}

void mnist_fp32::thread_tmp_762_fu_11103_p1() {
    tmp_762_fu_11103_p1 = p_62_reg_2233.read().range(3-1, 0);
}

void mnist_fp32::thread_tmp_763_cast_fu_9619_p1() {
    tmp_763_cast_fu_9619_p1 = esl_zext<64,12>(tmp_727_fu_9614_p2.read());
}

void mnist_fp32::thread_tmp_763_fu_11113_p2() {
    tmp_763_fu_11113_p2 = (!tmp_762_fu_11103_p1.read().is_01() || !ap_const_lv3_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_762_fu_11103_p1.read() != ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_764_fu_11167_p1() {
    tmp_764_fu_11167_p1 = mul9_fu_14009_p2.read().range(22-1, 0);
}

void mnist_fp32::thread_tmp_765_cast_fu_11504_p1() {
    tmp_765_cast_fu_11504_p1 = esl_zext<10,9>(tmp_732_fu_11496_p3.read());
}

void mnist_fp32::thread_tmp_766_fu_11196_p4() {
    tmp_766_fu_11196_p4 = neg_mul9_fu_11191_p2.read().range(21, 14);
}

void mnist_fp32::thread_tmp_767_cast_fu_11526_p1() {
    tmp_767_cast_fu_11526_p1 = esl_sext<10,9>(tmp_734_fu_11520_p2.read());
}

void mnist_fp32::thread_tmp_767_fu_11206_p1() {
    tmp_767_fu_11206_p1 = esl_sext<11,8>(tmp_766_fu_11196_p4.read());
}

void mnist_fp32::thread_tmp_768_cast_fu_11025_p1() {
    tmp_768_cast_fu_11025_p1 = esl_zext<12,9>(tmp_735_fu_11020_p2.read());
}

void mnist_fp32::thread_tmp_769_fu_11210_p1() {
    tmp_769_fu_11210_p1 = esl_sext<11,10>(tmp_768_reg_16482.read());
}

void mnist_fp32::thread_tmp_76_fu_4595_p1() {
    tmp_76_fu_4595_p1 = esl_zext<64,2>(p_28_reg_1385.read());
}

void mnist_fp32::thread_tmp_770_fu_11213_p3() {
    tmp_770_fu_11213_p3 = (!tmp_765_reg_16469.read()[0].is_01())? sc_lv<11>(): ((tmp_765_reg_16469.read()[0].to_bool())? tmp_767_fu_11206_p1.read(): tmp_769_fu_11210_p1.read());
}

void mnist_fp32::thread_tmp_771_cast_fu_10646_p1() {
    tmp_771_cast_fu_10646_p1 = esl_zext<64,11>(tmp_737_fu_10641_p2.read());
}

void mnist_fp32::thread_tmp_771_fu_11289_p1() {
    tmp_771_fu_11289_p1 = grp_fu_11233_p2.read().range(9-1, 0);
}

void mnist_fp32::thread_tmp_772_cast_fu_10411_p1() {
    tmp_772_cast_fu_10411_p1 = esl_zext<64,11>(tmp_745_fu_10406_p2.read());
}

void mnist_fp32::thread_tmp_772_fu_11179_p1() {
    tmp_772_fu_11179_p1 = mul10_fu_14017_p2.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_773_cast_fu_10362_p1() {
    tmp_773_cast_fu_10362_p1 = esl_zext<64,9>(tmp_746_fu_10357_p2.read());
}

void mnist_fp32::thread_tmp_773_fu_11244_p4() {
    tmp_773_fu_11244_p4 = neg_mul10_fu_11239_p2.read().range(22, 17);
}

void mnist_fp32::thread_tmp_774_fu_11254_p1() {
    tmp_774_fu_11254_p1 = esl_sext<12,6>(tmp_773_fu_11244_p4.read());
}

void mnist_fp32::thread_tmp_776_fu_11258_p1() {
    tmp_776_fu_11258_p1 = esl_sext<12,7>(tmp_775_reg_16492.read());
}

void mnist_fp32::thread_tmp_777_fu_11268_p1() {
    tmp_777_fu_11268_p1 = p_v4_fu_11261_p3.read().range(4-1, 0);
}

void mnist_fp32::thread_tmp_778_fu_11278_p1() {
    tmp_778_fu_11278_p1 = p_v4_fu_11261_p3.read().range(4-1, 0);
}

void mnist_fp32::thread_tmp_779_cast_fu_12511_p1() {
    tmp_779_cast_fu_12511_p1 = esl_sext<10,9>(tmp_752_fu_12505_p2.read());
}

void mnist_fp32::thread_tmp_779_fu_11296_p3() {
    tmp_779_fu_11296_p3 = esl_concat<4,3>(r_V_26_reg_16502.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_77_fu_5313_p2() {
    tmp_77_fu_5313_p2 = (!ap_const_lv8_80.is_01() || !tmp_428_reg_14728.read().is_01())? sc_lv<8>(): (sc_bigint<8>(ap_const_lv8_80) - sc_biguint<8>(tmp_428_reg_14728.read()));
}

void mnist_fp32::thread_tmp_780_cast_fu_11555_p1() {
    tmp_780_cast_fu_11555_p1 = esl_sext<11,10>(tmp_753_fu_11550_p2.read());
}

void mnist_fp32::thread_tmp_780_fu_11307_p2() {
    tmp_780_fu_11307_p2 = (!p_shl105_cast_fu_11303_p1.read().is_01() || !tmp_300_cast_fu_11293_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl105_cast_fu_11303_p1.read()) - sc_biguint<8>(tmp_300_cast_fu_11293_p1.read()));
}

void mnist_fp32::thread_tmp_781_fu_11317_p2() {
    tmp_781_fu_11317_p2 = (!tmp_800_cast_fu_11313_p1.read().is_01() || !tmp_771_fu_11289_p1.read().is_01())? sc_lv<9>(): (sc_bigint<9>(tmp_800_cast_fu_11313_p1.read()) + sc_biguint<9>(tmp_771_fu_11289_p1.read()));
}

void mnist_fp32::thread_tmp_782_fu_11323_p1() {
    tmp_782_fu_11323_p1 = tmp_781_fu_11317_p2.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_783_cast_fu_10446_p1() {
    tmp_783_cast_fu_10446_p1 = esl_zext<11,8>(tmp_756_fu_10441_p2.read());
}

void mnist_fp32::thread_tmp_783_fu_11340_p2() {
    tmp_783_fu_11340_p2 = (!p_shl106_cast_fu_11333_p3.read().is_01() || !tmp_801_cast_fu_11330_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl106_cast_fu_11333_p3.read()) - sc_bigint<11>(tmp_801_cast_fu_11330_p1.read()));
}

void mnist_fp32::thread_tmp_784_fu_11346_p2() {
    tmp_784_fu_11346_p2 = (!tmp_783_fu_11340_p2.read().is_01() || !tmp_264_cast_fu_11327_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_783_fu_11340_p2.read()) + sc_biguint<11>(tmp_264_cast_fu_11327_p1.read()));
}

void mnist_fp32::thread_tmp_785_fu_12913_p3() {
    tmp_785_fu_12913_p3 = esl_concat<5,3>(p_82_reg_2396.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_786_fu_12925_p2() {
    tmp_786_fu_12925_p2 = (!p_shl107_cast_fu_12921_p1.read().is_01() || !tmp_373_cast_fu_12909_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl107_cast_fu_12921_p1.read()) - sc_biguint<9>(tmp_373_cast_fu_12909_p1.read()));
}

void mnist_fp32::thread_tmp_787_fu_12531_p2() {
    tmp_787_fu_12531_p2 = (!tmp_374_cast_fu_12527_p1.read().is_01() || !tmp_779_cast_reg_16860.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_374_cast_fu_12527_p1.read()) + sc_bigint<10>(tmp_779_cast_reg_16860.read()));
}

void mnist_fp32::thread_tmp_788_fu_12540_p1() {
    tmp_788_fu_12540_p1 = tmp_787_fu_12531_p2.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_789_fu_12552_p2() {
    tmp_789_fu_12552_p2 = (!p_shl108_cast_fu_12544_p3.read().is_01() || !tmp_807_cast_fu_12536_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl108_cast_fu_12544_p3.read()) - sc_bigint<11>(tmp_807_cast_fu_12536_p1.read()));
}

void mnist_fp32::thread_tmp_78_fu_5318_p1() {
    tmp_78_fu_5318_p1 = esl_zext<8,1>(tmp_71_reg_14743.read());
}

void mnist_fp32::thread_tmp_790_fu_10520_p2() {
    tmp_790_fu_10520_p2 = (!tmp_757_reg_16242.read().is_01() || !tmp_352_cast_fu_10516_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_757_reg_16242.read()) + sc_biguint<11>(tmp_352_cast_fu_10516_p1.read()));
}

void mnist_fp32::thread_tmp_791_fu_11406_p1() {
    tmp_791_fu_11406_p1 = num_zeros_6_fu_11392_p3.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_792_fu_10534_p2() {
    tmp_792_fu_10534_p2 = (!tmp_761_reg_16247.read().is_01() || !tmp_353_cast_fu_10530_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_761_reg_16247.read()) + sc_biguint<12>(tmp_353_cast_fu_10530_p1.read()));
}

void mnist_fp32::thread_tmp_793_fu_11470_p2() {
    tmp_793_fu_11470_p2 = (!tmp_736_reg_16433.read().is_01() || !tmp_362_cast_fu_11466_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_736_reg_16433.read()) + sc_biguint<12>(tmp_362_cast_fu_11466_p1.read()));
}

void mnist_fp32::thread_tmp_794_fu_13230_p1() {
    tmp_794_fu_13230_p1 = ireg_V_s_fu_13226_p1.read().range(63-1, 0);
}

void mnist_fp32::thread_tmp_796_fu_13252_p1() {
    tmp_796_fu_13252_p1 = ireg_V_s_fu_13226_p1.read().range(52-1, 0);
}

void mnist_fp32::thread_tmp_797_fu_13326_p1() {
    tmp_797_fu_13326_p1 = man_V_26_fu_13276_p3.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_798_fu_13330_p4() {
    tmp_798_fu_13330_p4 = sh_amt_9_fu_13312_p3.read().range(11, 5);
}

void mnist_fp32::thread_tmp_799_fu_13399_p1() {
    tmp_799_fu_13399_p1 = tmp_450_fu_13394_p2.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_79_fu_5327_p3() {
    tmp_79_fu_5327_p3 = esl_concat<1,8>(is_neg_1_reg_14718.read(), p_Repl2_4_trunc_fu_5321_p2.read());
}

void mnist_fp32::thread_tmp_7_fu_2716_p3() {
    tmp_7_fu_2716_p3 = esl_concat<5,1>(p_s_reg_1097.read(), ap_const_lv1_0);
}

void mnist_fp32::thread_tmp_800_cast_fu_11313_p1() {
    tmp_800_cast_fu_11313_p1 = esl_sext<9,8>(tmp_780_fu_11307_p2.read());
}

void mnist_fp32::thread_tmp_800_fu_12951_p2() {
    tmp_800_fu_12951_p2 = (!tmp_393_cast_fu_12947_p1.read().is_01() || !tmp_806_cast_reg_16987.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_393_cast_fu_12947_p1.read()) + sc_bigint<10>(tmp_806_cast_reg_16987.read()));
}

void mnist_fp32::thread_tmp_801_cast_fu_11330_p1() {
    tmp_801_cast_fu_11330_p1 = esl_sext<11,9>(tmp_781_reg_16508.read());
}

void mnist_fp32::thread_tmp_801_fu_12960_p1() {
    tmp_801_fu_12960_p1 = tmp_800_fu_12951_p2.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_802_fu_12972_p2() {
    tmp_802_fu_12972_p2 = (!p_shl109_cast_fu_12964_p3.read().is_01() || !tmp_813_cast_fu_12956_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl109_cast_fu_12964_p3.read()) - sc_bigint<11>(tmp_813_cast_fu_12956_p1.read()));
}

void mnist_fp32::thread_tmp_803_fu_12574_p2() {
    tmp_803_fu_12574_p2 = (!tmp_394_cast_fu_12570_p1.read().is_01() || !tmp_789_reg_16873.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_394_cast_fu_12570_p1.read()) + sc_biguint<11>(tmp_789_reg_16873.read()));
}

void mnist_fp32::thread_tmp_804_cast_fu_11352_p1() {
    tmp_804_cast_fu_11352_p1 = esl_zext<64,11>(tmp_784_fu_11346_p2.read());
}

void mnist_fp32::thread_tmp_804_fu_12597_p1() {
    tmp_804_fu_12597_p1 = conv6_0_load_to_int_fu_12584_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_805_fu_12634_p1() {
    tmp_805_fu_12634_p1 = ireg_V_1_fu_12626_p3.read().range(63-1, 0);
}

void mnist_fp32::thread_tmp_806_cast_fu_12931_p1() {
    tmp_806_cast_fu_12931_p1 = esl_sext<10,9>(tmp_786_fu_12925_p2.read());
}

void mnist_fp32::thread_tmp_807_cast_fu_12536_p1() {
    tmp_807_cast_fu_12536_p1 = esl_sext<11,10>(tmp_787_fu_12531_p2.read());
}

void mnist_fp32::thread_tmp_807_fu_12656_p1() {
    tmp_807_fu_12656_p1 = ireg_V_1_fu_12626_p3.read().range(52-1, 0);
}

void mnist_fp32::thread_tmp_808_fu_12731_p1() {
    tmp_808_fu_12731_p1 = man_V_28_fu_12686_p3.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_809_fu_12735_p4() {
    tmp_809_fu_12735_p4 = sh_amt_s_fu_12717_p3.read().range(11, 5);
}

void mnist_fp32::thread_tmp_80_fu_7472_p2() {
    tmp_80_fu_7472_p2 = (!p_30_reg_1750.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_30_reg_1750.read() != ap_const_lv5_0);
}

void mnist_fp32::thread_tmp_810_cast_fu_10525_p1() {
    tmp_810_cast_fu_10525_p1 = esl_zext<64,11>(tmp_790_fu_10520_p2.read());
}

void mnist_fp32::thread_tmp_810_fu_12803_p1() {
    tmp_810_fu_12803_p1 = tmp_475_fu_12798_p2.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_811_cast_fu_10539_p1() {
    tmp_811_cast_fu_10539_p1 = esl_zext<64,12>(tmp_792_fu_10534_p2.read());
}

void mnist_fp32::thread_tmp_811_fu_11668_p2() {
    tmp_811_fu_11668_p2 = (!tmp_755_reg_16598.read().is_01() || !tmp_411_cast_fu_11664_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_755_reg_16598.read()) + sc_biguint<11>(tmp_411_cast_fu_11664_p1.read()));
}

void mnist_fp32::thread_tmp_812_cast_fu_11475_p1() {
    tmp_812_cast_fu_11475_p1 = esl_zext<64,12>(tmp_793_fu_11470_p2.read());
}

void mnist_fp32::thread_tmp_812_fu_12405_p1() {
    tmp_812_fu_12405_p1 = num_zeros_7_fu_12391_p3.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_813_cast_fu_12956_p1() {
    tmp_813_cast_fu_12956_p1 = esl_sext<11,10>(tmp_800_fu_12951_p2.read());
}

void mnist_fp32::thread_tmp_813_fu_11613_p2() {
    tmp_813_fu_11613_p2 = (!tmp_765_cast_reg_16575.read().is_01() || !tmp_348_cast_fu_11609_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_765_cast_reg_16575.read()) + sc_biguint<10>(tmp_348_cast_fu_11609_p1.read()));
}

void mnist_fp32::thread_tmp_814_fu_11622_p3() {
    tmp_814_fu_11622_p3 = esl_concat<10,2>(tmp_813_fu_11613_p2.read(), ap_const_lv2_0);
}

void mnist_fp32::thread_tmp_815_fu_11634_p2() {
    tmp_815_fu_11634_p2 = (!p_shl7_fu_11630_p1.read().is_01() || !tmp_818_cast_fu_11618_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl7_fu_11630_p1.read()) - sc_biguint<64>(tmp_818_cast_fu_11618_p1.read()));
}

void mnist_fp32::thread_tmp_816_cast_fu_12579_p1() {
    tmp_816_cast_fu_12579_p1 = esl_zext<64,11>(tmp_803_fu_12574_p2.read());
}

void mnist_fp32::thread_tmp_816_fu_11640_p3() {
    tmp_816_fu_11640_p3 = esl_concat<5,3>(p_73_reg_2306.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_817_cast_fu_12473_p1() {
    tmp_817_cast_fu_12473_p1 = esl_zext<64,11>(tmp_811_reg_16639.read());
}

void mnist_fp32::thread_tmp_817_fu_11652_p2() {
    tmp_817_fu_11652_p2 = (!tmp_348_cast1_fu_11605_p1.read().is_01() || !p_shl111_cast_fu_11648_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_348_cast1_fu_11605_p1.read()) + sc_biguint<9>(p_shl111_cast_fu_11648_p1.read()));
}

void mnist_fp32::thread_tmp_818_cast_fu_11618_p1() {
    tmp_818_cast_fu_11618_p1 = esl_zext<64,10>(tmp_813_fu_11613_p2.read());
}

void mnist_fp32::thread_tmp_818_fu_12994_p2() {
    tmp_818_fu_12994_p2 = (!tmp_802_reg_17000.read().is_01() || !tmp_415_cast_fu_12990_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_802_reg_17000.read()) + sc_biguint<11>(tmp_415_cast_fu_12990_p1.read()));
}

void mnist_fp32::thread_tmp_81_fu_7478_p2() {
    tmp_81_fu_7478_p2 = (!p_30_reg_1750.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_30_reg_1750.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp32::thread_tmp_820_fu_13052_p1() {
    tmp_820_fu_13052_p1 = num_zeros_8_fu_13038_p3.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_821_fu_13132_p1() {
    tmp_821_fu_13132_p1 = p_03_i5_to_int_fu_13119_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_822_fu_13150_p1() {
    tmp_822_fu_13150_p1 = tmp_408_to_int_fu_13136_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_823_cast_fu_12999_p1() {
    tmp_823_cast_fu_12999_p1 = esl_zext<64,11>(tmp_818_fu_12994_p2.read());
}

void mnist_fp32::thread_tmp_823_fu_11698_p2() {
    tmp_823_fu_11698_p2 = (!tmp_397_cast_fu_11694_p1.read().is_01() || !tmp_817_reg_16629.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_397_cast_fu_11694_p1.read()) + sc_biguint<9>(tmp_817_reg_16629.read()));
}

void mnist_fp32::thread_tmp_824_cast_fu_11703_p1() {
    tmp_824_cast_fu_11703_p1 = esl_zext<12,9>(tmp_823_fu_11698_p2.read());
}

void mnist_fp32::thread_tmp_824_fu_11715_p2() {
    tmp_824_fu_11715_p2 = (!p_shl112_cast_fu_11707_p3.read().is_01() || !tmp_824_cast_fu_11703_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl112_cast_fu_11707_p3.read()) + sc_biguint<12>(tmp_824_cast_fu_11703_p1.read()));
}

void mnist_fp32::thread_tmp_825_fu_11725_p2() {
    tmp_825_fu_11725_p2 = (!tmp_398_fu_11721_p1.read().is_01() || !tmp_815_reg_16624.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_398_fu_11721_p1.read()) + sc_biguint<64>(tmp_815_reg_16624.read()));
}

void mnist_fp32::thread_tmp_826_fu_11730_p1() {
    tmp_826_fu_11730_p1 = tmp_825_fu_11725_p2.read().range(13-1, 0);
}

void mnist_fp32::thread_tmp_827_fu_11734_p1() {
    tmp_827_fu_11734_p1 = tmp_825_fu_11725_p2.read().range(11-1, 0);
}

void mnist_fp32::thread_tmp_828_fu_11746_p2() {
    tmp_828_fu_11746_p2 = (!p_shl113_cast_fu_11738_p3.read().is_01() || !tmp_826_fu_11730_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl113_cast_fu_11738_p3.read()) - sc_biguint<13>(tmp_826_fu_11730_p1.read()));
}

void mnist_fp32::thread_tmp_829_fu_13711_p1() {
    tmp_829_fu_13711_p1 = p_a_assign_load_to_i_fu_13697_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_82_fu_6445_p3() {
    tmp_82_fu_6445_p3 = (!tmp_538_fu_6433_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_538_fu_6433_p2.read()[0].to_bool())? r_V_11_fu_6427_p2.read(): tmp_544_fu_6439_p2.read());
}

void mnist_fp32::thread_tmp_830_fu_13526_p3() {
    tmp_830_fu_13526_p3 = esl_concat<5,3>(p_89_reg_2476.read(), ap_const_lv3_0);
}

void mnist_fp32::thread_tmp_831_fu_13538_p3() {
    tmp_831_fu_13538_p3 = esl_concat<5,1>(p_89_reg_2476.read(), ap_const_lv1_0);
}

void mnist_fp32::thread_tmp_832_fu_13550_p2() {
    tmp_832_fu_13550_p2 = (!p_shl114_cast_fu_13534_p1.read().is_01() || !p_shl115_cast_fu_13546_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl114_cast_fu_13534_p1.read()) + sc_biguint<9>(p_shl115_cast_fu_13546_p1.read()));
}

void mnist_fp32::thread_tmp_833_cast_fu_13613_p1() {
    tmp_833_cast_fu_13613_p1 = esl_zext<64,9>(tmp_833_reg_17176.read());
}

void mnist_fp32::thread_tmp_833_fu_13556_p2() {
    tmp_833_fu_13556_p2 = (!tmp_413_cast_reg_17163.read().is_01() || !tmp_832_fu_13550_p2.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_413_cast_reg_17163.read()) + sc_biguint<9>(tmp_832_fu_13550_p2.read()));
}

void mnist_fp32::thread_tmp_834_cast_fu_11782_p1() {
    tmp_834_cast_fu_11782_p1 = esl_zext<64,12>(tmp_834_fu_11777_p2.read());
}

void mnist_fp32::thread_tmp_834_fu_11777_p2() {
    tmp_834_fu_11777_p2 = (!tmp_421_cast_fu_11773_p1.read().is_01() || !tmp_824_reg_16652.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_421_cast_fu_11773_p1.read()) + sc_biguint<12>(tmp_824_reg_16652.read()));
}

void mnist_fp32::thread_tmp_835_cast_fu_11796_p1() {
    tmp_835_cast_fu_11796_p1 = esl_zext<64,13>(tmp_835_fu_11791_p2.read());
}

void mnist_fp32::thread_tmp_835_fu_11791_p2() {
    tmp_835_fu_11791_p2 = (!tmp_457_cast_fu_11787_p1.read().is_01() || !tmp_828_reg_16657.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_457_cast_fu_11787_p1.read()) + sc_biguint<13>(tmp_828_reg_16657.read()));
}

void mnist_fp32::thread_tmp_836_fu_13729_p1() {
    tmp_836_fu_13729_p1 = compute6_to_int_fu_13715_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_838_fu_13609_p1() {
    tmp_838_fu_13609_p1 = num_zeros_9_fu_13595_p3.read().range(8-1, 0);
}

void mnist_fp32::thread_tmp_839_fu_11805_p1() {
    tmp_839_fu_11805_p1 = ireg_V_16_fu_11801_p1.read().range(63-1, 0);
}

void mnist_fp32::thread_tmp_83_cast_fu_6661_p1() {
    tmp_83_cast_fu_6661_p1 = esl_zext<11,4>(tmp_82_reg_15061.read());
}

void mnist_fp32::thread_tmp_83_fu_2971_p2() {
    tmp_83_fu_2971_p2 = (!ap_const_lv11_5.is_01())? sc_lv<11>(): grp_fu_2962_p2.read() << (unsigned short)ap_const_lv11_5.to_uint();
}

void mnist_fp32::thread_tmp_841_fu_11827_p1() {
    tmp_841_fu_11827_p1 = ireg_V_16_fu_11801_p1.read().range(52-1, 0);
}

void mnist_fp32::thread_tmp_842_fu_11938_p1() {
    tmp_842_fu_11938_p1 = man_V_20_fu_11893_p3.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_843_fu_11942_p4() {
    tmp_843_fu_11942_p4 = sh_amt_10_fu_11924_p3.read().range(11, 5);
}

void mnist_fp32::thread_tmp_844_fu_12060_p1() {
    tmp_844_fu_12060_p1 = tmp_446_fu_12055_p2.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_845_fu_11841_p1() {
    tmp_845_fu_11841_p1 = ireg_V_17_fu_11837_p1.read().range(63-1, 0);
}

void mnist_fp32::thread_tmp_847_fu_11863_p1() {
    tmp_847_fu_11863_p1 = ireg_V_17_fu_11837_p1.read().range(52-1, 0);
}

void mnist_fp32::thread_tmp_848_fu_12023_p1() {
    tmp_848_fu_12023_p1 = man_V_25_fu_11978_p3.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_849_fu_12027_p4() {
    tmp_849_fu_12027_p4 = sh_amt_11_fu_12009_p3.read().range(11, 5);
}

void mnist_fp32::thread_tmp_84_fu_2977_p2() {
    tmp_84_fu_2977_p2 = (!ap_const_lv11_2.is_01())? sc_lv<11>(): grp_fu_2962_p2.read() << (unsigned short)ap_const_lv11_2.to_uint();
}

void mnist_fp32::thread_tmp_850_fu_12201_p1() {
    tmp_850_fu_12201_p1 = tmp_482_fu_12196_p2.read().range(32-1, 0);
}

void mnist_fp32::thread_tmp_851_fu_13853_p1() {
    tmp_851_fu_13853_p1 = pred_2_to_int_fu_13840_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_852_fu_13871_p1() {
    tmp_852_fu_13871_p1 = pred_to_int_fu_13857_p1.read().range(23-1, 0);
}

void mnist_fp32::thread_tmp_85_cast_fu_6453_p1() {
    tmp_85_cast_fu_6453_p1 = esl_zext<5,4>(tmp_82_fu_6445_p3.read());
}

void mnist_fp32::thread_tmp_85_fu_3630_p2() {
    tmp_85_fu_3630_p2 = (!sh_amt_cast_fu_3602_p1.read().is_01())? sc_lv<32>(): tmp_148_reg_14292.read() << (unsigned short)sh_amt_cast_fu_3602_p1.read().to_uint();
}

void mnist_fp32::thread_tmp_87_fu_5487_p4() {
    tmp_87_fu_5487_p4 = conv2_0_load_to_int_fu_5484_p1.read().range(30, 23);
}

void mnist_fp32::thread_tmp_88_fu_5566_p1() {
    tmp_88_fu_5566_p1 = esl_zext<12,11>(p_Result_3_reg_14820.read());
}

void mnist_fp32::thread_tmp_89_fu_2983_p2() {
    tmp_89_fu_2983_p2 = (!tmp_83_fu_2971_p2.read().is_01() || !tmp_84_fu_2977_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_83_fu_2971_p2.read()) - sc_biguint<11>(tmp_84_fu_2977_p2.read()));
}

void mnist_fp32::thread_tmp_8_cast_fu_3089_p1() {
    tmp_8_cast_fu_3089_p1 = esl_zext<10,5>(p_4_reg_1147.read());
}

void mnist_fp32::thread_tmp_8_fu_3055_p3() {
    tmp_8_fu_3055_p3 = esl_concat<3,5>(p_1_reg_1136.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_tmp_90_fu_5560_p2() {
    tmp_90_fu_5560_p2 = (!tmp_394_fu_5534_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_394_fu_5534_p1.read() == ap_const_lv63_0);
}

void mnist_fp32::thread_tmp_91_cast_fu_5284_p1() {
    tmp_91_cast_fu_5284_p1 = esl_zext<13,5>(p_17_reg_1338.read());
}

void mnist_fp32::thread_tmp_91_fu_2989_p2() {
    tmp_91_fu_2989_p2 = (!tmp_cast_fu_2968_p1.read().is_01() || !tmp_89_fu_2983_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_cast_fu_2968_p1.read()) + sc_biguint<11>(tmp_89_fu_2983_p2.read()));
}

void mnist_fp32::thread_tmp_92_cast_fu_4648_p1() {
    tmp_92_cast_fu_4648_p1 = esl_zext<13,5>(r_V_10_fu_4642_p2.read());
}

void mnist_fp32::thread_tmp_92_fu_3003_p2() {
    tmp_92_fu_3003_p2 = (!tmp_s_reg_14033.read().is_01() || !tmp_10_cast1_fu_2999_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_s_reg_14033.read()) + sc_biguint<11>(tmp_10_cast1_fu_2999_p1.read()));
}

void mnist_fp32::thread_tmp_94_fu_4748_p1() {
    tmp_94_fu_4748_p1 = esl_zext<12,11>(exp_tmp_V_reg_14587.read());
}

void mnist_fp32::thread_tmp_95_fu_5513_p2() {
    tmp_95_fu_5513_p2 = (notrhs1_reg_14804.read() | notlhs1_reg_14799.read());
}

void mnist_fp32::thread_tmp_96_fu_4706_p2() {
    tmp_96_fu_4706_p2 = (!tmp_531_fu_4680_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_531_fu_4680_p1.read() == ap_const_lv63_0);
}

void mnist_fp32::thread_tmp_97_fu_3731_p3() {
    tmp_97_fu_3731_p3 = esl_concat<3,5>(p_5_reg_1251.read(), ap_const_lv5_0);
}

void mnist_fp32::thread_tmp_98_cast_fu_3319_p1() {
    tmp_98_cast_fu_3319_p1 = esl_sext<10,9>(tmp_45_fu_3313_p2.read());
}

void mnist_fp32::thread_tmp_98_fu_3743_p3() {
    tmp_98_fu_3743_p3 = esl_concat<3,1>(p_5_reg_1251.read(), ap_const_lv1_0);
}

void mnist_fp32::thread_tmp_99_fu_3755_p2() {
    tmp_99_fu_3755_p2 = (!p_shl24_cast_fu_3739_p1.read().is_01() || !p_shl25_cast_fu_3751_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl24_cast_fu_3739_p1.read()) - sc_biguint<9>(p_shl25_cast_fu_3751_p1.read()));
}

void mnist_fp32::thread_tmp_9_fu_2866_p3() {
    tmp_9_fu_2866_p3 = (!tmp_57_fu_2854_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_57_fu_2854_p2.read()[0].to_bool())? r_V_fu_2848_p2.read(): tmp_59_fu_2860_p2.read());
}

void mnist_fp32::thread_tmp_V_2_fu_6080_p3() {
    tmp_V_2_fu_6080_p3 = (!tmp_575_reg_14956.read()[0].is_01())? sc_lv<32>(): ((tmp_575_reg_14956.read()[0].to_bool())? tmp_169_reg_14962.read(): relu2_0_V_load_reg_14950.read());
}

void mnist_fp32::thread_tmp_V_5_fu_9643_p3() {
    tmp_V_5_fu_9643_p3 = (!tmp_728_reg_15993.read()[0].is_01())? sc_lv<32>(): ((tmp_728_reg_15993.read()[0].to_bool())? tmp_327_reg_15999.read(): relu4_0_V_load_reg_15987.read());
}

void mnist_fp32::thread_tmp_V_8_fu_13023_p3() {
    tmp_V_8_fu_13023_p3 = (!tmp_819_reg_17024.read()[0].is_01())? sc_lv<32>(): ((tmp_819_reg_17024.read()[0].to_bool())? tmp_454_reg_17030.read(): relu6_0_V_load_reg_17018.read());
}

void mnist_fp32::thread_tmp_V_9_fu_13580_p3() {
    tmp_V_9_fu_13580_p3 = (!tmp_837_reg_17192.read()[0].is_01())? sc_lv<32>(): ((tmp_837_reg_17192.read()[0].to_bool())? tmp_472_reg_17198.read(): pool3_0_V_load_reg_17186.read());
}

void mnist_fp32::thread_tmp_cast_fu_2968_p1() {
    tmp_cast_fu_2968_p1 = esl_zext<11,5>(tmp_9_reg_14057.read());
}

void mnist_fp32::thread_tmp_fu_2752_p2() {
    tmp_fu_2752_p2 = (!r_V_1_fu_2746_p2.read().is_01() || !ap_const_lv11_7E3.is_01())? sc_lv<11>(): (sc_biguint<11>(r_V_1_fu_2746_p2.read()) + sc_bigint<11>(ap_const_lv11_7E3));
}

void mnist_fp32::thread_tmp_s_fu_2728_p2() {
    tmp_s_fu_2728_p2 = (!p_shl6_cast_fu_2712_p1.read().is_01() || !p_shl7_cast_fu_2724_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl6_cast_fu_2712_p1.read()) - sc_biguint<11>(p_shl7_cast_fu_2724_p1.read()));
}

void mnist_fp32::thread_w1_V_fu_9501_p2() {
    w1_V_fu_9501_p2 = (!p_64_reg_1948.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_64_reg_1948.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_w_V_fu_5938_p2() {
    w_V_fu_5938_p2 = (!p_32_reg_1474.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_32_reg_1474.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_w_conv1_0_address0() {
    w_conv1_0_address0 =  (sc_lv<6>) (tmp_548_cast_fu_3272_p1.read());
}

void mnist_fp32::thread_w_conv1_0_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state28.read())) {
        w_conv1_0_ce0 = ap_const_logic_1;
    } else {
        w_conv1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_w_conv2_address0() {
    w_conv2_address0 =  (sc_lv<8>) (tmp_623_cast_fu_4671_p1.read());
}

void mnist_fp32::thread_w_conv2_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state85.read())) {
        w_conv2_ce0 = ap_const_logic_1;
    } else {
        w_conv2_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_w_conv3_address0() {
    w_conv3_address0 =  (sc_lv<9>) (tmp_706_cast_fu_6989_p1.read());
}

void mnist_fp32::thread_w_conv3_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state150.read())) {
        w_conv3_ce0 = ap_const_logic_1;
    } else {
        w_conv3_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_w_conv4_address0() {
    w_conv4_address0 =  (sc_lv<10>) (tmp_740_cast_fu_8250_p1.read());
}

void mnist_fp32::thread_w_conv4_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state205.read())) {
        w_conv4_ce0 = ap_const_logic_1;
    } else {
        w_conv4_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_w_conv5_address0() {
    w_conv5_address0 =  (sc_lv<11>) (tmp_811_cast_fu_10539_p1.read());
}

void mnist_fp32::thread_w_conv5_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state269.read())) {
        w_conv5_ce0 = ap_const_logic_1;
    } else {
        w_conv5_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_w_conv6_address0() {
    w_conv6_address0 =  (sc_lv<12>) (tmp_835_cast_fu_11796_p1.read());
}

void mnist_fp32::thread_w_conv6_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state323.read())) {
        w_conv6_ce0 = ap_const_logic_1;
    } else {
        w_conv6_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_w_dense_1_address0() {
    w_dense_1_address0 =  (sc_lv<8>) (tmp_833_cast_fu_13613_p1.read());
}

void mnist_fp32::thread_w_dense_1_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state372.read())) {
        w_dense_1_ce0 = ap_const_logic_1;
    } else {
        w_dense_1_ce0 = ap_const_logic_0;
    }
}

void mnist_fp32::thread_xx1_V_fu_4445_p2() {
    xx1_V_fu_4445_p2 = (!p_17_reg_1338.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_17_reg_1338.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_xx2_V_fu_6828_p2() {
    xx2_V_fu_6828_p2 = (!p_31_reg_1612.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_31_reg_1612.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_xx3_V_fu_8097_p2() {
    xx3_V_fu_8097_p2 = (!p_43_reg_1811.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_43_reg_1811.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_xx4_V_fu_10327_p2() {
    xx4_V_fu_10327_p2 = (!p_57_reg_2085.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_57_reg_2085.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_xx5_V_fu_11583_p2() {
    xx5_V_fu_11583_p2 = (!p_66_reg_2282.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_66_reg_2282.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_xx_V_fu_3134_p2() {
    xx_V_fu_3134_p2 = (!p_7_reg_1159.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_7_reg_1159.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_yy1_V_fu_4394_p2() {
    yy1_V_fu_4394_p2 = (!p_3_reg_1326.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_3_reg_1326.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp32::thread_yy2_V_fu_6777_p2() {
    yy2_V_fu_6777_p2 = (!p_24_reg_1600.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_24_reg_1600.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_yy3_V_fu_8046_p2() {
    yy3_V_fu_8046_p2 = (!p_34_reg_1799.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_34_reg_1799.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp32::thread_yy4_V_fu_10280_p2() {
    yy4_V_fu_10280_p2 = (!p_48_reg_2074.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_48_reg_2074.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_yy5_V_fu_11536_p2() {
    yy5_V_fu_11536_p2 = (!p_61_reg_2271.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_61_reg_2271.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp32::thread_yy_V_fu_3083_p2() {
    yy_V_fu_3083_p2 = (!p_4_reg_1147.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_4_reg_1147.read()) + sc_biguint<5>(ap_const_lv5_1));
}

}

