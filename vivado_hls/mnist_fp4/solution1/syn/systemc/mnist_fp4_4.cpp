#include "mnist_fp4.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp4::thread_F2_1_fu_5277_p2() {
    F2_1_fu_5277_p2 = (!ap_const_lv12_433.is_01() || !tmp_99_fu_5250_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_99_fu_5250_p1.read()));
}

void mnist_fp4::thread_F2_2_fu_4424_p2() {
    F2_2_fu_4424_p2 = (!ap_const_lv12_433.is_01() || !tmp_106_fu_4397_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_106_fu_4397_p1.read()));
}

void mnist_fp4::thread_F2_3_fu_4509_p2() {
    F2_3_fu_4509_p2 = (!ap_const_lv12_433.is_01() || !tmp_170_fu_4482_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_170_fu_4482_p1.read()));
}

void mnist_fp4::thread_F2_4_fu_7006_p2() {
    F2_4_fu_7006_p2 = (!ap_const_lv12_433.is_01() || !tmp_210_fu_6979_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_210_fu_6979_p1.read()));
}

void mnist_fp4::thread_F2_5_fu_9145_p2() {
    F2_5_fu_9145_p2 = (!ap_const_lv12_433.is_01() || !tmp_277_fu_9118_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_277_fu_9118_p1.read()));
}

void mnist_fp4::thread_F2_6_fu_8292_p2() {
    F2_6_fu_8292_p2 = (!ap_const_lv12_433.is_01() || !tmp_284_fu_8265_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_284_fu_8265_p1.read()));
}

void mnist_fp4::thread_F2_7_fu_8377_p2() {
    F2_7_fu_8377_p2 = (!ap_const_lv12_433.is_01() || !tmp_334_fu_8350_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_334_fu_8350_p1.read()));
}

void mnist_fp4::thread_F2_fu_3002_p2() {
    F2_fu_3002_p2 = (!ap_const_lv12_433.is_01() || !tmp_32_fu_2975_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_32_fu_2975_p1.read()));
}

void mnist_fp4::thread_ap_CS_fsm_pp1_stage0() {
    ap_CS_fsm_pp1_stage0 = ap_CS_fsm.read()[4];
}

void mnist_fp4::thread_ap_CS_fsm_pp2_stage0() {
    ap_CS_fsm_pp2_stage0 = ap_CS_fsm.read()[6];
}

void mnist_fp4::thread_ap_CS_fsm_state1() {
    ap_CS_fsm_state1 = ap_CS_fsm.read()[0];
}

void mnist_fp4::thread_ap_CS_fsm_state10() {
    ap_CS_fsm_state10 = ap_CS_fsm.read()[7];
}

void mnist_fp4::thread_ap_CS_fsm_state100() {
    ap_CS_fsm_state100 = ap_CS_fsm.read()[97];
}

void mnist_fp4::thread_ap_CS_fsm_state101() {
    ap_CS_fsm_state101 = ap_CS_fsm.read()[98];
}

void mnist_fp4::thread_ap_CS_fsm_state102() {
    ap_CS_fsm_state102 = ap_CS_fsm.read()[99];
}

void mnist_fp4::thread_ap_CS_fsm_state103() {
    ap_CS_fsm_state103 = ap_CS_fsm.read()[100];
}

void mnist_fp4::thread_ap_CS_fsm_state108() {
    ap_CS_fsm_state108 = ap_CS_fsm.read()[105];
}

void mnist_fp4::thread_ap_CS_fsm_state109() {
    ap_CS_fsm_state109 = ap_CS_fsm.read()[106];
}

void mnist_fp4::thread_ap_CS_fsm_state11() {
    ap_CS_fsm_state11 = ap_CS_fsm.read()[8];
}

void mnist_fp4::thread_ap_CS_fsm_state110() {
    ap_CS_fsm_state110 = ap_CS_fsm.read()[107];
}

void mnist_fp4::thread_ap_CS_fsm_state111() {
    ap_CS_fsm_state111 = ap_CS_fsm.read()[108];
}

void mnist_fp4::thread_ap_CS_fsm_state112() {
    ap_CS_fsm_state112 = ap_CS_fsm.read()[109];
}

void mnist_fp4::thread_ap_CS_fsm_state113() {
    ap_CS_fsm_state113 = ap_CS_fsm.read()[110];
}

void mnist_fp4::thread_ap_CS_fsm_state114() {
    ap_CS_fsm_state114 = ap_CS_fsm.read()[111];
}

void mnist_fp4::thread_ap_CS_fsm_state115() {
    ap_CS_fsm_state115 = ap_CS_fsm.read()[112];
}

void mnist_fp4::thread_ap_CS_fsm_state116() {
    ap_CS_fsm_state116 = ap_CS_fsm.read()[113];
}

void mnist_fp4::thread_ap_CS_fsm_state117() {
    ap_CS_fsm_state117 = ap_CS_fsm.read()[114];
}

void mnist_fp4::thread_ap_CS_fsm_state118() {
    ap_CS_fsm_state118 = ap_CS_fsm.read()[115];
}

void mnist_fp4::thread_ap_CS_fsm_state119() {
    ap_CS_fsm_state119 = ap_CS_fsm.read()[116];
}

void mnist_fp4::thread_ap_CS_fsm_state12() {
    ap_CS_fsm_state12 = ap_CS_fsm.read()[9];
}

void mnist_fp4::thread_ap_CS_fsm_state120() {
    ap_CS_fsm_state120 = ap_CS_fsm.read()[117];
}

void mnist_fp4::thread_ap_CS_fsm_state121() {
    ap_CS_fsm_state121 = ap_CS_fsm.read()[118];
}

void mnist_fp4::thread_ap_CS_fsm_state122() {
    ap_CS_fsm_state122 = ap_CS_fsm.read()[119];
}

void mnist_fp4::thread_ap_CS_fsm_state123() {
    ap_CS_fsm_state123 = ap_CS_fsm.read()[120];
}

void mnist_fp4::thread_ap_CS_fsm_state124() {
    ap_CS_fsm_state124 = ap_CS_fsm.read()[121];
}

void mnist_fp4::thread_ap_CS_fsm_state125() {
    ap_CS_fsm_state125 = ap_CS_fsm.read()[122];
}

void mnist_fp4::thread_ap_CS_fsm_state126() {
    ap_CS_fsm_state126 = ap_CS_fsm.read()[123];
}

void mnist_fp4::thread_ap_CS_fsm_state127() {
    ap_CS_fsm_state127 = ap_CS_fsm.read()[124];
}

void mnist_fp4::thread_ap_CS_fsm_state13() {
    ap_CS_fsm_state13 = ap_CS_fsm.read()[10];
}

void mnist_fp4::thread_ap_CS_fsm_state132() {
    ap_CS_fsm_state132 = ap_CS_fsm.read()[129];
}

void mnist_fp4::thread_ap_CS_fsm_state133() {
    ap_CS_fsm_state133 = ap_CS_fsm.read()[130];
}

void mnist_fp4::thread_ap_CS_fsm_state134() {
    ap_CS_fsm_state134 = ap_CS_fsm.read()[131];
}

void mnist_fp4::thread_ap_CS_fsm_state135() {
    ap_CS_fsm_state135 = ap_CS_fsm.read()[132];
}

void mnist_fp4::thread_ap_CS_fsm_state136() {
    ap_CS_fsm_state136 = ap_CS_fsm.read()[133];
}

void mnist_fp4::thread_ap_CS_fsm_state137() {
    ap_CS_fsm_state137 = ap_CS_fsm.read()[134];
}

void mnist_fp4::thread_ap_CS_fsm_state138() {
    ap_CS_fsm_state138 = ap_CS_fsm.read()[135];
}

void mnist_fp4::thread_ap_CS_fsm_state139() {
    ap_CS_fsm_state139 = ap_CS_fsm.read()[136];
}

void mnist_fp4::thread_ap_CS_fsm_state14() {
    ap_CS_fsm_state14 = ap_CS_fsm.read()[11];
}

void mnist_fp4::thread_ap_CS_fsm_state153() {
    ap_CS_fsm_state153 = ap_CS_fsm.read()[150];
}

void mnist_fp4::thread_ap_CS_fsm_state154() {
    ap_CS_fsm_state154 = ap_CS_fsm.read()[151];
}

void mnist_fp4::thread_ap_CS_fsm_state155() {
    ap_CS_fsm_state155 = ap_CS_fsm.read()[152];
}

void mnist_fp4::thread_ap_CS_fsm_state156() {
    ap_CS_fsm_state156 = ap_CS_fsm.read()[153];
}

void mnist_fp4::thread_ap_CS_fsm_state157() {
    ap_CS_fsm_state157 = ap_CS_fsm.read()[154];
}

void mnist_fp4::thread_ap_CS_fsm_state158() {
    ap_CS_fsm_state158 = ap_CS_fsm.read()[155];
}

void mnist_fp4::thread_ap_CS_fsm_state159() {
    ap_CS_fsm_state159 = ap_CS_fsm.read()[156];
}

void mnist_fp4::thread_ap_CS_fsm_state160() {
    ap_CS_fsm_state160 = ap_CS_fsm.read()[157];
}

void mnist_fp4::thread_ap_CS_fsm_state161() {
    ap_CS_fsm_state161 = ap_CS_fsm.read()[158];
}

void mnist_fp4::thread_ap_CS_fsm_state162() {
    ap_CS_fsm_state162 = ap_CS_fsm.read()[159];
}

void mnist_fp4::thread_ap_CS_fsm_state163() {
    ap_CS_fsm_state163 = ap_CS_fsm.read()[160];
}

void mnist_fp4::thread_ap_CS_fsm_state164() {
    ap_CS_fsm_state164 = ap_CS_fsm.read()[161];
}

void mnist_fp4::thread_ap_CS_fsm_state167() {
    ap_CS_fsm_state167 = ap_CS_fsm.read()[164];
}

void mnist_fp4::thread_ap_CS_fsm_state168() {
    ap_CS_fsm_state168 = ap_CS_fsm.read()[165];
}

void mnist_fp4::thread_ap_CS_fsm_state172() {
    ap_CS_fsm_state172 = ap_CS_fsm.read()[169];
}

void mnist_fp4::thread_ap_CS_fsm_state173() {
    ap_CS_fsm_state173 = ap_CS_fsm.read()[170];
}

void mnist_fp4::thread_ap_CS_fsm_state174() {
    ap_CS_fsm_state174 = ap_CS_fsm.read()[171];
}

void mnist_fp4::thread_ap_CS_fsm_state175() {
    ap_CS_fsm_state175 = ap_CS_fsm.read()[172];
}

void mnist_fp4::thread_ap_CS_fsm_state176() {
    ap_CS_fsm_state176 = ap_CS_fsm.read()[173];
}

void mnist_fp4::thread_ap_CS_fsm_state177() {
    ap_CS_fsm_state177 = ap_CS_fsm.read()[174];
}

void mnist_fp4::thread_ap_CS_fsm_state178() {
    ap_CS_fsm_state178 = ap_CS_fsm.read()[175];
}

void mnist_fp4::thread_ap_CS_fsm_state179() {
    ap_CS_fsm_state179 = ap_CS_fsm.read()[176];
}

void mnist_fp4::thread_ap_CS_fsm_state180() {
    ap_CS_fsm_state180 = ap_CS_fsm.read()[177];
}

void mnist_fp4::thread_ap_CS_fsm_state181() {
    ap_CS_fsm_state181 = ap_CS_fsm.read()[178];
}

void mnist_fp4::thread_ap_CS_fsm_state182() {
    ap_CS_fsm_state182 = ap_CS_fsm.read()[179];
}

void mnist_fp4::thread_ap_CS_fsm_state183() {
    ap_CS_fsm_state183 = ap_CS_fsm.read()[180];
}

void mnist_fp4::thread_ap_CS_fsm_state184() {
    ap_CS_fsm_state184 = ap_CS_fsm.read()[181];
}

void mnist_fp4::thread_ap_CS_fsm_state185() {
    ap_CS_fsm_state185 = ap_CS_fsm.read()[182];
}

void mnist_fp4::thread_ap_CS_fsm_state186() {
    ap_CS_fsm_state186 = ap_CS_fsm.read()[183];
}

void mnist_fp4::thread_ap_CS_fsm_state2() {
    ap_CS_fsm_state2 = ap_CS_fsm.read()[1];
}

void mnist_fp4::thread_ap_CS_fsm_state201() {
    ap_CS_fsm_state201 = ap_CS_fsm.read()[198];
}

void mnist_fp4::thread_ap_CS_fsm_state202() {
    ap_CS_fsm_state202 = ap_CS_fsm.read()[199];
}

void mnist_fp4::thread_ap_CS_fsm_state203() {
    ap_CS_fsm_state203 = ap_CS_fsm.read()[200];
}

void mnist_fp4::thread_ap_CS_fsm_state204() {
    ap_CS_fsm_state204 = ap_CS_fsm.read()[201];
}

void mnist_fp4::thread_ap_CS_fsm_state205() {
    ap_CS_fsm_state205 = ap_CS_fsm.read()[202];
}

void mnist_fp4::thread_ap_CS_fsm_state206() {
    ap_CS_fsm_state206 = ap_CS_fsm.read()[203];
}

void mnist_fp4::thread_ap_CS_fsm_state211() {
    ap_CS_fsm_state211 = ap_CS_fsm.read()[208];
}

void mnist_fp4::thread_ap_CS_fsm_state212() {
    ap_CS_fsm_state212 = ap_CS_fsm.read()[209];
}

void mnist_fp4::thread_ap_CS_fsm_state213() {
    ap_CS_fsm_state213 = ap_CS_fsm.read()[210];
}

void mnist_fp4::thread_ap_CS_fsm_state214() {
    ap_CS_fsm_state214 = ap_CS_fsm.read()[211];
}

void mnist_fp4::thread_ap_CS_fsm_state215() {
    ap_CS_fsm_state215 = ap_CS_fsm.read()[212];
}

void mnist_fp4::thread_ap_CS_fsm_state216() {
    ap_CS_fsm_state216 = ap_CS_fsm.read()[213];
}

void mnist_fp4::thread_ap_CS_fsm_state217() {
    ap_CS_fsm_state217 = ap_CS_fsm.read()[214];
}

void mnist_fp4::thread_ap_CS_fsm_state218() {
    ap_CS_fsm_state218 = ap_CS_fsm.read()[215];
}

void mnist_fp4::thread_ap_CS_fsm_state219() {
    ap_CS_fsm_state219 = ap_CS_fsm.read()[216];
}

void mnist_fp4::thread_ap_CS_fsm_state220() {
    ap_CS_fsm_state220 = ap_CS_fsm.read()[217];
}

void mnist_fp4::thread_ap_CS_fsm_state221() {
    ap_CS_fsm_state221 = ap_CS_fsm.read()[218];
}

void mnist_fp4::thread_ap_CS_fsm_state222() {
    ap_CS_fsm_state222 = ap_CS_fsm.read()[219];
}

void mnist_fp4::thread_ap_CS_fsm_state223() {
    ap_CS_fsm_state223 = ap_CS_fsm.read()[220];
}

void mnist_fp4::thread_ap_CS_fsm_state224() {
    ap_CS_fsm_state224 = ap_CS_fsm.read()[221];
}

void mnist_fp4::thread_ap_CS_fsm_state225() {
    ap_CS_fsm_state225 = ap_CS_fsm.read()[222];
}

void mnist_fp4::thread_ap_CS_fsm_state230() {
    ap_CS_fsm_state230 = ap_CS_fsm.read()[227];
}

void mnist_fp4::thread_ap_CS_fsm_state231() {
    ap_CS_fsm_state231 = ap_CS_fsm.read()[228];
}

void mnist_fp4::thread_ap_CS_fsm_state232() {
    ap_CS_fsm_state232 = ap_CS_fsm.read()[229];
}

void mnist_fp4::thread_ap_CS_fsm_state233() {
    ap_CS_fsm_state233 = ap_CS_fsm.read()[230];
}

void mnist_fp4::thread_ap_CS_fsm_state234() {
    ap_CS_fsm_state234 = ap_CS_fsm.read()[231];
}

void mnist_fp4::thread_ap_CS_fsm_state235() {
    ap_CS_fsm_state235 = ap_CS_fsm.read()[232];
}

void mnist_fp4::thread_ap_CS_fsm_state236() {
    ap_CS_fsm_state236 = ap_CS_fsm.read()[233];
}

void mnist_fp4::thread_ap_CS_fsm_state237() {
    ap_CS_fsm_state237 = ap_CS_fsm.read()[234];
}

void mnist_fp4::thread_ap_CS_fsm_state238() {
    ap_CS_fsm_state238 = ap_CS_fsm.read()[235];
}

void mnist_fp4::thread_ap_CS_fsm_state239() {
    ap_CS_fsm_state239 = ap_CS_fsm.read()[236];
}

void mnist_fp4::thread_ap_CS_fsm_state240() {
    ap_CS_fsm_state240 = ap_CS_fsm.read()[237];
}

void mnist_fp4::thread_ap_CS_fsm_state241() {
    ap_CS_fsm_state241 = ap_CS_fsm.read()[238];
}

void mnist_fp4::thread_ap_CS_fsm_state242() {
    ap_CS_fsm_state242 = ap_CS_fsm.read()[239];
}

void mnist_fp4::thread_ap_CS_fsm_state243() {
    ap_CS_fsm_state243 = ap_CS_fsm.read()[240];
}

void mnist_fp4::thread_ap_CS_fsm_state244() {
    ap_CS_fsm_state244 = ap_CS_fsm.read()[241];
}

void mnist_fp4::thread_ap_CS_fsm_state245() {
    ap_CS_fsm_state245 = ap_CS_fsm.read()[242];
}

void mnist_fp4::thread_ap_CS_fsm_state246() {
    ap_CS_fsm_state246 = ap_CS_fsm.read()[243];
}

void mnist_fp4::thread_ap_CS_fsm_state247() {
    ap_CS_fsm_state247 = ap_CS_fsm.read()[244];
}

void mnist_fp4::thread_ap_CS_fsm_state248() {
    ap_CS_fsm_state248 = ap_CS_fsm.read()[245];
}

void mnist_fp4::thread_ap_CS_fsm_state249() {
    ap_CS_fsm_state249 = ap_CS_fsm.read()[246];
}

void mnist_fp4::thread_ap_CS_fsm_state250() {
    ap_CS_fsm_state250 = ap_CS_fsm.read()[247];
}

void mnist_fp4::thread_ap_CS_fsm_state251() {
    ap_CS_fsm_state251 = ap_CS_fsm.read()[248];
}

void mnist_fp4::thread_ap_CS_fsm_state252() {
    ap_CS_fsm_state252 = ap_CS_fsm.read()[249];
}

void mnist_fp4::thread_ap_CS_fsm_state253() {
    ap_CS_fsm_state253 = ap_CS_fsm.read()[250];
}

void mnist_fp4::thread_ap_CS_fsm_state254() {
    ap_CS_fsm_state254 = ap_CS_fsm.read()[251];
}

void mnist_fp4::thread_ap_CS_fsm_state255() {
    ap_CS_fsm_state255 = ap_CS_fsm.read()[252];
}

void mnist_fp4::thread_ap_CS_fsm_state256() {
    ap_CS_fsm_state256 = ap_CS_fsm.read()[253];
}

void mnist_fp4::thread_ap_CS_fsm_state257() {
    ap_CS_fsm_state257 = ap_CS_fsm.read()[254];
}

void mnist_fp4::thread_ap_CS_fsm_state258() {
    ap_CS_fsm_state258 = ap_CS_fsm.read()[255];
}

void mnist_fp4::thread_ap_CS_fsm_state259() {
    ap_CS_fsm_state259 = ap_CS_fsm.read()[256];
}

void mnist_fp4::thread_ap_CS_fsm_state260() {
    ap_CS_fsm_state260 = ap_CS_fsm.read()[257];
}

void mnist_fp4::thread_ap_CS_fsm_state261() {
    ap_CS_fsm_state261 = ap_CS_fsm.read()[258];
}

void mnist_fp4::thread_ap_CS_fsm_state262() {
    ap_CS_fsm_state262 = ap_CS_fsm.read()[259];
}

void mnist_fp4::thread_ap_CS_fsm_state263() {
    ap_CS_fsm_state263 = ap_CS_fsm.read()[260];
}

void mnist_fp4::thread_ap_CS_fsm_state264() {
    ap_CS_fsm_state264 = ap_CS_fsm.read()[261];
}

void mnist_fp4::thread_ap_CS_fsm_state265() {
    ap_CS_fsm_state265 = ap_CS_fsm.read()[262];
}

void mnist_fp4::thread_ap_CS_fsm_state266() {
    ap_CS_fsm_state266 = ap_CS_fsm.read()[263];
}

void mnist_fp4::thread_ap_CS_fsm_state29() {
    ap_CS_fsm_state29 = ap_CS_fsm.read()[26];
}

void mnist_fp4::thread_ap_CS_fsm_state3() {
    ap_CS_fsm_state3 = ap_CS_fsm.read()[2];
}

void mnist_fp4::thread_ap_CS_fsm_state30() {
    ap_CS_fsm_state30 = ap_CS_fsm.read()[27];
}

void mnist_fp4::thread_ap_CS_fsm_state31() {
    ap_CS_fsm_state31 = ap_CS_fsm.read()[28];
}

void mnist_fp4::thread_ap_CS_fsm_state32() {
    ap_CS_fsm_state32 = ap_CS_fsm.read()[29];
}

void mnist_fp4::thread_ap_CS_fsm_state33() {
    ap_CS_fsm_state33 = ap_CS_fsm.read()[30];
}

void mnist_fp4::thread_ap_CS_fsm_state34() {
    ap_CS_fsm_state34 = ap_CS_fsm.read()[31];
}

void mnist_fp4::thread_ap_CS_fsm_state35() {
    ap_CS_fsm_state35 = ap_CS_fsm.read()[32];
}

void mnist_fp4::thread_ap_CS_fsm_state36() {
    ap_CS_fsm_state36 = ap_CS_fsm.read()[33];
}

void mnist_fp4::thread_ap_CS_fsm_state37() {
    ap_CS_fsm_state37 = ap_CS_fsm.read()[34];
}

void mnist_fp4::thread_ap_CS_fsm_state38() {
    ap_CS_fsm_state38 = ap_CS_fsm.read()[35];
}

void mnist_fp4::thread_ap_CS_fsm_state39() {
    ap_CS_fsm_state39 = ap_CS_fsm.read()[36];
}

void mnist_fp4::thread_ap_CS_fsm_state4() {
    ap_CS_fsm_state4 = ap_CS_fsm.read()[3];
}

void mnist_fp4::thread_ap_CS_fsm_state42() {
    ap_CS_fsm_state42 = ap_CS_fsm.read()[39];
}

void mnist_fp4::thread_ap_CS_fsm_state43() {
    ap_CS_fsm_state43 = ap_CS_fsm.read()[40];
}

void mnist_fp4::thread_ap_CS_fsm_state47() {
    ap_CS_fsm_state47 = ap_CS_fsm.read()[44];
}

void mnist_fp4::thread_ap_CS_fsm_state48() {
    ap_CS_fsm_state48 = ap_CS_fsm.read()[45];
}

void mnist_fp4::thread_ap_CS_fsm_state49() {
    ap_CS_fsm_state49 = ap_CS_fsm.read()[46];
}

void mnist_fp4::thread_ap_CS_fsm_state50() {
    ap_CS_fsm_state50 = ap_CS_fsm.read()[47];
}

void mnist_fp4::thread_ap_CS_fsm_state51() {
    ap_CS_fsm_state51 = ap_CS_fsm.read()[48];
}

void mnist_fp4::thread_ap_CS_fsm_state52() {
    ap_CS_fsm_state52 = ap_CS_fsm.read()[49];
}

void mnist_fp4::thread_ap_CS_fsm_state53() {
    ap_CS_fsm_state53 = ap_CS_fsm.read()[50];
}

void mnist_fp4::thread_ap_CS_fsm_state54() {
    ap_CS_fsm_state54 = ap_CS_fsm.read()[51];
}

void mnist_fp4::thread_ap_CS_fsm_state55() {
    ap_CS_fsm_state55 = ap_CS_fsm.read()[52];
}

void mnist_fp4::thread_ap_CS_fsm_state56() {
    ap_CS_fsm_state56 = ap_CS_fsm.read()[53];
}

void mnist_fp4::thread_ap_CS_fsm_state57() {
    ap_CS_fsm_state57 = ap_CS_fsm.read()[54];
}

void mnist_fp4::thread_ap_CS_fsm_state58() {
    ap_CS_fsm_state58 = ap_CS_fsm.read()[55];
}

void mnist_fp4::thread_ap_CS_fsm_state59() {
    ap_CS_fsm_state59 = ap_CS_fsm.read()[56];
}

void mnist_fp4::thread_ap_CS_fsm_state60() {
    ap_CS_fsm_state60 = ap_CS_fsm.read()[57];
}

void mnist_fp4::thread_ap_CS_fsm_state61() {
    ap_CS_fsm_state61 = ap_CS_fsm.read()[58];
}

void mnist_fp4::thread_ap_CS_fsm_state7() {
    ap_CS_fsm_state7 = ap_CS_fsm.read()[5];
}

void mnist_fp4::thread_ap_CS_fsm_state78() {
    ap_CS_fsm_state78 = ap_CS_fsm.read()[75];
}

void mnist_fp4::thread_ap_CS_fsm_state79() {
    ap_CS_fsm_state79 = ap_CS_fsm.read()[76];
}

void mnist_fp4::thread_ap_CS_fsm_state80() {
    ap_CS_fsm_state80 = ap_CS_fsm.read()[77];
}

void mnist_fp4::thread_ap_CS_fsm_state81() {
    ap_CS_fsm_state81 = ap_CS_fsm.read()[78];
}

void mnist_fp4::thread_ap_CS_fsm_state82() {
    ap_CS_fsm_state82 = ap_CS_fsm.read()[79];
}

void mnist_fp4::thread_ap_CS_fsm_state83() {
    ap_CS_fsm_state83 = ap_CS_fsm.read()[80];
}

void mnist_fp4::thread_ap_CS_fsm_state88() {
    ap_CS_fsm_state88 = ap_CS_fsm.read()[85];
}

void mnist_fp4::thread_ap_CS_fsm_state89() {
    ap_CS_fsm_state89 = ap_CS_fsm.read()[86];
}

void mnist_fp4::thread_ap_CS_fsm_state90() {
    ap_CS_fsm_state90 = ap_CS_fsm.read()[87];
}

void mnist_fp4::thread_ap_CS_fsm_state91() {
    ap_CS_fsm_state91 = ap_CS_fsm.read()[88];
}

void mnist_fp4::thread_ap_CS_fsm_state92() {
    ap_CS_fsm_state92 = ap_CS_fsm.read()[89];
}

void mnist_fp4::thread_ap_CS_fsm_state93() {
    ap_CS_fsm_state93 = ap_CS_fsm.read()[90];
}

void mnist_fp4::thread_ap_CS_fsm_state94() {
    ap_CS_fsm_state94 = ap_CS_fsm.read()[91];
}

void mnist_fp4::thread_ap_CS_fsm_state95() {
    ap_CS_fsm_state95 = ap_CS_fsm.read()[92];
}

void mnist_fp4::thread_ap_CS_fsm_state96() {
    ap_CS_fsm_state96 = ap_CS_fsm.read()[93];
}

void mnist_fp4::thread_ap_CS_fsm_state97() {
    ap_CS_fsm_state97 = ap_CS_fsm.read()[94];
}

void mnist_fp4::thread_ap_CS_fsm_state98() {
    ap_CS_fsm_state98 = ap_CS_fsm.read()[95];
}

void mnist_fp4::thread_ap_CS_fsm_state99() {
    ap_CS_fsm_state99 = ap_CS_fsm.read()[96];
}

void mnist_fp4::thread_ap_block_pp1_stage0() {
    ap_block_pp1_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp4::thread_ap_block_pp1_stage0_11001() {
    ap_block_pp1_stage0_11001 = (esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op346_read_state6.read()));
}

void mnist_fp4::thread_ap_block_pp1_stage0_subdone() {
    ap_block_pp1_stage0_subdone = (esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op346_read_state6.read()));
}

void mnist_fp4::thread_ap_block_pp2_stage0() {
    ap_block_pp2_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp4::thread_ap_block_pp2_stage0_11001() {
    ap_block_pp2_stage0_11001 = (esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_886.read()) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()));
}

void mnist_fp4::thread_ap_block_pp2_stage0_subdone() {
    ap_block_pp2_stage0_subdone = (esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_886.read()) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()));
}

void mnist_fp4::thread_ap_block_state5_pp1_stage0_iter0() {
    ap_block_state5_pp1_stage0_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp4::thread_ap_block_state6_pp1_stage0_iter1() {
    ap_block_state6_pp1_stage0_iter1 = (esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op346_read_state6.read()));
}

void mnist_fp4::thread_ap_block_state8_pp2_stage0_iter0() {
    ap_block_state8_pp2_stage0_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp4::thread_ap_block_state9_pp2_stage0_iter1() {
    ap_block_state9_pp2_stage0_iter1 = (esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_886.read()) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()));
}

void mnist_fp4::thread_ap_condition_2728() {
    ap_condition_2728 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9772.read()));
}

void mnist_fp4::thread_ap_done() {
    if ((esl_seteq<1,1,1>(exitcond54_fu_9592_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state261.read()))) {
        ap_done = ap_const_logic_1;
    } else {
        ap_done = ap_const_logic_0;
    }
}

void mnist_fp4::thread_ap_enable_pp1() {
    ap_enable_pp1 = (ap_idle_pp1.read() ^ ap_const_logic_1);
}

void mnist_fp4::thread_ap_enable_pp2() {
    ap_enable_pp2 = (ap_idle_pp2.read() ^ ap_const_logic_1);
}

void mnist_fp4::thread_ap_idle() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state1.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void mnist_fp4::thread_ap_idle_pp1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp1_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp1_iter1.read()))) {
        ap_idle_pp1 = ap_const_logic_1;
    } else {
        ap_idle_pp1 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_ap_idle_pp2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp2_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp2_iter1.read()))) {
        ap_idle_pp2 = ap_const_logic_1;
    } else {
        ap_idle_pp2 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_ap_phi_mux_eol_2_phi_fu_889_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp2_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_886.read()))) {
        ap_phi_mux_eol_2_phi_fu_889_p4 = stream_in_V_last_V_0_data_out.read();
    } else {
        ap_phi_mux_eol_2_phi_fu_889_p4 = eol_2_reg_886.read();
    }
}

void mnist_fp4::thread_ap_phi_mux_eol_phi_fu_831_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9772.read()))) {
        ap_phi_mux_eol_phi_fu_831_p4 = ap_phi_mux_pixel_last_V_2_phi_fu_866_p4.read();
    } else {
        ap_phi_mux_eol_phi_fu_831_p4 = eol_reg_827.read();
    }
}

void mnist_fp4::thread_ap_phi_mux_p_1_phi_fu_819_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9772.read()))) {
        ap_phi_mux_p_1_phi_fu_819_p4 = j_V_reg_9776.read();
    } else {
        ap_phi_mux_p_1_phi_fu_819_p4 = p_1_reg_815.read();
    }
}

void mnist_fp4::thread_ap_phi_mux_p_4_phi_fu_1101_p4() {
    ap_phi_mux_p_4_phi_fu_1101_p4 = p_4_reg_1097.read();
}

void mnist_fp4::thread_ap_phi_mux_p_s_phi_fu_925_p4() {
    ap_phi_mux_p_s_phi_fu_925_p4 = p_s_reg_921.read();
}

void mnist_fp4::thread_ap_phi_mux_pixel_data_V_2_phi_fu_878_p4() {
    if (esl_seteq<1,1,1>(ap_condition_2728.read(), ap_const_boolean_1)) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, brmerge_reg_9781.read())) {
            ap_phi_mux_pixel_data_V_2_phi_fu_878_p4 = pixel_data_V_1_reg_850.read();
        } else if (esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_9781.read())) {
            ap_phi_mux_pixel_data_V_2_phi_fu_878_p4 = stream_in_V_data_V_0_data_out.read();
        } else {
            ap_phi_mux_pixel_data_V_2_phi_fu_878_p4 = ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_874.read();
        }
    } else {
        ap_phi_mux_pixel_data_V_2_phi_fu_878_p4 = ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_874.read();
    }
}

void mnist_fp4::thread_ap_phi_mux_pixel_last_V_2_phi_fu_866_p4() {
    if (esl_seteq<1,1,1>(ap_condition_2728.read(), ap_const_boolean_1)) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, brmerge_reg_9781.read())) {
            ap_phi_mux_pixel_last_V_2_phi_fu_866_p4 = eol_1_reg_839.read();
        } else if (esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_9781.read())) {
            ap_phi_mux_pixel_last_V_2_phi_fu_866_p4 = stream_in_V_last_V_0_data_out.read();
        } else {
            ap_phi_mux_pixel_last_V_2_phi_fu_866_p4 = ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_861.read();
        }
    } else {
        ap_phi_mux_pixel_last_V_2_phi_fu_866_p4 = ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_861.read();
    }
}

void mnist_fp4::thread_ap_phi_mux_tmp_257_phi_fu_1601_p6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state212.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, or_cond_42_reg_11241.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_168_reg_11316.read()))) {
        ap_phi_mux_tmp_257_phi_fu_1601_p6 = f_5_fu_7841_p1.read();
    } else {
        ap_phi_mux_tmp_257_phi_fu_1601_p6 = tmp_257_reg_1597.read();
    }
}

void mnist_fp4::thread_ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_874() {
    ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_874 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
}

void mnist_fp4::thread_ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_861() {
    ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_861 =  (sc_lv<1>) ("X");
}

void mnist_fp4::thread_ap_predicate_op346_read_state6() {
    ap_predicate_op346_read_state6 = (esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9772.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_9781.read()));
}

void mnist_fp4::thread_ap_ready() {
    if ((esl_seteq<1,1,1>(exitcond54_fu_9592_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state261.read()))) {
        ap_ready = ap_const_logic_1;
    } else {
        ap_ready = ap_const_logic_0;
    }
}

void mnist_fp4::thread_ap_rst_n_inv() {
    ap_rst_n_inv =  (sc_logic) (~ap_rst_n.read());
}

void mnist_fp4::thread_args01_V_fu_5051_p2() {
    args01_V_fu_5051_p2 = (!p_19_reg_1243.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_19_reg_1243.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_args02_V_fu_6780_p2() {
    args02_V_fu_6780_p2 = (!p_36_reg_1519.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_36_reg_1519.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_args03_V_fu_8919_p2() {
    args03_V_fu_8919_p2 = (!p_50_reg_1717.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_50_reg_1717.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_args04_V_fu_9526_p2() {
    args04_V_fu_9526_p2 = (!p_68_reg_1904.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_68_reg_1904.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_args0_V_fu_2776_p2() {
    args0_V_fu_2776_p2 = (!p_7_reg_1042.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_7_reg_1042.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_args11_V_fu_5097_p2() {
    args11_V_fu_5097_p2 = (!p_23_reg_1254.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_23_reg_1254.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_args12_V_fu_6826_p2() {
    args12_V_fu_6826_p2 = (!p_41_reg_1530.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_41_reg_1530.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_args13_V_fu_8965_p2() {
    args13_V_fu_8965_p2 = (!p_55_reg_1728.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_55_reg_1728.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_args14_V_fu_9538_p2() {
    args14_V_fu_9538_p2 = (!p_70_reg_1915.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_70_reg_1915.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_args1_V_fu_2822_p2() {
    args1_V_fu_2822_p2 = (!p_11_reg_1053.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_11_reg_1053.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_args21_V_fu_5148_p2() {
    args21_V_fu_5148_p2 = (!p_28_reg_1265.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_28_reg_1265.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_args22_V_fu_6877_p2() {
    args22_V_fu_6877_p2 = (!p_47_reg_1541.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_47_reg_1541.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_args23_V_fu_9016_p2() {
    args23_V_fu_9016_p2 = (!p_60_reg_1739.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_60_reg_1739.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_args24_V_fu_9550_p2() {
    args24_V_fu_9550_p2 = (!p_73_reg_1926.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_73_reg_1926.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_args2_V_fu_2873_p2() {
    args2_V_fu_2873_p2 = (!p_16_reg_1064.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_16_reg_1064.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_brmerge_fu_2162_p2() {
    brmerge_fu_2162_p2 = (sof_1_fu_452.read() | ap_phi_mux_eol_phi_fu_831_p4.read());
}

void mnist_fp4::thread_c1_V_fu_9358_p2() {
    c1_V_fu_9358_p2 = (!p_54_reg_1750.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_54_reg_1750.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_c_V_fu_5490_p2() {
    c_V_fu_5490_p2 = (!p_22_reg_1276.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_22_reg_1276.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_conv1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state50.read())) {
        conv1_0_address0 =  (sc_lv<12>) (tmp_368_cast_fu_2888_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state36.read())) {
        conv1_0_address0 =  (sc_lv<12>) (tmp_369_cast_fu_2715_p1.read());
    } else {
        conv1_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp4::thread_conv1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state36.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state50.read()))) {
        conv1_0_ce0 = ap_const_logic_1;
    } else {
        conv1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_conv1_0_load_to_int_fu_2893_p1() {
    conv1_0_load_to_int_fu_2893_p1 = conv1_0_load_reg_10010.read();
}

void mnist_fp4::thread_conv1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond13_fu_2633_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state36.read()))) {
        conv1_0_we0 = ap_const_logic_1;
    } else {
        conv1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_conv2_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state112.read())) {
        conv2_0_address0 =  (sc_lv<12>) (tmp_429_cast_fu_5163_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state109.read())) {
        conv2_0_address0 =  (sc_lv<12>) (tmp_430_cast_fu_5041_p1.read());
    } else {
        conv2_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp4::thread_conv2_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state112.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state109.read()))) {
        conv2_0_ce0 = ap_const_logic_1;
    } else {
        conv2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_conv2_0_d0() {
    conv2_0_d0 = (!tmp_63_reg_10532.read()[0].is_01())? sc_lv<32>(): ((tmp_63_reg_10532.read()[0].to_bool())? ap_const_lv32_0: f_2_fu_5029_p1.read());
}

void mnist_fp4::thread_conv2_0_load_to_int_fu_5168_p1() {
    conv2_0_load_to_int_fu_5168_p1 = conv2_0_load_reg_10601.read();
}

void mnist_fp4::thread_conv2_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state109.read())) {
        conv2_0_we0 = ap_const_logic_1;
    } else {
        conv2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_conv3_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state175.read())) {
        conv3_0_address0 =  (sc_lv<11>) (tmp_506_cast_fu_6892_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state160.read())) {
        conv3_0_address0 =  (sc_lv<11>) (tmp_507_cast_fu_6666_p1.read());
    } else {
        conv3_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp4::thread_conv3_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state160.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state175.read()))) {
        conv3_0_ce0 = ap_const_logic_1;
    } else {
        conv3_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_conv3_0_load_to_int_fu_6897_p1() {
    conv3_0_load_to_int_fu_6897_p1 = conv3_0_load_reg_11104.read();
}

void mnist_fp4::thread_conv3_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond41_fu_6614_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state160.read()))) {
        conv3_0_we0 = ap_const_logic_1;
    } else {
        conv3_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_conv4_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state234.read())) {
        conv4_0_address0 =  (sc_lv<11>) (tmp_558_cast_fu_9031_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state231.read())) {
        conv4_0_address0 =  (sc_lv<11>) (tmp_559_cast_fu_8909_p1.read());
    } else {
        conv4_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp4::thread_conv4_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state234.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state231.read()))) {
        conv4_0_ce0 = ap_const_logic_1;
    } else {
        conv4_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_conv4_0_d0() {
    conv4_0_d0 = (!tmp_193_reg_11612.read()[0].is_01())? sc_lv<32>(): ((tmp_193_reg_11612.read()[0].to_bool())? ap_const_lv32_0: f_6_fu_8897_p1.read());
}

void mnist_fp4::thread_conv4_0_load_to_int_fu_9036_p1() {
    conv4_0_load_to_int_fu_9036_p1 = conv4_0_load_reg_11681.read();
}

void mnist_fp4::thread_conv4_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state231.read())) {
        conv4_0_we0 = ap_const_logic_1;
    } else {
        conv4_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_exitcond11_fu_3977_p2() {
    exitcond11_fu_3977_p2 = (!p_10_reg_1150.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_10_reg_1150.read() == ap_const_lv5_1C);
}

void mnist_fp4::thread_exitcond12_fu_2867_p2() {
    exitcond12_fu_2867_p2 = (!p_16_reg_1064.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_16_reg_1064.read() == ap_const_lv5_1C);
}

void mnist_fp4::thread_exitcond13_fu_2633_p2() {
    exitcond13_fu_2633_p2 = (!p_12_reg_995.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_12_reg_995.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond14_fu_6057_p2() {
    exitcond14_fu_6057_p2 = (!p_13_reg_1356.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_13_reg_1356.read() == ap_const_lv3_4);
}

void mnist_fp4::thread_exitcond15_fu_3349_p2() {
    exitcond15_fu_3349_p2 = (!p_14_reg_1109.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1109.read() == ap_const_lv5_1E);
}

void mnist_fp4::thread_exitcond16_fu_6493_p2() {
    exitcond16_fu_6493_p2 = (!p_15_reg_1413.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_15_reg_1413.read() == ap_const_lv4_8);
}

void mnist_fp4::thread_exitcond17_fu_5045_p2() {
    exitcond17_fu_5045_p2 = (!p_19_reg_1243.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_19_reg_1243.read() == ap_const_lv3_4);
}

void mnist_fp4::thread_exitcond18_fu_2720_p2() {
    exitcond18_fu_2720_p2 = (!p_17_reg_1019.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_17_reg_1019.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond19_fu_4028_p2() {
    exitcond19_fu_4028_p2 = (!p_18_reg_1162.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_18_reg_1162.read() == ap_const_lv5_1C);
}

void mnist_fp4::thread_exitcond1_fu_2105_p2() {
    exitcond1_fu_2105_p2 = (!p_0_reg_804.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_0_reg_804.read() == ap_const_lv5_1C);
}

void mnist_fp4::thread_exitcond20_fu_5484_p2() {
    exitcond20_fu_5484_p2 = (!p_22_reg_1276.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_22_reg_1276.read() == ap_const_lv3_4);
}

void mnist_fp4::thread_exitcond21_fu_6081_p2() {
    exitcond21_fu_6081_p2 = (!p_20_reg_1378.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_20_reg_1378.read() == ap_const_lv5_10);
}

void mnist_fp4::thread_exitcond22_fu_7223_p2() {
    exitcond22_fu_7223_p2 = (!p_21_reg_1552.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_21_reg_1552.read() == ap_const_lv4_8);
}

void mnist_fp4::thread_exitcond23_fu_5091_p2() {
    exitcond23_fu_5091_p2 = (!p_23_reg_1254.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_23_reg_1254.read() == ap_const_lv5_1C);
}

void mnist_fp4::thread_exitcond24_fu_5564_p2() {
    exitcond24_fu_5564_p2 = (!p_27_reg_1287.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_27_reg_1287.read() == ap_const_lv4_E);
}

void mnist_fp4::thread_exitcond25_fu_4040_p2() {
    exitcond25_fu_4040_p2 = (!p_24_reg_1186.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_24_reg_1186.read() == ap_const_lv3_4);
}

void mnist_fp4::thread_exitcond26_fu_6551_p2() {
    exitcond26_fu_6551_p2 = (!p_25_reg_1424.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_25_reg_1424.read() == ap_const_lv4_E);
}

void mnist_fp4::thread_exitcond27_fu_7860_p2() {
    exitcond27_fu_7860_p2 = (!p_26_reg_1612.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_26_reg_1612.read() == ap_const_lv4_8);
}

void mnist_fp4::thread_exitcond28_fu_5142_p2() {
    exitcond28_fu_5142_p2 = (!p_28_reg_1265.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_28_reg_1265.read() == ap_const_lv5_1C);
}

void mnist_fp4::thread_exitcond29_fu_5623_p2() {
    exitcond29_fu_5623_p2 = (!p_33_reg_1298.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_33_reg_1298.read() == ap_const_lv4_E);
}

void mnist_fp4::thread_exitcond30_fu_4183_p2() {
    exitcond30_fu_4183_p2 = (!p_29_reg_1209.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_29_reg_1209.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond31_fu_6169_p2() {
    exitcond31_fu_6169_p2 = (!p_30_reg_1389.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_30_reg_1389.read() == ap_const_lv5_10);
}

void mnist_fp4::thread_exitcond32_fu_7247_p2() {
    exitcond32_fu_7247_p2 = (!p_31_reg_1574.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_31_reg_1574.read() == ap_const_lv5_10);
}

void mnist_fp4::thread_exitcond33_fu_6602_p2() {
    exitcond33_fu_6602_p2 = (!p_32_reg_1436.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_32_reg_1436.read() == ap_const_lv4_E);
}

void mnist_fp4::thread_exitcond34_fu_6774_p2() {
    exitcond34_fu_6774_p2 = (!p_36_reg_1519.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_36_reg_1519.read() == ap_const_lv4_8);
}

void mnist_fp4::thread_exitcond35_fu_4275_p2() {
    exitcond35_fu_4275_p2 = (!p_34_reg_1232.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_34_reg_1232.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond36_fu_7918_p2() {
    exitcond36_fu_7918_p2 = (!p_35_reg_1623.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_35_reg_1623.read() == ap_const_lv4_E);
}

void mnist_fp4::thread_exitcond37_fu_5657_p2() {
    exitcond37_fu_5657_p2 = (!p_38_reg_1322.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_38_reg_1322.read() == ap_const_lv2_2);
}

void mnist_fp4::thread_exitcond38_fu_9412_p2() {
    exitcond38_fu_9412_p2 = (!p_37_reg_1805.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_37_reg_1805.read() == ap_const_lv4_8);
}

void mnist_fp4::thread_exitcond39_fu_6820_p2() {
    exitcond39_fu_6820_p2 = (!p_41_reg_1530.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_41_reg_1530.read() == ap_const_lv4_E);
}

void mnist_fp4::thread_exitcond3_fu_2506_p2() {
    exitcond3_fu_2506_p2 = (!p_2_reg_960.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_2_reg_960.read() == ap_const_lv3_4);
}

void mnist_fp4::thread_exitcond40_fu_7335_p2() {
    exitcond40_fu_7335_p2 = (!p_39_reg_1585.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_39_reg_1585.read() == ap_const_lv5_10);
}

void mnist_fp4::thread_exitcond41_fu_6614_p2() {
    exitcond41_fu_6614_p2 = (!p_40_reg_1448.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_40_reg_1448.read() == ap_const_lv3_4);
}

void mnist_fp4::thread_exitcond42_fu_5717_p2() {
    exitcond42_fu_5717_p2 = (!p_43_reg_1345.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_43_reg_1345.read() == ap_const_lv2_2);
}

void mnist_fp4::thread_exitcond43_fu_9448_p2() {
    exitcond43_fu_9448_p2 = (!p_42_reg_1838.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_42_reg_1838.read() == ap_const_lv5_10);
}

void mnist_fp4::thread_exitcond44_fu_6871_p2() {
    exitcond44_fu_6871_p2 = (!p_47_reg_1541.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_47_reg_1541.read() == ap_const_lv4_E);
}

void mnist_fp4::thread_exitcond45_fu_7969_p2() {
    exitcond45_fu_7969_p2 = (!p_44_reg_1635.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_44_reg_1635.read() == ap_const_lv4_E);
}

void mnist_fp4::thread_exitcond46_fu_9424_p2() {
    exitcond46_fu_9424_p2 = (!p_45_reg_1816.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_45_reg_1816.read() == ap_const_lv4_9);
}

void mnist_fp4::thread_exitcond47_fu_6671_p2() {
    exitcond47_fu_6671_p2 = (!p_46_reg_1473.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_46_reg_1473.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond48_fu_8913_p2() {
    exitcond48_fu_8913_p2 = (!p_50_reg_1717.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_50_reg_1717.read() == ap_const_lv4_8);
}

void mnist_fp4::thread_exitcond49_fu_9556_p2() {
    exitcond49_fu_9556_p2 = (!p_48_reg_1937.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_48_reg_1937.read() == ap_const_lv5_10);
}

void mnist_fp4::thread_exitcond4_fu_2252_p2() {
    exitcond4_fu_2252_p2 = (!p_3_reg_933.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_933.read() == ap_const_lv5_1E);
}

void mnist_fp4::thread_exitcond50_fu_9460_p2() {
    exitcond50_fu_9460_p2 = (!p_49_reg_1849.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_49_reg_1849.read() == ap_const_lv3_7);
}

void mnist_fp4::thread_exitcond51_fu_9352_p2() {
    exitcond51_fu_9352_p2 = (!p_54_reg_1750.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_54_reg_1750.read() == ap_const_lv4_8);
}

void mnist_fp4::thread_exitcond52_fu_7981_p2() {
    exitcond52_fu_7981_p2 = (!p_51_reg_1659.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_51_reg_1659.read() == ap_const_lv4_8);
}

void mnist_fp4::thread_exitcond53_fu_6724_p2() {
    exitcond53_fu_6724_p2 = (!p_52_reg_1496.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_52_reg_1496.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond54_fu_9592_p2() {
    exitcond54_fu_9592_p2 = (!p_53_reg_1970.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_53_reg_1970.read() == ap_const_lv5_10);
}

void mnist_fp4::thread_exitcond55_fu_8959_p2() {
    exitcond55_fu_8959_p2 = (!p_55_reg_1728.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_55_reg_1728.read() == ap_const_lv4_E);
}

void mnist_fp4::thread_exitcond56_fu_9364_p2() {
    exitcond56_fu_9364_p2 = (!p_59_reg_1761.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_59_reg_1761.read() == ap_const_lv3_7);
}

void mnist_fp4::thread_exitcond57_fu_9436_p2() {
    exitcond57_fu_9436_p2 = (!p_56_reg_1827.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_56_reg_1827.read() == ap_const_lv4_9);
}

void mnist_fp4::thread_exitcond58_fu_9568_p2() {
    exitcond58_fu_9568_p2 = (!p_57_reg_1948.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_57_reg_1948.read() == ap_const_lv4_9);
}

void mnist_fp4::thread_exitcond59_fu_9472_p2() {
    exitcond59_fu_9472_p2 = (!p_58_reg_1860.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_58_reg_1860.read() == ap_const_lv3_7);
}

void mnist_fp4::thread_exitcond5_fu_3927_p2() {
    exitcond5_fu_3927_p2 = (!p_9_reg_1139.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_9_reg_1139.read() == ap_const_lv3_4);
}

void mnist_fp4::thread_exitcond60_fu_9010_p2() {
    exitcond60_fu_9010_p2 = (!p_60_reg_1739.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_60_reg_1739.read() == ap_const_lv4_E);
}

void mnist_fp4::thread_exitcond61_fu_9376_p2() {
    exitcond61_fu_9376_p2 = (!p_65_reg_1772.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_65_reg_1772.read() == ap_const_lv3_7);
}

void mnist_fp4::thread_exitcond62_fu_8090_p2() {
    exitcond62_fu_8090_p2 = (!p_61_reg_1683.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_61_reg_1683.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond63_fu_9604_p2() {
    exitcond63_fu_9604_p2 = (!p_62_reg_1981.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_62_reg_1981.read() == ap_const_lv3_7);
}

void mnist_fp4::thread_exitcond64_fu_9580_p2() {
    exitcond64_fu_9580_p2 = (!p_63_reg_1959.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_63_reg_1959.read() == ap_const_lv4_9);
}

void mnist_fp4::thread_exitcond65_fu_9484_p2() {
    exitcond65_fu_9484_p2 = (!p_64_reg_1871.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_64_reg_1871.read() == ap_const_lv4_8);
}

void mnist_fp4::thread_exitcond66_fu_9520_p2() {
    exitcond66_fu_9520_p2 = (!p_68_reg_1904.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_68_reg_1904.read() == ap_const_lv5_10);
}

void mnist_fp4::thread_exitcond67_fu_8143_p2() {
    exitcond67_fu_8143_p2 = (!p_66_reg_1706.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_66_reg_1706.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond68_fu_9616_p2() {
    exitcond68_fu_9616_p2 = (!p_67_reg_1992.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_67_reg_1992.read() == ap_const_lv3_7);
}

void mnist_fp4::thread_exitcond69_fu_9388_p2() {
    exitcond69_fu_9388_p2 = (!p_69_reg_1783.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_69_reg_1783.read() == ap_const_lv2_2);
}

void mnist_fp4::thread_exitcond6_fu_2570_p2() {
    exitcond6_fu_2570_p2 = (!p_5_reg_971.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_5_reg_971.read() == ap_const_lv5_1C);
}

void mnist_fp4::thread_exitcond70_fu_9532_p2() {
    exitcond70_fu_9532_p2 = (!p_70_reg_1915.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_70_reg_1915.read() == ap_const_lv3_7);
}

void mnist_fp4::thread_exitcond71_fu_9400_p2() {
    exitcond71_fu_9400_p2 = (!p_72_reg_1794.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_72_reg_1794.read() == ap_const_lv2_2);
}

void mnist_fp4::thread_exitcond72_fu_9496_p2() {
    exitcond72_fu_9496_p2 = (!p_71_reg_1882.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_71_reg_1882.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond73_fu_9628_p2() {
    exitcond73_fu_9628_p2 = (!p_74_reg_2003.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_74_reg_2003.read() == ap_const_lv5_10);
}

void mnist_fp4::thread_exitcond74_fu_9544_p2() {
    exitcond74_fu_9544_p2 = (!p_73_reg_1926.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_73_reg_1926.read() == ap_const_lv3_7);
}

void mnist_fp4::thread_exitcond75_fu_9508_p2() {
    exitcond75_fu_9508_p2 = (!p_75_reg_1893.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_75_reg_1893.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond76_fu_9640_p2() {
    exitcond76_fu_9640_p2 = (!p_76_reg_2014.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_76_reg_2014.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond77_fu_9652_p2() {
    exitcond77_fu_9652_p2 = (!p_77_reg_2025.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_77_reg_2025.read() == ap_const_lv2_3);
}

void mnist_fp4::thread_exitcond7_fu_3219_p2() {
    exitcond7_fu_3219_p2 = (!p_6_reg_1075.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_6_reg_1075.read() == ap_const_lv3_4);
}

void mnist_fp4::thread_exitcond8_fu_2770_p2() {
    exitcond8_fu_2770_p2 = (!p_7_reg_1042.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_7_reg_1042.read() == ap_const_lv3_4);
}

void mnist_fp4::thread_exitcond9_fu_2621_p2() {
    exitcond9_fu_2621_p2 = (!p_8_reg_983.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_8_reg_983.read() == ap_const_lv5_1C);
}

void mnist_fp4::thread_exitcond_fu_2816_p2() {
    exitcond_fu_2816_p2 = (!p_11_reg_1053.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_11_reg_1053.read() == ap_const_lv5_1C);
}

void mnist_fp4::thread_f_2_fu_5029_p1() {
    f_2_fu_5029_p1 = p_Result_40_fu_5018_p5.read();
}

void mnist_fp4::thread_f_4_fu_5946_p1() {
    f_4_fu_5946_p1 = p_Result_17_fu_5935_p5.read();
}

void mnist_fp4::thread_f_5_fu_7841_p1() {
    f_5_fu_7841_p1 = p_Result_43_fu_7830_p5.read();
}

void mnist_fp4::thread_f_6_fu_8897_p1() {
    f_6_fu_8897_p1 = p_Result_48_fu_8886_p5.read();
}

void mnist_fp4::thread_f_fu_3910_p1() {
    f_fu_3910_p1 = p_Result_35_fu_3899_p5.read();
}

void mnist_fp4::thread_ff1_V_fu_3933_p2() {
    ff1_V_fu_3933_p2 = (!p_9_reg_1139.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_9_reg_1139.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_ff2_V_fu_6499_p2() {
    ff2_V_fu_6499_p2 = (!p_15_reg_1413.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_15_reg_1413.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_ff3_V_fu_7866_p2() {
    ff3_V_fu_7866_p2 = (!p_26_reg_1612.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_26_reg_1612.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_ff4_V_fu_9454_p2() {
    ff4_V_fu_9454_p2 = (!p_42_reg_1838.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_42_reg_1838.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_ff5_V_fu_9598_p2() {
    ff5_V_fu_9598_p2 = (!p_53_reg_1970.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_53_reg_1970.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_ff_V_fu_2512_p2() {
    ff_V_fu_2512_p2 = (!p_2_reg_960.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_2_reg_960.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_grp_fu_2036_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state168.read())) {
        grp_fu_2036_p1 = reducer183_2_reg_1507.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state43.read())) {
        grp_fu_2036_p1 = reducer180_1_reg_1030.read();
    } else {
        grp_fu_2036_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp4::thread_grp_fu_2042_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state164.read())) {
        grp_fu_2042_p0 = pad_temp2_0_load_reg_11050.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state39.read())) {
        grp_fu_2042_p0 = pad_temp_0_0_load_reg_9956.read();
    } else {
        grp_fu_2042_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp4::thread_grp_fu_2042_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state164.read())) {
        grp_fu_2042_p1 = w_conv3_load_reg_11055.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state39.read())) {
        grp_fu_2042_p1 = w_conv1_load_reg_9961.read();
    } else {
        grp_fu_2042_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp4::thread_grp_fu_2046_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state225.read())) {
        grp_fu_2046_p0 = tmp32_V_24_reg_11617.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state206.read())) {
        grp_fu_2046_p0 = tmp32_V_21_reg_11346.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state127.read())) {
        grp_fu_2046_p0 = p_012_0_i2_reg_10803.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state103.read())) {
        grp_fu_2046_p0 = tmp32_V_18_reg_10537.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state83.read())) {
        grp_fu_2046_p0 = tmp32_V_12_reg_10251.read();
    } else {
        grp_fu_2046_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp4::thread_grp_fu_2049_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state237.read())) {
        grp_fu_2049_p0 = conv4_0_load_reg_11681.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state220.read())) {
        grp_fu_2049_p0 = pad_temp3_0_load_reg_11475.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state178.read())) {
        grp_fu_2049_p0 = conv3_0_load_reg_11104.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state115.read())) {
        grp_fu_2049_p0 = conv2_0_load_reg_10601.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state98.read())) {
        grp_fu_2049_p0 = pad_temp1_0_load_reg_10395.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state53.read())) {
        grp_fu_2049_p0 = conv1_0_load_reg_10010.read();
    } else {
        grp_fu_2049_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp4::thread_grp_fu_2052_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state220.read())) {
        grp_fu_2052_p0 = w_conv4_load_reg_11480.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state98.read())) {
        grp_fu_2052_p0 = w_conv2_load_reg_10400.read();
    } else {
        grp_fu_2052_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp4::thread_grp_fu_2055_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state236.read())) {
        grp_fu_2055_p0 = conv4_0_load_reg_11681.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state177.read())) {
        grp_fu_2055_p0 = conv3_0_load_reg_11104.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state134.read())) {
        grp_fu_2055_p0 = p_03_i1_reg_10818.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state114.read())) {
        grp_fu_2055_p0 = conv2_0_load_reg_10601.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state52.read())) {
        grp_fu_2055_p0 = conv1_0_load_reg_10010.read();
    } else {
        grp_fu_2055_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp4::thread_grp_fu_2055_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state134.read())) {
        grp_fu_2055_p1 = tmp_140_reg_1333.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state52.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state114.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state177.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state236.read()))) {
        grp_fu_2055_p1 = ap_const_lv32_0;
    } else {
        grp_fu_2055_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp4::thread_grp_fu_2071_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state12.read())) {
        grp_fu_2071_p0 = p_s_reg_921.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state11.read())) {
        grp_fu_2071_p0 = ap_phi_mux_p_s_phi_fu_925_p4.read();
    } else {
        grp_fu_2071_p0 =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp4::thread_grp_fu_2071_p2() {
    grp_fu_2071_p2 = (!grp_fu_2071_p0.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(grp_fu_2071_p0.read() == ap_const_lv5_1E);
}

void mnist_fp4::thread_grp_fu_2078_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state59.read())) {
        grp_fu_2078_p0 = p_4_reg_1097.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state58.read())) {
        grp_fu_2078_p0 = ap_phi_mux_p_4_phi_fu_1101_p4.read();
    } else {
        grp_fu_2078_p0 =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp4::thread_grp_fu_2078_p2() {
    grp_fu_2078_p2 = (!grp_fu_2078_p0.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(grp_fu_2078_p0.read() == ap_const_lv5_1E);
}

void mnist_fp4::thread_grp_fu_2456_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state14.read())) {
        grp_fu_2456_ap_start = ap_const_logic_1;
    } else {
        grp_fu_2456_ap_start = ap_const_logic_0;
    }
}

void mnist_fp4::thread_grp_fu_2456_p0() {
    grp_fu_2456_p0 =  (sc_lv<12>) (grp_fu_2456_p00.read());
}

void mnist_fp4::thread_grp_fu_2456_p00() {
    grp_fu_2456_p00 = (!tmp_68_reg_9840.read()[0].is_01())? sc_lv<11>(): ((tmp_68_reg_9840.read()[0].to_bool())? neg_ti_fu_2443_p2.read(): tmp_77_fu_2433_p1.read());
}

void mnist_fp4::thread_grp_fu_2456_p1() {
    grp_fu_2456_p1 =  (sc_lv<6>) (ap_const_lv11_1C);
}

void mnist_fp4::thread_grp_fu_3575_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state61.read())) {
        grp_fu_3575_ap_start = ap_const_logic_1;
    } else {
        grp_fu_3575_ap_start = ap_const_logic_0;
    }
}

void mnist_fp4::thread_grp_fu_3575_p0() {
    grp_fu_3575_p0 =  (sc_lv<14>) (grp_fu_3575_p00.read());
}

void mnist_fp4::thread_grp_fu_3575_p00() {
    grp_fu_3575_p00 = (!tmp_256_reg_10165.read()[0].is_01())? sc_lv<13>(): ((tmp_256_reg_10165.read()[0].to_bool())? neg_ti1_fu_3562_p2.read(): tmp_263_fu_3552_p1.read());
}

void mnist_fp4::thread_grp_fu_3575_p1() {
    grp_fu_3575_p1 =  (sc_lv<6>) (ap_const_lv13_1C);
}

void mnist_fp4::thread_grp_fu_6340_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state139.read())) {
        grp_fu_6340_ap_start = ap_const_logic_1;
    } else {
        grp_fu_6340_ap_start = ap_const_logic_0;
    }
}

void mnist_fp4::thread_grp_fu_6340_p0() {
    grp_fu_6340_p0 = (!tmp_405_reg_10903.read()[0].is_01())? sc_lv<11>(): ((tmp_405_reg_10903.read()[0].to_bool())? neg_ti3_fu_6327_p2.read(): tmp_409_fu_6317_p1.read());
}

void mnist_fp4::thread_grp_fu_6340_p1() {
    grp_fu_6340_p1 =  (sc_lv<5>) (ap_const_lv11_E);
}

void mnist_fp4::thread_grp_fu_7506_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state186.read())) {
        grp_fu_7506_ap_start = ap_const_logic_1;
    } else {
        grp_fu_7506_ap_start = ap_const_logic_0;
    }
}

void mnist_fp4::thread_grp_fu_7506_p0() {
    grp_fu_7506_p0 = (!tmp_480_reg_11260.read()[0].is_01())? sc_lv<12>(): ((tmp_480_reg_11260.read()[0].to_bool())? neg_ti5_fu_7493_p2.read(): tmp_484_fu_7483_p1.read());
}

void mnist_fp4::thread_grp_fu_7506_p1() {
    grp_fu_7506_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp4::thread_grp_fu_9688_p2() {
    grp_fu_9688_p2 = esl_concat<4,2>(p_Val2_21_reg_1220.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_grp_fu_9729_p2() {
    grp_fu_9729_p2 = esl_concat<4,2>(p_Val2_37_reg_1694.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_h1_V_fu_9370_p2() {
    h1_V_fu_9370_p2 = (!p_59_reg_1761.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_59_reg_1761.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_h_V_fu_5570_p2() {
    h_V_fu_5570_p2 = (!p_27_reg_1287.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_27_reg_1287.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_i1_V_fu_3355_p2() {
    i1_V_fu_3355_p2 = (!p_14_reg_1109.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_14_reg_1109.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_i3_V_fu_6175_p2() {
    i3_V_fu_6175_p2 = (!p_30_reg_1389.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_30_reg_1389.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_i4_V_fu_7341_p2() {
    i4_V_fu_7341_p2 = (!p_39_reg_1585.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_39_reg_1585.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_i6_V_fu_9442_p2() {
    i6_V_fu_9442_p2 = (!p_56_reg_1827.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_56_reg_1827.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_i7_V_fu_9586_p2() {
    i7_V_fu_9586_p2 = (!p_63_reg_1959.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_63_reg_1959.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_i_V_1_fu_2258_p2() {
    i_V_1_fu_2258_p2 = (!p_3_reg_933.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_3_reg_933.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_i_V_fu_2111_p2() {
    i_V_fu_2111_p2 = (!p_0_reg_804.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_0_reg_804.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_icmp10_fu_8782_p2() {
    icmp10_fu_8782_p2 = (!tmp_529_fu_8772_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_529_fu_8772_p4.read() == ap_const_lv26_0);
}

void mnist_fp4::thread_icmp11_fu_8344_p2() {
    icmp11_fu_8344_p2 = (!tmp_547_fu_8334_p4.read().is_01() || !ap_const_lv10_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_547_fu_8334_p4.read() == ap_const_lv10_0);
}

void mnist_fp4::thread_icmp12_fu_8429_p2() {
    icmp12_fu_8429_p2 = (!tmp_554_fu_8419_p4.read().is_01() || !ap_const_lv10_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_554_fu_8419_p4.read() == ap_const_lv10_0);
}

void mnist_fp4::thread_icmp1_fu_3804_p2() {
    icmp1_fu_3804_p2 = (!tmp_306_fu_3794_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_306_fu_3794_p4.read() == ap_const_lv26_0);
}

void mnist_fp4::thread_icmp2_fu_5329_p2() {
    icmp2_fu_5329_p2 = (!tmp_330_fu_5319_p4.read().is_01() || !ap_const_lv10_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_330_fu_5319_p4.read() == ap_const_lv10_0);
}

void mnist_fp4::thread_icmp3_fu_4914_p2() {
    icmp3_fu_4914_p2 = (!tmp_346_fu_4904_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_346_fu_4904_p4.read() == ap_const_lv26_0);
}

void mnist_fp4::thread_icmp4_fu_4476_p2() {
    icmp4_fu_4476_p2 = (!tmp_392_fu_4466_p4.read().is_01() || !ap_const_lv10_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_392_fu_4466_p4.read() == ap_const_lv10_0);
}

void mnist_fp4::thread_icmp5_fu_4561_p2() {
    icmp5_fu_4561_p2 = (!tmp_399_fu_4551_p4.read().is_01() || !ap_const_lv10_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_399_fu_4551_p4.read() == ap_const_lv10_0);
}

void mnist_fp4::thread_icmp6_fu_5840_p2() {
    icmp6_fu_5840_p2 = (!tmp_437_fu_5830_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_437_fu_5830_p4.read() == ap_const_lv26_0);
}

void mnist_fp4::thread_icmp7_fu_7058_p2() {
    icmp7_fu_7058_p2 = (!tmp_458_fu_7048_p4.read().is_01() || !ap_const_lv10_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_458_fu_7048_p4.read() == ap_const_lv10_0);
}

void mnist_fp4::thread_icmp8_fu_7735_p2() {
    icmp8_fu_7735_p2 = (!tmp_509_fu_7725_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_509_fu_7725_p4.read() == ap_const_lv26_0);
}

void mnist_fp4::thread_icmp9_fu_9197_p2() {
    icmp9_fu_9197_p2 = (!tmp_523_fu_9187_p4.read().is_01() || !ap_const_lv10_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_523_fu_9187_p4.read() == ap_const_lv10_0);
}

void mnist_fp4::thread_icmp_fu_3054_p2() {
    icmp_fu_3054_p2 = (!tmp_149_fu_3044_p4.read().is_01() || !ap_const_lv10_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_149_fu_3044_p4.read() == ap_const_lv10_0);
}

void mnist_fp4::thread_image_0_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state30.read())) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_282_cast_fu_2489_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_97_cast_fu_2187_p1.read());
    } else {
        image_0_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp4::thread_image_0_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state30.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        image_0_0_ce0 = ap_const_logic_1;
    } else {
        image_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_image_0_0_d0() {
    image_0_0_d0 = ap_phi_mux_pixel_data_V_2_phi_fu_878_p4.read();
}

void mnist_fp4::thread_image_0_0_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9772.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0))) {
        image_0_0_we0 = ap_const_logic_1;
    } else {
        image_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_index_tuple1_V_fu_3265_p2() {
    index_tuple1_V_fu_3265_p2 = (!p_4_reg_1097.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_4_reg_1097.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_index_tuple2_V_fu_6087_p2() {
    index_tuple2_V_fu_6087_p2 = (!p_20_reg_1378.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_20_reg_1378.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_index_tuple3_V_fu_7253_p2() {
    index_tuple3_V_fu_7253_p2 = (!p_31_reg_1574.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_31_reg_1574.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_index_tuple4_V_fu_9430_p2() {
    index_tuple4_V_fu_9430_p2 = (!p_45_reg_1816.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_45_reg_1816.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_index_tuple5_V_fu_9574_p2() {
    index_tuple5_V_fu_9574_p2 = (!p_57_reg_1948.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_57_reg_1948.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_index_tuple_V_fu_2192_p2() {
    index_tuple_V_fu_2192_p2 = (!p_s_reg_921.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_s_reg_921.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_ireg_V_10_fu_8193_p1() {
    ireg_V_10_fu_8193_p1 = grp_fu_2049_p1.read();
}

void mnist_fp4::thread_ireg_V_11_fu_8229_p1() {
    ireg_V_11_fu_8229_p1 = grp_fu_2052_p1.read();
}

void mnist_fp4::thread_ireg_V_3_fu_5210_p3() {
    ireg_V_3_fu_5210_p3 = (!tmp_122_fu_5201_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_122_fu_5201_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_135_fu_5206_p1.read());
}

void mnist_fp4::thread_ireg_V_4_fu_6939_p3() {
    ireg_V_4_fu_6939_p3 = (!tmp_240_fu_6930_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_240_fu_6930_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_243_fu_6935_p1.read());
}

void mnist_fp4::thread_ireg_V_7_fu_9078_p3() {
    ireg_V_7_fu_9078_p3 = (!tmp_274_fu_9069_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_274_fu_9069_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_280_fu_9074_p1.read());
}

void mnist_fp4::thread_ireg_V_8_fu_4325_p1() {
    ireg_V_8_fu_4325_p1 = grp_fu_2049_p1.read();
}

void mnist_fp4::thread_ireg_V_9_fu_4361_p1() {
    ireg_V_9_fu_4361_p1 = grp_fu_2052_p1.read();
}

void mnist_fp4::thread_ireg_V_fu_2935_p3() {
    ireg_V_fu_2935_p3 = (!tmp_28_fu_2926_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_28_fu_2926_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_31_fu_2931_p1.read());
}

void mnist_fp4::thread_is_neg_1_fu_4117_p3() {
    is_neg_1_fu_4117_p3 = p_Val2_3_reg_1174.read().range(3, 3);
}

void mnist_fp4::thread_is_neg_2_fu_7652_p3() {
    is_neg_2_fu_7652_p3 = p_Val2_25_reg_11309.read().range(3, 3);
}

void mnist_fp4::thread_is_neg_3_fu_8024_p3() {
    is_neg_3_fu_8024_p3 = p_Val2_1_reg_1647.read().range(3, 3);
}

void mnist_fp4::thread_is_neg_fu_3721_p3() {
    is_neg_fu_3721_p3 = p_Val2_s_reg_10214.read().range(3, 3);
}

void mnist_fp4::thread_j_V_fu_2153_p2() {
    j_V_fu_2153_p2 = (!ap_phi_mux_p_1_phi_fu_819_p4.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(ap_phi_mux_p_1_phi_fu_819_p4.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_lhs_V_10_cast_fu_3479_p1() {
    lhs_V_10_cast_fu_3479_p1 = esl_sext<7,6>(lhs_V_2_fu_3473_p2.read());
}

void mnist_fp4::thread_lhs_V_18_cast_fu_6244_p1() {
    lhs_V_18_cast_fu_6244_p1 = esl_sext<6,5>(lhs_V_8_fu_6238_p2.read());
}

void mnist_fp4::thread_lhs_V_1_cast_fu_6105_p1() {
    lhs_V_1_cast_fu_6105_p1 = esl_zext<8,5>(p_20_reg_1378.read());
}

void mnist_fp4::thread_lhs_V_24_cast_fu_7410_p1() {
    lhs_V_24_cast_fu_7410_p1 = esl_sext<6,5>(lhs_V_4_fu_7404_p2.read());
}

void mnist_fp4::thread_lhs_V_2_fu_3473_p2() {
    lhs_V_2_fu_3473_p2 = (!tmp_45_cast_fu_3465_p1.read().is_01() || !tmp_46_cast_fu_3469_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_45_cast_fu_3465_p1.read()) - sc_biguint<6>(tmp_46_cast_fu_3469_p1.read()));
}

void mnist_fp4::thread_lhs_V_3_cast_fu_7271_p1() {
    lhs_V_3_cast_fu_7271_p1 = esl_zext<9,5>(p_31_reg_1574.read());
}

void mnist_fp4::thread_lhs_V_4_fu_7404_p2() {
    lhs_V_4_fu_7404_p2 = (!p_39_reg_1585.read().is_01() || !tmp_151_cast_fu_7400_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_39_reg_1585.read()) - sc_biguint<5>(tmp_151_cast_fu_7400_p1.read()));
}

void mnist_fp4::thread_lhs_V_5_cast_fu_5576_p1() {
    lhs_V_5_cast_fu_5576_p1 = esl_zext<9,4>(p_27_reg_1287.read());
}

void mnist_fp4::thread_lhs_V_7_cast_fu_5635_p1() {
    lhs_V_7_cast_fu_5635_p1 = esl_zext<11,4>(p_33_reg_1298.read());
}

void mnist_fp4::thread_lhs_V_8_fu_6238_p2() {
    lhs_V_8_fu_6238_p2 = (!p_30_reg_1389.read().is_01() || !tmp_90_cast_fu_6234_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_30_reg_1389.read()) - sc_biguint<5>(tmp_90_cast_fu_6234_p1.read()));
}

void mnist_fp4::thread_lhs_V_cast_39_fu_3271_p1() {
    lhs_V_cast_39_fu_3271_p1 = esl_zext<10,5>(p_4_reg_1097.read());
}

void mnist_fp4::thread_lhs_V_cast_fu_2382_p1() {
    lhs_V_cast_fu_2382_p1 = esl_sext<11,6>(lhs_V_s_fu_2376_p2.read());
}

void mnist_fp4::thread_lhs_V_s_fu_2376_p2() {
    lhs_V_s_fu_2376_p2 = (!tmp_24_cast_fu_2368_p1.read().is_01() || !tmp_28_cast_fu_2372_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_24_cast_fu_2368_p1.read()) - sc_biguint<6>(tmp_28_cast_fu_2372_p1.read()));
}

void mnist_fp4::thread_man_V_11_fu_8279_p2() {
    man_V_11_fu_8279_p2 = (!ap_const_lv54_0.is_01() || !p_Result_44_fu_8275_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_44_fu_8275_p1.read()));
}

void mnist_fp4::thread_man_V_12_fu_8285_p3() {
    man_V_12_fu_8285_p3 = (!isneg_2_reg_11485.read()[0].is_01())? sc_lv<54>(): ((isneg_2_reg_11485.read()[0].to_bool())? man_V_11_fu_8279_p2.read(): p_Result_44_fu_8275_p1.read());
}

void mnist_fp4::thread_man_V_13_fu_9132_p2() {
    man_V_13_fu_9132_p2 = (!ap_const_lv54_0.is_01() || !p_Result_25_fu_9128_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_25_fu_9128_p1.read()));
}

void mnist_fp4::thread_man_V_14_fu_8364_p2() {
    man_V_14_fu_8364_p2 = (!ap_const_lv54_0.is_01() || !p_Result_45_fu_8360_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_45_fu_8360_p1.read()));
}

void mnist_fp4::thread_man_V_15_fu_8370_p3() {
    man_V_15_fu_8370_p3 = (!isneg_3_reg_11507.read()[0].is_01())? sc_lv<54>(): ((isneg_3_reg_11507.read()[0].to_bool())? man_V_14_fu_8364_p2.read(): p_Result_45_fu_8360_p1.read());
}

void mnist_fp4::thread_man_V_16_fu_9138_p3() {
    man_V_16_fu_9138_p3 = (!tmp_520_reg_11703.read()[0].is_01())? sc_lv<54>(): ((tmp_520_reg_11703.read()[0].to_bool())? man_V_13_fu_9132_p2.read(): p_Result_25_fu_9128_p1.read());
}

void mnist_fp4::thread_man_V_1_fu_2989_p2() {
    man_V_1_fu_2989_p2 = (!ap_const_lv54_0.is_01() || !p_Result_4_fu_2985_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_4_fu_2985_p1.read()));
}

void mnist_fp4::thread_man_V_2_fu_2995_p3() {
    man_V_2_fu_2995_p3 = (!tmp_142_reg_10032.read()[0].is_01())? sc_lv<54>(): ((tmp_142_reg_10032.read()[0].to_bool())? man_V_1_fu_2989_p2.read(): p_Result_4_fu_2985_p1.read());
}

void mnist_fp4::thread_man_V_3_fu_4411_p2() {
    man_V_3_fu_4411_p2 = (!ap_const_lv54_0.is_01() || !p_Result_36_fu_4407_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_36_fu_4407_p1.read()));
}

void mnist_fp4::thread_man_V_4_fu_4417_p3() {
    man_V_4_fu_4417_p3 = (!isneg_reg_10405.read()[0].is_01())? sc_lv<54>(): ((isneg_reg_10405.read()[0].to_bool())? man_V_3_fu_4411_p2.read(): p_Result_36_fu_4407_p1.read());
}

void mnist_fp4::thread_man_V_5_fu_5264_p2() {
    man_V_5_fu_5264_p2 = (!ap_const_lv54_0.is_01() || !p_Result_7_fu_5260_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_7_fu_5260_p1.read()));
}

void mnist_fp4::thread_man_V_6_fu_5270_p3() {
    man_V_6_fu_5270_p3 = (!tmp_327_reg_10623.read()[0].is_01())? sc_lv<54>(): ((tmp_327_reg_10623.read()[0].to_bool())? man_V_5_fu_5264_p2.read(): p_Result_7_fu_5260_p1.read());
}

void mnist_fp4::thread_man_V_7_fu_6999_p3() {
    man_V_7_fu_6999_p3 = (!tmp_455_reg_11126.read()[0].is_01())? sc_lv<54>(): ((tmp_455_reg_11126.read()[0].to_bool())? man_V_s_fu_6993_p2.read(): p_Result_19_fu_6989_p1.read());
}

void mnist_fp4::thread_man_V_8_fu_4496_p2() {
    man_V_8_fu_4496_p2 = (!ap_const_lv54_0.is_01() || !p_Result_37_fu_4492_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_37_fu_4492_p1.read()));
}

void mnist_fp4::thread_man_V_9_fu_4502_p3() {
    man_V_9_fu_4502_p3 = (!isneg_1_reg_10427.read()[0].is_01())? sc_lv<54>(): ((isneg_1_reg_10427.read()[0].to_bool())? man_V_8_fu_4496_p2.read(): p_Result_37_fu_4492_p1.read());
}

void mnist_fp4::thread_man_V_s_fu_6993_p2() {
    man_V_s_fu_6993_p2 = (!ap_const_lv54_0.is_01() || !p_Result_19_fu_6989_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_19_fu_6989_p1.read()));
}

void mnist_fp4::thread_msb_idx_1_cast_fu_3790_p1() {
    msb_idx_1_cast_fu_3790_p1 = esl_zext<32,31>(msb_idx_1_fu_3784_p3.read());
}

void mnist_fp4::thread_msb_idx_1_fu_3784_p3() {
    msb_idx_1_fu_3784_p3 = (!tmp_305_reg_10246.read()[0].is_01())? sc_lv<31>(): ((tmp_305_reg_10246.read()[0].to_bool())? ap_const_lv31_0: tmp_302_reg_10241.read());
}

void mnist_fp4::thread_msb_idx_2_fu_4165_p2() {
    msb_idx_2_fu_4165_p2 = (!ap_const_lv32_3.is_01() || !num_zeros_1_fu_4157_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_3) - sc_biguint<32>(num_zeros_1_fu_4157_p3.read()));
}

void mnist_fp4::thread_msb_idx_3_cast_fu_4900_p1() {
    msb_idx_3_cast_fu_4900_p1 = esl_zext<32,31>(msb_idx_3_fu_4894_p3.read());
}

void mnist_fp4::thread_msb_idx_3_fu_4894_p3() {
    msb_idx_3_fu_4894_p3 = (!tmp_343_reg_10354.read()[0].is_01())? sc_lv<31>(): ((tmp_343_reg_10354.read()[0].to_bool())? ap_const_lv31_0: tmp_341_reg_10349.read());
}

void mnist_fp4::thread_msb_idx_4_fu_5802_p2() {
    msb_idx_4_fu_5802_p2 = (!ap_const_lv32_3.is_01() || !num_zeros_2_fu_5794_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_3) - sc_biguint<32>(num_zeros_2_fu_5794_p3.read()));
}

void mnist_fp4::thread_msb_idx_5_cast_fu_5826_p1() {
    msb_idx_5_cast_fu_5826_p1 = esl_zext<32,31>(msb_idx_5_fu_5820_p3.read());
}

void mnist_fp4::thread_msb_idx_5_fu_5820_p3() {
    msb_idx_5_fu_5820_p3 = (!tmp_436_reg_10798.read()[0].is_01())? sc_lv<31>(): ((tmp_436_reg_10798.read()[0].to_bool())? ap_const_lv31_0: tmp_435_reg_10793.read());
}

void mnist_fp4::thread_msb_idx_6_fu_7697_p2() {
    msb_idx_6_fu_7697_p2 = (!ap_const_lv32_3.is_01() || !num_zeros_3_fu_7689_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_3) - sc_biguint<32>(num_zeros_3_fu_7689_p3.read()));
}

void mnist_fp4::thread_msb_idx_7_cast_fu_7721_p1() {
    msb_idx_7_cast_fu_7721_p1 = esl_zext<32,31>(msb_idx_7_fu_7715_p3.read());
}

void mnist_fp4::thread_msb_idx_7_fu_7715_p3() {
    msb_idx_7_fu_7715_p3 = (!tmp_508_reg_11341.read()[0].is_01())? sc_lv<31>(): ((tmp_508_reg_11341.read()[0].to_bool())? ap_const_lv31_0: tmp_507_reg_11336.read());
}

void mnist_fp4::thread_msb_idx_8_fu_8072_p2() {
    msb_idx_8_fu_8072_p2 = (!ap_const_lv32_3.is_01() || !num_zeros_4_fu_8064_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_3) - sc_biguint<32>(num_zeros_4_fu_8064_p3.read()));
}

void mnist_fp4::thread_msb_idx_9_cast_fu_8768_p1() {
    msb_idx_9_cast_fu_8768_p1 = esl_zext<32,31>(msb_idx_9_fu_8762_p3.read());
}

void mnist_fp4::thread_msb_idx_9_fu_8762_p3() {
    msb_idx_9_fu_8762_p3 = (!tmp_528_reg_11434.read()[0].is_01())? sc_lv<31>(): ((tmp_528_reg_11434.read()[0].to_bool())? ap_const_lv31_0: tmp_527_reg_11429.read());
}

void mnist_fp4::thread_msb_idx_fu_3766_p2() {
    msb_idx_fu_3766_p2 = (!ap_const_lv32_3.is_01() || !num_zeros_fu_3758_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_3) - sc_biguint<32>(num_zeros_fu_3758_p3.read()));
}

void mnist_fp4::thread_mul1_fu_9672_p0() {
    mul1_fu_9672_p0 =  (sc_lv<15>) (ap_const_lv28_2493);
}

void mnist_fp4::thread_mul1_fu_9672_p1() {
    mul1_fu_9672_p1 =  (sc_lv<13>) (sext1_cast_fu_3497_p1.read());
}

void mnist_fp4::thread_mul2_fu_9680_p0() {
    mul2_fu_9680_p0 =  (sc_lv<15>) (ap_const_lv28_29CC);
}

void mnist_fp4::thread_mul2_fu_9680_p1() {
    mul2_fu_9680_p1 =  (sc_lv<13>) (sext1_cast_fu_3497_p1.read());
}

void mnist_fp4::thread_mul3_fu_9697_p0() {
    mul3_fu_9697_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp4::thread_mul3_fu_9697_p1() {
    mul3_fu_9697_p1 =  (sc_lv<11>) (sext3_cast_fu_6262_p1.read());
}

void mnist_fp4::thread_mul4_fu_9705_p0() {
    mul4_fu_9705_p0 =  (sc_lv<13>) (ap_const_lv24_A73);
}

void mnist_fp4::thread_mul4_fu_9705_p1() {
    mul4_fu_9705_p1 =  (sc_lv<11>) (sext3_cast_fu_6262_p1.read());
}

void mnist_fp4::thread_mul5_fu_9713_p0() {
    mul5_fu_9713_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp4::thread_mul5_fu_9713_p1() {
    mul5_fu_9713_p1 =  (sc_lv<12>) (sext5_cast_fu_7428_p1.read());
}

void mnist_fp4::thread_mul6_fu_9721_p0() {
    mul6_fu_9721_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp4::thread_mul6_fu_9721_p1() {
    mul6_fu_9721_p1 =  (sc_lv<12>) (sext5_cast_fu_7428_p1.read());
}

void mnist_fp4::thread_mul_fu_9664_p0() {
    mul_fu_9664_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp4::thread_neg_mul1_fu_3533_p2() {
    neg_mul1_fu_3533_p2 = (!ap_const_lv26_0.is_01() || !tmp_255_reg_10160.read().is_01())? sc_lv<26>(): (sc_biguint<26>(ap_const_lv26_0) - sc_biguint<26>(tmp_255_reg_10160.read()));
}

void mnist_fp4::thread_neg_mul2_fu_3581_p2() {
    neg_mul2_fu_3581_p2 = (!ap_const_lv27_0.is_01() || !tmp_266_reg_10178.read().is_01())? sc_lv<27>(): (sc_biguint<27>(ap_const_lv27_0) - sc_biguint<27>(tmp_266_reg_10178.read()));
}

void mnist_fp4::thread_neg_mul3_fu_6298_p2() {
    neg_mul3_fu_6298_p2 = (!ap_const_lv22_0.is_01() || !tmp_404_reg_10898.read().is_01())? sc_lv<22>(): (sc_biguint<22>(ap_const_lv22_0) - sc_biguint<22>(tmp_404_reg_10898.read()));
}

void mnist_fp4::thread_neg_mul4_fu_6346_p2() {
    neg_mul4_fu_6346_p2 = (!ap_const_lv23_0.is_01() || !tmp_412_reg_10916.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_412_reg_10916.read()));
}

void mnist_fp4::thread_neg_mul5_fu_7464_p2() {
    neg_mul5_fu_7464_p2 = (!ap_const_lv24_0.is_01() || !tmp_479_reg_11255.read().is_01())? sc_lv<24>(): (sc_biguint<24>(ap_const_lv24_0) - sc_biguint<24>(tmp_479_reg_11255.read()));
}

void mnist_fp4::thread_neg_mul6_fu_7512_p2() {
    neg_mul6_fu_7512_p2 = (!ap_const_lv25_0.is_01() || !tmp_487_reg_11273.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_487_reg_11273.read()));
}

void mnist_fp4::thread_neg_mul_fu_2414_p2() {
    neg_mul_fu_2414_p2 = (!ap_const_lv23_0.is_01() || !tmp_66_reg_9846.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_66_reg_9846.read()));
}

void mnist_fp4::thread_neg_ti1_fu_3562_p2() {
    neg_ti1_fu_3562_p2 = (!ap_const_lv13_0.is_01() || !tmp_264_fu_3555_p3.read().is_01())? sc_lv<13>(): (sc_biguint<13>(ap_const_lv13_0) - sc_biguint<13>(tmp_264_fu_3555_p3.read()));
}

void mnist_fp4::thread_neg_ti2_fu_3614_p2() {
    neg_ti2_fu_3614_p2 = (!ap_const_lv2_0.is_01() || !tmp_276_fu_3610_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_276_fu_3610_p1.read()));
}

void mnist_fp4::thread_neg_ti3_fu_6327_p2() {
    neg_ti3_fu_6327_p2 = (!ap_const_lv11_0.is_01() || !tmp_410_fu_6320_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_410_fu_6320_p3.read()));
}

void mnist_fp4::thread_neg_ti4_fu_6379_p2() {
    neg_ti4_fu_6379_p2 = (!ap_const_lv2_0.is_01() || !tmp_417_fu_6375_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_417_fu_6375_p1.read()));
}

void mnist_fp4::thread_neg_ti5_fu_7493_p2() {
    neg_ti5_fu_7493_p2 = (!ap_const_lv12_0.is_01() || !tmp_485_fu_7486_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_485_fu_7486_p3.read()));
}

void mnist_fp4::thread_neg_ti6_fu_7545_p2() {
    neg_ti6_fu_7545_p2 = (!ap_const_lv3_0.is_01() || !tmp_492_fu_7541_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_492_fu_7541_p1.read()));
}

void mnist_fp4::thread_neg_ti_fu_2443_p2() {
    neg_ti_fu_2443_p2 = (!ap_const_lv11_0.is_01() || !tmp_79_fu_2436_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_79_fu_2436_p3.read()));
}

void mnist_fp4::thread_newSel10_fu_4694_p3() {
    newSel10_fu_4694_p3 = (!or_cond6_fu_4675_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond6_fu_4675_p2.read()[0].to_bool())? newSel8_fu_4667_p3.read(): newSel9_fu_4681_p3.read());
}

void mnist_fp4::thread_newSel11_fu_4816_p3() {
    newSel11_fu_4816_p3 = (!sel_tmp35_fu_4811_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp35_fu_4811_p2.read()[0].to_bool())? tmp_401_fu_4753_p1.read(): tmp_400_fu_4733_p1.read());
}

void mnist_fp4::thread_newSel12_fu_4830_p3() {
    newSel12_fu_4830_p3 = (!sel_tmp32_fu_4788_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp32_fu_4788_p2.read()[0].to_bool())? storemerge6_fu_4737_p3.read(): tmp_398_reg_10506.read());
}

void mnist_fp4::thread_newSel13_fu_4843_p3() {
    newSel13_fu_4843_p3 = (!or_cond9_fu_4824_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond9_fu_4824_p2.read()[0].to_bool())? newSel11_fu_4816_p3.read(): newSel12_fu_4830_p3.read());
}

void mnist_fp4::thread_newSel14_fu_7164_p3() {
    newSel14_fu_7164_p3 = (!sel_tmp44_fu_7159_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp44_fu_7159_p2.read()[0].to_bool())? tmp_460_fu_7101_p1.read(): tmp_459_fu_7081_p1.read());
}

void mnist_fp4::thread_newSel15_fu_7178_p3() {
    newSel15_fu_7178_p3 = (!sel_tmp41_fu_7136_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp41_fu_7136_p2.read()[0].to_bool())? storemerge8_fu_7085_p3.read(): tmp_457_reg_11171.read());
}

void mnist_fp4::thread_newSel16_fu_7191_p3() {
    newSel16_fu_7191_p3 = (!or_cond12_fu_7172_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond12_fu_7172_p2.read()[0].to_bool())? newSel14_fu_7164_p3.read(): newSel15_fu_7178_p3.read());
}

void mnist_fp4::thread_newSel17_fu_7205_p3() {
    newSel17_fu_7205_p3 = (!or_cond14_fu_7199_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond14_fu_7199_p2.read()[0].to_bool())? newSel16_fu_7191_p3.read(): ap_const_lv4_0);
}

void mnist_fp4::thread_newSel18_fu_9303_p3() {
    newSel18_fu_9303_p3 = (!sel_tmp53_fu_9298_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp53_fu_9298_p2.read()[0].to_bool())? tmp_525_fu_9240_p1.read(): tmp_524_fu_9220_p1.read());
}

void mnist_fp4::thread_newSel19_fu_9317_p3() {
    newSel19_fu_9317_p3 = (!sel_tmp50_fu_9275_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp50_fu_9275_p2.read()[0].to_bool())? storemerge5_fu_9224_p3.read(): tmp_522_reg_11748.read());
}

void mnist_fp4::thread_newSel1_fu_3174_p3() {
    newSel1_fu_3174_p3 = (!sel_tmp9_fu_3132_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp9_fu_3132_p2.read()[0].to_bool())? storemerge_fu_3081_p3.read(): tmp_148_reg_10077.read());
}

void mnist_fp4::thread_newSel20_fu_9330_p3() {
    newSel20_fu_9330_p3 = (!or_cond15_fu_9311_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond15_fu_9311_p2.read()[0].to_bool())? newSel18_fu_9303_p3.read(): newSel19_fu_9317_p3.read());
}

void mnist_fp4::thread_newSel21_fu_9344_p3() {
    newSel21_fu_9344_p3 = (!or_cond17_fu_9338_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond17_fu_9338_p2.read()[0].to_bool())? newSel20_fu_9330_p3.read(): ap_const_lv4_0);
}

void mnist_fp4::thread_newSel22_fu_8535_p3() {
    newSel22_fu_8535_p3 = (!sel_tmp62_fu_8530_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp62_fu_8530_p2.read()[0].to_bool())? tmp_549_fu_8472_p1.read(): tmp_548_fu_8452_p1.read());
}

void mnist_fp4::thread_newSel23_fu_8549_p3() {
    newSel23_fu_8549_p3 = (!sel_tmp59_fu_8507_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp59_fu_8507_p2.read()[0].to_bool())? storemerge1_fu_8456_p3.read(): tmp_546_reg_11552.read());
}

void mnist_fp4::thread_newSel24_fu_8562_p3() {
    newSel24_fu_8562_p3 = (!or_cond18_fu_8543_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond18_fu_8543_p2.read()[0].to_bool())? newSel22_fu_8535_p3.read(): newSel23_fu_8549_p3.read());
}

void mnist_fp4::thread_newSel25_fu_8684_p3() {
    newSel25_fu_8684_p3 = (!sel_tmp71_fu_8679_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp71_fu_8679_p2.read()[0].to_bool())? tmp_556_fu_8621_p1.read(): tmp_555_fu_8601_p1.read());
}

void mnist_fp4::thread_newSel26_fu_8698_p3() {
    newSel26_fu_8698_p3 = (!sel_tmp68_fu_8656_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp68_fu_8656_p2.read()[0].to_bool())? storemerge3_fu_8605_p3.read(): tmp_553_reg_11586.read());
}

void mnist_fp4::thread_newSel27_fu_8711_p3() {
    newSel27_fu_8711_p3 = (!or_cond21_fu_8692_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond21_fu_8692_p2.read()[0].to_bool())? newSel25_fu_8684_p3.read(): newSel26_fu_8698_p3.read());
}

void mnist_fp4::thread_newSel2_fu_3187_p3() {
    newSel2_fu_3187_p3 = (!or_cond_fu_3168_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond_fu_3168_p2.read()[0].to_bool())? newSel_fu_3160_p3.read(): newSel1_fu_3174_p3.read());
}

void mnist_fp4::thread_newSel3_fu_3201_p3() {
    newSel3_fu_3201_p3 = (!or_cond2_fu_3195_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond2_fu_3195_p2.read()[0].to_bool())? newSel2_fu_3187_p3.read(): ap_const_lv4_0);
}

void mnist_fp4::thread_newSel4_fu_5435_p3() {
    newSel4_fu_5435_p3 = (!sel_tmp17_fu_5430_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp17_fu_5430_p2.read()[0].to_bool())? tmp_332_fu_5372_p1.read(): tmp_331_fu_5352_p1.read());
}

void mnist_fp4::thread_newSel5_fu_5449_p3() {
    newSel5_fu_5449_p3 = (!sel_tmp14_fu_5407_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp14_fu_5407_p2.read()[0].to_bool())? storemerge2_fu_5356_p3.read(): tmp_329_reg_10668.read());
}

void mnist_fp4::thread_newSel6_fu_5462_p3() {
    newSel6_fu_5462_p3 = (!or_cond3_fu_5443_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond3_fu_5443_p2.read()[0].to_bool())? newSel4_fu_5435_p3.read(): newSel5_fu_5449_p3.read());
}

void mnist_fp4::thread_newSel7_fu_5476_p3() {
    newSel7_fu_5476_p3 = (!or_cond5_fu_5470_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond5_fu_5470_p2.read()[0].to_bool())? newSel6_fu_5462_p3.read(): ap_const_lv4_0);
}

void mnist_fp4::thread_newSel8_fu_4667_p3() {
    newSel8_fu_4667_p3 = (!sel_tmp26_fu_4662_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp26_fu_4662_p2.read()[0].to_bool())? tmp_394_fu_4604_p1.read(): tmp_393_fu_4584_p1.read());
}

void mnist_fp4::thread_newSel9_fu_4681_p3() {
    newSel9_fu_4681_p3 = (!sel_tmp23_fu_4639_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp23_fu_4639_p2.read()[0].to_bool())? storemerge4_fu_4588_p3.read(): tmp_391_reg_10472.read());
}

void mnist_fp4::thread_newSel_fu_3160_p3() {
    newSel_fu_3160_p3 = (!sel_tmp4_fu_3155_p2.read()[0].is_01())? sc_lv<4>(): ((sel_tmp4_fu_3155_p2.read()[0].to_bool())? tmp_156_fu_3097_p1.read(): tmp_150_fu_3077_p1.read());
}

void mnist_fp4::thread_next_mul1_fu_6051_p2() {
    next_mul1_fu_6051_p2 = (!phi_mul1_reg_1367.read().is_01() || !ap_const_lv10_C4.is_01())? sc_lv<10>(): (sc_biguint<10>(phi_mul1_reg_1367.read()) + sc_biguint<10>(ap_const_lv10_C4));
}

void mnist_fp4::thread_next_mul2_fu_7217_p2() {
    next_mul2_fu_7217_p2 = (!phi_mul2_reg_1563.read().is_01() || !ap_const_lv11_C4.is_01())? sc_lv<11>(): (sc_biguint<11>(phi_mul2_reg_1563.read()) + sc_biguint<11>(ap_const_lv11_C4));
}

void mnist_fp4::thread_next_mul_fu_3213_p2() {
    next_mul_fu_3213_p2 = (!phi_mul_reg_1086.read().is_01() || !ap_const_lv12_310.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul_reg_1086.read()) + sc_biguint<12>(ap_const_lv12_310));
}

void mnist_fp4::thread_not_zero1_V_fu_6063_p2() {
    not_zero1_V_fu_6063_p2 = (!p_13_reg_1356.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_13_reg_1356.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_not_zero2_V_fu_7229_p2() {
    not_zero2_V_fu_7229_p2 = (!p_21_reg_1552.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_21_reg_1552.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_not_zero3_V_fu_9418_p2() {
    not_zero3_V_fu_9418_p2 = (!p_37_reg_1805.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_37_reg_1805.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_not_zero4_V_fu_9562_p2() {
    not_zero4_V_fu_9562_p2 = (!p_48_reg_1937.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_48_reg_1937.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_not_zero_V_fu_3225_p2() {
    not_zero_V_fu_3225_p2 = (!p_6_reg_1075.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_6_reg_1075.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_notlhs1_fu_5185_p2() {
    notlhs1_fu_5185_p2 = (!tmp_100_fu_5171_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_100_fu_5171_p4.read() != ap_const_lv8_FF);
}

void mnist_fp4::thread_notlhs2_fu_5992_p2() {
    notlhs2_fu_5992_p2 = (!tmp_211_fu_5960_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_211_fu_5960_p4.read() != ap_const_lv8_FF);
}

void mnist_fp4::thread_notlhs3_fu_6010_p2() {
    notlhs3_fu_6010_p2 = (!tmp_217_fu_5978_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_217_fu_5978_p4.read() != ap_const_lv8_FF);
}

void mnist_fp4::thread_notlhs4_fu_6914_p2() {
    notlhs4_fu_6914_p2 = (!tmp_225_fu_6900_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_225_fu_6900_p4.read() != ap_const_lv8_FF);
}

void mnist_fp4::thread_notlhs5_fu_9053_p2() {
    notlhs5_fu_9053_p2 = (!tmp_262_fu_9039_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_262_fu_9039_p4.read() != ap_const_lv8_FF);
}

void mnist_fp4::thread_notlhs_fu_2910_p2() {
    notlhs_fu_2910_p2 = (!tmp_21_fu_2896_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_21_fu_2896_p4.read() != ap_const_lv8_FF);
}

void mnist_fp4::thread_notrhs1_fu_5191_p2() {
    notrhs1_fu_5191_p2 = (!tmp_325_fu_5181_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_325_fu_5181_p1.read() == ap_const_lv23_0);
}

void mnist_fp4::thread_notrhs2_fu_5998_p2() {
    notrhs2_fu_5998_p2 = (!tmp_443_fu_5970_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_443_fu_5970_p1.read() == ap_const_lv23_0);
}

void mnist_fp4::thread_notrhs3_fu_6016_p2() {
    notrhs3_fu_6016_p2 = (!tmp_444_fu_5988_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_444_fu_5988_p1.read() == ap_const_lv23_0);
}

void mnist_fp4::thread_notrhs4_fu_6920_p2() {
    notrhs4_fu_6920_p2 = (!tmp_453_fu_6910_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_453_fu_6910_p1.read() == ap_const_lv23_0);
}

void mnist_fp4::thread_notrhs5_fu_9059_p2() {
    notrhs5_fu_9059_p2 = (!tmp_518_fu_9049_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_518_fu_9049_p1.read() == ap_const_lv23_0);
}

void mnist_fp4::thread_notrhs_fu_2916_p2() {
    notrhs_fu_2916_p2 = (!tmp_138_fu_2906_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_138_fu_2906_p1.read() == ap_const_lv23_0);
}

void mnist_fp4::thread_num_zeros_1_fu_4157_p3() {
    num_zeros_1_fu_4157_p3 = esl_cttz<32,32>(p_Result_39_fu_4149_p3.read());
}

void mnist_fp4::thread_num_zeros_2_fu_5794_p3() {
    num_zeros_2_fu_5794_p3 = esl_cttz<32,32>(p_Result_15_fu_5786_p3.read());
}

void mnist_fp4::thread_num_zeros_3_fu_7689_p3() {
    num_zeros_3_fu_7689_p3 = esl_cttz<32,32>(p_Result_42_fu_7681_p3.read());
}

void mnist_fp4::thread_num_zeros_4_fu_8064_p3() {
    num_zeros_4_fu_8064_p3 = esl_cttz<32,32>(p_Result_47_fu_8056_p3.read());
}

void mnist_fp4::thread_num_zeros_fu_3758_p3() {
    num_zeros_fu_3758_p3 = esl_cttz<32,32>(p_Result_34_fu_3750_p3.read());
}

void mnist_fp4::thread_or_cond10_fu_4837_p2() {
    or_cond10_fu_4837_p2 = (sel_tmp32_fu_4788_p2.read() | sel_tmp28_fu_4762_p2.read());
}

void mnist_fp4::thread_or_cond11_fu_4851_p2() {
    or_cond11_fu_4851_p2 = (or_cond9_fu_4824_p2.read() | or_cond10_fu_4837_p2.read());
}

void mnist_fp4::thread_or_cond12_fu_7172_p2() {
    or_cond12_fu_7172_p2 = (sel_tmp44_fu_7159_p2.read() | sel_tmp42_fu_7142_p2.read());
}

void mnist_fp4::thread_or_cond13_fu_7185_p2() {
    or_cond13_fu_7185_p2 = (sel_tmp41_fu_7136_p2.read() | sel_tmp37_fu_7110_p2.read());
}

void mnist_fp4::thread_or_cond14_fu_7199_p2() {
    or_cond14_fu_7199_p2 = (or_cond12_fu_7172_p2.read() | or_cond13_fu_7185_p2.read());
}

void mnist_fp4::thread_or_cond15_fu_9311_p2() {
    or_cond15_fu_9311_p2 = (sel_tmp53_fu_9298_p2.read() | sel_tmp51_fu_9281_p2.read());
}

void mnist_fp4::thread_or_cond16_fu_9324_p2() {
    or_cond16_fu_9324_p2 = (sel_tmp50_fu_9275_p2.read() | sel_tmp46_fu_9249_p2.read());
}

void mnist_fp4::thread_or_cond17_fu_9338_p2() {
    or_cond17_fu_9338_p2 = (or_cond15_fu_9311_p2.read() | or_cond16_fu_9324_p2.read());
}

void mnist_fp4::thread_or_cond18_fu_8543_p2() {
    or_cond18_fu_8543_p2 = (sel_tmp62_fu_8530_p2.read() | sel_tmp60_fu_8513_p2.read());
}

void mnist_fp4::thread_or_cond19_fu_8556_p2() {
    or_cond19_fu_8556_p2 = (sel_tmp59_fu_8507_p2.read() | sel_tmp55_fu_8481_p2.read());
}

void mnist_fp4::thread_or_cond1_fu_3181_p2() {
    or_cond1_fu_3181_p2 = (sel_tmp9_fu_3132_p2.read() | sel_tmp2_fu_3106_p2.read());
}

void mnist_fp4::thread_or_cond20_fu_8570_p2() {
    or_cond20_fu_8570_p2 = (or_cond18_fu_8543_p2.read() | or_cond19_fu_8556_p2.read());
}

void mnist_fp4::thread_or_cond21_fu_8692_p2() {
    or_cond21_fu_8692_p2 = (sel_tmp71_fu_8679_p2.read() | sel_tmp69_fu_8662_p2.read());
}

void mnist_fp4::thread_or_cond22_fu_8705_p2() {
    or_cond22_fu_8705_p2 = (sel_tmp68_fu_8656_p2.read() | sel_tmp64_fu_8630_p2.read());
}

void mnist_fp4::thread_or_cond23_fu_8719_p2() {
    or_cond23_fu_8719_p2 = (or_cond21_fu_8692_p2.read() | or_cond22_fu_8705_p2.read());
}

void mnist_fp4::thread_or_cond2_fu_3195_p2() {
    or_cond2_fu_3195_p2 = (or_cond_fu_3168_p2.read() | or_cond1_fu_3181_p2.read());
}

void mnist_fp4::thread_or_cond3_fu_5443_p2() {
    or_cond3_fu_5443_p2 = (sel_tmp17_fu_5430_p2.read() | sel_tmp15_fu_5413_p2.read());
}

void mnist_fp4::thread_or_cond4_fu_5456_p2() {
    or_cond4_fu_5456_p2 = (sel_tmp14_fu_5407_p2.read() | sel_tmp10_fu_5381_p2.read());
}

void mnist_fp4::thread_or_cond5_fu_5470_p2() {
    or_cond5_fu_5470_p2 = (or_cond3_fu_5443_p2.read() | or_cond4_fu_5456_p2.read());
}

void mnist_fp4::thread_or_cond6_fu_4675_p2() {
    or_cond6_fu_4675_p2 = (sel_tmp26_fu_4662_p2.read() | sel_tmp24_fu_4645_p2.read());
}

void mnist_fp4::thread_or_cond7_fu_4688_p2() {
    or_cond7_fu_4688_p2 = (sel_tmp23_fu_4639_p2.read() | sel_tmp19_fu_4613_p2.read());
}

void mnist_fp4::thread_or_cond8_41_fu_6199_p2() {
    or_cond8_41_fu_6199_p2 = (tmp137_fu_6193_p2.read() & tmp136_reg_10866.read());
}

void mnist_fp4::thread_or_cond8_fu_4702_p2() {
    or_cond8_fu_4702_p2 = (or_cond6_fu_4675_p2.read() | or_cond7_fu_4688_p2.read());
}

void mnist_fp4::thread_or_cond9_fu_4824_p2() {
    or_cond9_fu_4824_p2 = (sel_tmp35_fu_4811_p2.read() | sel_tmp33_fu_4794_p2.read());
}

void mnist_fp4::thread_or_cond_42_fu_7365_p2() {
    or_cond_42_fu_7365_p2 = (tmp245_fu_7359_p2.read() & tmp244_reg_11223.read());
}

void mnist_fp4::thread_or_cond_fu_3168_p2() {
    or_cond_fu_3168_p2 = (sel_tmp4_fu_3155_p2.read() | sel_tmp_fu_3138_p2.read());
}

void mnist_fp4::thread_p_012_0_i2_fu_5884_p3() {
    p_012_0_i2_fu_5884_p3 = (!icmp6_fu_5840_p2.read()[0].is_01())? sc_lv<32>(): ((icmp6_fu_5840_p2.read()[0].to_bool())? tmp32_V_3_fu_5855_p2.read(): tmp32_V_4_fu_5880_p1.read());
}

void mnist_fp4::thread_p_03_i1_fu_5950_p3() {
    p_03_i1_fu_5950_p3 = (!tmp_155_reg_10777.read()[0].is_01())? sc_lv<32>(): ((tmp_155_reg_10777.read()[0].to_bool())? ap_const_lv32_0: f_4_fu_5946_p1.read());
}

void mnist_fp4::thread_p_03_i1_to_int_fu_5957_p1() {
    p_03_i1_to_int_fu_5957_p1 = p_03_i1_reg_10818.read();
}

void mnist_fp4::thread_p_Repl2_10_trunc_fu_7817_p2() {
    p_Repl2_10_trunc_fu_7817_p2 = (!tmp336_cast_cast_fu_7810_p3.read().is_01() || !tmp_514_fu_7807_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp336_cast_cast_fu_7810_p3.read()) + sc_biguint<8>(tmp_514_fu_7807_p1.read()));
}

void mnist_fp4::thread_p_Repl2_13_trunc_fu_8873_p2() {
    p_Repl2_13_trunc_fu_8873_p2 = (!tmp_534_fu_8863_p1.read().is_01() || !tmp337_cast_cast_fu_8866_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_534_fu_8863_p1.read()) + sc_biguint<8>(tmp337_cast_cast_fu_8866_p3.read()));
}

void mnist_fp4::thread_p_Repl2_1_trunc_fu_3886_p2() {
    p_Repl2_1_trunc_fu_3886_p2 = (!tmp323_cast_cast_fu_3879_p3.read().is_01() || !tmp_315_fu_3876_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp323_cast_cast_fu_3879_p3.read()) + sc_biguint<8>(tmp_315_fu_3876_p1.read()));
}

void mnist_fp4::thread_p_Repl2_4_trunc_fu_5005_p2() {
    p_Repl2_4_trunc_fu_5005_p2 = (!tmp_353_fu_4995_p1.read().is_01() || !tmp324_cast_cast_fu_4998_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_353_fu_4995_p1.read()) + sc_biguint<8>(tmp324_cast_cast_fu_4998_p3.read()));
}

void mnist_fp4::thread_p_Repl2_7_trunc_fu_5922_p2() {
    p_Repl2_7_trunc_fu_5922_p2 = (!tmp_442_fu_5912_p1.read().is_01() || !tmp325_cast_cast_fu_5915_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_442_fu_5912_p1.read()) + sc_biguint<8>(tmp325_cast_cast_fu_5915_p3.read()));
}

void mnist_fp4::thread_p_Result_14_fu_5776_p4() {
    p_Result_14_fu_5776_p4 = tmp_V_2_fu_5770_p3.read().range(0, 3);
}

void mnist_fp4::thread_p_Result_15_fu_5786_p3() {
    p_Result_15_fu_5786_p3 = esl_concat<28,4>(ap_const_lv28_FFFFFFF, p_Result_14_fu_5776_p4.read());
}

void mnist_fp4::thread_p_Result_16_fu_5896_p4() {
    p_Result_16_fu_5896_p4 = tmp32_V_8_fu_5892_p1.read().range(30, 23);
}

void mnist_fp4::thread_p_Result_17_fu_5935_p5() {
    p_Result_17_fu_5935_p5 = esl_partset<32,32,9,32,32>(tmp32_V_8_reg_10808.read(), tmp_200_fu_5928_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp4::thread_p_Result_19_fu_6989_p1() {
    p_Result_19_fu_6989_p1 = esl_zext<54,53>(tmp_160_fu_6982_p3.read());
}

void mnist_fp4::thread_p_Result_21_fu_7791_p4() {
    p_Result_21_fu_7791_p4 = tmp32_V_29_fu_7787_p1.read().range(30, 23);
}

void mnist_fp4::thread_p_Result_22_fu_7770_p2() {
    p_Result_22_fu_7770_p2 = (!tmp_512_fu_7766_p1.read().is_01())? sc_lv<4>(): p_Val2_30_reg_11325.read() >> (unsigned short)tmp_512_fu_7766_p1.read().to_uint();
}

void mnist_fp4::thread_p_Result_25_fu_9128_p1() {
    p_Result_25_fu_9128_p1 = esl_zext<54,53>(tmp_224_fu_9121_p3.read());
}

void mnist_fp4::thread_p_Result_28_fu_8847_p4() {
    p_Result_28_fu_8847_p4 = tmp32_V_30_fu_8843_p1.read().range(30, 23);
}

void mnist_fp4::thread_p_Result_29_fu_8817_p2() {
    p_Result_29_fu_8817_p2 = (!tmp_532_fu_8813_p1.read().is_01())? sc_lv<4>(): p_Val2_33_reg_11418.read() >> (unsigned short)tmp_532_fu_8813_p1.read().to_uint();
}

void mnist_fp4::thread_p_Result_33_fu_3740_p4() {
    p_Result_33_fu_3740_p4 = p_Val2_6_fu_3733_p3.read().range(0, 3);
}

void mnist_fp4::thread_p_Result_34_fu_3750_p3() {
    p_Result_34_fu_3750_p3 = esl_concat<28,4>(ap_const_lv28_FFFFFFF, p_Result_33_fu_3740_p4.read());
}

void mnist_fp4::thread_p_Result_35_fu_3899_p5() {
    p_Result_35_fu_3899_p5 = esl_partset<32,32,9,32,32>(tmp32_V_27_reg_10256.read(), tmp_84_fu_3892_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp4::thread_p_Result_36_fu_4407_p1() {
    p_Result_36_fu_4407_p1 = esl_zext<54,53>(tmp_128_fu_4400_p3.read());
}

void mnist_fp4::thread_p_Result_37_fu_4492_p1() {
    p_Result_37_fu_4492_p1 = esl_zext<54,53>(tmp_171_fu_4485_p3.read());
}

void mnist_fp4::thread_p_Result_38_fu_4139_p4() {
    p_Result_38_fu_4139_p4 = p_Val2_12_fu_4131_p3.read().range(0, 3);
}

void mnist_fp4::thread_p_Result_39_fu_4149_p3() {
    p_Result_39_fu_4149_p3 = esl_concat<28,4>(ap_const_lv28_FFFFFFF, p_Result_38_fu_4139_p4.read());
}

void mnist_fp4::thread_p_Result_40_fu_5018_p5() {
    p_Result_40_fu_5018_p5 = esl_partset<32,32,9,32,32>(tmp32_V_28_reg_10547.read(), tmp_115_fu_5011_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp4::thread_p_Result_41_fu_7671_p4() {
    p_Result_41_fu_7671_p4 = p_Val2_30_fu_7664_p3.read().range(0, 3);
}

void mnist_fp4::thread_p_Result_42_fu_7681_p3() {
    p_Result_42_fu_7681_p3 = esl_concat<28,4>(ap_const_lv28_FFFFFFF, p_Result_41_fu_7671_p4.read());
}

void mnist_fp4::thread_p_Result_43_fu_7830_p5() {
    p_Result_43_fu_7830_p5 = esl_partset<32,32,9,32,32>(tmp32_V_29_reg_11351.read(), tmp_242_fu_7823_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp4::thread_p_Result_44_fu_8275_p1() {
    p_Result_44_fu_8275_p1 = esl_zext<54,53>(tmp_293_fu_8268_p3.read());
}

void mnist_fp4::thread_p_Result_45_fu_8360_p1() {
    p_Result_45_fu_8360_p1 = esl_zext<54,53>(tmp_335_fu_8353_p3.read());
}

void mnist_fp4::thread_p_Result_46_fu_8046_p4() {
    p_Result_46_fu_8046_p4 = p_Val2_33_fu_8038_p3.read().range(0, 3);
}

void mnist_fp4::thread_p_Result_47_fu_8056_p3() {
    p_Result_47_fu_8056_p3 = esl_concat<28,4>(ap_const_lv28_FFFFFFF, p_Result_46_fu_8046_p4.read());
}

void mnist_fp4::thread_p_Result_48_fu_8886_p5() {
    p_Result_48_fu_8886_p5 = esl_partset<32,32,9,32,32>(tmp32_V_30_reg_11627.read(), tmp_283_fu_8879_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp4::thread_p_Result_4_fu_2985_p1() {
    p_Result_4_fu_2985_p1 = esl_zext<54,53>(tmp_33_fu_2978_p3.read());
}

void mnist_fp4::thread_p_Result_7_fu_5260_p1() {
    p_Result_7_fu_5260_p1 = esl_zext<54,53>(tmp_72_fu_5253_p3.read());
}

void mnist_fp4::thread_p_Result_8_fu_4949_p2() {
    p_Result_8_fu_4949_p2 = (!tmp_351_fu_4945_p1.read().is_01())? sc_lv<4>(): p_Val2_12_reg_10338.read() >> (unsigned short)tmp_351_fu_4945_p1.read().to_uint();
}

void mnist_fp4::thread_p_Result_9_fu_3860_p4() {
    p_Result_9_fu_3860_p4 = tmp32_V_27_fu_3856_p1.read().range(30, 23);
}

void mnist_fp4::thread_p_Result_s_40_fu_4979_p4() {
    p_Result_s_40_fu_4979_p4 = tmp32_V_28_fu_4975_p1.read().range(30, 23);
}

void mnist_fp4::thread_p_Result_s_fu_3839_p2() {
    p_Result_s_fu_3839_p2 = (!tmp_313_fu_3835_p1.read().is_01())? sc_lv<4>(): p_Val2_6_reg_10230.read() >> (unsigned short)tmp_313_fu_3835_p1.read().to_uint();
}

void mnist_fp4::thread_p_Val2_12_fu_4131_p3() {
    p_Val2_12_fu_4131_p3 = (!is_neg_1_fu_4117_p3.read()[0].is_01())? sc_lv<4>(): ((is_neg_1_fu_4117_p3.read()[0].to_bool())? tmp_73_fu_4125_p2.read(): p_Val2_3_reg_1174.read());
}

void mnist_fp4::thread_p_Val2_30_fu_7664_p3() {
    p_Val2_30_fu_7664_p3 = (!is_neg_2_fu_7652_p3.read()[0].is_01())? sc_lv<4>(): ((is_neg_2_fu_7652_p3.read()[0].to_bool())? tmp_182_fu_7659_p2.read(): p_Val2_25_reg_11309.read());
}

void mnist_fp4::thread_p_Val2_33_fu_8038_p3() {
    p_Val2_33_fu_8038_p3 = (!is_neg_3_fu_8024_p3.read()[0].is_01())? sc_lv<4>(): ((is_neg_3_fu_8024_p3.read()[0].to_bool())? tmp_226_fu_8032_p2.read(): p_Val2_1_reg_1647.read());
}

void mnist_fp4::thread_p_Val2_4_fu_4708_p3() {
    p_Val2_4_fu_4708_p3 = (!or_cond8_fu_4702_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond8_fu_4702_p2.read()[0].to_bool())? newSel10_fu_4694_p3.read(): ap_const_lv4_0);
}

void mnist_fp4::thread_p_Val2_5_fu_8576_p3() {
    p_Val2_5_fu_8576_p3 = (!or_cond20_fu_8570_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond20_fu_8570_p2.read()[0].to_bool())? newSel24_fu_8562_p3.read(): ap_const_lv4_0);
}

void mnist_fp4::thread_p_Val2_6_fu_3733_p3() {
    p_Val2_6_fu_3733_p3 = (!is_neg_fu_3721_p3.read()[0].is_01())? sc_lv<4>(): ((is_neg_fu_3721_p3.read()[0].to_bool())? tmp_55_fu_3728_p2.read(): p_Val2_s_reg_10214.read());
}

void mnist_fp4::thread_p_Val2_7_fu_4857_p3() {
    p_Val2_7_fu_4857_p3 = (!or_cond11_fu_4851_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond11_fu_4851_p2.read()[0].to_bool())? newSel13_fu_4843_p3.read(): ap_const_lv4_0);
}

void mnist_fp4::thread_p_Val2_8_fu_8725_p3() {
    p_Val2_8_fu_8725_p3 = (!or_cond23_fu_8719_p2.read()[0].is_01())? sc_lv<4>(): ((or_cond23_fu_8719_p2.read()[0].to_bool())? newSel27_fu_8711_p3.read(): ap_const_lv4_0);
}

void mnist_fp4::thread_p_shl10_cast_fu_2206_p1() {
    p_shl10_cast_fu_2206_p1 = esl_zext<11,10>(tmp_7_fu_2198_p3.read());
}

void mnist_fp4::thread_p_shl11_cast_fu_2218_p1() {
    p_shl11_cast_fu_2218_p1 = esl_zext<11,6>(tmp_10_fu_2210_p3.read());
}

void mnist_fp4::thread_p_shl14_cast_fu_2556_p1() {
    p_shl14_cast_fu_2556_p1 = esl_zext<9,8>(tmp_17_fu_2548_p3.read());
}

void mnist_fp4::thread_p_shl16_cast1_fu_2530_p1() {
    p_shl16_cast1_fu_2530_p1 = esl_zext<9,5>(tmp_13_fu_2522_p3.read());
}

void mnist_fp4::thread_p_shl16_cast_fu_2534_p1() {
    p_shl16_cast_fu_2534_p1 = esl_zext<6,5>(tmp_13_fu_2522_p3.read());
}

void mnist_fp4::thread_p_shl17_cast_fu_2595_p3() {
    p_shl17_cast_fu_2595_p3 = esl_concat<8,5>(tmp_60_fu_2591_p1.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_p_shl18_cast_fu_2611_p1() {
    p_shl18_cast_fu_2611_p1 = esl_sext<13,12>(tmp_61_fu_2603_p3.read());
}

void mnist_fp4::thread_p_shl1_cast_fu_2236_p1() {
    p_shl1_cast_fu_2236_p1 = esl_zext<11,7>(p_shl1_fu_2228_p3.read());
}

void mnist_fp4::thread_p_shl1_fu_2228_p3() {
    p_shl1_fu_2228_p3 = esl_concat<5,2>(p_s_reg_921.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_p_shl20_cast_fu_2663_p1() {
    p_shl20_cast_fu_2663_p1 = esl_zext<11,10>(tmp_159_fu_2655_p3.read());
}

void mnist_fp4::thread_p_shl21_cast_fu_2675_p1() {
    p_shl21_cast_fu_2675_p1 = esl_zext<11,6>(tmp_161_fu_2667_p3.read());
}

void mnist_fp4::thread_p_shl22_cast_fu_2790_p1() {
    p_shl22_cast_fu_2790_p1 = esl_zext<9,8>(tmp_51_fu_2782_p3.read());
}

void mnist_fp4::thread_p_shl23_cast_fu_2802_p1() {
    p_shl23_cast_fu_2802_p1 = esl_zext<9,5>(tmp_53_fu_2794_p3.read());
}

void mnist_fp4::thread_p_shl24_cast_fu_2841_p3() {
    p_shl24_cast_fu_2841_p3 = esl_concat<8,5>(tmp_104_fu_2837_p1.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_p_shl25_cast_fu_2857_p1() {
    p_shl25_cast_fu_2857_p1 = esl_sext<13,12>(tmp_105_fu_2849_p3.read());
}

void mnist_fp4::thread_p_shl26_cast_fu_3239_p1() {
    p_shl26_cast_fu_3239_p1 = esl_zext<9,8>(tmp_96_fu_3231_p3.read());
}

void mnist_fp4::thread_p_shl27_cast_fu_3251_p1() {
    p_shl27_cast_fu_3251_p1 = esl_zext<9,4>(tmp_97_fu_3243_p3.read());
}

void mnist_fp4::thread_p_shl28_cast_fu_3284_p3() {
    p_shl28_cast_fu_3284_p3 = esl_concat<8,5>(tmp_120_fu_3280_p1.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_p_shl29_cast_fu_3300_p1() {
    p_shl29_cast_fu_3300_p1 = esl_sext<13,11>(tmp_127_fu_3292_p3.read());
}

void mnist_fp4::thread_p_shl2_cast_fu_3318_p1() {
    p_shl2_cast_fu_3318_p1 = esl_zext<11,10>(p_shl2_fu_3310_p3.read());
}

void mnist_fp4::thread_p_shl2_fu_3310_p3() {
    p_shl2_fu_3310_p3 = esl_concat<5,5>(p_4_reg_1097.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_p_shl30_cast_fu_3680_p3() {
    p_shl30_cast_fu_3680_p3 = esl_concat<8,5>(tmp_294_reg_10204.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_p_shl31_cast_fu_3694_p1() {
    p_shl31_cast_fu_3694_p1 = esl_sext<13,11>(tmp_295_fu_3687_p3.read());
}

void mnist_fp4::thread_p_shl32_cast_fu_3642_p1() {
    p_shl32_cast_fu_3642_p1 = esl_zext<8,7>(tmp_289_fu_3635_p3.read());
}

void mnist_fp4::thread_p_shl33_cast_fu_3653_p1() {
    p_shl33_cast_fu_3653_p1 = esl_zext<8,4>(tmp_290_fu_3646_p3.read());
}

void mnist_fp4::thread_p_shl34_cast_fu_3963_p1() {
    p_shl34_cast_fu_3963_p1 = esl_zext<9,8>(tmp_111_fu_3955_p3.read());
}

void mnist_fp4::thread_p_shl36_cast_fu_5065_p1() {
    p_shl36_cast_fu_5065_p1 = esl_zext<9,8>(tmp_179_fu_5057_p3.read());
}

void mnist_fp4::thread_p_shl37_cast_fu_5077_p1() {
    p_shl37_cast_fu_5077_p1 = esl_zext<9,5>(tmp_181_fu_5069_p3.read());
}

void mnist_fp4::thread_p_shl38_cast_fu_4002_p3() {
    p_shl38_cast_fu_4002_p3 = esl_concat<8,5>(tmp_186_fu_3998_p1.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_p_shl39_cast_fu_4018_p1() {
    p_shl39_cast_fu_4018_p1 = esl_sext<13,12>(tmp_189_fu_4010_p3.read());
}

void mnist_fp4::thread_p_shl3_cast_fu_3330_p1() {
    p_shl3_cast_fu_3330_p1 = esl_zext<11,7>(p_shl3_fu_3322_p3.read());
}

void mnist_fp4::thread_p_shl3_fu_3322_p3() {
    p_shl3_fu_3322_p3 = esl_concat<5,2>(p_4_reg_1097.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_p_shl40_cast_fu_5504_p1() {
    p_shl40_cast_fu_5504_p1 = esl_zext<9,8>(tmp_227_fu_5496_p3.read());
}

void mnist_fp4::thread_p_shl41_cast_fu_5516_p1() {
    p_shl41_cast_fu_5516_p1 = esl_zext<9,5>(tmp_228_fu_5508_p3.read());
}

void mnist_fp4::thread_p_shl42_cast_fu_5538_p1() {
    p_shl42_cast_fu_5538_p1 = esl_zext<8,7>(tmp_237_fu_5530_p3.read());
}

void mnist_fp4::thread_p_shl43_cast_fu_5550_p1() {
    p_shl43_cast_fu_5550_p1 = esl_zext<8,4>(tmp_238_fu_5542_p3.read());
}

void mnist_fp4::thread_p_shl44_cast_fu_5116_p3() {
    p_shl44_cast_fu_5116_p3 = esl_concat<8,5>(tmp_246_fu_5112_p1.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_p_shl45_cast_fu_5132_p1() {
    p_shl45_cast_fu_5132_p1 = esl_sext<13,12>(tmp_249_fu_5124_p3.read());
}

void mnist_fp4::thread_p_shl46_cast_fu_5589_p3() {
    p_shl46_cast_fu_5589_p3 = esl_concat<7,4>(tmp_316_fu_5585_p1.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_p_shl47_cast_fu_5605_p1() {
    p_shl47_cast_fu_5605_p1 = esl_sext<11,10>(tmp_318_fu_5597_p3.read());
}

void mnist_fp4::thread_p_shl49_cast_fu_4091_p1() {
    p_shl49_cast_fu_4091_p1 = esl_zext<9,8>(tmp_356_fu_4083_p3.read());
}

void mnist_fp4::thread_p_shl4_cast_fu_6134_p1() {
    p_shl4_cast_fu_6134_p1 = esl_zext<9,8>(p_shl4_fu_6126_p3.read());
}

void mnist_fp4::thread_p_shl4_fu_6126_p3() {
    p_shl4_fu_6126_p3 = esl_concat<4,4>(tmp_365_fu_6122_p1.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_p_shl50_cast_fu_4103_p1() {
    p_shl50_cast_fu_4103_p1 = esl_zext<9,4>(tmp_357_fu_4095_p3.read());
}

void mnist_fp4::thread_p_shl51_cast_fu_6525_p1() {
    p_shl51_cast_fu_6525_p1 = esl_zext<9,8>(tmp_361_fu_6517_p3.read());
}

void mnist_fp4::thread_p_shl52_cast_fu_6537_p1() {
    p_shl52_cast_fu_6537_p1 = esl_zext<9,5>(tmp_362_fu_6529_p3.read());
}

void mnist_fp4::thread_p_shl53_cast_fu_4218_p3() {
    p_shl53_cast_fu_4218_p3 = esl_concat<8,5>(tmp_369_fu_4214_p1.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_p_shl54_cast_fu_4261_p3() {
    p_shl54_cast_fu_4261_p3 = esl_concat<7,2>(tmp_374_fu_4257_p1.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_p_shl55_cast_fu_4234_p1() {
    p_shl55_cast_fu_4234_p1 = esl_sext<13,11>(tmp_370_fu_4226_p3.read());
}

void mnist_fp4::thread_p_shl56_cast_fu_6794_p1() {
    p_shl56_cast_fu_6794_p1 = esl_zext<9,8>(tmp_376_fu_6786_p3.read());
}

void mnist_fp4::thread_p_shl57_cast_fu_6806_p1() {
    p_shl57_cast_fu_6806_p1 = esl_zext<9,5>(tmp_377_fu_6798_p3.read());
}

void mnist_fp4::thread_p_shl58_cast_fu_6576_p3() {
    p_shl58_cast_fu_6576_p3 = esl_concat<8,4>(tmp_380_fu_6572_p1.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_p_shl59_cast_fu_6592_p1() {
    p_shl59_cast_fu_6592_p1 = esl_sext<12,11>(tmp_381_fu_6584_p3.read());
}

void mnist_fp4::thread_p_shl5_cast_fu_6144_p1() {
    p_shl5_cast_fu_6144_p1 = esl_zext<9,5>(tmp_366_fu_6138_p2.read());
}

void mnist_fp4::thread_p_shl5_fu_6647_p1() {
    p_shl5_fu_6647_p1 = esl_zext<64,9>(tmp_463_fu_6639_p3.read());
}

void mnist_fp4::thread_p_shl60_cast_fu_5691_p3() {
    p_shl60_cast_fu_5691_p3 = esl_concat<8,5>(tmp_384_fu_5687_p1.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_p_shl61_cast_fu_5707_p1() {
    p_shl61_cast_fu_5707_p1 = esl_sext<13,12>(tmp_385_fu_5699_p3.read());
}

void mnist_fp4::thread_p_shl62_cast_fu_6407_p1() {
    p_shl62_cast_fu_6407_p1 = esl_zext<7,6>(tmp_419_fu_6400_p3.read());
}

void mnist_fp4::thread_p_shl63_cast_fu_6418_p1() {
    p_shl63_cast_fu_6418_p1 = esl_zext<7,3>(tmp_420_fu_6411_p3.read());
}

void mnist_fp4::thread_p_shl64_cast_fu_6445_p3() {
    p_shl64_cast_fu_6445_p3 = esl_concat<7,4>(tmp_423_reg_10942.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_p_shl65_cast_fu_6459_p1() {
    p_shl65_cast_fu_6459_p1 = esl_sext<11,9>(tmp_424_fu_6452_p3.read());
}

void mnist_fp4::thread_p_shl66_cast_fu_6845_p3() {
    p_shl66_cast_fu_6845_p3 = esl_concat<8,4>(tmp_430_fu_6841_p1.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_p_shl67_cast_fu_6861_p1() {
    p_shl67_cast_fu_6861_p1 = esl_sext<12,11>(tmp_431_fu_6853_p3.read());
}

void mnist_fp4::thread_p_shl68_cast_fu_7892_p1() {
    p_shl68_cast_fu_7892_p1 = esl_zext<9,8>(tmp_446_fu_7884_p3.read());
}

void mnist_fp4::thread_p_shl69_cast_fu_7904_p1() {
    p_shl69_cast_fu_7904_p1 = esl_zext<9,5>(tmp_447_fu_7896_p3.read());
}

void mnist_fp4::thread_p_shl6_cast_fu_7300_p1() {
    p_shl6_cast_fu_7300_p1 = esl_zext<9,8>(p_shl6_fu_7292_p3.read());
}

void mnist_fp4::thread_p_shl6_fu_7292_p3() {
    p_shl6_fu_7292_p3 = esl_concat<4,4>(tmp_450_fu_7288_p1.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_p_shl71_cast_fu_8933_p1() {
    p_shl71_cast_fu_8933_p1 = esl_zext<9,8>(tmp_465_fu_8925_p3.read());
}

void mnist_fp4::thread_p_shl72_cast_fu_8945_p1() {
    p_shl72_cast_fu_8945_p1 = esl_zext<9,5>(tmp_466_fu_8937_p3.read());
}

void mnist_fp4::thread_p_shl73_cast_fu_7943_p3() {
    p_shl73_cast_fu_7943_p3 = esl_concat<8,4>(tmp_469_fu_7939_p1.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_p_shl74_cast_fu_7959_p1() {
    p_shl74_cast_fu_7959_p1 = esl_sext<12,11>(tmp_470_fu_7951_p3.read());
}

void mnist_fp4::thread_p_shl75_cast_fu_6710_p3() {
    p_shl75_cast_fu_6710_p3 = esl_concat<8,2>(tmp_474_fu_6706_p1.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_p_shl76_cast_fu_7573_p1() {
    p_shl76_cast_fu_7573_p1 = esl_zext<8,7>(tmp_494_fu_7566_p3.read());
}

void mnist_fp4::thread_p_shl77_cast_fu_7584_p1() {
    p_shl77_cast_fu_7584_p1 = esl_zext<8,4>(tmp_495_fu_7577_p3.read());
}

void mnist_fp4::thread_p_shl78_cast_fu_7611_p3() {
    p_shl78_cast_fu_7611_p3 = esl_concat<8,4>(tmp_498_reg_11299.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_p_shl79_cast_fu_7625_p1() {
    p_shl79_cast_fu_7625_p1 = esl_sext<12,10>(tmp_499_fu_7618_p3.read());
}

void mnist_fp4::thread_p_shl7_cast_fu_7310_p1() {
    p_shl7_cast_fu_7310_p1 = esl_zext<9,5>(tmp_451_fu_7304_p2.read());
}

void mnist_fp4::thread_p_shl7_fu_8014_p1() {
    p_shl7_fu_8014_p1 = esl_zext<64,10>(tmp_535_fu_8006_p3.read());
}

void mnist_fp4::thread_p_shl83_cast_fu_8984_p3() {
    p_shl83_cast_fu_8984_p3 = esl_concat<8,4>(tmp_503_fu_8980_p1.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_p_shl84_cast_fu_9000_p1() {
    p_shl84_cast_fu_9000_p1 = esl_sext<12,11>(tmp_504_fu_8992_p3.read());
}

void mnist_fp4::thread_p_shl89_cast_fu_8129_p3() {
    p_shl89_cast_fu_8129_p3 = esl_concat<9,2>(tmp_539_fu_8125_p1.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_p_shl8_cast_fu_2125_p1() {
    p_shl8_cast_fu_2125_p1 = esl_zext<11,10>(tmp_1_fu_2117_p3.read());
}

void mnist_fp4::thread_p_shl9_cast_fu_2137_p1() {
    p_shl9_cast_fu_2137_p1 = esl_zext<11,7>(tmp_9_fu_2129_p3.read());
}

void mnist_fp4::thread_p_shl_fu_4073_p1() {
    p_shl_fu_4073_p1 = esl_zext<64,8>(tmp_354_fu_4065_p3.read());
}

void mnist_fp4::thread_p_v1_fu_6368_p3() {
    p_v1_fu_6368_p3 = (!tmp_405_reg_10903.read()[0].is_01())? sc_lv<12>(): ((tmp_405_reg_10903.read()[0].to_bool())? tmp_414_fu_6361_p1.read(): tmp_416_fu_6365_p1.read());
}

void mnist_fp4::thread_p_v2_fu_7534_p3() {
    p_v2_fu_7534_p3 = (!tmp_480_reg_11260.read()[0].is_01())? sc_lv<13>(): ((tmp_480_reg_11260.read()[0].to_bool())? tmp_489_fu_7527_p1.read(): tmp_491_fu_7531_p1.read());
}

void mnist_fp4::thread_p_v_fu_3603_p3() {
    p_v_fu_3603_p3 = (!tmp_256_reg_10165.read()[0].is_01())? sc_lv<14>(): ((tmp_256_reg_10165.read()[0].to_bool())? tmp_268_fu_3596_p1.read(): tmp_273_fu_3600_p1.read());
}

void mnist_fp4::thread_pad_temp1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state96.read())) {
        pad_temp1_0_address0 =  (sc_lv<12>) (tmp_467_cast_fu_4306_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state90.read())) {
        pad_temp1_0_address0 =  (sc_lv<12>) (tmp_437_cast_fu_3923_p1.read());
    } else {
        pad_temp1_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp4::thread_pad_temp1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state96.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state90.read()))) {
        pad_temp1_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_pad_temp1_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state90.read())) {
        pad_temp1_0_we0 = ap_const_logic_1;
    } else {
        pad_temp1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_pad_temp2_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state162.read())) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_513_fu_6755_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state156.read())) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_488_cast_fu_6489_p1.read());
    } else {
        pad_temp2_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp4::thread_pad_temp2_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state162.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state156.read()))) {
        pad_temp2_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_pad_temp2_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state156.read())) {
        pad_temp2_0_we0 = ap_const_logic_1;
    } else {
        pad_temp2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_pad_temp3_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state218.read())) {
        pad_temp3_0_address0 =  (sc_lv<11>) (tmp_542_fu_8174_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state212.read())) {
        pad_temp3_0_address0 =  (sc_lv<11>) (tmp_552_cast_fu_7855_p1.read());
    } else {
        pad_temp3_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp4::thread_pad_temp3_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state218.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state212.read()))) {
        pad_temp3_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp3_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_pad_temp3_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state212.read())) {
        pad_temp3_0_we0 = ap_const_logic_1;
    } else {
        pad_temp3_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_pad_temp_0_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state37.read())) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_389_cast_fu_2751_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state32.read())) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_290_cast_fu_2502_p1.read());
    } else {
        pad_temp_0_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp4::thread_pad_temp_0_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state37.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state32.read()))) {
        pad_temp_0_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_pad_temp_0_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state32.read())) {
        pad_temp_0_0_we0 = ap_const_logic_1;
    } else {
        pad_temp_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_phi_mul160_cast_fu_6047_p1() {
    phi_mul160_cast_fu_6047_p1 = esl_zext<11,10>(phi_mul1_reg_1367.read());
}

void mnist_fp4::thread_phi_mul162_cast_fu_7213_p1() {
    phi_mul162_cast_fu_7213_p1 = esl_zext<12,11>(phi_mul2_reg_1563.read());
}

void mnist_fp4::thread_phi_mul_cast_fu_3209_p1() {
    phi_mul_cast_fu_3209_p1 = esl_zext<13,12>(phi_mul_reg_1086.read());
}

void mnist_fp4::thread_pool1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state154.read())) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_487_cast_fu_6475_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state122.read())) {
        pool1_0_address0 = pool1_0_addr_1_reg_10728.read();
    } else {
        pool1_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp4::thread_pool1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state122.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state154.read()))) {
        pool1_0_ce0 = ap_const_logic_1;
    } else {
        pool1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_pool1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond37_fu_5657_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state122.read()))) {
        pool1_0_we0 = ap_const_logic_1;
    } else {
        pool1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_r_V_10_fu_4291_p2() {
    r_V_10_fu_4291_p2 = (!rhs_V_3_cast_fu_4287_p1.read().is_01() || !p_18_reg_1162.read().is_01())? sc_lv<5>(): (sc_biguint<5>(rhs_V_3_cast_fu_4287_p1.read()) + sc_biguint<5>(p_18_reg_1162.read()));
}

void mnist_fp4::thread_r_V_11_fu_6208_p2() {
    r_V_11_fu_6208_p2 = (!ap_const_lv4_F.is_01() || !tmp_402_fu_6204_p1.read().is_01())? sc_lv<4>(): (sc_bigint<4>(ap_const_lv4_F) + sc_biguint<4>(tmp_402_fu_6204_p1.read()));
}

void mnist_fp4::thread_r_V_12_fu_6389_p3() {
    r_V_12_fu_6389_p3 = (!tmp_405_reg_10903.read()[0].is_01())? sc_lv<2>(): ((tmp_405_reg_10903.read()[0].to_bool())? neg_ti4_fu_6379_p2.read(): tmp_418_fu_6385_p1.read());
}

void mnist_fp4::thread_r_V_13_fu_7314_p2() {
    r_V_13_fu_7314_p2 = (!p_shl6_cast_fu_7300_p1.read().is_01() || !p_shl7_cast_fu_7310_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl6_cast_fu_7300_p1.read()) - sc_biguint<9>(p_shl7_cast_fu_7310_p1.read()));
}

void mnist_fp4::thread_r_V_14_fu_6687_p2() {
    r_V_14_fu_6687_p2 = (!rhs_V_13_cast_fu_6683_p1.read().is_01() || !p_25_reg_1424.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_13_cast_fu_6683_p1.read()) + sc_biguint<4>(p_25_reg_1424.read()));
}

void mnist_fp4::thread_r_V_15_fu_7374_p2() {
    r_V_15_fu_7374_p2 = (!ap_const_lv4_F.is_01() || !tmp_476_fu_7370_p1.read().is_01())? sc_lv<4>(): (sc_bigint<4>(ap_const_lv4_F) + sc_biguint<4>(tmp_476_fu_7370_p1.read()));
}

void mnist_fp4::thread_r_V_16_fu_7555_p3() {
    r_V_16_fu_7555_p3 = (!tmp_480_reg_11260.read()[0].is_01())? sc_lv<3>(): ((tmp_480_reg_11260.read()[0].to_bool())? neg_ti6_fu_7545_p2.read(): tmp_493_fu_7551_p1.read());
}

void mnist_fp4::thread_r_V_17_fu_6740_p2() {
    r_V_17_fu_6740_p2 = (!rhs_V_8_cast_fu_6736_p1.read().is_01() || !p_32_reg_1436.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_8_cast_fu_6736_p1.read()) + sc_biguint<4>(p_32_reg_1436.read()));
}

void mnist_fp4::thread_r_V_17_tr_fu_3492_p2() {
    r_V_17_tr_fu_3492_p2 = (!tmp322_cast_fu_3489_p1.read().is_01() || !tmp32_reg_10129.read().is_01())? sc_lv<13>(): (sc_bigint<13>(tmp322_cast_fu_3489_p1.read()) + sc_biguint<13>(tmp32_reg_10129.read()));
}

void mnist_fp4::thread_r_V_18_fu_8106_p2() {
    r_V_18_fu_8106_p2 = (!rhs_V_17_cast_fu_8102_p1.read().is_01() || !p_35_reg_1623.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_17_cast_fu_8102_p1.read()) + sc_biguint<4>(p_35_reg_1623.read()));
}

void mnist_fp4::thread_r_V_19_fu_8159_p2() {
    r_V_19_fu_8159_p2 = (!rhs_V_15_cast_fu_8155_p1.read().is_01() || !p_44_reg_1635.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_15_cast_fu_8155_p1.read()) + sc_biguint<4>(p_44_reg_1635.read()));
}

void mnist_fp4::thread_r_V_1_fu_2240_p2() {
    r_V_1_fu_2240_p2 = (!p_shl10_cast_fu_2206_p1.read().is_01() || !p_shl1_cast_fu_2236_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl10_cast_fu_2206_p1.read()) - sc_biguint<11>(p_shl1_cast_fu_2236_p1.read()));
}

void mnist_fp4::thread_r_V_2_fu_2736_p2() {
    r_V_2_fu_2736_p2 = (!rhs_V_cast_fu_2732_p1.read().is_01() || !p_8_reg_983.read().is_01())? sc_lv<5>(): (sc_biguint<5>(rhs_V_cast_fu_2732_p1.read()) + sc_biguint<5>(p_8_reg_983.read()));
}

void mnist_fp4::thread_r_V_31_tr_fu_6257_p2() {
    r_V_31_tr_fu_6257_p2 = (!tmp330_cast_fu_6254_p1.read().is_01() || !tmp138_reg_10871.read().is_01())? sc_lv<11>(): (sc_bigint<11>(tmp330_cast_fu_6254_p1.read()) + sc_biguint<11>(tmp138_reg_10871.read()));
}

void mnist_fp4::thread_r_V_3_fu_2649_p2() {
    r_V_3_fu_2649_p2 = (!p_5_reg_971.read().is_01() || !rhs_V_2_cast_fu_2645_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_5_reg_971.read()) + sc_biguint<5>(rhs_V_2_cast_fu_2645_p1.read()));
}

void mnist_fp4::thread_r_V_4_fu_3439_p2() {
    r_V_4_fu_3439_p2 = (!ap_const_lv5_1F.is_01() || !p_14_reg_1109.read().is_01())? sc_lv<5>(): (sc_bigint<5>(ap_const_lv5_1F) + sc_biguint<5>(p_14_reg_1109.read()));
}

void mnist_fp4::thread_r_V_5_fu_3334_p2() {
    r_V_5_fu_3334_p2 = (!p_shl2_cast_fu_3318_p1.read().is_01() || !p_shl3_cast_fu_3330_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl2_cast_fu_3318_p1.read()) - sc_biguint<11>(p_shl3_cast_fu_3330_p1.read()));
}

void mnist_fp4::thread_r_V_5_tr_fu_2386_p2() {
    r_V_5_tr_fu_2386_p2 = (!tmp1_reg_9809.read().is_01() || !lhs_V_cast_fu_2382_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp1_reg_9809.read()) + sc_bigint<11>(lhs_V_cast_fu_2382_p1.read()));
}

void mnist_fp4::thread_r_V_6_fu_3624_p3() {
    r_V_6_fu_3624_p3 = (!tmp_256_reg_10165.read()[0].is_01())? sc_lv<2>(): ((tmp_256_reg_10165.read()[0].to_bool())? neg_ti2_fu_3614_p2.read(): tmp_282_fu_3620_p1.read());
}

void mnist_fp4::thread_r_V_7_fu_6148_p2() {
    r_V_7_fu_6148_p2 = (!p_shl4_cast_fu_6134_p1.read().is_01() || !p_shl5_cast_fu_6144_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl4_cast_fu_6134_p1.read()) - sc_biguint<9>(p_shl5_cast_fu_6144_p1.read()));
}

void mnist_fp4::thread_r_V_85_tr_fu_7423_p2() {
    r_V_85_tr_fu_7423_p2 = (!tmp335_cast_fu_7420_p1.read().is_01() || !tmp246_reg_11228.read().is_01())? sc_lv<12>(): (sc_bigint<12>(tmp335_cast_fu_7420_p1.read()) + sc_biguint<12>(tmp246_reg_11228.read()));
}

void mnist_fp4::thread_r_V_8_fu_5649_p3() {
    r_V_8_fu_5649_p3 = esl_concat<4,1>(p_33_reg_1298.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_r_V_9_fu_4199_p2() {
    r_V_9_fu_4199_p2 = (!rhs_V_7_cast_fu_4195_p1.read().is_01() || !p_10_reg_1150.read().is_01())? sc_lv<5>(): (sc_biguint<5>(rhs_V_7_cast_fu_4195_p1.read()) + sc_biguint<5>(p_10_reg_1150.read()));
}

void mnist_fp4::thread_r_V_fu_2342_p2() {
    r_V_fu_2342_p2 = (!ap_const_lv5_1F.is_01() || !p_3_reg_933.read().is_01())? sc_lv<5>(): (sc_bigint<5>(ap_const_lv5_1F) + sc_biguint<5>(p_3_reg_933.read()));
}

void mnist_fp4::thread_r_V_s_fu_5615_p3() {
    r_V_s_fu_5615_p3 = esl_concat<4,1>(p_27_reg_1287.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_ra135_V_fu_5663_p2() {
    ra135_V_fu_5663_p2 = (!p_38_reg_1322.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_38_reg_1322.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_ra136_V_fu_5723_p2() {
    ra136_V_fu_5723_p2 = (!p_43_reg_1345.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_43_reg_1345.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_ra137_V_fu_9394_p2() {
    ra137_V_fu_9394_p2 = (!p_69_reg_1783.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_69_reg_1783.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_ra138_V_fu_9406_p2() {
    ra138_V_fu_9406_p2 = (!p_72_reg_1794.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_72_reg_1794.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_rc1_V_fu_6620_p2() {
    rc1_V_fu_6620_p2 = (!p_40_reg_1448.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_40_reg_1448.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_rc2_V_fu_7987_p2() {
    rc2_V_fu_7987_p2 = (!p_51_reg_1659.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_51_reg_1659.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_rc3_V_fu_9490_p2() {
    rc3_V_fu_9490_p2 = (!p_64_reg_1871.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_64_reg_1871.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_rc4_V_fu_9634_p2() {
    rc4_V_fu_9634_p2 = (!p_74_reg_2003.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_74_reg_2003.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_rc_V_fu_4046_p2() {
    rc_V_fu_4046_p2 = (!p_24_reg_1186.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_24_reg_1186.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_reducer4_fu_6040_p3() {
    reducer4_fu_6040_p3 = (!tmp_223_fu_6034_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_223_fu_6034_p2.read()[0].to_bool())? tmp_140_reg_1333.read(): p_03_i1_reg_10818.read());
}

void mnist_fp4::thread_relu1_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state79.read())) {
        relu1_0_V_address0 =  (sc_lv<12>) (tmp_422_cast_fu_3710_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state56.read())) {
        relu1_0_V_address0 =  (sc_lv<12>) (tmp_368_cast_reg_10000.read());
    } else {
        relu1_0_V_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp4::thread_relu1_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state79.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state56.read()))) {
        relu1_0_V_ce0 = ap_const_logic_1;
    } else {
        relu1_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_relu1_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state56.read())) {
        relu1_0_V_we0 = ap_const_logic_1;
    } else {
        relu1_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_relu2_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state123.read())) {
        relu2_0_V_address0 =  (sc_lv<12>) (tmp_495_cast_fu_5747_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state118.read())) {
        relu2_0_V_address0 =  (sc_lv<12>) (tmp_429_cast_reg_10591.read());
    } else {
        relu2_0_V_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp4::thread_relu2_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state123.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state118.read()))) {
        relu2_0_V_ce0 = ap_const_logic_1;
    } else {
        relu2_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_relu2_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state118.read())) {
        relu2_0_V_we0 = ap_const_logic_1;
    } else {
        relu2_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_relu3_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state202.read())) {
        relu3_0_V_address0 =  (sc_lv<11>) (tmp_539_cast_fu_7641_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state181.read())) {
        relu3_0_V_address0 =  (sc_lv<11>) (tmp_506_cast_reg_11094.read());
    } else {
        relu3_0_V_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp4::thread_relu3_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state202.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state181.read()))) {
        relu3_0_V_ce0 = ap_const_logic_1;
    } else {
        relu3_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_relu3_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state181.read())) {
        relu3_0_V_we0 = ap_const_logic_1;
    } else {
        relu3_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_relu4_0_V_address0() {
    relu4_0_V_address0 =  (sc_lv<11>) (tmp_558_cast_reg_11671.read());
}

void mnist_fp4::thread_relu4_0_V_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state240.read())) {
        relu4_0_V_ce0 = ap_const_logic_1;
    } else {
        relu4_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_relu4_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state240.read())) {
        relu4_0_V_we0 = ap_const_logic_1;
    } else {
        relu4_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_rhs_V_10_cast_fu_7320_p1() {
    rhs_V_10_cast_fu_7320_p1 = esl_sext<12,9>(r_V_13_fu_7314_p2.read());
}

void mnist_fp4::thread_rhs_V_13_cast_fu_6683_p1() {
    rhs_V_13_cast_fu_6683_p1 = esl_zext<4,2>(p_46_reg_1473.read());
}

void mnist_fp4::thread_rhs_V_15_cast_fu_8155_p1() {
    rhs_V_15_cast_fu_8155_p1 = esl_zext<4,2>(p_66_reg_1706.read());
}

void mnist_fp4::thread_rhs_V_17_cast_fu_8102_p1() {
    rhs_V_17_cast_fu_8102_p1 = esl_zext<4,2>(p_61_reg_1683.read());
}

void mnist_fp4::thread_rhs_V_2_cast_fu_2645_p1() {
    rhs_V_2_cast_fu_2645_p1 = esl_zext<5,2>(p_12_reg_995.read());
}

void mnist_fp4::thread_rhs_V_3_cast_fu_4287_p1() {
    rhs_V_3_cast_fu_4287_p1 = esl_zext<5,2>(p_34_reg_1232.read());
}

void mnist_fp4::thread_rhs_V_4_cast_fu_6154_p1() {
    rhs_V_4_cast_fu_6154_p1 = esl_sext<11,9>(r_V_7_fu_6148_p2.read());
}

void mnist_fp4::thread_rhs_V_5_cast_fu_3340_p1() {
    rhs_V_5_cast_fu_3340_p1 = esl_sext<13,11>(r_V_5_fu_3334_p2.read());
}

void mnist_fp4::thread_rhs_V_7_cast_fu_4195_p1() {
    rhs_V_7_cast_fu_4195_p1 = esl_zext<5,2>(p_29_reg_1209.read());
}

void mnist_fp4::thread_rhs_V_8_cast_fu_6736_p1() {
    rhs_V_8_cast_fu_6736_p1 = esl_zext<4,2>(p_52_reg_1496.read());
}

void mnist_fp4::thread_rhs_V_cast_fu_2732_p1() {
    rhs_V_cast_fu_2732_p1 = esl_zext<5,2>(p_17_reg_1019.read());
}

void mnist_fp4::thread_rx1_V_fu_4281_p2() {
    rx1_V_fu_4281_p2 = (!p_34_reg_1232.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_34_reg_1232.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_rx2_V_fu_6730_p2() {
    rx2_V_fu_6730_p2 = (!p_52_reg_1496.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_52_reg_1496.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_rx3_V_fu_8149_p2() {
    rx3_V_fu_8149_p2 = (!p_66_reg_1706.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_66_reg_1706.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_rx4_V_fu_9514_p2() {
    rx4_V_fu_9514_p2 = (!p_75_reg_1893.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_75_reg_1893.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_rx5_V_fu_9658_p2() {
    rx5_V_fu_9658_p2 = (!p_77_reg_2025.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_77_reg_2025.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_rx_V_fu_2726_p2() {
    rx_V_fu_2726_p2 = (!p_17_reg_1019.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_17_reg_1019.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_ry1_V_fu_4189_p2() {
    ry1_V_fu_4189_p2 = (!p_29_reg_1209.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_29_reg_1209.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_ry2_V_fu_6677_p2() {
    ry2_V_fu_6677_p2 = (!p_46_reg_1473.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_46_reg_1473.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_ry3_V_fu_8096_p2() {
    ry3_V_fu_8096_p2 = (!p_61_reg_1683.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_61_reg_1683.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_ry4_V_fu_9502_p2() {
    ry4_V_fu_9502_p2 = (!p_71_reg_1882.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_71_reg_1882.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_ry5_V_fu_9646_p2() {
    ry5_V_fu_9646_p2 = (!p_76_reg_2014.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_76_reg_2014.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_ry_V_fu_2639_p2() {
    ry_V_fu_2639_p2 = (!p_12_reg_995.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_12_reg_995.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp4::thread_sel_tmp10_fu_5381_p2() {
    sel_tmp10_fu_5381_p2 = (tmp_126_reg_10662.read() & sel_tmp5_fu_5376_p2.read());
}

void mnist_fp4::thread_sel_tmp110_demorgan_fu_7115_p2() {
    sel_tmp110_demorgan_fu_7115_p2 = (tmp_212_reg_11142.read() | tmp_235_reg_11165.read());
}

void mnist_fp4::thread_sel_tmp11_fu_5390_p2() {
    sel_tmp11_fu_5390_p2 = (sel_tmp83_demorgan_fu_5386_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp125_demorgan_fu_7148_p2() {
    sel_tmp125_demorgan_fu_7148_p2 = (sel_tmp110_demorgan_fu_7115_p2.read() | tmp_232_reg_11153.read());
}

void mnist_fp4::thread_sel_tmp12_fu_5396_p2() {
    sel_tmp12_fu_5396_p2 = (tmp_123_reg_10650.read() & sel_tmp11_fu_5390_p2.read());
}

void mnist_fp4::thread_sel_tmp137_demorgan_fu_8486_p2() {
    sel_tmp137_demorgan_fu_8486_p2 = (tmp_286_reg_11501.read() | tmp_311_reg_11546.read());
}

void mnist_fp4::thread_sel_tmp13_fu_5401_p2() {
    sel_tmp13_fu_5401_p2 = (tmp_141_fu_5338_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp14_fu_5407_p2() {
    sel_tmp14_fu_5407_p2 = (sel_tmp12_fu_5396_p2.read() & sel_tmp13_fu_5401_p2.read());
}

void mnist_fp4::thread_sel_tmp152_demorgan_fu_8519_p2() {
    sel_tmp152_demorgan_fu_8519_p2 = (sel_tmp137_demorgan_fu_8486_p2.read() | tmp_308_reg_11534.read());
}

void mnist_fp4::thread_sel_tmp15_fu_5413_p2() {
    sel_tmp15_fu_5413_p2 = (sel_tmp12_fu_5396_p2.read() & tmp_141_fu_5338_p2.read());
}

void mnist_fp4::thread_sel_tmp161_demorgan_fu_8635_p2() {
    sel_tmp161_demorgan_fu_8635_p2 = (tmp_336_reg_11523.read() | tmp_340_reg_11580.read());
}

void mnist_fp4::thread_sel_tmp16_fu_5424_p2() {
    sel_tmp16_fu_5424_p2 = (sel_tmp98_demorgan_fu_5419_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp176_demorgan_fu_8668_p2() {
    sel_tmp176_demorgan_fu_8668_p2 = (sel_tmp161_demorgan_fu_8635_p2.read() | tmp_337_reg_11568.read());
}

void mnist_fp4::thread_sel_tmp17_fu_5430_p2() {
    sel_tmp17_fu_5430_p2 = (icmp2_reg_10674.read() & sel_tmp16_fu_5424_p2.read());
}

void mnist_fp4::thread_sel_tmp188_demorgan_fu_9254_p2() {
    sel_tmp188_demorgan_fu_9254_p2 = (tmp_279_reg_11719.read() | tmp_288_reg_11742.read());
}

void mnist_fp4::thread_sel_tmp18_fu_4608_p2() {
    sel_tmp18_fu_4608_p2 = (tmp_108_reg_10421.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp19_fu_4613_p2() {
    sel_tmp19_fu_4613_p2 = (tmp_133_reg_10466.read() & sel_tmp18_fu_4608_p2.read());
}

void mnist_fp4::thread_sel_tmp1_fu_3101_p2() {
    sel_tmp1_fu_3101_p2 = (tmp_39_reg_10048.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp203_demorgan_fu_9287_p2() {
    sel_tmp203_demorgan_fu_9287_p2 = (sel_tmp188_demorgan_fu_9254_p2.read() | tmp_281_reg_11730.read());
}

void mnist_fp4::thread_sel_tmp20_fu_4622_p2() {
    sel_tmp20_fu_4622_p2 = (sel_tmp32_demorgan_fu_4618_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp21_demorgan_fu_3144_p2() {
    sel_tmp21_demorgan_fu_3144_p2 = (sel_tmp6_demorgan_fu_3111_p2.read() | tmp_43_reg_10059.read());
}

void mnist_fp4::thread_sel_tmp21_fu_4628_p2() {
    sel_tmp21_fu_4628_p2 = (tmp_130_reg_10454.read() & sel_tmp20_fu_4622_p2.read());
}

void mnist_fp4::thread_sel_tmp22_fu_4633_p2() {
    sel_tmp22_fu_4633_p2 = (tmp_144_fu_4570_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp23_fu_4639_p2() {
    sel_tmp23_fu_4639_p2 = (sel_tmp21_fu_4628_p2.read() & sel_tmp22_fu_4633_p2.read());
}

void mnist_fp4::thread_sel_tmp24_fu_4645_p2() {
    sel_tmp24_fu_4645_p2 = (sel_tmp21_fu_4628_p2.read() & tmp_144_fu_4570_p2.read());
}

void mnist_fp4::thread_sel_tmp25_fu_4656_p2() {
    sel_tmp25_fu_4656_p2 = (sel_tmp47_demorgan_fu_4651_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp26_fu_4662_p2() {
    sel_tmp26_fu_4662_p2 = (icmp4_reg_10478.read() & sel_tmp25_fu_4656_p2.read());
}

void mnist_fp4::thread_sel_tmp27_fu_4757_p2() {
    sel_tmp27_fu_4757_p2 = (tmp_172_reg_10443.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp28_fu_4762_p2() {
    sel_tmp28_fu_4762_p2 = (tmp_176_reg_10500.read() & sel_tmp27_fu_4757_p2.read());
}

void mnist_fp4::thread_sel_tmp29_fu_4771_p2() {
    sel_tmp29_fu_4771_p2 = (sel_tmp56_demorgan_fu_4767_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp2_fu_3106_p2() {
    sel_tmp2_fu_3106_p2 = (tmp_52_reg_10071.read() & sel_tmp1_fu_3101_p2.read());
}

void mnist_fp4::thread_sel_tmp30_fu_4777_p2() {
    sel_tmp30_fu_4777_p2 = (tmp_173_reg_10488.read() & sel_tmp29_fu_4771_p2.read());
}

void mnist_fp4::thread_sel_tmp31_fu_4782_p2() {
    sel_tmp31_fu_4782_p2 = (tmp_178_fu_4719_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp32_demorgan_fu_4618_p2() {
    sel_tmp32_demorgan_fu_4618_p2 = (tmp_108_reg_10421.read() | tmp_133_reg_10466.read());
}

void mnist_fp4::thread_sel_tmp32_fu_4788_p2() {
    sel_tmp32_fu_4788_p2 = (sel_tmp30_fu_4777_p2.read() & sel_tmp31_fu_4782_p2.read());
}

void mnist_fp4::thread_sel_tmp33_fu_4794_p2() {
    sel_tmp33_fu_4794_p2 = (sel_tmp30_fu_4777_p2.read() & tmp_178_fu_4719_p2.read());
}

void mnist_fp4::thread_sel_tmp34_fu_4805_p2() {
    sel_tmp34_fu_4805_p2 = (sel_tmp71_demorgan_fu_4800_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp35_fu_4811_p2() {
    sel_tmp35_fu_4811_p2 = (icmp5_reg_10512.read() & sel_tmp34_fu_4805_p2.read());
}

void mnist_fp4::thread_sel_tmp36_fu_7105_p2() {
    sel_tmp36_fu_7105_p2 = (tmp_212_reg_11142.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp37_fu_7110_p2() {
    sel_tmp37_fu_7110_p2 = (tmp_235_reg_11165.read() & sel_tmp36_fu_7105_p2.read());
}

void mnist_fp4::thread_sel_tmp38_fu_7119_p2() {
    sel_tmp38_fu_7119_p2 = (sel_tmp110_demorgan_fu_7115_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp39_fu_7125_p2() {
    sel_tmp39_fu_7125_p2 = (tmp_232_reg_11153.read() & sel_tmp38_fu_7119_p2.read());
}

void mnist_fp4::thread_sel_tmp3_fu_3149_p2() {
    sel_tmp3_fu_3149_p2 = (sel_tmp21_demorgan_fu_3144_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp40_fu_7130_p2() {
    sel_tmp40_fu_7130_p2 = (tmp_245_fu_7067_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp41_fu_7136_p2() {
    sel_tmp41_fu_7136_p2 = (sel_tmp39_fu_7125_p2.read() & sel_tmp40_fu_7130_p2.read());
}

void mnist_fp4::thread_sel_tmp42_fu_7142_p2() {
    sel_tmp42_fu_7142_p2 = (sel_tmp39_fu_7125_p2.read() & tmp_245_fu_7067_p2.read());
}

void mnist_fp4::thread_sel_tmp43_fu_7153_p2() {
    sel_tmp43_fu_7153_p2 = (sel_tmp125_demorgan_fu_7148_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp44_fu_7159_p2() {
    sel_tmp44_fu_7159_p2 = (icmp7_reg_11177.read() & sel_tmp43_fu_7153_p2.read());
}

void mnist_fp4::thread_sel_tmp45_fu_9244_p2() {
    sel_tmp45_fu_9244_p2 = (tmp_279_reg_11719.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp46_fu_9249_p2() {
    sel_tmp46_fu_9249_p2 = (tmp_288_reg_11742.read() & sel_tmp45_fu_9244_p2.read());
}

void mnist_fp4::thread_sel_tmp47_demorgan_fu_4651_p2() {
    sel_tmp47_demorgan_fu_4651_p2 = (sel_tmp32_demorgan_fu_4618_p2.read() | tmp_130_reg_10454.read());
}

void mnist_fp4::thread_sel_tmp47_fu_9258_p2() {
    sel_tmp47_fu_9258_p2 = (sel_tmp188_demorgan_fu_9254_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp48_fu_9264_p2() {
    sel_tmp48_fu_9264_p2 = (tmp_281_reg_11730.read() & sel_tmp47_fu_9258_p2.read());
}

void mnist_fp4::thread_sel_tmp49_fu_9269_p2() {
    sel_tmp49_fu_9269_p2 = (tmp_296_fu_9206_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp4_fu_3155_p2() {
    sel_tmp4_fu_3155_p2 = (icmp_reg_10083.read() & sel_tmp3_fu_3149_p2.read());
}

void mnist_fp4::thread_sel_tmp50_fu_9275_p2() {
    sel_tmp50_fu_9275_p2 = (sel_tmp48_fu_9264_p2.read() & sel_tmp49_fu_9269_p2.read());
}

void mnist_fp4::thread_sel_tmp51_fu_9281_p2() {
    sel_tmp51_fu_9281_p2 = (sel_tmp48_fu_9264_p2.read() & tmp_296_fu_9206_p2.read());
}

void mnist_fp4::thread_sel_tmp52_fu_9292_p2() {
    sel_tmp52_fu_9292_p2 = (sel_tmp203_demorgan_fu_9287_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp53_fu_9298_p2() {
    sel_tmp53_fu_9298_p2 = (icmp9_reg_11754.read() & sel_tmp52_fu_9292_p2.read());
}

void mnist_fp4::thread_sel_tmp54_fu_8476_p2() {
    sel_tmp54_fu_8476_p2 = (tmp_286_reg_11501.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp55_fu_8481_p2() {
    sel_tmp55_fu_8481_p2 = (tmp_311_reg_11546.read() & sel_tmp54_fu_8476_p2.read());
}

void mnist_fp4::thread_sel_tmp56_demorgan_fu_4767_p2() {
    sel_tmp56_demorgan_fu_4767_p2 = (tmp_172_reg_10443.read() | tmp_176_reg_10500.read());
}

void mnist_fp4::thread_sel_tmp56_fu_8490_p2() {
    sel_tmp56_fu_8490_p2 = (sel_tmp137_demorgan_fu_8486_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp57_fu_8496_p2() {
    sel_tmp57_fu_8496_p2 = (tmp_308_reg_11534.read() & sel_tmp56_fu_8490_p2.read());
}

void mnist_fp4::thread_sel_tmp58_fu_8501_p2() {
    sel_tmp58_fu_8501_p2 = (tmp_317_fu_8438_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp59_fu_8507_p2() {
    sel_tmp59_fu_8507_p2 = (sel_tmp57_fu_8496_p2.read() & sel_tmp58_fu_8501_p2.read());
}

void mnist_fp4::thread_sel_tmp5_fu_5376_p2() {
    sel_tmp5_fu_5376_p2 = (tmp_101_reg_10639.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp60_fu_8513_p2() {
    sel_tmp60_fu_8513_p2 = (sel_tmp57_fu_8496_p2.read() & tmp_317_fu_8438_p2.read());
}

void mnist_fp4::thread_sel_tmp61_fu_8524_p2() {
    sel_tmp61_fu_8524_p2 = (sel_tmp152_demorgan_fu_8519_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp62_fu_8530_p2() {
    sel_tmp62_fu_8530_p2 = (icmp11_reg_11558.read() & sel_tmp61_fu_8524_p2.read());
}

void mnist_fp4::thread_sel_tmp63_fu_8625_p2() {
    sel_tmp63_fu_8625_p2 = (tmp_336_reg_11523.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp64_fu_8630_p2() {
    sel_tmp64_fu_8630_p2 = (tmp_340_reg_11580.read() & sel_tmp63_fu_8625_p2.read());
}

void mnist_fp4::thread_sel_tmp65_fu_8639_p2() {
    sel_tmp65_fu_8639_p2 = (sel_tmp161_demorgan_fu_8635_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp66_fu_8645_p2() {
    sel_tmp66_fu_8645_p2 = (tmp_337_reg_11568.read() & sel_tmp65_fu_8639_p2.read());
}

void mnist_fp4::thread_sel_tmp67_fu_8650_p2() {
    sel_tmp67_fu_8650_p2 = (tmp_342_fu_8587_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp68_fu_8656_p2() {
    sel_tmp68_fu_8656_p2 = (sel_tmp66_fu_8645_p2.read() & sel_tmp67_fu_8650_p2.read());
}

void mnist_fp4::thread_sel_tmp69_fu_8662_p2() {
    sel_tmp69_fu_8662_p2 = (sel_tmp66_fu_8645_p2.read() & tmp_342_fu_8587_p2.read());
}

void mnist_fp4::thread_sel_tmp6_demorgan_fu_3111_p2() {
    sel_tmp6_demorgan_fu_3111_p2 = (tmp_39_reg_10048.read() | tmp_52_reg_10071.read());
}

void mnist_fp4::thread_sel_tmp6_fu_3115_p2() {
    sel_tmp6_fu_3115_p2 = (sel_tmp6_demorgan_fu_3111_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp70_fu_8673_p2() {
    sel_tmp70_fu_8673_p2 = (sel_tmp176_demorgan_fu_8668_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp71_demorgan_fu_4800_p2() {
    sel_tmp71_demorgan_fu_4800_p2 = (sel_tmp56_demorgan_fu_4767_p2.read() | tmp_173_reg_10488.read());
}

void mnist_fp4::thread_sel_tmp71_fu_8679_p2() {
    sel_tmp71_fu_8679_p2 = (icmp12_reg_11592.read() & sel_tmp70_fu_8673_p2.read());
}

void mnist_fp4::thread_sel_tmp7_fu_3121_p2() {
    sel_tmp7_fu_3121_p2 = (tmp_43_reg_10059.read() & sel_tmp6_fu_3115_p2.read());
}

void mnist_fp4::thread_sel_tmp83_demorgan_fu_5386_p2() {
    sel_tmp83_demorgan_fu_5386_p2 = (tmp_101_reg_10639.read() | tmp_126_reg_10662.read());
}

void mnist_fp4::thread_sel_tmp8_fu_3126_p2() {
    sel_tmp8_fu_3126_p2 = (tmp_54_fu_3063_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp4::thread_sel_tmp98_demorgan_fu_5419_p2() {
    sel_tmp98_demorgan_fu_5419_p2 = (sel_tmp83_demorgan_fu_5386_p2.read() | tmp_123_reg_10650.read());
}

void mnist_fp4::thread_sel_tmp9_fu_3132_p2() {
    sel_tmp9_fu_3132_p2 = (sel_tmp7_fu_3121_p2.read() & sel_tmp8_fu_3126_p2.read());
}

void mnist_fp4::thread_sel_tmp_fu_3138_p2() {
    sel_tmp_fu_3138_p2 = (sel_tmp7_fu_3121_p2.read() & tmp_54_fu_3063_p2.read());
}

void mnist_fp4::thread_sext1_cast_fu_3497_p1() {
    sext1_cast_fu_3497_p1 = esl_sext<28,13>(r_V_17_tr_fu_3492_p2.read());
}

void mnist_fp4::thread_sext3_cast_fu_6262_p1() {
    sext3_cast_fu_6262_p1 = esl_sext<24,11>(r_V_31_tr_fu_6257_p2.read());
}

void mnist_fp4::thread_sext5_cast_fu_7428_p1() {
    sext5_cast_fu_7428_p1 = esl_sext<26,12>(r_V_85_tr_fu_7423_p2.read());
}

void mnist_fp4::thread_sh_amt_1_cast_fu_5335_p1() {
    sh_amt_1_cast_fu_5335_p1 = esl_sext<32,12>(sh_amt_1_reg_10656.read());
}

void mnist_fp4::thread_sh_amt_1_fu_5301_p3() {
    sh_amt_1_fu_5301_p3 = (!tmp_123_fu_5283_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_123_fu_5283_p2.read()[0].to_bool())? tmp_124_fu_5289_p2.read(): tmp_125_fu_5295_p2.read());
}

void mnist_fp4::thread_sh_amt_2_cast_fu_4567_p1() {
    sh_amt_2_cast_fu_4567_p1 = esl_sext<32,12>(sh_amt_2_reg_10460.read());
}

void mnist_fp4::thread_sh_amt_2_fu_4448_p3() {
    sh_amt_2_fu_4448_p3 = (!tmp_130_fu_4430_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_130_fu_4430_p2.read()[0].to_bool())? tmp_131_fu_4436_p2.read(): tmp_132_fu_4442_p2.read());
}

void mnist_fp4::thread_sh_amt_3_cast_fu_4716_p1() {
    sh_amt_3_cast_fu_4716_p1 = esl_sext<32,12>(sh_amt_3_reg_10494.read());
}

void mnist_fp4::thread_sh_amt_3_fu_4533_p3() {
    sh_amt_3_fu_4533_p3 = (!tmp_173_fu_4515_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_173_fu_4515_p2.read()[0].to_bool())? tmp_174_fu_4521_p2.read(): tmp_175_fu_4527_p2.read());
}

void mnist_fp4::thread_sh_amt_4_cast_fu_7064_p1() {
    sh_amt_4_cast_fu_7064_p1 = esl_sext<32,12>(sh_amt_4_reg_11159.read());
}

void mnist_fp4::thread_sh_amt_4_fu_7030_p3() {
    sh_amt_4_fu_7030_p3 = (!tmp_232_fu_7012_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_232_fu_7012_p2.read()[0].to_bool())? tmp_233_fu_7018_p2.read(): tmp_234_fu_7024_p2.read());
}

void mnist_fp4::thread_sh_amt_5_cast_fu_9203_p1() {
    sh_amt_5_cast_fu_9203_p1 = esl_sext<32,12>(sh_amt_5_reg_11736.read());
}

void mnist_fp4::thread_sh_amt_5_fu_9169_p3() {
    sh_amt_5_fu_9169_p3 = (!tmp_281_fu_9151_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_281_fu_9151_p2.read()[0].to_bool())? tmp_285_fu_9157_p2.read(): tmp_287_fu_9163_p2.read());
}

void mnist_fp4::thread_sh_amt_6_cast_fu_8435_p1() {
    sh_amt_6_cast_fu_8435_p1 = esl_sext<32,12>(sh_amt_6_reg_11540.read());
}

void mnist_fp4::thread_sh_amt_6_fu_8316_p3() {
    sh_amt_6_fu_8316_p3 = (!tmp_308_fu_8298_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_308_fu_8298_p2.read()[0].to_bool())? tmp_309_fu_8304_p2.read(): tmp_310_fu_8310_p2.read());
}

void mnist_fp4::thread_sh_amt_7_cast_fu_8584_p1() {
    sh_amt_7_cast_fu_8584_p1 = esl_sext<32,12>(sh_amt_7_reg_11574.read());
}

void mnist_fp4::thread_sh_amt_7_fu_8401_p3() {
    sh_amt_7_fu_8401_p3 = (!tmp_337_fu_8383_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_337_fu_8383_p2.read()[0].to_bool())? tmp_338_fu_8389_p2.read(): tmp_339_fu_8395_p2.read());
}

void mnist_fp4::thread_sh_amt_cast_fu_3060_p1() {
    sh_amt_cast_fu_3060_p1 = esl_sext<32,12>(sh_amt_reg_10065.read());
}

void mnist_fp4::thread_sh_amt_fu_3026_p3() {
    sh_amt_fu_3026_p3 = (!tmp_43_fu_3008_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_43_fu_3008_p2.read()[0].to_bool())? tmp_45_fu_3014_p2.read(): tmp_46_fu_3020_p2.read());
}

void mnist_fp4::thread_storemerge1_fu_8456_p3() {
    storemerge1_fu_8456_p3 = (!isneg_2_reg_11485.read()[0].is_01())? sc_lv<4>(): ((isneg_2_reg_11485.read()[0].to_bool())? ap_const_lv4_F: ap_const_lv4_0);
}

void mnist_fp4::thread_storemerge2_fu_5356_p3() {
    storemerge2_fu_5356_p3 = (!tmp_327_reg_10623.read()[0].is_01())? sc_lv<4>(): ((tmp_327_reg_10623.read()[0].to_bool())? ap_const_lv4_F: ap_const_lv4_0);
}

void mnist_fp4::thread_storemerge3_fu_8605_p3() {
    storemerge3_fu_8605_p3 = (!isneg_3_reg_11507.read()[0].is_01())? sc_lv<4>(): ((isneg_3_reg_11507.read()[0].to_bool())? ap_const_lv4_F: ap_const_lv4_0);
}

void mnist_fp4::thread_storemerge4_fu_4588_p3() {
    storemerge4_fu_4588_p3 = (!isneg_reg_10405.read()[0].is_01())? sc_lv<4>(): ((isneg_reg_10405.read()[0].to_bool())? ap_const_lv4_F: ap_const_lv4_0);
}

void mnist_fp4::thread_storemerge5_fu_9224_p3() {
    storemerge5_fu_9224_p3 = (!tmp_520_reg_11703.read()[0].is_01())? sc_lv<4>(): ((tmp_520_reg_11703.read()[0].to_bool())? ap_const_lv4_F: ap_const_lv4_0);
}

void mnist_fp4::thread_storemerge6_fu_4737_p3() {
    storemerge6_fu_4737_p3 = (!isneg_1_reg_10427.read()[0].is_01())? sc_lv<4>(): ((isneg_1_reg_10427.read()[0].to_bool())? ap_const_lv4_F: ap_const_lv4_0);
}

void mnist_fp4::thread_storemerge8_fu_7085_p3() {
    storemerge8_fu_7085_p3 = (!tmp_455_reg_11126.read()[0].is_01())? sc_lv<4>(): ((tmp_455_reg_11126.read()[0].to_bool())? ap_const_lv4_F: ap_const_lv4_0);
}

void mnist_fp4::thread_storemerge_fu_3081_p3() {
    storemerge_fu_3081_p3 = (!tmp_142_reg_10032.read()[0].is_01())? sc_lv<4>(): ((tmp_142_reg_10032.read()[0].to_bool())? ap_const_lv4_F: ap_const_lv4_0);
}

void mnist_fp4::thread_stream_in_TDATA_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9772.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_9781.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_886.read())))) {
        stream_in_TDATA_blk_n = stream_in_V_data_V_0_state.read()[0];
    } else {
        stream_in_TDATA_blk_n = ap_const_logic_1;
    }
}

void mnist_fp4::thread_stream_in_TREADY() {
    stream_in_TREADY = stream_in_V_dest_V_0_state.read()[1];
}

void mnist_fp4::thread_stream_in_V_data_V_0_ack_in() {
    stream_in_V_data_V_0_ack_in = stream_in_V_data_V_0_state.read()[1];
}

void mnist_fp4::thread_stream_in_V_data_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_886.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op346_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_data_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_data_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp4::thread_stream_in_V_data_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_data_V_0_sel.read())) {
        stream_in_V_data_V_0_data_out = stream_in_V_data_V_0_payload_B.read();
    } else {
        stream_in_V_data_V_0_data_out = stream_in_V_data_V_0_payload_A.read();
    }
}

void mnist_fp4::thread_stream_in_V_data_V_0_load_A() {
    stream_in_V_data_V_0_load_A = (stream_in_V_data_V_0_state_cmp_full.read() & ~stream_in_V_data_V_0_sel_wr.read());
}

void mnist_fp4::thread_stream_in_V_data_V_0_load_B() {
    stream_in_V_data_V_0_load_B = (stream_in_V_data_V_0_sel_wr.read() & stream_in_V_data_V_0_state_cmp_full.read());
}

void mnist_fp4::thread_stream_in_V_data_V_0_sel() {
    stream_in_V_data_V_0_sel = stream_in_V_data_V_0_sel_rd.read();
}

void mnist_fp4::thread_stream_in_V_data_V_0_state_cmp_full() {
    stream_in_V_data_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_data_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_data_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp4::thread_stream_in_V_data_V_0_vld_in() {
    stream_in_V_data_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp4::thread_stream_in_V_data_V_0_vld_out() {
    stream_in_V_data_V_0_vld_out = stream_in_V_data_V_0_state.read()[0];
}

void mnist_fp4::thread_stream_in_V_dest_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_886.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op346_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_dest_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_dest_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp4::thread_stream_in_V_dest_V_0_vld_in() {
    stream_in_V_dest_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp4::thread_stream_in_V_last_V_0_ack_in() {
    stream_in_V_last_V_0_ack_in = stream_in_V_last_V_0_state.read()[1];
}

void mnist_fp4::thread_stream_in_V_last_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_886.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op346_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_last_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_last_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp4::thread_stream_in_V_last_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_last_V_0_sel.read())) {
        stream_in_V_last_V_0_data_out = stream_in_V_last_V_0_payload_B.read();
    } else {
        stream_in_V_last_V_0_data_out = stream_in_V_last_V_0_payload_A.read();
    }
}

void mnist_fp4::thread_stream_in_V_last_V_0_load_A() {
    stream_in_V_last_V_0_load_A = (stream_in_V_last_V_0_state_cmp_full.read() & ~stream_in_V_last_V_0_sel_wr.read());
}

void mnist_fp4::thread_stream_in_V_last_V_0_load_B() {
    stream_in_V_last_V_0_load_B = (stream_in_V_last_V_0_sel_wr.read() & stream_in_V_last_V_0_state_cmp_full.read());
}

void mnist_fp4::thread_stream_in_V_last_V_0_sel() {
    stream_in_V_last_V_0_sel = stream_in_V_last_V_0_sel_rd.read();
}

void mnist_fp4::thread_stream_in_V_last_V_0_state_cmp_full() {
    stream_in_V_last_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_last_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_last_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp4::thread_stream_in_V_last_V_0_vld_in() {
    stream_in_V_last_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp4::thread_stream_in_V_last_V_0_vld_out() {
    stream_in_V_last_V_0_vld_out = stream_in_V_last_V_0_state.read()[0];
}

void mnist_fp4::thread_stream_in_V_user_V_0_ack_in() {
    stream_in_V_user_V_0_ack_in = stream_in_V_user_V_0_state.read()[1];
}

void mnist_fp4::thread_stream_in_V_user_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_886.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op346_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_user_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_user_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp4::thread_stream_in_V_user_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_user_V_0_sel.read())) {
        stream_in_V_user_V_0_data_out = stream_in_V_user_V_0_payload_B.read();
    } else {
        stream_in_V_user_V_0_data_out = stream_in_V_user_V_0_payload_A.read();
    }
}

void mnist_fp4::thread_stream_in_V_user_V_0_load_A() {
    stream_in_V_user_V_0_load_A = (stream_in_V_user_V_0_state_cmp_full.read() & ~stream_in_V_user_V_0_sel_wr.read());
}

void mnist_fp4::thread_stream_in_V_user_V_0_load_B() {
    stream_in_V_user_V_0_load_B = (stream_in_V_user_V_0_sel_wr.read() & stream_in_V_user_V_0_state_cmp_full.read());
}

void mnist_fp4::thread_stream_in_V_user_V_0_sel() {
    stream_in_V_user_V_0_sel = stream_in_V_user_V_0_sel_rd.read();
}

void mnist_fp4::thread_stream_in_V_user_V_0_state_cmp_full() {
    stream_in_V_user_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_user_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_user_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp4::thread_stream_in_V_user_V_0_vld_in() {
    stream_in_V_user_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp4::thread_stream_in_V_user_V_0_vld_out() {
    stream_in_V_user_V_0_vld_out = stream_in_V_user_V_0_state.read()[0];
}

void mnist_fp4::thread_tmp136_fu_6158_p2() {
    tmp136_fu_6158_p2 = (tmp_56_fu_6093_p2.read() & tmp_57_fu_6099_p2.read());
}

void mnist_fp4::thread_tmp137_fu_6193_p2() {
    tmp137_fu_6193_p2 = (tmp_70_fu_6181_p2.read() & tmp_71_fu_6187_p2.read());
}

void mnist_fp4::thread_tmp138_fu_6164_p2() {
    tmp138_fu_6164_p2 = (!rhs_V_4_cast_fu_6154_p1.read().is_01() || !phi_mul160_cast_reg_10830.read().is_01())? sc_lv<11>(): (sc_bigint<11>(rhs_V_4_cast_fu_6154_p1.read()) + sc_biguint<11>(phi_mul160_cast_reg_10830.read()));
}

void mnist_fp4::thread_tmp139_fu_6248_p2() {
    tmp139_fu_6248_p2 = (!ap_const_lv6_31.is_01() || !lhs_V_18_cast_fu_6244_p1.read().is_01())? sc_lv<6>(): (sc_bigint<6>(ap_const_lv6_31) + sc_bigint<6>(lhs_V_18_cast_fu_6244_p1.read()));
}

void mnist_fp4::thread_tmp1_fu_2246_p2() {
    tmp1_fu_2246_p2 = (!r_V_1_fu_2240_p2.read().is_01() || !ap_const_lv11_7E3.is_01())? sc_lv<11>(): (sc_biguint<11>(r_V_1_fu_2240_p2.read()) + sc_bigint<11>(ap_const_lv11_7E3));
}

void mnist_fp4::thread_tmp244_fu_7324_p2() {
    tmp244_fu_7324_p2 = (tmp_85_fu_7259_p2.read() & tmp_86_fu_7265_p2.read());
}

void mnist_fp4::thread_tmp245_fu_7359_p2() {
    tmp245_fu_7359_p2 = (tmp_117_fu_7347_p2.read() & tmp_118_fu_7353_p2.read());
}

void mnist_fp4::thread_tmp246_fu_7330_p2() {
    tmp246_fu_7330_p2 = (!rhs_V_10_cast_fu_7320_p1.read().is_01() || !phi_mul162_cast_reg_11187.read().is_01())? sc_lv<12>(): (sc_bigint<12>(rhs_V_10_cast_fu_7320_p1.read()) + sc_biguint<12>(phi_mul162_cast_reg_11187.read()));
}

void mnist_fp4::thread_tmp247_fu_7414_p2() {
    tmp247_fu_7414_p2 = (!ap_const_lv6_31.is_01() || !lhs_V_24_cast_fu_7410_p1.read().is_01())? sc_lv<6>(): (sc_bigint<6>(ap_const_lv6_31) + sc_bigint<6>(lhs_V_24_cast_fu_7410_p1.read()));
}

void mnist_fp4::thread_tmp322_cast_fu_3489_p1() {
    tmp322_cast_fu_3489_p1 = esl_sext<13,7>(tmp33_reg_10155.read());
}

void mnist_fp4::thread_tmp323_cast_cast_fu_3879_p3() {
    tmp323_cast_cast_fu_3879_p3 = (!tmp_76_reg_10261.read()[0].is_01())? sc_lv<8>(): ((tmp_76_reg_10261.read()[0].to_bool())? ap_const_lv8_7E: ap_const_lv8_7D);
}

void mnist_fp4::thread_tmp324_cast_cast_fu_4998_p3() {
    tmp324_cast_cast_fu_4998_p3 = (!tmp_112_reg_10552.read()[0].is_01())? sc_lv<8>(): ((tmp_112_reg_10552.read()[0].to_bool())? ap_const_lv8_7E: ap_const_lv8_7D);
}

void mnist_fp4::thread_tmp325_cast_cast_fu_5915_p3() {
    tmp325_cast_cast_fu_5915_p3 = (!tmp_209_reg_10813.read()[0].is_01())? sc_lv<8>(): ((tmp_209_reg_10813.read()[0].to_bool())? ap_const_lv8_7E: ap_const_lv8_7D);
}

void mnist_fp4::thread_tmp32_V_10_fu_7750_p2() {
    tmp32_V_10_fu_7750_p2 = (!tmp_195_fu_7744_p2.read().is_01())? sc_lv<32>(): tmp32_V_9_fu_7741_p1.read() << (unsigned short)tmp_195_fu_7744_p2.read().to_uint();
}

void mnist_fp4::thread_tmp32_V_11_fu_7775_p1() {
    tmp32_V_11_fu_7775_p1 = esl_zext<32,4>(p_Result_22_fu_7770_p2.read());
}

void mnist_fp4::thread_tmp32_V_12_fu_3848_p3() {
    tmp32_V_12_fu_3848_p3 = (!icmp1_fu_3804_p2.read()[0].is_01())? sc_lv<32>(): ((icmp1_fu_3804_p2.read()[0].to_bool())? tmp32_V_1_fu_3819_p2.read(): tmp32_V_2_fu_3844_p1.read());
}

void mnist_fp4::thread_tmp32_V_13_fu_8788_p1() {
    tmp32_V_13_fu_8788_p1 = esl_zext<32,4>(p_Val2_33_reg_11418.read());
}

void mnist_fp4::thread_tmp32_V_14_fu_8797_p2() {
    tmp32_V_14_fu_8797_p2 = (!tmp_272_fu_8791_p2.read().is_01())? sc_lv<32>(): tmp32_V_13_fu_8788_p1.read() << (unsigned short)tmp_272_fu_8791_p2.read().to_uint();
}

void mnist_fp4::thread_tmp32_V_15_fu_8822_p1() {
    tmp32_V_15_fu_8822_p1 = esl_zext<32,4>(p_Result_29_fu_8817_p2.read());
}

void mnist_fp4::thread_tmp32_V_18_fu_4958_p3() {
    tmp32_V_18_fu_4958_p3 = (!icmp3_fu_4914_p2.read()[0].is_01())? sc_lv<32>(): ((icmp3_fu_4914_p2.read()[0].to_bool())? tmp32_V_6_fu_4929_p2.read(): tmp32_V_7_fu_4954_p1.read());
}

void mnist_fp4::thread_tmp32_V_1_fu_3819_p2() {
    tmp32_V_1_fu_3819_p2 = (!tmp_67_fu_3813_p2.read().is_01())? sc_lv<32>(): tmp32_V_fu_3810_p1.read() << (unsigned short)tmp_67_fu_3813_p2.read().to_uint();
}

void mnist_fp4::thread_tmp32_V_21_fu_7779_p3() {
    tmp32_V_21_fu_7779_p3 = (!icmp8_fu_7735_p2.read()[0].is_01())? sc_lv<32>(): ((icmp8_fu_7735_p2.read()[0].to_bool())? tmp32_V_10_fu_7750_p2.read(): tmp32_V_11_fu_7775_p1.read());
}

void mnist_fp4::thread_tmp32_V_24_fu_8826_p3() {
    tmp32_V_24_fu_8826_p3 = (!icmp10_fu_8782_p2.read()[0].is_01())? sc_lv<32>(): ((icmp10_fu_8782_p2.read()[0].to_bool())? tmp32_V_14_fu_8797_p2.read(): tmp32_V_15_fu_8822_p1.read());
}

void mnist_fp4::thread_tmp32_V_27_fu_3856_p1() {
    tmp32_V_27_fu_3856_p1 = grp_fu_2046_p1.read();
}

void mnist_fp4::thread_tmp32_V_28_fu_4975_p1() {
    tmp32_V_28_fu_4975_p1 = grp_fu_2046_p1.read();
}

void mnist_fp4::thread_tmp32_V_29_fu_7787_p1() {
    tmp32_V_29_fu_7787_p1 = grp_fu_2046_p1.read();
}

void mnist_fp4::thread_tmp32_V_2_fu_3844_p1() {
    tmp32_V_2_fu_3844_p1 = esl_zext<32,4>(p_Result_s_fu_3839_p2.read());
}

void mnist_fp4::thread_tmp32_V_30_fu_8843_p1() {
    tmp32_V_30_fu_8843_p1 = grp_fu_2046_p1.read();
}

void mnist_fp4::thread_tmp32_V_3_fu_5855_p2() {
    tmp32_V_3_fu_5855_p2 = (!tmp_205_fu_5849_p2.read().is_01())? sc_lv<32>(): tmp32_V_s_fu_5846_p1.read() << (unsigned short)tmp_205_fu_5849_p2.read().to_uint();
}

void mnist_fp4::thread_tmp32_V_4_fu_5880_p1() {
    tmp32_V_4_fu_5880_p1 = esl_zext<32,4>(tmp_441_fu_5875_p2.read());
}

void mnist_fp4::thread_tmp32_V_5_fu_4920_p1() {
    tmp32_V_5_fu_4920_p1 = esl_zext<32,4>(p_Val2_12_reg_10338.read());
}

void mnist_fp4::thread_tmp32_V_6_fu_4929_p2() {
    tmp32_V_6_fu_4929_p2 = (!tmp_102_fu_4923_p2.read().is_01())? sc_lv<32>(): tmp32_V_5_fu_4920_p1.read() << (unsigned short)tmp_102_fu_4923_p2.read().to_uint();
}

void mnist_fp4::thread_tmp32_V_7_fu_4954_p1() {
    tmp32_V_7_fu_4954_p1 = esl_zext<32,4>(p_Result_8_fu_4949_p2.read());
}

void mnist_fp4::thread_tmp32_V_8_fu_5892_p1() {
    tmp32_V_8_fu_5892_p1 = grp_fu_2046_p1.read();
}

void mnist_fp4::thread_tmp32_V_9_fu_7741_p1() {
    tmp32_V_9_fu_7741_p1 = esl_zext<32,4>(p_Val2_30_reg_11325.read());
}

void mnist_fp4::thread_tmp32_V_fu_3810_p1() {
    tmp32_V_fu_3810_p1 = esl_zext<32,4>(p_Val2_6_reg_10230.read());
}

void mnist_fp4::thread_tmp32_V_s_fu_5846_p1() {
    tmp32_V_s_fu_5846_p1 = esl_zext<32,4>(tmp_V_2_reg_10782.read());
}

void mnist_fp4::thread_tmp32_fu_3344_p2() {
    tmp32_fu_3344_p2 = (!rhs_V_5_cast_fu_3340_p1.read().is_01() || !phi_mul_cast_reg_10093.read().is_01())? sc_lv<13>(): (sc_bigint<13>(rhs_V_5_cast_fu_3340_p1.read()) + sc_biguint<13>(phi_mul_cast_reg_10093.read()));
}

void mnist_fp4::thread_tmp330_cast_fu_6254_p1() {
    tmp330_cast_fu_6254_p1 = esl_sext<11,6>(tmp139_reg_10893.read());
}

void mnist_fp4::thread_tmp335_cast_fu_7420_p1() {
    tmp335_cast_fu_7420_p1 = esl_sext<12,6>(tmp247_reg_11250.read());
}

void mnist_fp4::thread_tmp336_cast_cast_fu_7810_p3() {
    tmp336_cast_cast_fu_7810_p3 = (!tmp_230_reg_11356.read()[0].is_01())? sc_lv<8>(): ((tmp_230_reg_11356.read()[0].to_bool())? ap_const_lv8_7E: ap_const_lv8_7D);
}

void mnist_fp4::thread_tmp337_cast_cast_fu_8866_p3() {
    tmp337_cast_cast_fu_8866_p3 = (!tmp_275_reg_11632.read()[0].is_01())? sc_lv<8>(): ((tmp_275_reg_11632.read()[0].to_bool())? ap_const_lv8_7E: ap_const_lv8_7D);
}

void mnist_fp4::thread_tmp33_fu_3483_p2() {
    tmp33_fu_3483_p2 = (!ap_const_lv7_63.is_01() || !lhs_V_10_cast_fu_3479_p1.read().is_01())? sc_lv<7>(): (sc_bigint<7>(ap_const_lv7_63) + sc_bigint<7>(lhs_V_10_cast_fu_3479_p1.read()));
}

void mnist_fp4::thread_tmp_100_fu_5171_p4() {
    tmp_100_fu_5171_p4 = conv2_0_load_to_int_fu_5168_p1.read().range(30, 23);
}

void mnist_fp4::thread_tmp_101_fu_5244_p2() {
    tmp_101_fu_5244_p2 = (!tmp_326_fu_5218_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_326_fu_5218_p1.read() == ap_const_lv63_0);
}

void mnist_fp4::thread_tmp_102_fu_4923_p2() {
    tmp_102_fu_4923_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_3_cast_fu_4900_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_3_cast_fu_4900_p1.read()));
}

void mnist_fp4::thread_tmp_103_fu_2832_p2() {
    tmp_103_fu_2832_p2 = (!tmp_138_cast_reg_9974.read().is_01() || !tmp_18_cast_fu_2828_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_138_cast_reg_9974.read()) + sc_biguint<10>(tmp_18_cast_fu_2828_p1.read()));
}

void mnist_fp4::thread_tmp_104_cast_fu_4297_p1() {
    tmp_104_cast_fu_4297_p1 = esl_zext<13,5>(r_V_10_fu_4291_p2.read());
}

void mnist_fp4::thread_tmp_104_fu_2837_p1() {
    tmp_104_fu_2837_p1 = tmp_103_fu_2832_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_105_fu_2849_p3() {
    tmp_105_fu_2849_p3 = esl_concat<10,2>(tmp_103_fu_2832_p2.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_106_fu_4397_p1() {
    tmp_106_fu_4397_p1 = esl_zext<12,11>(exp_tmp_V_reg_10411.read());
}

void mnist_fp4::thread_tmp_107_fu_2861_p2() {
    tmp_107_fu_2861_p2 = (!p_shl24_cast_fu_2841_p3.read().is_01() || !p_shl25_cast_fu_2857_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl24_cast_fu_2841_p3.read()) - sc_bigint<13>(p_shl25_cast_fu_2857_p1.read()));
}

void mnist_fp4::thread_tmp_108_fu_4355_p2() {
    tmp_108_fu_4355_p2 = (!tmp_388_fu_4329_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_388_fu_4329_p1.read() == ap_const_lv63_0);
}

void mnist_fp4::thread_tmp_10_cast_fu_2462_p1() {
    tmp_10_cast_fu_2462_p1 = esl_zext<11,5>(tmp_8_reg_9830.read());
}

void mnist_fp4::thread_tmp_10_fu_2210_p3() {
    tmp_10_fu_2210_p3 = esl_concat<5,1>(p_s_reg_921.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_110_cast_fu_6480_p1() {
    tmp_110_cast_fu_6480_p1 = esl_zext<12,5>(p_30_reg_1389.read());
}

void mnist_fp4::thread_tmp_110_fu_3939_p3() {
    tmp_110_fu_3939_p3 = esl_concat<3,2>(p_9_reg_1139.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_111_cast_fu_7930_p1() {
    tmp_111_cast_fu_7930_p1 = esl_zext<10,4>(p_35_reg_1623.read());
}

void mnist_fp4::thread_tmp_111_fu_3955_p3() {
    tmp_111_fu_3955_p3 = esl_concat<3,5>(p_9_reg_1139.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_112_fu_4989_p2() {
    tmp_112_fu_4989_p2 = (!p_Result_s_40_fu_4979_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_s_40_fu_4979_p4.read() != ap_const_lv8_9E);
}

void mnist_fp4::thread_tmp_113_fu_5197_p2() {
    tmp_113_fu_5197_p2 = (notrhs1_reg_10613.read() | notlhs1_reg_10608.read());
}

void mnist_fp4::thread_tmp_115_fu_5011_p3() {
    tmp_115_fu_5011_p3 = esl_concat<1,8>(is_neg_1_reg_10333.read(), p_Repl2_4_trunc_fu_5005_p2.read());
}

void mnist_fp4::thread_tmp_116_fu_3967_p2() {
    tmp_116_fu_3967_p2 = (!p_shl34_cast_fu_3963_p1.read().is_01() || !tmp_327_cast_fu_3951_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl34_cast_fu_3963_p1.read()) - sc_biguint<9>(tmp_327_cast_fu_3951_p1.read()));
}

void mnist_fp4::thread_tmp_117_fu_7347_p2() {
    tmp_117_fu_7347_p2 = (!p_39_reg_1585.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_39_reg_1585.read() != ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_118_fu_7353_p2() {
    tmp_118_fu_7353_p2 = (!p_39_reg_1585.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_39_reg_1585.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp4::thread_tmp_119_cast_fu_6657_p1() {
    tmp_119_cast_fu_6657_p1 = esl_zext<12,4>(p_32_reg_1436.read());
}

void mnist_fp4::thread_tmp_119_fu_3275_p2() {
    tmp_119_fu_3275_p2 = (!lhs_V_cast_39_fu_3271_p1.read().is_01() || !tmp_298_cast_reg_10111.read().is_01())? sc_lv<10>(): (sc_biguint<10>(lhs_V_cast_39_fu_3271_p1.read()) + sc_bigint<10>(tmp_298_cast_reg_10111.read()));
}

void mnist_fp4::thread_tmp_11_cast_fu_2582_p1() {
    tmp_11_cast_fu_2582_p1 = esl_zext<10,5>(p_5_reg_971.read());
}

void mnist_fp4::thread_tmp_11_fu_2222_p2() {
    tmp_11_fu_2222_p2 = (!p_shl10_cast_fu_2206_p1.read().is_01() || !p_shl11_cast_fu_2218_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl10_cast_fu_2206_p1.read()) - sc_biguint<11>(p_shl11_cast_fu_2218_p1.read()));
}

void mnist_fp4::thread_tmp_120_cast_fu_6626_p1() {
    tmp_120_cast_fu_6626_p1 = esl_zext<7,3>(p_40_reg_1448.read());
}

}

