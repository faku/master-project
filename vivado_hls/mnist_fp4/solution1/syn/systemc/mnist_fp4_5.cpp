#include "mnist_fp4.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp4::thread_tmp_120_fu_3280_p1() {
    tmp_120_fu_3280_p1 = tmp_119_fu_3275_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_122_fu_5201_p2() {
    tmp_122_fu_5201_p2 = (tmp_113_fu_5197_p2.read() & tmp_114_reg_10618.read());
}

void mnist_fp4::thread_tmp_123_fu_5283_p2() {
    tmp_123_fu_5283_p2 = (!F2_1_fu_5277_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_1_fu_5277_p2.read()) > sc_bigint<12>(ap_const_lv12_2));
}

void mnist_fp4::thread_tmp_124_fu_5289_p2() {
    tmp_124_fu_5289_p2 = (!ap_const_lv12_FFE.is_01() || !F2_1_fu_5277_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFE) + sc_biguint<12>(F2_1_fu_5277_p2.read()));
}

void mnist_fp4::thread_tmp_125_fu_5295_p2() {
    tmp_125_fu_5295_p2 = (!ap_const_lv12_2.is_01() || !F2_1_fu_5277_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_2) - sc_biguint<12>(F2_1_fu_5277_p2.read()));
}

void mnist_fp4::thread_tmp_126_fu_5309_p2() {
    tmp_126_fu_5309_p2 = (!F2_1_fu_5277_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): sc_lv<1>(F2_1_fu_5277_p2.read() == ap_const_lv12_2);
}

void mnist_fp4::thread_tmp_127_cast_fu_4966_p1() {
    tmp_127_cast_fu_4966_p1 = esl_zext<13,5>(p_18_reg_1162.read());
}

void mnist_fp4::thread_tmp_127_fu_3292_p3() {
    tmp_127_fu_3292_p3 = esl_concat<10,1>(tmp_119_fu_3275_p2.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_128_fu_4400_p3() {
    tmp_128_fu_4400_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_390_reg_10416.read());
}

void mnist_fp4::thread_tmp_130_fu_4430_p2() {
    tmp_130_fu_4430_p2 = (!F2_2_fu_4424_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_2_fu_4424_p2.read()) > sc_bigint<12>(ap_const_lv12_2));
}

void mnist_fp4::thread_tmp_131_fu_4436_p2() {
    tmp_131_fu_4436_p2 = (!ap_const_lv12_FFE.is_01() || !F2_2_fu_4424_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFE) + sc_biguint<12>(F2_2_fu_4424_p2.read()));
}

void mnist_fp4::thread_tmp_132_fu_4442_p2() {
    tmp_132_fu_4442_p2 = (!ap_const_lv12_2.is_01() || !F2_2_fu_4424_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_2) - sc_biguint<12>(F2_2_fu_4424_p2.read()));
}

void mnist_fp4::thread_tmp_133_fu_4456_p2() {
    tmp_133_fu_4456_p2 = (!F2_2_fu_4424_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): sc_lv<1>(F2_2_fu_4424_p2.read() == ap_const_lv12_2);
}

void mnist_fp4::thread_tmp_134_fu_3304_p2() {
    tmp_134_fu_3304_p2 = (!p_shl28_cast_fu_3284_p3.read().is_01() || !p_shl29_cast_fu_3300_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl28_cast_fu_3284_p3.read()) - sc_bigint<13>(p_shl29_cast_fu_3300_p1.read()));
}

void mnist_fp4::thread_tmp_135_cast_fu_5669_p1() {
    tmp_135_cast_fu_5669_p1 = esl_zext<5,2>(p_38_reg_1322.read());
}

void mnist_fp4::thread_tmp_135_fu_5206_p1() {
    tmp_135_fu_5206_p1 = grp_fu_2049_p1.read();
}

void mnist_fp4::thread_tmp_136_cast_cast_fu_5678_p1() {
    tmp_136_cast_cast_fu_5678_p1 = esl_zext<10,5>(tmp_136_fu_5673_p2.read());
}

void mnist_fp4::thread_tmp_136_fu_5673_p2() {
    tmp_136_fu_5673_p2 = (!tmp_135_cast_fu_5669_p1.read().is_01() || !r_V_s_reg_10715.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_135_cast_fu_5669_p1.read()) + sc_biguint<5>(r_V_s_reg_10715.read()));
}

void mnist_fp4::thread_tmp_137_fu_2883_p2() {
    tmp_137_fu_2883_p2 = (!tmp_20_cast_fu_2879_p1.read().is_01() || !tmp_107_reg_9987.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_20_cast_fu_2879_p1.read()) + sc_biguint<13>(tmp_107_reg_9987.read()));
}

void mnist_fp4::thread_tmp_138_cast_fu_2812_p1() {
    tmp_138_cast_fu_2812_p1 = esl_sext<10,9>(tmp_58_fu_2806_p2.read());
}

void mnist_fp4::thread_tmp_138_fu_2906_p1() {
    tmp_138_fu_2906_p1 = conv1_0_load_to_int_fu_2893_p1.read().range(23-1, 0);
}

void mnist_fp4::thread_tmp_139_fu_2943_p1() {
    tmp_139_fu_2943_p1 = ireg_V_fu_2935_p3.read().range(63-1, 0);
}

void mnist_fp4::thread_tmp_13_fu_2522_p3() {
    tmp_13_fu_2522_p3 = esl_concat<3,2>(p_2_reg_960.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_140_to_int_fu_5974_p1() {
    tmp_140_to_int_fu_5974_p1 = tmp_140_reg_1333.read();
}

void mnist_fp4::thread_tmp_141_fu_5338_p2() {
    tmp_141_fu_5338_p2 = (!sh_amt_1_reg_10656.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_1_reg_10656.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp4::thread_tmp_143_fu_5343_p1() {
    tmp_143_fu_5343_p1 = esl_zext<54,32>(sh_amt_1_cast_fu_5335_p1.read());
}

void mnist_fp4::thread_tmp_144_fu_4570_p2() {
    tmp_144_fu_4570_p2 = (!sh_amt_2_reg_10460.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_2_reg_10460.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp4::thread_tmp_145_fu_5347_p2() {
    tmp_145_fu_5347_p2 = (!man_V_6_reg_10645.read().is_01() || !tmp_143_fu_5343_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_6_reg_10645.read()) >> (unsigned short)tmp_143_fu_5343_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_146_fu_2965_p1() {
    tmp_146_fu_2965_p1 = ireg_V_fu_2935_p3.read().range(52-1, 0);
}

void mnist_fp4::thread_tmp_147_fu_7392_p3() {
    tmp_147_fu_7392_p3 = (!tmp_477_fu_7380_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_477_fu_7380_p2.read()[0].to_bool())? r_V_15_fu_7374_p2.read(): tmp_478_fu_7386_p2.read());
}

void mnist_fp4::thread_tmp_148_cast_fu_7608_p1() {
    tmp_148_cast_fu_7608_p1 = esl_zext<12,4>(tmp_147_reg_11245.read());
}

void mnist_fp4::thread_tmp_148_fu_3040_p1() {
    tmp_148_fu_3040_p1 = man_V_2_fu_2995_p3.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_149_fu_3044_p4() {
    tmp_149_fu_3044_p4 = sh_amt_fu_3026_p3.read().range(11, 2);
}

void mnist_fp4::thread_tmp_150_cast_fu_6832_p1() {
    tmp_150_cast_fu_6832_p1 = esl_zext<10,4>(p_41_reg_1530.read());
}

void mnist_fp4::thread_tmp_150_fu_3077_p1() {
    tmp_150_fu_3077_p1 = tmp_82_fu_3072_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_151_cast_fu_7400_p1() {
    tmp_151_cast_fu_7400_p1 = esl_zext<5,4>(tmp_147_fu_7392_p3.read());
}

void mnist_fp4::thread_tmp_151_fu_5363_p1() {
    tmp_151_fu_5363_p1 = esl_sext<32,4>(tmp_329_reg_10668.read());
}

void mnist_fp4::thread_tmp_153_cast_fu_5729_p1() {
    tmp_153_cast_fu_5729_p1 = esl_zext<5,2>(p_43_reg_1345.read());
}

void mnist_fp4::thread_tmp_153_fu_5366_p2() {
    tmp_153_fu_5366_p2 = (!sh_amt_1_cast_fu_5335_p1.read().is_01())? sc_lv<32>(): tmp_151_fu_5363_p1.read() << (unsigned short)sh_amt_1_cast_fu_5335_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_154_cast_cast_fu_5738_p1() {
    tmp_154_cast_cast_fu_5738_p1 = esl_zext<13,5>(tmp_154_fu_5733_p2.read());
}

void mnist_fp4::thread_tmp_154_fu_5733_p2() {
    tmp_154_fu_5733_p2 = (!tmp_153_cast_fu_5729_p1.read().is_01() || !r_V_8_reg_10733.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_153_cast_fu_5729_p1.read()) + sc_biguint<5>(r_V_8_reg_10733.read()));
}

void mnist_fp4::thread_tmp_155_fu_5760_p2() {
    tmp_155_fu_5760_p2 = (!relu2_0_V_load_reg_10764.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): sc_lv<1>(relu2_0_V_load_reg_10764.read() == ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_156_fu_3097_p1() {
    tmp_156_fu_3097_p1 = tmp_90_fu_3091_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_158_fu_2710_p2() {
    tmp_158_fu_2710_p2 = (!tmp_62_reg_9907.read().is_01() || !tmp_25_cast_fu_2706_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_62_reg_9907.read()) + sc_biguint<13>(tmp_25_cast_fu_2706_p1.read()));
}

void mnist_fp4::thread_tmp_159_fu_2655_p3() {
    tmp_159_fu_2655_p3 = esl_concat<5,5>(r_V_3_fu_2649_p2.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_160_fu_6982_p3() {
    tmp_160_fu_6982_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_456_reg_11137.read());
}

void mnist_fp4::thread_tmp_161_fu_2667_p3() {
    tmp_161_fu_2667_p3 = esl_concat<5,1>(r_V_3_fu_2649_p2.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_162_fu_4575_p1() {
    tmp_162_fu_4575_p1 = esl_zext<54,32>(sh_amt_2_cast_fu_4567_p1.read());
}

void mnist_fp4::thread_tmp_163_fu_4579_p2() {
    tmp_163_fu_4579_p2 = (!man_V_4_reg_10449.read().is_01() || !tmp_162_fu_4575_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_4_reg_10449.read()) >> (unsigned short)tmp_162_fu_4575_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_164_fu_2679_p2() {
    tmp_164_fu_2679_p2 = (!p_shl20_cast_fu_2663_p1.read().is_01() || !p_shl21_cast_fu_2675_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl20_cast_fu_2663_p1.read()) - sc_biguint<11>(p_shl21_cast_fu_2675_p1.read()));
}

void mnist_fp4::thread_tmp_165_fu_4595_p1() {
    tmp_165_fu_4595_p1 = esl_sext<32,4>(tmp_391_reg_10472.read());
}

void mnist_fp4::thread_tmp_166_fu_4598_p2() {
    tmp_166_fu_4598_p2 = (!sh_amt_2_cast_fu_4567_p1.read().is_01())? sc_lv<32>(): tmp_165_fu_4595_p1.read() << (unsigned short)sh_amt_2_cast_fu_4567_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_167_fu_2689_p2() {
    tmp_167_fu_2689_p2 = (!tmp_27_cast_fu_2685_p1.read().is_01() || !tmp_49_cast_reg_9889.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_27_cast_fu_2685_p1.read()) + sc_bigint<7>(tmp_49_cast_reg_9889.read()));
}

void mnist_fp4::thread_tmp_168_fu_7646_p2() {
    tmp_168_fu_7646_p2 = (!relu3_0_V_q0.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): sc_lv<1>(relu3_0_V_q0.read() == ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_169_cast_fu_4311_p1() {
    tmp_169_cast_fu_4311_p1 = esl_zext<9,2>(p_34_reg_1232.read());
}

void mnist_fp4::thread_tmp_169_fu_2694_p2() {
    tmp_169_fu_2694_p2 = (!ap_const_lv7_2.is_01())? sc_lv<7>(): tmp_167_fu_2689_p2.read() << (unsigned short)ap_const_lv7_2.to_uint();
}

void mnist_fp4::thread_tmp_16_cast_fu_2493_p1() {
    tmp_16_cast_fu_2493_p1 = esl_zext<11,5>(p_3_reg_933.read());
}

void mnist_fp4::thread_tmp_16_fu_2538_p2() {
    tmp_16_fu_2538_p2 = (!p_shl16_cast_fu_2534_p1.read().is_01() || !tmp_7_cast_fu_2518_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(p_shl16_cast_fu_2534_p1.read()) - sc_biguint<6>(tmp_7_cast_fu_2518_p1.read()));
}

void mnist_fp4::thread_tmp_170_fu_4482_p1() {
    tmp_170_fu_4482_p1 = esl_zext<12,11>(exp_tmp_V_1_reg_10433.read());
}

void mnist_fp4::thread_tmp_171_fu_4485_p3() {
    tmp_171_fu_4485_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_397_reg_10438.read());
}

void mnist_fp4::thread_tmp_172_fu_4391_p2() {
    tmp_172_fu_4391_p2 = (!tmp_395_fu_4365_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_395_fu_4365_p1.read() == ap_const_lv63_0);
}

void mnist_fp4::thread_tmp_173_fu_4515_p2() {
    tmp_173_fu_4515_p2 = (!F2_3_fu_4509_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_3_fu_4509_p2.read()) > sc_bigint<12>(ap_const_lv12_2));
}

void mnist_fp4::thread_tmp_174_fu_4521_p2() {
    tmp_174_fu_4521_p2 = (!ap_const_lv12_FFE.is_01() || !F2_3_fu_4509_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFE) + sc_biguint<12>(F2_3_fu_4509_p2.read()));
}

void mnist_fp4::thread_tmp_175_fu_4527_p2() {
    tmp_175_fu_4527_p2 = (!ap_const_lv12_2.is_01() || !F2_3_fu_4509_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_2) - sc_biguint<12>(F2_3_fu_4509_p2.read()));
}

void mnist_fp4::thread_tmp_176_fu_4541_p2() {
    tmp_176_fu_4541_p2 = (!F2_3_fu_4509_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): sc_lv<1>(F2_3_fu_4509_p2.read() == ap_const_lv12_2);
}

void mnist_fp4::thread_tmp_177_fu_2700_p2() {
    tmp_177_fu_2700_p2 = (!tmp_169_fu_2694_p2.read().is_01() || !tmp_167_fu_2689_p2.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_169_fu_2694_p2.read()) - sc_biguint<7>(tmp_167_fu_2689_p2.read()));
}

void mnist_fp4::thread_tmp_178_fu_4719_p2() {
    tmp_178_fu_4719_p2 = (!sh_amt_3_reg_10494.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_3_reg_10494.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp4::thread_tmp_179_fu_5057_p3() {
    tmp_179_fu_5057_p3 = esl_concat<3,5>(p_19_reg_1243.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_17_fu_2548_p3() {
    tmp_17_fu_2548_p3 = esl_concat<3,5>(p_2_reg_960.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_180_fu_4724_p1() {
    tmp_180_fu_4724_p1 = esl_zext<54,32>(sh_amt_3_cast_fu_4716_p1.read());
}

void mnist_fp4::thread_tmp_181_fu_5069_p3() {
    tmp_181_fu_5069_p3 = esl_concat<3,2>(p_19_reg_1243.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_182_fu_7659_p2() {
    tmp_182_fu_7659_p2 = (!ap_const_lv4_0.is_01() || !p_Val2_25_reg_11309.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_0) - sc_biguint<4>(p_Val2_25_reg_11309.read()));
}

void mnist_fp4::thread_tmp_183_fu_5081_p2() {
    tmp_183_fu_5081_p2 = (!p_shl36_cast_fu_5065_p1.read().is_01() || !p_shl37_cast_fu_5077_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl36_cast_fu_5065_p1.read()) - sc_biguint<9>(p_shl37_cast_fu_5077_p1.read()));
}

void mnist_fp4::thread_tmp_184_cast_fu_6883_p1() {
    tmp_184_cast_fu_6883_p1 = esl_zext<12,4>(p_47_reg_1541.read());
}

void mnist_fp4::thread_tmp_184_fu_3993_p2() {
    tmp_184_fu_3993_p2 = (!tmp_350_cast_reg_10289.read().is_01() || !tmp_29_cast_fu_3989_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_350_cast_reg_10289.read()) + sc_biguint<10>(tmp_29_cast_fu_3989_p1.read()));
}

void mnist_fp4::thread_tmp_185_fu_4728_p2() {
    tmp_185_fu_4728_p2 = (!man_V_9_reg_10483.read().is_01() || !tmp_180_fu_4724_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_9_reg_10483.read()) >> (unsigned short)tmp_180_fu_4724_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_186_fu_3998_p1() {
    tmp_186_fu_3998_p1 = tmp_184_fu_3993_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_187_fu_6693_p1() {
    tmp_187_fu_6693_p1 = esl_zext<64,2>(p_46_reg_1473.read());
}

void mnist_fp4::thread_tmp_188_fu_5765_p2() {
    tmp_188_fu_5765_p2 = (!ap_const_lv4_0.is_01() || !relu2_0_V_load_reg_10764.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_0) - sc_biguint<4>(relu2_0_V_load_reg_10764.read()));
}

void mnist_fp4::thread_tmp_189_fu_4010_p3() {
    tmp_189_fu_4010_p3 = esl_concat<10,2>(tmp_184_fu_3993_p2.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_18_cast_fu_2828_p1() {
    tmp_18_cast_fu_2828_p1 = esl_zext<10,5>(p_11_reg_1053.read());
}

void mnist_fp4::thread_tmp_18_fu_2560_p2() {
    tmp_18_fu_2560_p2 = (!p_shl14_cast_fu_2556_p1.read().is_01() || !p_shl16_cast1_fu_2530_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl14_cast_fu_2556_p1.read()) - sc_biguint<9>(p_shl16_cast1_fu_2530_p1.read()));
}

void mnist_fp4::thread_tmp_190_fu_4022_p2() {
    tmp_190_fu_4022_p2 = (!p_shl38_cast_fu_4002_p3.read().is_01() || !p_shl39_cast_fu_4018_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl38_cast_fu_4002_p3.read()) - sc_bigint<13>(p_shl39_cast_fu_4018_p1.read()));
}

void mnist_fp4::thread_tmp_191_fu_3361_p2() {
    tmp_191_fu_3361_p2 = (!p_4_reg_1097.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_4_reg_1097.read() == ap_const_lv5_1F);
}

void mnist_fp4::thread_tmp_193_fu_8756_p2() {
    tmp_193_fu_8756_p2 = (!p_Val2_1_reg_1647.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): sc_lv<1>(p_Val2_1_reg_1647.read() == ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_194_cast_fu_7993_p1() {
    tmp_194_cast_fu_7993_p1 = esl_zext<8,4>(p_51_reg_1659.read());
}

void mnist_fp4::thread_tmp_194_fu_3367_p2() {
    tmp_194_fu_3367_p2 = (grp_fu_2078_p2.read() | tmp_191_fu_3361_p2.read());
}

void mnist_fp4::thread_tmp_195_fu_7744_p2() {
    tmp_195_fu_7744_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_7_cast_fu_7721_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_7_cast_fu_7721_p1.read()));
}

void mnist_fp4::thread_tmp_196_fu_3373_p2() {
    tmp_196_fu_3373_p2 = (!p_4_reg_1097.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_4_reg_1097.read() == ap_const_lv5_1D);
}

void mnist_fp4::thread_tmp_197_fu_3379_p2() {
    tmp_197_fu_3379_p2 = (tmp_196_fu_3373_p2.read() | tmp_194_fu_3367_p2.read());
}

void mnist_fp4::thread_tmp_198_fu_4744_p1() {
    tmp_198_fu_4744_p1 = esl_sext<32,4>(tmp_398_reg_10506.read());
}

void mnist_fp4::thread_tmp_199_fu_4747_p2() {
    tmp_199_fu_4747_p2 = (!sh_amt_3_cast_fu_4716_p1.read().is_01())? sc_lv<32>(): tmp_198_fu_4744_p1.read() << (unsigned short)sh_amt_3_cast_fu_4716_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_19_fu_2264_p2() {
    tmp_19_fu_2264_p2 = (!p_s_reg_921.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_921.read() == ap_const_lv5_1F);
}

void mnist_fp4::thread_tmp_1_fu_2117_p3() {
    tmp_1_fu_2117_p3 = esl_concat<5,5>(p_0_reg_804.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_200_fu_5928_p3() {
    tmp_200_fu_5928_p3 = esl_concat<1,8>(tmp_434_reg_10771.read(), p_Repl2_7_trunc_fu_5922_p2.read());
}

void mnist_fp4::thread_tmp_201_fu_3385_p2() {
    tmp_201_fu_3385_p2 = (!p_4_reg_1097.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_4_reg_1097.read() == ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_202_fu_3391_p2() {
    tmp_202_fu_3391_p2 = (tmp_201_fu_3385_p2.read() | tmp_197_fu_3379_p2.read());
}

void mnist_fp4::thread_tmp_203_fu_2746_p2() {
    tmp_203_fu_2746_p2 = (!tmp_164_reg_9928.read().is_01() || !tmp_34_cast_fu_2742_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_164_reg_9928.read()) + sc_biguint<11>(tmp_34_cast_fu_2742_p1.read()));
}

void mnist_fp4::thread_tmp_204_fu_2760_p2() {
    tmp_204_fu_2760_p2 = (!tmp_177_reg_9933.read().is_01() || !tmp_35_cast_fu_2756_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_177_reg_9933.read()) + sc_biguint<7>(tmp_35_cast_fu_2756_p1.read()));
}

void mnist_fp4::thread_tmp_205_fu_5849_p2() {
    tmp_205_fu_5849_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_5_cast_fu_5826_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_5_cast_fu_5826_p1.read()));
}

void mnist_fp4::thread_tmp_206_fu_3397_p2() {
    tmp_206_fu_3397_p2 = (!p_14_reg_1109.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1109.read() == ap_const_lv5_1F);
}

void mnist_fp4::thread_tmp_207_fu_3403_p2() {
    tmp_207_fu_3403_p2 = (!p_14_reg_1109.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1109.read() == ap_const_lv5_1E);
}

void mnist_fp4::thread_tmp_208_fu_3409_p2() {
    tmp_208_fu_3409_p2 = (tmp_207_fu_3403_p2.read() | tmp_206_fu_3397_p2.read());
}

void mnist_fp4::thread_tmp_209_fu_5906_p2() {
    tmp_209_fu_5906_p2 = (!p_Result_16_fu_5896_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_16_fu_5896_p4.read() != ap_const_lv8_9E);
}

void mnist_fp4::thread_tmp_20_cast_fu_2879_p1() {
    tmp_20_cast_fu_2879_p1 = esl_zext<13,5>(p_16_reg_1064.read());
}

void mnist_fp4::thread_tmp_210_fu_6979_p1() {
    tmp_210_fu_6979_p1 = esl_zext<12,11>(p_Result_18_reg_11132.read());
}

void mnist_fp4::thread_tmp_211_fu_5960_p4() {
    tmp_211_fu_5960_p4 = p_03_i1_to_int_fu_5957_p1.read().range(30, 23);
}

void mnist_fp4::thread_tmp_212_fu_6973_p2() {
    tmp_212_fu_6973_p2 = (!tmp_454_fu_6947_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_454_fu_6947_p1.read() == ap_const_lv63_0);
}

void mnist_fp4::thread_tmp_213_fu_3415_p2() {
    tmp_213_fu_3415_p2 = (!p_14_reg_1109.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1109.read() == ap_const_lv5_1D);
}

void mnist_fp4::thread_tmp_214_cast_fu_6760_p1() {
    tmp_214_cast_fu_6760_p1 = esl_zext<10,2>(p_52_reg_1496.read());
}

void mnist_fp4::thread_tmp_214_fu_3421_p2() {
    tmp_214_fu_3421_p2 = (tmp_213_fu_3415_p2.read() | tmp_208_fu_3409_p2.read());
}

void mnist_fp4::thread_tmp_216_fu_3427_p2() {
    tmp_216_fu_3427_p2 = (!p_14_reg_1109.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1109.read() == ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_217_fu_5978_p4() {
    tmp_217_fu_5978_p4 = tmp_140_to_int_fu_5974_p1.read().range(30, 23);
}

void mnist_fp4::thread_tmp_218_fu_3433_p2() {
    tmp_218_fu_3433_p2 = (tmp_216_fu_3427_p2.read() | tmp_214_fu_3421_p2.read());
}

void mnist_fp4::thread_tmp_219_fu_6004_p2() {
    tmp_219_fu_6004_p2 = (notrhs2_fu_5998_p2.read() | notlhs2_fu_5992_p2.read());
}

void mnist_fp4::thread_tmp_21_fu_2896_p4() {
    tmp_21_fu_2896_p4 = conv1_0_load_to_int_fu_2893_p1.read().range(30, 23);
}

void mnist_fp4::thread_tmp_220_fu_6022_p2() {
    tmp_220_fu_6022_p2 = (notrhs3_fu_6016_p2.read() | notlhs3_fu_6010_p2.read());
}

void mnist_fp4::thread_tmp_221_fu_6028_p2() {
    tmp_221_fu_6028_p2 = (tmp_219_fu_6004_p2.read() & tmp_220_fu_6022_p2.read());
}

void mnist_fp4::thread_tmp_223_fu_6034_p2() {
    tmp_223_fu_6034_p2 = (tmp_221_fu_6028_p2.read() & grp_fu_2055_p2.read());
}

void mnist_fp4::thread_tmp_224_fu_9121_p3() {
    tmp_224_fu_9121_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_521_reg_11714.read());
}

void mnist_fp4::thread_tmp_225_fu_6900_p4() {
    tmp_225_fu_6900_p4 = conv3_0_load_to_int_fu_6897_p1.read().range(30, 23);
}

void mnist_fp4::thread_tmp_226_fu_8032_p2() {
    tmp_226_fu_8032_p2 = (!ap_const_lv4_0.is_01() || !p_Val2_1_reg_1647.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_0) - sc_biguint<4>(p_Val2_1_reg_1647.read()));
}

void mnist_fp4::thread_tmp_227_fu_5496_p3() {
    tmp_227_fu_5496_p3 = esl_concat<3,5>(p_22_reg_1276.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_228_fu_5508_p3() {
    tmp_228_fu_5508_p3 = esl_concat<3,2>(p_22_reg_1276.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_229_fu_5520_p2() {
    tmp_229_fu_5520_p2 = (!p_shl40_cast_fu_5504_p1.read().is_01() || !p_shl41_cast_fu_5516_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl40_cast_fu_5504_p1.read()) - sc_biguint<9>(p_shl41_cast_fu_5516_p1.read()));
}

void mnist_fp4::thread_tmp_22_fu_2270_p2() {
    tmp_22_fu_2270_p2 = (grp_fu_2071_p2.read() | tmp_19_fu_2264_p2.read());
}

void mnist_fp4::thread_tmp_230_fu_7801_p2() {
    tmp_230_fu_7801_p2 = (!p_Result_21_fu_7791_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_21_fu_7791_p4.read() != ap_const_lv8_9E);
}

void mnist_fp4::thread_tmp_231_fu_6926_p2() {
    tmp_231_fu_6926_p2 = (notrhs4_reg_11116.read() | notlhs4_reg_11111.read());
}

void mnist_fp4::thread_tmp_232_fu_7012_p2() {
    tmp_232_fu_7012_p2 = (!F2_4_fu_7006_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_4_fu_7006_p2.read()) > sc_bigint<12>(ap_const_lv12_2));
}

void mnist_fp4::thread_tmp_233_fu_7018_p2() {
    tmp_233_fu_7018_p2 = (!ap_const_lv12_FFE.is_01() || !F2_4_fu_7006_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFE) + sc_biguint<12>(F2_4_fu_7006_p2.read()));
}

void mnist_fp4::thread_tmp_234_fu_7024_p2() {
    tmp_234_fu_7024_p2 = (!ap_const_lv12_2.is_01() || !F2_4_fu_7006_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_2) - sc_biguint<12>(F2_4_fu_7006_p2.read()));
}

void mnist_fp4::thread_tmp_235_fu_7038_p2() {
    tmp_235_fu_7038_p2 = (!F2_4_fu_7006_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): sc_lv<1>(F2_4_fu_7006_p2.read() == ap_const_lv12_2);
}

void mnist_fp4::thread_tmp_237_fu_5530_p3() {
    tmp_237_fu_5530_p3 = esl_concat<3,4>(p_22_reg_1276.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_238_fu_5542_p3() {
    tmp_238_fu_5542_p3 = esl_concat<3,1>(p_22_reg_1276.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_239_cast_fu_8971_p1() {
    tmp_239_cast_fu_8971_p1 = esl_zext<10,4>(p_55_reg_1728.read());
}

void mnist_fp4::thread_tmp_239_fu_5554_p2() {
    tmp_239_fu_5554_p2 = (!p_shl42_cast_fu_5538_p1.read().is_01() || !p_shl43_cast_fu_5550_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl42_cast_fu_5538_p1.read()) - sc_biguint<8>(p_shl43_cast_fu_5550_p1.read()));
}

void mnist_fp4::thread_tmp_23_fu_2922_p2() {
    tmp_23_fu_2922_p2 = (notrhs_reg_10022.read() | notlhs_reg_10017.read());
}

void mnist_fp4::thread_tmp_240_fu_6930_p2() {
    tmp_240_fu_6930_p2 = (tmp_231_fu_6926_p2.read() & tmp_236_reg_11121.read());
}

void mnist_fp4::thread_tmp_242_fu_7823_p3() {
    tmp_242_fu_7823_p3 = esl_concat<1,8>(is_neg_2_reg_11320.read(), p_Repl2_10_trunc_fu_7817_p2.read());
}

void mnist_fp4::thread_tmp_243_fu_6935_p1() {
    tmp_243_fu_6935_p1 = grp_fu_2049_p1.read();
}

void mnist_fp4::thread_tmp_244_fu_5107_p2() {
    tmp_244_fu_5107_p2 = (!tmp_384_cast_reg_10565.read().is_01() || !tmp_62_cast_fu_5103_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_384_cast_reg_10565.read()) + sc_biguint<10>(tmp_62_cast_fu_5103_p1.read()));
}

void mnist_fp4::thread_tmp_245_fu_7067_p2() {
    tmp_245_fu_7067_p2 = (!sh_amt_4_reg_11159.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_4_reg_11159.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp4::thread_tmp_246_fu_5112_p1() {
    tmp_246_fu_5112_p1 = tmp_244_fu_5107_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_247_fu_7072_p1() {
    tmp_247_fu_7072_p1 = esl_zext<54,32>(sh_amt_4_cast_fu_7064_p1.read());
}

void mnist_fp4::thread_tmp_248_fu_7076_p2() {
    tmp_248_fu_7076_p2 = (!man_V_7_reg_11148.read().is_01() || !tmp_247_fu_7072_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_7_reg_11148.read()) >> (unsigned short)tmp_247_fu_7072_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_249_fu_5124_p3() {
    tmp_249_fu_5124_p3 = esl_concat<10,2>(tmp_244_fu_5107_p2.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_24_cast_fu_2368_p1() {
    tmp_24_cast_fu_2368_p1 = esl_zext<6,5>(p_3_reg_933.read());
}

void mnist_fp4::thread_tmp_250_fu_7092_p1() {
    tmp_250_fu_7092_p1 = esl_sext<32,4>(tmp_457_reg_11171.read());
}

void mnist_fp4::thread_tmp_251_fu_7095_p2() {
    tmp_251_fu_7095_p2 = (!sh_amt_4_cast_fu_7064_p1.read().is_01())? sc_lv<32>(): tmp_250_fu_7092_p1.read() << (unsigned short)sh_amt_4_cast_fu_7064_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_252_fu_5136_p2() {
    tmp_252_fu_5136_p2 = (!p_shl44_cast_fu_5116_p3.read().is_01() || !p_shl45_cast_fu_5132_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl44_cast_fu_5116_p3.read()) - sc_bigint<13>(p_shl45_cast_fu_5132_p1.read()));
}

void mnist_fp4::thread_tmp_253_fu_3445_p2() {
    tmp_253_fu_3445_p2 = (!r_V_4_fu_3439_p2.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): (sc_biguint<5>(r_V_4_fu_3439_p2.read()) < sc_biguint<5>(ap_const_lv5_1C));
}

void mnist_fp4::thread_tmp_254_fu_3451_p2() {
    tmp_254_fu_3451_p2 = (!ap_const_lv5_3.is_01() || !p_14_reg_1109.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_3) + sc_biguint<5>(p_14_reg_1109.read()));
}

void mnist_fp4::thread_tmp_255_cast_fu_9022_p1() {
    tmp_255_cast_fu_9022_p1 = esl_zext<12,4>(p_60_reg_1739.read());
}

void mnist_fp4::thread_tmp_255_fu_3501_p1() {
    tmp_255_fu_3501_p1 = mul1_fu_9672_p2.read().range(26-1, 0);
}

void mnist_fp4::thread_tmp_258_fu_3538_p4() {
    tmp_258_fu_3538_p4 = neg_mul1_fu_3533_p2.read().range(25, 18);
}

void mnist_fp4::thread_tmp_259_fu_8112_p1() {
    tmp_259_fu_8112_p1 = esl_zext<64,2>(p_61_reg_1683.read());
}

void mnist_fp4::thread_tmp_25_cast_fu_2706_p1() {
    tmp_25_cast_fu_2706_p1 = esl_zext<13,5>(p_8_reg_983.read());
}

void mnist_fp4::thread_tmp_25_fu_2276_p2() {
    tmp_25_fu_2276_p2 = (!p_s_reg_921.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_921.read() == ap_const_lv5_1D);
}

void mnist_fp4::thread_tmp_260_cast_fu_7846_p1() {
    tmp_260_cast_fu_7846_p1 = esl_zext<13,5>(p_39_reg_1585.read());
}

void mnist_fp4::thread_tmp_260_fu_3548_p1() {
    tmp_260_fu_3548_p1 = esl_sext<13,8>(tmp_258_fu_3538_p4.read());
}

void mnist_fp4::thread_tmp_262_fu_9039_p4() {
    tmp_262_fu_9039_p4 = conv4_0_load_to_int_fu_9036_p1.read().range(30, 23);
}

void mnist_fp4::thread_tmp_263_fu_3552_p1() {
    tmp_263_fu_3552_p1 = esl_sext<13,10>(tmp_261_reg_10173.read());
}

void mnist_fp4::thread_tmp_264_fu_3555_p3() {
    tmp_264_fu_3555_p3 = (!tmp_256_reg_10165.read()[0].is_01())? sc_lv<13>(): ((tmp_256_reg_10165.read()[0].to_bool())? tmp_260_fu_3548_p1.read(): tmp_263_fu_3552_p1.read());
}

void mnist_fp4::thread_tmp_265_fu_3631_p1() {
    tmp_265_fu_3631_p1 = grp_fu_3575_p2.read().range(9-1, 0);
}

void mnist_fp4::thread_tmp_266_fu_3521_p1() {
    tmp_266_fu_3521_p1 = mul2_fu_9680_p2.read().range(27-1, 0);
}

void mnist_fp4::thread_tmp_267_fu_3586_p4() {
    tmp_267_fu_3586_p4 = neg_mul2_fu_3581_p2.read().range(26, 23);
}

void mnist_fp4::thread_tmp_268_fu_3596_p1() {
    tmp_268_fu_3596_p1 = esl_sext<14,4>(tmp_267_fu_3586_p4.read());
}

void mnist_fp4::thread_tmp_26_fu_2282_p2() {
    tmp_26_fu_2282_p2 = (tmp_25_fu_2276_p2.read() | tmp_22_fu_2270_p2.read());
}

void mnist_fp4::thread_tmp_270_fu_9065_p2() {
    tmp_270_fu_9065_p2 = (notrhs5_reg_11693.read() | notlhs5_reg_11688.read());
}

void mnist_fp4::thread_tmp_272_fu_8791_p2() {
    tmp_272_fu_8791_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_9_cast_fu_8768_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_9_cast_fu_8768_p1.read()));
}

void mnist_fp4::thread_tmp_273_fu_3600_p1() {
    tmp_273_fu_3600_p1 = esl_sext<14,5>(tmp_269_reg_10183.read());
}

void mnist_fp4::thread_tmp_274_fu_9069_p2() {
    tmp_274_fu_9069_p2 = (tmp_270_fu_9065_p2.read() & tmp_271_reg_11698.read());
}

void mnist_fp4::thread_tmp_275_fu_8857_p2() {
    tmp_275_fu_8857_p2 = (!p_Result_28_fu_8847_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_28_fu_8847_p4.read() != ap_const_lv8_9E);
}

void mnist_fp4::thread_tmp_276_fu_3610_p1() {
    tmp_276_fu_3610_p1 = p_v_fu_3603_p3.read().range(2-1, 0);
}

void mnist_fp4::thread_tmp_277_fu_9118_p1() {
    tmp_277_fu_9118_p1 = esl_zext<12,11>(p_Result_24_reg_11709.read());
}

void mnist_fp4::thread_tmp_279_fu_9112_p2() {
    tmp_279_fu_9112_p2 = (!tmp_519_fu_9086_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_519_fu_9086_p1.read() == ap_const_lv63_0);
}

void mnist_fp4::thread_tmp_27_cast_fu_2685_p1() {
    tmp_27_cast_fu_2685_p1 = esl_zext<7,2>(p_12_reg_995.read());
}

void mnist_fp4::thread_tmp_27_fu_2288_p2() {
    tmp_27_fu_2288_p2 = (!p_s_reg_921.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_921.read() == ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_280_fu_9074_p1() {
    tmp_280_fu_9074_p1 = grp_fu_2049_p1.read();
}

void mnist_fp4::thread_tmp_281_fu_9151_p2() {
    tmp_281_fu_9151_p2 = (!F2_5_fu_9145_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_5_fu_9145_p2.read()) > sc_bigint<12>(ap_const_lv12_2));
}

void mnist_fp4::thread_tmp_282_cast_fu_2489_p1() {
    tmp_282_cast_fu_2489_p1 = esl_sext<64,11>(tmp_93_reg_9861.read());
}

void mnist_fp4::thread_tmp_282_fu_3620_p1() {
    tmp_282_fu_3620_p1 = p_v_fu_3603_p3.read().range(2-1, 0);
}

void mnist_fp4::thread_tmp_283_fu_8879_p3() {
    tmp_283_fu_8879_p3 = esl_concat<1,8>(is_neg_3_reg_11413.read(), p_Repl2_13_trunc_fu_8873_p2.read());
}

void mnist_fp4::thread_tmp_284_fu_8265_p1() {
    tmp_284_fu_8265_p1 = esl_zext<12,11>(exp_tmp_V_2_reg_11491.read());
}

void mnist_fp4::thread_tmp_285_fu_9157_p2() {
    tmp_285_fu_9157_p2 = (!ap_const_lv12_FFE.is_01() || !F2_5_fu_9145_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFE) + sc_biguint<12>(F2_5_fu_9145_p2.read()));
}

void mnist_fp4::thread_tmp_286_fu_8223_p2() {
    tmp_286_fu_8223_p2 = (!tmp_543_fu_8197_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_543_fu_8197_p1.read() == ap_const_lv63_0);
}

void mnist_fp4::thread_tmp_287_fu_9163_p2() {
    tmp_287_fu_9163_p2 = (!ap_const_lv12_2.is_01() || !F2_5_fu_9145_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_2) - sc_biguint<12>(F2_5_fu_9145_p2.read()));
}

void mnist_fp4::thread_tmp_288_fu_9177_p2() {
    tmp_288_fu_9177_p2 = (!F2_5_fu_9145_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): sc_lv<1>(F2_5_fu_9145_p2.read() == ap_const_lv12_2);
}

void mnist_fp4::thread_tmp_289_fu_3635_p3() {
    tmp_289_fu_3635_p3 = esl_concat<2,5>(r_V_6_reg_10193.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_28_cast_fu_2372_p1() {
    tmp_28_cast_fu_2372_p1 = esl_zext<6,5>(tmp_8_fu_2360_p3.read());
}

void mnist_fp4::thread_tmp_28_fu_2926_p2() {
    tmp_28_fu_2926_p2 = (tmp_23_fu_2922_p2.read() & tmp_24_reg_10027.read());
}

void mnist_fp4::thread_tmp_290_cast_fu_2502_p1() {
    tmp_290_cast_fu_2502_p1 = esl_sext<64,11>(tmp_94_reg_9876.read());
}

void mnist_fp4::thread_tmp_290_fu_3646_p3() {
    tmp_290_fu_3646_p3 = esl_concat<2,2>(r_V_6_reg_10193.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_291_fu_3657_p2() {
    tmp_291_fu_3657_p2 = (!p_shl32_cast_fu_3642_p1.read().is_01() || !p_shl33_cast_fu_3653_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl32_cast_fu_3642_p1.read()) - sc_biguint<8>(p_shl33_cast_fu_3653_p1.read()));
}

void mnist_fp4::thread_tmp_292_cast_fu_8834_p1() {
    tmp_292_cast_fu_8834_p1 = esl_zext<12,4>(p_44_reg_1635.read());
}

void mnist_fp4::thread_tmp_292_fu_3667_p2() {
    tmp_292_fu_3667_p2 = (!tmp_265_fu_3631_p1.read().is_01() || !tmp_417_cast_fu_3663_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_265_fu_3631_p1.read()) + sc_bigint<9>(tmp_417_cast_fu_3663_p1.read()));
}

void mnist_fp4::thread_tmp_293_fu_8268_p3() {
    tmp_293_fu_8268_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_545_reg_11496.read());
}

void mnist_fp4::thread_tmp_294_fu_3673_p1() {
    tmp_294_fu_3673_p1 = tmp_292_fu_3667_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_295_fu_3687_p3() {
    tmp_295_fu_3687_p3 = esl_concat<9,2>(tmp_292_reg_10199.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_296_fu_9206_p2() {
    tmp_296_fu_9206_p2 = (!sh_amt_5_reg_11736.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_5_reg_11736.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp4::thread_tmp_297_fu_3698_p2() {
    tmp_297_fu_3698_p2 = (!p_shl30_cast_fu_3680_p3.read().is_01() || !p_shl31_cast_fu_3694_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl30_cast_fu_3680_p3.read()) - sc_bigint<13>(p_shl31_cast_fu_3694_p1.read()));
}

void mnist_fp4::thread_tmp_298_cast_fu_3261_p1() {
    tmp_298_cast_fu_3261_p1 = esl_sext<10,9>(tmp_98_fu_3255_p2.read());
}

void mnist_fp4::thread_tmp_298_fu_3704_p2() {
    tmp_298_fu_3704_p2 = (!tmp_38_cast_fu_3677_p1.read().is_01() || !tmp_297_fu_3698_p2.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_38_cast_fu_3677_p1.read()) + sc_biguint<13>(tmp_297_fu_3698_p2.read()));
}

void mnist_fp4::thread_tmp_299_fu_6069_p3() {
    tmp_299_fu_6069_p3 = esl_concat<3,4>(p_13_reg_1356.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_29_cast_fu_3989_p1() {
    tmp_29_cast_fu_3989_p1 = esl_zext<10,5>(p_10_reg_1150.read());
}

void mnist_fp4::thread_tmp_29_fu_2294_p2() {
    tmp_29_fu_2294_p2 = (tmp_27_fu_2288_p2.read() | tmp_26_fu_2282_p2.read());
}

void mnist_fp4::thread_tmp_2_fu_2147_p2() {
    tmp_2_fu_2147_p2 = (!ap_phi_mux_p_1_phi_fu_819_p4.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_1_phi_fu_819_p4.read() == ap_const_lv5_1C);
}

void mnist_fp4::thread_tmp_300_fu_9211_p1() {
    tmp_300_fu_9211_p1 = esl_zext<54,32>(sh_amt_5_cast_fu_9203_p1.read());
}

void mnist_fp4::thread_tmp_301_fu_9215_p2() {
    tmp_301_fu_9215_p2 = (!man_V_16_reg_11725.read().is_01() || !tmp_300_fu_9211_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_16_reg_11725.read()) >> (unsigned short)tmp_300_fu_9211_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_302_fu_3772_p1() {
    tmp_302_fu_3772_p1 = msb_idx_fu_3766_p2.read().range(31-1, 0);
}

void mnist_fp4::thread_tmp_303_fu_9231_p1() {
    tmp_303_fu_9231_p1 = esl_sext<32,4>(tmp_522_reg_11748.read());
}

void mnist_fp4::thread_tmp_304_fu_9234_p2() {
    tmp_304_fu_9234_p2 = (!sh_amt_5_cast_fu_9203_p1.read().is_01())? sc_lv<32>(): tmp_303_fu_9231_p1.read() << (unsigned short)sh_amt_5_cast_fu_9203_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_306_fu_3794_p4() {
    tmp_306_fu_3794_p4 = msb_idx_1_fu_3784_p3.read().range(30, 5);
}

void mnist_fp4::thread_tmp_307_fu_3825_p1() {
    tmp_307_fu_3825_p1 = msb_idx_1_fu_3784_p3.read().range(2-1, 0);
}

void mnist_fp4::thread_tmp_308_fu_8298_p2() {
    tmp_308_fu_8298_p2 = (!F2_6_fu_8292_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_6_fu_8292_p2.read()) > sc_bigint<12>(ap_const_lv12_2));
}

void mnist_fp4::thread_tmp_309_fu_8304_p2() {
    tmp_309_fu_8304_p2 = (!ap_const_lv12_FFE.is_01() || !F2_6_fu_8292_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFE) + sc_biguint<12>(F2_6_fu_8292_p2.read()));
}

void mnist_fp4::thread_tmp_310_fu_8310_p2() {
    tmp_310_fu_8310_p2 = (!ap_const_lv12_2.is_01() || !F2_6_fu_8292_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_2) - sc_biguint<12>(F2_6_fu_8292_p2.read()));
}

void mnist_fp4::thread_tmp_311_fu_8324_p2() {
    tmp_311_fu_8324_p2 = (!F2_6_fu_8292_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): sc_lv<1>(F2_6_fu_8292_p2.read() == ap_const_lv12_2);
}

void mnist_fp4::thread_tmp_312_fu_3829_p2() {
    tmp_312_fu_3829_p2 = (!ap_const_lv2_1.is_01() || !tmp_307_fu_3825_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(tmp_307_fu_3825_p1.read()));
}

void mnist_fp4::thread_tmp_313_fu_3835_p1() {
    tmp_313_fu_3835_p1 = esl_zext<4,2>(tmp_312_fu_3829_p2.read());
}

void mnist_fp4::thread_tmp_314_fu_5580_p2() {
    tmp_314_fu_5580_p2 = (!lhs_V_5_cast_fu_5576_p1.read().is_01() || !tmp_399_cast_reg_10697.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_5_cast_fu_5576_p1.read()) + sc_bigint<9>(tmp_399_cast_reg_10697.read()));
}

void mnist_fp4::thread_tmp_315_fu_3876_p1() {
    tmp_315_fu_3876_p1 = msb_idx_reg_10236.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_316_fu_5585_p1() {
    tmp_316_fu_5585_p1 = tmp_314_fu_5580_p2.read().range(7-1, 0);
}

void mnist_fp4::thread_tmp_317_fu_8438_p2() {
    tmp_317_fu_8438_p2 = (!sh_amt_6_reg_11540.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_6_reg_11540.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp4::thread_tmp_318_fu_5597_p3() {
    tmp_318_fu_5597_p3 = esl_concat<9,1>(tmp_314_fu_5580_p2.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_319_fu_5609_p2() {
    tmp_319_fu_5609_p2 = (!p_shl46_cast_fu_5589_p3.read().is_01() || !p_shl47_cast_fu_5605_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl46_cast_fu_5589_p3.read()) - sc_bigint<11>(p_shl47_cast_fu_5605_p1.read()));
}

void mnist_fp4::thread_tmp_31_fu_2931_p1() {
    tmp_31_fu_2931_p1 = grp_fu_2049_p1.read();
}

void mnist_fp4::thread_tmp_320_fu_8443_p1() {
    tmp_320_fu_8443_p1 = esl_zext<54,32>(sh_amt_6_cast_fu_8435_p1.read());
}

void mnist_fp4::thread_tmp_321_fu_8447_p2() {
    tmp_321_fu_8447_p2 = (!man_V_12_reg_11529.read().is_01() || !tmp_320_fu_8443_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_12_reg_11529.read()) >> (unsigned short)tmp_320_fu_8443_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_322_fu_5158_p2() {
    tmp_322_fu_5158_p2 = (!tmp_77_cast_fu_5154_p1.read().is_01() || !tmp_252_reg_10578.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_77_cast_fu_5154_p1.read()) + sc_biguint<13>(tmp_252_reg_10578.read()));
}

void mnist_fp4::thread_tmp_323_fu_8463_p1() {
    tmp_323_fu_8463_p1 = esl_sext<32,4>(tmp_546_reg_11552.read());
}

void mnist_fp4::thread_tmp_324_fu_8466_p2() {
    tmp_324_fu_8466_p2 = (!sh_amt_6_cast_fu_8435_p1.read().is_01())? sc_lv<32>(): tmp_323_fu_8463_p1.read() << (unsigned short)sh_amt_6_cast_fu_8435_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_325_fu_5181_p1() {
    tmp_325_fu_5181_p1 = conv2_0_load_to_int_fu_5168_p1.read().range(23-1, 0);
}

void mnist_fp4::thread_tmp_326_fu_5218_p1() {
    tmp_326_fu_5218_p1 = ireg_V_3_fu_5210_p3.read().range(63-1, 0);
}

void mnist_fp4::thread_tmp_327_cast1_fu_3947_p1() {
    tmp_327_cast1_fu_3947_p1 = esl_zext<6,5>(tmp_110_fu_3939_p3.read());
}

void mnist_fp4::thread_tmp_327_cast_fu_3951_p1() {
    tmp_327_cast_fu_3951_p1 = esl_zext<9,5>(tmp_110_fu_3939_p3.read());
}

void mnist_fp4::thread_tmp_328_fu_5240_p1() {
    tmp_328_fu_5240_p1 = ireg_V_3_fu_5210_p3.read().range(52-1, 0);
}

void mnist_fp4::thread_tmp_329_fu_5315_p1() {
    tmp_329_fu_5315_p1 = man_V_6_fu_5270_p3.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_32_fu_2975_p1() {
    tmp_32_fu_2975_p1 = esl_zext<12,11>(p_Result_2_reg_10038.read());
}

void mnist_fp4::thread_tmp_330_fu_5319_p4() {
    tmp_330_fu_5319_p4 = sh_amt_1_fu_5301_p3.read().range(11, 2);
}

void mnist_fp4::thread_tmp_331_cast_fu_8179_p1() {
    tmp_331_cast_fu_8179_p1 = esl_zext<11,2>(p_66_reg_1706.read());
}

void mnist_fp4::thread_tmp_331_fu_5352_p1() {
    tmp_331_fu_5352_p1 = tmp_145_fu_5347_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_332_fu_5372_p1() {
    tmp_332_fu_5372_p1 = tmp_153_fu_5366_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_333_fu_4970_p2() {
    tmp_333_fu_4970_p2 = (!tmp_190_reg_10302.read().is_01() || !tmp_127_cast_fu_4966_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_190_reg_10302.read()) + sc_biguint<13>(tmp_127_cast_fu_4966_p1.read()));
}

void mnist_fp4::thread_tmp_334_fu_8350_p1() {
    tmp_334_fu_8350_p1 = esl_zext<12,11>(exp_tmp_V_3_reg_11513.read());
}

void mnist_fp4::thread_tmp_335_fu_8353_p3() {
    tmp_335_fu_8353_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_552_reg_11518.read());
}

void mnist_fp4::thread_tmp_336_fu_8259_p2() {
    tmp_336_fu_8259_p2 = (!tmp_550_fu_8233_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_550_fu_8233_p1.read() == ap_const_lv63_0);
}

void mnist_fp4::thread_tmp_337_fu_8383_p2() {
    tmp_337_fu_8383_p2 = (!F2_7_fu_8377_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_7_fu_8377_p2.read()) > sc_bigint<12>(ap_const_lv12_2));
}

void mnist_fp4::thread_tmp_338_fu_8389_p2() {
    tmp_338_fu_8389_p2 = (!ap_const_lv12_FFE.is_01() || !F2_7_fu_8377_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFE) + sc_biguint<12>(F2_7_fu_8377_p2.read()));
}

void mnist_fp4::thread_tmp_339_fu_8395_p2() {
    tmp_339_fu_8395_p2 = (!ap_const_lv12_2.is_01() || !F2_7_fu_8377_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_2) - sc_biguint<12>(F2_7_fu_8377_p2.read()));
}

void mnist_fp4::thread_tmp_33_fu_2978_p3() {
    tmp_33_fu_2978_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_146_reg_10043.read());
}

void mnist_fp4::thread_tmp_340_fu_8409_p2() {
    tmp_340_fu_8409_p2 = (!F2_7_fu_8377_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): sc_lv<1>(F2_7_fu_8377_p2.read() == ap_const_lv12_2);
}

void mnist_fp4::thread_tmp_341_fu_4171_p1() {
    tmp_341_fu_4171_p1 = msb_idx_2_fu_4165_p2.read().range(31-1, 0);
}

void mnist_fp4::thread_tmp_342_fu_8587_p2() {
    tmp_342_fu_8587_p2 = (!sh_amt_7_reg_11574.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_7_reg_11574.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp4::thread_tmp_344_fu_8592_p1() {
    tmp_344_fu_8592_p1 = esl_zext<54,32>(sh_amt_7_cast_fu_8584_p1.read());
}

void mnist_fp4::thread_tmp_345_fu_8596_p2() {
    tmp_345_fu_8596_p2 = (!man_V_15_reg_11563.read().is_01() || !tmp_344_fu_8592_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_15_reg_11563.read()) >> (unsigned short)tmp_344_fu_8592_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_346_fu_4904_p4() {
    tmp_346_fu_4904_p4 = msb_idx_3_fu_4894_p3.read().range(30, 5);
}

void mnist_fp4::thread_tmp_347_fu_8612_p1() {
    tmp_347_fu_8612_p1 = esl_sext<32,4>(tmp_553_reg_11586.read());
}

void mnist_fp4::thread_tmp_348_fu_8615_p2() {
    tmp_348_fu_8615_p2 = (!sh_amt_7_cast_fu_8584_p1.read().is_01())? sc_lv<32>(): tmp_347_fu_8612_p1.read() << (unsigned short)sh_amt_7_cast_fu_8584_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_349_fu_4935_p1() {
    tmp_349_fu_4935_p1 = msb_idx_3_fu_4894_p3.read().range(2-1, 0);
}

void mnist_fp4::thread_tmp_34_cast_fu_2742_p1() {
    tmp_34_cast_fu_2742_p1 = esl_zext<11,5>(r_V_2_fu_2736_p2.read());
}

void mnist_fp4::thread_tmp_34_fu_2182_p2() {
    tmp_34_fu_2182_p2 = (!tmp_6_reg_9767.read().is_01() || !tmp_6_cast_fu_2178_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_6_reg_9767.read()) + sc_biguint<11>(tmp_6_cast_fu_2178_p1.read()));
}

void mnist_fp4::thread_tmp_350_cast_fu_3973_p1() {
    tmp_350_cast_fu_3973_p1 = esl_sext<10,9>(tmp_116_fu_3967_p2.read());
}

void mnist_fp4::thread_tmp_350_fu_4939_p2() {
    tmp_350_fu_4939_p2 = (!ap_const_lv2_1.is_01() || !tmp_349_fu_4935_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(tmp_349_fu_4935_p1.read()));
}

void mnist_fp4::thread_tmp_351_fu_4945_p1() {
    tmp_351_fu_4945_p1 = esl_zext<4,2>(tmp_350_fu_4939_p2.read());
}

void mnist_fp4::thread_tmp_352_fu_4056_p2() {
    tmp_352_fu_4056_p2 = (!tmp_327_cast1_reg_10284.read().is_01() || !tmp_64_cast_fu_4052_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_327_cast1_reg_10284.read()) + sc_biguint<6>(tmp_64_cast_fu_4052_p1.read()));
}

void mnist_fp4::thread_tmp_353_fu_4995_p1() {
    tmp_353_fu_4995_p1 = msb_idx_2_reg_10344.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_354_fu_4065_p3() {
    tmp_354_fu_4065_p3 = esl_concat<6,2>(tmp_352_fu_4056_p2.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_355_fu_4077_p2() {
    tmp_355_fu_4077_p2 = (!p_shl_fu_4073_p1.read().is_01() || !tmp_431_cast_fu_4061_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl_fu_4073_p1.read()) - sc_biguint<64>(tmp_431_cast_fu_4061_p1.read()));
}

void mnist_fp4::thread_tmp_356_fu_4083_p3() {
    tmp_356_fu_4083_p3 = esl_concat<3,5>(p_24_reg_1186.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_357_fu_4095_p3() {
    tmp_357_fu_4095_p3 = esl_concat<3,1>(p_24_reg_1186.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_358_fu_4107_p2() {
    tmp_358_fu_4107_p2 = (!p_shl49_cast_fu_4091_p1.read().is_01() || !p_shl50_cast_fu_4103_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl49_cast_fu_4091_p1.read()) - sc_biguint<9>(p_shl50_cast_fu_4103_p1.read()));
}

void mnist_fp4::thread_tmp_359_fu_3918_p2() {
    tmp_359_fu_3918_p2 = (!tmp_134_reg_10124.read().is_01() || !tmp_93_cast_fu_3914_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_134_reg_10124.read()) + sc_biguint<13>(tmp_93_cast_fu_3914_p1.read()));
}

void mnist_fp4::thread_tmp_35_cast_fu_2756_p1() {
    tmp_35_cast_fu_2756_p1 = esl_zext<7,2>(p_17_reg_1019.read());
}

void mnist_fp4::thread_tmp_35_fu_2300_p2() {
    tmp_35_fu_2300_p2 = (!p_3_reg_933.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_933.read() == ap_const_lv5_1F);
}

void mnist_fp4::thread_tmp_360_fu_6505_p3() {
    tmp_360_fu_6505_p3 = esl_concat<4,2>(p_15_reg_1413.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_361_fu_6517_p3() {
    tmp_361_fu_6517_p3 = esl_concat<4,4>(p_15_reg_1413.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_362_fu_6529_p3() {
    tmp_362_fu_6529_p3 = esl_concat<4,1>(p_15_reg_1413.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_363_fu_6541_p2() {
    tmp_363_fu_6541_p2 = (!p_shl51_cast_fu_6525_p1.read().is_01() || !p_shl52_cast_fu_6537_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl51_cast_fu_6525_p1.read()) - sc_biguint<9>(p_shl52_cast_fu_6537_p1.read()));
}

void mnist_fp4::thread_tmp_364_fu_6109_p2() {
    tmp_364_fu_6109_p2 = (!lhs_V_1_cast_fu_6105_p1.read().is_01() || !tmp_424_cast_reg_10848.read().is_01())? sc_lv<8>(): (sc_biguint<8>(lhs_V_1_cast_fu_6105_p1.read()) + sc_biguint<8>(tmp_424_cast_reg_10848.read()));
}

void mnist_fp4::thread_tmp_365_fu_6122_p1() {
    tmp_365_fu_6122_p1 = p_20_reg_1378.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_366_fu_6138_p2() {
    tmp_366_fu_6138_p2 = (!ap_const_lv5_1.is_01())? sc_lv<5>(): p_20_reg_1378.read() << (unsigned short)ap_const_lv5_1.to_uint();
}

void mnist_fp4::thread_tmp_367_fu_5639_p2() {
    tmp_367_fu_5639_p2 = (!lhs_V_7_cast_fu_5635_p1.read().is_01() || !tmp_319_reg_10710.read().is_01())? sc_lv<11>(): (sc_biguint<11>(lhs_V_7_cast_fu_5635_p1.read()) + sc_biguint<11>(tmp_319_reg_10710.read()));
}

void mnist_fp4::thread_tmp_368_cast_fu_2888_p1() {
    tmp_368_cast_fu_2888_p1 = esl_zext<64,13>(tmp_137_fu_2883_p2.read());
}

void mnist_fp4::thread_tmp_368_fu_4209_p2() {
    tmp_368_fu_4209_p2 = (!tmp_436_cast_reg_10328.read().is_01() || !tmp_80_cast_fu_4205_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_436_cast_reg_10328.read()) + sc_biguint<10>(tmp_80_cast_fu_4205_p1.read()));
}

void mnist_fp4::thread_tmp_369_cast_fu_2715_p1() {
    tmp_369_cast_fu_2715_p1 = esl_zext<64,13>(tmp_158_fu_2710_p2.read());
}

void mnist_fp4::thread_tmp_369_fu_4214_p1() {
    tmp_369_fu_4214_p1 = tmp_368_fu_4209_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_370_fu_4226_p3() {
    tmp_370_fu_4226_p3 = esl_concat<10,1>(tmp_368_fu_4209_p2.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_371_fu_4238_p2() {
    tmp_371_fu_4238_p2 = (!p_shl53_cast_fu_4218_p3.read().is_01() || !p_shl55_cast_fu_4234_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl53_cast_fu_4218_p3.read()) - sc_bigint<13>(p_shl55_cast_fu_4234_p1.read()));
}

void mnist_fp4::thread_tmp_372_fu_4248_p2() {
    tmp_372_fu_4248_p2 = (!tmp_355_reg_10323.read().is_01() || !tmp_81_fu_4244_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_355_reg_10323.read()) + sc_biguint<64>(tmp_81_fu_4244_p1.read()));
}

void mnist_fp4::thread_tmp_373_fu_4253_p1() {
    tmp_373_fu_4253_p1 = tmp_372_fu_4248_p2.read().range(9-1, 0);
}

void mnist_fp4::thread_tmp_374_fu_4257_p1() {
    tmp_374_fu_4257_p1 = tmp_372_fu_4248_p2.read().range(7-1, 0);
}

void mnist_fp4::thread_tmp_375_fu_4269_p2() {
    tmp_375_fu_4269_p2 = (!p_shl54_cast_fu_4261_p3.read().is_01() || !tmp_373_fu_4253_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl54_cast_fu_4261_p3.read()) - sc_biguint<9>(tmp_373_fu_4253_p1.read()));
}

void mnist_fp4::thread_tmp_376_fu_6786_p3() {
    tmp_376_fu_6786_p3 = esl_concat<4,4>(p_36_reg_1519.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_377_fu_6798_p3() {
    tmp_377_fu_6798_p3 = esl_concat<4,1>(p_36_reg_1519.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_378_fu_6810_p2() {
    tmp_378_fu_6810_p2 = (!p_shl56_cast_fu_6794_p1.read().is_01() || !p_shl57_cast_fu_6806_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl56_cast_fu_6794_p1.read()) - sc_biguint<9>(p_shl57_cast_fu_6806_p1.read()));
}

void mnist_fp4::thread_tmp_379_fu_6567_p2() {
    tmp_379_fu_6567_p2 = (!tmp_442_cast_reg_10975.read().is_01() || !tmp_66_cast_fu_6563_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_442_cast_reg_10975.read()) + sc_biguint<10>(tmp_66_cast_fu_6563_p1.read()));
}

void mnist_fp4::thread_tmp_37_fu_3457_p3() {
    tmp_37_fu_3457_p3 = (!tmp_253_fu_3445_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_253_fu_3445_p2.read()[0].to_bool())? r_V_4_fu_3439_p2.read(): tmp_254_fu_3451_p2.read());
}

void mnist_fp4::thread_tmp_380_fu_6572_p1() {
    tmp_380_fu_6572_p1 = tmp_379_fu_6567_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_381_fu_6584_p3() {
    tmp_381_fu_6584_p3 = esl_concat<10,1>(tmp_379_fu_6567_p2.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_382_fu_6596_p2() {
    tmp_382_fu_6596_p2 = (!p_shl58_cast_fu_6576_p3.read().is_01() || !p_shl59_cast_fu_6592_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl58_cast_fu_6576_p3.read()) - sc_bigint<12>(p_shl59_cast_fu_6592_p1.read()));
}

void mnist_fp4::thread_tmp_383_fu_5682_p2() {
    tmp_383_fu_5682_p2 = (!tmp_396_cast_reg_10692.read().is_01() || !tmp_136_cast_cast_fu_5678_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_396_cast_reg_10692.read()) + sc_biguint<10>(tmp_136_cast_cast_fu_5678_p1.read()));
}

void mnist_fp4::thread_tmp_384_cast_fu_5087_p1() {
    tmp_384_cast_fu_5087_p1 = esl_sext<10,9>(tmp_183_fu_5081_p2.read());
}

void mnist_fp4::thread_tmp_384_fu_5687_p1() {
    tmp_384_fu_5687_p1 = tmp_383_fu_5682_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_385_fu_5699_p3() {
    tmp_385_fu_5699_p3 = esl_concat<10,2>(tmp_383_fu_5682_p2.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_386_fu_5711_p2() {
    tmp_386_fu_5711_p2 = (!p_shl60_cast_fu_5691_p3.read().is_01() || !p_shl61_cast_fu_5707_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl60_cast_fu_5691_p3.read()) - sc_bigint<13>(p_shl61_cast_fu_5707_p1.read()));
}

void mnist_fp4::thread_tmp_387_fu_4301_p2() {
    tmp_387_fu_4301_p2 = (!tmp_104_cast_fu_4297_p1.read().is_01() || !tmp_371_reg_10367.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_104_cast_fu_4297_p1.read()) + sc_biguint<13>(tmp_371_reg_10367.read()));
}

void mnist_fp4::thread_tmp_388_fu_4329_p1() {
    tmp_388_fu_4329_p1 = ireg_V_8_fu_4325_p1.read().range(63-1, 0);
}

void mnist_fp4::thread_tmp_389_cast_fu_2751_p1() {
    tmp_389_cast_fu_2751_p1 = esl_sext<64,11>(tmp_203_fu_2746_p2.read());
}

void mnist_fp4::thread_tmp_389_fu_4315_p2() {
    tmp_389_fu_4315_p2 = (!tmp_169_cast_fu_4311_p1.read().is_01() || !tmp_375_reg_10372.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_169_cast_fu_4311_p1.read()) + sc_biguint<9>(tmp_375_reg_10372.read()));
}

void mnist_fp4::thread_tmp_38_cast_fu_3677_p1() {
    tmp_38_cast_fu_3677_p1 = esl_zext<13,5>(tmp_37_reg_10150.read());
}

void mnist_fp4::thread_tmp_38_fu_2306_p2() {
    tmp_38_fu_2306_p2 = (!p_3_reg_933.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_933.read() == ap_const_lv5_1E);
}

void mnist_fp4::thread_tmp_390_cast_fu_2765_p1() {
    tmp_390_cast_fu_2765_p1 = esl_zext<64,7>(tmp_204_fu_2760_p2.read());
}

void mnist_fp4::thread_tmp_390_fu_4351_p1() {
    tmp_390_fu_4351_p1 = ireg_V_8_fu_4325_p1.read().range(52-1, 0);
}

void mnist_fp4::thread_tmp_391_fu_4462_p1() {
    tmp_391_fu_4462_p1 = man_V_4_fu_4417_p3.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_392_fu_4466_p4() {
    tmp_392_fu_4466_p4 = sh_amt_2_fu_4448_p3.read().range(11, 2);
}

void mnist_fp4::thread_tmp_393_fu_4584_p1() {
    tmp_393_fu_4584_p1 = tmp_163_fu_4579_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_394_fu_4604_p1() {
    tmp_394_fu_4604_p1 = tmp_166_fu_4598_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_395_fu_4365_p1() {
    tmp_395_fu_4365_p1 = ireg_V_9_fu_4361_p1.read().range(63-1, 0);
}

void mnist_fp4::thread_tmp_396_cast_fu_5526_p1() {
    tmp_396_cast_fu_5526_p1 = esl_sext<10,9>(tmp_229_fu_5520_p2.read());
}

void mnist_fp4::thread_tmp_396_fu_6214_p2() {
    tmp_396_fu_6214_p2 = (!r_V_11_fu_6208_p2.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): (sc_biguint<4>(r_V_11_fu_6208_p2.read()) < sc_biguint<4>(ap_const_lv4_E));
}

void mnist_fp4::thread_tmp_397_fu_4387_p1() {
    tmp_397_fu_4387_p1 = ireg_V_9_fu_4361_p1.read().range(52-1, 0);
}

void mnist_fp4::thread_tmp_398_fu_4547_p1() {
    tmp_398_fu_4547_p1 = man_V_9_fu_4502_p3.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_399_cast_fu_5560_p1() {
    tmp_399_cast_fu_5560_p1 = esl_sext<9,8>(tmp_239_fu_5554_p2.read());
}

void mnist_fp4::thread_tmp_399_fu_4551_p4() {
    tmp_399_fu_4551_p4 = sh_amt_3_fu_4533_p3.read().range(11, 2);
}

void mnist_fp4::thread_tmp_39_fu_2969_p2() {
    tmp_39_fu_2969_p2 = (!tmp_139_fu_2943_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_139_fu_2943_p1.read() == ap_const_lv63_0);
}

void mnist_fp4::thread_tmp_400_fu_4733_p1() {
    tmp_400_fu_4733_p1 = tmp_185_fu_4728_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_401_fu_4753_p1() {
    tmp_401_fu_4753_p1 = tmp_199_fu_4747_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_402_fu_6204_p1() {
    tmp_402_fu_6204_p1 = p_30_reg_1389.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_403_fu_6220_p2() {
    tmp_403_fu_6220_p2 = (!ap_const_lv4_1.is_01() || !tmp_402_fu_6204_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_402_fu_6204_p1.read()));
}

void mnist_fp4::thread_tmp_404_fu_6266_p1() {
    tmp_404_fu_6266_p1 = mul3_fu_9697_p2.read().range(22-1, 0);
}

void mnist_fp4::thread_tmp_406_fu_6303_p4() {
    tmp_406_fu_6303_p4 = neg_mul3_fu_6298_p2.read().range(21, 15);
}

void mnist_fp4::thread_tmp_407_fu_6313_p1() {
    tmp_407_fu_6313_p1 = esl_sext<11,7>(tmp_406_fu_6303_p4.read());
}

void mnist_fp4::thread_tmp_409_fu_6317_p1() {
    tmp_409_fu_6317_p1 = esl_sext<11,9>(tmp_408_reg_10911.read());
}

void mnist_fp4::thread_tmp_40_fu_2312_p2() {
    tmp_40_fu_2312_p2 = (tmp_38_fu_2306_p2.read() | tmp_35_fu_2300_p2.read());
}

void mnist_fp4::thread_tmp_410_fu_6320_p3() {
    tmp_410_fu_6320_p3 = (!tmp_405_reg_10903.read()[0].is_01())? sc_lv<11>(): ((tmp_405_reg_10903.read()[0].to_bool())? tmp_407_fu_6313_p1.read(): tmp_409_fu_6317_p1.read());
}

void mnist_fp4::thread_tmp_411_fu_6396_p1() {
    tmp_411_fu_6396_p1 = grp_fu_6340_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_412_fu_6286_p1() {
    tmp_412_fu_6286_p1 = mul4_fu_9705_p2.read().range(23-1, 0);
}

void mnist_fp4::thread_tmp_413_fu_6351_p4() {
    tmp_413_fu_6351_p4 = neg_mul4_fu_6346_p2.read().range(22, 19);
}

void mnist_fp4::thread_tmp_414_fu_6361_p1() {
    tmp_414_fu_6361_p1 = esl_sext<12,4>(tmp_413_fu_6351_p4.read());
}

void mnist_fp4::thread_tmp_416_fu_6365_p1() {
    tmp_416_fu_6365_p1 = esl_sext<12,5>(tmp_415_reg_10921.read());
}

void mnist_fp4::thread_tmp_417_cast_fu_3663_p1() {
    tmp_417_cast_fu_3663_p1 = esl_sext<9,8>(tmp_291_fu_3657_p2.read());
}

void mnist_fp4::thread_tmp_417_fu_6375_p1() {
    tmp_417_fu_6375_p1 = p_v1_fu_6368_p3.read().range(2-1, 0);
}

void mnist_fp4::thread_tmp_418_fu_6385_p1() {
    tmp_418_fu_6385_p1 = p_v1_fu_6368_p3.read().range(2-1, 0);
}

void mnist_fp4::thread_tmp_419_fu_6400_p3() {
    tmp_419_fu_6400_p3 = esl_concat<2,4>(r_V_12_reg_10931.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_41_fu_2318_p2() {
    tmp_41_fu_2318_p2 = (!p_3_reg_933.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_933.read() == ap_const_lv5_1D);
}

void mnist_fp4::thread_tmp_420_fu_6411_p3() {
    tmp_420_fu_6411_p3 = esl_concat<2,1>(r_V_12_reg_10931.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_421_fu_6422_p2() {
    tmp_421_fu_6422_p2 = (!p_shl62_cast_fu_6407_p1.read().is_01() || !p_shl63_cast_fu_6418_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl62_cast_fu_6407_p1.read()) - sc_biguint<7>(p_shl63_cast_fu_6418_p1.read()));
}

void mnist_fp4::thread_tmp_422_cast_fu_3710_p1() {
    tmp_422_cast_fu_3710_p1 = esl_zext<64,13>(tmp_298_fu_3704_p2.read());
}

void mnist_fp4::thread_tmp_422_fu_6432_p2() {
    tmp_422_fu_6432_p2 = (!tmp_411_fu_6396_p1.read().is_01() || !tmp_482_cast_fu_6428_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_411_fu_6396_p1.read()) + sc_bigint<8>(tmp_482_cast_fu_6428_p1.read()));
}

void mnist_fp4::thread_tmp_423_fu_6438_p1() {
    tmp_423_fu_6438_p1 = tmp_422_fu_6432_p2.read().range(7-1, 0);
}

void mnist_fp4::thread_tmp_424_cast_fu_6077_p1() {
    tmp_424_cast_fu_6077_p1 = esl_zext<8,7>(tmp_299_fu_6069_p3.read());
}

void mnist_fp4::thread_tmp_424_fu_6452_p3() {
    tmp_424_fu_6452_p3 = esl_concat<8,1>(tmp_422_reg_10937.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_425_fu_6463_p2() {
    tmp_425_fu_6463_p2 = (!p_shl64_cast_fu_6445_p3.read().is_01() || !p_shl65_cast_fu_6459_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl64_cast_fu_6445_p3.read()) - sc_bigint<11>(p_shl65_cast_fu_6459_p1.read()));
}

void mnist_fp4::thread_tmp_426_fu_6469_p2() {
    tmp_426_fu_6469_p2 = (!tmp_88_cast_fu_6442_p1.read().is_01() || !tmp_425_fu_6463_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_88_cast_fu_6442_p1.read()) + sc_biguint<11>(tmp_425_fu_6463_p2.read()));
}

void mnist_fp4::thread_tmp_427_fu_6484_p2() {
    tmp_427_fu_6484_p2 = (!tmp_445_cast_reg_10861.read().is_01() || !tmp_110_cast_fu_6480_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_445_cast_reg_10861.read()) + sc_biguint<12>(tmp_110_cast_fu_6480_p1.read()));
}

void mnist_fp4::thread_tmp_428_fu_7235_p3() {
    tmp_428_fu_7235_p3 = esl_concat<4,4>(p_21_reg_1552.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_429_cast_fu_5163_p1() {
    tmp_429_cast_fu_5163_p1 = esl_zext<64,13>(tmp_322_fu_5158_p2.read());
}

void mnist_fp4::thread_tmp_429_fu_6836_p2() {
    tmp_429_fu_6836_p2 = (!tmp_458_cast_reg_11068.read().is_01() || !tmp_150_cast_fu_6832_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_458_cast_reg_11068.read()) + sc_biguint<10>(tmp_150_cast_fu_6832_p1.read()));
}

void mnist_fp4::thread_tmp_42_fu_2324_p2() {
    tmp_42_fu_2324_p2 = (tmp_41_fu_2318_p2.read() | tmp_40_fu_2312_p2.read());
}

void mnist_fp4::thread_tmp_430_cast_fu_5041_p1() {
    tmp_430_cast_fu_5041_p1 = esl_zext<64,13>(tmp_333_reg_10542.read());
}

void mnist_fp4::thread_tmp_430_fu_6841_p1() {
    tmp_430_fu_6841_p1 = tmp_429_fu_6836_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_431_cast_fu_4061_p1() {
    tmp_431_cast_fu_4061_p1 = esl_zext<64,6>(tmp_352_fu_4056_p2.read());
}

void mnist_fp4::thread_tmp_431_fu_6853_p3() {
    tmp_431_fu_6853_p3 = esl_concat<10,1>(tmp_429_fu_6836_p2.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_432_fu_6865_p2() {
    tmp_432_fu_6865_p2 = (!p_shl66_cast_fu_6845_p3.read().is_01() || !p_shl67_cast_fu_6861_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl66_cast_fu_6845_p3.read()) - sc_bigint<12>(p_shl67_cast_fu_6861_p1.read()));
}

void mnist_fp4::thread_tmp_433_fu_5742_p2() {
    tmp_433_fu_5742_p2 = (!tmp_386_reg_10746.read().is_01() || !tmp_154_cast_cast_fu_5738_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_386_reg_10746.read()) + sc_biguint<13>(tmp_154_cast_cast_fu_5738_p1.read()));
}

void mnist_fp4::thread_tmp_435_fu_5808_p1() {
    tmp_435_fu_5808_p1 = msb_idx_4_fu_5802_p2.read().range(31-1, 0);
}

void mnist_fp4::thread_tmp_436_cast_fu_4113_p1() {
    tmp_436_cast_fu_4113_p1 = esl_sext<10,9>(tmp_358_fu_4107_p2.read());
}

void mnist_fp4::thread_tmp_437_cast_fu_3923_p1() {
    tmp_437_cast_fu_3923_p1 = esl_zext<64,13>(tmp_359_reg_10271.read());
}

void mnist_fp4::thread_tmp_437_fu_5830_p4() {
    tmp_437_fu_5830_p4 = msb_idx_5_fu_5820_p3.read().range(30, 5);
}

void mnist_fp4::thread_tmp_438_fu_5861_p1() {
    tmp_438_fu_5861_p1 = msb_idx_5_fu_5820_p3.read().range(2-1, 0);
}

void mnist_fp4::thread_tmp_439_cast_fu_6513_p1() {
    tmp_439_cast_fu_6513_p1 = esl_zext<7,6>(tmp_360_fu_6505_p3.read());
}

void mnist_fp4::thread_tmp_439_fu_5865_p2() {
    tmp_439_fu_5865_p2 = (!ap_const_lv2_1.is_01() || !tmp_438_fu_5861_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(tmp_438_fu_5861_p1.read()));
}

void mnist_fp4::thread_tmp_43_fu_3008_p2() {
    tmp_43_fu_3008_p2 = (!F2_fu_3002_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_fu_3002_p2.read()) > sc_bigint<12>(ap_const_lv12_2));
}

void mnist_fp4::thread_tmp_440_fu_5871_p1() {
    tmp_440_fu_5871_p1 = esl_zext<4,2>(tmp_439_fu_5865_p2.read());
}

void mnist_fp4::thread_tmp_441_fu_5875_p2() {
    tmp_441_fu_5875_p2 = (!tmp_440_fu_5871_p1.read().is_01())? sc_lv<4>(): tmp_V_2_reg_10782.read() >> (unsigned short)tmp_440_fu_5871_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_442_cast_fu_6547_p1() {
    tmp_442_cast_fu_6547_p1 = esl_sext<10,9>(tmp_363_fu_6541_p2.read());
}

void mnist_fp4::thread_tmp_442_fu_5912_p1() {
    tmp_442_fu_5912_p1 = msb_idx_4_reg_10788.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_443_fu_5970_p1() {
    tmp_443_fu_5970_p1 = p_03_i1_to_int_fu_5957_p1.read().range(23-1, 0);
}

void mnist_fp4::thread_tmp_444_fu_5988_p1() {
    tmp_444_fu_5988_p1 = tmp_140_to_int_fu_5974_p1.read().range(23-1, 0);
}

void mnist_fp4::thread_tmp_445_cast_fu_6114_p3() {
    tmp_445_cast_fu_6114_p3 = esl_concat<8,4>(tmp_364_fu_6109_p2.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_445_fu_7872_p3() {
    tmp_445_fu_7872_p3 = esl_concat<4,3>(p_26_reg_1612.read(), ap_const_lv3_0);
}

void mnist_fp4::thread_tmp_446_fu_7884_p3() {
    tmp_446_fu_7884_p3 = esl_concat<4,4>(p_26_reg_1612.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_447_fu_7896_p3() {
    tmp_447_fu_7896_p3 = esl_concat<4,1>(p_26_reg_1612.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_448_cast_fu_5644_p1() {
    tmp_448_cast_fu_5644_p1 = esl_zext<64,11>(tmp_367_fu_5639_p2.read());
}

void mnist_fp4::thread_tmp_448_fu_7908_p2() {
    tmp_448_fu_7908_p2 = (!p_shl68_cast_fu_7892_p1.read().is_01() || !p_shl69_cast_fu_7904_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl68_cast_fu_7892_p1.read()) - sc_biguint<9>(p_shl69_cast_fu_7904_p1.read()));
}

void mnist_fp4::thread_tmp_449_fu_7275_p2() {
    tmp_449_fu_7275_p2 = (!lhs_V_3_cast_fu_7271_p1.read().is_01() || !tmp_490_cast_reg_11205.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_3_cast_fu_7271_p1.read()) + sc_biguint<9>(tmp_490_cast_reg_11205.read()));
}

void mnist_fp4::thread_tmp_44_fu_2330_p2() {
    tmp_44_fu_2330_p2 = (!p_3_reg_933.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_933.read() == ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_450_fu_7288_p1() {
    tmp_450_fu_7288_p1 = p_31_reg_1574.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_451_fu_7304_p2() {
    tmp_451_fu_7304_p2 = (!ap_const_lv5_1.is_01())? sc_lv<5>(): p_31_reg_1574.read() << (unsigned short)ap_const_lv5_1.to_uint();
}

void mnist_fp4::thread_tmp_452_fu_6887_p2() {
    tmp_452_fu_6887_p2 = (!tmp_184_cast_fu_6883_p1.read().is_01() || !tmp_432_reg_11081.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_184_cast_fu_6883_p1.read()) + sc_biguint<12>(tmp_432_reg_11081.read()));
}

void mnist_fp4::thread_tmp_453_fu_6910_p1() {
    tmp_453_fu_6910_p1 = conv3_0_load_to_int_fu_6897_p1.read().range(23-1, 0);
}

void mnist_fp4::thread_tmp_454_fu_6947_p1() {
    tmp_454_fu_6947_p1 = ireg_V_4_fu_6939_p3.read().range(63-1, 0);
}

void mnist_fp4::thread_tmp_456_fu_6969_p1() {
    tmp_456_fu_6969_p1 = ireg_V_4_fu_6939_p3.read().range(52-1, 0);
}

void mnist_fp4::thread_tmp_457_fu_7044_p1() {
    tmp_457_fu_7044_p1 = man_V_7_fu_6999_p3.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_458_cast_fu_6816_p1() {
    tmp_458_cast_fu_6816_p1 = esl_sext<10,9>(tmp_378_fu_6810_p2.read());
}

void mnist_fp4::thread_tmp_458_fu_7048_p4() {
    tmp_458_fu_7048_p4 = sh_amt_4_fu_7030_p3.read().range(11, 2);
}

void mnist_fp4::thread_tmp_459_fu_7081_p1() {
    tmp_459_fu_7081_p1 = tmp_248_fu_7076_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_45_cast_fu_3465_p1() {
    tmp_45_cast_fu_3465_p1 = esl_zext<6,5>(p_14_reg_1109.read());
}

void mnist_fp4::thread_tmp_45_fu_3014_p2() {
    tmp_45_fu_3014_p2 = (!ap_const_lv12_FFE.is_01() || !F2_fu_3002_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFE) + sc_biguint<12>(F2_fu_3002_p2.read()));
}

void mnist_fp4::thread_tmp_460_fu_7101_p1() {
    tmp_460_fu_7101_p1 = tmp_251_fu_7095_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_461_fu_6661_p2() {
    tmp_461_fu_6661_p2 = (!tmp_382_reg_10988.read().is_01() || !tmp_119_cast_fu_6657_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_382_reg_10988.read()) + sc_biguint<12>(tmp_119_cast_fu_6657_p1.read()));
}

void mnist_fp4::thread_tmp_462_fu_6630_p2() {
    tmp_462_fu_6630_p2 = (!tmp_120_cast_fu_6626_p1.read().is_01() || !tmp_439_cast_reg_10970.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_120_cast_fu_6626_p1.read()) + sc_biguint<7>(tmp_439_cast_reg_10970.read()));
}

void mnist_fp4::thread_tmp_463_fu_6639_p3() {
    tmp_463_fu_6639_p3 = esl_concat<7,2>(tmp_462_fu_6630_p2.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_464_fu_6651_p2() {
    tmp_464_fu_6651_p2 = (!p_shl5_fu_6647_p1.read().is_01() || !tmp_508_cast_fu_6635_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl5_fu_6647_p1.read()) - sc_biguint<64>(tmp_508_cast_fu_6635_p1.read()));
}

void mnist_fp4::thread_tmp_465_fu_8925_p3() {
    tmp_465_fu_8925_p3 = esl_concat<4,4>(p_50_reg_1717.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_466_fu_8937_p3() {
    tmp_466_fu_8937_p3 = esl_concat<4,1>(p_50_reg_1717.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_467_cast_fu_4306_p1() {
    tmp_467_cast_fu_4306_p1 = esl_zext<64,13>(tmp_387_fu_4301_p2.read());
}

void mnist_fp4::thread_tmp_467_fu_8949_p2() {
    tmp_467_fu_8949_p2 = (!p_shl71_cast_fu_8933_p1.read().is_01() || !p_shl72_cast_fu_8945_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl71_cast_fu_8933_p1.read()) - sc_biguint<9>(p_shl72_cast_fu_8945_p1.read()));
}

void mnist_fp4::thread_tmp_468_cast_fu_4320_p1() {
    tmp_468_cast_fu_4320_p1 = esl_zext<64,9>(tmp_389_fu_4315_p2.read());
}

void mnist_fp4::thread_tmp_468_fu_7934_p2() {
    tmp_468_fu_7934_p2 = (!tmp_500_cast_reg_11374.read().is_01() || !tmp_111_cast_fu_7930_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_500_cast_reg_11374.read()) + sc_biguint<10>(tmp_111_cast_fu_7930_p1.read()));
}

void mnist_fp4::thread_tmp_469_fu_7939_p1() {
    tmp_469_fu_7939_p1 = tmp_468_fu_7934_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_46_cast_fu_3469_p1() {
    tmp_46_cast_fu_3469_p1 = esl_zext<6,5>(tmp_37_fu_3457_p3.read());
}

void mnist_fp4::thread_tmp_46_fu_3020_p2() {
    tmp_46_fu_3020_p2 = (!ap_const_lv12_2.is_01() || !F2_fu_3002_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_2) - sc_biguint<12>(F2_fu_3002_p2.read()));
}

void mnist_fp4::thread_tmp_470_fu_7951_p3() {
    tmp_470_fu_7951_p3 = esl_concat<10,1>(tmp_468_fu_7934_p2.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_471_fu_7963_p2() {
    tmp_471_fu_7963_p2 = (!p_shl73_cast_fu_7943_p3.read().is_01() || !p_shl74_cast_fu_7959_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl73_cast_fu_7943_p3.read()) - sc_bigint<12>(p_shl74_cast_fu_7959_p1.read()));
}

void mnist_fp4::thread_tmp_472_fu_6697_p2() {
    tmp_472_fu_6697_p2 = (!tmp_464_reg_11009.read().is_01() || !tmp_187_fu_6693_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_464_reg_11009.read()) + sc_biguint<64>(tmp_187_fu_6693_p1.read()));
}

void mnist_fp4::thread_tmp_473_fu_6702_p1() {
    tmp_473_fu_6702_p1 = tmp_472_fu_6697_p2.read().range(10-1, 0);
}

void mnist_fp4::thread_tmp_474_fu_6706_p1() {
    tmp_474_fu_6706_p1 = tmp_472_fu_6697_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_475_fu_6718_p2() {
    tmp_475_fu_6718_p2 = (!p_shl75_cast_fu_6710_p3.read().is_01() || !tmp_473_fu_6702_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl75_cast_fu_6710_p3.read()) - sc_biguint<10>(tmp_473_fu_6702_p1.read()));
}

void mnist_fp4::thread_tmp_476_fu_7370_p1() {
    tmp_476_fu_7370_p1 = p_39_reg_1585.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_477_fu_7380_p2() {
    tmp_477_fu_7380_p2 = (!r_V_15_fu_7374_p2.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): (sc_biguint<4>(r_V_15_fu_7374_p2.read()) < sc_biguint<4>(ap_const_lv4_E));
}

void mnist_fp4::thread_tmp_478_fu_7386_p2() {
    tmp_478_fu_7386_p2 = (!ap_const_lv4_1.is_01() || !tmp_476_fu_7370_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_476_fu_7370_p1.read()));
}

void mnist_fp4::thread_tmp_479_fu_7432_p1() {
    tmp_479_fu_7432_p1 = mul5_fu_9713_p2.read().range(24-1, 0);
}

void mnist_fp4::thread_tmp_47_fu_3715_p2() {
    tmp_47_fu_3715_p2 = (!relu1_0_V_q0.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): sc_lv<1>(relu1_0_V_q0.read() == ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_481_fu_7469_p4() {
    tmp_481_fu_7469_p4 = neg_mul5_fu_7464_p2.read().range(23, 16);
}

void mnist_fp4::thread_tmp_482_cast_fu_6428_p1() {
    tmp_482_cast_fu_6428_p1 = esl_sext<8,7>(tmp_421_fu_6422_p2.read());
}

void mnist_fp4::thread_tmp_482_fu_7479_p1() {
    tmp_482_fu_7479_p1 = esl_sext<12,8>(tmp_481_fu_7469_p4.read());
}

void mnist_fp4::thread_tmp_484_fu_7483_p1() {
    tmp_484_fu_7483_p1 = esl_sext<12,10>(tmp_483_reg_11268.read());
}

void mnist_fp4::thread_tmp_485_fu_7486_p3() {
    tmp_485_fu_7486_p3 = (!tmp_480_reg_11260.read()[0].is_01())? sc_lv<12>(): ((tmp_480_reg_11260.read()[0].to_bool())? tmp_482_fu_7479_p1.read(): tmp_484_fu_7483_p1.read());
}

void mnist_fp4::thread_tmp_486_fu_7562_p1() {
    tmp_486_fu_7562_p1 = grp_fu_7506_p2.read().range(9-1, 0);
}

void mnist_fp4::thread_tmp_487_cast_fu_6475_p1() {
    tmp_487_cast_fu_6475_p1 = esl_zext<64,11>(tmp_426_fu_6469_p2.read());
}

void mnist_fp4::thread_tmp_487_fu_7452_p1() {
    tmp_487_fu_7452_p1 = mul6_fu_9721_p2.read().range(25-1, 0);
}

void mnist_fp4::thread_tmp_488_cast_fu_6489_p1() {
    tmp_488_cast_fu_6489_p1 = esl_zext<64,12>(tmp_427_reg_10957.read());
}

void mnist_fp4::thread_tmp_488_fu_7517_p4() {
    tmp_488_fu_7517_p4 = neg_mul6_fu_7512_p2.read().range(24, 20);
}

void mnist_fp4::thread_tmp_489_fu_7527_p1() {
    tmp_489_fu_7527_p1 = esl_sext<13,5>(tmp_488_fu_7517_p4.read());
}

void mnist_fp4::thread_tmp_490_cast_fu_7243_p1() {
    tmp_490_cast_fu_7243_p1 = esl_zext<9,8>(tmp_428_fu_7235_p3.read());
}

void mnist_fp4::thread_tmp_491_fu_7531_p1() {
    tmp_491_fu_7531_p1 = esl_sext<13,6>(tmp_490_reg_11278.read());
}

void mnist_fp4::thread_tmp_492_fu_7541_p1() {
    tmp_492_fu_7541_p1 = p_v2_fu_7534_p3.read().range(3-1, 0);
}

void mnist_fp4::thread_tmp_493_fu_7551_p1() {
    tmp_493_fu_7551_p1 = p_v2_fu_7534_p3.read().range(3-1, 0);
}

void mnist_fp4::thread_tmp_494_fu_7566_p3() {
    tmp_494_fu_7566_p3 = esl_concat<3,4>(r_V_16_reg_11288.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_495_cast_fu_5747_p1() {
    tmp_495_cast_fu_5747_p1 = esl_zext<64,13>(tmp_433_fu_5742_p2.read());
}

void mnist_fp4::thread_tmp_495_fu_7577_p3() {
    tmp_495_fu_7577_p3 = esl_concat<3,1>(r_V_16_reg_11288.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_496_fu_7588_p2() {
    tmp_496_fu_7588_p2 = (!p_shl76_cast_fu_7573_p1.read().is_01() || !p_shl77_cast_fu_7584_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl76_cast_fu_7573_p1.read()) - sc_biguint<8>(p_shl77_cast_fu_7584_p1.read()));
}

void mnist_fp4::thread_tmp_497_cast_fu_7880_p1() {
    tmp_497_cast_fu_7880_p1 = esl_zext<8,7>(tmp_445_fu_7872_p3.read());
}

void mnist_fp4::thread_tmp_497_fu_7598_p2() {
    tmp_497_fu_7598_p2 = (!tmp_486_fu_7562_p1.read().is_01() || !tmp_534_cast_fu_7594_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_486_fu_7562_p1.read()) + sc_bigint<9>(tmp_534_cast_fu_7594_p1.read()));
}

void mnist_fp4::thread_tmp_498_fu_7604_p1() {
    tmp_498_fu_7604_p1 = tmp_497_fu_7598_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_499_fu_7618_p3() {
    tmp_499_fu_7618_p3 = esl_concat<9,1>(tmp_497_reg_11294.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_49_cast_fu_2544_p1() {
    tmp_49_cast_fu_2544_p1 = esl_sext<7,6>(tmp_16_fu_2538_p2.read());
}

void mnist_fp4::thread_tmp_49_fu_2336_p2() {
    tmp_49_fu_2336_p2 = (tmp_44_fu_2330_p2.read() | tmp_42_fu_2324_p2.read());
}

void mnist_fp4::thread_tmp_500_cast_fu_7914_p1() {
    tmp_500_cast_fu_7914_p1 = esl_sext<10,9>(tmp_448_fu_7908_p2.read());
}

void mnist_fp4::thread_tmp_500_fu_7629_p2() {
    tmp_500_fu_7629_p2 = (!p_shl78_cast_fu_7611_p3.read().is_01() || !p_shl79_cast_fu_7625_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl78_cast_fu_7611_p3.read()) - sc_bigint<12>(p_shl79_cast_fu_7625_p1.read()));
}

void mnist_fp4::thread_tmp_501_fu_7635_p2() {
    tmp_501_fu_7635_p2 = (!tmp_148_cast_fu_7608_p1.read().is_01() || !tmp_500_fu_7629_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_148_cast_fu_7608_p1.read()) + sc_biguint<12>(tmp_500_fu_7629_p2.read()));
}

void mnist_fp4::thread_tmp_502_fu_8975_p2() {
    tmp_502_fu_8975_p2 = (!tmp_513_cast_reg_11645.read().is_01() || !tmp_239_cast_fu_8971_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_513_cast_reg_11645.read()) + sc_biguint<10>(tmp_239_cast_fu_8971_p1.read()));
}

void mnist_fp4::thread_tmp_503_cast_fu_7280_p3() {
    tmp_503_cast_fu_7280_p3 = esl_concat<9,4>(tmp_449_fu_7275_p2.read(), ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_503_fu_8980_p1() {
    tmp_503_fu_8980_p1 = tmp_502_fu_8975_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_504_fu_8992_p3() {
    tmp_504_fu_8992_p3 = esl_concat<10,1>(tmp_502_fu_8975_p2.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_505_fu_9004_p2() {
    tmp_505_fu_9004_p2 = (!p_shl83_cast_fu_8984_p3.read().is_01() || !p_shl84_cast_fu_9000_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl83_cast_fu_8984_p3.read()) - sc_bigint<12>(p_shl84_cast_fu_9000_p1.read()));
}

void mnist_fp4::thread_tmp_506_cast_fu_6892_p1() {
    tmp_506_cast_fu_6892_p1 = esl_zext<64,12>(tmp_452_fu_6887_p2.read());
}

void mnist_fp4::thread_tmp_506_fu_6746_p4() {
    tmp_506_fu_6746_p4 = esl_concat<7,4>(esl_concat<3,4>(p_40_reg_1448.read(), r_V_14_reg_11022.read()), r_V_17_fu_6740_p2.read());
}

void mnist_fp4::thread_tmp_507_cast_fu_6666_p1() {
    tmp_507_cast_fu_6666_p1 = esl_zext<64,12>(tmp_461_fu_6661_p2.read());
}

void mnist_fp4::thread_tmp_507_fu_7703_p1() {
    tmp_507_fu_7703_p1 = msb_idx_6_fu_7697_p2.read().range(31-1, 0);
}

void mnist_fp4::thread_tmp_508_cast_fu_6635_p1() {
    tmp_508_cast_fu_6635_p1 = esl_zext<64,7>(tmp_462_fu_6630_p2.read());
}

void mnist_fp4::thread_tmp_509_fu_7725_p4() {
    tmp_509_fu_7725_p4 = msb_idx_7_fu_7715_p3.read().range(30, 5);
}

void mnist_fp4::thread_tmp_510_fu_7756_p1() {
    tmp_510_fu_7756_p1 = msb_idx_7_fu_7715_p3.read().range(2-1, 0);
}

void mnist_fp4::thread_tmp_511_fu_7760_p2() {
    tmp_511_fu_7760_p2 = (!ap_const_lv2_1.is_01() || !tmp_510_fu_7756_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(tmp_510_fu_7756_p1.read()));
}

void mnist_fp4::thread_tmp_512_fu_7766_p1() {
    tmp_512_fu_7766_p1 = esl_zext<4,2>(tmp_511_fu_7760_p2.read());
}

void mnist_fp4::thread_tmp_513_cast_fu_8955_p1() {
    tmp_513_cast_fu_8955_p1 = esl_sext<10,9>(tmp_467_fu_8949_p2.read());
}

void mnist_fp4::thread_tmp_513_fu_6755_p1() {
    tmp_513_fu_6755_p1 = esl_zext<64,11>(tmp_506_fu_6746_p4.read());
}

void mnist_fp4::thread_tmp_514_fu_7807_p1() {
    tmp_514_fu_7807_p1 = msb_idx_6_reg_11331.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_515_fu_6764_p2() {
    tmp_515_fu_6764_p2 = (!tmp_475_reg_11027.read().is_01() || !tmp_214_cast_fu_6760_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_475_reg_11027.read()) + sc_biguint<10>(tmp_214_cast_fu_6760_p1.read()));
}

void mnist_fp4::thread_tmp_516_fu_7850_p2() {
    tmp_516_fu_7850_p2 = (!tmp_503_cast_reg_11218.read().is_01() || !tmp_260_cast_fu_7846_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_503_cast_reg_11218.read()) + sc_biguint<13>(tmp_260_cast_fu_7846_p1.read()));
}

void mnist_fp4::thread_tmp_517_fu_9026_p2() {
    tmp_517_fu_9026_p2 = (!tmp_255_cast_fu_9022_p1.read().is_01() || !tmp_505_reg_11658.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_255_cast_fu_9022_p1.read()) + sc_biguint<12>(tmp_505_reg_11658.read()));
}

void mnist_fp4::thread_tmp_518_fu_9049_p1() {
    tmp_518_fu_9049_p1 = conv4_0_load_to_int_fu_9036_p1.read().range(23-1, 0);
}

void mnist_fp4::thread_tmp_519_fu_9086_p1() {
    tmp_519_fu_9086_p1 = ireg_V_7_fu_9078_p3.read().range(63-1, 0);
}

void mnist_fp4::thread_tmp_51_fu_2782_p3() {
    tmp_51_fu_2782_p3 = esl_concat<3,5>(p_7_reg_1042.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_521_fu_9108_p1() {
    tmp_521_fu_9108_p1 = ireg_V_7_fu_9078_p3.read().range(52-1, 0);
}

void mnist_fp4::thread_tmp_522_fu_9183_p1() {
    tmp_522_fu_9183_p1 = man_V_16_fu_9138_p3.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_523_fu_9187_p4() {
    tmp_523_fu_9187_p4 = sh_amt_5_fu_9169_p3.read().range(11, 2);
}

void mnist_fp4::thread_tmp_524_fu_9220_p1() {
    tmp_524_fu_9220_p1 = tmp_301_fu_9215_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_525_fu_9240_p1() {
    tmp_525_fu_9240_p1 = tmp_304_fu_9234_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_526_fu_8838_p2() {
    tmp_526_fu_8838_p2 = (!tmp_471_reg_11387.read().is_01() || !tmp_292_cast_fu_8834_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_471_reg_11387.read()) + sc_biguint<12>(tmp_292_cast_fu_8834_p1.read()));
}

void mnist_fp4::thread_tmp_527_fu_8078_p1() {
    tmp_527_fu_8078_p1 = msb_idx_8_fu_8072_p2.read().range(31-1, 0);
}

void mnist_fp4::thread_tmp_529_fu_8772_p4() {
    tmp_529_fu_8772_p4 = msb_idx_9_fu_8762_p3.read().range(30, 5);
}

void mnist_fp4::thread_tmp_52_fu_3034_p2() {
    tmp_52_fu_3034_p2 = (!F2_fu_3002_p2.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<1>(): sc_lv<1>(F2_fu_3002_p2.read() == ap_const_lv12_2);
}

void mnist_fp4::thread_tmp_530_fu_8803_p1() {
    tmp_530_fu_8803_p1 = msb_idx_9_fu_8762_p3.read().range(2-1, 0);
}

void mnist_fp4::thread_tmp_531_fu_8807_p2() {
    tmp_531_fu_8807_p2 = (!ap_const_lv2_1.is_01() || !tmp_530_fu_8803_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(tmp_530_fu_8803_p1.read()));
}

void mnist_fp4::thread_tmp_532_fu_8813_p1() {
    tmp_532_fu_8813_p1 = esl_zext<4,2>(tmp_531_fu_8807_p2.read());
}

void mnist_fp4::thread_tmp_533_fu_7997_p2() {
    tmp_533_fu_7997_p2 = (!tmp_194_cast_fu_7993_p1.read().is_01() || !tmp_497_cast_reg_11369.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_194_cast_fu_7993_p1.read()) + sc_biguint<8>(tmp_497_cast_reg_11369.read()));
}

void mnist_fp4::thread_tmp_534_cast_fu_7594_p1() {
    tmp_534_cast_fu_7594_p1 = esl_sext<9,8>(tmp_496_fu_7588_p2.read());
}

void mnist_fp4::thread_tmp_534_fu_8863_p1() {
    tmp_534_fu_8863_p1 = msb_idx_8_reg_11424.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_535_fu_8006_p3() {
    tmp_535_fu_8006_p3 = esl_concat<8,2>(tmp_533_fu_7997_p2.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_536_fu_8018_p2() {
    tmp_536_fu_8018_p2 = (!p_shl7_fu_8014_p1.read().is_01() || !tmp_560_cast_fu_8002_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl7_fu_8014_p1.read()) - sc_biguint<64>(tmp_560_cast_fu_8002_p1.read()));
}

void mnist_fp4::thread_tmp_537_fu_8116_p2() {
    tmp_537_fu_8116_p2 = (!tmp_536_reg_11408.read().is_01() || !tmp_259_fu_8112_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_536_reg_11408.read()) + sc_biguint<64>(tmp_259_fu_8112_p1.read()));
}

void mnist_fp4::thread_tmp_538_fu_8121_p1() {
    tmp_538_fu_8121_p1 = tmp_537_fu_8116_p2.read().range(11-1, 0);
}

void mnist_fp4::thread_tmp_539_cast_fu_7641_p1() {
    tmp_539_cast_fu_7641_p1 = esl_zext<64,12>(tmp_501_fu_7635_p2.read());
}

void mnist_fp4::thread_tmp_539_fu_8125_p1() {
    tmp_539_fu_8125_p1 = tmp_537_fu_8116_p2.read().range(9-1, 0);
}

void mnist_fp4::thread_tmp_53_fu_2794_p3() {
    tmp_53_fu_2794_p3 = esl_concat<3,2>(p_7_reg_1042.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_540_fu_8137_p2() {
    tmp_540_fu_8137_p2 = (!p_shl89_cast_fu_8129_p3.read().is_01() || !tmp_538_fu_8121_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl89_cast_fu_8129_p3.read()) - sc_biguint<11>(tmp_538_fu_8121_p1.read()));
}

void mnist_fp4::thread_tmp_541_fu_8165_p4() {
    tmp_541_fu_8165_p4 = esl_concat<8,4>(esl_concat<4,4>(p_51_reg_1659.read(), r_V_18_reg_11447.read()), r_V_19_fu_8159_p2.read());
}

void mnist_fp4::thread_tmp_542_fu_8174_p1() {
    tmp_542_fu_8174_p1 = esl_zext<64,12>(tmp_541_fu_8165_p4.read());
}

void mnist_fp4::thread_tmp_543_fu_8197_p1() {
    tmp_543_fu_8197_p1 = ireg_V_10_fu_8193_p1.read().range(63-1, 0);
}

void mnist_fp4::thread_tmp_544_fu_8183_p2() {
    tmp_544_fu_8183_p2 = (!tmp_331_cast_fu_8179_p1.read().is_01() || !tmp_540_reg_11452.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_331_cast_fu_8179_p1.read()) + sc_biguint<11>(tmp_540_reg_11452.read()));
}

void mnist_fp4::thread_tmp_545_fu_8219_p1() {
    tmp_545_fu_8219_p1 = ireg_V_10_fu_8193_p1.read().range(52-1, 0);
}

void mnist_fp4::thread_tmp_546_fu_8330_p1() {
    tmp_546_fu_8330_p1 = man_V_12_fu_8285_p3.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_547_fu_8334_p4() {
    tmp_547_fu_8334_p4 = sh_amt_6_fu_8316_p3.read().range(11, 2);
}

void mnist_fp4::thread_tmp_548_fu_8452_p1() {
    tmp_548_fu_8452_p1 = tmp_321_fu_8447_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_549_fu_8472_p1() {
    tmp_549_fu_8472_p1 = tmp_324_fu_8466_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_54_fu_3063_p2() {
    tmp_54_fu_3063_p2 = (!sh_amt_reg_10065.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_reg_10065.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp4::thread_tmp_550_fu_8233_p1() {
    tmp_550_fu_8233_p1 = ireg_V_11_fu_8229_p1.read().range(63-1, 0);
}

void mnist_fp4::thread_tmp_551_cast_fu_6769_p1() {
    tmp_551_cast_fu_6769_p1 = esl_zext<64,10>(tmp_515_fu_6764_p2.read());
}

void mnist_fp4::thread_tmp_552_cast_fu_7855_p1() {
    tmp_552_cast_fu_7855_p1 = esl_zext<64,13>(tmp_516_fu_7850_p2.read());
}

void mnist_fp4::thread_tmp_552_fu_8255_p1() {
    tmp_552_fu_8255_p1 = ireg_V_11_fu_8229_p1.read().range(52-1, 0);
}

void mnist_fp4::thread_tmp_553_fu_8415_p1() {
    tmp_553_fu_8415_p1 = man_V_15_fu_8370_p3.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_554_fu_8419_p4() {
    tmp_554_fu_8419_p4 = sh_amt_7_fu_8401_p3.read().range(11, 2);
}

void mnist_fp4::thread_tmp_555_fu_8601_p1() {
    tmp_555_fu_8601_p1 = tmp_345_fu_8596_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_556_fu_8621_p1() {
    tmp_556_fu_8621_p1 = tmp_348_fu_8615_p2.read().range(4-1, 0);
}

void mnist_fp4::thread_tmp_558_cast_fu_9031_p1() {
    tmp_558_cast_fu_9031_p1 = esl_zext<64,12>(tmp_517_fu_9026_p2.read());
}

void mnist_fp4::thread_tmp_559_cast_fu_8909_p1() {
    tmp_559_cast_fu_8909_p1 = esl_zext<64,12>(tmp_526_reg_11622.read());
}

void mnist_fp4::thread_tmp_55_fu_3728_p2() {
    tmp_55_fu_3728_p2 = (!ap_const_lv4_0.is_01() || !p_Val2_s_reg_10214.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_0) - sc_biguint<4>(p_Val2_s_reg_10214.read()));
}

void mnist_fp4::thread_tmp_560_cast_fu_8002_p1() {
    tmp_560_cast_fu_8002_p1 = esl_zext<64,8>(tmp_533_fu_7997_p2.read());
}

void mnist_fp4::thread_tmp_56_fu_6093_p2() {
    tmp_56_fu_6093_p2 = (!p_20_reg_1378.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_20_reg_1378.read() != ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_578_cast_fu_8188_p1() {
    tmp_578_cast_fu_8188_p1 = esl_zext<64,11>(tmp_544_fu_8183_p2.read());
}

void mnist_fp4::thread_tmp_57_fu_6099_p2() {
    tmp_57_fu_6099_p2 = (!p_20_reg_1378.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_20_reg_1378.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp4::thread_tmp_58_fu_2806_p2() {
    tmp_58_fu_2806_p2 = (!p_shl22_cast_fu_2790_p1.read().is_01() || !p_shl23_cast_fu_2802_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl22_cast_fu_2790_p1.read()) - sc_biguint<9>(p_shl23_cast_fu_2802_p1.read()));
}

void mnist_fp4::thread_tmp_59_fu_2586_p2() {
    tmp_59_fu_2586_p2 = (!tmp_69_cast_reg_9894.read().is_01() || !tmp_11_cast_fu_2582_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_69_cast_reg_9894.read()) + sc_biguint<10>(tmp_11_cast_fu_2582_p1.read()));
}

void mnist_fp4::thread_tmp_60_fu_2591_p1() {
    tmp_60_fu_2591_p1 = tmp_59_fu_2586_p2.read().range(8-1, 0);
}

void mnist_fp4::thread_tmp_61_fu_2603_p3() {
    tmp_61_fu_2603_p3 = esl_concat<10,2>(tmp_59_fu_2586_p2.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_62_cast_fu_5103_p1() {
    tmp_62_cast_fu_5103_p1 = esl_zext<10,5>(p_23_reg_1254.read());
}

void mnist_fp4::thread_tmp_62_fu_2615_p2() {
    tmp_62_fu_2615_p2 = (!p_shl17_cast_fu_2595_p3.read().is_01() || !p_shl18_cast_fu_2611_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl17_cast_fu_2595_p3.read()) - sc_bigint<13>(p_shl18_cast_fu_2611_p1.read()));
}

void mnist_fp4::thread_tmp_63_fu_4888_p2() {
    tmp_63_fu_4888_p2 = (!p_Val2_3_reg_1174.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): sc_lv<1>(p_Val2_3_reg_1174.read() == ap_const_lv4_0);
}

void mnist_fp4::thread_tmp_64_cast_fu_4052_p1() {
    tmp_64_cast_fu_4052_p1 = esl_zext<6,3>(p_24_reg_1186.read());
}

void mnist_fp4::thread_tmp_64_fu_2348_p2() {
    tmp_64_fu_2348_p2 = (!r_V_fu_2342_p2.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): (sc_biguint<5>(r_V_fu_2342_p2.read()) < sc_biguint<5>(ap_const_lv5_1C));
}

void mnist_fp4::thread_tmp_65_fu_2354_p2() {
    tmp_65_fu_2354_p2 = (!ap_const_lv5_3.is_01() || !p_3_reg_933.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_3) + sc_biguint<5>(p_3_reg_933.read()));
}

void mnist_fp4::thread_tmp_66_cast_fu_6563_p1() {
    tmp_66_cast_fu_6563_p1 = esl_zext<10,4>(p_25_reg_1424.read());
}

void mnist_fp4::thread_tmp_66_fu_2402_p1() {
    tmp_66_fu_2402_p1 = mul_fu_9664_p2.read().range(23-1, 0);
}

void mnist_fp4::thread_tmp_67_fu_3813_p2() {
    tmp_67_fu_3813_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_1_cast_fu_3790_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_1_cast_fu_3790_p1.read()));
}

void mnist_fp4::thread_tmp_69_cast_fu_2566_p1() {
    tmp_69_cast_fu_2566_p1 = esl_sext<10,9>(tmp_18_fu_2560_p2.read());
}

void mnist_fp4::thread_tmp_69_fu_2419_p4() {
    tmp_69_fu_2419_p4 = neg_mul_fu_2414_p2.read().range(22, 16);
}

void mnist_fp4::thread_tmp_6_cast_fu_2178_p1() {
    tmp_6_cast_fu_2178_p1 = esl_zext<11,5>(p_1_reg_815.read());
}

void mnist_fp4::thread_tmp_6_fu_2141_p2() {
    tmp_6_fu_2141_p2 = (!p_shl8_cast_fu_2125_p1.read().is_01() || !p_shl9_cast_fu_2137_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl8_cast_fu_2125_p1.read()) - sc_biguint<11>(p_shl9_cast_fu_2137_p1.read()));
}

void mnist_fp4::thread_tmp_70_fu_6181_p2() {
    tmp_70_fu_6181_p2 = (!p_30_reg_1389.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_30_reg_1389.read() != ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_71_fu_6187_p2() {
    tmp_71_fu_6187_p2 = (!p_30_reg_1389.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_30_reg_1389.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp4::thread_tmp_72_fu_5253_p3() {
    tmp_72_fu_5253_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_328_reg_10634.read());
}

void mnist_fp4::thread_tmp_73_fu_4125_p2() {
    tmp_73_fu_4125_p2 = (!ap_const_lv4_0.is_01() || !p_Val2_3_reg_1174.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_0) - sc_biguint<4>(p_Val2_3_reg_1174.read()));
}

void mnist_fp4::thread_tmp_74_fu_2429_p1() {
    tmp_74_fu_2429_p1 = esl_sext<11,7>(tmp_69_fu_2419_p4.read());
}

void mnist_fp4::thread_tmp_76_fu_3870_p2() {
    tmp_76_fu_3870_p2 = (!p_Result_9_fu_3860_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_9_fu_3860_p4.read() != ap_const_lv8_9E);
}

void mnist_fp4::thread_tmp_77_cast_fu_5154_p1() {
    tmp_77_cast_fu_5154_p1 = esl_zext<13,5>(p_28_reg_1265.read());
}

void mnist_fp4::thread_tmp_77_fu_2433_p1() {
    tmp_77_fu_2433_p1 = esl_sext<11,8>(tmp_75_reg_9851.read());
}

void mnist_fp4::thread_tmp_78_fu_3068_p1() {
    tmp_78_fu_3068_p1 = esl_zext<54,32>(sh_amt_cast_fu_3060_p1.read());
}

void mnist_fp4::thread_tmp_79_fu_2436_p3() {
    tmp_79_fu_2436_p3 = (!tmp_68_reg_9840.read()[0].is_01())? sc_lv<11>(): ((tmp_68_reg_9840.read()[0].to_bool())? tmp_74_fu_2429_p1.read(): tmp_77_fu_2433_p1.read());
}

void mnist_fp4::thread_tmp_7_cast_fu_2518_p1() {
    tmp_7_cast_fu_2518_p1 = esl_zext<6,3>(p_2_reg_960.read());
}

void mnist_fp4::thread_tmp_7_fu_2198_p3() {
    tmp_7_fu_2198_p3 = esl_concat<5,5>(p_s_reg_921.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_80_cast_fu_4205_p1() {
    tmp_80_cast_fu_4205_p1 = esl_zext<10,5>(r_V_9_fu_4199_p2.read());
}

void mnist_fp4::thread_tmp_80_fu_2465_p2() {
    tmp_80_fu_2465_p2 = (!ap_const_lv11_5.is_01())? sc_lv<11>(): grp_fu_2456_p2.read() << (unsigned short)ap_const_lv11_5.to_uint();
}

void mnist_fp4::thread_tmp_81_fu_4244_p1() {
    tmp_81_fu_4244_p1 = esl_zext<64,2>(p_29_reg_1209.read());
}

void mnist_fp4::thread_tmp_82_fu_3072_p2() {
    tmp_82_fu_3072_p2 = (!man_V_2_reg_10054.read().is_01() || !tmp_78_fu_3068_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_2_reg_10054.read()) >> (unsigned short)tmp_78_fu_3068_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_83_fu_2471_p2() {
    tmp_83_fu_2471_p2 = (!ap_const_lv11_2.is_01())? sc_lv<11>(): grp_fu_2456_p2.read() << (unsigned short)ap_const_lv11_2.to_uint();
}

void mnist_fp4::thread_tmp_84_fu_3892_p3() {
    tmp_84_fu_3892_p3 = esl_concat<1,8>(is_neg_reg_10225.read(), p_Repl2_1_trunc_fu_3886_p2.read());
}

void mnist_fp4::thread_tmp_85_fu_7259_p2() {
    tmp_85_fu_7259_p2 = (!p_31_reg_1574.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_31_reg_1574.read() != ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_86_fu_7265_p2() {
    tmp_86_fu_7265_p2 = (!p_31_reg_1574.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_31_reg_1574.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp4::thread_tmp_87_fu_6226_p3() {
    tmp_87_fu_6226_p3 = (!tmp_396_fu_6214_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_396_fu_6214_p2.read()[0].to_bool())? r_V_11_fu_6208_p2.read(): tmp_403_fu_6220_p2.read());
}

void mnist_fp4::thread_tmp_88_cast_fu_6442_p1() {
    tmp_88_cast_fu_6442_p1 = esl_zext<11,4>(tmp_87_reg_10888.read());
}

void mnist_fp4::thread_tmp_88_fu_2477_p2() {
    tmp_88_fu_2477_p2 = (!tmp_80_fu_2465_p2.read().is_01() || !tmp_83_fu_2471_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_80_fu_2465_p2.read()) - sc_biguint<11>(tmp_83_fu_2471_p2.read()));
}

void mnist_fp4::thread_tmp_89_fu_3088_p1() {
    tmp_89_fu_3088_p1 = esl_sext<32,4>(tmp_148_reg_10077.read());
}

void mnist_fp4::thread_tmp_8_fu_2360_p3() {
    tmp_8_fu_2360_p3 = (!tmp_64_fu_2348_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_64_fu_2348_p2.read()[0].to_bool())? r_V_fu_2342_p2.read(): tmp_65_fu_2354_p2.read());
}

void mnist_fp4::thread_tmp_90_cast_fu_6234_p1() {
    tmp_90_cast_fu_6234_p1 = esl_zext<5,4>(tmp_87_fu_6226_p3.read());
}

void mnist_fp4::thread_tmp_90_fu_3091_p2() {
    tmp_90_fu_3091_p2 = (!sh_amt_cast_fu_3060_p1.read().is_01())? sc_lv<32>(): tmp_89_fu_3088_p1.read() << (unsigned short)sh_amt_cast_fu_3060_p1.read().to_uint();
}

void mnist_fp4::thread_tmp_93_cast_fu_3914_p1() {
    tmp_93_cast_fu_3914_p1 = esl_zext<13,5>(p_14_reg_1109.read());
}

void mnist_fp4::thread_tmp_93_fu_2483_p2() {
    tmp_93_fu_2483_p2 = (!tmp_10_cast_fu_2462_p1.read().is_01() || !tmp_88_fu_2477_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_10_cast_fu_2462_p1.read()) + sc_biguint<11>(tmp_88_fu_2477_p2.read()));
}

void mnist_fp4::thread_tmp_94_fu_2497_p2() {
    tmp_94_fu_2497_p2 = (!tmp_11_reg_9804.read().is_01() || !tmp_16_cast_fu_2493_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_11_reg_9804.read()) + sc_biguint<11>(tmp_16_cast_fu_2493_p1.read()));
}

void mnist_fp4::thread_tmp_96_fu_3231_p3() {
    tmp_96_fu_3231_p3 = esl_concat<3,5>(p_6_reg_1075.read(), ap_const_lv5_0);
}

void mnist_fp4::thread_tmp_97_cast_fu_2187_p1() {
    tmp_97_cast_fu_2187_p1 = esl_sext<64,11>(tmp_34_fu_2182_p2.read());
}

void mnist_fp4::thread_tmp_97_fu_3243_p3() {
    tmp_97_fu_3243_p3 = esl_concat<3,1>(p_6_reg_1075.read(), ap_const_lv1_0);
}

void mnist_fp4::thread_tmp_98_fu_3255_p2() {
    tmp_98_fu_3255_p2 = (!p_shl26_cast_fu_3239_p1.read().is_01() || !p_shl27_cast_fu_3251_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl26_cast_fu_3239_p1.read()) - sc_biguint<9>(p_shl27_cast_fu_3251_p1.read()));
}

void mnist_fp4::thread_tmp_99_fu_5250_p1() {
    tmp_99_fu_5250_p1 = esl_zext<12,11>(p_Result_6_reg_10629.read());
}

void mnist_fp4::thread_tmp_9_fu_2129_p3() {
    tmp_9_fu_2129_p3 = esl_concat<5,2>(p_0_reg_804.read(), ap_const_lv2_0);
}

void mnist_fp4::thread_tmp_V_2_fu_5770_p3() {
    tmp_V_2_fu_5770_p3 = (!tmp_434_reg_10771.read()[0].is_01())? sc_lv<4>(): ((tmp_434_reg_10771.read()[0].to_bool())? tmp_188_fu_5765_p2.read(): relu2_0_V_load_reg_10764.read());
}

void mnist_fp4::thread_tmp_user_V_fu_2096_p1() {
    tmp_user_V_fu_2096_p1 = stream_in_V_user_V_0_data_out.read();
}

void mnist_fp4::thread_w1_V_fu_9382_p2() {
    w1_V_fu_9382_p2 = (!p_65_reg_1772.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_65_reg_1772.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_w_V_fu_5629_p2() {
    w_V_fu_5629_p2 = (!p_33_reg_1298.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_33_reg_1298.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_w_conv1_address0() {
    w_conv1_address0 =  (sc_lv<6>) (tmp_390_cast_fu_2765_p1.read());
}

void mnist_fp4::thread_w_conv1_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state37.read())) {
        w_conv1_ce0 = ap_const_logic_1;
    } else {
        w_conv1_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_w_conv2_address0() {
    w_conv2_address0 =  (sc_lv<8>) (tmp_468_cast_fu_4320_p1.read());
}

void mnist_fp4::thread_w_conv2_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state96.read())) {
        w_conv2_ce0 = ap_const_logic_1;
    } else {
        w_conv2_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_w_conv3_address0() {
    w_conv3_address0 =  (sc_lv<9>) (tmp_551_cast_fu_6769_p1.read());
}

void mnist_fp4::thread_w_conv3_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state162.read())) {
        w_conv3_ce0 = ap_const_logic_1;
    } else {
        w_conv3_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_w_conv4_address0() {
    w_conv4_address0 =  (sc_lv<10>) (tmp_578_cast_fu_8188_p1.read());
}

void mnist_fp4::thread_w_conv4_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state218.read())) {
        w_conv4_ce0 = ap_const_logic_1;
    } else {
        w_conv4_ce0 = ap_const_logic_0;
    }
}

void mnist_fp4::thread_w_conv5_address0() {
    w_conv5_address0 = ap_const_lv11_0;
}

void mnist_fp4::thread_w_conv5_address1() {
    w_conv5_address1 = ap_const_lv11_0;
}

void mnist_fp4::thread_w_conv5_ce0() {
    w_conv5_ce0 = ap_const_logic_0;
}

void mnist_fp4::thread_w_conv5_ce1() {
    w_conv5_ce1 = ap_const_logic_0;
}

void mnist_fp4::thread_w_conv5_d0() {
    w_conv5_d0 = ap_const_lv32_0;
}

void mnist_fp4::thread_w_conv5_d1() {
    w_conv5_d1 = ap_const_lv32_0;
}

void mnist_fp4::thread_w_conv5_we0() {
    w_conv5_we0 = ap_const_logic_0;
}

void mnist_fp4::thread_w_conv5_we1() {
    w_conv5_we1 = ap_const_logic_0;
}

void mnist_fp4::thread_w_conv6_address0() {
    w_conv6_address0 = ap_const_lv12_0;
}

void mnist_fp4::thread_w_conv6_address1() {
    w_conv6_address1 = ap_const_lv12_0;
}

void mnist_fp4::thread_w_conv6_ce0() {
    w_conv6_ce0 = ap_const_logic_0;
}

void mnist_fp4::thread_w_conv6_ce1() {
    w_conv6_ce1 = ap_const_logic_0;
}

void mnist_fp4::thread_w_conv6_d0() {
    w_conv6_d0 = ap_const_lv32_0;
}

void mnist_fp4::thread_w_conv6_d1() {
    w_conv6_d1 = ap_const_lv32_0;
}

void mnist_fp4::thread_w_conv6_we0() {
    w_conv6_we0 = ap_const_logic_0;
}

void mnist_fp4::thread_w_conv6_we1() {
    w_conv6_we1 = ap_const_logic_0;
}

void mnist_fp4::thread_w_dense_1_address0() {
    w_dense_1_address0 = ap_const_lv8_0;
}

void mnist_fp4::thread_w_dense_1_address1() {
    w_dense_1_address1 = ap_const_lv8_0;
}

void mnist_fp4::thread_w_dense_1_ce0() {
    w_dense_1_ce0 = ap_const_logic_0;
}

void mnist_fp4::thread_w_dense_1_ce1() {
    w_dense_1_ce1 = ap_const_logic_0;
}

void mnist_fp4::thread_w_dense_1_d0() {
    w_dense_1_d0 = ap_const_lv32_0;
}

void mnist_fp4::thread_w_dense_1_d1() {
    w_dense_1_d1 = ap_const_lv32_0;
}

void mnist_fp4::thread_w_dense_1_we0() {
    w_dense_1_we0 = ap_const_logic_0;
}

void mnist_fp4::thread_w_dense_1_we1() {
    w_dense_1_we1 = ap_const_logic_0;
}

void mnist_fp4::thread_xx1_V_fu_4034_p2() {
    xx1_V_fu_4034_p2 = (!p_18_reg_1162.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_18_reg_1162.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_xx2_V_fu_6608_p2() {
    xx2_V_fu_6608_p2 = (!p_32_reg_1436.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_32_reg_1436.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_xx3_V_fu_7975_p2() {
    xx3_V_fu_7975_p2 = (!p_44_reg_1635.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_44_reg_1635.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_xx4_V_fu_9478_p2() {
    xx4_V_fu_9478_p2 = (!p_58_reg_1860.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_58_reg_1860.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_xx5_V_fu_9622_p2() {
    xx5_V_fu_9622_p2 = (!p_67_reg_1992.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_67_reg_1992.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_xx_V_fu_2627_p2() {
    xx_V_fu_2627_p2 = (!p_8_reg_983.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_8_reg_983.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_yy1_V_fu_3983_p2() {
    yy1_V_fu_3983_p2 = (!p_10_reg_1150.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_10_reg_1150.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp4::thread_yy2_V_fu_6557_p2() {
    yy2_V_fu_6557_p2 = (!p_25_reg_1424.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_25_reg_1424.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_yy3_V_fu_7924_p2() {
    yy3_V_fu_7924_p2 = (!p_35_reg_1623.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_35_reg_1623.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp4::thread_yy4_V_fu_9466_p2() {
    yy4_V_fu_9466_p2 = (!p_49_reg_1849.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_49_reg_1849.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_yy5_V_fu_9610_p2() {
    yy5_V_fu_9610_p2 = (!p_62_reg_1981.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_62_reg_1981.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp4::thread_yy_V_fu_2576_p2() {
    yy_V_fu_2576_p2 = (!p_5_reg_971.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_5_reg_971.read()) + sc_biguint<5>(ap_const_lv5_1));
}

}

