#include "mnist_fp8.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp8::thread_tmp_11_fu_2234_p2() {
    tmp_11_fu_2234_p2 = (!p_shl10_cast_fu_2218_p1.read().is_01() || !p_shl11_cast_fu_2230_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl10_cast_fu_2218_p1.read()) - sc_biguint<11>(p_shl11_cast_fu_2230_p1.read()));
}

void mnist_fp8::thread_tmp_120_cast_fu_6637_p1() {
    tmp_120_cast_fu_6637_p1 = esl_zext<7,3>(p_40_reg_1460.read());
}

void mnist_fp8::thread_tmp_120_fu_3292_p1() {
    tmp_120_fu_3292_p1 = tmp_119_fu_3287_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_122_fu_5212_p2() {
    tmp_122_fu_5212_p2 = (tmp_113_fu_5208_p2.read() & tmp_114_reg_10637.read());
}

void mnist_fp8::thread_tmp_123_fu_5294_p2() {
    tmp_123_fu_5294_p2 = (!F2_1_fu_5288_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_1_fu_5288_p2.read()) > sc_bigint<12>(ap_const_lv12_6));
}

void mnist_fp8::thread_tmp_124_fu_5300_p2() {
    tmp_124_fu_5300_p2 = (!ap_const_lv12_FFA.is_01() || !F2_1_fu_5288_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFA) + sc_biguint<12>(F2_1_fu_5288_p2.read()));
}

void mnist_fp8::thread_tmp_125_fu_5306_p2() {
    tmp_125_fu_5306_p2 = (!ap_const_lv12_6.is_01() || !F2_1_fu_5288_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_6) - sc_biguint<12>(F2_1_fu_5288_p2.read()));
}

void mnist_fp8::thread_tmp_126_fu_5320_p2() {
    tmp_126_fu_5320_p2 = (!F2_1_fu_5288_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): sc_lv<1>(F2_1_fu_5288_p2.read() == ap_const_lv12_6);
}

void mnist_fp8::thread_tmp_127_cast_fu_4977_p1() {
    tmp_127_cast_fu_4977_p1 = esl_zext<13,5>(p_18_reg_1174.read());
}

void mnist_fp8::thread_tmp_127_fu_3304_p3() {
    tmp_127_fu_3304_p3 = esl_concat<10,1>(tmp_119_fu_3287_p2.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_128_fu_4352_p3() {
    tmp_128_fu_4352_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_390_reg_10409.read());
}

void mnist_fp8::thread_tmp_130_fu_4382_p2() {
    tmp_130_fu_4382_p2 = (!F2_2_fu_4376_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_2_fu_4376_p2.read()) > sc_bigint<12>(ap_const_lv12_6));
}

void mnist_fp8::thread_tmp_131_fu_4388_p2() {
    tmp_131_fu_4388_p2 = (!ap_const_lv12_FFA.is_01() || !F2_2_fu_4376_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFA) + sc_biguint<12>(F2_2_fu_4376_p2.read()));
}

void mnist_fp8::thread_tmp_132_fu_4394_p2() {
    tmp_132_fu_4394_p2 = (!ap_const_lv12_6.is_01() || !F2_2_fu_4376_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_6) - sc_biguint<12>(F2_2_fu_4376_p2.read()));
}

void mnist_fp8::thread_tmp_133_fu_4408_p2() {
    tmp_133_fu_4408_p2 = (!F2_2_fu_4376_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): sc_lv<1>(F2_2_fu_4376_p2.read() == ap_const_lv12_6);
}

void mnist_fp8::thread_tmp_134_fu_3316_p2() {
    tmp_134_fu_3316_p2 = (!p_shl28_cast_fu_3296_p3.read().is_01() || !p_shl29_cast_fu_3312_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl28_cast_fu_3296_p3.read()) - sc_bigint<13>(p_shl29_cast_fu_3312_p1.read()));
}

void mnist_fp8::thread_tmp_135_cast_fu_5680_p1() {
    tmp_135_cast_fu_5680_p1 = esl_zext<5,2>(p_38_reg_1334.read());
}

void mnist_fp8::thread_tmp_135_fu_5217_p1() {
    tmp_135_fu_5217_p1 = grp_fu_2061_p1.read();
}

void mnist_fp8::thread_tmp_136_cast_cast_fu_5689_p1() {
    tmp_136_cast_cast_fu_5689_p1 = esl_zext<10,5>(tmp_136_fu_5684_p2.read());
}

void mnist_fp8::thread_tmp_136_fu_5684_p2() {
    tmp_136_fu_5684_p2 = (!tmp_135_cast_fu_5680_p1.read().is_01() || !r_V_s_reg_10734.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_135_cast_fu_5680_p1.read()) + sc_biguint<5>(r_V_s_reg_10734.read()));
}

void mnist_fp8::thread_tmp_137_fu_2895_p2() {
    tmp_137_fu_2895_p2 = (!tmp_20_cast_fu_2891_p1.read().is_01() || !tmp_107_reg_9997.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_20_cast_fu_2891_p1.read()) + sc_biguint<13>(tmp_107_reg_9997.read()));
}

void mnist_fp8::thread_tmp_138_cast_fu_2824_p1() {
    tmp_138_cast_fu_2824_p1 = esl_sext<10,9>(tmp_58_fu_2818_p2.read());
}

void mnist_fp8::thread_tmp_138_fu_2918_p1() {
    tmp_138_fu_2918_p1 = conv1_0_load_to_int_fu_2905_p1.read().range(23-1, 0);
}

void mnist_fp8::thread_tmp_139_fu_2955_p1() {
    tmp_139_fu_2955_p1 = ireg_V_fu_2947_p3.read().range(63-1, 0);
}

void mnist_fp8::thread_tmp_13_fu_2534_p3() {
    tmp_13_fu_2534_p3 = esl_concat<3,2>(p_2_reg_972.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_140_to_int_fu_5985_p1() {
    tmp_140_to_int_fu_5985_p1 = tmp_140_reg_1345.read();
}

void mnist_fp8::thread_tmp_141_fu_5349_p2() {
    tmp_141_fu_5349_p2 = (!sh_amt_1_reg_10675.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_1_reg_10675.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp8::thread_tmp_143_fu_5354_p1() {
    tmp_143_fu_5354_p1 = esl_zext<54,32>(sh_amt_1_cast_fu_5346_p1.read());
}

void mnist_fp8::thread_tmp_144_fu_4522_p2() {
    tmp_144_fu_4522_p2 = (!sh_amt_2_reg_10453.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_2_reg_10453.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp8::thread_tmp_145_fu_5358_p2() {
    tmp_145_fu_5358_p2 = (!man_V_6_reg_10664.read().is_01() || !tmp_143_fu_5354_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_6_reg_10664.read()) >> (unsigned short)tmp_143_fu_5354_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_146_fu_2977_p1() {
    tmp_146_fu_2977_p1 = ireg_V_fu_2947_p3.read().range(52-1, 0);
}

void mnist_fp8::thread_tmp_147_fu_7403_p3() {
    tmp_147_fu_7403_p3 = (!tmp_477_fu_7391_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_477_fu_7391_p2.read()[0].to_bool())? r_V_15_fu_7385_p2.read(): tmp_478_fu_7397_p2.read());
}

void mnist_fp8::thread_tmp_148_cast_fu_7619_p1() {
    tmp_148_cast_fu_7619_p1 = esl_zext<12,4>(tmp_147_reg_11268.read());
}

void mnist_fp8::thread_tmp_148_fu_3052_p1() {
    tmp_148_fu_3052_p1 = man_V_2_fu_3007_p3.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_149_fu_3056_p4() {
    tmp_149_fu_3056_p4 = sh_amt_fu_3038_p3.read().range(11, 3);
}

void mnist_fp8::thread_tmp_150_cast_fu_6843_p1() {
    tmp_150_cast_fu_6843_p1 = esl_zext<10,4>(p_41_reg_1542.read());
}

void mnist_fp8::thread_tmp_150_fu_3089_p1() {
    tmp_150_fu_3089_p1 = tmp_82_fu_3084_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_151_cast_fu_7411_p1() {
    tmp_151_cast_fu_7411_p1 = esl_zext<5,4>(tmp_147_fu_7403_p3.read());
}

void mnist_fp8::thread_tmp_151_fu_5374_p1() {
    tmp_151_fu_5374_p1 = esl_sext<32,8>(tmp_329_reg_10687.read());
}

void mnist_fp8::thread_tmp_153_cast_fu_5740_p1() {
    tmp_153_cast_fu_5740_p1 = esl_zext<5,2>(p_43_reg_1357.read());
}

void mnist_fp8::thread_tmp_153_fu_5377_p2() {
    tmp_153_fu_5377_p2 = (!sh_amt_1_cast_fu_5346_p1.read().is_01())? sc_lv<32>(): tmp_151_fu_5374_p1.read() << (unsigned short)sh_amt_1_cast_fu_5346_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_154_cast_cast_fu_5749_p1() {
    tmp_154_cast_cast_fu_5749_p1 = esl_zext<13,5>(tmp_154_fu_5744_p2.read());
}

void mnist_fp8::thread_tmp_154_fu_5744_p2() {
    tmp_154_fu_5744_p2 = (!tmp_153_cast_fu_5740_p1.read().is_01() || !r_V_8_reg_10752.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_153_cast_fu_5740_p1.read()) + sc_biguint<5>(r_V_8_reg_10752.read()));
}

void mnist_fp8::thread_tmp_155_fu_5777_p2() {
    tmp_155_fu_5777_p2 = (!relu2_0_V_load_reg_10783.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(relu2_0_V_load_reg_10783.read() == ap_const_lv8_0);
}

void mnist_fp8::thread_tmp_156_fu_3109_p1() {
    tmp_156_fu_3109_p1 = tmp_90_fu_3103_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_158_fu_2722_p2() {
    tmp_158_fu_2722_p2 = (!tmp_62_reg_9917.read().is_01() || !tmp_25_cast_fu_2718_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_62_reg_9917.read()) + sc_biguint<13>(tmp_25_cast_fu_2718_p1.read()));
}

void mnist_fp8::thread_tmp_159_fu_2667_p3() {
    tmp_159_fu_2667_p3 = esl_concat<5,5>(r_V_3_fu_2661_p2.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_160_fu_6993_p3() {
    tmp_160_fu_6993_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_456_reg_11160.read());
}

void mnist_fp8::thread_tmp_161_fu_2679_p3() {
    tmp_161_fu_2679_p3 = esl_concat<5,1>(r_V_3_fu_2661_p2.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_162_fu_4527_p1() {
    tmp_162_fu_4527_p1 = esl_zext<54,32>(sh_amt_2_cast_fu_4519_p1.read());
}

void mnist_fp8::thread_tmp_163_fu_4531_p2() {
    tmp_163_fu_4531_p2 = (!man_V_4_reg_10442.read().is_01() || !tmp_162_fu_4527_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_4_reg_10442.read()) >> (unsigned short)tmp_162_fu_4527_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_164_fu_2691_p2() {
    tmp_164_fu_2691_p2 = (!p_shl20_cast_fu_2675_p1.read().is_01() || !p_shl21_cast_fu_2687_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl20_cast_fu_2675_p1.read()) - sc_biguint<11>(p_shl21_cast_fu_2687_p1.read()));
}

void mnist_fp8::thread_tmp_165_fu_4547_p1() {
    tmp_165_fu_4547_p1 = esl_sext<32,8>(tmp_391_reg_10465.read());
}

void mnist_fp8::thread_tmp_166_fu_4550_p2() {
    tmp_166_fu_4550_p2 = (!sh_amt_2_cast_fu_4519_p1.read().is_01())? sc_lv<32>(): tmp_165_fu_4547_p1.read() << (unsigned short)sh_amt_2_cast_fu_4519_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_167_fu_2701_p2() {
    tmp_167_fu_2701_p2 = (!tmp_27_cast_fu_2697_p1.read().is_01() || !tmp_49_cast_reg_9899.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_27_cast_fu_2697_p1.read()) + sc_bigint<7>(tmp_49_cast_reg_9899.read()));
}

void mnist_fp8::thread_tmp_168_fu_7657_p2() {
    tmp_168_fu_7657_p2 = (!relu3_0_V_q0.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(relu3_0_V_q0.read() == ap_const_lv8_0);
}

void mnist_fp8::thread_tmp_169_cast_fu_4263_p1() {
    tmp_169_cast_fu_4263_p1 = esl_zext<9,2>(p_34_reg_1244.read());
}

void mnist_fp8::thread_tmp_169_fu_2706_p2() {
    tmp_169_fu_2706_p2 = (!ap_const_lv7_2.is_01())? sc_lv<7>(): tmp_167_fu_2701_p2.read() << (unsigned short)ap_const_lv7_2.to_uint();
}

void mnist_fp8::thread_tmp_16_cast_fu_2505_p1() {
    tmp_16_cast_fu_2505_p1 = esl_zext<11,5>(p_3_reg_945.read());
}

void mnist_fp8::thread_tmp_16_fu_2550_p2() {
    tmp_16_fu_2550_p2 = (!p_shl16_cast_fu_2546_p1.read().is_01() || !tmp_7_cast_fu_2530_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(p_shl16_cast_fu_2546_p1.read()) - sc_biguint<6>(tmp_7_cast_fu_2530_p1.read()));
}

void mnist_fp8::thread_tmp_170_fu_4434_p1() {
    tmp_170_fu_4434_p1 = esl_zext<12,11>(exp_tmp_V_1_reg_10426.read());
}

void mnist_fp8::thread_tmp_171_fu_4437_p3() {
    tmp_171_fu_4437_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_397_reg_10431.read());
}

void mnist_fp8::thread_tmp_172_fu_4343_p2() {
    tmp_172_fu_4343_p2 = (!tmp_395_fu_4317_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_395_fu_4317_p1.read() == ap_const_lv63_0);
}

void mnist_fp8::thread_tmp_173_fu_4467_p2() {
    tmp_173_fu_4467_p2 = (!F2_3_fu_4461_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_3_fu_4461_p2.read()) > sc_bigint<12>(ap_const_lv12_6));
}

void mnist_fp8::thread_tmp_174_fu_4473_p2() {
    tmp_174_fu_4473_p2 = (!ap_const_lv12_FFA.is_01() || !F2_3_fu_4461_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFA) + sc_biguint<12>(F2_3_fu_4461_p2.read()));
}

void mnist_fp8::thread_tmp_175_fu_4479_p2() {
    tmp_175_fu_4479_p2 = (!ap_const_lv12_6.is_01() || !F2_3_fu_4461_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_6) - sc_biguint<12>(F2_3_fu_4461_p2.read()));
}

void mnist_fp8::thread_tmp_176_fu_4493_p2() {
    tmp_176_fu_4493_p2 = (!F2_3_fu_4461_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): sc_lv<1>(F2_3_fu_4461_p2.read() == ap_const_lv12_6);
}

void mnist_fp8::thread_tmp_177_fu_2712_p2() {
    tmp_177_fu_2712_p2 = (!tmp_169_fu_2706_p2.read().is_01() || !tmp_167_fu_2701_p2.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_169_fu_2706_p2.read()) - sc_biguint<7>(tmp_167_fu_2701_p2.read()));
}

void mnist_fp8::thread_tmp_178_fu_4671_p2() {
    tmp_178_fu_4671_p2 = (!sh_amt_3_reg_10487.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_3_reg_10487.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp8::thread_tmp_179_fu_5068_p3() {
    tmp_179_fu_5068_p3 = esl_concat<3,5>(p_19_reg_1255.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_17_fu_2560_p3() {
    tmp_17_fu_2560_p3 = esl_concat<3,5>(p_2_reg_972.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_180_fu_4676_p1() {
    tmp_180_fu_4676_p1 = esl_zext<54,32>(sh_amt_3_cast_fu_4668_p1.read());
}

void mnist_fp8::thread_tmp_181_fu_5080_p3() {
    tmp_181_fu_5080_p3 = esl_concat<3,2>(p_19_reg_1255.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_182_fu_7663_p2() {
    tmp_182_fu_7663_p2 = (!ap_const_lv8_0.is_01() || !relu3_0_V_q0.read().is_01())? sc_lv<8>(): (sc_biguint<8>(ap_const_lv8_0) - sc_biguint<8>(relu3_0_V_q0.read()));
}

void mnist_fp8::thread_tmp_183_fu_5092_p2() {
    tmp_183_fu_5092_p2 = (!p_shl36_cast_fu_5076_p1.read().is_01() || !p_shl37_cast_fu_5088_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl36_cast_fu_5076_p1.read()) - sc_biguint<9>(p_shl37_cast_fu_5088_p1.read()));
}

void mnist_fp8::thread_tmp_184_cast_fu_6894_p1() {
    tmp_184_cast_fu_6894_p1 = esl_zext<12,4>(p_47_reg_1553.read());
}

void mnist_fp8::thread_tmp_184_fu_4005_p2() {
    tmp_184_fu_4005_p2 = (!tmp_350_cast_reg_10303.read().is_01() || !tmp_29_cast_fu_4001_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_350_cast_reg_10303.read()) + sc_biguint<10>(tmp_29_cast_fu_4001_p1.read()));
}

void mnist_fp8::thread_tmp_185_fu_4680_p2() {
    tmp_185_fu_4680_p2 = (!man_V_9_reg_10476.read().is_01() || !tmp_180_fu_4676_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_9_reg_10476.read()) >> (unsigned short)tmp_180_fu_4676_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_186_fu_4010_p1() {
    tmp_186_fu_4010_p1 = tmp_184_fu_4005_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_187_fu_6704_p1() {
    tmp_187_fu_6704_p1 = esl_zext<64,2>(p_46_reg_1485.read());
}

void mnist_fp8::thread_tmp_188_fu_5771_p2() {
    tmp_188_fu_5771_p2 = (!ap_const_lv8_0.is_01() || !relu2_0_V_q0.read().is_01())? sc_lv<8>(): (sc_biguint<8>(ap_const_lv8_0) - sc_biguint<8>(relu2_0_V_q0.read()));
}

void mnist_fp8::thread_tmp_189_fu_4022_p3() {
    tmp_189_fu_4022_p3 = esl_concat<10,2>(tmp_184_fu_4005_p2.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_18_cast_fu_2840_p1() {
    tmp_18_cast_fu_2840_p1 = esl_zext<10,5>(p_11_reg_1065.read());
}

void mnist_fp8::thread_tmp_18_fu_2572_p2() {
    tmp_18_fu_2572_p2 = (!p_shl14_cast_fu_2568_p1.read().is_01() || !p_shl16_cast1_fu_2542_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl14_cast_fu_2568_p1.read()) - sc_biguint<9>(p_shl16_cast1_fu_2542_p1.read()));
}

void mnist_fp8::thread_tmp_190_fu_4034_p2() {
    tmp_190_fu_4034_p2 = (!p_shl38_cast_fu_4014_p3.read().is_01() || !p_shl39_cast_fu_4030_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl38_cast_fu_4014_p3.read()) - sc_bigint<13>(p_shl39_cast_fu_4030_p1.read()));
}

void mnist_fp8::thread_tmp_191_fu_3373_p2() {
    tmp_191_fu_3373_p2 = (!p_4_reg_1109.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_4_reg_1109.read() == ap_const_lv5_1F);
}

void mnist_fp8::thread_tmp_193_fu_8707_p2() {
    tmp_193_fu_8707_p2 = (!p_Val2_1_reg_1659.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(p_Val2_1_reg_1659.read() == ap_const_lv8_0);
}

void mnist_fp8::thread_tmp_194_cast_fu_8004_p1() {
    tmp_194_cast_fu_8004_p1 = esl_zext<8,4>(p_51_reg_1671.read());
}

void mnist_fp8::thread_tmp_194_fu_3379_p2() {
    tmp_194_fu_3379_p2 = (grp_fu_2090_p2.read() | tmp_191_fu_3373_p2.read());
}

void mnist_fp8::thread_tmp_195_fu_7755_p2() {
    tmp_195_fu_7755_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_7_cast_fu_7732_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_7_cast_fu_7732_p1.read()));
}

void mnist_fp8::thread_tmp_196_fu_3385_p2() {
    tmp_196_fu_3385_p2 = (!p_4_reg_1109.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_4_reg_1109.read() == ap_const_lv5_1D);
}

void mnist_fp8::thread_tmp_197_fu_3391_p2() {
    tmp_197_fu_3391_p2 = (tmp_196_fu_3385_p2.read() | tmp_194_fu_3379_p2.read());
}

void mnist_fp8::thread_tmp_198_fu_4696_p1() {
    tmp_198_fu_4696_p1 = esl_sext<32,8>(tmp_398_reg_10499.read());
}

void mnist_fp8::thread_tmp_199_fu_4699_p2() {
    tmp_199_fu_4699_p2 = (!sh_amt_3_cast_fu_4668_p1.read().is_01())? sc_lv<32>(): tmp_198_fu_4696_p1.read() << (unsigned short)sh_amt_3_cast_fu_4668_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_19_fu_2276_p2() {
    tmp_19_fu_2276_p2 = (!p_s_reg_933.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_933.read() == ap_const_lv5_1F);
}

void mnist_fp8::thread_tmp_1_fu_2129_p3() {
    tmp_1_fu_2129_p3 = esl_concat<5,5>(p_0_reg_816.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_200_fu_5939_p3() {
    tmp_200_fu_5939_p3 = esl_concat<1,8>(tmp_434_reg_10789.read(), p_Repl2_7_trunc_fu_5933_p2.read());
}

void mnist_fp8::thread_tmp_201_fu_3397_p2() {
    tmp_201_fu_3397_p2 = (!p_4_reg_1109.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_4_reg_1109.read() == ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_202_fu_3403_p2() {
    tmp_202_fu_3403_p2 = (tmp_201_fu_3397_p2.read() | tmp_197_fu_3391_p2.read());
}

void mnist_fp8::thread_tmp_203_fu_2758_p2() {
    tmp_203_fu_2758_p2 = (!tmp_164_reg_9938.read().is_01() || !tmp_34_cast_fu_2754_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_164_reg_9938.read()) + sc_biguint<11>(tmp_34_cast_fu_2754_p1.read()));
}

void mnist_fp8::thread_tmp_204_fu_2772_p2() {
    tmp_204_fu_2772_p2 = (!tmp_177_reg_9943.read().is_01() || !tmp_35_cast_fu_2768_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_177_reg_9943.read()) + sc_biguint<7>(tmp_35_cast_fu_2768_p1.read()));
}

void mnist_fp8::thread_tmp_205_fu_5860_p2() {
    tmp_205_fu_5860_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_5_cast_fu_5837_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_5_cast_fu_5837_p1.read()));
}

void mnist_fp8::thread_tmp_206_fu_3409_p2() {
    tmp_206_fu_3409_p2 = (!p_14_reg_1121.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1121.read() == ap_const_lv5_1F);
}

void mnist_fp8::thread_tmp_207_fu_3415_p2() {
    tmp_207_fu_3415_p2 = (!p_14_reg_1121.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1121.read() == ap_const_lv5_1E);
}

void mnist_fp8::thread_tmp_208_fu_3421_p2() {
    tmp_208_fu_3421_p2 = (tmp_207_fu_3415_p2.read() | tmp_206_fu_3409_p2.read());
}

void mnist_fp8::thread_tmp_209_fu_5917_p2() {
    tmp_209_fu_5917_p2 = (!p_Result_16_fu_5907_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_16_fu_5907_p4.read() != ap_const_lv8_9E);
}

void mnist_fp8::thread_tmp_20_cast_fu_2891_p1() {
    tmp_20_cast_fu_2891_p1 = esl_zext<13,5>(p_16_reg_1076.read());
}

void mnist_fp8::thread_tmp_210_fu_6990_p1() {
    tmp_210_fu_6990_p1 = esl_zext<12,11>(p_Result_18_reg_11155.read());
}

void mnist_fp8::thread_tmp_211_fu_5971_p4() {
    tmp_211_fu_5971_p4 = p_03_i1_to_int_fu_5968_p1.read().range(30, 23);
}

void mnist_fp8::thread_tmp_212_fu_6984_p2() {
    tmp_212_fu_6984_p2 = (!tmp_454_fu_6958_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_454_fu_6958_p1.read() == ap_const_lv63_0);
}

void mnist_fp8::thread_tmp_213_fu_3427_p2() {
    tmp_213_fu_3427_p2 = (!p_14_reg_1121.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1121.read() == ap_const_lv5_1D);
}

void mnist_fp8::thread_tmp_214_cast_fu_6771_p1() {
    tmp_214_cast_fu_6771_p1 = esl_zext<10,2>(p_52_reg_1508.read());
}

void mnist_fp8::thread_tmp_214_fu_3433_p2() {
    tmp_214_fu_3433_p2 = (tmp_213_fu_3427_p2.read() | tmp_208_fu_3421_p2.read());
}

void mnist_fp8::thread_tmp_216_fu_3439_p2() {
    tmp_216_fu_3439_p2 = (!p_14_reg_1121.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1121.read() == ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_217_fu_5989_p4() {
    tmp_217_fu_5989_p4 = tmp_140_to_int_fu_5985_p1.read().range(30, 23);
}

void mnist_fp8::thread_tmp_218_fu_3445_p2() {
    tmp_218_fu_3445_p2 = (tmp_216_fu_3439_p2.read() | tmp_214_fu_3433_p2.read());
}

void mnist_fp8::thread_tmp_219_fu_6015_p2() {
    tmp_219_fu_6015_p2 = (notrhs2_fu_6009_p2.read() | notlhs2_fu_6003_p2.read());
}

void mnist_fp8::thread_tmp_21_fu_2908_p4() {
    tmp_21_fu_2908_p4 = conv1_0_load_to_int_fu_2905_p1.read().range(30, 23);
}

void mnist_fp8::thread_tmp_220_fu_6033_p2() {
    tmp_220_fu_6033_p2 = (notrhs3_fu_6027_p2.read() | notlhs3_fu_6021_p2.read());
}

void mnist_fp8::thread_tmp_221_fu_6039_p2() {
    tmp_221_fu_6039_p2 = (tmp_219_fu_6015_p2.read() & tmp_220_fu_6033_p2.read());
}

void mnist_fp8::thread_tmp_223_fu_6045_p2() {
    tmp_223_fu_6045_p2 = (tmp_221_fu_6039_p2.read() & grp_fu_2067_p2.read());
}

void mnist_fp8::thread_tmp_224_fu_9131_p3() {
    tmp_224_fu_9131_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_521_reg_11746.read());
}

void mnist_fp8::thread_tmp_225_fu_6911_p4() {
    tmp_225_fu_6911_p4 = conv3_0_load_to_int_fu_6908_p1.read().range(30, 23);
}

void mnist_fp8::thread_tmp_226_fu_8035_p2() {
    tmp_226_fu_8035_p2 = (!ap_const_lv8_0.is_01() || !p_Val2_1_reg_1659.read().is_01())? sc_lv<8>(): (sc_biguint<8>(ap_const_lv8_0) - sc_biguint<8>(p_Val2_1_reg_1659.read()));
}

void mnist_fp8::thread_tmp_227_fu_5507_p3() {
    tmp_227_fu_5507_p3 = esl_concat<3,5>(p_22_reg_1288.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_228_fu_5519_p3() {
    tmp_228_fu_5519_p3 = esl_concat<3,2>(p_22_reg_1288.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_229_fu_5531_p2() {
    tmp_229_fu_5531_p2 = (!p_shl40_cast_fu_5515_p1.read().is_01() || !p_shl41_cast_fu_5527_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl40_cast_fu_5515_p1.read()) - sc_biguint<9>(p_shl41_cast_fu_5527_p1.read()));
}

void mnist_fp8::thread_tmp_22_fu_2282_p2() {
    tmp_22_fu_2282_p2 = (grp_fu_2083_p2.read() | tmp_19_fu_2276_p2.read());
}

void mnist_fp8::thread_tmp_230_fu_7812_p2() {
    tmp_230_fu_7812_p2 = (!p_Result_21_fu_7802_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_21_fu_7802_p4.read() != ap_const_lv8_9E);
}

void mnist_fp8::thread_tmp_231_fu_6937_p2() {
    tmp_231_fu_6937_p2 = (notrhs4_reg_11139.read() | notlhs4_reg_11134.read());
}

void mnist_fp8::thread_tmp_232_fu_7023_p2() {
    tmp_232_fu_7023_p2 = (!F2_4_fu_7017_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_4_fu_7017_p2.read()) > sc_bigint<12>(ap_const_lv12_6));
}

void mnist_fp8::thread_tmp_233_fu_7029_p2() {
    tmp_233_fu_7029_p2 = (!ap_const_lv12_FFA.is_01() || !F2_4_fu_7017_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFA) + sc_biguint<12>(F2_4_fu_7017_p2.read()));
}

void mnist_fp8::thread_tmp_234_fu_7035_p2() {
    tmp_234_fu_7035_p2 = (!ap_const_lv12_6.is_01() || !F2_4_fu_7017_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_6) - sc_biguint<12>(F2_4_fu_7017_p2.read()));
}

void mnist_fp8::thread_tmp_235_fu_7049_p2() {
    tmp_235_fu_7049_p2 = (!F2_4_fu_7017_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): sc_lv<1>(F2_4_fu_7017_p2.read() == ap_const_lv12_6);
}

void mnist_fp8::thread_tmp_237_fu_5541_p3() {
    tmp_237_fu_5541_p3 = esl_concat<3,4>(p_22_reg_1288.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_238_fu_5553_p3() {
    tmp_238_fu_5553_p3 = esl_concat<3,1>(p_22_reg_1288.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_239_cast_fu_8981_p1() {
    tmp_239_cast_fu_8981_p1 = esl_zext<10,4>(p_55_reg_1740.read());
}

void mnist_fp8::thread_tmp_239_fu_5565_p2() {
    tmp_239_fu_5565_p2 = (!p_shl42_cast_fu_5549_p1.read().is_01() || !p_shl43_cast_fu_5561_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl42_cast_fu_5549_p1.read()) - sc_biguint<8>(p_shl43_cast_fu_5561_p1.read()));
}

void mnist_fp8::thread_tmp_23_fu_2934_p2() {
    tmp_23_fu_2934_p2 = (notrhs_reg_10032.read() | notlhs_reg_10027.read());
}

void mnist_fp8::thread_tmp_240_fu_6941_p2() {
    tmp_240_fu_6941_p2 = (tmp_231_fu_6937_p2.read() & tmp_236_reg_11144.read());
}

void mnist_fp8::thread_tmp_242_fu_7834_p3() {
    tmp_242_fu_7834_p3 = esl_concat<1,8>(is_neg_2_reg_11347.read(), p_Repl2_10_trunc_fu_7828_p2.read());
}

void mnist_fp8::thread_tmp_243_fu_6946_p1() {
    tmp_243_fu_6946_p1 = grp_fu_2061_p1.read();
}

void mnist_fp8::thread_tmp_244_fu_5118_p2() {
    tmp_244_fu_5118_p2 = (!tmp_384_cast_reg_10584.read().is_01() || !tmp_62_cast_fu_5114_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_384_cast_reg_10584.read()) + sc_biguint<10>(tmp_62_cast_fu_5114_p1.read()));
}

void mnist_fp8::thread_tmp_245_fu_7078_p2() {
    tmp_245_fu_7078_p2 = (!sh_amt_4_reg_11182.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_4_reg_11182.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp8::thread_tmp_246_fu_5123_p1() {
    tmp_246_fu_5123_p1 = tmp_244_fu_5118_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_247_fu_7083_p1() {
    tmp_247_fu_7083_p1 = esl_zext<54,32>(sh_amt_4_cast_fu_7075_p1.read());
}

void mnist_fp8::thread_tmp_248_fu_7087_p2() {
    tmp_248_fu_7087_p2 = (!man_V_7_reg_11171.read().is_01() || !tmp_247_fu_7083_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_7_reg_11171.read()) >> (unsigned short)tmp_247_fu_7083_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_249_fu_5135_p3() {
    tmp_249_fu_5135_p3 = esl_concat<10,2>(tmp_244_fu_5118_p2.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_24_cast_fu_2380_p1() {
    tmp_24_cast_fu_2380_p1 = esl_zext<6,5>(p_3_reg_945.read());
}

void mnist_fp8::thread_tmp_250_fu_7103_p1() {
    tmp_250_fu_7103_p1 = esl_sext<32,8>(tmp_457_reg_11194.read());
}

void mnist_fp8::thread_tmp_251_fu_7106_p2() {
    tmp_251_fu_7106_p2 = (!sh_amt_4_cast_fu_7075_p1.read().is_01())? sc_lv<32>(): tmp_250_fu_7103_p1.read() << (unsigned short)sh_amt_4_cast_fu_7075_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_252_fu_5147_p2() {
    tmp_252_fu_5147_p2 = (!p_shl44_cast_fu_5127_p3.read().is_01() || !p_shl45_cast_fu_5143_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl44_cast_fu_5127_p3.read()) - sc_bigint<13>(p_shl45_cast_fu_5143_p1.read()));
}

void mnist_fp8::thread_tmp_253_fu_3457_p2() {
    tmp_253_fu_3457_p2 = (!r_V_4_fu_3451_p2.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): (sc_biguint<5>(r_V_4_fu_3451_p2.read()) < sc_biguint<5>(ap_const_lv5_1C));
}

void mnist_fp8::thread_tmp_254_fu_3463_p2() {
    tmp_254_fu_3463_p2 = (!ap_const_lv5_3.is_01() || !p_14_reg_1121.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_3) + sc_biguint<5>(p_14_reg_1121.read()));
}

void mnist_fp8::thread_tmp_255_cast_fu_9032_p1() {
    tmp_255_cast_fu_9032_p1 = esl_zext<12,4>(p_60_reg_1751.read());
}

void mnist_fp8::thread_tmp_255_fu_3513_p1() {
    tmp_255_fu_3513_p1 = mul1_fu_9682_p2.read().range(26-1, 0);
}

void mnist_fp8::thread_tmp_258_fu_3550_p4() {
    tmp_258_fu_3550_p4 = neg_mul1_fu_3545_p2.read().range(25, 18);
}

void mnist_fp8::thread_tmp_259_fu_8063_p1() {
    tmp_259_fu_8063_p1 = esl_zext<64,2>(p_61_reg_1695.read());
}

void mnist_fp8::thread_tmp_25_cast_fu_2718_p1() {
    tmp_25_cast_fu_2718_p1 = esl_zext<13,5>(p_8_reg_995.read());
}

void mnist_fp8::thread_tmp_25_fu_2288_p2() {
    tmp_25_fu_2288_p2 = (!p_s_reg_933.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_933.read() == ap_const_lv5_1D);
}

void mnist_fp8::thread_tmp_260_cast_fu_7857_p1() {
    tmp_260_cast_fu_7857_p1 = esl_zext<13,5>(p_39_reg_1597.read());
}

void mnist_fp8::thread_tmp_260_fu_3560_p1() {
    tmp_260_fu_3560_p1 = esl_sext<13,8>(tmp_258_fu_3550_p4.read());
}

void mnist_fp8::thread_tmp_262_fu_9049_p4() {
    tmp_262_fu_9049_p4 = conv4_0_load_to_int_fu_9046_p1.read().range(30, 23);
}

void mnist_fp8::thread_tmp_263_fu_3564_p1() {
    tmp_263_fu_3564_p1 = esl_sext<13,10>(tmp_261_reg_10183.read());
}

void mnist_fp8::thread_tmp_264_fu_3567_p3() {
    tmp_264_fu_3567_p3 = (!tmp_256_reg_10175.read()[0].is_01())? sc_lv<13>(): ((tmp_256_reg_10175.read()[0].to_bool())? tmp_260_fu_3560_p1.read(): tmp_263_fu_3564_p1.read());
}

void mnist_fp8::thread_tmp_265_fu_3643_p1() {
    tmp_265_fu_3643_p1 = grp_fu_3587_p2.read().range(9-1, 0);
}

void mnist_fp8::thread_tmp_266_fu_3533_p1() {
    tmp_266_fu_3533_p1 = mul2_fu_9690_p2.read().range(27-1, 0);
}

void mnist_fp8::thread_tmp_267_fu_3598_p4() {
    tmp_267_fu_3598_p4 = neg_mul2_fu_3593_p2.read().range(26, 23);
}

void mnist_fp8::thread_tmp_268_fu_3608_p1() {
    tmp_268_fu_3608_p1 = esl_sext<14,4>(tmp_267_fu_3598_p4.read());
}

void mnist_fp8::thread_tmp_26_fu_2294_p2() {
    tmp_26_fu_2294_p2 = (tmp_25_fu_2288_p2.read() | tmp_22_fu_2282_p2.read());
}

void mnist_fp8::thread_tmp_270_fu_9075_p2() {
    tmp_270_fu_9075_p2 = (notrhs5_reg_11725.read() | notlhs5_reg_11720.read());
}

void mnist_fp8::thread_tmp_272_fu_8801_p2() {
    tmp_272_fu_8801_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_9_cast_fu_8778_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_9_cast_fu_8778_p1.read()));
}

void mnist_fp8::thread_tmp_273_fu_3612_p1() {
    tmp_273_fu_3612_p1 = esl_sext<14,5>(tmp_269_reg_10193.read());
}

void mnist_fp8::thread_tmp_274_fu_9079_p2() {
    tmp_274_fu_9079_p2 = (tmp_270_fu_9075_p2.read() & tmp_271_reg_11730.read());
}

void mnist_fp8::thread_tmp_275_fu_8867_p2() {
    tmp_275_fu_8867_p2 = (!p_Result_28_fu_8857_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_28_fu_8857_p4.read() != ap_const_lv8_9E);
}

void mnist_fp8::thread_tmp_276_fu_3622_p1() {
    tmp_276_fu_3622_p1 = p_v_fu_3615_p3.read().range(2-1, 0);
}

void mnist_fp8::thread_tmp_277_fu_9128_p1() {
    tmp_277_fu_9128_p1 = esl_zext<12,11>(p_Result_24_reg_11741.read());
}

void mnist_fp8::thread_tmp_279_fu_9122_p2() {
    tmp_279_fu_9122_p2 = (!tmp_519_fu_9096_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_519_fu_9096_p1.read() == ap_const_lv63_0);
}

void mnist_fp8::thread_tmp_27_cast_fu_2697_p1() {
    tmp_27_cast_fu_2697_p1 = esl_zext<7,2>(p_12_reg_1007.read());
}

void mnist_fp8::thread_tmp_27_fu_2300_p2() {
    tmp_27_fu_2300_p2 = (!p_s_reg_933.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_933.read() == ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_280_fu_9084_p1() {
    tmp_280_fu_9084_p1 = grp_fu_2061_p1.read();
}

void mnist_fp8::thread_tmp_281_fu_9161_p2() {
    tmp_281_fu_9161_p2 = (!F2_5_fu_9155_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_5_fu_9155_p2.read()) > sc_bigint<12>(ap_const_lv12_6));
}

void mnist_fp8::thread_tmp_282_cast_fu_2501_p1() {
    tmp_282_cast_fu_2501_p1 = esl_sext<64,11>(tmp_93_reg_9871.read());
}

void mnist_fp8::thread_tmp_282_fu_3632_p1() {
    tmp_282_fu_3632_p1 = p_v_fu_3615_p3.read().range(2-1, 0);
}

void mnist_fp8::thread_tmp_283_fu_8889_p3() {
    tmp_283_fu_8889_p3 = esl_concat<1,8>(is_neg_3_reg_11623.read(), p_Repl2_13_trunc_fu_8883_p2.read());
}

void mnist_fp8::thread_tmp_284_fu_8216_p1() {
    tmp_284_fu_8216_p1 = esl_zext<12,11>(exp_tmp_V_2_reg_11497.read());
}

void mnist_fp8::thread_tmp_285_fu_9167_p2() {
    tmp_285_fu_9167_p2 = (!ap_const_lv12_FFA.is_01() || !F2_5_fu_9155_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFA) + sc_biguint<12>(F2_5_fu_9155_p2.read()));
}

void mnist_fp8::thread_tmp_286_fu_8174_p2() {
    tmp_286_fu_8174_p2 = (!tmp_543_fu_8148_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_543_fu_8148_p1.read() == ap_const_lv63_0);
}

void mnist_fp8::thread_tmp_287_fu_9173_p2() {
    tmp_287_fu_9173_p2 = (!ap_const_lv12_6.is_01() || !F2_5_fu_9155_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_6) - sc_biguint<12>(F2_5_fu_9155_p2.read()));
}

void mnist_fp8::thread_tmp_288_fu_9187_p2() {
    tmp_288_fu_9187_p2 = (!F2_5_fu_9155_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): sc_lv<1>(F2_5_fu_9155_p2.read() == ap_const_lv12_6);
}

void mnist_fp8::thread_tmp_289_fu_3647_p3() {
    tmp_289_fu_3647_p3 = esl_concat<2,5>(r_V_6_reg_10203.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_28_cast_fu_2384_p1() {
    tmp_28_cast_fu_2384_p1 = esl_zext<6,5>(tmp_8_fu_2372_p3.read());
}

void mnist_fp8::thread_tmp_28_fu_2938_p2() {
    tmp_28_fu_2938_p2 = (tmp_23_fu_2934_p2.read() & tmp_24_reg_10037.read());
}

void mnist_fp8::thread_tmp_290_cast_fu_2514_p1() {
    tmp_290_cast_fu_2514_p1 = esl_sext<64,11>(tmp_94_reg_9886.read());
}

void mnist_fp8::thread_tmp_290_fu_3658_p3() {
    tmp_290_fu_3658_p3 = esl_concat<2,2>(r_V_6_reg_10203.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_291_fu_3669_p2() {
    tmp_291_fu_3669_p2 = (!p_shl32_cast_fu_3654_p1.read().is_01() || !p_shl33_cast_fu_3665_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl32_cast_fu_3654_p1.read()) - sc_biguint<8>(p_shl33_cast_fu_3665_p1.read()));
}

void mnist_fp8::thread_tmp_292_cast_fu_8844_p1() {
    tmp_292_cast_fu_8844_p1 = esl_zext<12,4>(p_44_reg_1647.read());
}

void mnist_fp8::thread_tmp_292_fu_3679_p2() {
    tmp_292_fu_3679_p2 = (!tmp_265_fu_3643_p1.read().is_01() || !tmp_417_cast_fu_3675_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_265_fu_3643_p1.read()) + sc_bigint<9>(tmp_417_cast_fu_3675_p1.read()));
}

void mnist_fp8::thread_tmp_293_fu_8219_p3() {
    tmp_293_fu_8219_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_545_reg_11502.read());
}

void mnist_fp8::thread_tmp_294_fu_3685_p1() {
    tmp_294_fu_3685_p1 = tmp_292_fu_3679_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_295_fu_3699_p3() {
    tmp_295_fu_3699_p3 = esl_concat<9,2>(tmp_292_reg_10209.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_296_fu_9216_p2() {
    tmp_296_fu_9216_p2 = (!sh_amt_5_reg_11768.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_5_reg_11768.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp8::thread_tmp_297_fu_3710_p2() {
    tmp_297_fu_3710_p2 = (!p_shl30_cast_fu_3692_p3.read().is_01() || !p_shl31_cast_fu_3706_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl30_cast_fu_3692_p3.read()) - sc_bigint<13>(p_shl31_cast_fu_3706_p1.read()));
}

void mnist_fp8::thread_tmp_298_cast_fu_3273_p1() {
    tmp_298_cast_fu_3273_p1 = esl_sext<10,9>(tmp_98_fu_3267_p2.read());
}

void mnist_fp8::thread_tmp_298_fu_3716_p2() {
    tmp_298_fu_3716_p2 = (!tmp_38_cast_fu_3689_p1.read().is_01() || !tmp_297_fu_3710_p2.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_38_cast_fu_3689_p1.read()) + sc_biguint<13>(tmp_297_fu_3710_p2.read()));
}

void mnist_fp8::thread_tmp_299_fu_6080_p3() {
    tmp_299_fu_6080_p3 = esl_concat<3,4>(p_13_reg_1368.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_29_cast_fu_4001_p1() {
    tmp_29_cast_fu_4001_p1 = esl_zext<10,5>(p_10_reg_1162.read());
}

void mnist_fp8::thread_tmp_29_fu_2306_p2() {
    tmp_29_fu_2306_p2 = (tmp_27_fu_2300_p2.read() | tmp_26_fu_2294_p2.read());
}

void mnist_fp8::thread_tmp_2_fu_2159_p2() {
    tmp_2_fu_2159_p2 = (!ap_phi_mux_p_1_phi_fu_831_p4.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_1_phi_fu_831_p4.read() == ap_const_lv5_1C);
}

void mnist_fp8::thread_tmp_300_fu_9221_p1() {
    tmp_300_fu_9221_p1 = esl_zext<54,32>(sh_amt_5_cast_fu_9213_p1.read());
}

void mnist_fp8::thread_tmp_301_fu_9225_p2() {
    tmp_301_fu_9225_p2 = (!man_V_16_reg_11757.read().is_01() || !tmp_300_fu_9221_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_16_reg_11757.read()) >> (unsigned short)tmp_300_fu_9221_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_302_fu_3784_p1() {
    tmp_302_fu_3784_p1 = msb_idx_fu_3778_p2.read().range(31-1, 0);
}

void mnist_fp8::thread_tmp_303_fu_9241_p1() {
    tmp_303_fu_9241_p1 = esl_sext<32,8>(tmp_522_reg_11780.read());
}

void mnist_fp8::thread_tmp_304_fu_9244_p2() {
    tmp_304_fu_9244_p2 = (!sh_amt_5_cast_fu_9213_p1.read().is_01())? sc_lv<32>(): tmp_303_fu_9241_p1.read() << (unsigned short)sh_amt_5_cast_fu_9213_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_306_fu_3806_p4() {
    tmp_306_fu_3806_p4 = msb_idx_1_fu_3796_p3.read().range(30, 5);
}

void mnist_fp8::thread_tmp_307_fu_3837_p1() {
    tmp_307_fu_3837_p1 = msb_idx_1_fu_3796_p3.read().range(3-1, 0);
}

void mnist_fp8::thread_tmp_308_fu_8249_p2() {
    tmp_308_fu_8249_p2 = (!F2_6_fu_8243_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_6_fu_8243_p2.read()) > sc_bigint<12>(ap_const_lv12_6));
}

void mnist_fp8::thread_tmp_309_fu_8255_p2() {
    tmp_309_fu_8255_p2 = (!ap_const_lv12_FFA.is_01() || !F2_6_fu_8243_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFA) + sc_biguint<12>(F2_6_fu_8243_p2.read()));
}

void mnist_fp8::thread_tmp_310_fu_8261_p2() {
    tmp_310_fu_8261_p2 = (!ap_const_lv12_6.is_01() || !F2_6_fu_8243_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_6) - sc_biguint<12>(F2_6_fu_8243_p2.read()));
}

void mnist_fp8::thread_tmp_311_fu_8275_p2() {
    tmp_311_fu_8275_p2 = (!F2_6_fu_8243_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): sc_lv<1>(F2_6_fu_8243_p2.read() == ap_const_lv12_6);
}

void mnist_fp8::thread_tmp_312_fu_3841_p2() {
    tmp_312_fu_3841_p2 = (!ap_const_lv3_1.is_01() || !tmp_307_fu_3837_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(tmp_307_fu_3837_p1.read()));
}

void mnist_fp8::thread_tmp_313_fu_3847_p1() {
    tmp_313_fu_3847_p1 = esl_zext<8,3>(tmp_312_fu_3841_p2.read());
}

void mnist_fp8::thread_tmp_314_fu_5591_p2() {
    tmp_314_fu_5591_p2 = (!lhs_V_5_cast_fu_5587_p1.read().is_01() || !tmp_399_cast_reg_10716.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_5_cast_fu_5587_p1.read()) + sc_bigint<9>(tmp_399_cast_reg_10716.read()));
}

void mnist_fp8::thread_tmp_315_fu_3888_p1() {
    tmp_315_fu_3888_p1 = msb_idx_reg_10250.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_316_fu_5596_p1() {
    tmp_316_fu_5596_p1 = tmp_314_fu_5591_p2.read().range(7-1, 0);
}

void mnist_fp8::thread_tmp_317_fu_8389_p2() {
    tmp_317_fu_8389_p2 = (!sh_amt_6_reg_11546.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_6_reg_11546.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp8::thread_tmp_318_fu_5608_p3() {
    tmp_318_fu_5608_p3 = esl_concat<9,1>(tmp_314_fu_5591_p2.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_319_fu_5620_p2() {
    tmp_319_fu_5620_p2 = (!p_shl46_cast_fu_5600_p3.read().is_01() || !p_shl47_cast_fu_5616_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl46_cast_fu_5600_p3.read()) - sc_bigint<11>(p_shl47_cast_fu_5616_p1.read()));
}

void mnist_fp8::thread_tmp_31_fu_2943_p1() {
    tmp_31_fu_2943_p1 = grp_fu_2061_p1.read();
}

void mnist_fp8::thread_tmp_320_fu_8394_p1() {
    tmp_320_fu_8394_p1 = esl_zext<54,32>(sh_amt_6_cast_fu_8386_p1.read());
}

void mnist_fp8::thread_tmp_321_fu_8398_p2() {
    tmp_321_fu_8398_p2 = (!man_V_12_reg_11535.read().is_01() || !tmp_320_fu_8394_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_12_reg_11535.read()) >> (unsigned short)tmp_320_fu_8394_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_322_fu_5169_p2() {
    tmp_322_fu_5169_p2 = (!tmp_77_cast_fu_5165_p1.read().is_01() || !tmp_252_reg_10597.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_77_cast_fu_5165_p1.read()) + sc_biguint<13>(tmp_252_reg_10597.read()));
}

void mnist_fp8::thread_tmp_323_fu_8414_p1() {
    tmp_323_fu_8414_p1 = esl_sext<32,8>(tmp_546_reg_11558.read());
}

void mnist_fp8::thread_tmp_324_fu_8417_p2() {
    tmp_324_fu_8417_p2 = (!sh_amt_6_cast_fu_8386_p1.read().is_01())? sc_lv<32>(): tmp_323_fu_8414_p1.read() << (unsigned short)sh_amt_6_cast_fu_8386_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_325_fu_5192_p1() {
    tmp_325_fu_5192_p1 = conv2_0_load_to_int_fu_5179_p1.read().range(23-1, 0);
}

void mnist_fp8::thread_tmp_326_fu_5229_p1() {
    tmp_326_fu_5229_p1 = ireg_V_3_fu_5221_p3.read().range(63-1, 0);
}

void mnist_fp8::thread_tmp_327_cast1_fu_3959_p1() {
    tmp_327_cast1_fu_3959_p1 = esl_zext<6,5>(tmp_110_fu_3951_p3.read());
}

void mnist_fp8::thread_tmp_327_cast_fu_3963_p1() {
    tmp_327_cast_fu_3963_p1 = esl_zext<9,5>(tmp_110_fu_3951_p3.read());
}

void mnist_fp8::thread_tmp_328_fu_5251_p1() {
    tmp_328_fu_5251_p1 = ireg_V_3_fu_5221_p3.read().range(52-1, 0);
}

void mnist_fp8::thread_tmp_329_fu_5326_p1() {
    tmp_329_fu_5326_p1 = man_V_6_fu_5281_p3.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_32_fu_2987_p1() {
    tmp_32_fu_2987_p1 = esl_zext<12,11>(p_Result_2_reg_10048.read());
}

void mnist_fp8::thread_tmp_330_fu_5330_p4() {
    tmp_330_fu_5330_p4 = sh_amt_1_fu_5312_p3.read().range(11, 3);
}

void mnist_fp8::thread_tmp_331_cast_fu_8130_p1() {
    tmp_331_cast_fu_8130_p1 = esl_zext<11,2>(p_66_reg_1718.read());
}

void mnist_fp8::thread_tmp_331_fu_5363_p1() {
    tmp_331_fu_5363_p1 = tmp_145_fu_5358_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_332_fu_5383_p1() {
    tmp_332_fu_5383_p1 = tmp_153_fu_5377_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_333_fu_4981_p2() {
    tmp_333_fu_4981_p2 = (!tmp_190_reg_10316.read().is_01() || !tmp_127_cast_fu_4977_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_190_reg_10316.read()) + sc_biguint<13>(tmp_127_cast_fu_4977_p1.read()));
}

void mnist_fp8::thread_tmp_334_fu_8301_p1() {
    tmp_334_fu_8301_p1 = esl_zext<12,11>(exp_tmp_V_3_reg_11519.read());
}

void mnist_fp8::thread_tmp_335_fu_8304_p3() {
    tmp_335_fu_8304_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_552_reg_11524.read());
}

void mnist_fp8::thread_tmp_336_fu_8210_p2() {
    tmp_336_fu_8210_p2 = (!tmp_550_fu_8184_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_550_fu_8184_p1.read() == ap_const_lv63_0);
}

void mnist_fp8::thread_tmp_337_fu_8334_p2() {
    tmp_337_fu_8334_p2 = (!F2_7_fu_8328_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_7_fu_8328_p2.read()) > sc_bigint<12>(ap_const_lv12_6));
}

void mnist_fp8::thread_tmp_338_fu_8340_p2() {
    tmp_338_fu_8340_p2 = (!ap_const_lv12_FFA.is_01() || !F2_7_fu_8328_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFA) + sc_biguint<12>(F2_7_fu_8328_p2.read()));
}

void mnist_fp8::thread_tmp_339_fu_8346_p2() {
    tmp_339_fu_8346_p2 = (!ap_const_lv12_6.is_01() || !F2_7_fu_8328_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_6) - sc_biguint<12>(F2_7_fu_8328_p2.read()));
}

void mnist_fp8::thread_tmp_33_fu_2990_p3() {
    tmp_33_fu_2990_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_146_reg_10053.read());
}

void mnist_fp8::thread_tmp_340_fu_8360_p2() {
    tmp_340_fu_8360_p2 = (!F2_7_fu_8328_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): sc_lv<1>(F2_7_fu_8328_p2.read() == ap_const_lv12_6);
}

void mnist_fp8::thread_tmp_341_fu_4893_p1() {
    tmp_341_fu_4893_p1 = msb_idx_2_fu_4887_p2.read().range(31-1, 0);
}

void mnist_fp8::thread_tmp_342_fu_8538_p2() {
    tmp_342_fu_8538_p2 = (!sh_amt_7_reg_11580.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_7_reg_11580.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp8::thread_tmp_344_fu_8543_p1() {
    tmp_344_fu_8543_p1 = esl_zext<54,32>(sh_amt_7_cast_fu_8535_p1.read());
}

void mnist_fp8::thread_tmp_345_fu_8547_p2() {
    tmp_345_fu_8547_p2 = (!man_V_15_reg_11569.read().is_01() || !tmp_344_fu_8543_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_15_reg_11569.read()) >> (unsigned short)tmp_344_fu_8543_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_346_fu_4915_p4() {
    tmp_346_fu_4915_p4 = msb_idx_3_fu_4905_p3.read().range(30, 5);
}

void mnist_fp8::thread_tmp_347_fu_8563_p1() {
    tmp_347_fu_8563_p1 = esl_sext<32,8>(tmp_553_reg_11592.read());
}

void mnist_fp8::thread_tmp_348_fu_8566_p2() {
    tmp_348_fu_8566_p2 = (!sh_amt_7_cast_fu_8535_p1.read().is_01())? sc_lv<32>(): tmp_347_fu_8563_p1.read() << (unsigned short)sh_amt_7_cast_fu_8535_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_349_fu_4946_p1() {
    tmp_349_fu_4946_p1 = msb_idx_3_fu_4905_p3.read().range(3-1, 0);
}

void mnist_fp8::thread_tmp_34_cast_fu_2754_p1() {
    tmp_34_cast_fu_2754_p1 = esl_zext<11,5>(r_V_2_fu_2748_p2.read());
}

void mnist_fp8::thread_tmp_34_fu_2194_p2() {
    tmp_34_fu_2194_p2 = (!tmp_6_reg_9777.read().is_01() || !tmp_6_cast_fu_2190_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_6_reg_9777.read()) + sc_biguint<11>(tmp_6_cast_fu_2190_p1.read()));
}

void mnist_fp8::thread_tmp_350_cast_fu_3985_p1() {
    tmp_350_cast_fu_3985_p1 = esl_sext<10,9>(tmp_116_fu_3979_p2.read());
}

void mnist_fp8::thread_tmp_350_fu_4950_p2() {
    tmp_350_fu_4950_p2 = (!ap_const_lv3_1.is_01() || !tmp_349_fu_4946_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(tmp_349_fu_4946_p1.read()));
}

void mnist_fp8::thread_tmp_351_fu_4956_p1() {
    tmp_351_fu_4956_p1 = esl_zext<8,3>(tmp_350_fu_4950_p2.read());
}

void mnist_fp8::thread_tmp_352_fu_4068_p2() {
    tmp_352_fu_4068_p2 = (!tmp_327_cast1_reg_10298.read().is_01() || !tmp_64_cast_fu_4064_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_327_cast1_reg_10298.read()) + sc_biguint<6>(tmp_64_cast_fu_4064_p1.read()));
}

void mnist_fp8::thread_tmp_353_fu_5006_p1() {
    tmp_353_fu_5006_p1 = msb_idx_2_reg_10541.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_354_fu_4077_p3() {
    tmp_354_fu_4077_p3 = esl_concat<6,2>(tmp_352_fu_4068_p2.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_355_fu_4089_p2() {
    tmp_355_fu_4089_p2 = (!p_shl_fu_4085_p1.read().is_01() || !tmp_431_cast_fu_4073_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl_fu_4085_p1.read()) - sc_biguint<64>(tmp_431_cast_fu_4073_p1.read()));
}

void mnist_fp8::thread_tmp_356_fu_4095_p3() {
    tmp_356_fu_4095_p3 = esl_concat<3,5>(p_24_reg_1198.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_357_fu_4107_p3() {
    tmp_357_fu_4107_p3 = esl_concat<3,1>(p_24_reg_1198.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_358_fu_4119_p2() {
    tmp_358_fu_4119_p2 = (!p_shl49_cast_fu_4103_p1.read().is_01() || !p_shl50_cast_fu_4115_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl49_cast_fu_4103_p1.read()) - sc_biguint<9>(p_shl50_cast_fu_4115_p1.read()));
}

void mnist_fp8::thread_tmp_359_fu_3930_p2() {
    tmp_359_fu_3930_p2 = (!tmp_134_reg_10134.read().is_01() || !tmp_93_cast_fu_3926_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_134_reg_10134.read()) + sc_biguint<13>(tmp_93_cast_fu_3926_p1.read()));
}

void mnist_fp8::thread_tmp_35_cast_fu_2768_p1() {
    tmp_35_cast_fu_2768_p1 = esl_zext<7,2>(p_17_reg_1031.read());
}

void mnist_fp8::thread_tmp_35_fu_2312_p2() {
    tmp_35_fu_2312_p2 = (!p_3_reg_945.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_945.read() == ap_const_lv5_1F);
}

void mnist_fp8::thread_tmp_360_fu_6516_p3() {
    tmp_360_fu_6516_p3 = esl_concat<4,2>(p_15_reg_1425.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_361_fu_6528_p3() {
    tmp_361_fu_6528_p3 = esl_concat<4,4>(p_15_reg_1425.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_362_fu_6540_p3() {
    tmp_362_fu_6540_p3 = esl_concat<4,1>(p_15_reg_1425.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_363_fu_6552_p2() {
    tmp_363_fu_6552_p2 = (!p_shl51_cast_fu_6536_p1.read().is_01() || !p_shl52_cast_fu_6548_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl51_cast_fu_6536_p1.read()) - sc_biguint<9>(p_shl52_cast_fu_6548_p1.read()));
}

void mnist_fp8::thread_tmp_364_fu_6120_p2() {
    tmp_364_fu_6120_p2 = (!lhs_V_1_cast_fu_6116_p1.read().is_01() || !tmp_424_cast_reg_10871.read().is_01())? sc_lv<8>(): (sc_biguint<8>(lhs_V_1_cast_fu_6116_p1.read()) + sc_biguint<8>(tmp_424_cast_reg_10871.read()));
}

void mnist_fp8::thread_tmp_365_fu_6133_p1() {
    tmp_365_fu_6133_p1 = p_20_reg_1390.read().range(4-1, 0);
}

void mnist_fp8::thread_tmp_366_fu_6149_p2() {
    tmp_366_fu_6149_p2 = (!ap_const_lv5_1.is_01())? sc_lv<5>(): p_20_reg_1390.read() << (unsigned short)ap_const_lv5_1.to_uint();
}

void mnist_fp8::thread_tmp_367_fu_5650_p2() {
    tmp_367_fu_5650_p2 = (!lhs_V_7_cast_fu_5646_p1.read().is_01() || !tmp_319_reg_10729.read().is_01())? sc_lv<11>(): (sc_biguint<11>(lhs_V_7_cast_fu_5646_p1.read()) + sc_biguint<11>(tmp_319_reg_10729.read()));
}

void mnist_fp8::thread_tmp_368_cast_fu_2900_p1() {
    tmp_368_cast_fu_2900_p1 = esl_zext<64,13>(tmp_137_fu_2895_p2.read());
}

void mnist_fp8::thread_tmp_368_fu_4161_p2() {
    tmp_368_fu_4161_p2 = (!tmp_436_cast_reg_10342.read().is_01() || !tmp_80_cast_fu_4157_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_436_cast_reg_10342.read()) + sc_biguint<10>(tmp_80_cast_fu_4157_p1.read()));
}

void mnist_fp8::thread_tmp_369_cast_fu_2727_p1() {
    tmp_369_cast_fu_2727_p1 = esl_zext<64,13>(tmp_158_fu_2722_p2.read());
}

void mnist_fp8::thread_tmp_369_fu_4166_p1() {
    tmp_369_fu_4166_p1 = tmp_368_fu_4161_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_370_fu_4178_p3() {
    tmp_370_fu_4178_p3 = esl_concat<10,1>(tmp_368_fu_4161_p2.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_371_fu_4190_p2() {
    tmp_371_fu_4190_p2 = (!p_shl53_cast_fu_4170_p3.read().is_01() || !p_shl55_cast_fu_4186_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl53_cast_fu_4170_p3.read()) - sc_bigint<13>(p_shl55_cast_fu_4186_p1.read()));
}

void mnist_fp8::thread_tmp_372_fu_4200_p2() {
    tmp_372_fu_4200_p2 = (!tmp_355_reg_10337.read().is_01() || !tmp_81_fu_4196_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_355_reg_10337.read()) + sc_biguint<64>(tmp_81_fu_4196_p1.read()));
}

void mnist_fp8::thread_tmp_373_fu_4205_p1() {
    tmp_373_fu_4205_p1 = tmp_372_fu_4200_p2.read().range(9-1, 0);
}

void mnist_fp8::thread_tmp_374_fu_4209_p1() {
    tmp_374_fu_4209_p1 = tmp_372_fu_4200_p2.read().range(7-1, 0);
}

void mnist_fp8::thread_tmp_375_fu_4221_p2() {
    tmp_375_fu_4221_p2 = (!p_shl54_cast_fu_4213_p3.read().is_01() || !tmp_373_fu_4205_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl54_cast_fu_4213_p3.read()) - sc_biguint<9>(tmp_373_fu_4205_p1.read()));
}

void mnist_fp8::thread_tmp_376_fu_6797_p3() {
    tmp_376_fu_6797_p3 = esl_concat<4,4>(p_36_reg_1531.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_377_fu_6809_p3() {
    tmp_377_fu_6809_p3 = esl_concat<4,1>(p_36_reg_1531.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_378_fu_6821_p2() {
    tmp_378_fu_6821_p2 = (!p_shl56_cast_fu_6805_p1.read().is_01() || !p_shl57_cast_fu_6817_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl56_cast_fu_6805_p1.read()) - sc_biguint<9>(p_shl57_cast_fu_6817_p1.read()));
}

void mnist_fp8::thread_tmp_379_fu_6578_p2() {
    tmp_379_fu_6578_p2 = (!tmp_442_cast_reg_10998.read().is_01() || !tmp_66_cast_fu_6574_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_442_cast_reg_10998.read()) + sc_biguint<10>(tmp_66_cast_fu_6574_p1.read()));
}

void mnist_fp8::thread_tmp_37_fu_3469_p3() {
    tmp_37_fu_3469_p3 = (!tmp_253_fu_3457_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_253_fu_3457_p2.read()[0].to_bool())? r_V_4_fu_3451_p2.read(): tmp_254_fu_3463_p2.read());
}

void mnist_fp8::thread_tmp_380_fu_6583_p1() {
    tmp_380_fu_6583_p1 = tmp_379_fu_6578_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_381_fu_6595_p3() {
    tmp_381_fu_6595_p3 = esl_concat<10,1>(tmp_379_fu_6578_p2.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_382_fu_6607_p2() {
    tmp_382_fu_6607_p2 = (!p_shl58_cast_fu_6587_p3.read().is_01() || !p_shl59_cast_fu_6603_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl58_cast_fu_6587_p3.read()) - sc_bigint<12>(p_shl59_cast_fu_6603_p1.read()));
}

void mnist_fp8::thread_tmp_383_fu_5693_p2() {
    tmp_383_fu_5693_p2 = (!tmp_396_cast_reg_10711.read().is_01() || !tmp_136_cast_cast_fu_5689_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_396_cast_reg_10711.read()) + sc_biguint<10>(tmp_136_cast_cast_fu_5689_p1.read()));
}

void mnist_fp8::thread_tmp_384_cast_fu_5098_p1() {
    tmp_384_cast_fu_5098_p1 = esl_sext<10,9>(tmp_183_fu_5092_p2.read());
}

void mnist_fp8::thread_tmp_384_fu_5698_p1() {
    tmp_384_fu_5698_p1 = tmp_383_fu_5693_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_385_fu_5710_p3() {
    tmp_385_fu_5710_p3 = esl_concat<10,2>(tmp_383_fu_5693_p2.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_386_fu_5722_p2() {
    tmp_386_fu_5722_p2 = (!p_shl60_cast_fu_5702_p3.read().is_01() || !p_shl61_cast_fu_5718_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl60_cast_fu_5702_p3.read()) - sc_bigint<13>(p_shl61_cast_fu_5718_p1.read()));
}

void mnist_fp8::thread_tmp_387_fu_4253_p2() {
    tmp_387_fu_4253_p2 = (!tmp_104_cast_fu_4249_p1.read().is_01() || !tmp_371_reg_10360.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_104_cast_fu_4249_p1.read()) + sc_biguint<13>(tmp_371_reg_10360.read()));
}

void mnist_fp8::thread_tmp_388_fu_4281_p1() {
    tmp_388_fu_4281_p1 = ireg_V_8_fu_4277_p1.read().range(63-1, 0);
}

void mnist_fp8::thread_tmp_389_cast_fu_2763_p1() {
    tmp_389_cast_fu_2763_p1 = esl_sext<64,11>(tmp_203_fu_2758_p2.read());
}

void mnist_fp8::thread_tmp_389_fu_4267_p2() {
    tmp_389_fu_4267_p2 = (!tmp_169_cast_fu_4263_p1.read().is_01() || !tmp_375_reg_10365.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_169_cast_fu_4263_p1.read()) + sc_biguint<9>(tmp_375_reg_10365.read()));
}

void mnist_fp8::thread_tmp_38_cast_fu_3689_p1() {
    tmp_38_cast_fu_3689_p1 = esl_zext<13,5>(tmp_37_reg_10160.read());
}

void mnist_fp8::thread_tmp_38_fu_2318_p2() {
    tmp_38_fu_2318_p2 = (!p_3_reg_945.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_945.read() == ap_const_lv5_1E);
}

void mnist_fp8::thread_tmp_390_cast_fu_2777_p1() {
    tmp_390_cast_fu_2777_p1 = esl_zext<64,7>(tmp_204_fu_2772_p2.read());
}

void mnist_fp8::thread_tmp_390_fu_4303_p1() {
    tmp_390_fu_4303_p1 = ireg_V_8_fu_4277_p1.read().range(52-1, 0);
}

void mnist_fp8::thread_tmp_391_fu_4414_p1() {
    tmp_391_fu_4414_p1 = man_V_4_fu_4369_p3.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_392_fu_4418_p4() {
    tmp_392_fu_4418_p4 = sh_amt_2_fu_4400_p3.read().range(11, 3);
}

void mnist_fp8::thread_tmp_393_fu_4536_p1() {
    tmp_393_fu_4536_p1 = tmp_163_fu_4531_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_394_fu_4556_p1() {
    tmp_394_fu_4556_p1 = tmp_166_fu_4550_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_395_fu_4317_p1() {
    tmp_395_fu_4317_p1 = ireg_V_9_fu_4313_p1.read().range(63-1, 0);
}

void mnist_fp8::thread_tmp_396_cast_fu_5537_p1() {
    tmp_396_cast_fu_5537_p1 = esl_sext<10,9>(tmp_229_fu_5531_p2.read());
}

void mnist_fp8::thread_tmp_396_fu_6225_p2() {
    tmp_396_fu_6225_p2 = (!r_V_11_fu_6219_p2.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): (sc_biguint<4>(r_V_11_fu_6219_p2.read()) < sc_biguint<4>(ap_const_lv4_E));
}

void mnist_fp8::thread_tmp_397_fu_4339_p1() {
    tmp_397_fu_4339_p1 = ireg_V_9_fu_4313_p1.read().range(52-1, 0);
}

void mnist_fp8::thread_tmp_398_fu_4499_p1() {
    tmp_398_fu_4499_p1 = man_V_9_fu_4454_p3.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_399_cast_fu_5571_p1() {
    tmp_399_cast_fu_5571_p1 = esl_sext<9,8>(tmp_239_fu_5565_p2.read());
}

void mnist_fp8::thread_tmp_399_fu_4503_p4() {
    tmp_399_fu_4503_p4 = sh_amt_3_fu_4485_p3.read().range(11, 3);
}

void mnist_fp8::thread_tmp_39_fu_2981_p2() {
    tmp_39_fu_2981_p2 = (!tmp_139_fu_2955_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_139_fu_2955_p1.read() == ap_const_lv63_0);
}

void mnist_fp8::thread_tmp_400_fu_4685_p1() {
    tmp_400_fu_4685_p1 = tmp_185_fu_4680_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_401_fu_4705_p1() {
    tmp_401_fu_4705_p1 = tmp_199_fu_4699_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_402_fu_6215_p1() {
    tmp_402_fu_6215_p1 = p_30_reg_1401.read().range(4-1, 0);
}

void mnist_fp8::thread_tmp_403_fu_6231_p2() {
    tmp_403_fu_6231_p2 = (!ap_const_lv4_1.is_01() || !tmp_402_fu_6215_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_402_fu_6215_p1.read()));
}

void mnist_fp8::thread_tmp_404_fu_6277_p1() {
    tmp_404_fu_6277_p1 = mul3_fu_9707_p2.read().range(22-1, 0);
}

void mnist_fp8::thread_tmp_406_fu_6314_p4() {
    tmp_406_fu_6314_p4 = neg_mul3_fu_6309_p2.read().range(21, 15);
}

void mnist_fp8::thread_tmp_407_fu_6324_p1() {
    tmp_407_fu_6324_p1 = esl_sext<11,7>(tmp_406_fu_6314_p4.read());
}

void mnist_fp8::thread_tmp_409_fu_6328_p1() {
    tmp_409_fu_6328_p1 = esl_sext<11,9>(tmp_408_reg_10934.read());
}

void mnist_fp8::thread_tmp_40_fu_2324_p2() {
    tmp_40_fu_2324_p2 = (tmp_38_fu_2318_p2.read() | tmp_35_fu_2312_p2.read());
}

void mnist_fp8::thread_tmp_410_fu_6331_p3() {
    tmp_410_fu_6331_p3 = (!tmp_405_reg_10926.read()[0].is_01())? sc_lv<11>(): ((tmp_405_reg_10926.read()[0].to_bool())? tmp_407_fu_6324_p1.read(): tmp_409_fu_6328_p1.read());
}

void mnist_fp8::thread_tmp_411_fu_6407_p1() {
    tmp_411_fu_6407_p1 = grp_fu_6351_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_412_fu_6297_p1() {
    tmp_412_fu_6297_p1 = mul4_fu_9715_p2.read().range(23-1, 0);
}

void mnist_fp8::thread_tmp_413_fu_6362_p4() {
    tmp_413_fu_6362_p4 = neg_mul4_fu_6357_p2.read().range(22, 19);
}

void mnist_fp8::thread_tmp_414_fu_6372_p1() {
    tmp_414_fu_6372_p1 = esl_sext<12,4>(tmp_413_fu_6362_p4.read());
}

void mnist_fp8::thread_tmp_416_fu_6376_p1() {
    tmp_416_fu_6376_p1 = esl_sext<12,5>(tmp_415_reg_10944.read());
}

void mnist_fp8::thread_tmp_417_cast_fu_3675_p1() {
    tmp_417_cast_fu_3675_p1 = esl_sext<9,8>(tmp_291_fu_3669_p2.read());
}

void mnist_fp8::thread_tmp_417_fu_6386_p1() {
    tmp_417_fu_6386_p1 = p_v1_fu_6379_p3.read().range(2-1, 0);
}

void mnist_fp8::thread_tmp_418_fu_6396_p1() {
    tmp_418_fu_6396_p1 = p_v1_fu_6379_p3.read().range(2-1, 0);
}

void mnist_fp8::thread_tmp_419_fu_6411_p3() {
    tmp_419_fu_6411_p3 = esl_concat<2,4>(r_V_12_reg_10954.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_41_fu_2330_p2() {
    tmp_41_fu_2330_p2 = (!p_3_reg_945.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_945.read() == ap_const_lv5_1D);
}

void mnist_fp8::thread_tmp_420_fu_6422_p3() {
    tmp_420_fu_6422_p3 = esl_concat<2,1>(r_V_12_reg_10954.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_421_fu_6433_p2() {
    tmp_421_fu_6433_p2 = (!p_shl62_cast_fu_6418_p1.read().is_01() || !p_shl63_cast_fu_6429_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl62_cast_fu_6418_p1.read()) - sc_biguint<7>(p_shl63_cast_fu_6429_p1.read()));
}

void mnist_fp8::thread_tmp_422_cast_fu_3722_p1() {
    tmp_422_cast_fu_3722_p1 = esl_zext<64,13>(tmp_298_fu_3716_p2.read());
}

void mnist_fp8::thread_tmp_422_fu_6443_p2() {
    tmp_422_fu_6443_p2 = (!tmp_411_fu_6407_p1.read().is_01() || !tmp_482_cast_fu_6439_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_411_fu_6407_p1.read()) + sc_bigint<8>(tmp_482_cast_fu_6439_p1.read()));
}

void mnist_fp8::thread_tmp_423_fu_6449_p1() {
    tmp_423_fu_6449_p1 = tmp_422_fu_6443_p2.read().range(7-1, 0);
}

void mnist_fp8::thread_tmp_424_cast_fu_6088_p1() {
    tmp_424_cast_fu_6088_p1 = esl_zext<8,7>(tmp_299_fu_6080_p3.read());
}

void mnist_fp8::thread_tmp_424_fu_6463_p3() {
    tmp_424_fu_6463_p3 = esl_concat<8,1>(tmp_422_reg_10960.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_425_fu_6474_p2() {
    tmp_425_fu_6474_p2 = (!p_shl64_cast_fu_6456_p3.read().is_01() || !p_shl65_cast_fu_6470_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl64_cast_fu_6456_p3.read()) - sc_bigint<11>(p_shl65_cast_fu_6470_p1.read()));
}

void mnist_fp8::thread_tmp_426_fu_6480_p2() {
    tmp_426_fu_6480_p2 = (!tmp_88_cast_fu_6453_p1.read().is_01() || !tmp_425_fu_6474_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_88_cast_fu_6453_p1.read()) + sc_biguint<11>(tmp_425_fu_6474_p2.read()));
}

void mnist_fp8::thread_tmp_427_fu_6495_p2() {
    tmp_427_fu_6495_p2 = (!tmp_445_cast_reg_10884.read().is_01() || !tmp_110_cast_fu_6491_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_445_cast_reg_10884.read()) + sc_biguint<12>(tmp_110_cast_fu_6491_p1.read()));
}

void mnist_fp8::thread_tmp_428_fu_7246_p3() {
    tmp_428_fu_7246_p3 = esl_concat<4,4>(p_21_reg_1564.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_429_cast_fu_5174_p1() {
    tmp_429_cast_fu_5174_p1 = esl_zext<64,13>(tmp_322_fu_5169_p2.read());
}

void mnist_fp8::thread_tmp_429_fu_6847_p2() {
    tmp_429_fu_6847_p2 = (!tmp_458_cast_reg_11091.read().is_01() || !tmp_150_cast_fu_6843_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_458_cast_reg_11091.read()) + sc_biguint<10>(tmp_150_cast_fu_6843_p1.read()));
}

void mnist_fp8::thread_tmp_42_fu_2336_p2() {
    tmp_42_fu_2336_p2 = (tmp_41_fu_2330_p2.read() | tmp_40_fu_2324_p2.read());
}

void mnist_fp8::thread_tmp_430_cast_fu_5052_p1() {
    tmp_430_cast_fu_5052_p1 = esl_zext<64,13>(tmp_333_reg_10561.read());
}

void mnist_fp8::thread_tmp_430_fu_6852_p1() {
    tmp_430_fu_6852_p1 = tmp_429_fu_6847_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_431_cast_fu_4073_p1() {
    tmp_431_cast_fu_4073_p1 = esl_zext<64,6>(tmp_352_fu_4068_p2.read());
}

void mnist_fp8::thread_tmp_431_fu_6864_p3() {
    tmp_431_fu_6864_p3 = esl_concat<10,1>(tmp_429_fu_6847_p2.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_432_fu_6876_p2() {
    tmp_432_fu_6876_p2 = (!p_shl66_cast_fu_6856_p3.read().is_01() || !p_shl67_cast_fu_6872_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl66_cast_fu_6856_p3.read()) - sc_bigint<12>(p_shl67_cast_fu_6872_p1.read()));
}

void mnist_fp8::thread_tmp_433_fu_5753_p2() {
    tmp_433_fu_5753_p2 = (!tmp_386_reg_10765.read().is_01() || !tmp_154_cast_cast_fu_5749_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_386_reg_10765.read()) + sc_biguint<13>(tmp_154_cast_cast_fu_5749_p1.read()));
}

void mnist_fp8::thread_tmp_435_fu_5819_p1() {
    tmp_435_fu_5819_p1 = msb_idx_4_fu_5813_p2.read().range(31-1, 0);
}

void mnist_fp8::thread_tmp_436_cast_fu_4125_p1() {
    tmp_436_cast_fu_4125_p1 = esl_sext<10,9>(tmp_358_fu_4119_p2.read());
}

void mnist_fp8::thread_tmp_437_cast_fu_3935_p1() {
    tmp_437_cast_fu_3935_p1 = esl_zext<64,13>(tmp_359_reg_10285.read());
}

void mnist_fp8::thread_tmp_437_fu_5841_p4() {
    tmp_437_fu_5841_p4 = msb_idx_5_fu_5831_p3.read().range(30, 5);
}

void mnist_fp8::thread_tmp_438_fu_5872_p1() {
    tmp_438_fu_5872_p1 = msb_idx_5_fu_5831_p3.read().range(3-1, 0);
}

void mnist_fp8::thread_tmp_439_cast_fu_6524_p1() {
    tmp_439_cast_fu_6524_p1 = esl_zext<7,6>(tmp_360_fu_6516_p3.read());
}

void mnist_fp8::thread_tmp_439_fu_5876_p2() {
    tmp_439_fu_5876_p2 = (!ap_const_lv3_1.is_01() || !tmp_438_fu_5872_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(tmp_438_fu_5872_p1.read()));
}

void mnist_fp8::thread_tmp_43_fu_3020_p2() {
    tmp_43_fu_3020_p2 = (!F2_fu_3014_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_fu_3014_p2.read()) > sc_bigint<12>(ap_const_lv12_6));
}

void mnist_fp8::thread_tmp_440_fu_5882_p1() {
    tmp_440_fu_5882_p1 = esl_zext<8,3>(tmp_439_fu_5876_p2.read());
}

void mnist_fp8::thread_tmp_441_fu_5886_p2() {
    tmp_441_fu_5886_p2 = (!tmp_440_fu_5882_p1.read().is_01())? sc_lv<8>(): tmp_V_2_reg_10805.read() >> (unsigned short)tmp_440_fu_5882_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_442_cast_fu_6558_p1() {
    tmp_442_cast_fu_6558_p1 = esl_sext<10,9>(tmp_363_fu_6552_p2.read());
}

void mnist_fp8::thread_tmp_442_fu_5923_p1() {
    tmp_442_fu_5923_p1 = msb_idx_4_reg_10811.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_443_fu_5981_p1() {
    tmp_443_fu_5981_p1 = p_03_i1_to_int_fu_5968_p1.read().range(23-1, 0);
}

void mnist_fp8::thread_tmp_444_fu_5999_p1() {
    tmp_444_fu_5999_p1 = tmp_140_to_int_fu_5985_p1.read().range(23-1, 0);
}

void mnist_fp8::thread_tmp_445_cast_fu_6125_p3() {
    tmp_445_cast_fu_6125_p3 = esl_concat<8,4>(tmp_364_fu_6120_p2.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_445_fu_7883_p3() {
    tmp_445_fu_7883_p3 = esl_concat<4,3>(p_26_reg_1624.read(), ap_const_lv3_0);
}

void mnist_fp8::thread_tmp_446_fu_7895_p3() {
    tmp_446_fu_7895_p3 = esl_concat<4,4>(p_26_reg_1624.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_447_fu_7907_p3() {
    tmp_447_fu_7907_p3 = esl_concat<4,1>(p_26_reg_1624.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_448_cast_fu_5655_p1() {
    tmp_448_cast_fu_5655_p1 = esl_zext<64,11>(tmp_367_fu_5650_p2.read());
}

void mnist_fp8::thread_tmp_448_fu_7919_p2() {
    tmp_448_fu_7919_p2 = (!p_shl68_cast_fu_7903_p1.read().is_01() || !p_shl69_cast_fu_7915_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl68_cast_fu_7903_p1.read()) - sc_biguint<9>(p_shl69_cast_fu_7915_p1.read()));
}

void mnist_fp8::thread_tmp_449_fu_7286_p2() {
    tmp_449_fu_7286_p2 = (!lhs_V_3_cast_fu_7282_p1.read().is_01() || !tmp_490_cast_reg_11228.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_3_cast_fu_7282_p1.read()) + sc_biguint<9>(tmp_490_cast_reg_11228.read()));
}

void mnist_fp8::thread_tmp_44_fu_2342_p2() {
    tmp_44_fu_2342_p2 = (!p_3_reg_945.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_945.read() == ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_450_fu_7299_p1() {
    tmp_450_fu_7299_p1 = p_31_reg_1586.read().range(4-1, 0);
}

void mnist_fp8::thread_tmp_451_fu_7315_p2() {
    tmp_451_fu_7315_p2 = (!ap_const_lv5_1.is_01())? sc_lv<5>(): p_31_reg_1586.read() << (unsigned short)ap_const_lv5_1.to_uint();
}

void mnist_fp8::thread_tmp_452_fu_6898_p2() {
    tmp_452_fu_6898_p2 = (!tmp_184_cast_fu_6894_p1.read().is_01() || !tmp_432_reg_11104.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_184_cast_fu_6894_p1.read()) + sc_biguint<12>(tmp_432_reg_11104.read()));
}

void mnist_fp8::thread_tmp_453_fu_6921_p1() {
    tmp_453_fu_6921_p1 = conv3_0_load_to_int_fu_6908_p1.read().range(23-1, 0);
}

void mnist_fp8::thread_tmp_454_fu_6958_p1() {
    tmp_454_fu_6958_p1 = ireg_V_4_fu_6950_p3.read().range(63-1, 0);
}

void mnist_fp8::thread_tmp_456_fu_6980_p1() {
    tmp_456_fu_6980_p1 = ireg_V_4_fu_6950_p3.read().range(52-1, 0);
}

void mnist_fp8::thread_tmp_457_fu_7055_p1() {
    tmp_457_fu_7055_p1 = man_V_7_fu_7010_p3.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_458_cast_fu_6827_p1() {
    tmp_458_cast_fu_6827_p1 = esl_sext<10,9>(tmp_378_fu_6821_p2.read());
}

void mnist_fp8::thread_tmp_458_fu_7059_p4() {
    tmp_458_fu_7059_p4 = sh_amt_4_fu_7041_p3.read().range(11, 3);
}

void mnist_fp8::thread_tmp_459_fu_7092_p1() {
    tmp_459_fu_7092_p1 = tmp_248_fu_7087_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_45_cast_fu_3477_p1() {
    tmp_45_cast_fu_3477_p1 = esl_zext<6,5>(p_14_reg_1121.read());
}

void mnist_fp8::thread_tmp_45_fu_3026_p2() {
    tmp_45_fu_3026_p2 = (!ap_const_lv12_FFA.is_01() || !F2_fu_3014_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FFA) + sc_biguint<12>(F2_fu_3014_p2.read()));
}

void mnist_fp8::thread_tmp_460_fu_7112_p1() {
    tmp_460_fu_7112_p1 = tmp_251_fu_7106_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_461_fu_6672_p2() {
    tmp_461_fu_6672_p2 = (!tmp_382_reg_11011.read().is_01() || !tmp_119_cast_fu_6668_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_382_reg_11011.read()) + sc_biguint<12>(tmp_119_cast_fu_6668_p1.read()));
}

void mnist_fp8::thread_tmp_462_fu_6641_p2() {
    tmp_462_fu_6641_p2 = (!tmp_120_cast_fu_6637_p1.read().is_01() || !tmp_439_cast_reg_10993.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_120_cast_fu_6637_p1.read()) + sc_biguint<7>(tmp_439_cast_reg_10993.read()));
}

void mnist_fp8::thread_tmp_463_fu_6650_p3() {
    tmp_463_fu_6650_p3 = esl_concat<7,2>(tmp_462_fu_6641_p2.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_464_fu_6662_p2() {
    tmp_464_fu_6662_p2 = (!p_shl5_fu_6658_p1.read().is_01() || !tmp_508_cast_fu_6646_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl5_fu_6658_p1.read()) - sc_biguint<64>(tmp_508_cast_fu_6646_p1.read()));
}

void mnist_fp8::thread_tmp_465_fu_8935_p3() {
    tmp_465_fu_8935_p3 = esl_concat<4,4>(p_50_reg_1729.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_466_fu_8947_p3() {
    tmp_466_fu_8947_p3 = esl_concat<4,1>(p_50_reg_1729.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_467_cast_fu_4258_p1() {
    tmp_467_cast_fu_4258_p1 = esl_zext<64,13>(tmp_387_fu_4253_p2.read());
}

void mnist_fp8::thread_tmp_467_fu_8959_p2() {
    tmp_467_fu_8959_p2 = (!p_shl71_cast_fu_8943_p1.read().is_01() || !p_shl72_cast_fu_8955_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl71_cast_fu_8943_p1.read()) - sc_biguint<9>(p_shl72_cast_fu_8955_p1.read()));
}

void mnist_fp8::thread_tmp_468_cast_fu_4272_p1() {
    tmp_468_cast_fu_4272_p1 = esl_zext<64,9>(tmp_389_fu_4267_p2.read());
}

void mnist_fp8::thread_tmp_468_fu_7945_p2() {
    tmp_468_fu_7945_p2 = (!tmp_500_cast_reg_11401.read().is_01() || !tmp_111_cast_fu_7941_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_500_cast_reg_11401.read()) + sc_biguint<10>(tmp_111_cast_fu_7941_p1.read()));
}

void mnist_fp8::thread_tmp_469_fu_7950_p1() {
    tmp_469_fu_7950_p1 = tmp_468_fu_7945_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_46_cast_fu_3481_p1() {
    tmp_46_cast_fu_3481_p1 = esl_zext<6,5>(tmp_37_fu_3469_p3.read());
}

void mnist_fp8::thread_tmp_46_fu_3032_p2() {
    tmp_46_fu_3032_p2 = (!ap_const_lv12_6.is_01() || !F2_fu_3014_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_6) - sc_biguint<12>(F2_fu_3014_p2.read()));
}

void mnist_fp8::thread_tmp_470_fu_7962_p3() {
    tmp_470_fu_7962_p3 = esl_concat<10,1>(tmp_468_fu_7945_p2.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_471_fu_7974_p2() {
    tmp_471_fu_7974_p2 = (!p_shl73_cast_fu_7954_p3.read().is_01() || !p_shl74_cast_fu_7970_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl73_cast_fu_7954_p3.read()) - sc_bigint<12>(p_shl74_cast_fu_7970_p1.read()));
}

void mnist_fp8::thread_tmp_472_fu_6708_p2() {
    tmp_472_fu_6708_p2 = (!tmp_464_reg_11032.read().is_01() || !tmp_187_fu_6704_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_464_reg_11032.read()) + sc_biguint<64>(tmp_187_fu_6704_p1.read()));
}

void mnist_fp8::thread_tmp_473_fu_6713_p1() {
    tmp_473_fu_6713_p1 = tmp_472_fu_6708_p2.read().range(10-1, 0);
}

void mnist_fp8::thread_tmp_474_fu_6717_p1() {
    tmp_474_fu_6717_p1 = tmp_472_fu_6708_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_475_fu_6729_p2() {
    tmp_475_fu_6729_p2 = (!p_shl75_cast_fu_6721_p3.read().is_01() || !tmp_473_fu_6713_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl75_cast_fu_6721_p3.read()) - sc_biguint<10>(tmp_473_fu_6713_p1.read()));
}

void mnist_fp8::thread_tmp_476_fu_7381_p1() {
    tmp_476_fu_7381_p1 = p_39_reg_1597.read().range(4-1, 0);
}

void mnist_fp8::thread_tmp_477_fu_7391_p2() {
    tmp_477_fu_7391_p2 = (!r_V_15_fu_7385_p2.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): (sc_biguint<4>(r_V_15_fu_7385_p2.read()) < sc_biguint<4>(ap_const_lv4_E));
}

void mnist_fp8::thread_tmp_478_fu_7397_p2() {
    tmp_478_fu_7397_p2 = (!ap_const_lv4_1.is_01() || !tmp_476_fu_7381_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_476_fu_7381_p1.read()));
}

void mnist_fp8::thread_tmp_479_fu_7443_p1() {
    tmp_479_fu_7443_p1 = mul5_fu_9723_p2.read().range(24-1, 0);
}

void mnist_fp8::thread_tmp_47_fu_3727_p2() {
    tmp_47_fu_3727_p2 = (!relu1_0_V_q0.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(relu1_0_V_q0.read() == ap_const_lv8_0);
}

void mnist_fp8::thread_tmp_481_fu_7480_p4() {
    tmp_481_fu_7480_p4 = neg_mul5_fu_7475_p2.read().range(23, 16);
}

void mnist_fp8::thread_tmp_482_cast_fu_6439_p1() {
    tmp_482_cast_fu_6439_p1 = esl_sext<8,7>(tmp_421_fu_6433_p2.read());
}

void mnist_fp8::thread_tmp_482_fu_7490_p1() {
    tmp_482_fu_7490_p1 = esl_sext<12,8>(tmp_481_fu_7480_p4.read());
}

void mnist_fp8::thread_tmp_484_fu_7494_p1() {
    tmp_484_fu_7494_p1 = esl_sext<12,10>(tmp_483_reg_11291.read());
}

void mnist_fp8::thread_tmp_485_fu_7497_p3() {
    tmp_485_fu_7497_p3 = (!tmp_480_reg_11283.read()[0].is_01())? sc_lv<12>(): ((tmp_480_reg_11283.read()[0].to_bool())? tmp_482_fu_7490_p1.read(): tmp_484_fu_7494_p1.read());
}

void mnist_fp8::thread_tmp_486_fu_7573_p1() {
    tmp_486_fu_7573_p1 = grp_fu_7517_p2.read().range(9-1, 0);
}

void mnist_fp8::thread_tmp_487_cast_fu_6486_p1() {
    tmp_487_cast_fu_6486_p1 = esl_zext<64,11>(tmp_426_fu_6480_p2.read());
}

void mnist_fp8::thread_tmp_487_fu_7463_p1() {
    tmp_487_fu_7463_p1 = mul6_fu_9731_p2.read().range(25-1, 0);
}

void mnist_fp8::thread_tmp_488_cast_fu_6500_p1() {
    tmp_488_cast_fu_6500_p1 = esl_zext<64,12>(tmp_427_reg_10980.read());
}

void mnist_fp8::thread_tmp_488_fu_7528_p4() {
    tmp_488_fu_7528_p4 = neg_mul6_fu_7523_p2.read().range(24, 20);
}

void mnist_fp8::thread_tmp_489_fu_7538_p1() {
    tmp_489_fu_7538_p1 = esl_sext<13,5>(tmp_488_fu_7528_p4.read());
}

void mnist_fp8::thread_tmp_490_cast_fu_7254_p1() {
    tmp_490_cast_fu_7254_p1 = esl_zext<9,8>(tmp_428_fu_7246_p3.read());
}

void mnist_fp8::thread_tmp_491_fu_7542_p1() {
    tmp_491_fu_7542_p1 = esl_sext<13,6>(tmp_490_reg_11301.read());
}

void mnist_fp8::thread_tmp_492_fu_7552_p1() {
    tmp_492_fu_7552_p1 = p_v2_fu_7545_p3.read().range(3-1, 0);
}

void mnist_fp8::thread_tmp_493_fu_7562_p1() {
    tmp_493_fu_7562_p1 = p_v2_fu_7545_p3.read().range(3-1, 0);
}

void mnist_fp8::thread_tmp_494_fu_7577_p3() {
    tmp_494_fu_7577_p3 = esl_concat<3,4>(r_V_16_reg_11311.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_495_cast_fu_5758_p1() {
    tmp_495_cast_fu_5758_p1 = esl_zext<64,13>(tmp_433_fu_5753_p2.read());
}

void mnist_fp8::thread_tmp_495_fu_7588_p3() {
    tmp_495_fu_7588_p3 = esl_concat<3,1>(r_V_16_reg_11311.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_496_fu_7599_p2() {
    tmp_496_fu_7599_p2 = (!p_shl76_cast_fu_7584_p1.read().is_01() || !p_shl77_cast_fu_7595_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl76_cast_fu_7584_p1.read()) - sc_biguint<8>(p_shl77_cast_fu_7595_p1.read()));
}

void mnist_fp8::thread_tmp_497_cast_fu_7891_p1() {
    tmp_497_cast_fu_7891_p1 = esl_zext<8,7>(tmp_445_fu_7883_p3.read());
}

void mnist_fp8::thread_tmp_497_fu_7609_p2() {
    tmp_497_fu_7609_p2 = (!tmp_486_fu_7573_p1.read().is_01() || !tmp_534_cast_fu_7605_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_486_fu_7573_p1.read()) + sc_bigint<9>(tmp_534_cast_fu_7605_p1.read()));
}

void mnist_fp8::thread_tmp_498_fu_7615_p1() {
    tmp_498_fu_7615_p1 = tmp_497_fu_7609_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_499_fu_7629_p3() {
    tmp_499_fu_7629_p3 = esl_concat<9,1>(tmp_497_reg_11317.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_49_cast_fu_2556_p1() {
    tmp_49_cast_fu_2556_p1 = esl_sext<7,6>(tmp_16_fu_2550_p2.read());
}

void mnist_fp8::thread_tmp_49_fu_2348_p2() {
    tmp_49_fu_2348_p2 = (tmp_44_fu_2342_p2.read() | tmp_42_fu_2336_p2.read());
}

void mnist_fp8::thread_tmp_500_cast_fu_7925_p1() {
    tmp_500_cast_fu_7925_p1 = esl_sext<10,9>(tmp_448_fu_7919_p2.read());
}

void mnist_fp8::thread_tmp_500_fu_7640_p2() {
    tmp_500_fu_7640_p2 = (!p_shl78_cast_fu_7622_p3.read().is_01() || !p_shl79_cast_fu_7636_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl78_cast_fu_7622_p3.read()) - sc_bigint<12>(p_shl79_cast_fu_7636_p1.read()));
}

void mnist_fp8::thread_tmp_501_fu_7646_p2() {
    tmp_501_fu_7646_p2 = (!tmp_148_cast_fu_7619_p1.read().is_01() || !tmp_500_fu_7640_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_148_cast_fu_7619_p1.read()) + sc_biguint<12>(tmp_500_fu_7640_p2.read()));
}

void mnist_fp8::thread_tmp_502_fu_8985_p2() {
    tmp_502_fu_8985_p2 = (!tmp_513_cast_reg_11677.read().is_01() || !tmp_239_cast_fu_8981_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_513_cast_reg_11677.read()) + sc_biguint<10>(tmp_239_cast_fu_8981_p1.read()));
}

void mnist_fp8::thread_tmp_503_cast_fu_7291_p3() {
    tmp_503_cast_fu_7291_p3 = esl_concat<9,4>(tmp_449_fu_7286_p2.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_tmp_503_fu_8990_p1() {
    tmp_503_fu_8990_p1 = tmp_502_fu_8985_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_504_fu_9002_p3() {
    tmp_504_fu_9002_p3 = esl_concat<10,1>(tmp_502_fu_8985_p2.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_505_fu_9014_p2() {
    tmp_505_fu_9014_p2 = (!p_shl83_cast_fu_8994_p3.read().is_01() || !p_shl84_cast_fu_9010_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl83_cast_fu_8994_p3.read()) - sc_bigint<12>(p_shl84_cast_fu_9010_p1.read()));
}

void mnist_fp8::thread_tmp_506_cast_fu_6903_p1() {
    tmp_506_cast_fu_6903_p1 = esl_zext<64,12>(tmp_452_fu_6898_p2.read());
}

void mnist_fp8::thread_tmp_506_fu_6757_p4() {
    tmp_506_fu_6757_p4 = esl_concat<7,4>(esl_concat<3,4>(p_40_reg_1460.read(), r_V_14_reg_11045.read()), r_V_17_fu_6751_p2.read());
}

void mnist_fp8::thread_tmp_507_cast_fu_6677_p1() {
    tmp_507_cast_fu_6677_p1 = esl_zext<64,12>(tmp_461_fu_6672_p2.read());
}

void mnist_fp8::thread_tmp_507_fu_7714_p1() {
    tmp_507_fu_7714_p1 = msb_idx_6_fu_7708_p2.read().range(31-1, 0);
}

void mnist_fp8::thread_tmp_508_cast_fu_6646_p1() {
    tmp_508_cast_fu_6646_p1 = esl_zext<64,7>(tmp_462_fu_6641_p2.read());
}

void mnist_fp8::thread_tmp_509_fu_7736_p4() {
    tmp_509_fu_7736_p4 = msb_idx_7_fu_7726_p3.read().range(30, 5);
}

void mnist_fp8::thread_tmp_510_fu_7767_p1() {
    tmp_510_fu_7767_p1 = msb_idx_7_fu_7726_p3.read().range(3-1, 0);
}

void mnist_fp8::thread_tmp_511_fu_7771_p2() {
    tmp_511_fu_7771_p2 = (!ap_const_lv3_1.is_01() || !tmp_510_fu_7767_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(tmp_510_fu_7767_p1.read()));
}

void mnist_fp8::thread_tmp_512_fu_7777_p1() {
    tmp_512_fu_7777_p1 = esl_zext<8,3>(tmp_511_fu_7771_p2.read());
}

void mnist_fp8::thread_tmp_513_cast_fu_8965_p1() {
    tmp_513_cast_fu_8965_p1 = esl_sext<10,9>(tmp_467_fu_8959_p2.read());
}

void mnist_fp8::thread_tmp_513_fu_6766_p1() {
    tmp_513_fu_6766_p1 = esl_zext<64,11>(tmp_506_fu_6757_p4.read());
}

void mnist_fp8::thread_tmp_514_fu_7818_p1() {
    tmp_514_fu_7818_p1 = msb_idx_6_reg_11358.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_515_fu_6775_p2() {
    tmp_515_fu_6775_p2 = (!tmp_475_reg_11050.read().is_01() || !tmp_214_cast_fu_6771_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_475_reg_11050.read()) + sc_biguint<10>(tmp_214_cast_fu_6771_p1.read()));
}

void mnist_fp8::thread_tmp_516_fu_7861_p2() {
    tmp_516_fu_7861_p2 = (!tmp_503_cast_reg_11241.read().is_01() || !tmp_260_cast_fu_7857_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_503_cast_reg_11241.read()) + sc_biguint<13>(tmp_260_cast_fu_7857_p1.read()));
}

void mnist_fp8::thread_tmp_517_fu_9036_p2() {
    tmp_517_fu_9036_p2 = (!tmp_255_cast_fu_9032_p1.read().is_01() || !tmp_505_reg_11690.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_255_cast_fu_9032_p1.read()) + sc_biguint<12>(tmp_505_reg_11690.read()));
}

void mnist_fp8::thread_tmp_518_fu_9059_p1() {
    tmp_518_fu_9059_p1 = conv4_0_load_to_int_fu_9046_p1.read().range(23-1, 0);
}

void mnist_fp8::thread_tmp_519_fu_9096_p1() {
    tmp_519_fu_9096_p1 = ireg_V_7_fu_9088_p3.read().range(63-1, 0);
}

void mnist_fp8::thread_tmp_51_fu_2794_p3() {
    tmp_51_fu_2794_p3 = esl_concat<3,5>(p_7_reg_1054.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_521_fu_9118_p1() {
    tmp_521_fu_9118_p1 = ireg_V_7_fu_9088_p3.read().range(52-1, 0);
}

void mnist_fp8::thread_tmp_522_fu_9193_p1() {
    tmp_522_fu_9193_p1 = man_V_16_fu_9148_p3.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_523_fu_9197_p4() {
    tmp_523_fu_9197_p4 = sh_amt_5_fu_9179_p3.read().range(11, 3);
}

void mnist_fp8::thread_tmp_524_fu_9230_p1() {
    tmp_524_fu_9230_p1 = tmp_301_fu_9225_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_525_fu_9250_p1() {
    tmp_525_fu_9250_p1 = tmp_304_fu_9244_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_526_fu_8848_p2() {
    tmp_526_fu_8848_p2 = (!tmp_471_reg_11414.read().is_01() || !tmp_292_cast_fu_8844_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_471_reg_11414.read()) + sc_biguint<12>(tmp_292_cast_fu_8844_p1.read()));
}

void mnist_fp8::thread_tmp_527_fu_8760_p1() {
    tmp_527_fu_8760_p1 = msb_idx_8_fu_8754_p2.read().range(31-1, 0);
}

void mnist_fp8::thread_tmp_529_fu_8782_p4() {
    tmp_529_fu_8782_p4 = msb_idx_9_fu_8772_p3.read().range(30, 5);
}

void mnist_fp8::thread_tmp_52_fu_3046_p2() {
    tmp_52_fu_3046_p2 = (!F2_fu_3014_p2.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<1>(): sc_lv<1>(F2_fu_3014_p2.read() == ap_const_lv12_6);
}

void mnist_fp8::thread_tmp_530_fu_8813_p1() {
    tmp_530_fu_8813_p1 = msb_idx_9_fu_8772_p3.read().range(3-1, 0);
}

void mnist_fp8::thread_tmp_531_fu_8817_p2() {
    tmp_531_fu_8817_p2 = (!ap_const_lv3_1.is_01() || !tmp_530_fu_8813_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(tmp_530_fu_8813_p1.read()));
}

void mnist_fp8::thread_tmp_532_fu_8823_p1() {
    tmp_532_fu_8823_p1 = esl_zext<8,3>(tmp_531_fu_8817_p2.read());
}

void mnist_fp8::thread_tmp_533_fu_8008_p2() {
    tmp_533_fu_8008_p2 = (!tmp_194_cast_fu_8004_p1.read().is_01() || !tmp_497_cast_reg_11396.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_194_cast_fu_8004_p1.read()) + sc_biguint<8>(tmp_497_cast_reg_11396.read()));
}

void mnist_fp8::thread_tmp_534_cast_fu_7605_p1() {
    tmp_534_cast_fu_7605_p1 = esl_sext<9,8>(tmp_496_fu_7599_p2.read());
}

void mnist_fp8::thread_tmp_534_fu_8873_p1() {
    tmp_534_fu_8873_p1 = msb_idx_8_reg_11634.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_535_fu_8017_p3() {
    tmp_535_fu_8017_p3 = esl_concat<8,2>(tmp_533_fu_8008_p2.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_536_fu_8029_p2() {
    tmp_536_fu_8029_p2 = (!p_shl7_fu_8025_p1.read().is_01() || !tmp_560_cast_fu_8013_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl7_fu_8025_p1.read()) - sc_biguint<64>(tmp_560_cast_fu_8013_p1.read()));
}

void mnist_fp8::thread_tmp_537_fu_8067_p2() {
    tmp_537_fu_8067_p2 = (!tmp_536_reg_11435.read().is_01() || !tmp_259_fu_8063_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_536_reg_11435.read()) + sc_biguint<64>(tmp_259_fu_8063_p1.read()));
}

void mnist_fp8::thread_tmp_538_fu_8072_p1() {
    tmp_538_fu_8072_p1 = tmp_537_fu_8067_p2.read().range(11-1, 0);
}

void mnist_fp8::thread_tmp_539_cast_fu_7652_p1() {
    tmp_539_cast_fu_7652_p1 = esl_zext<64,12>(tmp_501_fu_7646_p2.read());
}

void mnist_fp8::thread_tmp_539_fu_8076_p1() {
    tmp_539_fu_8076_p1 = tmp_537_fu_8067_p2.read().range(9-1, 0);
}

void mnist_fp8::thread_tmp_53_fu_2806_p3() {
    tmp_53_fu_2806_p3 = esl_concat<3,2>(p_7_reg_1054.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_540_fu_8088_p2() {
    tmp_540_fu_8088_p2 = (!p_shl89_cast_fu_8080_p3.read().is_01() || !tmp_538_fu_8072_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl89_cast_fu_8080_p3.read()) - sc_biguint<11>(tmp_538_fu_8072_p1.read()));
}

void mnist_fp8::thread_tmp_541_fu_8116_p4() {
    tmp_541_fu_8116_p4 = esl_concat<8,4>(esl_concat<4,4>(p_51_reg_1671.read(), r_V_18_reg_11453.read()), r_V_19_fu_8110_p2.read());
}

void mnist_fp8::thread_tmp_542_fu_8125_p1() {
    tmp_542_fu_8125_p1 = esl_zext<64,12>(tmp_541_fu_8116_p4.read());
}

void mnist_fp8::thread_tmp_543_fu_8148_p1() {
    tmp_543_fu_8148_p1 = ireg_V_10_fu_8144_p1.read().range(63-1, 0);
}

void mnist_fp8::thread_tmp_544_fu_8134_p2() {
    tmp_544_fu_8134_p2 = (!tmp_331_cast_fu_8130_p1.read().is_01() || !tmp_540_reg_11458.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_331_cast_fu_8130_p1.read()) + sc_biguint<11>(tmp_540_reg_11458.read()));
}

void mnist_fp8::thread_tmp_545_fu_8170_p1() {
    tmp_545_fu_8170_p1 = ireg_V_10_fu_8144_p1.read().range(52-1, 0);
}

void mnist_fp8::thread_tmp_546_fu_8281_p1() {
    tmp_546_fu_8281_p1 = man_V_12_fu_8236_p3.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_547_fu_8285_p4() {
    tmp_547_fu_8285_p4 = sh_amt_6_fu_8267_p3.read().range(11, 3);
}

void mnist_fp8::thread_tmp_548_fu_8403_p1() {
    tmp_548_fu_8403_p1 = tmp_321_fu_8398_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_549_fu_8423_p1() {
    tmp_549_fu_8423_p1 = tmp_324_fu_8417_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_54_fu_3075_p2() {
    tmp_54_fu_3075_p2 = (!sh_amt_reg_10075.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_reg_10075.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp8::thread_tmp_550_fu_8184_p1() {
    tmp_550_fu_8184_p1 = ireg_V_11_fu_8180_p1.read().range(63-1, 0);
}

void mnist_fp8::thread_tmp_551_cast_fu_6780_p1() {
    tmp_551_cast_fu_6780_p1 = esl_zext<64,10>(tmp_515_fu_6775_p2.read());
}

void mnist_fp8::thread_tmp_552_cast_fu_7866_p1() {
    tmp_552_cast_fu_7866_p1 = esl_zext<64,13>(tmp_516_fu_7861_p2.read());
}

void mnist_fp8::thread_tmp_552_fu_8206_p1() {
    tmp_552_fu_8206_p1 = ireg_V_11_fu_8180_p1.read().range(52-1, 0);
}

void mnist_fp8::thread_tmp_553_fu_8366_p1() {
    tmp_553_fu_8366_p1 = man_V_15_fu_8321_p3.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_554_fu_8370_p4() {
    tmp_554_fu_8370_p4 = sh_amt_7_fu_8352_p3.read().range(11, 3);
}

void mnist_fp8::thread_tmp_555_fu_8552_p1() {
    tmp_555_fu_8552_p1 = tmp_345_fu_8547_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_556_fu_8572_p1() {
    tmp_556_fu_8572_p1 = tmp_348_fu_8566_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_558_cast_fu_9041_p1() {
    tmp_558_cast_fu_9041_p1 = esl_zext<64,12>(tmp_517_fu_9036_p2.read());
}

void mnist_fp8::thread_tmp_559_cast_fu_8919_p1() {
    tmp_559_cast_fu_8919_p1 = esl_zext<64,12>(tmp_526_reg_11654.read());
}

void mnist_fp8::thread_tmp_55_fu_3733_p2() {
    tmp_55_fu_3733_p2 = (!ap_const_lv8_0.is_01() || !relu1_0_V_q0.read().is_01())? sc_lv<8>(): (sc_biguint<8>(ap_const_lv8_0) - sc_biguint<8>(relu1_0_V_q0.read()));
}

void mnist_fp8::thread_tmp_560_cast_fu_8013_p1() {
    tmp_560_cast_fu_8013_p1 = esl_zext<64,8>(tmp_533_fu_8008_p2.read());
}

void mnist_fp8::thread_tmp_56_fu_6104_p2() {
    tmp_56_fu_6104_p2 = (!p_20_reg_1390.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_20_reg_1390.read() != ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_578_cast_fu_8139_p1() {
    tmp_578_cast_fu_8139_p1 = esl_zext<64,11>(tmp_544_fu_8134_p2.read());
}

void mnist_fp8::thread_tmp_57_fu_6110_p2() {
    tmp_57_fu_6110_p2 = (!p_20_reg_1390.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_20_reg_1390.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp8::thread_tmp_58_fu_2818_p2() {
    tmp_58_fu_2818_p2 = (!p_shl22_cast_fu_2802_p1.read().is_01() || !p_shl23_cast_fu_2814_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl22_cast_fu_2802_p1.read()) - sc_biguint<9>(p_shl23_cast_fu_2814_p1.read()));
}

void mnist_fp8::thread_tmp_59_fu_2598_p2() {
    tmp_59_fu_2598_p2 = (!tmp_69_cast_reg_9904.read().is_01() || !tmp_11_cast_fu_2594_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_69_cast_reg_9904.read()) + sc_biguint<10>(tmp_11_cast_fu_2594_p1.read()));
}

void mnist_fp8::thread_tmp_60_fu_2603_p1() {
    tmp_60_fu_2603_p1 = tmp_59_fu_2598_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_61_fu_2615_p3() {
    tmp_61_fu_2615_p3 = esl_concat<10,2>(tmp_59_fu_2598_p2.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_62_cast_fu_5114_p1() {
    tmp_62_cast_fu_5114_p1 = esl_zext<10,5>(p_23_reg_1266.read());
}

void mnist_fp8::thread_tmp_62_fu_2627_p2() {
    tmp_62_fu_2627_p2 = (!p_shl17_cast_fu_2607_p3.read().is_01() || !p_shl18_cast_fu_2623_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl17_cast_fu_2607_p3.read()) - sc_bigint<13>(p_shl18_cast_fu_2623_p1.read()));
}

void mnist_fp8::thread_tmp_63_fu_4840_p2() {
    tmp_63_fu_4840_p2 = (!p_Val2_3_reg_1186.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(p_Val2_3_reg_1186.read() == ap_const_lv8_0);
}

void mnist_fp8::thread_tmp_64_cast_fu_4064_p1() {
    tmp_64_cast_fu_4064_p1 = esl_zext<6,3>(p_24_reg_1198.read());
}

void mnist_fp8::thread_tmp_64_fu_2360_p2() {
    tmp_64_fu_2360_p2 = (!r_V_fu_2354_p2.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): (sc_biguint<5>(r_V_fu_2354_p2.read()) < sc_biguint<5>(ap_const_lv5_1C));
}

void mnist_fp8::thread_tmp_65_fu_2366_p2() {
    tmp_65_fu_2366_p2 = (!ap_const_lv5_3.is_01() || !p_3_reg_945.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_3) + sc_biguint<5>(p_3_reg_945.read()));
}

void mnist_fp8::thread_tmp_66_cast_fu_6574_p1() {
    tmp_66_cast_fu_6574_p1 = esl_zext<10,4>(p_25_reg_1436.read());
}

void mnist_fp8::thread_tmp_66_fu_2414_p1() {
    tmp_66_fu_2414_p1 = mul_fu_9674_p2.read().range(23-1, 0);
}

void mnist_fp8::thread_tmp_67_fu_3825_p2() {
    tmp_67_fu_3825_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_1_cast_fu_3802_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_1_cast_fu_3802_p1.read()));
}

void mnist_fp8::thread_tmp_69_cast_fu_2578_p1() {
    tmp_69_cast_fu_2578_p1 = esl_sext<10,9>(tmp_18_fu_2572_p2.read());
}

void mnist_fp8::thread_tmp_69_fu_2431_p4() {
    tmp_69_fu_2431_p4 = neg_mul_fu_2426_p2.read().range(22, 16);
}

void mnist_fp8::thread_tmp_6_cast_fu_2190_p1() {
    tmp_6_cast_fu_2190_p1 = esl_zext<11,5>(p_1_reg_827.read());
}

void mnist_fp8::thread_tmp_6_fu_2153_p2() {
    tmp_6_fu_2153_p2 = (!p_shl8_cast_fu_2137_p1.read().is_01() || !p_shl9_cast_fu_2149_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl8_cast_fu_2137_p1.read()) - sc_biguint<11>(p_shl9_cast_fu_2149_p1.read()));
}

void mnist_fp8::thread_tmp_70_fu_6192_p2() {
    tmp_70_fu_6192_p2 = (!p_30_reg_1401.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_30_reg_1401.read() != ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_71_fu_6198_p2() {
    tmp_71_fu_6198_p2 = (!p_30_reg_1401.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_30_reg_1401.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp8::thread_tmp_72_fu_5264_p3() {
    tmp_72_fu_5264_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_328_reg_10653.read());
}

void mnist_fp8::thread_tmp_73_fu_4129_p2() {
    tmp_73_fu_4129_p2 = (!ap_const_lv8_0.is_01() || !p_Val2_3_reg_1186.read().is_01())? sc_lv<8>(): (sc_biguint<8>(ap_const_lv8_0) - sc_biguint<8>(p_Val2_3_reg_1186.read()));
}

void mnist_fp8::thread_tmp_74_fu_2441_p1() {
    tmp_74_fu_2441_p1 = esl_sext<11,7>(tmp_69_fu_2431_p4.read());
}

void mnist_fp8::thread_tmp_76_fu_3882_p2() {
    tmp_76_fu_3882_p2 = (!p_Result_9_fu_3872_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_9_fu_3872_p4.read() != ap_const_lv8_9E);
}

void mnist_fp8::thread_tmp_77_cast_fu_5165_p1() {
    tmp_77_cast_fu_5165_p1 = esl_zext<13,5>(p_28_reg_1277.read());
}

void mnist_fp8::thread_tmp_77_fu_2445_p1() {
    tmp_77_fu_2445_p1 = esl_sext<11,8>(tmp_75_reg_9861.read());
}

void mnist_fp8::thread_tmp_78_fu_3080_p1() {
    tmp_78_fu_3080_p1 = esl_zext<54,32>(sh_amt_cast_fu_3072_p1.read());
}

void mnist_fp8::thread_tmp_79_fu_2448_p3() {
    tmp_79_fu_2448_p3 = (!tmp_68_reg_9850.read()[0].is_01())? sc_lv<11>(): ((tmp_68_reg_9850.read()[0].to_bool())? tmp_74_fu_2441_p1.read(): tmp_77_fu_2445_p1.read());
}

void mnist_fp8::thread_tmp_7_cast_fu_2530_p1() {
    tmp_7_cast_fu_2530_p1 = esl_zext<6,3>(p_2_reg_972.read());
}

void mnist_fp8::thread_tmp_7_fu_2210_p3() {
    tmp_7_fu_2210_p3 = esl_concat<5,5>(p_s_reg_933.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_80_cast_fu_4157_p1() {
    tmp_80_cast_fu_4157_p1 = esl_zext<10,5>(r_V_9_fu_4151_p2.read());
}

void mnist_fp8::thread_tmp_80_fu_2477_p2() {
    tmp_80_fu_2477_p2 = (!ap_const_lv11_5.is_01())? sc_lv<11>(): grp_fu_2468_p2.read() << (unsigned short)ap_const_lv11_5.to_uint();
}

void mnist_fp8::thread_tmp_81_fu_4196_p1() {
    tmp_81_fu_4196_p1 = esl_zext<64,2>(p_29_reg_1221.read());
}

void mnist_fp8::thread_tmp_82_fu_3084_p2() {
    tmp_82_fu_3084_p2 = (!man_V_2_reg_10064.read().is_01() || !tmp_78_fu_3080_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_2_reg_10064.read()) >> (unsigned short)tmp_78_fu_3080_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_83_fu_2483_p2() {
    tmp_83_fu_2483_p2 = (!ap_const_lv11_2.is_01())? sc_lv<11>(): grp_fu_2468_p2.read() << (unsigned short)ap_const_lv11_2.to_uint();
}

void mnist_fp8::thread_tmp_84_fu_3904_p3() {
    tmp_84_fu_3904_p3 = esl_concat<1,8>(is_neg_reg_10239.read(), p_Repl2_1_trunc_fu_3898_p2.read());
}

void mnist_fp8::thread_tmp_85_fu_7270_p2() {
    tmp_85_fu_7270_p2 = (!p_31_reg_1586.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_31_reg_1586.read() != ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_86_fu_7276_p2() {
    tmp_86_fu_7276_p2 = (!p_31_reg_1586.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_31_reg_1586.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp8::thread_tmp_87_fu_6237_p3() {
    tmp_87_fu_6237_p3 = (!tmp_396_fu_6225_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_396_fu_6225_p2.read()[0].to_bool())? r_V_11_fu_6219_p2.read(): tmp_403_fu_6231_p2.read());
}

void mnist_fp8::thread_tmp_88_cast_fu_6453_p1() {
    tmp_88_cast_fu_6453_p1 = esl_zext<11,4>(tmp_87_reg_10911.read());
}

void mnist_fp8::thread_tmp_88_fu_2489_p2() {
    tmp_88_fu_2489_p2 = (!tmp_80_fu_2477_p2.read().is_01() || !tmp_83_fu_2483_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_80_fu_2477_p2.read()) - sc_biguint<11>(tmp_83_fu_2483_p2.read()));
}

void mnist_fp8::thread_tmp_89_fu_3100_p1() {
    tmp_89_fu_3100_p1 = esl_sext<32,8>(tmp_148_reg_10087.read());
}

void mnist_fp8::thread_tmp_8_fu_2372_p3() {
    tmp_8_fu_2372_p3 = (!tmp_64_fu_2360_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_64_fu_2360_p2.read()[0].to_bool())? r_V_fu_2354_p2.read(): tmp_65_fu_2366_p2.read());
}

void mnist_fp8::thread_tmp_90_cast_fu_6245_p1() {
    tmp_90_cast_fu_6245_p1 = esl_zext<5,4>(tmp_87_fu_6237_p3.read());
}

void mnist_fp8::thread_tmp_90_fu_3103_p2() {
    tmp_90_fu_3103_p2 = (!sh_amt_cast_fu_3072_p1.read().is_01())? sc_lv<32>(): tmp_89_fu_3100_p1.read() << (unsigned short)sh_amt_cast_fu_3072_p1.read().to_uint();
}

void mnist_fp8::thread_tmp_93_cast_fu_3926_p1() {
    tmp_93_cast_fu_3926_p1 = esl_zext<13,5>(p_14_reg_1121.read());
}

void mnist_fp8::thread_tmp_93_fu_2495_p2() {
    tmp_93_fu_2495_p2 = (!tmp_10_cast_fu_2474_p1.read().is_01() || !tmp_88_fu_2489_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_10_cast_fu_2474_p1.read()) + sc_biguint<11>(tmp_88_fu_2489_p2.read()));
}

void mnist_fp8::thread_tmp_94_fu_2509_p2() {
    tmp_94_fu_2509_p2 = (!tmp_11_reg_9814.read().is_01() || !tmp_16_cast_fu_2505_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_11_reg_9814.read()) + sc_biguint<11>(tmp_16_cast_fu_2505_p1.read()));
}

void mnist_fp8::thread_tmp_96_fu_3243_p3() {
    tmp_96_fu_3243_p3 = esl_concat<3,5>(p_6_reg_1087.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_97_cast_fu_2199_p1() {
    tmp_97_cast_fu_2199_p1 = esl_sext<64,11>(tmp_34_fu_2194_p2.read());
}

void mnist_fp8::thread_tmp_97_fu_3255_p3() {
    tmp_97_fu_3255_p3 = esl_concat<3,1>(p_6_reg_1087.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_98_fu_3267_p2() {
    tmp_98_fu_3267_p2 = (!p_shl26_cast_fu_3251_p1.read().is_01() || !p_shl27_cast_fu_3263_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl26_cast_fu_3251_p1.read()) - sc_biguint<9>(p_shl27_cast_fu_3263_p1.read()));
}

void mnist_fp8::thread_tmp_99_fu_5261_p1() {
    tmp_99_fu_5261_p1 = esl_zext<12,11>(p_Result_6_reg_10648.read());
}

void mnist_fp8::thread_tmp_9_fu_2141_p3() {
    tmp_9_fu_2141_p3 = esl_concat<5,2>(p_0_reg_816.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_V_2_fu_5782_p3() {
    tmp_V_2_fu_5782_p3 = (!tmp_434_reg_10789.read()[0].is_01())? sc_lv<8>(): ((tmp_434_reg_10789.read()[0].to_bool())? tmp_188_reg_10795.read(): relu2_0_V_load_reg_10783.read());
}

void mnist_fp8::thread_tmp_user_V_fu_2108_p1() {
    tmp_user_V_fu_2108_p1 = stream_in_V_user_V_0_data_out.read();
}

void mnist_fp8::thread_w1_V_fu_9392_p2() {
    w1_V_fu_9392_p2 = (!p_65_reg_1784.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_65_reg_1784.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_w_V_fu_5640_p2() {
    w_V_fu_5640_p2 = (!p_33_reg_1310.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_33_reg_1310.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_w_conv1_address0() {
    w_conv1_address0 =  (sc_lv<6>) (tmp_390_cast_fu_2777_p1.read());
}

void mnist_fp8::thread_w_conv1_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state37.read())) {
        w_conv1_ce0 = ap_const_logic_1;
    } else {
        w_conv1_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_w_conv2_address0() {
    w_conv2_address0 =  (sc_lv<8>) (tmp_468_cast_fu_4272_p1.read());
}

void mnist_fp8::thread_w_conv2_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state96.read())) {
        w_conv2_ce0 = ap_const_logic_1;
    } else {
        w_conv2_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_w_conv3_address0() {
    w_conv3_address0 =  (sc_lv<9>) (tmp_551_cast_fu_6780_p1.read());
}

void mnist_fp8::thread_w_conv3_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state163.read())) {
        w_conv3_ce0 = ap_const_logic_1;
    } else {
        w_conv3_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_w_conv4_address0() {
    w_conv4_address0 =  (sc_lv<10>) (tmp_578_cast_fu_8139_p1.read());
}

void mnist_fp8::thread_w_conv4_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state219.read())) {
        w_conv4_ce0 = ap_const_logic_1;
    } else {
        w_conv4_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_w_conv5_address0() {
    w_conv5_address0 = ap_const_lv11_0;
}

void mnist_fp8::thread_w_conv5_address1() {
    w_conv5_address1 = ap_const_lv11_0;
}

void mnist_fp8::thread_w_conv5_ce0() {
    w_conv5_ce0 = ap_const_logic_0;
}

void mnist_fp8::thread_w_conv5_ce1() {
    w_conv5_ce1 = ap_const_logic_0;
}

void mnist_fp8::thread_w_conv5_d0() {
    w_conv5_d0 = ap_const_lv32_0;
}

void mnist_fp8::thread_w_conv5_d1() {
    w_conv5_d1 = ap_const_lv32_0;
}

void mnist_fp8::thread_w_conv5_we0() {
    w_conv5_we0 = ap_const_logic_0;
}

void mnist_fp8::thread_w_conv5_we1() {
    w_conv5_we1 = ap_const_logic_0;
}

void mnist_fp8::thread_w_conv6_address0() {
    w_conv6_address0 = ap_const_lv12_0;
}

void mnist_fp8::thread_w_conv6_address1() {
    w_conv6_address1 = ap_const_lv12_0;
}

void mnist_fp8::thread_w_conv6_ce0() {
    w_conv6_ce0 = ap_const_logic_0;
}

void mnist_fp8::thread_w_conv6_ce1() {
    w_conv6_ce1 = ap_const_logic_0;
}

void mnist_fp8::thread_w_conv6_d0() {
    w_conv6_d0 = ap_const_lv32_0;
}

void mnist_fp8::thread_w_conv6_d1() {
    w_conv6_d1 = ap_const_lv32_0;
}

void mnist_fp8::thread_w_conv6_we0() {
    w_conv6_we0 = ap_const_logic_0;
}

void mnist_fp8::thread_w_conv6_we1() {
    w_conv6_we1 = ap_const_logic_0;
}

void mnist_fp8::thread_w_dense_1_address0() {
    w_dense_1_address0 = ap_const_lv8_0;
}

void mnist_fp8::thread_w_dense_1_address1() {
    w_dense_1_address1 = ap_const_lv8_0;
}

void mnist_fp8::thread_w_dense_1_ce0() {
    w_dense_1_ce0 = ap_const_logic_0;
}

void mnist_fp8::thread_w_dense_1_ce1() {
    w_dense_1_ce1 = ap_const_logic_0;
}

void mnist_fp8::thread_w_dense_1_d0() {
    w_dense_1_d0 = ap_const_lv32_0;
}

void mnist_fp8::thread_w_dense_1_d1() {
    w_dense_1_d1 = ap_const_lv32_0;
}

void mnist_fp8::thread_w_dense_1_we0() {
    w_dense_1_we0 = ap_const_logic_0;
}

void mnist_fp8::thread_w_dense_1_we1() {
    w_dense_1_we1 = ap_const_logic_0;
}

void mnist_fp8::thread_xx1_V_fu_4046_p2() {
    xx1_V_fu_4046_p2 = (!p_18_reg_1174.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_18_reg_1174.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_xx2_V_fu_6619_p2() {
    xx2_V_fu_6619_p2 = (!p_32_reg_1448.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_32_reg_1448.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_xx3_V_fu_7986_p2() {
    xx3_V_fu_7986_p2 = (!p_44_reg_1647.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_44_reg_1647.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_xx4_V_fu_9488_p2() {
    xx4_V_fu_9488_p2 = (!p_58_reg_1872.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_58_reg_1872.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_xx5_V_fu_9632_p2() {
    xx5_V_fu_9632_p2 = (!p_67_reg_2004.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_67_reg_2004.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_xx_V_fu_2639_p2() {
    xx_V_fu_2639_p2 = (!p_8_reg_995.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_8_reg_995.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_yy1_V_fu_3995_p2() {
    yy1_V_fu_3995_p2 = (!p_10_reg_1162.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_10_reg_1162.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_yy2_V_fu_6568_p2() {
    yy2_V_fu_6568_p2 = (!p_25_reg_1436.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_25_reg_1436.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_yy3_V_fu_7935_p2() {
    yy3_V_fu_7935_p2 = (!p_35_reg_1635.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_35_reg_1635.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_yy4_V_fu_9476_p2() {
    yy4_V_fu_9476_p2 = (!p_49_reg_1861.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_49_reg_1861.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_yy5_V_fu_9620_p2() {
    yy5_V_fu_9620_p2 = (!p_62_reg_1993.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_62_reg_1993.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_yy_V_fu_2588_p2() {
    yy_V_fu_2588_p2 = (!p_5_reg_983.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_5_reg_983.read()) + sc_biguint<5>(ap_const_lv5_1));
}

}

