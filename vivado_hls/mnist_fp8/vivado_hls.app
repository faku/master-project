<project xmlns="com.autoesl.autopilot.project" top="mnist_fp8" name="mnist_fp8">
    <includePaths/>
    <libraryPaths/>
    <Simulation>
        <SimFlow name="csim"/>
    </Simulation>
    <files xmlns="">
        <file name="mnist_fp8/src/w_dense_1.h" sc="0" tb="false" cflags=""/>
        <file name="mnist_fp8/src/w_conv6.h" sc="0" tb="false" cflags=""/>
        <file name="mnist_fp8/src/w_conv5.h" sc="0" tb="false" cflags=""/>
        <file name="mnist_fp8/src/w_conv4.h" sc="0" tb="false" cflags=""/>
        <file name="mnist_fp8/src/w_conv3.h" sc="0" tb="false" cflags=""/>
        <file name="mnist_fp8/src/w_conv2.h" sc="0" tb="false" cflags=""/>
        <file name="mnist_fp8/src/w_conv1.h" sc="0" tb="false" cflags=""/>
        <file name="mnist_fp8/src/mnist_fp8.cpp" sc="0" tb="false" cflags=""/>
    </files>
    <solutions xmlns="">
        <solution name="solution1" status="active"/>
    </solutions>
</project>

