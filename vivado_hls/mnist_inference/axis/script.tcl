############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project mnist_inference
set_top mnist_inference
add_files mnist_inference/src/w_reshape1.h
add_files mnist_inference/src/w_conv2.h
add_files mnist_inference/src/w_conv1.h
add_files mnist_inference/src/w_add3.h
add_files mnist_inference/src/w_add2.h
add_files mnist_inference/src/w_add1.h
add_files mnist_inference/src/mnist.cpp
add_files -tb mnist_inference/tb/mnist_tb.cpp -cflags "-Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas"
open_solution "axis"
set_part {xc7z020clg400-1} -tool vivado
create_clock -period 10 -name default
#source "./mnist_inference/axis/directives.tcl"
csim_design
csynth_design
cosim_design -setup -rtl vhdl -tool xsim
export_design -rtl vhdl -format ip_catalog
