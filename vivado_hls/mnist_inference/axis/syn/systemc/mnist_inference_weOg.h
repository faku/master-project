// ==============================================================
// File generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2018.2
// Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
// 
// ==============================================================

#ifndef __mnist_inference_weOg_H__
#define __mnist_inference_weOg_H__


#include <systemc>
using namespace sc_core;
using namespace sc_dt;




#include <iostream>
#include <fstream>

struct mnist_inference_weOg_ram : public sc_core::sc_module {

  static const unsigned DataWidth = 32;
  static const unsigned AddressRange = 16;
  static const unsigned AddressWidth = 4;

//latency = 1
//input_reg = 1
//output_reg = 0
sc_core::sc_in <sc_lv<AddressWidth> > address0;
sc_core::sc_in <sc_logic> ce0;
sc_core::sc_out <sc_lv<DataWidth> > q0;
sc_core::sc_in<sc_logic> reset;
sc_core::sc_in<bool> clk;


sc_lv<DataWidth> ram[AddressRange];


   SC_CTOR(mnist_inference_weOg_ram) {
        ram[0] = "0b10111101101010000111001000010010";
        ram[1] = "0b10111101110111101111011010011000";
        ram[2] = "0b10111110000100000110110010101111";
        ram[3] = "0b10111110010100011100100100111001";
        ram[4] = "0b10111110001101110110111101010110";
        ram[5] = "0b10111110010111001001101111100001";
        ram[6] = "0b10111110000010010000010000110010";
        ram[7] = "0b10111110010010000110110000000101";
        ram[8] = "0b10111110100010010101100000100110";
        ram[9] = "0b10111110100001000011010001100100";
        ram[10] = "0b10111101100110111111011110110110";
        ram[11] = "0b00111100010110011010010110111111";
        ram[12] = "0b10111011100100011010010001100001";
        ram[13] = "0b10111110110101000101100011101001";
        ram[14] = "0b10111110001101110001010100000111";
        ram[15] = "0b10111101000111100101010110100011";


SC_METHOD(prc_write_0);
  sensitive<<clk.pos();
   }


void prc_write_0()
{
    if (ce0.read() == sc_dt::Log_1) 
    {
            if(address0.read().is_01() && address0.read().to_uint()<AddressRange)
              q0 = ram[address0.read().to_uint()];
            else
              q0 = sc_lv<DataWidth>();
    }
}


}; //endmodule


SC_MODULE(mnist_inference_weOg) {


static const unsigned DataWidth = 32;
static const unsigned AddressRange = 16;
static const unsigned AddressWidth = 4;

sc_core::sc_in <sc_lv<AddressWidth> > address0;
sc_core::sc_in<sc_logic> ce0;
sc_core::sc_out <sc_lv<DataWidth> > q0;
sc_core::sc_in<sc_logic> reset;
sc_core::sc_in<bool> clk;


mnist_inference_weOg_ram* meminst;


SC_CTOR(mnist_inference_weOg) {
meminst = new mnist_inference_weOg_ram("mnist_inference_weOg_ram");
meminst->address0(address0);
meminst->ce0(ce0);
meminst->q0(q0);

meminst->reset(reset);
meminst->clk(clk);
}
~mnist_inference_weOg() {
    delete meminst;
}


};//endmodule
#endif
