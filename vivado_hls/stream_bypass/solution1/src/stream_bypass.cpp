#include <ap_axi_sdata.h>
#include <hls_video.h>

typedef ap_axiu<32,1,1,1> pixel_t;
typedef hls::stream<pixel_t> stream_t;

void stream_bypass(stream_t& stream_in, ap_uint<1> enable, stream_t& stream_out) {
#pragma HLS INTERFACE axis port=stream_in
#pragma HLS INTERFACE ap_none port=enable
#pragma HLS INTERFACE axis port=stream_out

    pixel_t pixel;

    if (enable.to_uint() == 0) {
        while (stream_in.empty() == 0) {
            stream_in >> pixel;
        }
    }
    else {
        while (stream_in.empty() == 0) {
            stream_in >> pixel;
            stream_out << pixel;
        }
    }
}
