#include <ap_int.h>
#include <hls_video.h>

typedef ap_axiu<24,1,1,1> pixel_t;
typedef hls::stream<pixel_t> stream_t;

#define HEIGHT  1080
#define WIDTH   1920

void stream_duplicate(stream_t &stream_in, stream_t &stream0_out, stream_t &stream1_out) {
#pragma HLS INTERFACE axis port=stream_in
#pragma HLS INTERFACE axis port=stream0_out
#pragma HLS INTERFACE axis port=stream1_out

    hls::Mat<HEIGHT, WIDTH, HLS_8UC3>    input_data(HEIGHT, WIDTH);
    hls::Mat<HEIGHT, WIDTH, HLS_8UC3> output_data_0(HEIGHT, WIDTH);
    hls::Mat<HEIGHT, WIDTH, HLS_8UC3> output_data_1(HEIGHT, WIDTH);

#pragma HLS dataflow

    hls::AXIvideo2Mat(stream_in, input_data);
    hls::Duplicate(input_data, output_data_0, output_data_1);
    hls::Mat2AXIvideo(output_data_0, stream0_out);
    hls::Mat2AXIvideo(output_data_1, stream1_out);
}
